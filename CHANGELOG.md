# Changelog

### 2.12.0 (2022-04-29)
* **backend** feat: provide luca Pay functionality
* **backend** feat: added feature flag for payment features
* **backend** feat: added jwt for payment api communication
* **locations** feat: added luca Pay
* **locations** feat: handle feature flag for payment features
* **e2e** fix: improved test stability

### 2.11.1 (2022-04-25)
* **backend** chore: remove moment-timezone
* **locations** feat: add warning banner in case of an expired daily key
* **contact-form** feat: add warning banner in case of an expired daily key
* **scanner** feat: add warning banner in case of an expired daily key

### 2.11.0 (2022-04-06)
* **backend** fix: logJobCompletion for deleteOldTracingProcesses
* **backend** feat: improve password strength validation
* **backend** feat: distinguish between voluntary checkin with and without contact data
* **backend** feat: allow deletion of voluntary check-ins
* **backend** feat: adjust check out route so apps can set check out time
* **backend** feat: remove timing difference for MAC comparisons
* **backend** feat: mirror certificate validation rules
* **backend** feat: make signedKey attributes in key tables not nullable
* **backend** feat: extract port binding into own docker compose file
* **backend** feat: add feature flag rollout endpoints
* **backend** feat: enable restaurants to add a menu to their account
* **locations** fix: website, menu and other links collapsable area does not collapse
* **locations** fix: last active column shows wrong time
* **locations** fix: show guest list modal tooltip size and position issue
* **locations** fix: automatic “inactive” state not working
* **locations** fix: adding business data on registration missing description
* **locations** fix: alignment for check-in entry policy
* **locations** fix: deactivating device does not notify user
* **locations** feat: apply cohesive naming to qr code download
* **locations** feat: introduce anonymous check-ins
* **locations** feat: extend error messages during registration
* **locations** feat: implement language picker
* **locations** feat: integrate password strength meter
* **locations** feat: implement additional area information in dashboard
* **locations** feat: implement design updates
* **locations** feat: introduce new flow and design for location creation
* **locations** feat: introduce new flow and design for area creation
* **health-department** fix: screen “hash match but no name match” is shown for a moment in the check and select process
* **health-department** fix: missing sorting for contact people
* **health-department** feat: handle voluntary and anonymous checkins
* **health-department** feat: sort “not found” list alphabetically
* **health-department** feat: switch password strength meter
* **scanner** feat: handle anonymous qr codes
* **register-badge** fix: several minor errors

### 2.10.3 (2022-04-01)
* **webapp** feat: reintroduce the screen to notify about missing features

### 2.10.2 (2022-03-29)
* **backend** feat: provide minimum version for business app
* **locations** feat: handle data requests with exp value
* **health-department** feat: set exp value to 28 days
* **webapp** feat: change Check-in Webapp to Link Webapp

### 2.10.1 (2022-03-08)
* **backend** fix: invalid trace timestamps through manual checkout by location owner
* **locations** feat: sidebar now hold the overview of locations and areas
* **locations** feat: move location creation into sidebar
* **locations** feat: divided dashboard into profile and check-in tab
* **locations** feat: remove functionalities like location overview and locations creation from header

### 2.10.0 (2022-02-24)
* **backend** fix: creation of location allows creating more additionalData questions than allowed
* **backend** fix: improper error handling if changing operator mail to existing user
* **backend** feat: allow language change by the user
* **backend** feat: remove email template model and reference
* **backend** feat: add encrypted routes to v4/keys/daily
* **backend** feat: block API requests from outdated user agents
* **backend** feat: add indices for cronjobs
* **backend** feat: store operator phone number
* **backend** feat: calculate duration for pow challenges
* **locations** fix: checkout button in table allocation modal not working
* **locations** feat: improve mobile device detection
* **locations** feat: rework navigation button to last seen location
* **locations** feat: integrate google maps in location creation flow
* **locations** feat: design rework for guestlist modal and overview in dashboard
* **locations** feat: improve qr code scanning for operator app activation process
* **locations** feat: integrate download of HD signature in open data requests
* **health-department** fix: second confirmation implemented in wrong place
* **health-department** fix: faulty grey highlights for name column in guest list
* **health-department** fix: device view design and wording issues
* **health-department** fix: adapt and improve error messages
* **health-department** fix: all tracing processes are being loaded when clicking on a single one
* **health-department** feat: improve HD handling for locations/timeframes without check-ins
* **health-department** feat: adapt sent messages overview
* **health-department** feat: fix breaking changes for sormas API
* **health-department** feat: integrate an empty message template
* **health-department** feat: implement device type of users in guestlist
* **contact-form** feat: upgrade contact form to v4/keys/daily routes
* **register-badge** fix: inconsistent buttons on successful badge registration
* **register-badge** fix: footer is stick to register keychain window

### 2.9.3 (2022-02-15)
* **backend** fix: wrong cronjob log for tracing process deletion
* **health-department** feat: adjust userDataSecret mapping

### 2.9.2 (2022-02-10)
* **webapp** feat: show venue links after scanning QR code
* **webapp** feat: improve checkin logic after user creation

### 2.9.1 (2022-02-08)
* **backend** fix: reorder operator forgot password route middlewares

### 2.9.0 (2022-02-07)
* **backend** feat: create new properties for additional location information
* **backend** feat: create new properties for additional location information on locationTransfer
* **backend** feat: add swagger docs for v4
* **backend** feat: add rate limit for trustList/dsc
* **backend** feat: add more entity limits
* **backend** feat: parse header values in passport strategies
* **backend** feat: remove location transfers legacy encryption compatibility
* **backend** feat: add TLS config to postgres connection
* **backend** feat: automatically delete users that haven't been touched within the grace period
* **backend** feat: instantly cleanup private meeting traces
* **backend** feat: add audit log entries for luca connect searches and messages
* **backend** feat: expose info about current max risk lvl
* **backend** feat: require password for location deletion
* **backend** feat: create CRUD operations for HD notification urls
* **backend** feat: store business name and business address during registration
* **backend** feat: updating a location address will automatically update the addresses of all areas from that location with the same address
* **locations** fix: implement specific error handling if data request is too big to share
* **locations** fix: unify asset imports/exports
* **locations** fix: add notification for reset private key process
* **locations** fix: area section scrollable in Firefox
* **locations** feat: reset private key modal is non-closable
* **locations** feat: handle data sharing transfers for soft-deleted locations
* **locations** feat: improve error handling when inserting wrong key
* **locations** feat: add business information on registration and for existing accounts
* **locations** feat: require password for location deletion
* **locations** feat: implement new design for generate qr code and location/area overview
* **health-department** fix: index person is not listed on top of guest list
* **health-department** feat: implement warning for for large data requests

### 2.8.1 (2022-01-27)
* **backend** feat: set explicit route for zip codes of luca Connect enabled health departments
* **backend** feat: publish job for automatic deletion of inactive users
* **health-department** feat: display optional personal details in luca Connect
* **health-department** feat: adapt schema for multiple userIds sharing history
* **health-department** feat: second confirmation modal when contacting people with luca Connect

### 2.8.0 (2022-01-21)
* **backend** feat: add audit log events
* **backend** feat: allow multiple cookie secrets
* **backend** feat: 2G+ support
* **backend** feat: invalidate session that changes password
* **backend** feat: allow recovering locationGroups after deletion
* **backend** chore: move email templateIds back to code
* **locations** feat: reactivate deleted locations with their areas (archive)
* **locations** feat: unify navigation button and redirect to base location
* **locations** feat: unify and rework tooltips
* **locations** feat: access help center via footer
* **locations** feat: 2G+ support
* **health-department** fix: connection to SORMAS API
* **scanner** feat: add 2G+ entry policy support
* **register-badge** feat: is now an own service
* **register-badge** feat: new design
* chore: Update node base images

### 2.7.1 (2022-01-18)
* **backend** feat: check Organizational Unit Name (OU) from certificates on key rotation

### 2.7.0 (2022-01-07)
* **backend** feat: require password for area deletion
* **backend** feat: require password for email change
* **locations** fix: checkin entry policy is not aligned
* **locations** fix: wrong error message when leaving fields empty on set a new password step
* **locations** feat: require password for area deletion
* **locations** feat: require password for email change
* **locations** feat: remove popup for providing private key on every login
* **locations** feat: remove label if switch is not activated
* **locations** feat: shorten commit hash
* **health-department** feat: shorten commit hash
* **health-department** feat: notify about certificate errors during login
* **scanner** feat: allow v5 badges
* **scanner** feat: shorten commit hash
* **contact-form** feat: shorten commit hash
* **webapp** feat: shorten commit hash


### 2.6.0 (2021-12-28)
* **backend** feat: extend notification configuration
* **backend** feat: store language of operator
* **backend** feat: add phone number blocklist
* **backend** feat: add a job completion log
* **backend** feat: add option to enable a bypass rate limit header
* **backend** feat: remove v3 notification endpoints
* **backend** feat: remove v3 auth endpoints
* **backend** feat: remove deprecated shouldProvideGeoLocation attribute
* **backend** feat: store initial trace count in tracing processes
* **backend** feat: timeout bloomfilter via keepalive mechanism
* **health-department** fix: disable state in search for locations via name or zip code modal
* **health-department** fix: contact person order in confirmed data list changes randomly
* **health-department** feat: handle corrupted checkins gracefully
* **health-department** feat: replace react-data-export library
* **health-department** feat: allow health-departments to search for locations within a ZIP code
* **health-department** feat: delete a process
* **locations** fix: wrong notification when user updates the profile information
* **locations** fix: adjust error notification for resetting key functionality if password did not match
* **locations** fix: validation on check-in questions
* **locations** fix: autocheckout untoggled when updating other location properties
* **locations** fix: impossible to close create location modal when operator had no locations prior to this
* **locations** fix: elements out of bounds in share data view
* **locations** fix: step to select Indoor/Outdoor missing for certain location categories during creation
* **locations** fix: table styles in share data tables
* **locations** feat: new icons in guestlist
* **webapp** fix: app unusable due to immersive fullscreen mode
* **webapp** feat: add confirmation modal when checking into event via self check-in
* **contact-form** feat: allow foreign zip codes and phone numbers in contact form
* **contact-form** feat: improve error handling

### 2.5.7 (2021-12-22)
* **backend** feat: support for luca connect in hd and apps
* **health-department** feat: support for luca connect

### 2.5.6 (2021-12-20)
* **locations** fix: prevent key update if reset was not completed
* **locations** feat: show not transferable entries in share data due to possible operator key reset
* **health-department** feat: show missing entries count in contact list due to possible operator key reset

### 2.5.5 (2021-12-06)
* **backend** chore: improved etag handling
* **backend** fix: prevent too long receiver names in mails
* **backend** feat: reset operator devices after reseting keys
* **health-department** feat: added link to register for luca workshops
* **locations** fix: CWA shows wrong location category

### 2.5.4 (2021-12-05)
* **health-department** feat: check validity of daily keys
* **locations** fix: optional email badge registration
* **locations** feat: possibility to reset the operator keys in case of lost keys
* **locations** feat: pulsating button for operator app connection if the user has not seen the view yet
* **locations** feat: redesign of the empty screen page for no registered devices for a better onboarding flow

### 2.5.3 (2021-11-23)

* **backend** feat: add v4 daily key api

### 2.5.2 (2021-11-23)

* **backend** feat: add external url functionality
* **locations** fix: generate single seed for CWA codes
* **locations** feat: add external url functionality

### 2.5.1 (2021-11-17)

* **backend** fix: improve no cache headers
* **backend** fix: password reset functionality
* **backend** fix: send email change email to the original email
* **backend** feat: add etag headers to key, issuer and notification routes
* **locations** feat: provide selection of 2g/3g entry policies for locations
  and areas
* **scanner** feat: verify the 2g/3g entry policies for checkins

### 2.5.0 (2021-11-17)

* **backend** fix: remove timing differences from auth routes
* **backend** feat: add last seen entry for locations operator private key
  import
* **backend** feat: rework registration and email change routes
* **backend** feat: migrate mailer to Sendinblue
* **backend** feat: notify health-departments via email when data was shared
* **health-department** fix: missing validation on userTransfer
* **health-department** fix: contact infos from edit employees are incorrect
* **health-department** fix: trying to access processes when not logged in
  doesn't route users correctly
* **health-department** fix: remove undefined values from empty exports cells
* **health-department** fix: locale key instead of value is being shown for
  notifications
* **health-department** feat: naming and oder improvements on downloadfiles for
  contact persons
* **health-department** feat: add timestamp of sharedata request and answer to
  guest list, change wording for process expiry
* **locations** fix: guest list "All" shows todays guest count
* **locations** fix: health-department verification badge is not shown for share
  all data
* **locations** fix: additional check-in question input field is not shown right
  away
* **locations** fix: share data step only sends well-formed traces
* **locations** feat: limit additional questions to 10 questions
* **locations** feat: change "Show area profile" to edit icon

### 2.4.2 (2021-11-05)

* **backend** fix: GET locationTransfers should only be available to manager
  role
* **backend** feat: mirror DSCG API
* **backend** feat: add property to define 2g/3g in locations
* **backend** feat: add notifications v4 route accepting hex chunkIds
* **locations** feat: enhance usability for operator app onboarding flow
* **scanner** feat: add basic mobile v4 qr code support

### 2.4.1 (2021-10-28)

* **backend** feat: allow different trace retention policies

### 2.4.0 (2021-10-28)

* **backend** feat: add search auditlog entry
* **backend** feat: adjust and add rate limits
* **backend** feat: add support routes
* **backend** feat: expose average checkin time to public
* **backend** feat: add additional authentication to internal routes
* **backend** feat: invalidate sessions after password chang
* **backend** perf: stream audit log download
* **backend** chore: type sequelize models
* **backend** chore: reduce email expiry to 1 hour
* **locations** fix: emails with mutated vowel
* **locations** fix: add minimum time for average checkin time
* **locations** fix: keep check-out reminder time picker state
* **locations** fix: redirect to created area or location when closing creation
  modal via cross
* **locations** feat: adjust change email address process
* **locations** feat: add back button to share data view
* **locations** feat: prevent triggering multiple data sharing processes in
  share data view
* **health-department** feat: improved help center
* chore: setup lint, prettier and husky for e2e tests
* chore: completely remove ua-parser-js from dependencies

### 2.3.0 (2021-10-18)

* **backend** fix: change GET to POST for api/v3/userTransfers/tan/
* **backend** fix: missing name of location in share data email
* **backend** fix: multiple activation links being valid at the same time
* **backend** fix: metrics path label for errors
* **backend** feat: limit number of available tables
* **backend** feat: limit number of checkin questions
* **backend** feat: implement level 4 notifications
* **backend** feat: keep daily keys for 35 days
* **backend** feat: update ip lists from external sources
* **backend** feat: add new events to audit log system
* **backend** chore: replace winston with pino
* **backend** chore: seperate private meeting endpoints
* **health-department** feat: prevent unsigned HDs from requesting data from
  locations
* **locations** fix: two emails are sent for sharing data request
* **locations** fix: share data link in mail leads to blank page when operator
  is not logged in
* **locations** fix: footer might overlap on small devices
* **locations** fix: wording in check-out reminder
* **locations** fix: QR-Print instructions text and alignment
* **locations** fix: wording in checkin options to be consistent
* **locations** fix: cursor property for change location while hovering
* **locations** fix: German diacritic letters are not counted as upper/lower
  characters
* **locations** feat: new registration/login flow
* **locations** feat: update privacy policy documents
* **locations** feat: update AVV document
* **locations** feat: edit address for existing areas and locations
* **locations** feat: move links from profile page to help center
* chore: replace react-helmet in all frontend services

### 2.2.0 (2021-10-01)

* **backend** feat: add operator device support
* **backend** feat: add operator device feature flag
* **locations** feat: add devices list screen
* **locations** feat: add device reactivation flow
* **locations** feat: add device registration flow

### 2.1.0 (2021-10-01)

* **backend** feat: add signed location transfer support
* **backend** feat: add average checkin time
* **backend** feat: add support route
* **backend** feat: delete redeemed vaccination certificates after 72 hours
* **backend:** fix: extend unique constraint for notification messages
* **backend:** fix: deletion of notification chunks
* **health-department** fix: check public HDSKP when uploading private key
* **health-department** fix: cursor did not change on hovering download options
  in contact person view
* **health-department** fix: update contact list when value changes for location
  tranfers
* **health-department** fix: warning in console concerning button properties
* **health-department** fix: remove default icon tooltip for notification
* **health-department** feat: sign share data requests to locations with HDSKP
* **health-department** feat: improve edit employee fields
* **health-department** feat: change menu button
* **health-department** feat: add 1.63.1 to supported SORMAS versions
* **locations** fix: timestamp in email for share data requests
* **locations** fix: limit traceId in guest list to seven characters
* **locations** fix: QR-Print instructions text and alignment
* **locations** fix: wrong time format in share data modal
* **locations** fix: make whole entry in dropdown menu clickable
* **locations** feat: venue owners can declare an average visit duration for
  their venue
* **locations** feat: new help center
* **locations** feat: user can choose to not load the Google Places API when
  creating locations
* **locations** feat: update privacy policy documents
* **locations** feat: update data processing agreement document
* **locations** feat: improve usablity of the dropdown to switch between
  locations
* **scanner** chore: integrate PO editor
* chore: enable static compression

### 2.0.2 (2021-09-15)

* **backend:** perf: add bloomfilter generation metrics
* **backend:** fix: set timezone via config
* **backend:** fix: remove duplicate term
* chore: update dependencies

### 2.0.1 (2021-09-14)

* **backend:** feat: allow notification of individual users with level 2
  notifications

### 2.0.0 (2021-09-10)

* **backend:** fix: strict JWT schema check for signing app
* **backend:** fix: netmask of ipv6 addresses
* **backend:** feat: add v4 notification endpoint
* **backend:** feat: restrict location transfers endpoint
* **backend:** feat: add health department account based rate limits
* **backend:** feat: add rate limits for notifications and bloomfilter endpoints
* **backend:** feat: add phone number and email to health departments
* **backend:** feat: deny daily key rotation from unsigned health departments
* **backend:** perf: disable default etag generation
* **locations:** fix: redirection to login after successful share data request
* **locations:** fix: typo in authentication footer
* **locations:** fix: time display in completed share data requests
* **locations:** fix: values in csv download for qr codes
* **health-department:** fix: mismatch between list entries and counter
* **health-department:** fix: view update after venue owner completes share data
  request
* **health-department:** fix: misleading location name property in proccess
  details
* **health-department:** feat: improved usability in the timepicker when
  searching for locations
* **health-department:** feat: add note to tracing processes
* **health-department:** feat: possibility to trigger notifications for a
  specific location
* **health-department:** feat: possibility to trigger notifications for specific
  contacts
* **health-department:** feat: possibility to add public contact informations
* **health-department:** feat: possibility for admins to download the audit
  logfile
* **webapp:** fix: user transfer creation before approval

### 1.9.2 (2021-09-01)

* **locations:** fix: domain specific email validation
* **scanner:** feat: deny check ins of v3 badges

### 1.9.1 (2021-08-25)

* **health-department:** fix: handling of unregistered badges in tracing
  processes

### 1.9.0 (2021-08-18)

* **backend:** fix: openAPI JSON
* **backend:** feat: send email to operator after approved location transfer
* **backend:** feat: add note to tracing proccess
* **backend:** feat: add rate limit for change operator email
* **backend:** feat: hash IP addresses in rate-limit redis
* **locations:** fix: area list not scrollable
* **locations:** fix: native tooltips for icons
* **locations:** fix: popconfirm texts for checkout guests
* **locations:** feat: notify that luca does not support IE
* **locations:** feat: add timestamp of answered data request to data requests
  view
* **locations:** feat: add tooltip for support code in help center
* **health-department:** fix: button alignment in track infection modal
* **health-department:** fix: native tooltips for icons
* **health-department:** feat: notify that luca does not support IE
* **health-department:** feat: improved wording in process details table
* **health-department:** feat: add help center
* **health-department:** feat: update design of private key modal
* **contact-form:** fix: possibility to enter a streetname with only numbers
* **contact-form:** fix: native tooltips for icons
* **scanner:** fix: native tooltips for icons
* **scanner:** fix: use correct camera depending on the used device
* **webapp:** fix: possibility to enter a streetname with only numbers
* **webapp:** fix: native tooltips for icons
* **webapp:** feat: extend check-in history to 28 days
* **webapp:** feat: remove Checkbox for privacy policy

### 1.8.1 (2021-08-13)

* **health-department:** fix: dependency resolution jszip

### 1.8.0 (2021-08-12)

* **backend:** fix: limit number of locations in location transfers
* **backend:** feat: check integrity of device types in tracing processes
* **backend:** feat: take badge attestation key offline
* **health-department:** fix: additional data under certain conditions not
  displayed in the contact person overview
* **health-department:** fix: clear storage after logout
* **health-department:** feat: encrypt share data request only for the
  requesting health department
* **health-department:** feat: download contact persons in octowareTN format
* **location:** fix: improve error handling during share data process
* **location:** fix: improve error handling during private key import
* **location:** fix: broken button arrangement when deleting the account
* **location:** feat: check private key after initial key download for new
  operators
* **scanner:** feat: add warning that v3 badges are to be replaced
* **scanner:** feat: improve scanning qr codes on badges
* **webapp:** fix: error message in the background shown after deleting the
  account
* **webapp:** feat: add screen to accept new AGBs
* chore: improve e2e tests

### 1.7.0 (2021-08-05)

* **backend:** feat: instrument with metrics
* **health-department:** feat: update route access restrictions for employees
* **health-department:** feat: unified buttons and updated designs
* **locations:** fix: qr code labels and names
* **locations:** feat: inform venue owners about unusally long request times
  from health departments before sharing data
* **locations:** feat: support special characters in location creation
* **contact-form:** feat: updated string validation
* **webapp:** fix: checkin timer starting time
* **webapp:** fix: timer resets after refresh
* **scanner:** fix: improve camera resolution to scan badges faster

### 1.6.2 (2021-07-29)

* **backend:** ref: split checkin route for scanner and contact-form

### 1.6.1 (2021-07-28)

* **backend:** feat: lower redis usage by adding etag caching for large values

### 1.6.0 (2021-07-27)

* **backend:** fix: schema validation mismatch
* **backend:** feat: added dev setup for signing tool
* **backend:** feat: added route to delete test redeems
* **backend:** feat: global rate limit configuraable via feature flag
* **backend:** feat: send notifications on authentication change
* **backend:** feat: increase user TAN validity to 48h
* **backend:** feat: add authenticated checkin route for operators
* **backend:** feat: add version endpoint for signing tool
* **backend:** chore: setup typescript
* **health-department:** fix: possible duplicate index case due to name
  comparison
* **health-department:** fix: time format in location search modal
* **health-department:** fix: input validation and error handling based on
  backend validations
* **health-department:** feat: enabled SORMAS api for latest SORMAS versions
* **health-department:** feat: added version, commit hash and GitLab link
* **health-department:** feat: added download for signing tool
* **health-department:** feat: updated designs for the contact person view
* **contact-form:** feat: added version, commit hash and GitLab link
* **contact-form:** feat: updated data privacy link
* **locations:** fix: removed whitespace overflow when downloading QR codes
* **locations:** fix: sorting for share data requests
* **locations:** fix: input validation and error handling based on backend
  validations
* **locations:** fix: improved naming in guestlist
* **locations:** fix: improved checkin and checkout visualization
* **locations:** fix: store emails case sensitive
* **locations:** feat: added help button to header with specific contact
  information
* **locations:** feat: added version, commit hash and GitLab link
* **locations:** feat: improved QR code generation by using web workers
* **locations:** feat: unified buttons and updated designs
* **locations:** feat: updated designs of the location card components
* **locations:** feat: updated data privacy link for badge registration
* **locations:** chore: enabled sonar
* **webapp:** feat: added version, commit hash and GitLab link
* **webapp:** feat: updated data privacy link
* **scanner:** feat: added version, commit hash and GitLab link

### 1.5.4 (2021-07-21)

* **scanner:** feat: deny checkins from unregistered badges via bloomfilter

### 1.5.3 (2021-07-16)

* **health-department:** feat: add utf-8 BOM for better excel compatibility

### 1.5.2 (2021-07-14)

* **backend:** fix: mark expireAt as optional for test redeem request

### 1.5.1 (2021-07-13)

* **backend:** ref: refactor input validations
* **health-department:** fix: SORMAS import file

### 1.5.0 (2021-07-06)

* **backend:** feat: replace node-mailjet with axios
* **backend:** feat: move some error handling to cryto package
* **backend:** feat: add GTX SMS provider
* **backend:** feat: add route to provide download url for signing tool
* **backend:** feat: return a different status code for expired SMS challenges
* **backend:** feat: moved traceId calculation to backend
* **backend:** feat: completely delete tracing processes after 28 days
* **backend:** ref: move json parse middleware into validateSchema middleware
* **backend:** chore: colorize and improve dev logging output
* **backend:** chore: publish traceIds for notifications when data was shared
* **health-department:** fix: add missing mac check
* **health-department:** chore: add limit for private key file size
* **health-department:** feat: visualise signed public keys
* **health-department:** feat: set main font and remove duplicate definitions
* **locations:** fix: "forgot password" displaying incorrect error message for
  not activated users
* **locations:** fix: typos in registration email step
* **locations:** fix: private key modal issue
* **locations:** chore: add limit for private key file size
* **locations:** feat: redesign header
* **locations:** feat: checkin options for guests are directly accessible via
  location view
* **locations:** feat: provide link to checkin options via qr code
* **webapp:** chore: fix typos
* **webapp:** feat: add gitlab link
* **webapp:** feat: validate that private meeting is not spoofing a location
* **webapp:** feat: add a consent modal for data sharing with the health
  department
- security: update container base images and install security patches
- feat: add husky for git hooks

### 1.4.0 (2021-06-29)

* **backend:** feat: add test provider key route
* **backend:** feat: add isTrusted to Operators
* **backend:** chore: remove LocationTransferGroups table
* **health-department:** fix: added missing mac check
* **health-department:** fix: session not cleared correctly after automatic
  checkout due to inactivity
* **health-department:** fix: order of locations changed after contacting venues
* **health-department:** feat: add hover effect to process list for better
  usability
* **health-department:** feat: new designs for location search
* **health-department:** feat: locations can be search by zip code as additional
  parameter to the location name
* **health-department:** feat: new designs for profile view
* **health-department:** feat: select profile view got moved from tab to header
* **health-department:** feat: improved locales
* **locations:** fix: session not cleared correctly after automatic checkout due
  to inactivity
* **locations:** fix: incorrect download file names when downloading qr codes
* **locations:** fix: clear session storage after session timed out
* **locations:** fix: forgot password view displayed a wrong error message if
  the user is not activated
* **locations:** feat: updated DPA document
* **locations:** feat: updated terms and conditions links for venues
* **locations:** feat: trusted venues can register badges without phone
  validation

### 1.3.0 (2021-06-20)

* **backend:** feat: improve IP blocks
* **backend:** feat: improve email storage in postgres
* **backend:** feat: updated API documentation
* **backend:** feat: removed unused LocationTransferGroups
* **backend:** feat: add expireAt functionality to test redeems
* **contact-form:** fix: handle input starting with mutated vowels
* **contact-form:** feat: improved input validation
* **health-department:** fix: use secure-json-parse to prevent prototype
  pollution
* **health-department:** fix: replace react-csv with papaparse and improve csv
  output encoding
* **health-department:** fix: typo in error notification
* **health-department:** fix: status names in process table and filter were not
  identical
* **health-department:** feat: validate object schema directly after decryption
  and apply sanitization
* **health-department:** feat: new designs for process details
* **locations:** fix: input fields for manual input are disabled under certain
  conditions
* **locations:** fix: password not persisted after back action in registration
* **locations:** fix: password criterias were not updated after back action in
  registration
* **locations:** fix: password criterias were not updated after clearing
  password in registration
* **locations:** fix: button to checkout all guests was clickable even though no
  active guest where at the location
* **locations:** fix: redirection to correct location/area after editing
  locations or areas
* **locations:** feat: phone number validation for contact person of a location
  and area
* **locations:** feat: new designs for share data view
* **webapp:** fix: error message when visiting a scanner link with an
  unregistered webapp
* **development:** feature: improve project cloning on windows machine

### 1.2.3 (2021-06-19)

* **health-department:** fix: disable SORMAS CSV export

### 1.2.2 (2021-06-18)

* **health-department:** fix: fix trying to trim a non-string
* **health-department:** chore: upgrade xlsx package
* **backend:** feat: add expireAt functionality to test redeems

### 1.2.1 (2021-06-17)

* **backend:** feat: add v4 signed keys upload

### 1.2.0 (2021-05-31)

* **backend:** feature: split generic locations route into specific routes for
  apps and healthdepartment
* **backend:** fix: only include app traceIds in notification endpoint
* **backend:** security: added jsonbigint resolution
* **backend:** security: added redis resolution
* **health-department:** fix: floating dropdown menu for filter
* **health-department:** security: added hosted-git-info resolution
* **health-department:** feature: escape download filenames
* **health-department:** feature: update the designs of the modal to track an
  infection
* **health-department:** feature: escape all data provided by operators and
  users
* **health-department:** feature: added the possibility for an admin to change
  name and phone of other users
* **health-department:** feature: added the possibility to assign processes to
  users
* **health-department:** feature: added the possibility for an admin to generate
  a new password for other users
* **health-department:** feature: update the designs of the modal to add new
  users
* **health-department:** feature: order processes by creation time and process
  name
* **health-department:** feature: show additional data infos in the contact
  person view
* **health-department:** feature: filter processes by assignee
* **locations:** fix: qr code download on edge
* **locations:** fix: private key download on iPad x Safari
* **locations:** security: added hosted-git-info resolution
* **locations:** feature: link FAQs in menu
* **locations:** feature: improve error messages in login and registration
* **locations:** feature: added the possibility to delete an account
* **locations:** feature: added a "Whats new modal" to inform about the AVV's
* **webapp:** security: added hosted-git-info resolution
* **webapp:** feature: support cwa qr codes
* **webapp:** feature: prefer native apps modal
* **webapp:** feature: improved accessibility
* **scanner:** security: added hosted-git-info resolution
* **contact-form:** security: added hosted-git-info resolution
* **development:** feature: added script to run yarn in all services at once
* **development:** feature: speed up rebuilding images by using common yarn
  cache

### 1.1.16 (2021-06-02)

* **backend:** feat: expire sms challenges
* **health-department:** feat: include additional check in data directly in
  contact person view and not only in the download files
* **health-department:** feat: include address information directly in contact
  person view and not only in the download files
* **health-department:** fix: dependency tree
* **scanner:** fix: update check ins counter after scanning badges
* **scanner:** fix: refocus after outbounds clicks in hardware scanner
* **webapp:** feat: support international phone numbers
* **webapp:** feat: handle unsupported deeplinks

### 1.1.15 (2021-05-28)

* **locations:** chore: improved location locales
* **health-department:** chore: update tests for sanitization

### 1.1.14 (2021-05-27)

* **health-department:** chore: improved health department locales
* **health-department:** fix: add more error handling in csv generation

### 1.1.13 (2021-05-26)

* **health-department:** fix: whitlisted special characters for csv

### 1.1.12 (2021-05-26)

* **health-department:** fix: csv sanitization

### 1.1.11 (2021-05-25)

* **locations:** feat: checkout users from tables
* **locations:** feat: checkout single users from locations
* **locations:** feat: generate qr codes compatible with CWA
* **locations:** perf: improve fetching of current guest count
* **scanner:** perf: improve fetching of current and total guest count

### 1.1.10 (2021-05-21)

* **backend** chore: improve text for fixed line voice message

### 1.1.9 (2021-05-21)

* **backend** chore: add rate limit
* **backend** chore: add additional status code to delete user route
* **locations:** chore: add support for legacy private operator keys

### 1.1.8 (2021-05-20)

* **locations:** feat: show current table allocation
* **locations:** feat: updated DPA and privacy policies files for download
* **locations:** feat: added TOMs file for download in profile
* **locations:** feat: added explaination for external printing service for qr
  codes
* **webapp:** feat: delete user account data

### 1.1.7 (2021-05-20)

* **backend:** feat: implement allow list for IP addresses
* **backend:** chore: increase rate limit for SMS tan requests

### 1.1.6 (2021-05-18)

* **health-department:** fix: filter contact persons by overlapping time
* **health-department:** fix: sorting of locations in history view by name
* **scanner:** fix: scan field not cleared after scan
* **locations:** feat: download qr code content as csv file

### 1.1.5 (2021-05-14)

* **backend:** fix: deny updating data on static badges via create route

### 1.1.4 (2021-05-11)

* **health-department:** fix: use correct base32 decoding

### 1.1.3 (2021-05-10)

* **backend:** feat: add route to redeem tests
* **backend:** feat: allow to dynamically set dummy rates
* **backend:** perf: further improvement of the notifications endpoint
* **backend:** fix: pin json-bigint version to 1.0.0

### 1.1.2 (2021-05-07)

* **backend:** perf: add multiple indizes to improve query performance
* **backend:** fix: pin redis version to 3.1.2
* **frontend:** fix: return promises in validation handler
* **frontend:** fix: pin hosted-git-info to 4.0.2

### 1.1.1 (2021-05-03)

* **backend:** fix: ignore expired traces in notifications route

### 1.1.0 (2021-05-03)

* **locations:** feat: improved serial code input for badge registration
* **locations:** feat: the download of the private key needs to be confirmed
* **locations:** feat: new checkbox for data processing agreement (DPA) in
  registration
* **locations:** feat: operators can provide indoor or outdoor information for a
  location
* **locations:** feat: share all open data requests at once
* **locations:** fix: limit checkout radius to a maximum of 5000
* **locations:** fix: add loading spinner for opening guest list modal
* **locations:** fix: notifications for errors during registration
* **health-department:** feat: download a selection of contacts
* **health-department:** feat: filter processes by type and status and updated
  designs for filter section
* **health-department:** feat: visualise expiration time (28 days) to history
  view to indicate the remaining time of a process
* **health-department:** feat: improved serial code input to create a process
  via tan
* **health-department:** feat: increase search limit for locations from 10 to
  100
* **health-department:** feat: enable admins to change roles of employees
* **health-department:** feat: included indoor/outdoor information of locations
  to history
* **health-department:** feat: add creation date to process table
* **health-department:** fix: resolved console error in contact person list
* **health-department:** fix: improve flow of requesting a location manually
* **webapp:** fix: styling for reset device text
* **webapp:** feat: tests for the onboarding flow
* **contact-form:** feat: mark additional checkin questions to be generated by
  the venue owner
* **contact-form:** feat: additional checkin questions are not mandatory anymore
* **contact-form:** fix: fixed checkin with additional checkin data
* **scanner:** feat: improved error and qr data handling
* **scanner:** feat: additional checkin questions are not mandatory anymore
* **backend:** feat: cleanup deleted users after 28 days
* **backend:** feat: add node inspect for debugging
* **backend:** fix: use safer entropy
* **backend:** fix: add missing parameter validations
* **backend:** fix: error handling for traces
* **backend:** fix: increase maximum requestable period from 6 to 24
* chore: add Jenkinsfile to repository
* chore: speedup build time for local environment

### 1.0.5 (2021-04-29)

* **locations:** fix: handle decryption errors

### 1.0.4 (2021-04-27)

* **backend:** fix: allow larger body size

### 1.0.3 (2021-04-27)

* **backend:** chore: add separate rate limit for fixed line phone numbers

### 1.0.2 (2021-04-23)

* **backend:** fix: pin netmask package to patched version
* **backend:** fix: increase maximum requestable period for traces
* **backend:** feat: add dummy traces for the notifications endpoint

### 1.0.1 (2021-04-16)

* **locations:** fix: use correct base32 decoding
* **scanner:** fix: add v3b signature format support
* **backend:** fix: fix redis initialization
* **backend:** feat: add rate limit by phone number

### 1.0.0 (2021-04-14)

- initial public release
