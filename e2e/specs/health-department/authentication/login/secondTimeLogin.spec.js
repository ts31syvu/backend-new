import { loginHealthDepartment } from '../../helpers/api/auth';
import { verifyLoggedIn } from '../../helpers/ui/login';
import { addHealthDepartmentPrivateKeyFile } from '../../helpers/ui/handlePrivateKeyFile';

describe('Authentication', () => {
  describe('Health Department / Authentication / Login', () => {
    describe('when a user login for the second time', () => {
      it('ask to upload private key and redirect to tracking page', () => {
        loginHealthDepartment();
        addHealthDepartmentPrivateKeyFile();
        verifyLoggedIn();
        cy.logoutHD();
      });
    });
  });
});
