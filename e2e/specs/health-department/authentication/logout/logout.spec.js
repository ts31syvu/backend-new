import { loginHealthDepartment } from '../../helpers/api/auth';
import { addHealthDepartmentPrivateKeyFile } from '../../helpers/ui/handlePrivateKeyFile';

describe('Health Department / Authentication / Logout', () => {
  beforeEach(() => {
    loginHealthDepartment();
    addHealthDepartmentPrivateKeyFile();
  });
  describe('when an operator logs out', () => {
    it('logs the user out successfully', () => {
      cy.logoutHD().then(response => {
        expect(response.status).to.eq(204);
      });
    });
  });
});
