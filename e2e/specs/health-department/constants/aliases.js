export const KEY_ROTATION = 'keyRotation';
export const TRACING_PROCESS = 'tracingProcess';
export const PROCESS_UUID = 'processUuid';
export const PASSWORD = 'password';
export const TEST_EMPLOYEE_UUID = 'testEmployeeUuid';
