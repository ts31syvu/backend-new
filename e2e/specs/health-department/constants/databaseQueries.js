export const RESET_HD_KEYS_QUERY = `UPDATE "HealthDepartments" SET "publicHDEKP" = NULL, "publicHDSKP" = NULL;
DELETE FROM "EncryptedDailyPrivateKeys";
DELETE FROM "DailyPublicKeys";
DELETE FROM "EncryptedBadgePrivateKeys";
DELETE FROM "BadgePublicKeys";
`;
