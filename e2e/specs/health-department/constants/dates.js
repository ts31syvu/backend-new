import moment from 'moment';

export const YESTERDAY = moment().subtract(1, 'days').format('DD.MM.YYYY');
export const TODAY = moment().format('DD.MM.YYYY');
