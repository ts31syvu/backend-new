// FILE PATH
export const HEALTH_DEPARTMENT_PRIVATE_KEY_PATH =
  './downloads/HealthDepartmentKeyFile.luca';
export const HEALTH_DEPARTMENT_WRONG_PRIVATE_KEY_PATH =
  './assets/HealthDepartmentWrongKeyFile.luca';
export const HEALTH_DEPARTMENT_WRONG_PDF_FILE_PATH = './assets/dummy.pdf';
export const HEALTH_DEPARTMENT_LARGE_SIZE_FILE_PRIVATE_KEY_PATH =
  './assets/HealthDepartmentLargeFile.luca';
// FILE NAME
export const HD_CORRECT_PRIVATE_KEY_FILE_NAME = 'HealthDepartmentKeyFile.luca';
export const HD_WRONG_PRIVATE_KEY_FILE_NAME =
  'HealthDepartmentWrongKeyFile.luca';
export const HD_LARGE_PRIVATE_KEY_FILE_NAME = 'HealthDepartmentLargeFile.luca';
