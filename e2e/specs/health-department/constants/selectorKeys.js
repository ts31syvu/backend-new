// General
export const SUBMIT_BUTTON = 'button[type=submit]';
export const BUTTON = 'button[type=button]';
export const NEXT_BUTTON = 'next';
export const FINISH_BUTTON = 'finish';
export const TRY_AGAIN_BUTTON = 'tryAgain';

// Login
export const DOWNLOAD_PRIVATE_KEY_BUTTON = 'downloadPrivateKey';
export const INPUT_TYPE_FILE = 'input[type="file"]';

// Ant
export const ANT_NOTIFICATION_NOTICE = '.ant-notification-notice';
export const ANT_NOTIFICATION = '.ant-notification';
export const ANT_MODAL = '.ant-modal';
export const ANT_MODAL_CONTENT = '.ant-modal-content';
export const ANT_POPCONFIRM = '.ant-popconfirm';
export const ANT_POPOVER_BUTTONS = '.ant-popover-buttons button';
export const ANT_POPOVER_PRIMARY_BUTTON =
  '.ant-popover-buttons > .ant-btn-primary';
export const ANT_POPOVER_INNER_CONTENT = '.ant-popover-inner-content';
export const ANT_EXPLAIN_ERROR = '.ant-form-item-explain-error';
export const ANT_TOOLTIP_OPEN = '.ant-tooltip-open';

// Header
export const HEADER = 'header';
export const VERIFICATION_ICON = 'verificationIcon';
export const PROFILE_ICON = 'profileIcon';
export const HELPCENTER_ICON = 'helpCenterIcon';
export const LINK_MENU_ICON = 'linkMenuIcon';
export const HEADER_DROPDOWN_MENU = 'headerDropdownMenu';
export const HEADER_LICENSE_LINK = 'headerLicenseLink';
export const HEADER_PRIVACY_POLICY_LINK = 'headerPrivacyPolicyLink';
export const LOGOUT_BUTTON = 'logoutButton';

// Profile Page
export const PROFILE_BACK_ICON = 'profileBackIcon';
export const PROFILE_INFORMATION_SECTION = 'profileInformationSection';
export const PROFILE_CHANGE_PASSWORD_SECTION = 'profileChangePasswordSection';

// Help Center Page
export const HELP_CENTER_BACK_ICON = 'helpCenterBackIcon';
export const HELP_CENTER_LINKS_SECTION = 'helpCenterLinksSection';
export const HELP_CENTER_CONTACT_SECTION = 'helpCenterContactSection';

// Actions
export const ADD_EMPLOYEE = 'addEmployee';
export const GENERATE_PASSWORD = 'generatedPassword';

// User
export const FIRST_NAME = '#firstName';
export const LAST_NAME = '#lastName';
export const PHONE = '#phone';
export const EMAIL = '#email';

// Filter
export const PROCESS_STATE_FILTER = 'Processes';
export const CLOSED_FILTER = 'Closed';

// Group
export const GROUP_PREFIX = 'group_';
export const SEARCH_GROUP_BUTTON = 'searchGroup';
export const GROUP_NAME_INPUT_FIELD = 'groupNameInput';
export const START_GROUP_SEARCH_BUTTON = 'startGroupSearch';
export const REQUEST_GROUP_DATA_BUTTON = 'requestGroupData';
export const PROCESS_ENTRY = 'processEntry';
export const COMPLETE_BUTTON = 'complete';
export const EMPTY_PROCESSES = 'emptyProcesses';

// Data Requests
export const COMPLETE_DATA_TRANSFER = 'completeDataTransfer';
export const CONTACT_PERSONS_TABLE = '#contactPersonsTable';
export const CONFIRMED_LOCATION_PREFIX = 'confirmedLocation_';
export const CONTACT_LOCATION_PREFIX = 'contactLocation_';
export const BACK_TO_SEARCH_BUTTON = 'backToSearchButton';

// Employees
export const EMPLOYEE_PREFIX = 'employeeEntry-';
