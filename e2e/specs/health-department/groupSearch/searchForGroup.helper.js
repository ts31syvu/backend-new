import { GROUP_PREFIX } from '../constants/selectorKeys';
import { getByCy } from '../utils/selectors';

export const verifyMatchedResults = groupsToCheck => {
  groupsToCheck.forEach(group => {
    getByCy(GROUP_PREFIX + group.groupName)
      .last()
      .scrollIntoView()
      .children()
      .then($group => {
        expect($group.text()).contains(group.groupName);
        expect($group.text()).contains(
          `${group.streetName} ${group.streetNr}, ${group.zipCode} ${group.city}`
        );
      });
  });
};
