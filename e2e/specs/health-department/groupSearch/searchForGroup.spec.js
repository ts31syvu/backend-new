import { loginHealthDepartment } from '../helpers/api/auth';
import { getGroupPayload } from '../../locations/helpers/api/groups';
import { signHealthDepartment } from '../helpers/api/signHealthDepartment';
import { addHealthDepartmentPrivateKeyFile } from '../helpers/ui/handlePrivateKeyFile';
import {
  shouldBeDisabled,
  shouldBeVisible,
  shouldNotBeDisabled,
  shouldNotExist,
} from '../utils/assertions';
import { getByCy, getDOMElement } from '../utils/selectors';
import { type, visit } from '../utils/commands';
import { createTestGroups } from '../../locations/network/api.helper';
import { createTestOperator } from '../../locations/utils/setup';
import {
  ANT_EXPLAIN_ERROR,
  ANT_MODAL_CONTENT,
  BACK_TO_SEARCH_BUTTON,
  GROUP_NAME_INPUT_FIELD,
  GROUP_PREFIX,
  SEARCH_GROUP_BUTTON,
  START_GROUP_SEARCH_BUTTON,
} from '../constants/selectorKeys';
import { DEFAULT_COOKIE } from '../../locations/utils/setup.helper';
import { HEALTH_DEPARTMENT_APP_ROUTE } from '../constants/routes';
import { ZIP_CODE_INPUT_FIELD } from '../../locations/constants/selectorKeys';
import { verifyMatchedResults } from './searchForGroup.helper';
import { INVALID_SEARCH_TERM, INVALID_ZIP_CODES } from '../constants/inputs';

const FIRST_GROUP_NAME = 'HD-search first location';
const SECOND_GROUP_NAME = 'HD-search second location';
const THIRD_GROUP_NAME = 'not matching group';
const SEARCH_TERM = 'HD-';
const ZIP_CODE_MATCHED = '11111';
const ZIP_CODE_MISMATCHED = '99999';

let testGroups = [];
let matchedGroupByTerm = [];
let matchedGroupsByZip = [];

describe('Health Department / Group search / Search for a group by name and zip', () => {
  before(() => {
    // create groups with different names and zip codes
    createTestOperator();
    createTestGroups([
      getGroupPayload({ name: FIRST_GROUP_NAME, zipCode: ZIP_CODE_MATCHED }),
      getGroupPayload({
        name: SECOND_GROUP_NAME,
        zipCode: ZIP_CODE_MISMATCHED,
      }),
      getGroupPayload({ name: THIRD_GROUP_NAME, zipCode: ZIP_CODE_MATCHED }),
    ]).then(groups => {
      testGroups = groups;
      matchedGroupByTerm = testGroups.filter(
        group => group.groupName !== THIRD_GROUP_NAME
      );
      matchedGroupsByZip = testGroups.filter(
        group => group.zipCode !== ZIP_CODE_MISMATCHED
      );
    });
    // login HD
    cy.logoutLocations();
    loginHealthDepartment();
    signHealthDepartment();
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(HEALTH_DEPARTMENT_APP_ROUTE);
    addHealthDepartmentPrivateKeyFile();
    // get the search modal
    shouldBeVisible(getByCy(SEARCH_GROUP_BUTTON)).click();
    shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
  });

  it('displays groups matching search term', () => {
    // enable search with search term disabled by default
    shouldBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    // throw validation error
    type(getByCy(GROUP_NAME_INPUT_FIELD).clear(), INVALID_SEARCH_TERM);
    shouldBeVisible(getDOMElement(ANT_EXPLAIN_ERROR));
    shouldBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    // can submit valid input
    type(getByCy(GROUP_NAME_INPUT_FIELD).clear(), SEARCH_TERM);
    shouldNotBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    // search group by term
    getByCy(START_GROUP_SEARCH_BUTTON).click();
    // verify 2 matching groups are found and one is not
    verifyMatchedResults(matchedGroupByTerm);
    shouldNotExist(getByCy(GROUP_PREFIX + THIRD_GROUP_NAME));
  });

  it('displays groups matching zip code when searched by zip', () => {
    // search groups by zip disabled by default
    shouldBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    // throw validation error
    INVALID_ZIP_CODES.map(invalidInput => {
      type(getDOMElement(ZIP_CODE_INPUT_FIELD).clear(), invalidInput);
      shouldBeVisible(getDOMElement(ANT_EXPLAIN_ERROR));
      shouldBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    });
    // can submit valid input
    type(getDOMElement(ZIP_CODE_INPUT_FIELD).clear(), ZIP_CODE_MATCHED);
    shouldNotBeDisabled(getByCy(START_GROUP_SEARCH_BUTTON));
    getByCy(START_GROUP_SEARCH_BUTTON).click();
    // verify groups with matching zip are found
    verifyMatchedResults(matchedGroupsByZip);
    // verify groups with different zip are not found
    shouldNotExist(getByCy(GROUP_PREFIX + SECOND_GROUP_NAME));
  });

  it('displays groups matching all search criteria when searched by name and zip', () => {
    // search groups by name and goes back
    type(getByCy(GROUP_NAME_INPUT_FIELD), SEARCH_TERM);
    getByCy(START_GROUP_SEARCH_BUTTON).click();
    getByCy(GROUP_PREFIX + FIRST_GROUP_NAME)
      .first()
      .click();
    getByCy(BACK_TO_SEARCH_BUTTON).click();
    // search group by name and zip
    type(getByCy(GROUP_NAME_INPUT_FIELD).clear(), SEARCH_TERM);
    type(getDOMElement(ZIP_CODE_INPUT_FIELD), ZIP_CODE_MATCHED);
    getByCy(START_GROUP_SEARCH_BUTTON).click();
    // verify that group matching name and zip is found
    getByCy(GROUP_PREFIX + FIRST_GROUP_NAME)
      .last()
      .scrollIntoView()
      .children()
      .then($group => {
        expect($group.text()).contains(FIRST_GROUP_NAME);
        expect($group.text()).contains(
          `${testGroups[0].streetName} ${testGroups[0].streetNr}, ${ZIP_CODE_MATCHED} ${testGroups[0].city}`
        );
      });
    // verify groups with different zip or name are not found
    shouldNotExist(getByCy(GROUP_PREFIX + SECOND_GROUP_NAME));
    shouldNotExist(getByCy(GROUP_PREFIX + THIRD_GROUP_NAME));
  });
});
