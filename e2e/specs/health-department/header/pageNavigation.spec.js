import {
  shouldHaveAttribute,
  urlShouldInclude,
} from '../../locations/utils/assertions';
import {
  HEALTH_DEPARTMENT_HELPCENTER_ROUTE,
  HEALTH_DEPARTMENT_PROFILE_ROUTE,
} from '../constants/routes';
import {
  ANT_TOOLTIP_OPEN,
  HEADER,
  HEADER_DROPDOWN_MENU,
  HEADER_LICENSE_LINK,
  HEADER_PRIVACY_POLICY_LINK,
  HELPCENTER_ICON,
  HELP_CENTER_BACK_ICON,
  HELP_CENTER_CONTACT_SECTION,
  HELP_CENTER_LINKS_SECTION,
  LINK_MENU_ICON,
  PROFILE_BACK_ICON,
  PROFILE_CHANGE_PASSWORD_SECTION,
  PROFILE_ICON,
  PROFILE_INFORMATION_SECTION,
  VERIFICATION_ICON,
} from '../constants/selectorKeys';
import { removeHDPrivateKeyFile } from '../../workflow/helpers/functions';
import { loginHealthDepartment } from '../helpers/api/auth';
import { signHealthDepartment } from '../helpers/api/signHealthDepartment';
import { addHealthDepartmentPrivateKeyFile } from '../helpers/ui/handlePrivateKeyFile';
import { shouldBeVisible } from '../utils/assertions';
import { getByCy, getDOMElement } from '../utils/selectors';
import { verifyLinkOpensInNewTab } from '../helpers/ui/validations';
import { DEFAULT_COOKIE } from '../utils/setup.helper';

describe('Header/Page Navigation', () => {
  before(() => {
    removeHDPrivateKeyFile();
    loginHealthDepartment();
    addHealthDepartmentPrivateKeyFile();
    signHealthDepartment();
    cy.reload();
    addHealthDepartmentPrivateKeyFile();
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    shouldBeVisible(getByCy(HEADER));
  });

  it('should display information tooltip when hovering over verification sign', () => {
    getByCy(VERIFICATION_ICON).trigger('mouseover');
    shouldBeVisible(getDOMElement(ANT_TOOLTIP_OPEN));
    getByCy(VERIFICATION_ICON).trigger('mouseout');
  });

  it('should open profile page', () => {
    getByCy(PROFILE_ICON).click();
    urlShouldInclude(HEALTH_DEPARTMENT_PROFILE_ROUTE);
    shouldBeVisible(getByCy(HEADER));
    shouldBeVisible(getByCy(PROFILE_BACK_ICON));
    shouldBeVisible(getByCy(PROFILE_INFORMATION_SECTION));
    shouldBeVisible(getByCy(PROFILE_CHANGE_PASSWORD_SECTION));
  });

  it('should open helpcenter page', () => {
    getByCy(HELPCENTER_ICON).click();
    urlShouldInclude(HEALTH_DEPARTMENT_HELPCENTER_ROUTE);
    shouldBeVisible(getByCy(HEADER));
    shouldBeVisible(getByCy(HELP_CENTER_BACK_ICON));
    shouldBeVisible(getByCy(HELP_CENTER_LINKS_SECTION));
    shouldBeVisible(getByCy(HELP_CENTER_CONTACT_SECTION));
  });

  it('should open lisence link from dropdown menu', () => {
    // dropdown menu should display dropdown with choice between license and privacy policy
    getByCy(LINK_MENU_ICON).trigger('mouseover');
    shouldBeVisible(getByCy(HEADER_DROPDOWN_MENU));
    shouldHaveAttribute(getByCy(HEADER_LICENSE_LINK), 'target', '_blank');
    // open lisence link
    getByCy(HEADER_LICENSE_LINK).click();
    verifyLinkOpensInNewTab(getByCy(HEADER_LICENSE_LINK));
  });

  it('should open privacy policy link from dropdown menu', () => {
    getByCy(LINK_MENU_ICON).trigger('mouseover');
    shouldBeVisible(getByCy(HEADER_DROPDOWN_MENU));
    shouldHaveAttribute(
      getByCy(HEADER_PRIVACY_POLICY_LINK),
      'target',
      '_blank'
    );
    // open privacy policy link
    getByCy(HEADER_PRIVACY_POLICY_LINK).click();
    verifyLinkOpensInNewTab(getByCy(HEADER_PRIVACY_POLICY_LINK));
  });

  it('should logout user', () => {
    cy.logoutHD().then(response => {
      expect(response.status).to.eq(204);
    });
  });
});
