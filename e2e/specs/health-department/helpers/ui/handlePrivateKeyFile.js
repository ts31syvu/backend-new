import {
  HD_CORRECT_PRIVATE_KEY_FILE_NAME,
  HD_LARGE_PRIVATE_KEY_FILE_NAME,
  HD_WRONG_PRIVATE_KEY_FILE_NAME,
  HEALTH_DEPARTMENT_LARGE_SIZE_FILE_PRIVATE_KEY_PATH,
  HEALTH_DEPARTMENT_PRIVATE_KEY_PATH,
  HEALTH_DEPARTMENT_WRONG_PDF_FILE_PATH,
  HEALTH_DEPARTMENT_WRONG_PRIVATE_KEY_PATH,
} from '../../constants/keyFiles';
import {
  ANT_MODAL,
  ANT_NOTIFICATION_NOTICE,
  DOWNLOAD_PRIVATE_KEY_BUTTON,
  NEXT_BUTTON,
  FINISH_BUTTON,
  TRY_AGAIN_BUTTON,
  INPUT_TYPE_FILE,
} from '../../constants/selectorKeys';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { shouldBeVisible, shouldNotExist } from '../../utils/assertions';
import { readFile, task, log } from '../../utils/commands';

export const readHDPrivateKeyFile = (filepath, filename) => {
  readFile(filepath).then(fileContent => {
    getDOMElement(INPUT_TYPE_FILE).attachFile({
      fileContent,
      fileName: filename,
      mimeType: 'text/plain',
    });
  });
};

export const downloadHealthDepartmentPrivateKey = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON, { timeout: 8000 }).click();
  shouldBeVisible(getByCy(NEXT_BUTTON)).click();
  shouldBeVisible(getByCy(FINISH_BUTTON)).click();
};

export const uploadHealthDepartmentPrivateKeyFile = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_PRIVATE_KEY_PATH,
    HD_CORRECT_PRIVATE_KEY_FILE_NAME
  );
};

export const uploadHealthDepartmentPrivateKeyFileLargeSize = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_LARGE_SIZE_FILE_PRIVATE_KEY_PATH,
    HD_LARGE_PRIVATE_KEY_FILE_NAME
  );
};

export const uploadWrongHealthDepartmentPrivateKeyFile = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_WRONG_PRIVATE_KEY_PATH,
    HD_WRONG_PRIVATE_KEY_FILE_NAME
  );
};

export const removeHealthDepartmentPrivateKeyFile = () => {
  task('deleteFileIfExists', HEALTH_DEPARTMENT_PRIVATE_KEY_PATH);
};

export const uploadWrongHealthDepartmentPrivateKeyFileType = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readFile(HEALTH_DEPARTMENT_WRONG_PDF_FILE_PATH).then(fileContent => {
    getDOMElement(INPUT_TYPE_FILE).attachFile({
      fileContent,
      fileName: 'dummy.pdf',
      mimeType: 'application/pdf',
    });
  });
};

export const uploadWrongHealthDepartmentPrivateKeyFileTypeReUploadCorrectFile = () => {
  shouldBeVisible(getDOMElement(ANT_MODAL));
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_WRONG_PRIVATE_KEY_PATH,
    HD_WRONG_PRIVATE_KEY_FILE_NAME
  );
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE, { timeout: 10000 }));
  shouldBeVisible(getDOMElement(ANT_MODAL));
  shouldBeVisible(getByCy(TRY_AGAIN_BUTTON)).click();
  readHDPrivateKeyFile(
    HEALTH_DEPARTMENT_PRIVATE_KEY_PATH,
    HD_CORRECT_PRIVATE_KEY_FILE_NAME
  );
  shouldNotExist(getDOMElement(ANT_MODAL));
};

export const addHealthDepartmentPrivateKeyFile = () => {
  task('fileExists', HEALTH_DEPARTMENT_PRIVATE_KEY_PATH).then(exists => {
    if (!exists) {
      log('Private key should be downloaded');
      downloadHealthDepartmentPrivateKey();
    } else {
      uploadHealthDepartmentPrivateKeyFile();
    }
  });
};
