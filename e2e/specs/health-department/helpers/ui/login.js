import { shouldBeVisible } from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';

import { HEALTH_DEPARTMENT_BASE_ROUTE } from '../../constants/routes';

export const openHDLoginPage = () => {
  cy.visit(HEALTH_DEPARTMENT_BASE_ROUTE);
};

export const verifyLoggedIn = () => {
  shouldBeVisible(getByCy('header'));
  shouldBeVisible(getByCy('healthDepartmentHeader'));
  shouldBeVisible(getByCy('linkMenuIcon'));
  shouldBeVisible(getByCy('logoutButton'));
  shouldBeVisible(getDOMElement('.ant-menu-horizontal'));
  shouldBeVisible(getByCy('navigation'));
};
