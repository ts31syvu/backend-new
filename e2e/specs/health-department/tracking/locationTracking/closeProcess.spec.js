import { loginHealthDepartment } from '../../helpers/api/auth';
import {
  setDatePickerStartDate,
  setDatePickerEndDate,
} from '../../helpers/ui/tracking';
import { addHealthDepartmentPrivateKeyFile } from '../../helpers/ui/handlePrivateKeyFile';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { shouldBeVisible, shouldNotExist } from '../../utils/assertions';
import { type } from '../../utils/commands';
import {
  ANT_POPOVER_INNER_CONTENT,
  ANT_POPOVER_PRIMARY_BUTTON,
  GROUP_PREFIX,
  SEARCH_GROUP_BUTTON,
  GROUP_NAME_INPUT_FIELD,
  START_GROUP_SEARCH_BUTTON,
  REQUEST_GROUP_DATA_BUTTON,
  PROCESS_ENTRY,
  COMPLETE_BUTTON,
  ANT_EXPLAIN_ERROR,
  PROCESS_STATE_FILTER,
  CLOSED_FILTER,
} from '../../constants/selectorKeys';
import { PROCESS_UUID, TRACING_PROCESS } from '../../constants/aliases';
import { YESTERDAY, TODAY } from '../../constants/dates';
import { createTestGroup } from '../../../locations/network/api';
import { getGroupPayload } from '../../../locations/helpers/api/groups';
import { createTestOperator } from '../../../locations/utils/setup';

let testGroup;

describe('Health Department / Tracking / Location tracking', () => {
  before(() => {
    // create groups with different names and zip codes
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
    // login HD
    cy.logoutLocations();
    loginHealthDepartment();
    addHealthDepartmentPrivateKeyFile();
  });

  describe('Open and close process', () => {
    it('opens a process and can close it', () => {
      cy.intercept('POST', '/api/v4/locationTransfers/').as(TRACING_PROCESS);
      // get the search modal
      shouldBeVisible(getByCy(SEARCH_GROUP_BUTTON)).click();
      shouldBeVisible(getByCy(GROUP_NAME_INPUT_FIELD));
      type(getByCy(GROUP_NAME_INPUT_FIELD), testGroup.groupName);
      shouldBeVisible(getByCy(START_GROUP_SEARCH_BUTTON)).click();
      shouldBeVisible(getByCy(GROUP_PREFIX + testGroup.groupName)).click();
      // cannot submit without dates and time
      getByCy(REQUEST_GROUP_DATA_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_EXPLAIN_ERROR));
      // set tracking dates, cannot submit without enddate
      setDatePickerStartDate(YESTERDAY);
      getByCy(REQUEST_GROUP_DATA_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_EXPLAIN_ERROR));
      // set tracking end dates, can submit with all filled
      setDatePickerEndDate(TODAY);
      shouldNotExist(getDOMElement(ANT_EXPLAIN_ERROR));
      // open tracking process
      getByCy(REQUEST_GROUP_DATA_BUTTON).click();
      // wait for request to complete
      cy.wait(`@${TRACING_PROCESS}`);
      getDOMElement(`@${TRACING_PROCESS}`).then(({ response }) => {
        cy.wrap(response.body.tracingProcessId).as(PROCESS_UUID);
      });
      // find and get the entry
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        getByCy(`${PROCESS_ENTRY}-${uuid}`).click();
      });
      // can close Process
      shouldBeVisible(getByCy(COMPLETE_BUTTON)).click();
      getDOMElement(ANT_POPOVER_INNER_CONTENT).within(() => {
        shouldBeVisible(getDOMElement(ANT_POPOVER_PRIMARY_BUTTON)).click();
      });
      // verify process is not in the list
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        shouldNotExist(getByCy(`${PROCESS_ENTRY}-${uuid}`));
      });
      // verfiy process is in list of closed processes
      getByCy(PROCESS_STATE_FILTER).click();
      getByCy(CLOSED_FILTER).click();
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        shouldBeVisible(getByCy(`${PROCESS_ENTRY}-${uuid}`));
      });
    });
  });
});
