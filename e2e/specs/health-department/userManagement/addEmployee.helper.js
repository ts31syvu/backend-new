import { TEST_EMPLOYEE_UUID } from '../constants/aliases';

export const getEmployee = employeeEmail => {
  cy.request('api/v3/healthDepartmentEmployees/?includeDeleted=false').then(
    // eslint-disable-next-line require-await
    async response => {
      const testEmployee = response.body.find(
        element => element.email === employeeEmail
      );
      cy.wrap(testEmployee.uuid).as(TEST_EMPLOYEE_UUID);
    }
  );
};
