import { loginHealthDepartment } from '../helpers/api/auth';
import { HEALTH_DEPARTMENT_USER_MANAGEMENT_ROUTE } from '../constants/routes';
import { getByCy, getDOMElement } from '../utils/selectors';
import { type, visit } from '../utils/commands';
import { shouldBeVisible } from '../utils/assertions';
import {
  FIRST_NAME,
  LAST_NAME,
  PHONE,
  EMAIL,
  SUBMIT_BUTTON,
  BUTTON,
  ANT_MODAL,
  ADD_EMPLOYEE,
  ANT_NOTIFICATION,
  GENERATE_PASSWORD,
  ANT_POPCONFIRM,
  ANT_POPOVER_PRIMARY_BUTTON,
  EMPLOYEE_PREFIX,
} from '../constants/selectorKeys';
import { PASSWORD, TEST_EMPLOYEE_UUID } from '../constants/aliases';
import { getEmployee } from './addEmployee.helper';

const EMPLOYEE_FIRST_NAME = 'Luca';
const EMPLOYEE_LAST_NAME = 'Tester';
const EMPLOYEE_EMAIL = 'employee@hd.com';
const EMPLOYEE_PHONE = '+49 176 12345678';

describe('Health Department / User Management / Create new employee', () => {
  describe('when create a new HD employee', () => {
    it('the employee is created and able to login', () => {
      loginHealthDepartment();
      visit(HEALTH_DEPARTMENT_USER_MANAGEMENT_ROUTE);
      shouldBeVisible(getByCy(ADD_EMPLOYEE));
      getByCy(ADD_EMPLOYEE).click();
      // filling employee form
      getDOMElement(ANT_MODAL).within(() => {
        shouldBeVisible(getDOMElement(FIRST_NAME));
        type(getDOMElement(FIRST_NAME), EMPLOYEE_FIRST_NAME);
        shouldBeVisible(getDOMElement(LAST_NAME));
        type(getDOMElement(LAST_NAME), EMPLOYEE_LAST_NAME);
        shouldBeVisible(getDOMElement(PHONE));
        type(getDOMElement(PHONE), EMPLOYEE_PHONE);
        shouldBeVisible(getDOMElement(EMAIL));
        type(getDOMElement(EMAIL), EMPLOYEE_EMAIL);
        shouldBeVisible(getDOMElement(BUTTON));
        shouldBeVisible(getDOMElement(SUBMIT_BUTTON));
        getDOMElement(SUBMIT_BUTTON).click();
      });
      // save generated password
      shouldBeVisible(getDOMElement(ANT_NOTIFICATION));
      getDOMElement(ANT_MODAL).within(() => {
        getByCy(GENERATE_PASSWORD).then($password => {
          cy.wrap($password.text()).as(PASSWORD);
        });
        shouldBeVisible(getDOMElement(BUTTON));
        getDOMElement(BUTTON).click();
      });
      getDOMElement(ANT_POPCONFIRM).within(() => {
        shouldBeVisible(
          getDOMElement('.ant-popover-buttons > button:nth-child(1)')
        );

        shouldBeVisible(getDOMElement(ANT_POPOVER_PRIMARY_BUTTON));
        getDOMElement(ANT_POPOVER_PRIMARY_BUTTON).click();
      });
      // verify employee is created
      getEmployee(EMPLOYEE_EMAIL);

      getDOMElement(`@${TEST_EMPLOYEE_UUID}`).then(uuid =>
        shouldBeVisible(getByCy(EMPLOYEE_PREFIX + uuid))
          .and('contain', `${EMPLOYEE_LAST_NAME}`)
          .and('contain', `${EMPLOYEE_LAST_NAME}`)
          .and('contain', `${EMPLOYEE_PHONE}`)
          .and('contain', `${EMPLOYEE_EMAIL}`)
      );
      cy.logoutHD();
      // login as a new employee
      getDOMElement(`@${PASSWORD}`).then(password => {
        // eslint-disable-next-line promise/no-nesting
        cy.basicLoginHD(EMPLOYEE_EMAIL, password).then(response => {
          expect(response.status).to.eq(204);
        });
      });
    });
  });
});
