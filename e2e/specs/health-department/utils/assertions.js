export const shouldBeVisible = element => element.should('be.visible');

export const shouldNotExist = element => element.should('not.exist');

export const shouldContain = (element, value) =>
  element.should('contain', value);

export const shouldBeDisabled = element => element.should('be.disabled');

export const shouldNotBeDisabled = element => element.should('not.be.disabled');
