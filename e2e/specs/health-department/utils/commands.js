export const type = (element, value) => element.type(value, { delay: 0 });

export const visit = url => cy.visit(url);

export const readFile = filepath => cy.readFile(filepath);

export const task = (event, argument) => cy.task(event, argument);

export const log = message => cy.log(message);
