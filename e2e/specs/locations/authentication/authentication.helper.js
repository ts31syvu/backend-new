import { shouldBeVisible, shouldNotExist } from '../utils/assertions';
import { getByCy, getDOMElement } from '../utils/selectors';
import { type } from '../utils/commands';

import {
  EMAIL_FIELD,
  ANT_VALIDATION_ERROR,
  CREATE_NEW_ACCOUNT_BUTTON,
  SET_PASSWORD_SUBMIT_BUTTON,
  SET_PASSWORD,
  LEGAL_TERMS,
  LEGAL_TERMS_SUBMIT_BUTTON,
  FIRST_NAME,
  LAST_NAME,
  PHONE,
  CONFIRM_NAME_BUTTON,
  PASSWORD,
  SET_PASSWORD_FIELD,
  SET_PASSWORD_CONFIRM_FIELD,
  FINISH_REGISTER,
  LOGIN_SUBMIT_BUTTON,
  SET_TERMS_BACK_BUTTON,
  BUSINESS_NAME,
  BUSINESS_STREET,
  BUSINESS_STREET_NUMBER,
  BUSINESS_ZIP_CODE,
  BUSINESS_CITY,
  CONFIRM_BUSINESS_INFO_BUTTON,
} from '../constants/selectorKeys';

export const isValidationErrorShown = () =>
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));

export const enterEmail = email => {
  type(getDOMElement(EMAIL_FIELD), email);
};

export const getNewAccountButton = () => getByCy(CREATE_NEW_ACCOUNT_BUTTON);

export const enterContacts = (firstName, lastName, phone) => {
  type(getDOMElement(FIRST_NAME), firstName);
  type(getDOMElement(LAST_NAME), lastName);
  type(getDOMElement(PHONE), phone);
  getByCy(CONFIRM_NAME_BUTTON).click();
};

export const enterBusinessInformation = (businessName, businessAddress) => {
  type(getDOMElement(BUSINESS_NAME), businessName);
  type(getDOMElement(BUSINESS_STREET), businessAddress.street);
  type(getDOMElement(BUSINESS_STREET_NUMBER), businessAddress.streetNr);
  type(getDOMElement(BUSINESS_ZIP_CODE), businessAddress.zipCode);
  type(getDOMElement(BUSINESS_CITY), businessAddress.city);
  getByCy(CONFIRM_BUSINESS_INFO_BUTTON).click();
};

export const getSetPassword = () => getByCy(SET_PASSWORD);

export const getPasswordSubmitButton = () =>
  getByCy(SET_PASSWORD_SUBMIT_BUTTON);

export const getTermsBackButton = () => getByCy(SET_TERMS_BACK_BUTTON);

export const enterPassword = password => {
  type(getDOMElement(PASSWORD), password);
  getByCy(LOGIN_SUBMIT_BUTTON).click();
};

export const setNewPassword = (password, confirmPassword) => {
  type(getByCy(SET_PASSWORD_FIELD), password);
  type(getByCy(SET_PASSWORD_CONFIRM_FIELD), confirmPassword);
  getPasswordSubmitButton().click();
};

export const clearPasswordFields = () => {
  getByCy(SET_PASSWORD_FIELD).clear();
  getByCy(SET_PASSWORD_CONFIRM_FIELD).clear();
};

export const getLegalTerms = () => getByCy(LEGAL_TERMS);

export const registerDoesNotExist = () => {
  getByCy(LEGAL_TERMS_SUBMIT_BUTTON).click();
  shouldNotExist(getByCy(FINISH_REGISTER));
};
