import { enterEmail } from '../authentication.helper';
import { createTestOperator } from '../../utils/setup';

import { BASE_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { shouldBeVisible } from '../../utils/assertions';
import { visit } from '../../utils/commands';
import {
  EMAIL_FIELD,
  PASSWORD,
  LOGIN_PAGE,
  FORGOT_PASSWORD_LINK,
  FORGOT_PASSWORD_PAGE,
  SEND_RESET_LINK_BUTTON,
  ANT_NOTIFICATION_NOTICE_MESSAGE,
} from '../../constants/selectorKeys';

const NONEXISTENT_EMAIL = 'non-existant-user@nexenio.com';
let testOperator;

describe('Forgot password', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
  });
  beforeEach(() => visit(BASE_ROUTE));
  it('can send the reset password email and redirect the user back to the login page', () => {
    enterEmail(testOperator.email);
    shouldBeVisible(getDOMElement(PASSWORD));
    getByCy(FORGOT_PASSWORD_LINK).click();
    shouldBeVisible(getByCy(FORGOT_PASSWORD_PAGE));
    enterEmail(testOperator.email);
    getByCy(SEND_RESET_LINK_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_MESSAGE));
    shouldBeVisible(getByCy(LOGIN_PAGE));
    shouldBeVisible(getDOMElement(EMAIL_FIELD));
  });

  it('displays the success notification even if the account does not exist ', () => {
    enterEmail(NONEXISTENT_EMAIL);
    shouldBeVisible(getDOMElement(PASSWORD));
    getByCy(FORGOT_PASSWORD_LINK).click();
    enterEmail(NONEXISTENT_EMAIL);
    getByCy(SEND_RESET_LINK_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_MESSAGE));
    shouldBeVisible(getByCy(LOGIN_PAGE));
    shouldBeVisible(getDOMElement(EMAIL_FIELD));
  });
});
