import { shouldBeVisible } from '../../utils/assertions';
import {
  createOperatorValues,
  STANDARD_PASSWORD,
} from '../../utils/setup.helper';
import { WRONG_PASSWORD } from '../../constants/inputs';
import { createBasicOperator } from '../../utils/setup';
import { visit } from '../../utils/commands';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  LOGIN_ERROR_NOTIFICATION,
  ANT_MESSAGE_CHECK,
  ANT_MESSAGE_ERROR,
  KEY_UPLOAD_ERROR,
} from '../../constants/selectorKeys';
import { APP_ROUTE, BASE_ROUTE } from '../../constants/routes';
import { enterEmail, enterPassword } from '../authentication.helper';
import {
  downloadLocationPrivateKeyFile,
  uploadLocationPrivateKeyFile,
} from '../../helpers/ui/handlePrivateKeyFile';
import { getKeyFileName, getKeyFilePath } from '../../utils/keyFile.helper';
import {
  E2E_LOCATION_WRONG_PRIVATE_KEY_PATH,
  E2E_LOCATION_WRONG_PRIVATE_KEY_NAME,
} from '../../constants/users';
import { login } from '../../network/api';

let testOperator;
const unknownOperator = createOperatorValues();
let KEY_NAME;
let KEY_PATH;

describe('Location login', () => {
  before(() => {
    testOperator = createBasicOperator();
  });
  beforeEach(() => {
    visit(BASE_ROUTE);
  });
  describe('Login password, email and key file validation', () => {
    it('does not log in operator with invalid password', () => {
      enterEmail(testOperator.email);
      enterPassword(WRONG_PASSWORD);
      shouldBeVisible(getByCy(LOGIN_ERROR_NOTIFICATION));
    });
    it('does not log in operator with invalid email', () => {
      enterEmail(unknownOperator.email);
      enterPassword(unknownOperator.password);
      shouldBeVisible(getByCy(LOGIN_ERROR_NOTIFICATION));
    });
    describe('Login successfully and upload private key', () => {
      before(() => {
        KEY_NAME = getKeyFileName(
          testOperator.firstName,
          testOperator.lastName
        );
        KEY_PATH = getKeyFilePath(
          testOperator.firstName,
          testOperator.lastName
        );
      });
      it('should show success message on first attempt of downloading and uploading correct private key', () => {
        login({ username: testOperator.email, password: STANDARD_PASSWORD });
        visit(APP_ROUTE);
        downloadLocationPrivateKeyFile();
        // upload wrong private key is rejected
        uploadLocationPrivateKeyFile(
          E2E_LOCATION_WRONG_PRIVATE_KEY_PATH,
          E2E_LOCATION_WRONG_PRIVATE_KEY_NAME
        );
        shouldBeVisible(getDOMElement(ANT_MESSAGE_ERROR));
        shouldBeVisible(getByCy(KEY_UPLOAD_ERROR));
        // upload correct private key is successful
        uploadLocationPrivateKeyFile(KEY_PATH, KEY_NAME);
        shouldBeVisible(getDOMElement(ANT_MESSAGE_CHECK));
      });
    });
  });
});
