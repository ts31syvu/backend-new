import { createTestOperator } from '../../utils/setup';
import {
  verifyLocationHomePage,
  verifyModalWindowIsClosed,
} from '../../helpers/ui/validations';

describe('Location / Authentication / Login', () => {
  describe('Login as an operator and provide private key later', () => {
    it('displays location home page', () => {
      createTestOperator();
      verifyModalWindowIsClosed();
      verifyLocationHomePage();
    });
  });
});
