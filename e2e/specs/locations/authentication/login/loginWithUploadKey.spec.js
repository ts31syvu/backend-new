import { createTestOperator } from '../../utils/setup';
import {
  verifyLocationHomePage,
  verifyModalWindowIsClosed,
} from '../../helpers/ui/validations';

describe('Login as an operator and upload private key', () => {
  it('accepts private key and displays location home page', () => {
    // Login with privateKeyUpload
    createTestOperator(true);
    // Close private key upload modal
    verifyModalWindowIsClosed();
    verifyLocationHomePage();
  });
});
