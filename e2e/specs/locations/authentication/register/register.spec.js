import {
  enterEmail,
  enterContacts,
  setNewPassword,
  clearPasswordFields,
  registerDoesNotExist,
  getNewAccountButton,
  getPasswordSubmitButton,
  isValidationErrorShown,
  getSetPassword,
  getLegalTerms,
  enterBusinessInformation,
} from '../authentication.helper';

import {
  NEW_E2E_EMAIL,
  NEW_E2E_FIRST_NAME,
  NEW_E2E_LAST_NAME,
  NEW_E2E_PHONE,
  NEW_E2E_VALID_PASSWORD,
  SOME_OTHER_INVALID_PASSWORD,
  STRENGTH1_PASSWORD,
  STRENGTH2_PASSWORD,
  STRENGTH3_PASSWORD,
  STRENGTH4_PASSWORD,
  STRENGTH0_PASSWORD,
  BUSINESS_NAME,
  BUSINESS_ADDRESS,
} from '../../constants/inputs';
import {
  AVV_CHECKBOX,
  TERMS_AND_CONDITIONS_CHECKBOX,
  FINISH_REGISTER,
  LEGAL_TERMS_SUBMIT_BUTTON,
  END_REGISTRATION_BUTTON,
  EMAIL_FIELD,
} from '../../constants/selectorKeys';
import { REGISTER_ROUTE } from '../../constants/routes';
import { shouldBeVisible } from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { visit, waitForAlias } from '../../utils/commands';
import { REGISTER_OPERATOR } from '../../constants/aliases';

describe('Registration', () => {
  it('registers a new user and handles errors', { retries: 3 }, () => {
    cy.intercept('POST', 'api/v3/operators').as(REGISTER_OPERATOR);
    visit(REGISTER_ROUTE);
    // Enter new email
    enterEmail(NEW_E2E_EMAIL);
    getNewAccountButton().click();
    // Enter name
    enterContacts(NEW_E2E_FIRST_NAME, NEW_E2E_LAST_NAME, NEW_E2E_PHONE);
    enterBusinessInformation(BUSINESS_NAME, BUSINESS_ADDRESS);
    // Set password - Starting with error handling
    shouldBeVisible(getSetPassword());
    // No input
    getPasswordSubmitButton().click();
    isValidationErrorShown();
    // Incorrect PW repeat
    setNewPassword(NEW_E2E_VALID_PASSWORD, SOME_OTHER_INVALID_PASSWORD);
    isValidationErrorShown();
    clearPasswordFields();

    // PW strength 0
    setNewPassword(STRENGTH0_PASSWORD, STRENGTH0_PASSWORD);
    isValidationErrorShown();
    clearPasswordFields();
    // PW strength 1
    setNewPassword(STRENGTH1_PASSWORD, STRENGTH1_PASSWORD);
    isValidationErrorShown();
    clearPasswordFields();
    // PW strength 2
    setNewPassword(STRENGTH2_PASSWORD, STRENGTH2_PASSWORD);
    isValidationErrorShown();
    clearPasswordFields();
    // PW strength 3
    setNewPassword(STRENGTH3_PASSWORD, STRENGTH3_PASSWORD);
    isValidationErrorShown();
    // PW strength 4 - Happy case
    clearPasswordFields();
    setNewPassword(STRENGTH4_PASSWORD, STRENGTH4_PASSWORD);
    // Set legal - Starting with error handling
    shouldBeVisible(getLegalTerms());
    // None accepted
    registerDoesNotExist();
    //  No legals
    getByCy(AVV_CHECKBOX).check();
    registerDoesNotExist();
    // No AVV
    getByCy(AVV_CHECKBOX).uncheck();
    getByCy(TERMS_AND_CONDITIONS_CHECKBOX).check();
    registerDoesNotExist();
    // Set legals - Happy case
    getByCy(AVV_CHECKBOX).check();
    getByCy(LEGAL_TERMS_SUBMIT_BUTTON).click();
    // Finish
    waitForAlias(`@${REGISTER_OPERATOR}`, 7000).should(xhr => {
      expect(xhr.response).to.have.property('statusCode', 204);
      expect(xhr.request.body).to.have.property('email', NEW_E2E_EMAIL);
    });
    shouldBeVisible(getByCy(FINISH_REGISTER));
    getByCy(END_REGISTRATION_BUTTON).click();
    shouldBeVisible(getDOMElement(EMAIL_FIELD));
  });
});
