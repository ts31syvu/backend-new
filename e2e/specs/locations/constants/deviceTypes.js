export const DEVICE_TYPE_IOS = 0;
export const DEVICE_TYPE_ANDROID = 1;
export const DEVICE_TYPE_STATIC = 2;
export const DEVICE_TYPE_WEBAPP = 3;
export const DEVICE_TYPE_FORM = 4;

export const MIN_WIDTH_REGULAR_DEVICE = 993;
export const MAX_WIDTH_SMALL_DEVICE = 991;
export const MIN_HEIGHT = 700;
