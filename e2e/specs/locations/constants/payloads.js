import {
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_FORM,
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_WEBAPP,
} from './deviceTypes';

export const E2E_DYNAMIC_TRACE_ID = 'w2RiSHiitpfJ0hxcJz5ZYw==';
export const E2E_FORM_TRACE_ID = 'MPc2dmPJQkfS5uqIc4yG1A==';

export const DYNAMIC_DEVICE_TYPES = {
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_WEBAPP,
};

export const createGroupPayload = {
  type: 'base',
  name: 'Testing group',
  firstName: 'Torsten',
  lastName: 'Tester',
  phone: '+4917612345678',
  streetName: 'Charlottenstr.',
  streetNr: '59',
  zipCode: '10117',
  city: 'Berlin',
  state: 'Berlin',
  lat: 0,
  lng: 0,
  radius: 0,
  tableCount: null,
  isIndoor: true,
};

// Fake trace data
export const dynamicTraceDataPayload = ({
  deviceType,
  publicKey,
  scannerId,
  traceId,
}) => {
  return {
    data:
      'xjpySSaTjWlaudphEb/gUG7ZpYtbkuD93PgKgSZhj+MLzRyreu4CwN+Z9rogtwzQXp2E6GfdHSRYXBPvUdMnlqOQAIjU7dYrmIY4',
    deviceType,
    iv: 'ZpW6zMLHKjix9c10a+4d6w==',
    mac: 'LSMAjTjtBDAAFzhMeiV4/vjjHZO64EJa45jFgzG7iYg=',
    publicKey,
    scannerId,
    timestamp: Number.parseInt(Date.now() / 1000, 10),
    traceId,
  };
};

export const formTraceDataPayload = ({
  traceId,
  scannerId,
  locationId,
  publicKey,
}) => {
  return {
    traceId,
    scannerId,
    locationId,
    timestamp: Number.parseInt(Date.now() / 1000, 10),
    data:
      '/1/wt5WkIbVcPlzq+dF+ELMJaGBiA0ufzIjKbFKlSuKGDvEQBskHemi48bzhAd/97pmlYkHtYUB+8MFGc7pZgaJNGa1SmCLuYcJ6',
    publicKey,
    iv: 'YgyJXzO2mwUmi1EHKVUlBg==',
    mac: 'wgCeNczYMh69q+ysje7DKG6XcoWLO0g5UbSmCT1XEUI=',
    deviceType: DEVICE_TYPE_FORM,
  };
};
