export const BASE_ROUTE = '/';
export const LOGIN_ROUTE = '/login';
export const REGISTER_ROUTE = '/register';
export const FORGOT_PASSWORD_ROUTE = '/forgotPassword';
export const APP_ROUTE = '/app/group';
export const CONTACT_FORM_ROUTE = '/contact-form';
export const PROFILE_ROUTE = 'app/profile';
export const DATA_TRANSFERS_ROUTE = 'app/dataTransfers';

export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';

export const DPA_FRAGMENT = 'AVV_Luca';
export const TOMS_FRAGMENT = 'TOMS_Luca';

export const PRIVACY_MANDATORY_FRAGMENT = 'DSE_Luca_mandatory';
export const PRIVACY_OPTIONAL_FRAGMENT = 'DSE_Luca_optional';

export const LOCATION_GROUPS_ROUTE = 'api/v3/locationGroups';
export const OPERATOR_LOCATIONS_ROUTE = 'api/v3/operators/locations';
export const HELP_CENTER_ROUTE = '/app/help';

export const LICENSES_ROUTE = '/licenses';

export const CHECKOUT_ROUTE = '/checkout';
