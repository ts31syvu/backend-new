// General
export const SUBMIT_BUTTON = 'button[type=submit]';
export const BUTTON_TYPE = 'button[type=button]';
export const BUTTON = 'button';
export const PROCEED = 'proceed';
export const NEXT_STEP_BUTTON = 'nextStep';
export const STEP_COUNT = 'stepCount';
export const NEXT_BUTTON = 'next';
export const BACK_BUTTON = 'back';
export const YES_BUTTON = 'yes';
export const NO_BUTTON = 'no';
export const DONE_BUTTON = 'done';
export const CLOSE_MODAL_X_BUTTON = 'button[aria-label="Close"]';
export const INPUT_TYPE_FILE = 'input[type=file]';

// General: Ids
export const ROOT = '#root';
export const EMAIL_FIELD = '#email';
export const EMAIL_DISABLED = '#emailDisabled';
export const PASSWORD = '#password';
export const STREET = '#streetName';
export const STREET_NO = '#streetNr';
export const ZIP = '#zipCode';
export const CITY = '#city';
export const PHONE = '#phone';
export const LOCATION_SEARCH = '#locationSearch';
export const GROUP_NAME = '#groupName';
export const PASSWORD_CONFIRM = '#passwordConfirm';
export const FIRST_NAME = '#firstName';
export const LAST_NAME = '#lastName';
export const BUSINESS_NAME = '#businessEntityName';
export const BUSINESS_STREET = '#businessEntityStreetName';
export const BUSINESS_STREET_NUMBER = '#businessEntityStreetNumber';
export const BUSINESS_ZIP_CODE = '#businessEntityZipCode';
export const BUSINESS_CITY = '#businessEntityCity';
export const AVV = '#avv';
export const RADIUS = '#radius';
export const LOCATION_NAME = '#locationName';
export const TERMS_AND_CONDITIONS = '#termsAndConditions';
export const CURRENT_PASSWORD = '#currentPassword';
export const NEW_PASSWORD_INPUT_FIELD = '#newPassword';
export const NEW_PASSWORD_REPEAT_INPUT_FIELD = '#newPasswordConfirm';
export const EDIT_PERSONAL_PROFILE_BUTTON = 'editPersonalProfileInfo';

// General: Error
export const LOGIN_ERROR_NOTIFICATION = 'loginError';

// Forgot password
export const SEND_RESET_LINK_BUTTON = 'sentResetLinkSubmit';

// Login
export const LOGIN_SUBMIT_BUTTON = 'loginSubmitButton';
export const LOGIN_PAGE = 'loginPage';
export const DOWNLOAD_PRIVATE_KEY_BUTTON = 'downloadPrivateKey';
export const PRIVATE_KEY_DOWNLOADED_CHECKBOX = 'checkPrivateKeyIsDownloaded';
export const SKIP_PRIVATE_KEY_UPLOAD_BUTTON = 'skipPrivateKeyUpload';
export const CONFIRM_BUTTON = 'complete';
export const KEY_UPLOAD_ERROR = 'keyUploadError';

// Registration
export const CREATE_NEW_ACCOUNT_BUTTON = 'createNewAccountButton';
export const CONFIRM_NAME_BUTTON = 'confirmContactsButton';
export const CONFIRM_BUSINESS_INFO_BUTTON = 'confirmBusinessInfoButton';

export const SET_PASSWORD = 'setPassword';
export const SET_PASSWORD_SUBMIT_BUTTON = 'setPasswordSubmitButton';

export const SET_PASSWORD_FIELD = 'setPasswordField';
export const SET_PASSWORD_CONFIRM_FIELD = 'setPasswordConfirmField';

export const LEGAL_TERMS = 'legalTerms';
export const AVV_CHECKBOX = 'avvCheckbox';
export const TERMS_AND_CONDITIONS_CHECKBOX = 'termsAndConditionsCheckbox';
export const LEGAL_TERMS_SUBMIT_BUTTON = 'legalTermsSubmitButton';
export const FINISH_REGISTER = 'finishRegister';
export const END_REGISTRATION_BUTTON = 'endRegistrationButton';
export const SET_TERMS_BACK_BUTTON = 'setTermsBackButton';

// Input Field Ids
export const EMAIL_INPUT_FIELD = '#email';
export const LOCATION_NAME_INPUT_FIELD = '#locationName';
export const STREET_NAME_INPUT_FIELD = '#streetName';
export const STREET_NUMBER_INPUT_FIELD = '#streetNr';
export const ZIP_CODE_INPUT_FIELD = '#zipCode';
export const CITY_INPUT_FIELD = '#city';
export const PHONE_INPUT_FIELD = '#phone';
export const RADIUS_INPUT_FIELD = '#radius';
export const LOCATION_SEARCH_INPUT_FIELD = '#locationSearch';
export const TABLE_COUNT_INPUT_FIELD = '#tableCount';
export const NAME_INPUT_FIELD = '#name';

// Header
export const OPEN_DATA_REQUESTS_BUTTON = 'dataRequests';
export const OPEN_HELPCENTER_BUTTON = 'buttonHelpCenter';
export const PROFILE_DROPDOWN_MENU_TRIGGER = 'dropdownMenuTrigger';
export const OPEN_PROFILE_BUTTON = 'profile';
export const LOGO_HOME = 'home';

// Pages
export const FORGOT_PASSWORD_PAGE = 'forgotPasswordPage';

// Links
export const FORGOT_PASSWORD_LINK = 'forgotPasswordLink';
export const OPEN_AREA_SETTINGS_LINK = "[id$='-tab-openAreaSettings']";
export const OPEN_AREA_CHECKIN_LINK = "[id$='-tab-openAreaCheckin']";
export const OPEN_LOCATION_SETTINGS_LINK = "[id$='-tab-openLocationSettings']";
export const OPEN_LOCATION_CHECKIN_LINK = "[id$='-tab-openLocationCheckin']";

// Area Sidebar
export const AREA_LIST_GENERAL_LOCATION = 'location-General';
export const AREA_LIST_WRAPPER = '#groupList';
export const AREA_PREFIX = 'location-';
export const LOCATION_PREFIX = 'location-';
export const CREATE_LOCATION_PREFIX = 'createLocation-';
export const SECTION_ASIDE = 'section > aside';

// Area Settings
export const DELETE_AREA_BUTTON = 'deleteLocation';
export const SUCCESS_DELETE_NOTIFICATION = '.successDeletedNotification';
export const EDIT_ADDRESS_BUTTON = 'editAddress';
export const SAVE_CHANGES_AREA_BUTTON = 'editLocation';
export const EDIT_SUCCESS_MESSAGE = '.editLocationSuccess';
export const BACK_TO_LOCATION_BUTTON = 'backButton';
export const DELETE_LOCATION_CONFIRM_BUTTON = 'deleteLocationConfirmButton';
export const AREA_PROFILE_DISPLAY = 'areaProfile';

// Password modal
export const PASSWORD_INPUT_FIELD = 'passwordInputField';
export const PROCEED_BUTTON = 'proceedButton';

// Create Area / Location Modal
export const SAVE_ADRESS_BUTTON = 'saveAddress';
export const GOOGLE_SEARCH_DROPDOWN_ENTRY =
  '.pac-container > div:first-of-type';
export const RESTAURANT = 'restaurant';
export const QR_CODE_DOWNLOAD_LOCATION = 'download';
export const QR_CODE_DOWNLOAD_BUTTON_PDF = 'qrCodeDownload';
export const CREATE_AREA_MODAL_HEADER = 'createAreaModalHeader';
export const CREATE_AREA_MODAL_DESCRIPTION = 'createAreaModalDescription';

// Group
export const GROUPNAME = 'groupName';
export const DELETE_GROUP = 'deleteGroup';
export const GROUP_SETTINGS_HEADER = 'groupSettingsHeader';
export const INDOOR_SELECTION = 'indoorSelection';
export const SELECT_INDOOR = 'selectIndoor';
export const AREA_NAME_INPUT = 'areaNameInput';
export const FINISH_GROUP_CREATION = 'finishGroupCreation';
export const GROUP_ITEM = 'groupItem';
export const GROUP_SELECT = 'groupSelect';
export const SELECT_GROUP_DROPDOWN = 'selectGroupDropdown';
export const CREATE_GROUP = 'createGroup';
export const SELECT_GROUP_TYPE = 'selectGroupType';
export const CREATE_GROUP_TABLET_BUTTON = 'createGroupTabletButton';

// Location Dashboard
export const LOCATION_DISPLAY_NAME = 'locationDisplayName';
export const GUEST_COUNT = 'guestCount';
export const CHECKOUT_GUEST = 'checkoutGuest';
export const CHECKOUT_SUCCESS = '.successCheckout';
export const CHECKIN_DATE = 'checkinDate';
export const CHECKIN_TIME = 'checkinTime';
export const CHECKOUT_DATE = 'checkoutDate';
export const CHECKOUT_TIME = 'checkoutTime';
export const CAM_SCANNER = 'camScanner';
export const SCANNER = 'scanner';
export const CONTACT_FORM = 'contactForm';
export const SHOW_GUEST_LIST_MODAL_TRIGGER = 'showGuestList';
export const TOTAL_CHECKIN_COUNT = 'totalCheckinCount';
export const TABLE_SUBDIVISION_TOGGLE = 'activateTableSubdivision';
export const LOCATION_TABLE_SUBDEVISION = 'locationCard-tableSubdivision';
export const CUSTOMIZED_CHECKOUT_CARD = 'locationCard-checkoutRadius';
export const CHECKOUT_RADIUS_SETTINGS = 'checkoutRadius';
export const CHECKOUT_RADIUS_TOGGLE = 'activateCheckoutRadius';
export const SECTION_MAIN = 'section > main';
export const AREA_CHECKED = 'aria-checked';
export const LOCATION_CARD_PREFIX = 'locationCard-';
export const ADD_REQUESTS_BUTTON = 'addRequestButton';
export const REMOVE_ADDITIONAL_DATA_BUTTON = 'removeAdditionalData';

// Location dashboard - Generate Qr Codes
export const LOCATION_GENERATE_QR_CODES = 'locationCard-generateQRCodes';
export const QR_CODE_FOR_ENTIRE_AREA_SECTION = 'qrCodeForEntireAreaSection';
export const QR_CODE_FOR_ENTIRE_AREA_TOGGLE = 'qrCodeForEntireAreaToggle';
export const QR_CODE_DOWNLOAD_BUTTON_CSV = 'qrCodeDownloadCsv';

export const QR_CODE_COMPATABILITY_SECTION = 'qrCodeCompatabilitySection';
export const QR_CODE_COMPATABILITY_TOGGLE = 'qrCodeCompatabilityToggle';
export const QR_CODE_COMPATABILITY_INFO_ICON = 'qrCodeCompatabilityInfoIcon';
export const QR_CODES_CSV_INFO_ICON = 'qrCodeCsvInfoIcon';

export const QR_PRINT_SECTION = 'qrPrintSection';
export const QR_PRINT_LINK = 'qrPrintLink';

export const QR_CODES_FOR_TABLES_SECTION = 'qrCodesForTablesSection';
export const QR_CODES_FOR_TABLES_TOGGLE = 'qrCodesForTablesToggle';

// Location dashboard - Website, Menu and other Links
export const DASHBOARD_WEBSITE_MENU_AND_LINKS =
  'locationCard-websiteMenuAndLinks';
export const WEBSITE_MENU_LINKS_SECTIONS = 'websiteMenuLinksSection';
export const LINK_CONTENT_INFORMATION_ICON = 'linkContentInformationIcon';
export const LINK_INPUT_FIELD_MENU = 'linkInput-menu';
export const LINK_INPUT_FIELD_WEBSITE = 'linkInput-website';
export const LINK_INPUT_FIELD_SCHEDULE = 'linkInput-schedule';
export const LINK_INPUT_FIELD_MAP = 'linkInput-map';
export const LINK_INPUT_FIELD_GENERAL = 'linkInput-general';
export const WEBSITE_LINKS_SAVE_CHANGES_BUTTON =
  'websiteLinksSaveChangesButton';

// Table Allocation
export const SHOW_TABLE_ALLOCATION = 'showTableAllocation';
export const OPEN_CHECKOUT_CONFIRMATION_BUTTON =
  'openCheckoutConfirmationButton';
export const OPEN_ALL_TABLES_CHECKOUT_CONFIRMATION_BUTTON =
  'openCheckoutAllTablesConfirmation';
export const CANCEL_BUTTON = 'cancelButton';
export const CHECKOUT_BUTTON = 'checkoutButton';
export const CANCEL_CHECKOUT_ALL_TABLES_BUTTON =
  'cancelCheckoutAllTablesButton';
export const CHECKOUT_ALL_TABLES_BUTTON = 'checkoutAllTablesButton';
export const ALLOCATED_TABLE_COUNT = 'allocatedTableCount';
export const REFRESH_BUTTON = 'refreshButton';
export const REFRESH_TIME = 'refreshTime';

// Area
export const AREA_NAME_DISPLAY = 'locationDisplayName';

// Ant design
export const ANT_MODAL_BODY = '.ant-modal-body';
export const ANT_MODAL_CONTENT = '.ant-modal-content';
export const ANT_NOTIFICATION = '.ant-notification';
export const ANT_FORM_HORIZONTAL = '.ant-form-horizontal';
export const ANT_VALIDATION_ERROR = '.ant-form-item-explain-error';
export const ANT_NOTIFICATION_NOTICE_MESSAGE =
  '.ant-notification-notice-message';
export const ANT_NOTIFICATION_NOTICE_WITH_ICON =
  '.ant-notification-notice-with-icon';
export const ANT_NOTIFICATION_WARNING_ICON =
  '.ant-notification-notice-icon-warning';
export const ANT_PRIMARY_POPOVER_BUTTON =
  '.ant-popover-buttons .ant-btn-primary';
export const ANT_POPOVER_BUTTONS = ' .ant-btn.ant-btn-sm';
export const ANT_MESSAGE_NOTICE = '.ant-message-notice';
export const ANT_MESSAGE_SUCCESS = '.ant-message-success';
export const ANT_MESSAGE_CHECK = '.anticon-check-circle';
export const ANT_MESSAGE_ERROR = '.anticon-close-circle';
export const ANT_CLOSE_MODAL = '.ant-modal-close-x';
export const ANT_INPUT_FIELD = '.ant-input';
export const ANT_POPOVER_CONTENT = '.ant-popover-content';

export const ANT_COLLAPSE_ITEM_ACTIVE = '.ant-collapse-item-active';
export const ANT_INPUT_NUMBER_HANDLER_UP = '.ant-input-number-handler-up';
export const ANT_INPUT_NUMBER_HANDLER_DOWN = '.ant-input-number-handler-down';

export const ANT_TOOLTIP_INNER = '.ant-tooltip-inner';

// Ant modal Icons
export const ANT_TOOLTIP_OPEN = '.ant-tooltip-open';

// Icons
export const SUCCESS_CHECK_ICON = 'span[aria-label="check-circle"]';
export const ERROR_ICON = 'span[aria-label="close-circle"]';

// Profile Page
export const BUSINESS_PROFILE_PREVIEW = 'businessProfilePreview';
export const PERSONAL_PROFILE_PREVIEW = 'personalProfilePreview';
export const OPERATOR_NAME = 'operatorName';
export const SAVE_CHANGES_BUTTON = 'changeOperatorName';
export const DELETE_ACCOUNT_SECTION = 'deleteAccountSection';
export const DELETE_ACCOUNT_BUTTON = 'deleteAccount';
export const GROUPS_OVERVIEW = 'groupsOverview';
export const GROUPS_COUNT = 'groupsCount';
export const GROUPS_NUMBER = 'groupsNumber';
export const GROUPS_NAME = 'groupsName';
export const PRIVACY_MANDATORY_LINK_LABEL = 'privacyLinkMandatory';
export const PRIVACY_OPTIONAL_LINK_LABEL = 'privacyLinkOptional';
export const TERMS_LINK_LABEL = 'termsLink';
export const DPA_LINK_LABEL = 'dpaLink';
export const TOMS_LINK_LABEL = 'tomsLink';
export const IN_PROGRESS = 'inProgress';
export const DELETION_REQUESTED = 'deletionRequested';
export const RESTORE_ACCOUNT = 'restoreAccount';
export const CHANGE_PASSWORD = 'changePassword';
export const UPDATE_EMAIL_NOTIFICATION = 'activeEmailChange';
export const RESET_KEY_BUTTON = 'resetPrivateKeyButton';
export const RESET_KEY_INFO = 'resetPrivateKeyInfo';

// Reset Key
export const RESET_KEY_SUBMIT_BUTTON = 'resetPrivateKeySubmitButton';
export const READ_CONFIRMATION_CHECKBOX = 'readConfirmationCheckbox';

export const DIV_ARIA_EXPANDED_TRUE = 'div[aria-expanded="true"]';

// Contact form
export const CONTACT_FORM_MODAL = 'contactFormModal';
export const CONTACT_FORM_OPERATOR_SUPPORT_CODE =
  'contactFormOperatorSupportCode';
export const CONTACT_FORM_OPERATOR_NAME = 'contactFormOperatorName';
export const CONTACT_FORM_OPERATOR_EMAIL = 'contactFormOperatorEmail';
export const CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD = 'contactFormPhoneNumber';
export const CONTACT_FORM_MESSAGE_INPUT_FIELD = 'contactFormMessage';
export const CONTACT_FORM_SEND_BUTTON = 'contactFormSendButton';
export const SUCCESS_NOTIFICATION_MODAL = 'successNotificationModal';

// Aliases
export const ALIAS_CONTACT_FORM = '@contactForm';
export const ALIAS_SCANNER = '@scanner';
export const ALIAS_SCANNER_ID = '@scannerId';
export const ALIAS_CAM_SCANNER = '@camScanner';

// Data Transfers Page
export const DATA_TRANSFERS_SECTION = 'dataTransfers';
export const DATA_TRANSFERS_TITLE = 'dataTransfersTitle';
export const ALIAS_OPERATOR = '@operator';
export const SHARE_NOW_BUTTON = 'next';

// Helpcenter Page
export const HELP_CENTER_TITLE = 'helpCenterTitle';
export const HELPCENTER_MODAL_TRIGGER = 'helpCenterModalTrigger';
export const HELPCENTER_LINKS = 'helpCenterLinks';
export const SUPPORT_VIDEO_LINK_LABEL = 'supportVideoLink';
export const FAQ_LINK_LABEL = 'faqLink';
export const LICENSE_LINK_LABEL = 'licenseLink';
export const TOOLTIP_LINK_LABEL = 'toolkitLink';

// Footer
export const LUCA_VERSION_HEADLINE = 'lucaVersionHeadline';
export const LUCA_VERSION_NUMBER = 'lucaVersionNumber';
export const GITLAB_LINK_LABEL = 'gitLabLink';
export const TERMS_CONDITIONS_LINK_LABEL = 'termsAndConditionsLink';

// Scanner
export const RE_DO_BUTTON = 'span[aria-label=redo]';
export const VIDEO = 'video';

// Locations Settings
export const EDIT_GROUP_NAME = 'editGroupName';
export const STREET_NAME_AND_STREET_NUMBER = 'addressRowStreetNameAndStreetNr';
export const ZIP_CODE_AND_CITY = 'addressRowZipCodeAndCity';

// Checkin Contact Form
export const FIRST_NAME_INPUT_FIELD = 'firstName';
export const LAST_NAME_INPUT_FIELD = 'lastName';
export const STREET_INPUT_FIELD = 'street';
export const STREET_NUM_INPUT_FIELD = 'number';
export const ZIP_INPUT_FIELD = 'zip';
export const CITY_NAME_INPUT_FIELD = 'city';
export const PHONE_NUM_INPUT_FIELD = 'phone';
export const EMAIL_ADDRESS_INPUT_FIELD = 'email';
export const TABLE_NUM_INPUT_FIELD = '#additionalData-table';
export const ACCEPT_AGB_CHECKBOX = 'acceptAGB';
export const CONTACT_FORM_SUBMIT_BUTTON = 'contactSubmit';

// Banner
export const BANNER = 'banner';
export const CLOSE_BANNER_BUTTON = 'closeBanner';
