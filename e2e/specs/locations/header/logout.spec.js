import { createTestOperator } from '../utils/setup';

describe('Logout', () => {
  before(() => {
    Cypress.config('chromeWebSecurity', true);
    createTestOperator();
  });
  it('logs the user out', () => {
    cy.logoutLocations().then(response => {
      expect(response.status).to.eq(204);
    });
  });
});
