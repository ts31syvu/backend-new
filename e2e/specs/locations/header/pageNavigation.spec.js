import { createTestOperator } from '../utils/setup';
import { getGroupPayload } from '../helpers/api/groups';
import { createTestGroup } from '../network/api';
import {
  HELP_CENTER_ROUTE,
  DATA_TRANSFERS_ROUTE,
  PROFILE_ROUTE,
  APP_ROUTE,
} from '../constants/routes';
import { getByCy } from '../utils/selectors';
import { visit } from '../utils/commands';
import { DEFAULT_COOKIE } from '../utils/setup.helper';

import {
  shouldBeVisible,
  urlShouldInclude,
  shouldNotExist,
} from '../utils/assertions';
import {
  OPEN_DATA_REQUESTS_BUTTON,
  GROUPNAME,
  DATA_TRANSFERS_TITLE,
  OPEN_HELPCENTER_BUTTON,
  HELP_CENTER_TITLE,
  PROFILE_DROPDOWN_MENU_TRIGGER,
  OPEN_PROFILE_BUTTON,
  LOGO_HOME,
  LOCATION_DISPLAY_NAME,
  BUSINESS_PROFILE_PREVIEW,
  PERSONAL_PROFILE_PREVIEW,
} from '../constants/selectorKeys';

// base group is decided by group name in ascending alphabet order
const group0Payload = getGroupPayload({ name: 'AAA' });
let group0;

describe('Page navigation', () => {
  before(() => {
    createTestOperator();
    createTestGroup(group0Payload).then(location => {
      group0 = location;
    });
  });

  beforeEach(() => {
    visit(APP_ROUTE);
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  describe('Open different pages', () => {
    it('opens data transfer page', () => {
      getByCy(PROFILE_DROPDOWN_MENU_TRIGGER).click();
      getByCy(OPEN_DATA_REQUESTS_BUTTON).click();
      urlShouldInclude(DATA_TRANSFERS_ROUTE);
      shouldBeVisible(getByCy(DATA_TRANSFERS_TITLE));
    });
    it('opens help center page', () => {
      getByCy(OPEN_HELPCENTER_BUTTON).click();
      urlShouldInclude(HELP_CENTER_ROUTE);
      shouldBeVisible(getByCy(HELP_CENTER_TITLE));
    });
    it('opens profile page', () => {
      getByCy(PROFILE_DROPDOWN_MENU_TRIGGER).click();
      getByCy(OPEN_PROFILE_BUTTON).click();
      urlShouldInclude(PROFILE_ROUTE);
      shouldBeVisible(getByCy(BUSINESS_PROFILE_PREVIEW));
      shouldBeVisible(getByCy(PERSONAL_PROFILE_PREVIEW));
    });
  });

  describe('Luca logo opens general overview page', () => {
    it('opens base group overview from data transfers', () => {
      visit(DATA_TRANSFERS_ROUTE);
      shouldNotExist(getByCy(GROUPNAME));
      getByCy(LOGO_HOME).click();
      urlShouldInclude(APP_ROUTE);
      getByCy(GROUPNAME).contains(group0.groupName);
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
    });
    it('opens base group overview from profile', () => {
      visit(PROFILE_ROUTE);
      shouldNotExist(getByCy(GROUPNAME));
      getByCy(LOGO_HOME).click();
      urlShouldInclude(APP_ROUTE);
      getByCy(GROUPNAME).contains(group0.groupName);
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
    });
  });
});
