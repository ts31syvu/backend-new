import faker from 'faker';
import {
  LOCATION_GROUPS_ROUTE,
  OPERATOR_LOCATIONS_ROUTE,
} from '../../constants/routes';
import {
  E2E_DEFAULT_LOCATION_GROUP,
  E2E_DEFAULT_LOCATION_GROUP_2,
} from '../../constants/locations';
import { STANDARD_PASSWORD } from '../../utils/setup.helper';

export const deleteGroup = (groupId, username) => {
  cy.request({
    method: 'DELETE',
    url: `api/v3/locationGroups/${groupId}`,
    auth: {
      username,
      password: STANDARD_PASSWORD,
    },
    body: {
      username,
      password: STANDARD_PASSWORD,
    },
  });
};

export const deleteArea = (locationId, username) => {
  cy.request({
    method: 'DELETE',
    url: `${OPERATOR_LOCATIONS_ROUTE}/${locationId}`,
    auth: {
      username,
      password: STANDARD_PASSWORD,
    },
    body: {
      password: STANDARD_PASSWORD,
    },
  });
};

export const resetGroups = () => {
  // eslint-disable-next-line require-await
  cy.request('GET', `${LOCATION_GROUPS_ROUTE}/`).then(async response => {
    const deletableGroups = response.body.filter(
      group =>
        group.groupId !== E2E_DEFAULT_LOCATION_GROUP &&
        group.groupId !== E2E_DEFAULT_LOCATION_GROUP_2
    );
    deletableGroups.forEach(group => deleteGroup(group.groupId));
  });
};

export const getGroupPayload = ({
  name = null,
  tableCount = null,
  zipCode = null,
} = {}) => ({
  type: 'restaurant',
  name: name || faker.company.companyName().replace("'", ' '),
  phone: '017612345678',
  streetName: faker.address.streetName().replace("'", ' '),
  streetNr: faker.datatype.number().toString(),
  zipCode: zipCode || faker.address.zipCode('#####'),
  city: faker.address.city().replace("'", ' '),
  isIndoor: true,
  lat: Number(faker.address.latitude()),
  lng: Number(faker.address.longitude()),
  tableCount,
});

export const getAreaPayload = (groupId, locationName = null) => ({
  groupId,
  locationName: locationName || faker.company.companyName().replace("'", ' '),
  type: 'restaurant',
  phone: '017612345678',
  streetName: faker.address.streetName().replace("'", ' '),
  streetNr: faker.datatype.number().toString(),
  zipCode: faker.address.zipCode('#####'),
  city: faker.address.city().replace("'", ' '),
  isIndoor: true,
});
