import { E2E_PASSWORD } from '../../constants/users';

export const resetPassword = currentPassword => {
  cy.request('POST', 'api/v3/operators/password/change', {
    currentPassword,
    newPassword: E2E_PASSWORD,
  });
};

export const requestAccountDeletion = () => {
  return cy.request('DELETE', 'api/v3/operators');
};

export const undoAccountDeletion = () => {
  return cy.request('POST', 'api/v3/operators/restore');
};

// eslint-disable-next-line require-await
export async function getMe() {
  // eslint-disable-next-line require-await
  cy.request('GET', '/api/v4/auth/operator/me').then(async response => {
    cy.wrap(response.body).as('operator');
    return response;
  });
}

export const changeUserName = async (firstName, lastName) => {
  cy.request('PATCH', 'api/v3/operators', {
    firstName,
    lastName,
  });
  await getMe();
};
