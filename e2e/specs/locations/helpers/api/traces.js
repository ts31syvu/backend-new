export const checkInMobileUser = traceDataPayload => {
  cy.request('POST', '/api/v3/traces/checkin', traceDataPayload);
};

export const formCheckIn = ({ formId, traceDataPayload }) => {
  cy.request(
    'POST',
    `/api/v3/forms/${formId}/traces/checkin`,
    traceDataPayload
  );
};
