import {
  FAQ_LINK,
  GITLAB_LINK,
  TERMS_CONDITIONS_LINK,
} from '../../constants/links';
import {
  LUCA_VERSION_HEADLINE,
  LUCA_VERSION_NUMBER,
  FAQ_LINK_LABEL,
  GITLAB_LINK_LABEL,
  TERMS_CONDITIONS_LINK_LABEL,
} from '../../constants/selectorKeys';
import { shouldBeVisible, shouldHaveAttribute } from '../../utils/assertions';
import { verifyLinkOpensInNewTab } from './validations';
import { getByCy } from '../../utils/selectors';

export const checkLinksInFooter = () => {
  it('opens FAQ link link in new tab', () => {
    shouldHaveAttribute(getByCy(FAQ_LINK_LABEL), 'href', FAQ_LINK);
    shouldHaveAttribute(getByCy(FAQ_LINK_LABEL), 'target', '_blank');
    verifyLinkOpensInNewTab(getByCy(FAQ_LINK_LABEL));
  });

  it('opens GitLab link in new tab', () => {
    shouldHaveAttribute(getByCy(GITLAB_LINK_LABEL), 'href', GITLAB_LINK);
    shouldHaveAttribute(getByCy(GITLAB_LINK_LABEL), 'target', '_blank');
    verifyLinkOpensInNewTab(getByCy(GITLAB_LINK_LABEL));
  });

  it('opens terms and conditions link in new tab', () => {
    shouldHaveAttribute(
      getByCy(TERMS_CONDITIONS_LINK_LABEL),
      'href',
      TERMS_CONDITIONS_LINK
    );
    shouldHaveAttribute(
      getByCy(TERMS_CONDITIONS_LINK_LABEL),
      'target',
      '_blank'
    );
    verifyLinkOpensInNewTab(getByCy(TERMS_CONDITIONS_LINK_LABEL));
  });

  it('shows the latest/most recent version of luca Location ', () => {
    shouldBeVisible(getByCy(LUCA_VERSION_HEADLINE));
    shouldBeVisible(getByCy(LUCA_VERSION_NUMBER));
  });
};
