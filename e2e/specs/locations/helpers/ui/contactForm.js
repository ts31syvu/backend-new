import faker from 'faker';
import {
  ANT_FORM_ITEM_ERROR_NOTIFICATION,
  ANT_SUCCESS_NOTIFICATION_MESSAGE,
} from '../../../workflow/constants/selectorKeys';
import { check } from '../../../workflow/utils/commands';
import {
  ACCEPT_AGB_CHECKBOX,
  CITY_NAME_INPUT_FIELD,
  EMAIL_ADDRESS_INPUT_FIELD,
  FIRST_NAME_INPUT_FIELD,
  LAST_NAME_INPUT_FIELD,
  PHONE_NUM_INPUT_FIELD,
  STREET_INPUT_FIELD,
  STREET_NUM_INPUT_FIELD,
  ZIP_INPUT_FIELD,
} from '../../constants/selectorKeys';
import { shouldBeVisible, shouldNotExist } from '../../utils/assertions';
import { type } from '../../utils/commands';
import { getByCy, getDOMElement } from '../../utils/selectors';

export function fillContactForm({
  firstName = faker.name.firstName().replace("'", ' '),
  lastName = faker.name.lastName().replace("'", ' '),
  street = faker.address.streetName().replace("'", ' '),
  houseNumber = faker.datatype.number({
    min: 10,
    max: 50,
  }),
  zip = faker.address.zipCode('#####'),
  city = faker.address.city().replace("'", ' '),
  phoneNumber = faker.phone.phoneNumber('0049176#######'),
  email = faker.internet.email(),
} = {}) {
  type(getByCy(FIRST_NAME_INPUT_FIELD), firstName);
  type(getByCy(LAST_NAME_INPUT_FIELD), lastName);
  type(getByCy(STREET_INPUT_FIELD), street);
  type(getByCy(STREET_NUM_INPUT_FIELD), houseNumber);
  type(getByCy(CITY_NAME_INPUT_FIELD), city);
  type(getByCy(ZIP_INPUT_FIELD), zip);
  type(getByCy(PHONE_NUM_INPUT_FIELD), phoneNumber);
  type(getByCy(EMAIL_ADDRESS_INPUT_FIELD), email);

  return {
    firstName,
    lastName,
    street,
    houseNumber,
    zip,
    city,
    phoneNumber,
    email,
  };
}

export const submitContactForm = () => {
  check(getByCy(ACCEPT_AGB_CHECKBOX));
  getByCy('contactSubmit').click();
  shouldNotExist(getDOMElement(ANT_FORM_ITEM_ERROR_NOTIFICATION));
  shouldBeVisible(getDOMElement(ANT_SUCCESS_NOTIFICATION_MESSAGE));
};
