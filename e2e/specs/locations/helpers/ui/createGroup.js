import { checkRadiusInput } from './validations';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  shouldBeVisible,
  shouldBeDisabled,
  shouldNotBeDisabled,
} from '../../utils/assertions';
import { type } from '../../utils/commands';

import {
  STREET,
  STREET_NO,
  ZIP,
  CITY,
  GROUP_NAME,
  SUBMIT_BUTTON,
  RADIUS,
  TABLE_COUNT_INPUT_FIELD,
  YES_BUTTON,
  CREATE_GROUP,
  INDOOR_SELECTION,
  SELECT_INDOOR,
  AREA_NAME_INPUT,
} from '../../constants/selectorKeys';

export const addressFields = [STREET, STREET_NO, ZIP, CITY];

export const openCreateGroupModal = () => {
  // Modal create group modal
  shouldBeVisible(getByCy(CREATE_GROUP));
  getByCy(CREATE_GROUP).click();
};

export const selectGroupType = groupType => {
  getByCy(groupType).click();
};

export const setGroupName = name => {
  // Enter name
  type(getDOMElement(GROUP_NAME), name);
  // Proceed
  getDOMElement(SUBMIT_BUTTON).click();
};

export const setGroupAddress = address => {
  // Accept Google API
  getByCy(YES_BUTTON).click();
  // Enter location
  getDOMElement('#locationSearch').focus().clear().type(address);
  // Select from googleApi
  getDOMElement('.pac-item', { timeout: 10000 }).should('be.visible');
  cy.contains('.pac-container', address).click();
};

export const setGroupTables = tableCount => {
  // Select tables
  getByCy(YES_BUTTON).click();
  // Enter tables
  type(getDOMElement(TABLE_COUNT_INPUT_FIELD), tableCount);
  // Proceed
  getDOMElement(SUBMIT_BUTTON).click();
};

export const checkForExistingFields = fields =>
  fields.map(field => shouldBeVisible(getDOMElement(field)));

export const checkForDisabledFields = fields =>
  fields.map(field => shouldBeDisabled(getDOMElement(field)));

export const checkForNonDisabledFields = fields =>
  fields.map(field => shouldNotBeDisabled(getDOMElement(field)));

export const setGroupIndoorSelection = () => {
  // Select indoor
  getByCy(INDOOR_SELECTION).click();
  getByCy(SELECT_INDOOR).click();
  // Proceed
  getDOMElement(SUBMIT_BUTTON).click();
};

export const setGroupArea = areaName => {
  // Select more areas
  getByCy(YES_BUTTON).click();
  type(getByCy(AREA_NAME_INPUT), areaName);
  setGroupIndoorSelection();
};

export const setGroupRadius = radius => {
  // Select automatic checkout
  getByCy(YES_BUTTON).click();
  // Enter radius
  checkRadiusInput();
  type(getDOMElement(RADIUS).clear(), radius);
  // Proceed
  getDOMElement(SUBMIT_BUTTON).click();
};
