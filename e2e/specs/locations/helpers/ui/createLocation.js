import { type } from '../../utils/commands';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  SUBMIT_BUTTON,
  NEXT_STEP_BUTTON,
  NO_BUTTON,
  DONE_BUTTON,
  LOCATION_NAME,
  TABLE_COUNT_INPUT_FIELD,
  SELECT_INDOOR,
  INDOOR_SELECTION,
} from '../../constants/selectorKeys';

export const setLocationName = name => {
  type(getDOMElement(LOCATION_NAME), name);
  getByCy(NEXT_STEP_BUTTON).click();
};

export const setLocationIndoorSelection = () => {
  // Select indoor
  getByCy(INDOOR_SELECTION).click();
  getByCy(SELECT_INDOOR).click();
  getDOMElement(SUBMIT_BUTTON).click();
};

export const setLocationTableCount = count => {
  type(getDOMElement(TABLE_COUNT_INPUT_FIELD), count);
  getByCy(NEXT_STEP_BUTTON).click();
};

export const skipLocationDetails = () => {
  getByCy(NEXT_STEP_BUTTON).click();
  getByCy(NEXT_STEP_BUTTON).click();
  setLocationIndoorSelection();
  getByCy(NO_BUTTON).click();
  getByCy(NO_BUTTON).click();
  getByCy(DONE_BUTTON).click();
};
