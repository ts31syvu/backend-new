import { getByCy, getDOMElement } from '../../utils/selectors';
import { type } from '../../utils/commands';
import { STANDARD_PASSWORD } from '../../utils/setup.helper';
import { shouldBeVisible } from '../../utils/assertions';
import {
  ANT_NOTIFICATION_NOTICE_WITH_ICON,
  DELETE_AREA_BUTTON,
  DELETE_LOCATION_CONFIRM_BUTTON,
  OPEN_AREA_SETTINGS_LINK,
  PASSWORD_INPUT_FIELD,
  PROCEED_BUTTON,
  SUCCESS_CHECK_ICON,
} from '../../constants/selectorKeys';

export const deleteLocation = () => {
  getByCy(OPEN_AREA_SETTINGS_LINK).click();
  getByCy(DELETE_AREA_BUTTON).click();
  getByCy(DELETE_LOCATION_CONFIRM_BUTTON).click();
  type(getByCy(PASSWORD_INPUT_FIELD), STANDARD_PASSWORD);
  getByCy(PROCEED_BUTTON).click();
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_WITH_ICON));
  shouldBeVisible(getDOMElement(SUCCESS_CHECK_ICON));
};
