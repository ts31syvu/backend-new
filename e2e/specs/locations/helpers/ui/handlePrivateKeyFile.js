import {
  E2E_LOCATION_PRIVATE_KEY_NAME,
  E2E_LOCATION_PRIVATE_KEY_PATH,
} from '../../constants/users';

import { readFile, fileExists, log } from '../../utils/commands';
import { shouldBeChecked, shouldBeVisible } from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  NEXT_BUTTON,
  INPUT_TYPE_FILE,
  ANT_MODAL_CONTENT,
  DOWNLOAD_PRIVATE_KEY_BUTTON,
  PRIVATE_KEY_DOWNLOADED_CHECKBOX,
  SKIP_PRIVATE_KEY_UPLOAD_BUTTON,
} from '../../constants/selectorKeys';

export const downloadLocationPrivateKeyFile = () => {
  getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
  getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX).check();
  shouldBeChecked(getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX));
  shouldBeVisible(getByCy(NEXT_BUTTON)).click();
};

export const uploadLocationPrivateKeyFile = (filename, name) => {
  readFile(filename).then(fileContent => {
    getDOMElement(INPUT_TYPE_FILE, { timeout: 10000 }).attachFile({
      fileContent,
      mimeType: 'text/plain',
      fileName: name,
    });
  });
};

export const skipLocationPrivateKeyFile = () => {
  getDOMElement(ANT_MODAL_CONTENT).within(() => {
    shouldBeVisible(getByCy(SKIP_PRIVATE_KEY_UPLOAD_BUTTON)).click();
  });
};

export const addLocationPrivateKeyFile = (
  filename = E2E_LOCATION_PRIVATE_KEY_PATH,
  name = E2E_LOCATION_PRIVATE_KEY_NAME
) => {
  fileExists(filename).then(exists => {
    if (!exists) {
      log('Private key should be downloaded');
      downloadLocationPrivateKeyFile();
    } else {
      uploadLocationPrivateKeyFile(filename, name);
    }
  });
};
