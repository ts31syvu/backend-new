import moment from 'moment';

import {
  verifyGuestsCount,
  verifyCheckinGuestTime,
  verifyCheckoutGuestTime,
} from './validations';
import {
  shouldBeVisible,
  shouldContain,
  shouldHaveAttribute,
} from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  SHOW_GUEST_LIST_MODAL_TRIGGER,
  TOTAL_CHECKIN_COUNT,
  ANT_CLOSE_MODAL,
  CHECKOUT_GUEST,
  ANT_PRIMARY_POPOVER_BUTTON,
  CHECKOUT_SUCCESS,
  CHECKOUT_RADIUS_TOGGLE,
  AREA_CHECKED,
  RADIUS_INPUT_FIELD,
  CONTACT_FORM,
  ANT_FORM_HORIZONTAL,
  OPEN_ALL_TABLES_CHECKOUT_CONFIRMATION_BUTTON,
  ANT_POPOVER_CONTENT,
  CUSTOMIZED_CHECKOUT_CARD,
} from '../../constants/selectorKeys';

const CURRENT_DATE = moment().format('DD.MM.YYYY');

export const checkoutGuests = () => {
  getByCy(CHECKOUT_GUEST).click();
  getDOMElement(ANT_PRIMARY_POPOVER_BUTTON).click();
  shouldBeVisible(getDOMElement(CHECKOUT_SUCCESS));
};

export const openAllTablesCheckoutConfirmationModal = () => {
  getByCy(OPEN_ALL_TABLES_CHECKOUT_CONFIRMATION_BUTTON).click();
  shouldBeVisible(getDOMElement(ANT_POPOVER_CONTENT));
};

export const openContactForm = () => {
  shouldBeVisible(getByCy(CONTACT_FORM)).click();
  shouldBeVisible(getDOMElement(ANT_FORM_HORIZONTAL));
};

export const checkGuestInList = () => {
  // Expect the guest count to be 1
  verifyGuestsCount(1);
  // Expect the total checkin count to be 1 within the guest list modal
  getByCy(SHOW_GUEST_LIST_MODAL_TRIGGER).click();
  shouldBeVisible(getByCy(TOTAL_CHECKIN_COUNT));
  shouldContain(getByCy(TOTAL_CHECKIN_COUNT), 1);

  verifyCheckinGuestTime(CURRENT_DATE);
  getDOMElement(ANT_CLOSE_MODAL).click();
  // Check out the guest
  checkoutGuests();
  // Expect the total checkin count to be 1 in the guest list modal
  getByCy(SHOW_GUEST_LIST_MODAL_TRIGGER).click();
  verifyCheckoutGuestTime(CURRENT_DATE);
};

export const setCheckoutRadiusValueToZero = uuid => {
  cy.request('PATCH', `api/v3/operators/locations/${uuid}`, {
    radius: 0,
  });
};

export const activateCheckoutRadiusToggle = () => {
  getByCy(CUSTOMIZED_CHECKOUT_CARD).click();
  // Expect the status to be inactive by default
  shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'false');
  // Activate
  getByCy(CHECKOUT_RADIUS_TOGGLE).click();
  // Expect the switch button to be ative
  shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'true');
  shouldBeVisible(getDOMElement(RADIUS_INPUT_FIELD));
};

export const deactivateCheckoutRadiusToggle = () => {
  getByCy(CHECKOUT_RADIUS_TOGGLE).click();
  shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'false');
};
