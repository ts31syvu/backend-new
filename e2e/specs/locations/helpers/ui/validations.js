import { DEFAULT_LOCATION_NAMES_DE_EN } from '../../constants/locations';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  shouldBeVisible,
  shouldBeDisabled,
  shouldHaveText,
  shouldContain,
} from '../../utils/assertions';
import { type } from '../../utils/commands';
import {
  RADIUS,
  ANT_VALIDATION_ERROR,
  ANT_MODAL_CONTENT,
  ROOT,
  GUEST_COUNT,
  CHECKOUT_GUEST,
  CHECKIN_DATE,
  CHECKIN_TIME,
  CHECKOUT_DATE,
  CHECKOUT_TIME,
  CAM_SCANNER,
  SCANNER,
  CONTACT_FORM,
  SHOW_GUEST_LIST_MODAL_TRIGGER,
  SECTION_ASIDE,
  SECTION_MAIN,
  NEXT_STEP_BUTTON,
  LOCATION_NAME,
  ANT_NOTIFICATION_NOTICE_WITH_ICON,
  SUCCESS_CHECK_ICON,
  ERROR_ICON,
} from '../../constants/selectorKeys';

const TIME_REGEXP = /^\d{2}:\d{2}$/;
const TOO_SMALL_INPUT = 10;
const TOO_BIG_INPUT = 5050;
const SPECIAL_CHAR_INPUT = '#%?@@';
const TEXT_INPUT = 'testing';
const INITIAL_GUEST_COUNT = '0/0';
const REDO_ICON = 'span[aria-label=redo]';

export const checkRadiusInput = () => {
  // Invalid radius input (empty, under 50 or over 5000):
  getDOMElement(RADIUS).clear();
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  type(getDOMElement(RADIUS).clear(), TOO_SMALL_INPUT);
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  type(getDOMElement(RADIUS).clear(), TOO_BIG_INPUT);
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
};

export const checkRadiusInputEdgeCase = () => {
  type(getDOMElement(RADIUS).clear(), SPECIAL_CHAR_INPUT);
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  type(getDOMElement(RADIUS).clear(), TEXT_INPUT);
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
};

export const verifyLocationOverview = () => {
  shouldBeVisible(getByCy(CAM_SCANNER));
  shouldBeVisible(getByCy(SCANNER));
  shouldBeVisible(getByCy(CONTACT_FORM));
  shouldBeVisible(getByCy(GUEST_COUNT));
  shouldBeVisible(getByCy(SHOW_GUEST_LIST_MODAL_TRIGGER));
  shouldBeVisible(getByCy(CHECKOUT_GUEST));
  shouldBeDisabled(getByCy(CHECKOUT_GUEST));
};

export const verifyScannerCounter = groupName => {
  cy.contains(groupName);
  cy.contains(INITIAL_GUEST_COUNT);
  shouldBeVisible(getDOMElement(REDO_ICON));
};

export const verifyLocationHomePage = () => {
  shouldBeVisible(getDOMElement(SECTION_ASIDE));
  shouldBeVisible(getDOMElement(SECTION_MAIN));
};

export const verifyModalWindowIsClosed = () => {
  getDOMElement(ROOT).within($root => {
    // eslint-disable-next-line unicorn/no-array-callback-reference
    expect($root.find(ANT_MODAL_CONTENT).length).to.equal(0);
  });
};

export const verifyGuestsCount = count => {
  getByCy(GUEST_COUNT).next().click();
  shouldContain(getByCy(GUEST_COUNT), count);
};

export const verifyCheckinGuestTime = date => {
  shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
  getDOMElement(ANT_MODAL_CONTENT).within(() => {
    shouldHaveText(getByCy(CHECKIN_DATE), date);
    getByCy(CHECKIN_TIME).contains(TIME_REGEXP);
    shouldHaveText(getByCy(CHECKOUT_DATE), '');
    shouldContain(getByCy(CHECKOUT_TIME), '');
  });
};

export const verifyCheckoutGuestTime = date => {
  shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
  getDOMElement(ANT_MODAL_CONTENT).within(() => {
    shouldHaveText(getByCy(CHECKIN_DATE), date);
    getByCy(CHECKIN_TIME).contains(TIME_REGEXP);
    shouldHaveText(getByCy(CHECKOUT_DATE), date);
    getByCy(CHECKOUT_TIME).contains(TIME_REGEXP);
  });
};

export const verifyLinkOpensInNewTab = element => {
  element.then(link => {
    cy.request(link.prop('href')).its('status').should('eq', 200);
  });
};

export const verifyDefaultLocationHeadline = selector => {
  selector.invoke('text').then(headline => {
    expect(headline).to.be.oneOf(DEFAULT_LOCATION_NAMES_DE_EN);
  });
};

export const defaultLocationNameShouldBeRejected = () => {
  DEFAULT_LOCATION_NAMES_DE_EN.map(defaultName => {
    type(getDOMElement(LOCATION_NAME).clear(), defaultName);
    getByCy(NEXT_STEP_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  });
};

export const checkLocationNameIsUnique = locationName => {
  type(getDOMElement(LOCATION_NAME), locationName);
  getByCy(NEXT_STEP_BUTTON).click();
  shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
};

export const verifyCheckoutSuccess = () => {
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_WITH_ICON));
  shouldBeVisible(getDOMElement(SUCCESS_CHECK_ICON));
};

export const antErrorModalAppears = () => {
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_WITH_ICON));
  shouldBeVisible(getDOMElement(ERROR_ICON));
};
