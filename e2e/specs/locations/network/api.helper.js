import { createTestGroup } from './api';

export const createTestGroups = groupPayloads =>
  Cypress.Promise.all(
    groupPayloads.map(groupPayload => createTestGroup(groupPayload))
  );
