import { OPERATOR_LOCATIONS_ROUTE } from '../constants/routes';
import { getAreaPayload } from '../helpers/api/groups';

const { baseUrl } = Cypress.config();

// eslint-disable-next-line require-await
export async function createOperator(operatorValues) {
  cy.request('POST', '/api/v3/operators', operatorValues).then(
    // eslint-disable-next-line require-await
    async response => {
      // eslint-disable-next-line require-await
      cy.wrap(response.body).as('newOperator');
      return response;
    }
  );
}

// eslint-disable-next-line require-await
export async function storePublicKey(publicKey) {
  cy.request('POST', '/api/v3/operators/publicKey', publicKey).then(
    response => response
  );
}

// eslint-disable-next-line require-await
export async function activateOperator(user) {
  cy.request({
    method: 'POST',
    url: `${baseUrl}/api/internal/end2end/activateOperator`,
    headers: {
      'internal-access-authorization': 'bHVjYTpBOTNrcE01em1DdHZ2dEhO',
    },
    body: user,
  }).then(response => response);
}

export const regenerateBloomFilter = () => {
  cy.request({
    method: 'POST',
    url: `${baseUrl}/api/internal/jobs/regenerateBloomFilter`,
    headers: {
      'internal-access-authorization': 'bHVjYTpBOTNrcE01em1DdHZ2dEhO',
    },
  });
};

// eslint-disable-next-line require-await
export async function login(credentials) {
  cy.request({
    method: 'POST',
    url: 'api/v4/auth/operator/login',
    failOnStatusCode: false,
    body: credentials,
    headers: {
      origin: baseUrl,
    },
  });
}

export const createTestGroup = groupValues => {
  return new Cypress.Promise(resolve => {
    // eslint-disable-next-line require-await
    cy.request('POST', 'api/v3/locationGroups', groupValues).then(
      // eslint-disable-next-line require-await
      async response => {
        const group = response.body;
        // eslint-disable-next-line promise/no-nesting
        cy.request(
          'GET',
          `api/v3/operators/locations/${group.location.locationId}`
        ).then(
          // eslint-disable-next-line require-await, no-shadow
          async response => {
            const location = response.body;
            resolve(location);
          }
        );
      }
    );
  });
};

export const createTestArea = (groupId, locationName) => {
  return new Cypress.Promise(resolve => {
    cy.request(
      'POST',
      OPERATOR_LOCATIONS_ROUTE,
      getAreaPayload(groupId, locationName)
      // eslint-disable-next-line require-await
    ).then(async response => {
      const newArea = response.body;
      resolve(newArea);
    });
  });
};

export const createTestAreas = areaPayloads =>
  Cypress.Promise.all(
    areaPayloads.map(areaPayload => {
      return new Cypress.Promise(resolve => {
        cy.request(
          'POST',
          OPERATOR_LOCATIONS_ROUTE,
          areaPayload
          // eslint-disable-next-line require-await
        ).then(async response => {
          const newArea = response.body;
          resolve(newArea);
        });
      });
    })
  );
