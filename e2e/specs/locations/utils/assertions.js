export const shouldBeVisible = element => element.should('be.visible');

export const shouldNotExist = element => element.should('not.exist');

export const shouldContain = (element, value) =>
  element.should('contain', value);

export const shouldNotContain = (element, value) =>
  element.should('not.contain', value);

export const contains = value => cy.contains(value);

export const urlShouldInclude = path => cy.url().should('include', path);

export const shouldBeDisabled = element => element.should('be.disabled');

export const shouldNotBeDisabled = element => element.should('not.be.disabled');

export const shouldHaveValue = (element, value) =>
  element.should('have.value', value);

export const shouldNotHaveValue = (element, value) =>
  element.should('not.have.value', value);

export const shouldHaveText = (element, string) =>
  element.should('have.text', string);

export const shouldBeEmpty = element => element.should('be.empty');

export const shouldHaveLength = (element, number) => {
  element.should('have.length', number);
};

export const shouldHaveAttribute = (element, attribute, attributeValue) =>
  element.should('have.attr', attribute, attributeValue);

export const shouldHaveHref = (element, hrefValue) =>
  element.should('have.attr', 'href').and('contain', hrefValue);

export const shouldBeCalled = element => element.should('be.called');

export const shouldBeChecked = element => element.should('be.checked');
