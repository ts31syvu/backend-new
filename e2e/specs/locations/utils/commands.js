export const type = (element, value, delay = 0) =>
  element.type(value, { delay });

export const visit = url => cy.visit(url);

export const waitForAlias = (alias, timeout = 5000) =>
  cy.wait(alias, { timeout });

export const window = () => cy.window();

export const log = message => cy.log(message);

export const fileExists = file => cy.task('fileExists', file);

export const deleteFileIfExists = file => cy.task('deleteFileIfExists', file);

export const readFile = (file, timeout = 4000) =>
  cy.readFile(file, { timeout });

export const setViewport = (width, height) => cy.viewport(width, height);

export const triggerMouseover = element => element.trigger('mouseover');

export const triggerEvent = (element, event) => {
  element.trigger(event);
};
