const getQrCodePrefix = hasTable =>
  hasTable ? './downloads/luca_QRCodes_' : './downloads/luca_QRCode_';
const getQrCodeEnding = hasTable => (hasTable ? '_Tables.pdf' : '.pdf');

// format according to file generator
const formatName = name => name.replaceAll(' ', '_');

export const getQrCodeFilePathPdf = ({
  locationName,
  isArea = false,
  areaName = '',
  hasTable,
}) =>
  isArea
    ? `${getQrCodePrefix(hasTable)}${formatName(locationName)}_${formatName(
        areaName
      )}${getQrCodeEnding(hasTable)}`
    : `${getQrCodePrefix(hasTable)}${formatName(locationName)}${getQrCodeEnding(
        hasTable
      )}`;
