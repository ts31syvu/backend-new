// Profile page download files
export const PRIVACY_DOWNLOAD_FILEPATH = './downloads/DataPrivacy_luca.pdf';
export const DPA_DOWNLOAD_FILEPATH = './downloads/DPA_luca.pdf';
export const TOMS_DOWNLOAD_FILEPATH = './downloads/TOMS_luca.pdf';

// Get file name and path for operator
export const getOperatorKeyFile = (firstName, lastName) => ({
  name: `luca_locations_${firstName}_${lastName}_privateKey.luca`,
  path: `./downloads/luca_locations_${firstName}_${lastName}_privateKey.luca`,
});
