export const getQRCodeFilePath = (location, isTableEnabled, format) => {
  const locationName = `_${location.name}`;
  const fileName = `${location.groupName.replace(' ', '_')}${
    location.name ? locationName.replace(new RegExp(' ', 'g'), '_') : ''
  }`;
  return isTableEnabled
    ? `./downloads/luca_QRCodes_${fileName}_Tables.${format}`
    : `./downloads/luca_QRCode_${fileName}.${format}`;
};
