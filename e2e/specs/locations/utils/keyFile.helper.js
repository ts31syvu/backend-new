export const getKeyFileName = (firstName, lastName) =>
  `luca_locations_${firstName}_${lastName}_privateKey.luca`;

export const getKeyFilePath = (firstName, lastName) =>
  `./downloads/luca_locations_${firstName}_${lastName}_privateKey.luca`;
