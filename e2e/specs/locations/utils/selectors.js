export const getByCy = key => cy.getByCy(key);

export const getDOMElement = (key, timeout = 4000) => cy.get(key, { timeout });
