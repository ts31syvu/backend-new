import faker from 'faker';
import { NEW_E2E_PHONE } from '../constants/inputs';

// The password is used for testing purpose only
export const STANDARD_PASSWORD = '5@4vKJDJwvWBFi4R';

export const createOperatorValues = () => ({
  firstName: faker.name.firstName().replace("'", ' '),
  lastName: faker.name.lastName().replace("'", ' '),
  email: faker.internet.email(),
  password: STANDARD_PASSWORD,
  agreement: true,
  avvAccepted: true,
  lastVersionSeen: '1.0.0',
  lang: 'de',
  phone: NEW_E2E_PHONE,
  businessEntityName: 'Nexenio',
  businessEntityCity: 'Berlin',
  businessEntityStreetName: 'Charlottenstraße',
  businessEntityStreetNumber: '59',
  businessEntityZipCode: '10117',
});

export const DEFAULT_COOKIE = '__Secure-connect.sid';
