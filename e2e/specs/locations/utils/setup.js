import { EC_KEYPAIR_GENERATE, hexToBase64 } from '@lucaapp/crypto';
import { STANDARD_PASSWORD, createOperatorValues } from './setup.helper';
import { APP_ROUTE } from '../constants/routes';
import {
  createOperator,
  storePublicKey,
  login,
  activateOperator,
} from '../network/api';
import {
  downloadLocationPrivateKeyFile,
  uploadLocationPrivateKeyFile,
} from '../helpers/ui/handlePrivateKeyFile';
import { getByCy } from './selectors';
import { visit } from './commands';
import { loginHealthDepartment } from '../../health-department/helpers/api/auth';
import { addHealthDepartmentPrivateKeyFile } from '../../health-department/helpers/ui/handlePrivateKeyFile';
import { signHealthDepartment } from '../../health-department/helpers/api/signHealthDepartment';
import { KEY_ROTATION } from '../../health-department/constants/aliases';
import { removeHDPrivateKeyFile } from '../../workflow/helpers/functions';

export const getMe = () => {
  return new Cypress.Promise(resolve => {
    // eslint-disable-next-line require-await
    cy.request('GET', '/api/v4/auth/operator/me').then(async response => {
      const me = response.body;
      resolve(me);
    });
  });
};

export const createTestOperator = async (
  withPrivateKey = false,
  route = APP_ROUTE
) => {
  cy.window().then(win => {
    win.sessionStorage.removeItem('PRIVATE_KEY_MODAL_SEEN');
  });
  // Create a new operator
  const newOperator = createOperatorValues();
  createOperator(newOperator);
  // Activate account
  activateOperator({
    email: newOperator.email,
  });
  // Login
  login({ username: newOperator.email, password: STANDARD_PASSWORD });
  // Get operator
  const testOperator = await getMe();

  if (withPrivateKey) {
    visit(route);
    downloadLocationPrivateKeyFile();
    const KEY_PATH = `./downloads/luca_locations_${testOperator.firstName}_${testOperator.lastName}_privateKey.luca`;
    const KEY_NAME = `luca_locations_${testOperator.firstName}_${testOperator.lastName}_privateKey.luca`;
    uploadLocationPrivateKeyFile(KEY_PATH, KEY_NAME);
    getByCy('finish', 4000).click();

    return { ...testOperator, keyName: KEY_NAME };
  }
  // Create key pair
  const keyPair = EC_KEYPAIR_GENERATE();
  // Store public key in order to not download it on every test
  storePublicKey({ publicKey: hexToBase64(keyPair.publicKey) });
  visit(route);
  return {
    ...testOperator,
    publicKey: hexToBase64(keyPair.publicKey),
  };
};

export const createBasicOperator = () => {
  // Create a new operator
  const newOperator = createOperatorValues();
  createOperator(newOperator);
  // Activate account
  activateOperator({
    email: newOperator.email,
  });
  return newOperator;
};

export const setupSignedHdWithKeyRoation = () => {
  cy.intercept('POST', '/api/v4/keys/daily/rotate').as(KEY_ROTATION);
  removeHDPrivateKeyFile();
  loginHealthDepartment();
  addHealthDepartmentPrivateKeyFile();
  signHealthDepartment();
  cy.reload();
  addHealthDepartmentPrivateKeyFile();
  cy.wait(`@${KEY_ROTATION}`);
  cy.logoutHD();
};
