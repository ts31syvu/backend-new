import { createTestOperator } from '../../utils/setup';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestGroup, createTestArea } from '../../network/api';
import { visit, type } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { DEFAULT_COOKIE, STANDARD_PASSWORD } from '../../utils/setup.helper';
import { getByCy, getDOMElement } from '../../utils/selectors';

import {
  shouldNotExist,
  shouldContain,
  shouldBeVisible,
} from '../../utils/assertions';
import {
  OPEN_AREA_SETTINGS_LINK,
  DELETE_AREA_BUTTON,
  AREA_NAME_DISPLAY,
  ANT_PRIMARY_POPOVER_BUTTON,
  ANT_POPOVER_BUTTONS,
  SUCCESS_DELETE_NOTIFICATION,
  LOCATION_PREFIX,
  PASSWORD,
  SUBMIT_BUTTON,
} from '../../constants/selectorKeys';

let testGroup;
let testGroupArea;

describe('Delete area', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
      // eslint-disable-next-line promise/no-nesting
      createTestArea(testGroup.groupId).then(area => {
        testGroupArea = area;
      });
    });
  });
  beforeEach(() => {
    visit(APP_ROUTE);
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  it('cannot delete the base location', () => {
    getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
    shouldNotExist(getByCy(DELETE_AREA_BUTTON));
  });
  it('can delete a non-default location', () => {
    getByCy(LOCATION_PREFIX + testGroupArea.name).click();
    shouldContain(getByCy(AREA_NAME_DISPLAY), testGroupArea.name);
    // Cancel delete area process
    getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
    shouldBeVisible(getByCy(DELETE_AREA_BUTTON)).click();
    shouldBeVisible(getDOMElement(ANT_POPOVER_BUTTONS)).first().click();
    // Delete the area
    getByCy(DELETE_AREA_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_POPOVER_BUTTONS));
    getDOMElement(ANT_PRIMARY_POPOVER_BUTTON).click();
    // Enter password
    type(getDOMElement(PASSWORD).focus(), STANDARD_PASSWORD);
    getDOMElement(SUBMIT_BUTTON).click();
    // Success notification
    shouldBeVisible(getDOMElement(SUCCESS_DELETE_NOTIFICATION));
    // Expect that the area doesn't exist in the link menu
    shouldNotExist(getByCy(LOCATION_PREFIX + testGroupArea.name));
  });
});
