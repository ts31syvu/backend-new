import faker from 'faker';
import {
  shouldBeDisabled,
  shouldBeVisible,
  shouldNotExist,
  shouldHaveValue,
} from '../../utils/assertions';
import {
  ANT_NOTIFICATION,
  STREET_NAME_INPUT_FIELD,
  STREET_NUMBER_INPUT_FIELD,
  ZIP_CODE_INPUT_FIELD,
  CITY_INPUT_FIELD,
  YES_BUTTON,
  NO_BUTTON,
  EDIT_ADDRESS_BUTTON,
  ANT_MODAL_CONTENT,
  SAVE_ADRESS_BUTTON,
  OPEN_AREA_CHECKIN_LINK,
  CUSTOMIZED_CHECKOUT_CARD,
  CHECKOUT_RADIUS_TOGGLE,
  RADIUS_INPUT_FIELD,
  LOCATION_SEARCH_INPUT_FIELD,
  GOOGLE_SEARCH_DROPDOWN_ENTRY,
} from '../../constants/selectorKeys';

import { getDOMElement, getByCy } from '../../utils/selectors';
import { type } from '../../utils/commands';

export const openEditingModal = enableGoogle => {
  getByCy(EDIT_ADDRESS_BUTTON).click();
  shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
  getByCy(enableGoogle ? YES_BUTTON : NO_BUTTON).click();
};

export const saveAddress = () => {
  getByCy(SAVE_ADRESS_BUTTON).click();
  shouldBeVisible(getDOMElement(ANT_NOTIFICATION));
};

export const changeAddressManually = () => {
  const street = faker.address.streetName().replace("'", ' ');
  const streetNumber = faker.datatype.number({
    min: 10,
    max: 50,
  });
  const zipCode = faker.address.zipCode('#####');
  const city = faker.address.city().replace("'", ' ');

  type(getDOMElement(STREET_NAME_INPUT_FIELD), street);
  type(getDOMElement(STREET_NUMBER_INPUT_FIELD), streetNumber);
  type(getDOMElement(ZIP_CODE_INPUT_FIELD), zipCode);
  type(getDOMElement(CITY_INPUT_FIELD), city);
};

export const checkRadiusExists = shouldExist => {
  if (shouldExist) {
    getDOMElement(OPEN_AREA_CHECKIN_LINK).click();
    shouldBeVisible(getByCy(CUSTOMIZED_CHECKOUT_CARD)).click();
    getByCy(CHECKOUT_RADIUS_TOGGLE).click();
    shouldBeVisible(getDOMElement(RADIUS_INPUT_FIELD));
    shouldHaveValue(getDOMElement(RADIUS_INPUT_FIELD), 50);
  } else {
    getDOMElement(OPEN_AREA_CHECKIN_LINK).click();
    shouldBeVisible(getByCy(CUSTOMIZED_CHECKOUT_CARD));
    shouldNotExist(getByCy(CHECKOUT_RADIUS_TOGGLE));
  }
};

export const changeAddressViaGoogleApi = RESTAURANT_ADDRESS => {
  // Enter location
  type(getDOMElement(LOCATION_SEARCH_INPUT_FIELD), RESTAURANT_ADDRESS, 5);

  // Select from Google Api
  shouldBeVisible(getDOMElement(GOOGLE_SEARCH_DROPDOWN_ENTRY, 5000)).click();
  // Expect fields to be filled out and disabled
  shouldBeVisible(getDOMElement(STREET_NAME_INPUT_FIELD));
  shouldBeVisible(getDOMElement(STREET_NUMBER_INPUT_FIELD));
  shouldBeVisible(getDOMElement(ZIP_CODE_INPUT_FIELD));
  shouldBeVisible(getDOMElement(CITY_INPUT_FIELD));
  shouldBeDisabled(getDOMElement(STREET_NAME_INPUT_FIELD));
  shouldBeDisabled(getDOMElement(STREET_NUMBER_INPUT_FIELD));
  shouldBeDisabled(getDOMElement(ZIP_CODE_INPUT_FIELD));
  shouldBeDisabled(getDOMElement(CITY_INPUT_FIELD));
};
