import { createTestOperator } from '../../utils/setup';
import { createTestArea, createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { APP_ROUTE } from '../../constants/routes';
import { visit } from '../../utils/commands';
import { shouldContain } from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  AREA_NAME_DISPLAY,
  OPEN_AREA_SETTINGS_LINK,
  OPEN_AREA_CHECKIN_LINK,
  AREA_PREFIX,
} from '../../constants/selectorKeys';
import {
  changeAddressManually,
  checkRadiusExists,
  openEditingModal,
  saveAddress,
} from './editAddressArea.helper';

let testGroup;
let testArea;

describe('Updating the address for an area', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
      // eslint-disable-next-line promise/no-nesting
      createTestArea(testGroup.groupId).then(area => {
        testArea = area;
      });
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    getByCy(AREA_PREFIX + testArea.name).click();
    shouldContain(getByCy(AREA_NAME_DISPLAY), testArea.name);
  });

  context('Updating the address manually', () => {
    it('can update address manually without affecting radius checker', () => {
      checkRadiusExists();
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      openEditingModal();
      changeAddressManually();
      saveAddress();
      getDOMElement(OPEN_AREA_CHECKIN_LINK).click();
      checkRadiusExists();
    });
  });
});
