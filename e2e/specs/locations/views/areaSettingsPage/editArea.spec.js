import { createTestOperator } from '../../utils/setup';
import { getAreaPayload, getGroupPayload } from '../../helpers/api/groups';
import { createTestGroup, createTestAreas } from '../../network/api';
import { APP_ROUTE } from '../../constants/routes';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  EDIT_SUCCESS_MESSAGE,
  LOCATION_NAME_INPUT_FIELD,
  OPEN_AREA_SETTINGS_LINK,
  PHONE_INPUT_FIELD,
  SAVE_CHANGES_AREA_BUTTON,
  AREA_PREFIX,
  LOCATION_DISPLAY_NAME,
  ANT_VALIDATION_ERROR,
} from '../../constants/selectorKeys';
import {
  TEST_LOCATION_NAME,
  TEST_NUMBER,
  DEFAULT_LOCATION_NAMES_DE_EN,
} from '../../constants/locations';
import {
  shouldBeVisible,
  shouldBeDisabled,
  shouldHaveValue,
  shouldContain,
} from '../../utils/assertions';
import { type, visit } from '../../utils/commands';
import { verifyDefaultLocationHeadline } from '../../helpers/ui/validations';

let testGroup;
let testArea0;
let testArea1;

describe('Location settings', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });

  describe('Edit the base location', () => {
    beforeEach(() => {
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
      verifyDefaultLocationHeadline(getByCy(LOCATION_DISPLAY_NAME));
    });
    it('has disabled save button if there is no change', () => {
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      shouldBeDisabled(getByCy(SAVE_CHANGES_AREA_BUTTON));
    });
    it('has disabled name input', () => {
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      shouldBeDisabled(getDOMElement(LOCATION_NAME_INPUT_FIELD));
    });
    it('can change the phone number of the base location', () => {
      // Open the location settings
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      shouldHaveValue(getDOMElement(PHONE_INPUT_FIELD), testGroup.phone);
      type(getDOMElement(PHONE_INPUT_FIELD).clear(), TEST_NUMBER);
      // Save the changes
      getByCy(SAVE_CHANGES_AREA_BUTTON).click();
      // Success notification
      shouldBeVisible(getDOMElement(EDIT_SUCCESS_MESSAGE));
      shouldHaveValue(getDOMElement(PHONE_INPUT_FIELD), TEST_NUMBER);
    });
  });
  describe('Edit other locations', () => {
    before(() => {
      createTestAreas([
        getAreaPayload(testGroup.groupId),
        getAreaPayload(testGroup.groupId),
      ]).then(areas => {
        [testArea0, testArea1] = areas;
      });
    });

    it('has disabled save button if there is no change', () => {
      // Click the other location
      getByCy(AREA_PREFIX + testArea0.name).click();
      shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea0.name);
      // Open the location settings
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      shouldBeDisabled(getByCy(SAVE_CHANGES_AREA_BUTTON));
    });

    it('checks if the location name is the default name', () => {
      getByCy(AREA_PREFIX + testArea0.name).click();
      shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea0.name);
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      // verify default location name is rejected in english and german
      DEFAULT_LOCATION_NAMES_DE_EN.map(defaultName => {
        type(getDOMElement(LOCATION_NAME_INPUT_FIELD).clear(), defaultName);
        getByCy(SAVE_CHANGES_AREA_BUTTON).click();
        shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      });
    });

    it('checks if the location name is unique', () => {
      getByCy(AREA_PREFIX + testArea0.name).click();
      shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea0.name);
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      type(getDOMElement(LOCATION_NAME_INPUT_FIELD).clear(), testArea1.name);
      getByCy(SAVE_CHANGES_AREA_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    });

    it('can change both the name and phone number of the location', () => {
      // Click the other location
      getByCy(AREA_PREFIX + testArea0.name).click();
      shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea0.name);
      // Open the location settings
      getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
      type(
        getDOMElement(LOCATION_NAME_INPUT_FIELD).clear(),
        TEST_LOCATION_NAME
      );
      type(getDOMElement(PHONE_INPUT_FIELD).clear(), TEST_NUMBER);
      // Save the changes
      getByCy(SAVE_CHANGES_AREA_BUTTON).click();
      // Success notification
      shouldBeVisible(getDOMElement(EDIT_SUCCESS_MESSAGE));
      shouldHaveValue(
        getDOMElement(LOCATION_NAME_INPUT_FIELD),
        TEST_LOCATION_NAME
      );
      shouldHaveValue(getDOMElement(PHONE_INPUT_FIELD), TEST_NUMBER);
    });
  });
});
