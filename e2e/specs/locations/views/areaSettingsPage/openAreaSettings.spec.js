import { createTestOperator } from '../../utils/setup';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestArea, createTestGroup } from '../../network/api';
import { visit } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { shouldBeVisible, shouldContain } from '../../utils/assertions';
import {
  AREA_NAME_DISPLAY,
  OPEN_AREA_SETTINGS_LINK,
  AREA_PREFIX,
  AREA_PROFILE_DISPLAY,
} from '../../constants/selectorKeys';

let testArea;

describe('Location overview - show location profile link', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      // eslint-disable-next-line promise/no-nesting
      createTestArea(group.groupId).then(area => {
        testArea = area;
      });
      visit(APP_ROUTE);
    });
  });
  it('opens the area settings page', () => {
    getByCy(AREA_PREFIX + testArea.name).click();
    shouldContain(getByCy(AREA_NAME_DISPLAY), testArea.name);
    getDOMElement(OPEN_AREA_SETTINGS_LINK).click();
    shouldBeVisible(getByCy(AREA_PROFILE_DISPLAY));
  });
});
