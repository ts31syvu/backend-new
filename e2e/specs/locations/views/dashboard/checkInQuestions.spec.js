import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  LOCATION_CARD_PREFIX,
  ADD_REQUESTS_BUTTON,
  REMOVE_ADDITIONAL_DATA_BUTTON,
  ANT_INPUT_FIELD,
} from '../../constants/selectorKeys';
import { shouldBeVisible } from '../../utils/assertions';
import { type, visit } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';

const CHECKIN_QUESTIONS = 'additionalData';
const TEST_DATA = 'Test Data';

describe('Location overview Check-in questions card', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload());
  });

  beforeEach(() => {
    visit(APP_ROUTE);
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  it('has no additional data', () => {
    getByCy(LOCATION_CARD_PREFIX + CHECKIN_QUESTIONS).click();
    shouldBeVisible(getByCy(ADD_REQUESTS_BUTTON));
  });
  it('adds two additional data keys and can remove it', () => {
    getByCy(LOCATION_CARD_PREFIX + CHECKIN_QUESTIONS).click();
    shouldBeVisible(getByCy(ADD_REQUESTS_BUTTON));
    getByCy(ADD_REQUESTS_BUTTON).click();
    type(getDOMElement(ANT_INPUT_FIELD), TEST_DATA);
    // remove additional data
    visit(APP_ROUTE);
    getByCy(LOCATION_CARD_PREFIX + CHECKIN_QUESTIONS).click();
    getByCy(REMOVE_ADDITIONAL_DATA_BUTTON).click();
    shouldBeVisible(getByCy(ADD_REQUESTS_BUTTON));
  });
});
