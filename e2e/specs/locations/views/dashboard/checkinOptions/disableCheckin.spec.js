import { removeHDPrivateKeyFile } from '../../../../workflow/helpers/functions';
import { APP_ROUTE } from '../../../constants/routes';
import { CLOSE_BANNER_BUTTON } from '../../../constants/selectorKeys';
import { getGroupPayload } from '../../../helpers/api/groups';
import { createTestGroup } from '../../../network/api';
import { visit } from '../../../utils/commands';
import { getByCy } from '../../../utils/selectors';
import {
  createTestOperator,
  setupSignedHdWithKeyRoation,
} from '../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../utils/setup.helper';
import { loginLocations } from '../../../helpers/api/auth';

describe('Missing daily key', () => {
  before(() => {
    removeHDPrivateKeyFile();
    createTestOperator();
    createTestGroup(getGroupPayload());
  });

  after(() => {
    removeHDPrivateKeyFile();
  });

  describe('without daily key', () => {
    beforeEach(() => {
      visit(APP_ROUTE);
      Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    });

    it('displays banner on missing daily key on dashboard only', () => {
      getByCy('banner').should('be.visible');
    });
    it('shows disabled contact options', () => {
      getByCy('disabledTabletIcon').should('be.visible');
      getByCy('disabledHandScannerIcon').should('be.visible');
      getByCy('disabledContactFormIcon').should('be.visible');
    });

    it('can close banner on missing daily key', () => {
      getByCy(CLOSE_BANNER_BUTTON).click();
      getByCy('banner').should('not.exist');
    });
  });

  describe('with daily key', () => {
    it('doesnt show banner when daily key is created', () => {
      setupSignedHdWithKeyRoation();
      loginLocations();
      getByCy('tabletIcon').should('be.visible');
      getByCy('handScannerIcon').should('be.visible');
      getByCy('contactFormIcon').should('be.visible');
    });
  });
});
