import { createTestOperator } from '../../utils/setup';
import { createTestArea, createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { E2E_DEFAULT_LOCATION_NAME_EN } from '../../constants/locations';
import {
  checkRadiusInput,
  checkRadiusInputEdgeCase,
} from '../../helpers/ui/validations';
import {
  setCheckoutRadiusValueToZero,
  activateCheckoutRadiusToggle,
  deactivateCheckoutRadiusToggle,
} from '../../helpers/ui/locationOverview';
import { APP_ROUTE } from '../../constants/routes';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { type, visit } from '../../utils/commands';
import {
  AREA_CHECKED,
  AREA_PREFIX,
  LOCATION_CARD_PREFIX,
  CHECKOUT_RADIUS_SETTINGS,
  CHECKOUT_RADIUS_TOGGLE,
  RADIUS_INPUT_FIELD,
  ANT_INPUT_NUMBER_HANDLER_UP,
  ANT_INPUT_NUMBER_HANDLER_DOWN,
  DIV_ARIA_EXPANDED_TRUE,
  ANT_COLLAPSE_ITEM_ACTIVE,
} from '../../constants/selectorKeys';
import { shouldHaveAttribute, shouldNotExist } from '../../utils/assertions';

let testArea;

describe('Checkout radius configuration', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      // eslint-disable-next-line promise/no-nesting
      createTestArea(group.groupId).then(area => {
        testArea = area;
      });
    });
  });

  beforeEach(() => {
    visit(APP_ROUTE);
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    setCheckoutRadiusValueToZero(testArea.uuid);
    activateCheckoutRadiusToggle();
  });

  it('cannot accept invalid inputs', () => {
    // Invalid radius input: empty, under 50 or over 5000
    checkRadiusInput();
    // Invalid radius input: letters and special characters
    checkRadiusInputEdgeCase();
    deactivateCheckoutRadiusToggle();
  });

  it('sets the invalid number to the minimum or maximum', () => {
    // Under 50
    type(getDOMElement(RADIUS_INPUT_FIELD).clear(), '-20').blur();
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '50');
    // Check if the radius number is persisted by re-opening the radius configuration
    getByCy(AREA_PREFIX + testArea.name).click();
    getByCy(AREA_PREFIX + E2E_DEFAULT_LOCATION_NAME_EN).click();
    getByCy(LOCATION_CARD_PREFIX + CHECKOUT_RADIUS_SETTINGS).click();
    // Expect the status to be active after re-opening
    shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'true');
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '50');
    // Over 5000
    type(getDOMElement(RADIUS_INPUT_FIELD).clear(), '6000').blur();
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '5000');
    // Check if the radius number is persisted by re-opening the radius configuration
    getByCy(AREA_PREFIX + testArea.name).click();
    getByCy(AREA_PREFIX + E2E_DEFAULT_LOCATION_NAME_EN).click();
    getByCy(LOCATION_CARD_PREFIX + CHECKOUT_RADIUS_SETTINGS).click();
    // Expect the status to be active after re-opening
    shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'true');
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '5000');
    deactivateCheckoutRadiusToggle();
  });

  it('can change the checkout radius', { retries: 3 }, () => {
    type(getDOMElement(RADIUS_INPUT_FIELD).clear(), '100').blur();
    getDOMElement(RADIUS_INPUT_FIELD, 10000).invoke('val').should('eq', '100');
    // Increase and lower the radius by clicking on arrows
    getDOMElement(ANT_INPUT_NUMBER_HANDLER_UP).click();
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '101');
    getDOMElement(ANT_INPUT_NUMBER_HANDLER_DOWN).click();
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '100');
    // Check if the radius number is persisted by re-opening the radius configuration
    getByCy(AREA_PREFIX + testArea.name).click();
    getByCy(AREA_PREFIX + E2E_DEFAULT_LOCATION_NAME_EN).click();
    getByCy(LOCATION_CARD_PREFIX + CHECKOUT_RADIUS_SETTINGS).click();
    // Expect the status to be active after re-opening
    shouldHaveAttribute(getByCy(CHECKOUT_RADIUS_TOGGLE), AREA_CHECKED, 'true');
    getDOMElement(RADIUS_INPUT_FIELD).invoke('val').should('eq', '100');
    // Close the modal
    getDOMElement(DIV_ARIA_EXPANDED_TRUE).click();
    shouldNotExist(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
  });
});
