import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import {
  deleteFileIfExists,
  readFile,
  visit,
  triggerEvent,
} from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  ANT_COLLAPSE_ITEM_ACTIVE,
  AREA_CHECKED,
  DIV_ARIA_EXPANDED_TRUE,
  LOCATION_GENERATE_QR_CODES,
  LOCATION_TABLE_SUBDEVISION,
  QR_CODE_FOR_ENTIRE_AREA_SECTION,
  QR_CODE_FOR_ENTIRE_AREA_TOGGLE,
  TABLE_SUBDIVISION_TOGGLE,
  QR_CODE_COMPATABILITY_SECTION,
  QR_CODE_COMPATABILITY_TOGGLE,
  QR_CODE_COMPATABILITY_INFO_ICON,
  ANT_TOOLTIP_OPEN,
  QR_CODES_CSV_INFO_ICON,
  QR_PRINT_SECTION,
  QR_PRINT_LINK,
  QR_CODES_FOR_TABLES_SECTION,
  QR_CODES_FOR_TABLES_TOGGLE,
  QR_CODE_DOWNLOAD_BUTTON_CSV,
  QR_CODE_DOWNLOAD_BUTTON_PDF,
} from '../../constants/selectorKeys';
import {
  shouldBeDisabled,
  shouldBeVisible,
  shouldHaveAttribute,
  shouldNotBeDisabled,
  shouldNotExist,
} from '../../utils/assertions';
import { getQRCodeFilePath } from '../../utils/generateQrCodes.helper';
import { verifyLinkOpensInNewTab } from '../../helpers/ui/validations';

let testGroup;
let qrCodeEntireAreaPdf;
let qrCodeEntireAreaCsv;
let qrCodesTablesPdf;
let qrCodesTablesCsv;

describe('Dashboard Overview/Generate QR codes card', { retries: 2 }, () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload({ name: 'AAA' })).then(group => {
      testGroup = group;
      qrCodeEntireAreaPdf = getQRCodeFilePath(testGroup, false, 'pdf');
      qrCodeEntireAreaCsv = getQRCodeFilePath(testGroup, false, 'csv');
      qrCodesTablesPdf = getQRCodeFilePath(testGroup, true, 'pdf');
      qrCodesTablesCsv = getQRCodeFilePath(testGroup, true, 'csv');
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    // open Generate QR codes collapsible
    getByCy(LOCATION_GENERATE_QR_CODES).click();
    shouldBeVisible(getByCy(QR_CODE_FOR_ENTIRE_AREA_SECTION));
  });

  it('should deactivate/activate "Qr code for entire area" toggle', () => {
    // Expect 'One QR code for the entre area' toggle to be activated by default
    shouldBeVisible(getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE));
    shouldHaveAttribute(
      getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
      AREA_CHECKED,
      'true'
    );
    // When toggle is activated, expect 'Download qr codes' button to be clickable
    shouldNotBeDisabled(getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF));
    // Deactivate toggle
    getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE).click();
    // When toggle is de-activated, expect 'Download qr codes' button to be unclickable
    shouldBeDisabled(getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF));
  });

  it('should deactivate/activate "Corona-Warn-App compatability" toggle', () => {
    shouldBeVisible(getByCy(QR_CODE_COMPATABILITY_SECTION));
    // Expect 'Corona-Warn-App compatability' toggle to be active be default
    shouldHaveAttribute(
      getByCy(QR_CODE_COMPATABILITY_TOGGLE),
      AREA_CHECKED,
      'true'
    );
    // Deactivate toggle
    getByCy(QR_CODE_COMPATABILITY_TOGGLE).click();
    shouldHaveAttribute(
      getByCy(QR_CODE_COMPATABILITY_TOGGLE),
      AREA_CHECKED,
      'false'
    );
  });

  it('should display information tooltip when hovering over qr code compatability information icon', () => {
    shouldBeVisible(getByCy(QR_CODE_COMPATABILITY_INFO_ICON));
    triggerEvent(getByCy(QR_CODE_COMPATABILITY_INFO_ICON), 'mouseover');
    shouldBeVisible(getDOMElement(ANT_TOOLTIP_OPEN));
  });

  it('should download qr code as PDF file when clicking on "Create qr-codes" button', () => {
    getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
    readFile(qrCodeEntireAreaPdf, 10000);
    deleteFileIfExists(qrCodeEntireAreaPdf);
  });

  it('should download qr code as CSV file when clicking on "Download as CSV" link', () => {
    getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
    readFile(qrCodeEntireAreaCsv, 10000);
    deleteFileIfExists(qrCodeEntireAreaCsv);
  });

  it('should display information tooltip when hovering over CSV information icon', () => {
    shouldBeVisible(getByCy(QR_CODES_CSV_INFO_ICON));
    triggerEvent(getByCy(QR_CODES_CSV_INFO_ICON), 'mouseover');
    shouldBeVisible(getDOMElement(ANT_TOOLTIP_OPEN));
  });

  it('should open website of "qr-print" in new tab', () => {
    shouldBeVisible(getByCy(QR_PRINT_SECTION));
    shouldHaveAttribute(getByCy(QR_PRINT_LINK), 'target', '_blank');
    getByCy(QR_PRINT_LINK).click();
    verifyLinkOpensInNewTab(getByCy(QR_PRINT_LINK));
  });

  it('opens and closes collapsible upon clicking on location card', () => {
    // opened collapsible should be visible
    shouldBeVisible(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
    // opened collapsible should be closed
    getDOMElement(DIV_ARIA_EXPANDED_TRUE).click();
    shouldNotExist(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
  });

  context('If location is divided into tables', () => {
    before(() => {
      // Activate table subdivision
      getByCy(LOCATION_TABLE_SUBDEVISION).click();
      getByCy(TABLE_SUBDIVISION_TOGGLE).click();
    });

    it('should display a new section with activated toggle by default', () => {
      // Qr codes for tables should not be activated by default
      shouldBeVisible(getByCy(QR_CODES_FOR_TABLES_SECTION));
      shouldHaveAttribute(
        getByCy(QR_CODES_FOR_TABLES_TOGGLE),
        AREA_CHECKED,
        'true'
      );
      // qr code for entire area should now be de-activated
      shouldHaveAttribute(
        getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
        AREA_CHECKED,
        'false'
      );
    });

    it('should automatically deactivate "Qr codes for tables" toggle when "Qr code for entire area" toggle is activated and vice versa', () => {
      // Activate qrCodeForEntireAreaToggle
      getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE).click();
      // qrCodesForTablesToggle should automatically be deactivated
      shouldHaveAttribute(
        getByCy(QR_CODES_FOR_TABLES_TOGGLE),
        AREA_CHECKED,
        'false'
      );
      // Activate qrCodesForTablesToggle
      getByCy(QR_CODES_FOR_TABLES_TOGGLE).click();
      // qrCodeForEntireAreaToggle should automatically be deactivated
      shouldHaveAttribute(
        getByCy(QR_CODE_FOR_ENTIRE_AREA_TOGGLE),
        AREA_CHECKED,
        'false'
      );
    });

    it('should download qr codes for all tables as PDF file when clicking on "Create qr-codes" button', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_PDF).click();
      readFile(qrCodesTablesPdf, 10000);
      deleteFileIfExists(qrCodesTablesPdf);
    });

    it('should download qr codes for all tables as CSV file when clicking on "Download as CSV" link', () => {
      getByCy(QR_CODE_DOWNLOAD_BUTTON_CSV).click();
      readFile(qrCodesTablesCsv, 10000);
      deleteFileIfExists(qrCodesTablesCsv);
    });
  });
});
