import { GET_RANDOM_BYTES, hexToBase64 } from '@lucaapp/crypto';
import { deleteTraceById } from '../../utils/databaseQueries';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestOperator } from '../../utils/setup';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { getByCy } from '../../utils/selectors';
import { shouldContain } from '../../utils/assertions';
import { visit } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import {
  SHOW_GUEST_LIST_MODAL_TRIGGER,
  TOTAL_CHECKIN_COUNT,
} from '../../constants/selectorKeys';

import {
  dynamicTraceDataPayload,
  DYNAMIC_DEVICE_TYPES,
  formTraceDataPayload,
} from '../../constants/payloads';

import { loginHealthDepartment } from '../../../health-department/helpers/api/auth';
import { signHealthDepartment } from '../../../health-department/helpers/api/signHealthDepartment';
import { checkGuestInList } from '../../helpers/ui/locationOverview';
import { checkInMobileUser, formCheckIn } from '../../helpers/api/traces';
import { verifyGuestsCount } from '../../helpers/ui/validations';
import { createTestGroup } from '../../network/api';

let testGroup;
let testOperator;
const TEST_DYNAMIC_TRACE_ID = hexToBase64(GET_RANDOM_BYTES(16));
const TEST_FORM_TRACE_ID = hexToBase64(GET_RANDOM_BYTES(16));

describe('Locations / Location overview', { retries: 2 }, () => {
  before(() => {
    loginHealthDepartment();
    signHealthDepartment();
    visit(APP_ROUTE);
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });

  describe('When viewing a new location', () => {
    it('displays no checked-in guests by default', () => {
      verifyGuestsCount(0);
      getByCy(SHOW_GUEST_LIST_MODAL_TRIGGER).click();
      shouldContain(getByCy(TOTAL_CHECKIN_COUNT), 0);
    });
  });

  describe('Guest count and the tracking time change', () => {
    it('when check-in/check-out location with contact form', () => {
      formCheckIn({
        formId: testGroup.formId,
        traceDataPayload: formTraceDataPayload({
          traceId: TEST_FORM_TRACE_ID,
          publicKey: testOperator.publicKey,
          scannerId: testGroup.scannerId,
          locationId: testGroup.uuid,
        }),
      });
      checkGuestInList();
      cy.executeQuery(deleteTraceById(TEST_FORM_TRACE_ID));
    });

    it('when check-in/check-out location with camera scanner', () => {
      checkInMobileUser(
        dynamicTraceDataPayload({
          scannerId: testGroup.scannerId,
          deviceType: DYNAMIC_DEVICE_TYPES.DEVICE_TYPE_WEBAPP,
          traceId: TEST_DYNAMIC_TRACE_ID,
          publicKey: testOperator.publicKey,
        })
      );
      checkGuestInList();
      cy.executeQuery(deleteTraceById(TEST_DYNAMIC_TRACE_ID));
    });
  });
});
