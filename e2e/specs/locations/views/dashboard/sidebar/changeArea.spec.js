import { createTestOperator } from '../../../utils/setup';
import { createTestGroup, createTestAreas } from '../../../network/api';
import { getGroupPayload, getAreaPayload } from '../../../helpers/api/groups';
import { visit } from '../../../utils/commands';
import { APP_ROUTE } from '../../../constants/routes';
import { getByCy } from '../../../utils/selectors';
import { shouldContain } from '../../../utils/assertions';
import {
  AREA_PREFIX,
  LOCATION_DISPLAY_NAME,
} from '../../../constants/selectorKeys';
import { E2E_DEFAULT_LOCATION_NAME_EN } from '../../../constants/locations';

const groupPayload = getGroupPayload();
let testGroup;
// areas are displayed by name in ascending alphabetical order
let testArea0;
let testArea1;

describe('Change area', () => {
  before(() => {
    createTestOperator();
    createTestGroup(groupPayload).then(group => {
      testGroup = group;
      // eslint-disable-next-line promise/no-nesting
      createTestAreas([
        getAreaPayload(testGroup.groupId, 'AAA'),
        getAreaPayload(testGroup.groupId, 'BBB'),
      ]).then(areas => {
        [testArea0, testArea1] = areas;
      });
    });
    visit(APP_ROUTE);
  });

  it('changes the area settings overview when clicking on different areas', () => {
    shouldContain(getByCy(LOCATION_DISPLAY_NAME), E2E_DEFAULT_LOCATION_NAME_EN);
    getByCy(AREA_PREFIX + testArea0.name).click();
    shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea0.name);

    getByCy(AREA_PREFIX + testArea1.name).click();
    shouldContain(getByCy(LOCATION_DISPLAY_NAME), testArea1.name);

    getByCy(AREA_PREFIX + E2E_DEFAULT_LOCATION_NAME_EN).click();
    shouldContain(getByCy(LOCATION_DISPLAY_NAME), E2E_DEFAULT_LOCATION_NAME_EN);
  });
});
