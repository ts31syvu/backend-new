import { createTestOperator } from '../../../utils/setup';
import { createTestGroups } from '../../../network/api.helper';
import { getGroupPayload } from '../../../helpers/api/groups';
import { APP_ROUTE } from '../../../constants/routes';
import {
  GROUPNAME,
  GROUP_SETTINGS_HEADER,
  GROUP_ITEM,
} from '../../../constants/selectorKeys';
import { visit } from '../../../utils/commands';
import { getByCy } from '../../../utils/selectors';
import { shouldBeVisible, shouldContain } from '../../../utils/assertions';

describe('Change location dropdown menu', () => {
  context('when searchable select is not present', () => {
    let testGroups = [];
    before(() => {
      createTestOperator();
      createTestGroups([
        getGroupPayload({ name: 'AAA' }),
        getGroupPayload({ name: 'BBB' }),
      ]).then(groups => {
        testGroups = groups;
      });
      visit(APP_ROUTE);
    });

    it('can change location', () => {
      shouldContain(getByCy(GROUPNAME), testGroups[0].groupName);
      shouldBeVisible(getByCy(`${GROUP_ITEM}-${testGroups[0].groupId}`));
      shouldBeVisible(getByCy(`${GROUP_ITEM}-${testGroups[1].groupId}`));
      getByCy(`${GROUP_ITEM}-${testGroups[1].groupId}`).click();
      shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroups[1].groupName);
      getByCy(`${GROUP_ITEM}-${testGroups[0].groupId}`).click();
      shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroups[0].groupName);
    });
  });
});
