import { createTestOperator } from '../../../utils/setup';
import { createTestGroup } from '../../../network/api';
import { getGroupPayload } from '../../../helpers/api/groups';
import { APP_ROUTE } from '../../../constants/routes';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import {
  ANT_CLOSE_MODAL,
  ANT_MODAL_CONTENT,
  CREATE_AREA_MODAL_DESCRIPTION,
  CREATE_AREA_MODAL_HEADER,
  CREATE_LOCATION_PREFIX,
} from '../../../constants/selectorKeys';
import { shouldBeVisible, shouldNotExist } from '../../../utils/assertions';

let testGroup;

describe('Create area button', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });
  it('opens the modal to create a new location area', () => {
    cy.url().should('include', APP_ROUTE);
    getByCy(`${CREATE_LOCATION_PREFIX}${testGroup.groupId}`).click();
    shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
    shouldBeVisible(getByCy(CREATE_AREA_MODAL_HEADER));
    shouldBeVisible(getByCy(CREATE_AREA_MODAL_DESCRIPTION));
    getDOMElement(ANT_CLOSE_MODAL).click();
    shouldNotExist(getDOMElement(ANT_MODAL_CONTENT));
  });
});
