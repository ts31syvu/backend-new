import { BASE_TYPE } from '../../../../../constants/locations';
import {
  checkLocationNameIsUnique,
  defaultLocationNameShouldBeRejected,
} from '../../../../../helpers/ui/validations';
import { deleteLocationByName } from '../../../../../utils/databaseQueries';
import { createTestArea, createTestGroup } from '../../../../../network/api';
import { createTestOperator } from '../../../../../utils/setup';
import { getGroupPayload } from '../../../../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getByCy } from '../../../../../utils/selectors';

let testGroup;
const TEST_NAME = 'Unique area name';

describe('Area name validation', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });

  it('checks if the area name cannot be the default name', () => {
    getByCy(`createLocation-${testGroup.groupId}`).click();
    getByCy(BASE_TYPE).click();
    defaultLocationNameShouldBeRejected();
  });

  it('checks if the area name is unique', () => {
    cy.executeQuery(deleteLocationByName(TEST_NAME));
    createTestArea(testGroup.groupId, TEST_NAME).then(body => {
      cy.wrap(body.name).as('areaName');
    });
    getByCy(`createLocation-${testGroup.groupId}`).click();
    getByCy(BASE_TYPE).click();

    cy.get('@areaName').then(areaName => {
      checkLocationNameIsUnique(areaName);
    });
  });
});
