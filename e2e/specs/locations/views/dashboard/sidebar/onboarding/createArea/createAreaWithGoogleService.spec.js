import { deleteLocationByName } from '../../../../../utils/databaseQueries';
import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import { BUILDING_TYPE } from '../../../../../constants/locations';
import {
  NEXT_STEP_BUTTON,
  STEP_COUNT,
} from '../../../../../constants/selectorKeys';
import {
  AREA_TYPE,
  AVERAGE_CHECKIN_TIME,
  AVERAGE_CHECKIN_TIME_MINUTES,
  checkAreaDetailsSelection,
  checkRoomSizeValueOnDashboard,
  ENTRY_POLICY_INFO,
  MASKS,
  RADIUS,
  ROOM_DEPTH,
  ROOM_HEIGHT,
  ROOM_WIDTH,
  TABLE_COUNT,
  typeRoomSize,
  VENTILATION,
} from '../helpers';
import { createTestOperator } from '../../../../../utils/setup';
import { createTestGroup } from '../../../../../network/api';
import { getGroupPayload } from '../../../../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getAreaAttributes } from '../../../../../utils/createAttributes';

let testOperator;
let testGroup;
const testArea1 = getAreaAttributes();

describe('Create area with google service', { retries: 2 }, () => {
  before(() => {
    cy.executeQuery(deleteLocationByName(testArea1.name));
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
    cy.intercept('POST', 'api/v3/operators/locations').as('createLocation');
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });

  it('creates a new building area with google service and customised inputs', () => {
    cy.executeQuery(deleteLocationByName(testArea1.name));
    getByCy(`createLocation-${testGroup.groupId}`).click();
    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 8');
    getByCy(BUILDING_TYPE).click();

    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 8');
    getDOMElement('#locationName').focus().type(testArea1.name);

    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(STEP_COUNT).should('contain', '3 / 8');
    getDOMElement('#phone').should('have.value', testOperator.phone);
    getDOMElement('#phone').focus().clear().type('017686862728');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(STEP_COUNT).should('contain', '4 / 8');
    getByCy('toggleGoogleService').click();
    getDOMElement('#locationSearch').should('be.visible');
    getByCy(NEXT_STEP_BUTTON).should('be.disabled');

    getDOMElement('#locationSearch').focus().type('Charlottenstraße');
    getDOMElement('.pac-item', { timeout: 10000 }).should('be.visible');
    cy.contains('.pac-container', 'Charlottenstraße').click();

    // Error message expected if google autocomplete doesn't fill in all input fields
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');

    getDOMElement('#locationSearch').focus().clear().type('nexenio');
    getDOMElement('.pac-item', { timeout: 10000 }).should('be.visible');
    cy.contains('.pac-container', 'neXenio').click();

    getByCy('googlemapAddressPreview').should('be.visible');
    getByCy('googlemapAddressPreview').should(
      'contain',
      'Charlottenstraße 59',
      '10117 Berlin'
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(STEP_COUNT).should('contain', '5 / 8');
    getByCy('toggleAutomaticCheckout').should('not.be.checked');
    getByCy('toggleAutomaticCheckout').click();
    getDOMElement('#radius').focus().clear().type(RADIUS);
    getByCy('toggleAutomaticCheckinTime').click();
    getDOMElement('#averageCheckinTime').click({ force: true });
    getDOMElement('.ant-picker-time-panel-cell').contains('02').click();
    getDOMElement('.ant-picker-time-panel-cell').contains('30').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy(STEP_COUNT).should('contain', '6 / 8');
    getByCy('selectAreaType').click();
    getByCy(AREA_TYPE.selectedValue).click();
    getByCy('selectMasks').click();
    getByCy(MASKS.selectedValue).click();
    getByCy('selectEntryPolicyInfo').click();
    getByCy(ENTRY_POLICY_INFO.selectedValue).click();
    getByCy('selectVentilation').click();
    getByCy(VENTILATION.selectedValue).click();

    typeRoomSize();

    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy(STEP_COUNT).should('contain', '7 / 8');
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy('toggleTableInput').click();
    getDOMElement('#tableCount').focus().type(TABLE_COUNT);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Finish and QR code(s) download
    cy.wait('@createLocation')
      .its('response')
      .should('deep.include', {
        statusCode: 201,
      })
      .and('have.property', 'body')
      .should('deep.include', {
        name: testArea1.name,
        phone: '+49 176 86862728',
        streetName: 'Charlottenstraße',
        streetNr: '59',
        zipCode: '10117',
        city: 'Berlin',
        state: 'Berlin',
        radius: RADIUS,
        tableCount: TABLE_COUNT,
        isIndoor: true,
        type: BUILDING_TYPE,
        averageCheckinTime: AVERAGE_CHECKIN_TIME_MINUTES,
        entryPolicyInfo: ENTRY_POLICY_INFO.selectedValue,
        ventilation: VENTILATION.selectedValue,
        masks: MASKS.selectedValue,
        roomHeight: ROOM_HEIGHT,
        roomWidth: ROOM_WIDTH,
        roomDepth: ROOM_DEPTH,
      });

    getByCy(STEP_COUNT).should('contain', '8 / 8');
    getByCy('downloadQRCode').should('be.visible');
    getByCy('done').should('be.visible');
    getByCy('done').click();

    // check created area
    getByCy('locationDisplayName').should('be.visible');
    getByCy('locationDisplayName').should('contain.text', testArea1.name);
    // check checkout values
    getByCy('locationCard-checkoutRadius').click();
    getDOMElement('#radius').should('have.value', RADIUS);
    getDOMElement('#averageCheckinTime').should(
      'have.value',
      AVERAGE_CHECKIN_TIME
    );
    // check area details
    getByCy('locationCard-areaDetails').click();
    checkAreaDetailsSelection();
    checkRoomSizeValueOnDashboard();
    // check table input
    getByCy('locationCard-tableSubdivision').click();
    getDOMElement('#tableCount').should('have.value', TABLE_COUNT);
  });
});
