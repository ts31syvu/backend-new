import { deleteLocationByName } from '../../../../../utils/databaseQueries';
import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import {
  areaTypesShouldBeVisible,
  checkOptionalStickerExist,
  roomSizeShouldBeDisabeld,
  roomSizeShouldNotBeDisabled,
  typeManualAddress,
} from '../helpers';
import { BASE_TYPE } from '../../../../../constants/locations';
import {
  NEXT_STEP_BUTTON,
  STEP_COUNT,
} from '../../../../../constants/selectorKeys';
import { createTestOperator } from '../../../../../utils/setup';
import { createTestGroup } from '../../../../../network/api';
import { getGroupPayload } from '../../../../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getAreaAttributes } from '../../../../../utils/createAttributes';

let testOperator;
let testGroup;
const testArea = getAreaAttributes();

describe('Create area with manual address input', { retries: 1 }, () => {
  before(() => {
    cy.executeQuery(deleteLocationByName(testArea.name));
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    cy.intercept('POST', 'api/v3/operators/locations').as('createLocation');
    visit(APP_ROUTE);
  });

  it('creates a new base area with manual address input', () => {
    getByCy(`createLocation-${testGroup.groupId}`).click();
    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 8');
    areaTypesShouldBeVisible();
    getByCy(BASE_TYPE).click();

    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 8');
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');
    cy.executeQuery(deleteLocationByName(testArea.name));
    getDOMElement('#locationName').type(testArea.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(STEP_COUNT).should('contain', '3 / 8');
    getDOMElement('#phone').should('have.value', testOperator.phone);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(STEP_COUNT).should('contain', '4 / 8');
    getByCy('toggleGoogleService').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    getByCy('toggleUseBusinessAddress').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('exist');
    typeManualAddress(
      testGroup.streetName,
      testGroup.streetNr,
      testGroup.zipCode,
      testGroup.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(STEP_COUNT).should('contain', '5 / 8');
    getByCy('toggleAutomaticCheckout').should('be.disabled');
    getByCy('toggleAutomaticCheckinTime').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy(STEP_COUNT).should('contain', '6 / 8');
    getByCy('entryPolicyInfoTooltip').should('be.visible');

    checkOptionalStickerExist();

    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();

    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');

    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getDOMElement('#ventilation').should('not.be.disabled');
    roomSizeShouldNotBeDisabled();
    // select outdoor
    getByCy('selectAreaType').click();
    getByCy('outdoor').click();
    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy(STEP_COUNT).should('contain', '7 / 8');
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Finish and QR code(s) download
    cy.wait('@createLocation')
      .its('response')
      .should('deep.include', {
        statusCode: 201,
      })
      .and('have.property', 'body');
    getByCy(STEP_COUNT).should('contain', '8 / 8');
    getByCy('downloadQRCode').should('be.visible');
    getByCy('done').should('be.visible');
    getByCy('done').click();
    getByCy('locationDisplayName').should('be.visible');
    getByCy('locationDisplayName').should('contain.text', testArea.name);
    // check checkout values
    getByCy('locationCard-checkoutRadius').click();
    getByCy('activateCheckoutRadius').should('not.exist');
    getByCy('activateAutomaticCheckinTime').should('not.be.checked');
    // check area details
    getByCy('locationCard-areaDetails').click();
    getByCy('dashboard-areaSelection').should('contain', 'Outdoor');
    getByCy('dashboard-medicalMasksSelection').should('contain', 'Select');
    getByCy('dashboard-entryPolicyInfoSelection').should('contain', 'Select');
    getDOMElement('#ventilation').should('be.disabled');
    getDOMElement('#roomWidth-input').should('not.contain.value');
    getDOMElement('#roomDepth-input').should('not.contain.value');
    getDOMElement('#roomHeight-input').should('not.contain.value');
    // check table input
    getByCy('locationCard-tableSubdivision').click();
    getByCy('activateTableSubdivision').should('not.be.checked');
  });
});
