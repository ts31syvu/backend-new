import { createTestOperator } from '../../../../../utils/setup';
import { getGroupPayload } from '../../../../../helpers/api/groups';
import { createTestGroup } from '../../../../../network/api';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import { RESTAURANT_TYPE } from '../../../../../constants/locations';
import {
  deleteFileIfExists,
  readFile,
  visit,
} from '../../../../../utils/commands';
import { getAreaAttributes } from '../../../../../utils/createAttributes';
import { getQrCodeFilePathPdf } from '../../../../../utils/downloadQrCodePdf.helper';
import { TABLE_COUNT, typeManualAddress } from '../helpers';
import { deleteLocationByName } from '../../../../../utils/databaseQueries';
import { NEXT_STEP_BUTTON } from '../../../../../constants/selectorKeys';

let testGroup;

const testArea1 = getAreaAttributes('Building area');
const testArea2 = getAreaAttributes('Restaurant area');

const TEST_AREA_1_QR_CODE_PATH = getQrCodeFilePathPdf({
  locationName: 'Nexenio group',
  isArea: true,
  areaName: testArea1.name,
  hasTable: true,
});

const TEST_AREA_2_QR_CODE_PATH = getQrCodeFilePathPdf({
  locationName: 'Nexenio group',
  isArea: true,
  areaName: testArea2.name,
  hasTable: false,
});

describe('Download QR Codes PDF on area creation', { retries: 2 }, () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload({ name: 'Nexenio group' })).then(group => {
      testGroup = group;
    });
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    cy.intercept('POST', 'api/v3/operators/locations').as('createLocation');
    visit(APP_ROUTE);
  });

  it('downloads the area qr-code PDF with table count', () => {
    cy.executeQuery(deleteLocationByName(testArea1.name));
    getByCy(`createLocation-${testGroup.groupId}`).click();

    // Step 1: Select group type
    getByCy(RESTAURANT_TYPE).click();

    // Step 2: Group name input
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('#locationName').type(testArea1.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    typeManualAddress(
      testGroup.streetName,
      testGroup.streetNr,
      testGroup.zipCode,
      testGroup.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy('toggleTableInput').click();
    getDOMElement('#tableCount').focus().type(TABLE_COUNT);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Finish and QR codes download
    cy.wait('@createLocation').its('response.statusCode').should('eq', 201);
    getByCy('downloadQRCode').should('be.visible');
    getByCy('downloadQRCode').should('contain.text', 'Generate QR Codes');
    getByCy('done').should('be.visible');
    getByCy('downloadQRCode').click();

    readFile(TEST_AREA_1_QR_CODE_PATH, 30000);
    deleteFileIfExists(TEST_AREA_1_QR_CODE_PATH);
  });

  it('downloads the area qr-code PDF without table count', () => {
    cy.executeQuery(deleteLocationByName(testArea2.name));
    getByCy(`createLocation-${testGroup.groupId}`).click();
    // Step 1: Select group type
    getByCy(RESTAURANT_TYPE).click();

    // Step 2: Group name input
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('#locationName').type(testArea2.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    typeManualAddress(
      testGroup.streetName,
      testGroup.streetNr,
      testGroup.zipCode,
      testGroup.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Finish and QR codes download
    cy.wait('@createLocation').its('response.statusCode').should('eq', 201);
    getByCy('downloadQRCode').should('be.visible');
    getByCy('downloadQRCode').should('contain.text', 'Generate QR Code');
    getByCy('done').should('be.visible');
    getByCy('downloadQRCode').click();

    readFile(TEST_AREA_2_QR_CODE_PATH, 30000);
    deleteFileIfExists(TEST_AREA_2_QR_CODE_PATH);
  });
});
