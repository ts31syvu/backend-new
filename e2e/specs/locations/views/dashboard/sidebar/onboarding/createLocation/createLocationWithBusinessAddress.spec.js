import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import {
  checkAddressPrefill,
  checkOptionalStickerExist,
  locationTypesShouldBeVisible,
  roomSizeShouldBeDisabeld,
  roomSizeShouldNotBeDisabled,
} from '../helpers';
import { RESTAURANT_TYPE } from '../../../../../constants/locations';
import { createTestOperator } from '../../../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import {
  CREATE_GROUP,
  NEXT_STEP_BUTTON,
  STEP_COUNT,
} from '../../../../../constants/selectorKeys';
import { getGroupAttributes } from '../../../../../utils/createAttributes';

let testOperator;
const testGroup = getGroupAttributes('Nexenio restaurant');

describe('Create location with business address', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    cy.intercept('POST', 'api/v3/locationGroups').as('createGroup');
    visit(APP_ROUTE);
  });

  it('creates a new restaurant with business address', () => {
    getByCy(CREATE_GROUP).first().click();

    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 9');
    locationTypesShouldBeVisible();
    getByCy(RESTAURANT_TYPE).click();

    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 9');
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');
    getDOMElement('#groupName').type(testGroup.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(STEP_COUNT).should('contain', '3 / 9');
    getDOMElement('#phone').should('have.value', testOperator.phone);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(STEP_COUNT).should('contain', '4 / 9');
    getByCy('toggleGoogleService').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    getByCy('toggleUseBusinessAddress').should('not.be.checked');
    getByCy('toggleUseBusinessAddress').click();
    checkAddressPrefill(null, testOperator);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(STEP_COUNT).should('contain', '5 / 9');
    getByCy('toggleAutomaticCheckout').should('be.disabled');
    getByCy('toggleAutomaticCheckinTime').should('not.be.checked');
    getByCy('toggleAutomaticCheckinTime').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy(STEP_COUNT).should('contain', '6 / 9');
    getByCy('entryPolicyInfoTooltip').should('be.visible');

    checkOptionalStickerExist();

    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();

    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');

    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getDOMElement('#ventilation').should('not.be.disabled');
    roomSizeShouldNotBeDisabled();
    // select outdoor
    getByCy('selectAreaType').click();
    getByCy('outdoor').click();
    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy(STEP_COUNT).should('contain', '7 / 9');
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Area division
    getByCy(STEP_COUNT).should('contain', '8 / 9');
    getByCy('toggleAreaDivision').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 9: Finish and QR code(s) download
    cy.wait('@createGroup').its('response.statusCode').should('eq', 201);
    getByCy(STEP_COUNT).should('contain', '9 / 9');
    getByCy('downloadQRCode').should('be.visible');
    getByCy('done').should('be.visible');
    getByCy('done').click();
    getByCy('groupName').should('be.visible');
    getByCy('groupName').should('contain.text', testGroup.name);
  });
});
