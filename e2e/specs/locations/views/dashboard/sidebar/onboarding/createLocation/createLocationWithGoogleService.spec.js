import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import {
  E2E_DEFAULT_LOCATION_NAME_EN,
  NURSING_HOME_TYPE,
} from '../../../../../constants/locations';
import {
  AREA_TYPE,
  AVERAGE_CHECKIN_TIME,
  checkAreaDetailsSelection,
  checkRoomSizeValueOnDashboard,
  ENTRY_POLICY_INFO,
  MASKS,
  RADIUS,
  TABLE_COUNT,
  typeRoomSize,
  VENTILATION,
} from '../helpers';
import { createTestOperator } from '../../../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import {
  CREATE_GROUP,
  NEXT_STEP_BUTTON,
  STEP_COUNT,
} from '../../../../../constants/selectorKeys';
import { getGroupAttributes } from '../../../../../utils/createAttributes';

const testGroup = getGroupAttributes('Nexenio nursing home');
const AREA_1 = 'Area_1';
const AREA_2 = 'Area_2';

describe('Create location with google service', { retries: 2 }, () => {
  before(() => {
    createTestOperator();
    cy.intercept('POST', 'api/v3/locationGroups').as('createGroup');
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });

  it('creates a new nursing home with google service and customised inputs', () => {
    getByCy(CREATE_GROUP).first().click();
    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 9');
    getByCy(NURSING_HOME_TYPE).click();

    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 9');
    getDOMElement('#groupName').focus().type(testGroup.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(STEP_COUNT).should('contain', '3 / 9');
    getDOMElement('#phone').focus().clear().type('017686862728');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(STEP_COUNT).should('contain', '4 / 9');
    getByCy('toggleGoogleService').click();
    getDOMElement('#locationSearch').should('be.visible');
    getByCy(NEXT_STEP_BUTTON).should('be.disabled');

    getDOMElement('#locationSearch').focus().type('Charlottenstraße');
    getDOMElement('.pac-item', { timeout: 10000 }).should('be.visible');
    cy.contains('.pac-container', 'Charlottenstraße').click();

    // Error message expected if google autocomplete doesn't fill in all input fields
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');

    getDOMElement('#locationSearch').focus().clear().type('nexenio');
    getDOMElement('.pac-item', { timeout: 10000 }).should('be.visible');
    cy.contains('.pac-container', 'neXenio').click();

    getByCy('googlemapAddressPreview').should('be.visible');
    getByCy('googlemapAddressPreview').should(
      'contain',
      'Charlottenstraße 59',
      '10117 Berlin'
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(STEP_COUNT).should('contain', '5 / 9');
    getByCy('toggleAutomaticCheckout').click();
    getDOMElement('#radius').focus().clear().type(RADIUS);
    getByCy('toggleAutomaticCheckinTime').click();
    getDOMElement('#averageCheckinTime').click();
    getDOMElement('.ant-picker-time-panel-cell').contains('02').click();
    getDOMElement('.ant-picker-time-panel-cell').contains('30').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy(STEP_COUNT).should('contain', '6 / 9');
    getByCy('selectAreaType').click();
    getByCy(AREA_TYPE.selectedValue).click();
    getByCy('selectMasks').click();
    getByCy(MASKS.selectedValue).click();
    getByCy('selectEntryPolicyInfo').click();
    getByCy(ENTRY_POLICY_INFO.selectedValue).click();
    getByCy('selectVentilation').click();
    getByCy(VENTILATION.selectedValue).click();

    typeRoomSize();

    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy(STEP_COUNT).should('contain', '7 / 9');
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy('toggleTableInput').click();
    getDOMElement('#tableCount').focus().type(TABLE_COUNT);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Area division
    getByCy(STEP_COUNT).should('contain', '8 / 9');
    getByCy('toggleAreaDivision').should('not.be.checked');
    getByCy('toggleAreaDivision').click();
    getDOMElement('#areaName0').focus().type(AREA_1);
    getByCy('areaType0').click();
    getByCy('outdoor').click();
    getByCy('addMoreArea').click();
    getDOMElement('#areaName1').focus().type(AREA_2);
    getByCy('areaType1').click();
    getByCy('indoor').last().click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 9: Finish and QR code(s) download
    cy.wait('@createGroup').its('response.statusCode').should('eq', 201);
    getByCy(STEP_COUNT).should('contain', '9 / 9');
    getByCy('downloadQRCode').should('be.visible');
    getByCy('done').should('be.visible');
    getByCy('done').click();

    // check created location
    getByCy('groupName').should('be.visible');
    getByCy('groupName').should('contain.text', testGroup.name);
    // check sub areas
    getByCy(`location-${testGroup.name}`).click();
    getByCy(`location-${AREA_1}`).should('contain', AREA_1);
    getByCy(`location-${AREA_2}`).should('contain', AREA_2);
    // go to the created default area
    getByCy(`location-${E2E_DEFAULT_LOCATION_NAME_EN}`).click();
    // check checkout values
    getByCy('locationCard-checkoutRadius').click();
    getDOMElement('#radius').should('have.value', RADIUS);
    getDOMElement('#averageCheckinTime').should(
      'have.value',
      AVERAGE_CHECKIN_TIME
    );
    // check area details
    getByCy('locationCard-areaDetails').click();
    checkAreaDetailsSelection();
    checkRoomSizeValueOnDashboard();
    // check table input
    getByCy('locationCard-tableSubdivision').click();
    getDOMElement('#tableCount').should('have.value', TABLE_COUNT);
  });
});
