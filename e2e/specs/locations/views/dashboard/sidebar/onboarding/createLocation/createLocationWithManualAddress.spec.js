import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import { STORE_TYPE } from '../../../../../constants/locations';
import {
  checkOptionalStickerExist,
  roomSizeShouldBeDisabeld,
  roomSizeShouldNotBeDisabled,
  typeManualAddress,
} from '../helpers';
import { createTestOperator } from '../../../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { visit } from '../../../../../utils/commands';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getGroupAttributes } from '../../../../../utils/createAttributes';
import {
  CREATE_GROUP,
  NEXT_STEP_BUTTON,
  STEP_COUNT,
} from '../../../../../constants/selectorKeys';

let testOperator;
const testGroup = getGroupAttributes('Nexenio store');

describe('Create location with manual address', { retries: 1 }, () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    cy.intercept('POST', 'api/v3/locationGroups').as('createGroup');
    visit(APP_ROUTE);
  });

  it('creates a new store with manual address input', () => {
    getByCy(CREATE_GROUP).first().click();

    // Step 1: Select group type
    getByCy(STEP_COUNT).should('contain', '1 / 9');
    getByCy(STORE_TYPE).click();

    // Step 2: Group name input
    getByCy(STEP_COUNT).should('contain', '2 / 9');
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');
    getDOMElement('#groupName').type(testGroup.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(STEP_COUNT).should('contain', '3 / 9');
    getDOMElement('#phone').should('have.value', testOperator.phone);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(STEP_COUNT).should('contain', '4 / 9');
    getByCy('toggleGoogleService').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    getByCy('toggleUseBusinessAddress').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');
    typeManualAddress(
      testGroup.streetName,
      testGroup.streetNr,
      testGroup.zipCode,
      testGroup.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(STEP_COUNT).should('contain', '5 / 9');
    getByCy('toggleAutomaticCheckout').should('be.disabled');
    getByCy('toggleAutomaticCheckinTime').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    getByCy(STEP_COUNT).should('contain', '6 / 9');
    getByCy('entryPolicyInfoTooltip').should('be.visible');

    checkOptionalStickerExist();

    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();

    getByCy(NEXT_STEP_BUTTON).click();
    getDOMElement('.ant-form-item-explain-error').should('be.visible');

    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getDOMElement('#ventilation').should('not.be.disabled');
    roomSizeShouldNotBeDisabled();
    // select outdoor
    getByCy('selectAreaType').click();
    getByCy('outdoor').click();
    getDOMElement('#ventilation').should('be.disabled');
    roomSizeShouldBeDisabeld();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy(STEP_COUNT).should('contain', '7 / 9');
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Area division
    getByCy(STEP_COUNT).should('contain', '8 / 9');
    getByCy('toggleAreaDivision').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 9: Finish and QR code(s) download
    cy.wait('@createGroup').its('response.statusCode').should('eq', 201);
    getByCy(STEP_COUNT).should('contain', '9 / 9');
    getByCy('downloadQRCode').should('be.visible');
    getByCy('done').should('be.visible');
    getByCy('done').click();

    // check created location
    getByCy('groupName').should('be.visible');
    getByCy('groupName').should('contain.text', testGroup.name);
    // check checkout values
    getByCy('locationCard-checkoutRadius').click();
    getByCy('activateCheckoutRadius').should('not.exist');
    getByCy('activateAutomaticCheckinTime').should('not.be.checked');
    // check area details
    getByCy('locationCard-areaDetails').click();
    getByCy('dashboard-areaSelection').should('contain', 'Outdoor');
    getByCy('dashboard-medicalMasksSelection').should('contain', 'Select');
    getByCy('dashboard-entryPolicyInfoSelection').should('contain', 'Select');
    getDOMElement('#ventilation').should('be.disabled');
    getDOMElement('#roomWidth-input').should('not.contain.value');
    getDOMElement('#roomDepth-input').should('not.contain.value');
    getDOMElement('#roomHeight-input').should('not.contain.value');
    // check table input
    getByCy('locationCard-tableSubdivision').click();
    getByCy('activateTableSubdivision').should('not.be.checked');
  });
});
