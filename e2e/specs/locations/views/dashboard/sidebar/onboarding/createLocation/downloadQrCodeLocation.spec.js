import { createTestOperator } from '../../../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../../../utils/setup.helper';
import { APP_ROUTE } from '../../../../../constants/routes';
import { getByCy, getDOMElement } from '../../../../../utils/selectors';
import {
  RESTAURANT_TYPE,
  STORE_TYPE,
} from '../../../../../constants/locations';
import {
  deleteFileIfExists,
  readFile,
  visit,
} from '../../../../../utils/commands';
import { getGroupAttributes } from '../../../../../utils/createAttributes';
import { getQrCodeFilePathPdf } from '../../../../../utils/downloadQrCodePdf.helper';
import { TABLE_COUNT, typeManualAddress } from '../helpers';
import {
  CREATE_GROUP,
  NEXT_STEP_BUTTON,
} from '../../../../../constants/selectorKeys';

const testGroup1 = getGroupAttributes('Nexenio store');
const testGroup2 = getGroupAttributes('Nexenio restaurant');

const TEST_GROUP_1_QR_CODE_PATH = getQrCodeFilePathPdf({
  locationName: testGroup1.name,
  hasTable: true,
});

const TEST_GROUP_2_QR_CODE_PATH = getQrCodeFilePathPdf({
  locationName: testGroup2.name,
  hasTable: false,
});

describe('Download QR Codes PDF on location creation', { retries: 2 }, () => {
  before(() => {
    createTestOperator();
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    cy.intercept('POST', 'api/v3/locationGroups').as('createGroup');
    visit(APP_ROUTE);
  });

  it('downloads the location qr-code PDF with table count', () => {
    getByCy(CREATE_GROUP).first().click();

    // Step 1: Select group type
    getByCy(STORE_TYPE).click();

    // Step 2: Group name input
    getDOMElement('#groupName').type(testGroup1.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    getByCy(NEXT_STEP_BUTTON).click();
    typeManualAddress(
      testGroup1.streetName,
      testGroup1.streetNr,
      testGroup1.zipCode,
      testGroup1.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy('toggleTableInput').click();
    getDOMElement('#tableCount').focus().type(TABLE_COUNT);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Area division
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 9: Finish and QR code(s) download
    cy.wait('@createGroup').its('response.statusCode').should('eq', 201);
    getByCy('downloadQRCode').should('be.visible');
    getByCy('downloadQRCode').should('contain.text', 'Generate QR Codes');
    getByCy('done').should('be.visible');
    getByCy('downloadQRCode').click();

    readFile(TEST_GROUP_1_QR_CODE_PATH, 30000);
    deleteFileIfExists(TEST_GROUP_1_QR_CODE_PATH);
  });

  it('downloads the location qr-code PDF without table count', () => {
    getByCy(CREATE_GROUP).click();

    // Step 1: Select group type
    getByCy(RESTAURANT_TYPE).click();

    // Step 2: Group name input
    getDOMElement('#groupName').type(testGroup2.name);
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 3: Phone number input
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 4: Google maps step
    getByCy(NEXT_STEP_BUTTON).click();
    // Manual address input
    getByCy(NEXT_STEP_BUTTON).click();
    typeManualAddress(
      testGroup2.streetName,
      testGroup2.streetNr,
      testGroup2.zipCode,
      testGroup2.city
    );
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 5: Automatic checkout
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 6: Area details
    // select indoor
    getByCy('selectAreaType').click();
    getByCy('indoor').click();
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 7: Table allocation
    getByCy('toggleTableInput').should('not.be.checked');
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 8: Area division
    getByCy(NEXT_STEP_BUTTON).click();

    // Step 9: Finish and QR code(s) download
    cy.wait('@createGroup').its('response.statusCode').should('eq', 201);
    getByCy('downloadQRCode').should('be.visible');
    getByCy('downloadQRCode').should('contain.text', 'Generate QR Code');
    getByCy('done').should('be.visible');
    getByCy('downloadQRCode').click();

    readFile(TEST_GROUP_2_QR_CODE_PATH, 30000);
    deleteFileIfExists(TEST_GROUP_2_QR_CODE_PATH);
  });
});
