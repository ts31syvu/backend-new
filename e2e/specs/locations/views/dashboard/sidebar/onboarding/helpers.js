import { getByCy, getDOMElement } from '../../../../utils/selectors';
import {
  BASE_TYPE,
  BUILDING_TYPE,
  HOTEL_TYPE,
  NURSING_HOME_TYPE,
  RESTAURANT_TYPE,
  ROOM_TYPE,
  STORE_TYPE,
} from '../../../../constants/locations';

export const ROOM_WIDTH = 111;
export const ROOM_DEPTH = 222;
export const ROOM_HEIGHT = 333;

export const TABLE_COUNT = 4;
export const RADIUS = 500;

export const AVERAGE_CHECKIN_TIME = '02:30';
export const AVERAGE_CHECKIN_TIME_MINUTES = 150;

export const SELECTED_ENTRY_POLICY_INFO = '2G';
export const SELECTED_VENTILATION = 'electric';
export const SELECTED_MASKS = 'yes';

export const AREA_TYPE = {
  selectedValue: 'indoor',
  initialOption: 'Indoor area',
};

export const ENTRY_POLICY_INFO = {
  selectedValue: '2G',
  initialOption: '2G',
};

export const VENTILATION = {
  selectedValue: 'electric',
  initialOption: 'Ventilation system',
};

export const MASKS = {
  selectedValue: 'yes',
  initialOption: 'Yes, during the entire attendance time',
};

export const checkAddressPrefill = (testGroup, testOperator) => {
  getDOMElement('#streetName')
    .invoke('attr', 'value')
    .should('to.be.oneOf', [
      testGroup?.streetName,
      testOperator.businessEntityStreetName,
    ]);

  getDOMElement('#streetNr')
    .invoke('attr', 'value')
    .should('to.be.oneOf', [
      testGroup?.streetNr,
      testOperator.businessEntityStreetNumber,
    ]);

  getDOMElement('#zipCode')
    .invoke('attr', 'value')
    .should('to.be.oneOf', [
      testGroup?.zipCode,
      testOperator.businessEntityZipCode,
    ]);

  getDOMElement('#city')
    .invoke('attr', 'value')
    .should('to.be.oneOf', [testGroup?.city, testOperator.businessEntityCity]);
};

export const areaTypesShouldBeVisible = () => {
  getByCy(RESTAURANT_TYPE).should('be.visible');
  getByCy(ROOM_TYPE).should('be.visible');
  getByCy(BUILDING_TYPE).should('be.visible');
  getByCy(BASE_TYPE).should('be.visible');
};

export const locationTypesShouldBeVisible = () => {
  getByCy(RESTAURANT_TYPE).should('be.visible');
  getByCy(STORE_TYPE).should('be.visible');
  getByCy(NURSING_HOME_TYPE).should('be.visible');
  getByCy(HOTEL_TYPE).should('be.visible');
  getByCy(BASE_TYPE).should('be.visible');
};

export const checkOptionalStickerExist = () => {
  getByCy('optionalSticker-areaType').should('not.exist');
  getByCy('optionalSticker-masks').should('be.visible');
  getByCy('optionalSticker-entryPolicyInfo').should('be.visible');
  getByCy('optionalSticker-ventilation').should('be.visible');
  getByCy('optionalSticker-roomSize').should('be.visible');
};

export const roomSizeShouldNotBeDisabled = () => {
  getByCy('roomWidth').should('not.be.disabled');
  getByCy('roomHeight').should('not.be.disabled');
  getByCy('roomDepth').should('not.be.disabled');
};

export const roomSizeShouldBeDisabeld = () => {
  getByCy('roomWidth').should('be.disabled');
  getByCy('roomHeight').should('be.disabled');
  getByCy('roomDepth').should('be.disabled');
};

export const checkRoomSizeValueOnDashboard = () => {
  getDOMElement('#roomWidth-input').should('have.value', ROOM_WIDTH);
  getDOMElement('#roomDepth-input').should('have.value', ROOM_DEPTH);
  getDOMElement('#roomHeight-input').should('have.value', ROOM_HEIGHT);
};

export const checkAreaDetailsSelection = () => {
  getByCy('dashboard-areaSelection').should('contain', AREA_TYPE.initialOption);
  getByCy('dashboard-medicalMasksSelection').should(
    'contain',
    MASKS.initialOption
  );
  getByCy('dashboard-entryPolicyInfoSelection').should(
    'contain',
    ENTRY_POLICY_INFO.initialOption
  );
  getByCy('dashboard-ventilationSelection').should(
    'contain',
    VENTILATION.initialOption
  );
};

export const typeManualAddress = (streetName, streetNr, zipCode, city) => {
  getDOMElement('#streetName').focus().type(streetName);
  getDOMElement('#streetNr').focus().type(streetNr);
  getDOMElement('#zipCode').focus().type(zipCode);
  getDOMElement('#city').focus().type(city);
};

export const typeRoomSize = () => {
  getByCy('roomWidth').focus().type(ROOM_WIDTH);
  getByCy('roomDepth').focus().type(ROOM_DEPTH);
  getByCy('roomHeight').focus().type(ROOM_HEIGHT);
};
