import { createTestOperator } from '../../utils/setup';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestArea, createTestGroup } from '../../network/api';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { type, visit } from '../../utils/commands';
import {
  shouldHaveAttribute,
  shouldBeVisible,
  shouldHaveValue,
  shouldNotExist,
} from '../../utils/assertions';
import {
  AREA_CHECKED,
  TABLE_COUNT_INPUT_FIELD,
  TABLE_SUBDIVISION_TOGGLE,
  ANT_VALIDATION_ERROR,
  LOCATION_CARD_PREFIX,
  AREA_PREFIX,
  AREA_LIST_GENERAL_LOCATION,
  ANT_COLLAPSE_ITEM_ACTIVE,
  ANT_INPUT_NUMBER_HANDLER_UP,
  ANT_INPUT_NUMBER_HANDLER_DOWN,
  DIV_ARIA_EXPANDED_TRUE,
} from '../../constants/selectorKeys';
import { APP_ROUTE } from '../../constants/routes';

const TABLE_SUBDIVISION_SETTINGS = 'tableSubdivision';
const TEST_TABLE_COUNT = '50';
let testArea;

describe('Location dashboard "Subdivision into tables"', { retries: 3 }, () => {
  before(() => {
    createTestOperator();

    createTestGroup(getGroupPayload()).then(group => {
      // eslint-disable-next-line promise/no-nesting
      createTestArea(group.groupId).then(area => {
        testArea = area;
      });
    });
    visit(APP_ROUTE);
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  it('is inactive by default', () => {
    getByCy(LOCATION_CARD_PREFIX + TABLE_SUBDIVISION_SETTINGS).click();
    shouldHaveAttribute(
      getByCy(TABLE_SUBDIVISION_TOGGLE),
      AREA_CHECKED,
      'false'
    );
  });

  it('opens and closes collapsible upon clicking on location card', () => {
    // opened collapsible should be visible
    shouldBeVisible(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
    // opened collapsible should be closed
    getDOMElement(DIV_ARIA_EXPANDED_TRUE).click();
    shouldNotExist(getDOMElement(ANT_COLLAPSE_ITEM_ACTIVE));
  });

  it('cannot accept non-numberic input', () => {
    // Open collapsible
    getByCy(LOCATION_CARD_PREFIX + TABLE_SUBDIVISION_SETTINGS).click();
    // Activate toggle
    shouldBeVisible(getByCy(TABLE_SUBDIVISION_TOGGLE)).click();
    // Test case: empty
    getDOMElement(TABLE_COUNT_INPUT_FIELD).clear();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    // Test case: special characters
    type(getDOMElement(TABLE_COUNT_INPUT_FIELD), '####');
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    // Test case: text
    type(getDOMElement(TABLE_COUNT_INPUT_FIELD), 'testing');
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  });

  it('sets the count to 1 if the input is less than 1', () => {
    // Test case: negative number
    type(getDOMElement(TABLE_COUNT_INPUT_FIELD).clear(), '-10').blur();
    // Expect the table count value to be changed to 1
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), 1);
    // Check if the count is persisted by re-opening the table configuration
    getByCy(AREA_PREFIX + testArea.name).click();
    getByCy(AREA_LIST_GENERAL_LOCATION).click();
    getByCy(LOCATION_CARD_PREFIX + TABLE_SUBDIVISION_SETTINGS).click();
    // Expect the status to be active after re-opening
    shouldHaveAttribute(
      getByCy(TABLE_SUBDIVISION_TOGGLE),
      AREA_CHECKED,
      'true'
    );
    // Expect the table count value to be 1
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), 1);
  });

  it('can change the number of tables', () => {
    // Test case: positive number
    type(getDOMElement(TABLE_COUNT_INPUT_FIELD).clear(), TEST_TABLE_COUNT);
    // Expect the table count value to be changed to the test count
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), TEST_TABLE_COUNT);
    // Check if the count is persisted by re-opening the table configuration
    getByCy(AREA_PREFIX + testArea.name).click();
    getByCy(AREA_LIST_GENERAL_LOCATION).click();
    getByCy(LOCATION_CARD_PREFIX + TABLE_SUBDIVISION_SETTINGS).click();
    // Expect the status to be active after re-opening
    shouldHaveAttribute(
      getByCy(TABLE_SUBDIVISION_TOGGLE),
      AREA_CHECKED,
      'true'
    );
    // Expect the table count value to be the test count
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), TEST_TABLE_COUNT);
  });

  it('increases number of tables by 1 when clicking up-arrow', () => {
    // Expect the table count value to be the test count
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), TEST_TABLE_COUNT);

    getDOMElement(ANT_INPUT_NUMBER_HANDLER_UP).click();
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), 51);
  });

  it('decreases number of tables by 1 when clicking down-arrow', () => {
    shouldBeVisible(getDOMElement(TABLE_COUNT_INPUT_FIELD));
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), 51);

    getDOMElement(ANT_INPUT_NUMBER_HANDLER_DOWN).click();
    shouldHaveValue(getDOMElement(TABLE_COUNT_INPUT_FIELD), TEST_TABLE_COUNT);
  });
});
