import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { triggerEvent, type, visit } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  shouldBeDisabled,
  shouldBeEmpty,
  shouldBeVisible,
  shouldHaveValue,
  shouldNotExist,
} from '../../utils/assertions';
import {
  ANT_TOOLTIP_INNER,
  ANT_VALIDATION_ERROR,
  DASHBOARD_WEBSITE_MENU_AND_LINKS,
  LINK_CONTENT_INFORMATION_ICON,
  LINK_INPUT_FIELD_GENERAL,
  LINK_INPUT_FIELD_MAP,
  LINK_INPUT_FIELD_MENU,
  LINK_INPUT_FIELD_SCHEDULE,
  LINK_INPUT_FIELD_WEBSITE,
  WEBSITE_LINKS_SAVE_CHANGES_BUTTON,
  WEBSITE_MENU_LINKS_SECTIONS,
} from '../../constants/selectorKeys';
import { antErrorModalAppears } from '../../helpers/ui/validations';
import { INVALID_WEBSITE_INPUTS } from '../../constants/inputs';

describe('Dashboard Overview/Website, menu and other links card', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload());
  });

  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    // Open 'Website, menu and other links' collapsible
    getByCy(DASHBOARD_WEBSITE_MENU_AND_LINKS).click();
    shouldBeVisible(getByCy(WEBSITE_MENU_LINKS_SECTIONS));
  });

  it('should display information tooltip when hovering over information icon', () => {
    triggerEvent(getByCy(LINK_CONTENT_INFORMATION_ICON), 'mouseover');
    shouldBeVisible(getDOMElement(ANT_TOOLTIP_INNER));
  });

  it('should decline inputs that start with https:// but are not real websites', () => {
    // Error modal should appear upon typing invalid input and clicking save button and clear the input fields
    type(getByCy(LINK_INPUT_FIELD_MENU), 'https://nexenio');
    type(getByCy(LINK_INPUT_FIELD_WEBSITE), 'https://nexenio!!');
    type(getByCy(LINK_INPUT_FIELD_SCHEDULE), 'https://nexenio?');
    type(getByCy(LINK_INPUT_FIELD_MAP), 'https://www.nexenio!com');
    type(getByCy(LINK_INPUT_FIELD_GENERAL), 'https://nexenio?com');

    shouldHaveValue(getByCy(LINK_INPUT_FIELD_MENU), 'https://nexenio');
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_WEBSITE), 'https://nexenio!!');
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_SCHEDULE), 'https://nexenio?');
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_MAP), 'https://www.nexenio!com');
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_GENERAL), 'https://nexenio?com');

    getByCy(WEBSITE_LINKS_SAVE_CHANGES_BUTTON).click();
    antErrorModalAppears();

    shouldBeEmpty(getByCy(LINK_INPUT_FIELD_MENU));
    shouldBeEmpty(getByCy(LINK_INPUT_FIELD_WEBSITE));
    shouldBeEmpty(getByCy(LINK_INPUT_FIELD_SCHEDULE));
    shouldBeEmpty(getByCy(LINK_INPUT_FIELD_MAP));
    shouldBeEmpty(getByCy(LINK_INPUT_FIELD_GENERAL));
  });

  it('should decline inputs that do not start with https://', () => {
    INVALID_WEBSITE_INPUTS.map(invalidWebsite => {
      type(getByCy(LINK_INPUT_FIELD_MENU), invalidWebsite);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(LINK_INPUT_FIELD_MENU).clear();
    });

    INVALID_WEBSITE_INPUTS.map(invalidWebsite => {
      type(getByCy(LINK_INPUT_FIELD_WEBSITE), invalidWebsite);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(LINK_INPUT_FIELD_WEBSITE).clear();
    });

    INVALID_WEBSITE_INPUTS.map(invalidWebsite => {
      type(getByCy(LINK_INPUT_FIELD_SCHEDULE), invalidWebsite);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(LINK_INPUT_FIELD_SCHEDULE).clear();
    });

    INVALID_WEBSITE_INPUTS.map(invalidWebsite => {
      type(getByCy(LINK_INPUT_FIELD_MAP), invalidWebsite);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(LINK_INPUT_FIELD_MAP).clear();
    });

    INVALID_WEBSITE_INPUTS.map(invalidWebsite => {
      type(getByCy(LINK_INPUT_FIELD_GENERAL), invalidWebsite);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(LINK_INPUT_FIELD_GENERAL).clear();
    });
  });

  it('should accept valid inputs and allow to save changes', () => {
    const validInput = 'https://www.nexenio.com';

    type(getByCy(LINK_INPUT_FIELD_MENU), validInput);
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));

    type(getByCy(LINK_INPUT_FIELD_WEBSITE), validInput);
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));

    type(getByCy(LINK_INPUT_FIELD_SCHEDULE), validInput);
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));

    type(getByCy(LINK_INPUT_FIELD_MAP), validInput);
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));

    type(getByCy(LINK_INPUT_FIELD_GENERAL), validInput);
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));

    getByCy(WEBSITE_LINKS_SAVE_CHANGES_BUTTON).click();
    // After saving, button should no longer be clickbale without any new changes to any input field
    shouldBeDisabled(getByCy(WEBSITE_LINKS_SAVE_CHANGES_BUTTON));
    // Website links should now be visible in input fields
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_MENU), validInput);
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_WEBSITE), validInput);
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_SCHEDULE), validInput);
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_MAP), validInput);
    shouldHaveValue(getByCy(LINK_INPUT_FIELD_GENERAL), validInput);
  });
});
