import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import {
  HELP_CENTER_ROUTE,
  DATA_TRANSFERS_ROUTE,
  APP_ROUTE,
} from '../../constants/routes';
import { getByCy } from '../../utils/selectors';
import { shouldBeVisible, urlShouldInclude } from '../../utils/assertions';
import { visit } from '../../utils/commands';
import {
  HELP_CENTER_TITLE,
  BACK_TO_LOCATION_BUTTON,
  DATA_TRANSFERS_SECTION,
  GROUPNAME,
  LOCATION_DISPLAY_NAME,
} from '../../constants/selectorKeys';

describe('Help center back button', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload());
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  context('Starting in location overview', () => {
    it('redirects user to previous destinaton', () => {
      shouldBeVisible(getByCy(GROUPNAME));
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
      visit(HELP_CENTER_ROUTE);
      shouldBeVisible(getByCy(HELP_CENTER_TITLE));
      getByCy(BACK_TO_LOCATION_BUTTON).click();
      urlShouldInclude(APP_ROUTE);
      shouldBeVisible(getByCy(GROUPNAME));
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
    });
  });

  context('Starting in share data view', () => {
    it('redirects user to previous destinaton', () => {
      visit(DATA_TRANSFERS_ROUTE);
      shouldBeVisible(getByCy(DATA_TRANSFERS_SECTION));
      visit(HELP_CENTER_ROUTE);
      shouldBeVisible(getByCy(HELP_CENTER_TITLE));
      getByCy(BACK_TO_LOCATION_BUTTON).click();
      urlShouldInclude(APP_ROUTE);
      shouldBeVisible(getByCy(GROUPNAME));
      shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
    });
  });
});
