import { HELP_CENTER_ROUTE } from '../../../constants/routes';
import { createTestOperator } from '../../../utils/setup';
import { getByCy } from '../../../utils/selectors';
import { visit } from '../../../utils/commands';
import { shouldBeVisible, shouldHaveText } from '../../../utils/assertions';
import {
  HELPCENTER_MODAL_TRIGGER,
  CONTACT_FORM_MODAL,
  CONTACT_FORM_OPERATOR_SUPPORT_CODE,
  CONTACT_FORM_OPERATOR_NAME,
  CONTACT_FORM_OPERATOR_EMAIL,
} from '../../../constants/selectorKeys';

let testOperator;

describe('Send support mail modal', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    visit(HELP_CENTER_ROUTE);
  });
  it('shows the correct operator values', () => {
    // Open contact modal
    shouldBeVisible(getByCy(HELPCENTER_MODAL_TRIGGER)).click();
    // Expect modal to open
    shouldBeVisible(getByCy(CONTACT_FORM_MODAL));
    // Check if values exist
    shouldBeVisible(getByCy(CONTACT_FORM_OPERATOR_SUPPORT_CODE));
    shouldBeVisible(getByCy(CONTACT_FORM_OPERATOR_NAME));
    shouldBeVisible(getByCy(CONTACT_FORM_OPERATOR_EMAIL));
    // Compare displayed values to operator values
    shouldHaveText(getByCy(CONTACT_FORM_OPERATOR_EMAIL), testOperator.email);
    shouldHaveText(
      getByCy(CONTACT_FORM_OPERATOR_NAME),
      `${testOperator.firstName} ${testOperator.lastName}`
    );
    shouldHaveText(
      getByCy(CONTACT_FORM_OPERATOR_SUPPORT_CODE),
      testOperator.supportCode
    );
  });
});
