import {
  INVALID_MESSAGES,
  INVALID_PHONE_NUMBERS,
  VALID_MESSAGES,
  VALID_PHONE_NUMBERS,
} from '../../../constants/inputs';
import { HELP_CENTER_ROUTE } from '../../../constants/routes';
import {
  ANT_VALIDATION_ERROR,
  CONTACT_FORM_MESSAGE_INPUT_FIELD,
  CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD,
  CONTACT_FORM_SEND_BUTTON,
  SUCCESS_NOTIFICATION_MODAL,
} from '../../../constants/selectorKeys';
import { shouldBeVisible, shouldNotExist } from '../../../utils/assertions';
import { type, visit } from '../../../utils/commands';
import { getByCy, getDOMElement } from '../../../utils/selectors';
import { createTestOperator } from '../../../utils/setup';
import { DEFAULT_COOKIE } from '../../../utils/setup.helper';

describe('Send support mail input fields and modals validation', () => {
  before(() => {
    createTestOperator();
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(HELP_CENTER_ROUTE);
    getByCy('helpCenterModalTrigger').click();
  });

  it('declines an invalid phone number and message', () => {
    INVALID_PHONE_NUMBERS.map(invalidNumber => {
      shouldBeVisible(getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD));
      type(getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD), invalidNumber);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD).clear();
    });
    INVALID_MESSAGES.map(invalidMessage => {
      shouldBeVisible(getByCy(CONTACT_FORM_MESSAGE_INPUT_FIELD));
      type(getByCy(CONTACT_FORM_MESSAGE_INPUT_FIELD), invalidMessage);
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(CONTACT_FORM_MESSAGE_INPUT_FIELD).clear();
    });
  });

  it('declines an empty form submit', () => {
    getByCy('contactFormSendButton').click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  });

  it('opens send mail success modal after valid input', () => {
    VALID_PHONE_NUMBERS.map(validNumber => {
      shouldBeVisible(getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD));
      type(getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD), validNumber);
      shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
      getByCy(CONTACT_FORM_PHONE_NUMBER_INPUT_FIELD).clear();
    });
    VALID_MESSAGES.map(validMessage => {
      shouldBeVisible(getByCy(CONTACT_FORM_MESSAGE_INPUT_FIELD));
      type(getByCy(CONTACT_FORM_MESSAGE_INPUT_FIELD), validMessage);
      shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
    });
    getByCy(CONTACT_FORM_SEND_BUTTON).click();
    shouldBeVisible(getByCy(SUCCESS_NOTIFICATION_MODAL));
  });
});
