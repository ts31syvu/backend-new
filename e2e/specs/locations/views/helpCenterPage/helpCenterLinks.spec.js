import { HELP_CENTER_ROUTE, LICENSES_ROUTE } from '../../constants/routes';
import { FAQ_LINK, VIDEOS_LINK, TOOLKIT_LINK } from '../../constants/links';
import { getByCy } from '../../utils/selectors';
import { shouldBeVisible, shouldHaveHref } from '../../utils/assertions';
import {
  HELPCENTER_LINKS,
  SUPPORT_VIDEO_LINK_LABEL,
  FAQ_LINK_LABEL,
  LICENSE_LINK_LABEL,
  TOOLTIP_LINK_LABEL,
} from '../../constants/selectorKeys';
import { createTestOperator } from '../../utils/setup';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { visit } from '../../utils/commands';
import { verifyLinkOpensInNewTab } from '../../helpers/ui/validations';

describe('Help center link section', () => {
  before(() => {
    createTestOperator();
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(HELP_CENTER_ROUTE);
    shouldBeVisible(getByCy(HELPCENTER_LINKS));
  });

  it('opens the correct support video link', () => {
    shouldHaveHref(getByCy(SUPPORT_VIDEO_LINK_LABEL), VIDEOS_LINK);
    verifyLinkOpensInNewTab(getByCy(SUPPORT_VIDEO_LINK_LABEL));
  });

  it('opens the correct FAQ link', () => {
    shouldHaveHref(getByCy(FAQ_LINK_LABEL), FAQ_LINK);
    verifyLinkOpensInNewTab(getByCy(FAQ_LINK_LABEL));
  });

  it('opens the correct toolkit link', () => {
    shouldHaveHref(getByCy(TOOLTIP_LINK_LABEL), TOOLKIT_LINK);
    verifyLinkOpensInNewTab(getByCy(TOOLTIP_LINK_LABEL));
  });

  it('opens the correct license link', () => {
    shouldHaveHref(getByCy(LICENSE_LINK_LABEL), LICENSES_ROUTE);
    verifyLinkOpensInNewTab(getByCy(LICENSE_LINK_LABEL));
  });
});
