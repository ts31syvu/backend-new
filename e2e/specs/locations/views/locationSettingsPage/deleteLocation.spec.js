import { getGroupPayload } from '../../helpers/api/groups';
import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { visit, type } from '../../utils/commands';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  shouldBeVisible,
  shouldContain,
  shouldNotExist,
} from '../../utils/assertions';
import {
  GROUPNAME,
  DELETE_GROUP,
  GROUP_SETTINGS_HEADER,
  ANT_PRIMARY_POPOVER_BUTTON,
  ANT_POPOVER_BUTTONS,
  PASSWORD,
  SUBMIT_BUTTON,
  ANT_NOTIFICATION,
  GROUP_ITEM,
  OPEN_LOCATION_SETTINGS_LINK,
} from '../../constants/selectorKeys';
import { DEFAULT_COOKIE, STANDARD_PASSWORD } from '../../utils/setup.helper';

let testGroup;

describe('Group deletion', { retries: 1 }, () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    shouldContain(getByCy(GROUPNAME), testGroup.groupName);
    shouldBeVisible(getByCy(`${GROUP_ITEM}-${testGroup.groupId}`)).click();
    getDOMElement(OPEN_LOCATION_SETTINGS_LINK).click();
  });

  it('can cancel before deleting a group ', () => {
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroup.groupName);
    getByCy(DELETE_GROUP).click();
    getDOMElement(ANT_POPOVER_BUTTONS).first().click();
    shouldBeVisible(getByCy(GROUP_SETTINGS_HEADER));
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroup.groupName);
  });

  it('deletes a group', () => {
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroup.groupName);
    getByCy(DELETE_GROUP).click();
    getDOMElement(ANT_PRIMARY_POPOVER_BUTTON).click();
    // Enter password
    type(getDOMElement(PASSWORD).focus(), STANDARD_PASSWORD);
    getDOMElement(SUBMIT_BUTTON).click();
    shouldNotExist(getDOMElement(ANT_NOTIFICATION));
  });
});
