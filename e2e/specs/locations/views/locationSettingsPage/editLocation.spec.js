import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { APP_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { type, visit } from '../../utils/commands';
import { shouldBeVisible, shouldContain } from '../../utils/assertions';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import {
  GROUPNAME,
  GROUP_SETTINGS_HEADER,
  NAME_INPUT_FIELD,
  EDIT_GROUP_NAME,
  GROUP_ITEM,
  OPEN_LOCATION_SETTINGS_LINK,
} from '../../constants/selectorKeys';

let testGroup;
const NEW_GROUP_NAME = 'Nexenio new group';

describe('Group editing', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
  });
  it('changes the name of the group', () => {
    shouldContain(getByCy(GROUPNAME), testGroup.groupName);
    shouldBeVisible(getByCy(`${GROUP_ITEM}-${testGroup.groupId}`)).click();
    getDOMElement(OPEN_LOCATION_SETTINGS_LINK).click();
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroup.groupName);
    getByCy(EDIT_GROUP_NAME).click();
    type(getDOMElement(NAME_INPUT_FIELD).clear(), NEW_GROUP_NAME);
    getByCy(EDIT_GROUP_NAME).click();
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), NEW_GROUP_NAME);
  });
});
