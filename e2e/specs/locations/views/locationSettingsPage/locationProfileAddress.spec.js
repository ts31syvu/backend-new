import { createTestOperator } from '../../utils/setup';
import { createTestGroup } from '../../network/api';
import { getGroupPayload } from '../../helpers/api/groups';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { APP_ROUTE } from '../../constants/routes';
import {
  shouldBeVisible,
  shouldContain,
  shouldHaveText,
} from '../../utils/assertions';
import {
  GROUPNAME,
  GROUP_SETTINGS_HEADER,
  STREET_NAME_AND_STREET_NUMBER,
  ZIP_CODE_AND_CITY,
  GROUP_ITEM,
  OPEN_LOCATION_SETTINGS_LINK,
} from '../../constants/selectorKeys';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { visit } from '../../utils/commands';

let testGroup;

describe('Location profile settings page', () => {
  before(() => {
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      testGroup = group;
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(APP_ROUTE);
    shouldContain(getByCy(GROUPNAME), testGroup.groupName);
    shouldBeVisible(getByCy(`${GROUP_ITEM}-${testGroup.groupId}`)).click();
    getDOMElement(OPEN_LOCATION_SETTINGS_LINK).click();
  });

  it('should display the correct location address', () => {
    shouldContain(getByCy(GROUP_SETTINGS_HEADER), testGroup.groupName);
    shouldContain(
      getByCy(STREET_NAME_AND_STREET_NUMBER),
      `${testGroup.streetName} ${testGroup.streetNr}`
    );
    shouldHaveText(
      getByCy(ZIP_CODE_AND_CITY),
      `${testGroup.zipCode} ${testGroup.city}`
    );
    shouldContain(
      getByCy(STREET_NAME_AND_STREET_NUMBER),
      `${testGroup.streetName} ${testGroup.streetNr}`
    );
  });
});
