import { LOGIN_ROUTE } from '../../constants/routes';
import { checkLinksInFooter } from '../../helpers/ui/checkLinksInFooter';
import { visit } from '../../utils/commands';

describe('Login page has the correct links in the footer', () => {
  beforeEach(() => {
    visit(LOGIN_ROUTE);
  });
  checkLinksInFooter();
});
