import { createTestOperator } from '../../utils/setup';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestGroup, login } from '../../network/api';
import { STANDARD_PASSWORD, DEFAULT_COOKIE } from '../../utils/setup.helper';
import {
  PROFILE_ROUTE,
  APP_ROUTE,
  DATA_TRANSFERS_ROUTE,
} from '../../constants/routes';
import { E2E_DEFAULT_LOCATION_NAME_EN } from '../../constants/locations';
import { requestAccountDeletion } from '../../helpers/api/operators';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { visit } from '../../utils/commands';
import {
  shouldNotExist,
  contains,
  shouldBeVisible,
} from '../../utils/assertions';
import {
  DELETE_ACCOUNT_SECTION,
  DELETE_ACCOUNT_BUTTON,
  ANT_POPOVER_BUTTONS,
  ANT_PRIMARY_POPOVER_BUTTON,
  ANT_MESSAGE_SUCCESS,
  GROUPS_OVERVIEW,
  PRIVACY_MANDATORY_LINK_LABEL,
  PRIVACY_OPTIONAL_LINK_LABEL,
  DPA_LINK_LABEL,
  TOMS_LINK_LABEL,
  IN_PROGRESS,
  DELETION_REQUESTED,
  DATA_TRANSFERS_SECTION,
  RESTORE_ACCOUNT,
  LOCATION_DISPLAY_NAME,
  LOCATION_PREFIX,
} from '../../constants/selectorKeys';

let testOperator;

describe('request account deletion and reactivation', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    createTestGroup(getGroupPayload());
  });

  describe('an active account', () => {
    it('can be deactivated', () => {
      visit(PROFILE_ROUTE);
      getByCy(DELETE_ACCOUNT_SECTION).within(() => {
        getByCy(DELETE_ACCOUNT_BUTTON).click();
      });
      // Cancel account deletion process
      shouldBeVisible(getDOMElement(ANT_POPOVER_BUTTONS)).first().click();
      // Delete account
      getByCy(DELETE_ACCOUNT_BUTTON).click();
      getDOMElement(ANT_PRIMARY_POPOVER_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_MESSAGE_SUCCESS));
    });
  });

  describe('a deactivated account', () => {
    before(() => {
      login({ username: testOperator.email, password: STANDARD_PASSWORD });
      requestAccountDeletion();
    });
    beforeEach(() => {
      Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    });

    it('does not show a delete button, still shows relevant profile content', () => {
      visit(PROFILE_ROUTE);
      shouldBeVisible(getByCy(GROUPS_OVERVIEW));
      shouldBeVisible(getByCy(PRIVACY_MANDATORY_LINK_LABEL));
      shouldBeVisible(getByCy(PRIVACY_OPTIONAL_LINK_LABEL));
      shouldBeVisible(getByCy(DPA_LINK_LABEL));
      shouldBeVisible(getByCy(TOMS_LINK_LABEL));
      shouldNotExist(getByCy(DELETE_ACCOUNT_BUTTON));
      // shows deletion in progres
      getByCy(DELETE_ACCOUNT_SECTION).within(() => {
        getByCy(IN_PROGRESS);
        contains('27'); // days remaining
      });
    });

    it('shows a notice on the main route', () => {
      visit(APP_ROUTE);
      shouldBeVisible(getByCy(DELETION_REQUESTED));
      shouldBeVisible(getByCy(RESTORE_ACCOUNT));
    });

    it('still shows data requests', () => {
      visit(DATA_TRANSFERS_ROUTE);
      shouldBeVisible(getByCy(DATA_TRANSFERS_SECTION));
    });

    context('when reactivating', () => {
      it('shows a success message', () => {
        visit(PROFILE_ROUTE);
        getByCy(RESTORE_ACCOUNT).click();
        shouldBeVisible(getDOMElement(ANT_MESSAGE_SUCCESS));
        // shows the deletion button again
        getByCy(DELETE_ACCOUNT_BUTTON, { timeout: 10000 }).should('be.visible');
        // shows location and groups again
        visit(APP_ROUTE);
        shouldBeVisible(getByCy(LOCATION_DISPLAY_NAME));
        shouldBeVisible(
          getByCy(LOCATION_PREFIX + E2E_DEFAULT_LOCATION_NAME_EN)
        );
      });
    });
  });
});
