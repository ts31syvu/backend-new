import { createTestOperator } from '../../utils/setup';
import { STANDARD_PASSWORD } from '../../utils/setup.helper';
import { PROFILE_ROUTE } from '../../constants/routes';
import {
  EMAIL_INPUT_FIELD,
  SAVE_CHANGES_BUTTON,
  SUBMIT_BUTTON,
  PASSWORD,
  UPDATE_EMAIL_NOTIFICATION,
  ANT_VALIDATION_ERROR,
  EDIT_PERSONAL_PROFILE_BUTTON,
} from '../../constants/selectorKeys';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  shouldBeVisible,
  shouldHaveValue,
  shouldNotExist,
  shouldNotHaveValue,
} from '../../utils/assertions';
import { type, visit } from '../../utils/commands';
import { INVALID_EMAILS, NEW_E2E_EMAIL } from '../../constants/inputs';

let testOperator;

describe('Profile page/ Change operator email', () => {
  beforeEach(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    visit(PROFILE_ROUTE);
  });

  it('can not submit changes with invalid email address', () => {
    // verify operators email
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    shouldHaveValue(getDOMElement(EMAIL_INPUT_FIELD), testOperator.email);
    shouldNotExist(getByCy(UPDATE_EMAIL_NOTIFICATION));
    // submit invalid email throws error
    INVALID_EMAILS.map(invalidEmail => {
      type(getDOMElement(EMAIL_INPUT_FIELD).clear(), invalidEmail);
      getByCy(SAVE_CHANGES_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    });
    // verify no changes have been applied
    visit(PROFILE_ROUTE);
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    shouldHaveValue(getDOMElement(EMAIL_INPUT_FIELD), testOperator.email);
    shouldNotExist(getByCy(UPDATE_EMAIL_NOTIFICATION));
  });
  it('sends a request to change the operator email', () => {
    // verify operators email
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    shouldHaveValue(getDOMElement(EMAIL_INPUT_FIELD), testOperator.email);
    shouldNotExist(getByCy(UPDATE_EMAIL_NOTIFICATION));
    // update email with valid input
    type(getDOMElement(EMAIL_INPUT_FIELD).clear(), NEW_E2E_EMAIL);
    getByCy(SAVE_CHANGES_BUTTON).click();
    type(getDOMElement(PASSWORD).clear(), STANDARD_PASSWORD);
    getDOMElement(SUBMIT_BUTTON).click();
    // verify changes have been applied
    shouldNotHaveValue(getDOMElement(EMAIL_INPUT_FIELD), testOperator.email);
    shouldHaveValue(getDOMElement(EMAIL_INPUT_FIELD), NEW_E2E_EMAIL);
    shouldBeVisible(getByCy(UPDATE_EMAIL_NOTIFICATION));
  });
});
