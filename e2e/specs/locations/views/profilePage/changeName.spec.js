import { createTestOperator } from '../../utils/setup';
import { PROFILE_ROUTE } from '../../constants/routes';
import {
  FIRST_NAME,
  LAST_NAME,
  OPERATOR_NAME,
  SAVE_CHANGES_BUTTON,
  ANT_VALIDATION_ERROR,
  ANT_NOTIFICATION_NOTICE_MESSAGE,
  EDIT_PERSONAL_PROFILE_BUTTON,
} from '../../constants/selectorKeys';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { type, visit } from '../../utils/commands';
import {
  shouldHaveValue,
  shouldContain,
  shouldBeVisible,
  shouldNotExist,
} from '../../utils/assertions';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import {
  NEW_E2E_FIRST_NAME,
  NEW_E2E_LAST_NAME,
  INVALID_NAME,
} from '../../constants/inputs';

let testOperator;

describe('Profile page name change', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(PROFILE_ROUTE);
  });

  it('expects operator name to equal name in header and inputs', () => {
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    shouldContain(
      getByCy(OPERATOR_NAME),
      `${testOperator.firstName} ${testOperator.lastName}`
    );
    shouldHaveValue(getDOMElement(FIRST_NAME), testOperator.firstName);
    shouldHaveValue(getDOMElement(LAST_NAME), testOperator.lastName);
  });
  it('can not submit invalid inputs', () => {
    // cannot submit empty first name
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    getDOMElement(FIRST_NAME).clear();
    getByCy(SAVE_CHANGES_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    // cannot submit empty last name
    type(getDOMElement(FIRST_NAME), testOperator.firstName);
    getDOMElement(LAST_NAME).clear();
    getByCy(SAVE_CHANGES_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    // cannot submit invalid first name
    type(getDOMElement(LAST_NAME), testOperator.lastName);
    type(getDOMElement(FIRST_NAME).clear(), INVALID_NAME);
    getByCy(SAVE_CHANGES_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    // cannot submit invalid last name
    type(getDOMElement(FIRST_NAME).clear(), testOperator.firstName);
    type(getDOMElement(LAST_NAME).clear(), INVALID_NAME);
    getByCy(SAVE_CHANGES_BUTTON).click();
    shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
  });
  it('can change name successfully', () => {
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    type(getDOMElement(FIRST_NAME).clear(), NEW_E2E_FIRST_NAME);
    type(getDOMElement(LAST_NAME).clear(), NEW_E2E_LAST_NAME);
    getByCy(SAVE_CHANGES_BUTTON).click();
    shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
    shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_MESSAGE));
    // Expect new name to be set
    getByCy(EDIT_PERSONAL_PROFILE_BUTTON).click();
    shouldContain(
      getByCy(OPERATOR_NAME),
      `${NEW_E2E_FIRST_NAME} ${NEW_E2E_LAST_NAME}`
    );
    shouldHaveValue(getDOMElement(FIRST_NAME), NEW_E2E_FIRST_NAME);
    shouldHaveValue(getDOMElement(LAST_NAME), NEW_E2E_LAST_NAME);
  });
});
