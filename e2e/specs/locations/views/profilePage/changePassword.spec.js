import { createTestOperator } from '../../utils/setup';
import { BASE_ROUTE, PROFILE_ROUTE } from '../../constants/routes';
import { getByCy, getDOMElement } from '../../utils/selectors';
import { type, visit } from '../../utils/commands';

import { shouldBeVisible, shouldHaveValue } from '../../utils/assertions';
import {
  CURRENT_PASSWORD,
  NEW_PASSWORD_INPUT_FIELD,
  NEW_PASSWORD_REPEAT_INPUT_FIELD,
  CHANGE_PASSWORD,
} from '../../constants/selectorKeys';
import { STANDARD_PASSWORD } from '../../utils/setup.helper';

let testOperator;

// The password is used for testing purpose only
const NEW_PASSWORD_INPUT = 'NewPassword123!A12@';
const EMPTY_STRING = '';

describe('Change Operator Password', () => {
  before(() => {
    createTestOperator().then(operator => {
      testOperator = operator;
    });
    visit(PROFILE_ROUTE);
  });

  it('is possible to change password and login with new password', () => {
    shouldBeVisible(getDOMElement(CURRENT_PASSWORD));
    shouldHaveValue(getDOMElement(CURRENT_PASSWORD), EMPTY_STRING);
    shouldBeVisible(getDOMElement(NEW_PASSWORD_INPUT_FIELD));
    shouldHaveValue(getDOMElement(NEW_PASSWORD_INPUT_FIELD), EMPTY_STRING);
    shouldBeVisible(getDOMElement(NEW_PASSWORD_REPEAT_INPUT_FIELD));
    shouldHaveValue(
      getDOMElement(NEW_PASSWORD_REPEAT_INPUT_FIELD),
      EMPTY_STRING
    );
    // Change password
    type(getDOMElement(CURRENT_PASSWORD), STANDARD_PASSWORD);
    type(getDOMElement(NEW_PASSWORD_INPUT_FIELD), NEW_PASSWORD_INPUT);
    type(getDOMElement(NEW_PASSWORD_REPEAT_INPUT_FIELD), NEW_PASSWORD_INPUT);
    getByCy(CHANGE_PASSWORD).click();
    // Logout
    cy.logoutLocations().then(response => {
      expect(response.status).to.eq(204);
    });
    visit(BASE_ROUTE);
    // Old login does not work anymore
    cy.loginLocations(testOperator.email, STANDARD_PASSWORD).then(response => {
      expect(response.status).to.eq(401);
    });
    // Logging in with new password
    cy.loginLocations(testOperator.email, NEW_PASSWORD_INPUT).then(response => {
      expect(response.status).to.eq(204);
    });
  });
});
