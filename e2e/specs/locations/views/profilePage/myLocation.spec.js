import { createTestOperator } from '../../utils/setup';
import { getGroupPayload } from '../../helpers/api/groups';
import { createTestGroups } from '../../network/api.helper';
import { DEFAULT_COOKIE } from '../../utils/setup.helper';
import { visit } from '../../utils/commands';
import { PROFILE_ROUTE } from '../../constants/routes';
import {
  shouldBeVisible,
  shouldHaveLength,
  shouldContain,
  shouldHaveText,
} from '../../utils/assertions';
import { getByCy } from '../../utils/selectors';
import {
  GROUPS_OVERVIEW,
  GROUPS_COUNT,
  GROUPS_NUMBER,
  GROUPS_NAME,
} from '../../constants/selectorKeys';

const groupPayloads = [
  getGroupPayload({ name: 'AAA' }),
  getGroupPayload({ name: 'BBB' }),
];
let testGroups;

describe('Profile page/ My location section', () => {
  before(() => {
    createTestOperator();
  });
  beforeEach(() => {
    visit(PROFILE_ROUTE);
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
  });

  context('When there are no locations', () => {
    it('the amount of locations is zero and no names are displayed', () => {
      shouldBeVisible(getByCy(GROUPS_OVERVIEW));
      shouldHaveText(getByCy(GROUPS_COUNT), '0');
    });
  });

  context('When there are one or more locations', () => {
    before(() => {
      createTestGroups(groupPayloads).then(groups => {
        testGroups = groups;
      });
    });
    it('the amount of locations and their corresponding names are displayed in ascending alphabetical order', () => {
      shouldBeVisible(getByCy(GROUPS_OVERVIEW));
      shouldContain(getByCy(GROUPS_COUNT), testGroups.length);
      shouldHaveLength(getByCy(GROUPS_NUMBER), testGroups.length);
      shouldHaveText(getByCy(GROUPS_NUMBER).eq(0), '1');
      shouldHaveText(getByCy(GROUPS_NUMBER).eq(1), '2');
      shouldHaveLength(getByCy(GROUPS_NAME), testGroups.length);
      shouldHaveText(getByCy(GROUPS_NAME).eq(0), `${testGroups[0].groupName}`);
      shouldHaveText(getByCy(GROUPS_NAME).eq(1), `${testGroups[1].groupName}`);
    });
  });
});
