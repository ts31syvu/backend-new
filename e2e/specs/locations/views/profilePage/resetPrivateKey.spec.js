import { createTestOperator } from '../../utils/setup';
import { DEFAULT_COOKIE, STANDARD_PASSWORD } from '../../utils/setup.helper';
import { type, visit } from '../../utils/commands';
import { PROFILE_ROUTE } from '../../constants/routes';
import {
  shouldBeVisible,
  shouldNotExist,
  shouldBeDisabled,
} from '../../utils/assertions';
import { getByCy, getDOMElement } from '../../utils/selectors';
import {
  PASSWORD,
  RESET_KEY_BUTTON,
  RESET_KEY_INFO,
  ANT_VALIDATION_ERROR,
  NEXT_BUTTON,
  ANT_MODAL_CONTENT,
  ANT_NOTIFICATION_NOTICE_MESSAGE,
  DOWNLOAD_PRIVATE_KEY_BUTTON,
  BACK_BUTTON,
  RESET_KEY_SUBMIT_BUTTON,
  READ_CONFIRMATION_CHECKBOX,
  PRIVATE_KEY_DOWNLOADED_CHECKBOX,
  ANT_MESSAGE_ERROR,
  ANT_MESSAGE_CHECK,
} from '../../constants/selectorKeys';
import {
  NEW_E2E_FIRST_NAME,
  NEW_E2E_LAST_NAME,
  WRONG_PASSWORD,
} from '../../constants/inputs';
import { uploadLocationPrivateKeyFile } from '../../helpers/ui/handlePrivateKeyFile';
import { getOperatorKeyFile } from '../../utils/downloads.helper';

import { changeUserName } from '../../helpers/api/operators';

let operatorKeyFile;
let newOperatorKeyFile;

describe('Profile page key reset', () => {
  before(() => {
    createTestOperator(true).then(operator => {
      const { firstName, lastName } = operator;
      operatorKeyFile = getOperatorKeyFile(firstName, lastName);
    });
  });
  beforeEach(() => {
    Cypress.Cookies.preserveOnce(DEFAULT_COOKIE);
    visit(PROFILE_ROUTE);
    // get key reset modal
    getByCy(RESET_KEY_BUTTON).click();
    shouldBeVisible(getByCy(RESET_KEY_INFO));
  });

  describe('Reset private key is not successful', () => {
    it('validates info checkbox and password', () => {
      // cannot submit empty form
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      // cannot submit without password
      getByCy(READ_CONFIRMATION_CHECKBOX).check();
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
      // cannot submit without checkbox checked
      getByCy(READ_CONFIRMATION_CHECKBOX).uncheck();
      type(getDOMElement(PASSWORD).clear(), STANDARD_PASSWORD);
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_VALIDATION_ERROR));
    });

    it('can submit form with invalid password, receiving error at final step', () => {
      getByCy(READ_CONFIRMATION_CHECKBOX).check();
      // go on with invalid password
      type(getDOMElement(PASSWORD), WRONG_PASSWORD);
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
      // get key step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      // get key download step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX).check();
      getByCy(NEXT_BUTTON).click();
      uploadLocationPrivateKeyFile(operatorKeyFile.path, operatorKeyFile.name);
      // error message on wrong password
      shouldBeVisible(getDOMElement(ANT_NOTIFICATION_NOTICE_MESSAGE));
      shouldBeVisible(getByCy(RESET_KEY_INFO));
    });

    it('can go back while requesting a key file reset', () => {
      getByCy(READ_CONFIRMATION_CHECKBOX).check();
      type(getDOMElement(PASSWORD), STANDARD_PASSWORD);
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      // get key step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      // get key download step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      // go back
      getByCy(BACK_BUTTON).click();
      shouldBeVisible(getByCy(RESET_KEY_INFO));
    });
  });

  describe('Replace and validate keyfile successfully', () => {
    before(() => {
      // change username to validate new keyfile
      changeUserName(NEW_E2E_FIRST_NAME, NEW_E2E_LAST_NAME);
      getDOMElement('@operator').then(operator => {
        const { firstName, lastName } = operator;
        newOperatorKeyFile = getOperatorKeyFile(firstName, lastName);
      });
    });
    it('can submit form with valid credentials and validate new keyfile', () => {
      getByCy(READ_CONFIRMATION_CHECKBOX).check();
      type(getDOMElement(PASSWORD), STANDARD_PASSWORD);
      getByCy(RESET_KEY_SUBMIT_BUTTON).click();
      shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
      // get key step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      // get key download step
      getByCy(DOWNLOAD_PRIVATE_KEY_BUTTON).click();
      // verify checkbox
      shouldBeDisabled(getByCy(NEXT_BUTTON));
      getByCy(PRIVATE_KEY_DOWNLOADED_CHECKBOX).check();
      getByCy(NEXT_BUTTON).click();
      shouldNotExist(getDOMElement(ANT_VALIDATION_ERROR));
      // old file is rejected
      uploadLocationPrivateKeyFile(operatorKeyFile.path, operatorKeyFile.name);
      shouldBeVisible(getDOMElement(ANT_MESSAGE_ERROR));
      // modal stays open
      shouldBeVisible(getDOMElement(ANT_MODAL_CONTENT));
      // upload new private key file
      uploadLocationPrivateKeyFile(
        newOperatorKeyFile.path,
        newOperatorKeyFile.name
      );
      shouldBeVisible(getDOMElement(ANT_MESSAGE_CHECK));
      // Back on Profile
      getByCy('finish').click();
    });
  });
});
