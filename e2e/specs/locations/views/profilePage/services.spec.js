import {
  PRIVACY_MANDATORY_LINK_LABEL,
  PRIVACY_OPTIONAL_LINK_LABEL,
  DPA_LINK_LABEL,
  TOMS_LINK_LABEL,
  TERMS_LINK_LABEL,
} from '../../constants/selectorKeys';
import {
  PRIVACY_MANDATORY_FRAGMENT,
  PROFILE_ROUTE,
  PRIVACY_OPTIONAL_FRAGMENT,
  DPA_FRAGMENT,
  TOMS_FRAGMENT,
  TERMS_CONDITIONS_LINK,
} from '../../constants/routes';
import {
  DPA_DOWNLOAD_FILEPATH,
  PRIVACY_DOWNLOAD_FILEPATH,
  TOMS_DOWNLOAD_FILEPATH,
} from '../../utils/downloads.helper';
import { shouldHaveHref } from '../../utils/assertions';
import { deleteFileIfExists, readFile, visit } from '../../utils/commands';
import { getByCy } from '../../utils/selectors';
import { createTestOperator } from '../../utils/setup';
import { verifyLinkOpensInNewTab } from '../../helpers/ui/validations';

describe('Profile page/ services', () => {
  before(() => {
    createTestOperator();
    visit(PROFILE_ROUTE);
  });
  it('can download the privacy policy for locations which must collect data', () => {
    shouldHaveHref(
      getByCy(PRIVACY_MANDATORY_LINK_LABEL),
      PRIVACY_MANDATORY_FRAGMENT
    );
    getByCy(PRIVACY_MANDATORY_LINK_LABEL).click();
    readFile(PRIVACY_DOWNLOAD_FILEPATH);
    deleteFileIfExists(PRIVACY_DOWNLOAD_FILEPATH);
  });
  it('can download the privacy policy for locations which can collect data', () => {
    shouldHaveHref(
      getByCy(PRIVACY_OPTIONAL_LINK_LABEL),
      PRIVACY_OPTIONAL_FRAGMENT
    );
    getByCy(PRIVACY_OPTIONAL_LINK_LABEL).click();
    readFile(PRIVACY_DOWNLOAD_FILEPATH);
    deleteFileIfExists(PRIVACY_DOWNLOAD_FILEPATH);
  });

  it('can download the DPA', () => {
    shouldHaveHref(getByCy(DPA_LINK_LABEL), DPA_FRAGMENT);
    getByCy(DPA_LINK_LABEL).click();
    readFile(DPA_DOWNLOAD_FILEPATH);
    deleteFileIfExists(DPA_DOWNLOAD_FILEPATH);
  });

  it('can download the toms for locations', () => {
    shouldHaveHref(getByCy(TOMS_LINK_LABEL), TOMS_FRAGMENT);
    getByCy(TOMS_LINK_LABEL).click();
    readFile(TOMS_DOWNLOAD_FILEPATH);
    deleteFileIfExists(TOMS_DOWNLOAD_FILEPATH);
  });

  it('opens terms and conditions link in a new tab', () => {
    shouldHaveHref(getByCy(TERMS_LINK_LABEL), TERMS_CONDITIONS_LINK);
    verifyLinkOpensInNewTab(getByCy(TERMS_LINK_LABEL));
  });
});
