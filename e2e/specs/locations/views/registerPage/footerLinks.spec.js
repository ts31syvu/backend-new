import { REGISTER_ROUTE } from '../../constants/routes';
import { checkLinksInFooter } from '../../helpers/ui/checkLinksInFooter';
import { visit } from '../../utils/commands';

describe('Register page has the correct links in the footer', () => {
  beforeEach(() => {
    visit(REGISTER_ROUTE);
  });
  checkLinksInFooter();
});
