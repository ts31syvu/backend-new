import { APP_ROUTE } from '../helpers/routes';
import { footerLink } from '../helpers/functions';

describe('Check if Header links are clickable and working', () => {
  beforeEach(() => {
    cy.visit(APP_ROUTE);
  });
  describe('Open Menu dropdown', () => {
    it('expect FAQ link to be clickable', () => {
      footerLink('linkFaq');
    });
    it('expect GitLab link to be clickable', () => {
      footerLink('linkGit');
    });
    it('expect Terms & Condition link to be clickable', () => {
      footerLink('linkTermsCondition');
    });
  });
});
