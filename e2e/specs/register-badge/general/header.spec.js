import { APP_ROUTE } from '../helpers/routes';
import { dropdownLinkTest } from '../helpers/functions';
import { shouldBeVisible } from '../../locations/utils/assertions';
import { getByCy } from '../../health-department/utils/selectors';
import { LINK_MENU_ICON } from '../../health-department/constants/selectorKeys';

describe('Check if Header links are clickable and working', () => {
  beforeEach(() => {
    cy.visit(APP_ROUTE);
  });
  describe('Header Menu dropdown', () => {
    it('expect Header dropdown to be visible', () => {
      shouldBeVisible(getByCy(LINK_MENU_ICON));
    });
  });
  describe('License link', () => {
    it('expect License link to be clickable', () => {
      dropdownLinkTest(0);
    });
    it('expect Privacy Policy link to be clickable', () => {
      dropdownLinkTest(1);
    });
  });
});
