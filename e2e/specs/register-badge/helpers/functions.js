export function footerLink(linkName) {
  cy.getByCy('footerLinks').should('exist').should('be.visible');
  cy.getByCy(linkName).click();
}

export function dropdownLinkTest(linkNumber) {
  cy.getByCy('linkMenuIcon').trigger('mouseover');
  // eslint-disable-next-line cypress/no-force
  cy.get('.ant-dropdown-menu li a').eq(linkNumber).click({ force: true });
}
