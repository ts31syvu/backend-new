const { baseUrl } = Cypress.config();

export const interceptBloomFilterCall = () =>
  cy.intercept({
    method: 'GET',
    url: `${baseUrl}/api/v3/badges/bloomFilter`,
  });
