import {
  DELETED_REGISTERED_BADGE,
  REGISTERED_BADGE,
  UNREGISTERED_BADGE,
} from '../constants/badges';
import { inputQRCodeData } from '../helpers/input';
import { getByCy, getDOMElement } from '../utils/selectors';
import { shouldBeVisible } from '../utils/assertions';
import {
  NO_BADGE_USER_DATA_ERROR,
  BADGE_CHECKIN_SUCCESS,
} from '../constants/selectorKeys';
import { visit } from '../../locations/utils/commands';
import { createTestOperator } from '../../locations/utils/setup';
import {
  createTestGroup,
  regenerateBloomFilter,
} from '../../locations/network/api';
import { getGroupPayload } from '../../locations/helpers/api/groups';
import { SCANNER_ROUTE } from '../constants/routes';
import { interceptBloomFilterCall } from './checkBloomFilter.helper';

describe('Check bloom filter for unregistered and deleted badges', () => {
  before(() => {
    regenerateBloomFilter();
    interceptBloomFilterCall().as('getBloomFilter');
    createTestOperator();
    createTestGroup(getGroupPayload()).then(group => {
      visit(`${SCANNER_ROUTE}/${group.scannerAccessId}`);
    });
    cy.wait('@getBloomFilter');
  });

  it('can check in a registered badge', () => {
    inputQRCodeData(REGISTERED_BADGE);
    shouldBeVisible(getByCy(BADGE_CHECKIN_SUCCESS));
  });

  it('cannot check in a unregistered badge', () => {
    inputQRCodeData(UNREGISTERED_BADGE);
    shouldBeVisible(getDOMElement(NO_BADGE_USER_DATA_ERROR));
  });

  it('cannot check in a deleted registered badge', () => {
    inputQRCodeData(DELETED_REGISTERED_BADGE);
    shouldBeVisible(getDOMElement(NO_BADGE_USER_DATA_ERROR));
  });
});
