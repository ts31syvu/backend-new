export const shouldBeVisible = element => element.should('be.visible');

export const shouldNotExist = element => element.should('not.exist');

export const shouldBeDisabled = element => element.should('be.disabled');

export const shouldNotContain = (element, value) =>
  element.should('not.contain', value);

export const shouldContain = (element, value) =>
  element.should('contain', value);
