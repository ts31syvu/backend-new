export const type = (element, value) => element.type(value, { delay: 0 });

export const visit = url => cy.visit(url);
