// LOCATIONS

// Location Login
// export const CONFIRM_BUTTON = 'complete';

// Loation Header
// export const OPEN_DATA_REQUESTS_BUTTON = 'dataRequests';

// Location overview

// HEALTH DEPARTMENT

// export const OPEN_PROCESS_BUTTON = 'requestGroupData';

// Health Department Data Requests Page
// export const COMPLETE_DATA_TRANSFER = 'completeDataTransfer';
// export const CONTACT_PERSONS_TABLE = '#contactPersonsTable';

// Confirm data release modal
// export const SHARE_NOW_BUTTON = 'next';

// ANT DESIGN

// export const ANT_MODAL = '.ant-modal';

// PREFIX
// export const GROUP_PREFIX = 'group_';
// export const CONFIRMED_LOCATION_PREFIX = 'confirmedLocation_';

// Input fields
export const FIRST_NAME_INPUT_FIELD = 'firstName';
export const LAST_NAME_INPUT_FIELD = 'lastName';
export const STREET_NAME_INPUT_FIELD = 'street';
export const HOUSE_NUMBER_INPUT_FIELD = 'number';
export const CITY_NAME_INPUT_FIELD = 'city';
export const ZIP_CODE_INPUT_FIELD = 'zip';
export const PHONE_NUMBER_INPUT_FIELD = 'phone';
export const EMAIL_INPUT_FIELD = 'email';

// Contact form
export const ACCEPT_AGB_CHECKBOX = 'acceptAGB';
export const SAVE_BUTTON = 'contactSubmit';

// Ant design
export const ANT_FORM_ITEM_ERROR_NOTIFICATION = '.ant-form-item-has-error';
export const ANT_SUCCESS_NOTIFICATION_MESSAGE = '.ant-notification-notice';
