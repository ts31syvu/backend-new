import faker from 'faker';
import { shouldNotExist, shouldBeVisible } from '../utils/assertions';
import { type, check } from '../utils/commands';
import { getByCy, getDOMElement } from '../utils/selectors';
import {
  FIRST_NAME_INPUT_FIELD,
  LAST_NAME_INPUT_FIELD,
  STREET_NAME_INPUT_FIELD,
  HOUSE_NUMBER_INPUT_FIELD,
  CITY_NAME_INPUT_FIELD,
  ZIP_CODE_INPUT_FIELD,
  PHONE_NUMBER_INPUT_FIELD,
  EMAIL_INPUT_FIELD,
  ACCEPT_AGB_CHECKBOX,
  ANT_FORM_ITEM_ERROR_NOTIFICATION,
  ANT_SUCCESS_NOTIFICATION_MESSAGE,
  SAVE_BUTTON,
} from '../constants/selectorKeys';

faker.locale = 'de';

export function fillForm({
  firstName = faker.name.firstName().replace("'", ' '),
  lastName = faker.name.lastName().replace("'", ' '),
  street = faker.address.streetName().replace("'", ' '),
  houseNumber = faker.datatype.number({
    min: 10,
    max: 50,
  }),
  zip = faker.address.zipCode('#####'),
  city = faker.address.city().replace("'", ' '),
  phoneNumber = faker.phone.phoneNumber('0049176#######'),
  email = faker.internet.email(),
} = {}) {
  type(getByCy(FIRST_NAME_INPUT_FIELD), firstName);
  type(getByCy(LAST_NAME_INPUT_FIELD), lastName);
  type(getByCy(STREET_NAME_INPUT_FIELD), street);
  type(getByCy(HOUSE_NUMBER_INPUT_FIELD), houseNumber);
  type(getByCy(CITY_NAME_INPUT_FIELD), city);
  type(getByCy(ZIP_CODE_INPUT_FIELD), zip);
  type(getByCy(PHONE_NUMBER_INPUT_FIELD), phoneNumber);
  type(getByCy(EMAIL_INPUT_FIELD), email);

  check(getByCy(ACCEPT_AGB_CHECKBOX));
  getByCy(SAVE_BUTTON).click();
  shouldNotExist(getDOMElement(ANT_FORM_ITEM_ERROR_NOTIFICATION));
  shouldBeVisible(getDOMElement(ANT_SUCCESS_NOTIFICATION_MESSAGE));

  return {
    firstName,
    lastName,
    street,
    houseNumber,
    zip,
    city,
    phoneNumber,
    email,
  };
}
