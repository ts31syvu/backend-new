import moment from 'moment';
import { fillForm } from './contactForm.helper';
import { loginHealthDepartment } from '../../health-department/helpers/api/auth';
import { signHealthDepartment } from '../../health-department/helpers/api/signHealthDepartment';
import { removeHDPrivateKeyFile } from '../helpers/functions';

import {
  setDatePickerStartDate,
  setDatePickerEndDate,
} from '../../health-department/helpers/ui/tracking';
import {
  CONTACT_FORM_ROUTE,
  DATA_TRANSFERS_ROUTE,
} from '../../locations/constants/routes';
import { addHealthDepartmentPrivateKeyFile } from '../../health-department/helpers/ui/handlePrivateKeyFile';
import { getByCy, getDOMElement } from '../utils/selectors';
import { SHARE_NOW_BUTTON } from '../../locations/constants/selectorKeys';
import {
  SEARCH_GROUP_BUTTON,
  ANT_MODAL,
  GROUP_NAME_INPUT_FIELD,
  START_GROUP_SEARCH_BUTTON,
  REQUEST_GROUP_DATA_BUTTON,
  PROCESS_ENTRY,
  COMPLETE_DATA_TRANSFER,
  CONTACT_PERSONS_TABLE,
  GROUP_PREFIX,
  CONFIRMED_LOCATION_PREFIX,
  CONTACT_LOCATION_PREFIX,
  ANT_POPOVER_BUTTONS,
} from '../../health-department/constants/selectorKeys';
import { shouldBeVisible } from '../utils/assertions';
import { type } from '../utils/commands';
import { visit } from '../../health-department/utils/commands';
import { createTestGroup, login } from '../../locations/network/api';
import { createTestOperator } from '../../locations/utils/setup';
import { getGroupPayload } from '../../locations/helpers/api/groups';
import { STANDARD_PASSWORD } from '../../locations/utils/setup.helper';
import {
  KEY_ROTATION,
  PROCESS_UUID,
  TRACING_PROCESS,
} from '../../health-department/constants/aliases';

const yesterdayDate = moment().subtract(1, 'days').format('DD.MM.YYYY');
const tomorrowDate = moment().add(1, 'days').format('DD.MM.YYYY');

let testOperator;
let testGroup;

context('Workflow', { retries: 2 }, () => {
  describe('when the Luca workflow will checked with contact form data', () => {
    beforeEach(() => {
      cy.intercept('POST', '/api/v4/keys/daily/rotate').as(KEY_ROTATION);
      cy.intercept('POST', '/api/v4/locationTransfers/').as(TRACING_PROCESS);
      // login and sign HD to enable checkin with contactform
      loginHealthDepartment();
      addHealthDepartmentPrivateKeyFile();
      signHealthDepartment();
      cy.reload();
      addHealthDepartmentPrivateKeyFile();
      cy.wait(`@${KEY_ROTATION}`);
      cy.logoutHD();
      // create location and operator
      createTestOperator(true).then(operator => {
        testOperator = operator;
      });
      createTestGroup(getGroupPayload()).then(group => {
        testGroup = group;
      });
      cy.logoutLocations();
    });

    afterEach(() => {
      removeHDPrivateKeyFile();
    });

    it('decrypt all data successfully', () => {
      // Checkin with contact form
      visit(`${CONTACT_FORM_ROUTE}/${testGroup.formId}`);
      const users = [];
      for (let index = 0; index < 2; index += 1) {
        users.push(fillForm());
      }
      // Request data from testing Location
      loginHealthDepartment();
      addHealthDepartmentPrivateKeyFile();
      getByCy(SEARCH_GROUP_BUTTON).click();
      shouldBeVisible(getDOMElement(ANT_MODAL));
      type(getByCy(GROUP_NAME_INPUT_FIELD), testGroup.groupName);
      getByCy(START_GROUP_SEARCH_BUTTON).click();
      getByCy(GROUP_PREFIX + testGroup.groupName).click();
      setDatePickerStartDate(yesterdayDate);
      setDatePickerEndDate(tomorrowDate);
      // submit process opening
      getByCy(REQUEST_GROUP_DATA_BUTTON).click();
      // Wait for request to complete
      cy.wait(`@${TRACING_PROCESS}`);
      getDOMElement(`@${TRACING_PROCESS}`).then(({ response }) => {
        cy.wrap(response.body.tracingProcessId).as(PROCESS_UUID);
      });
      // Find and get the entry
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        getByCy(`${PROCESS_ENTRY}-${uuid}`).click();
      });
      getByCy(CONTACT_LOCATION_PREFIX + testGroup.groupName)
        .first()
        .click();
      shouldBeVisible(getDOMElement(ANT_POPOVER_BUTTONS));
      getDOMElement(ANT_POPOVER_BUTTONS)
        .eq(1)
        .then($element => {
          cy.wrap($element).click();
        });
      cy.logoutHD();
      // Share testing data from testing Location
      login({ username: testOperator.email, password: STANDARD_PASSWORD });
      visit(DATA_TRANSFERS_ROUTE);
      cy.stubNewWindow();
      getByCy(COMPLETE_DATA_TRANSFER, { timeout: 10000 }).first().click();
      getByCy(SHARE_NOW_BUTTON, { timeout: 10000 }).click();
      cy.logoutLocations();
      // Check requested data in Health department
      loginHealthDepartment();
      addHealthDepartmentPrivateKeyFile();
      getDOMElement(`@${PROCESS_UUID}`).then(uuid => {
        getByCy(`${PROCESS_ENTRY}-${uuid}`).click();
      });
      getByCy(CONFIRMED_LOCATION_PREFIX + testGroup.groupName, {
        timeout: 10000,
      })
        .first()
        .click();
      shouldBeVisible(getDOMElement(ANT_MODAL));
      for (const user of users) {
        getDOMElement(CONTACT_PERSONS_TABLE, { timeout: 10000 }).contains(
          user.lastName
        );
        getDOMElement(CONTACT_PERSONS_TABLE).contains(user.firstName);
        getDOMElement(CONTACT_PERSONS_TABLE).contains(user.phoneNumber);
      }
    });
  });
});
