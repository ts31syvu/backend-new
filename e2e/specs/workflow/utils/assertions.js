export const shouldBeVisible = element => element.should('be.visible');

export const shouldNotExist = element => element.should('not.exist');

export const shouldBeDisabled = element => element.should('be.disabled');

export const contains = value => cy.contains(value);
