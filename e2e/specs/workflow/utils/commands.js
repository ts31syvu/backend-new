export const type = (element, value) => element.type(value, { delay: 0 });

export const check = element => element.check();

export const log = message => cy.log(message);
