// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands';

import 'cypress-file-upload';
// eslint-disable-next-line import/no-extraneous-dependencies
import 'cypress-fail-fast';

const MAX_TEST_EXECUTION = 120000;

let timeoutId;

beforeEach(() => {
  cy.intercept('/', request => {
    request.headers['X-Forwarded-For'] = '10.0.0.1';
  });

  timeoutId = setTimeout(() => {
    // eslint-disable-next-line no-console
    console.log('ERROR');
    throw new Error('TestExecutionTooLongError');
  }, MAX_TEST_EXECUTION);
});

afterEach(() => {
  clearTimeout(timeoutId);
});
// eslint-disable-next-line consistent-return
Cypress.on('uncaught:exception', error => {
  // returning false here prevents Cypress from failing the test
  // deal with issue https://github.com/quasarframework/quasar/issues/2233
  // eslint-disable-next-line no-console
  console.log('got uncaught execp error');
  if (error.message.includes('ResizeObserver')) {
    return false;
  }
  // eslint-disable-next-line no-console
  console.log('got uncaught execp error not false', error, error.message);

  if (error.message.includes('TestExecutionTooLongError')) {
    // eslint-disable-next-line no-console
    console.log('got uncaught execp error with TooLongMessage');
    return true;
  }
  return false;
});

// Alternatively you can use CommonJS syntax:
// require('./commands')
