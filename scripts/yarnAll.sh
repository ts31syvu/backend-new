#!/bin/bash
set -euo pipefail

BACKEND_SERVICES="backend"
SERVICES="backend contact-form register-badge health-department locations scanner webapp"

for SERVICE in ${SERVICES}
do
  pushd "services/$SERVICE"
  yarn "$@"
  popd
done

# workaround for our loved m1 users
for SERVICE in ${BACKEND_SERVICES}
do
  pushd "services/$SERVICE"
  yarn add-no-save sqlite3
  popd
done