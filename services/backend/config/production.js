const version = require('../version.json');

module.exports = {
  e2e: false,
  debug: false,
  enableJobRoutes: false,
  skipSmsVerification: false,
  loglevel: 'info',
  defaultHttpLogLevel: 'info',
  hostname: 'app.luca-app.de',
  shutdownDelay: 15,
  version,
  jwt: {
    expiration: '60s',
  },
};
