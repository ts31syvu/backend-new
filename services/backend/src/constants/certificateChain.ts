import config from 'config';

export const certificateChain = [
  config.get<string>('certs.dtrust.root'),
  config.get<string>('certs.dtrust.basic'),
];
