export enum PrefixType {
  NAME = 'name',
  PHONE = 'phone',
}

export const PrefixTableMapping: Record<PrefixType, string> = {
  [PrefixType.NAME]: 'namePrefix',
  [PrefixType.PHONE]: 'phonePrefix',
};
