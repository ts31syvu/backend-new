export enum RiskLevel {
  RISK_LEVEL_1 = 1,
  RISK_LEVEL_2 = 2,
  RISK_LEVEL_3 = 3,
  RISK_LEVEL_4 = 4,
}
