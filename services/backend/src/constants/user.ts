export enum UserType {
  OPERATOR = 'Operator',
  HEALTH_DEPARTMENT_EMPLOYEE = 'HealthDepartmentEmployee',
  OPERATOR_DEVICE = 'OperatorDevice',
}
