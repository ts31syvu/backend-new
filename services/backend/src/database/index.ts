/* eslint-disable import/no-cycle, max-lines */
import config from 'config';
import {
  Sequelize,
  Options,
  ConnectionError,
  ConnectionRefusedError,
  ConnectionTimedOutError,
} from 'sequelize';
import sequelizeStream from 'node-sequelize-stream';
import logger from 'utils/logger';
import * as client from 'utils/metrics';
import dbConfig from 'database/config';

import { initBadgeGenerators } from './models/badgeGenerators';
import { initBadgePublicKeys } from './models/badgePublicKeys';
import { initCachFallbacks } from './models/cacheFallback';
import { initChallenges } from './models/challenge';
import {
  initConnectContact,
  associateConnectContact,
} from './models/connectContact';
import {
  initConnectConversation,
  associateConnectConversation,
} from './models/connectConversation';
import {
  initConnectMessage,
  associateConnectMessage,
} from './models/connectMessage';
import {
  initConnectSearch,
  associateConnectSearch,
} from './models/connectSearch';
import {
  initConnectSearchProcess,
  associateConnectSearchProcess,
} from './models/connectSearchProcess';
import { initDailyPublicKeys } from './models/dailyPublicKeys';
import { initDummyTraces, associateDummyTrace } from './models/dummyTrace';
import {
  initEmailActivations,
  associateEmailActivation,
} from './models/emailActivation';
import { initEncryptedBadgePrivateKeys } from './models/encryptedBadgePrivateKeys';
import { initEncryptedDailyPrivateKeys } from './models/encryptedDailyPrivateKeys';
import {
  initAdditionalDataSchemas,
  associateAdditionalDataSchema,
} from './models/additionalDataSchema';
import { initFeatureFlags } from './models/featureFlags';
import { initFeatureRollouts } from './models/featureRollouts';
import {
  initHealthDepartments,
  associateHealthDepartment,
} from './models/healthDepartment';
import {
  initHealthDepartmentAuditLogs,
  associateHealthDepartmentAuditLog,
} from './models/healthDepartmentAuditLog';
import {
  initHealthDepartmentEmployees,
  associateHealthDepartmentEmployee,
} from './models/healthDepartmentEmployee';
import { initInternalAccessUser } from './models/internalAccessUser';
import { initIPAddressAllowLists } from './models/ipAddressAllowList';
import { initIPAddressBlockList } from './models/ipAddressBlockList';
import { initLocations, associateLocation } from './models/location';
import {
  initLocationGroups,
  associateLocationGroup,
} from './models/locationGroup';
import {
  initLocationMenus,
  associateLocationMenu,
} from './models/locationMenu';
import {
  initLocationTransfers,
  associateLocationTransfer,
} from './models/locationTransfer';
import {
  initLocationTransferTraces,
  associateLocationTransferTrace,
} from './models/locationTransferTrace';
import { initLocationURLs, associateLocationURLs } from './models/locationUrl';
import { initLocationURLAllowRules } from './models/locationUrlAllowRules';
import { initLocationURLDenyRules } from './models/locationUrlDenyRules';
import { initLocationURLDomainBlockList } from './models/locationUrlDomainBlockList';
import { initNotificationChunks } from './models/notificationChunk';
import {
  initNotificationConfigs,
  associateNotificationConfig,
} from './models/notificationConfigs';
import {
  initNotificationMessages,
  associateNotificationMessage,
} from './models/notificationMessage';
import { initOperators, associateOperator } from './models/operator';
import {
  initOperatorDevices,
  associateOperatorDevice,
} from './models/operatorDevice';
import { initOperatorKeyHistories } from './models/operatorKeyHistory';
import { initPasswordReset } from './models/passwordReset';
import { initPowChallenge } from './models/powChallenge';
import { initPowConfig } from './models/powConfig';
import { initPhoneNumberBlockList } from './models/phoneNumberBlockList';
import { initRiskLevels, associateRiskLevel } from './models/riskLevel';
import { initSessions } from './models/session';
import { initSigningToolDownload } from './models/signingToolDownload';
import { initSMSCHallenge } from './models/smsChallenge';
import { initSupportedZipCodes } from './models/supportedZipCodes';
import { initTestProviders } from './models/testProvider';
import { initTestRedeems } from './models/testRedeems';
import { initTraces, associateTrace } from './models/trace';
import { initTraceData, associateTraceData } from './models/traceData';
import {
  initTracingProcesses,
  associateTracingProcess,
} from './models/tracingProcess';
import { initUsers } from './models/users';
import { initUserTransfers } from './models/userTransfer';
import { initZipCodeMapping } from './models/zipCodeMapping';
import {
  initZipCodeResponsibility,
  associateZipCodeResponsibility,
} from './models/zipCodeResponsibility';
import {
  initRetentionPolicy,
  associateRetentionPolicy,
} from './models/retentionPolicy';
import { initJobCompletions } from './models/jobCompletions';
import { initSeenOverlappingLocationTranfers } from './models/seenOverlappingLocationTranfers';

const environment = config.util.getEnv('NODE_ENV');
const databaseConfig = dbConfig[environment as keyof typeof dbConfig];

export const database = new Sequelize({
  ...(databaseConfig as Partial<Options>),
  logging: (message, time) => logger.debug({ time, message }),
  logQueryParameters: false,
  benchmark: true,
  retry: {
    name: 'database',
    max: 60 * 2,
    backoffBase: 500,
    backoffExponent: 1,
    report: (_message: string, info: { $current: number }) => {
      if (info.$current === 1) return;
      logger.warn(`Trying to connect to database (try #${info.$current})`);
    },
    match: [ConnectionError, ConnectionRefusedError, ConnectionTimedOutError],
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } as any,
});

const counter = new client.Counter({
  name: 'sequelize_table_creation_count',
  help: 'Total database rows created since process start.',
  labelNames: ['tableName'],
});

database.addHook('beforeCreate', instance => {
  counter.inc({
    tableName: instance.constructor.name,
  });
});

database.addHook('beforeBulkCreate', instances => {
  if (instances.length <= 0) return;
  counter.inc(
    {
      tableName: instances[0].constructor.name,
    },
    instances.length
  );
});

export const AdditionalDataSchema = initAdditionalDataSchemas(database);
export const BadgeGenerator = initBadgeGenerators(database);
export const BadgePublicKey = initBadgePublicKeys(database);
export const CacheFallback = initCachFallbacks(database);
export const Challenge = initChallenges(database);
export const ConnectContact = initConnectContact(database);
export const ConnectConversation = initConnectConversation(database);
export const ConnectMessage = initConnectMessage(database);
export const ConnectSearch = initConnectSearch(database);
export const ConnectSearchProcess = initConnectSearchProcess(database);
export const DailyPublicKey = initDailyPublicKeys(database);
export const DummyTrace = initDummyTraces(database);
export const EmailActivation = initEmailActivations(database);
export const EncryptedBadgePrivateKey = initEncryptedBadgePrivateKeys(database);
export const EncryptedDailyPrivateKey = initEncryptedDailyPrivateKeys(database);
export const FeatureFlag = initFeatureFlags(database);
export const FeatureRollout = initFeatureRollouts(database);
export const HealthDepartment = initHealthDepartments(database);
export const HealthDepartmentAuditLog = initHealthDepartmentAuditLogs(database);
export const HealthDepartmentEmployee = initHealthDepartmentEmployees(database);
export const internalAccessUser = initInternalAccessUser(database);
export const IPAddressAllowList = initIPAddressAllowLists(database);
export const IPAddressBlockList = initIPAddressBlockList(database);
export const Location = initLocations(database);
export const LocationGroup = initLocationGroups(database);
export const LocationMenu = initLocationMenus(database);
export const LocationTransfer = initLocationTransfers(database);
export const LocationTransferTrace = initLocationTransferTraces(database);
export const LocationURL = initLocationURLs(database);
export const LocationURLAllowRules = initLocationURLAllowRules(database);
export const LocationURLDenyRules = initLocationURLDenyRules(database);
export const LocationURLDomainBlockList = initLocationURLDomainBlockList(
  database
);
export const NotificationChunk = initNotificationChunks(database);
export const NotificationConfig = initNotificationConfigs(database);
export const NotificationMessage = initNotificationMessages(database);
export const Operator = initOperators(database);
export const OperatorDevice = initOperatorDevices(database);
export const OperatorKeyHistory = initOperatorKeyHistories(database);
export const PasswordReset = initPasswordReset(database);
export const PowChallenge = initPowChallenge(database);
export const PowConfig = initPowConfig(database);
export const PhoneNumberBlockList = initPhoneNumberBlockList(database);
export const RiskLevel = initRiskLevels(database);
export const SigningToolDownload = initSigningToolDownload(database);
export const SMSChallenge = initSMSCHallenge(database);
export const SupportedZipCode = initSupportedZipCodes(database);
export const TestProvider = initTestProviders(database);
export const TestRedeem = initTestRedeems(database);
export const Trace = initTraces(database);
export const TraceData = initTraceData(database);
export const TracingProcess = initTracingProcesses(database);
export const SeenOverlappingLocationTranfers = initSeenOverlappingLocationTranfers(
  database
);
export const RetentionPolicy = initRetentionPolicy(database);
export const User = initUsers(database);
export const UserTransfer = initUserTransfers(database);
export const Session = initSessions(database);
export const ZipCodeMapping = initZipCodeMapping(database);
export const ZipCodeResponsibility = initZipCodeResponsibility(database);
export const JobCompletion = initJobCompletions(database);

const models = {
  AdditionalDataSchema,
  BadgeGenerator,
  BadgePublicKey,
  CacheFallback,
  Challenge,
  ConnectContact,
  ConnectConversation,
  ConnectMessage,
  ConnectSearch,
  ConnectSearchProcess,
  DailyPublicKey,
  DummyTrace,
  EmailActivation,
  EncryptedBadgePrivateKey,
  EncryptedDailyPrivateKey,
  FeatureFlag,
  FeatureRollout,
  HealthDepartment,
  HealthDepartmentAuditLog,
  HealthDepartmentEmployee,
  IPAddressAllowList,
  IPAddressBlockList,
  Location,
  LocationGroup,
  LocationMenu,
  LocationTransfer,
  LocationTransferTrace,
  LocationURL,
  LocationURLAllowRules,
  LocationURLDenyRules,
  LocationURLDomainBlockList,
  NotificationChunk,
  NotificationConfig,
  NotificationMessage,
  Operator,
  OperatorDevice,
  OperatorKeyHistory,
  PasswordReset,
  PowChallenge,
  PowConfig,
  PhoneNumberBlockList,
  SeenOverlappingLocationTranfers,
  RiskLevel,
  Session,
  SigningToolDownload,
  SMSChallenge,
  SupportedZipCode,
  TestProvider,
  TestRedeem,
  Trace,
  TraceData,
  TracingProcess,
  RetentionPolicy,
  User,
  UserTransfer,
  ZipCodeMapping,
  ZipCodeResponsibility,
  JobCompletion,
};

export type Models = typeof models;

associateAdditionalDataSchema(models);
associateConnectContact(models);
associateConnectConversation(models);
associateConnectMessage(models);
associateConnectSearch(models);
associateConnectSearchProcess(models);
associateDummyTrace(models);
associateEmailActivation(models);
associateHealthDepartment(models);
associateHealthDepartmentAuditLog(models);
associateHealthDepartmentEmployee(models);
associateLocation(models);
associateLocationGroup(models);
associateLocationMenu(models);
associateLocationTransfer(models);
associateLocationTransferTrace(models);
associateLocationURLs(models);
associateNotificationConfig(models);
associateNotificationMessage(models);
associateOperator(models);
associateOperatorDevice(models);
associateRetentionPolicy(models);
associateRiskLevel(models);
associateTrace(models);
associateTraceData(models);
associateTracingProcess(models);
associateZipCodeResponsibility(models);

sequelizeStream(database);
