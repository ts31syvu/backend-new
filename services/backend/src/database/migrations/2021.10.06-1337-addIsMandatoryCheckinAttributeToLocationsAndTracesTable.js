module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Locations',
        'isContactDataMandatory',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );

      await queryInterface.addColumn(
        'Traces',
        'isContactDataMandatory',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );
    });

    await queryInterface.addIndex('Locations', {
      fields: ['isContactDataMandatory'],
      concurrently: true,
    });

    await queryInterface.addIndex('Traces', {
      fields: ['isContactDataMandatory'],
      concurrently: true,
    });
  },
  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeIndex(
        'LocationTransfers',
        'traces_is_contact_data_mandatory',
        {
          transaction,
        }
      );

      await queryInterface.removeIndex(
        'LocationTransfers',
        'location_is_contact_data_mandatory',
        { transaction }
      );

      await queryInterface.removeColumn('Locations', 'isContactDataMandatory', {
        transaction,
      });

      await queryInterface.removeColumn('Traces', 'isContactDataMandatory', {
        transaction,
      });
    });
  },
};
