module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Traces',
        'isContactDataIncluded',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );
    });

    await queryInterface.addIndex('Traces', {
      fields: ['isContactDataIncluded'],
      concurrently: true,
    });
  },
  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeIndex(
        'LocationTransfers',
        'traces_is_contact_data_included',
        {
          transaction,
        }
      );

      await queryInterface.removeColumn('Traces', 'isContactDataIncluded', {
        transaction,
      });
    });
  },
};
