module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransferTraces',
        'isContactDataIncluded',
        {
          type: DataTypes.BOOLEAN,
          allowNull: false,
          defaultValue: true,
        },
        {
          transaction,
        }
      );
    });

    await queryInterface.addIndex('LocationTransferTraces', {
      fields: ['isContactDataIncluded'],
      concurrently: true,
    });
  },
  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransferTraces',
        'isContactDataIncluded',
        {
          transaction,
        }
      );
    });
  },
};
