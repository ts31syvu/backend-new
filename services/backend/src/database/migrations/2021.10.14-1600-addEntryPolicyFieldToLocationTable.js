module.exports = {
  up: async (queryInterface, DataTypes) =>
    queryInterface.addColumn('Locations', 'entryPolicy', {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.ENUM(['2G', '3G']),
    }),
  down: queryInterface => {
    return queryInterface.removeColumn('Locations', 'entryPolicy');
  },
};
