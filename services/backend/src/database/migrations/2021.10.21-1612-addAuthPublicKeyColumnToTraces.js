module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('Traces', 'authPublicKey', {
      type: DataTypes.STRING(88),
      allowNull: true,
    });
  },
  down: async queryInterface => {
    return queryInterface.removeColumn('Traces', 'authPublicKey');
  },
};
