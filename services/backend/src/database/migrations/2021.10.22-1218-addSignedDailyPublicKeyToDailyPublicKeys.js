module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('DailyPublicKeys', 'signedPublicDailyKey', {
      type: DataTypes.STRING(1024),
    });
  },
  down: async queryInterface => {
    await queryInterface.removeColumn(
      'DailyPublicKeys',
      'signedPublicDailyKey'
    );
  },
};
