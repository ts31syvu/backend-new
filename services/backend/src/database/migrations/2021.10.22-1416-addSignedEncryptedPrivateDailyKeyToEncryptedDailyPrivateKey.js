module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
      }
    );
  },
  down: async queryInterface => {
    await queryInterface.removeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey'
    );
  },
};
