module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'initialTracesCount',
        {
          allowNull: true,
          type: DataTypes.INTEGER,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn(
        'LocationTransfers',
        'initialTracesCount',
        {
          transaction,
        }
      );
    });
  },
};
