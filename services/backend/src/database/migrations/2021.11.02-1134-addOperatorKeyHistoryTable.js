module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('OperatorKeyHistories', {
      operatorId: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      privateKeySecret: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
    });
  },
  down: queryInterface => {
    return queryInterface.dropTable('OperatorKeyHistories');
  },
};
