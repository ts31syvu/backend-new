module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('NotificationConfigs', {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      level: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      key: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      value: {
        allowNull: false,
        type: DataTypes.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
    });
  },

  /**
   * uuid, departmentId, key, value
   * @param queryInterface
   * @returns {Promise<void>}
   */
  down: queryInterface => {
    return queryInterface.dropTable('NotificationConfigs');
  },
};
