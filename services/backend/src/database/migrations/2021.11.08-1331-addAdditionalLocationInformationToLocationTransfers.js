module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'LocationTransfers',
        'masks',
        {
          allowNull: true,
          type: DataTypes.STRING,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationTransfers',
        'ventilation',
        {
          allowNull: true,
          type: DataTypes.STRING,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationTransfers',
        'roomHeight',
        {
          allowNull: true,
          type: DataTypes.FLOAT,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationTransfers',
        'roomWidth',
        {
          allowNull: true,
          type: DataTypes.FLOAT,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
      await queryInterface.addColumn(
        'LocationTransfers',
        'roomDepth',
        {
          allowNull: true,
          type: DataTypes.FLOAT,
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('LocationTransfers', 'masks', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransfers', 'ventilation', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransfers', 'roomHeight', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransfers', 'roomWidth', {
        transaction,
      });
      await queryInterface.removeColumn('LocationTransfers', 'roomDepth', {
        transaction,
      });
    });
  },
};
