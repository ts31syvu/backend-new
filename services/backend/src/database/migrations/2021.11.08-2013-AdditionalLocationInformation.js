module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('Locations', 'masks', {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    });
    await queryInterface.addColumn('Locations', 'ventilation', {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    });
    await queryInterface.addColumn('Locations', 'entryPolicyInfo', {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: null,
    });
    await queryInterface.addColumn('Locations', 'roomHeight', {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: null,
    });
    await queryInterface.addColumn('Locations', 'roomWidth', {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: null,
    });
    await queryInterface.addColumn('Locations', 'roomDepth', {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: null,
    });
  },

  down: async queryInterface => {
    await queryInterface.removeColumn('Locations', 'masks');
    await queryInterface.removeColumn('Locations', 'ventilation');
    await queryInterface.removeColumn('Locations', 'entryPolicyInfo');
    await queryInterface.removeColumn('Locations', 'roomHeight');
    await queryInterface.removeColumn('Locations', 'roomWidth');
    await queryInterface.removeColumn('Locations', 'roomDepth');
  },
};
