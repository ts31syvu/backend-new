module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Operators',
        'language',
        {
          allowNull: true,
          type: DataTypes.STRING(5),
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Operators', 'language', {
        transaction,
      });
    });
  },
};
