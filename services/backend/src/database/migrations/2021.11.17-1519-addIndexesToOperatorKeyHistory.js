module.exports = {
  up: async queryInterface => {
    await queryInterface.addIndex('OperatorKeyHistories', {
      fields: ['operatorId'],
    });

    await queryInterface.addIndex('OperatorKeyHistories', {
      fields: ['publicKey'],
    });
  },
  down: async queryInterface => {
    await queryInterface.removeIndex(
      'OperatorKeyHistories',
      'operator_key_histories_operator_id'
    );
    await queryInterface.removeIndex(
      'OperatorKeyHistories',
      'operator_key_histories_public_key'
    );
  },
};
