module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLAllowRules',
        {
          rule: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLAllowRules', {
        fields: ['rule'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('LocationURLAllowRules');
  },
};
