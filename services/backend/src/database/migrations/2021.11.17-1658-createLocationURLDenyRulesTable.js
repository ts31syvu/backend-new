module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLDenyRules',
        {
          rule: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLDenyRules', {
        fields: ['rule'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('LocationURLDenyRules');
  },
};
