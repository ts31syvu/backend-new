module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'LocationURLDomainBlockList',
        {
          domain: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true,
          },
        },
        { transaction }
      );

      await queryInterface.addIndex('LocationURLDomainBlockList', {
        fields: ['domain'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('LocationURLDomainBlockList');
  },
};
