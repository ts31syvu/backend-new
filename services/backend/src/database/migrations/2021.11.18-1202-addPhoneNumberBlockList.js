module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PhoneNumberBlockList',
        {
          phoneNumber: {
            type: DataTypes.STRING,
            primaryKey: true,
            allowNull: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
          },
          updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
          },
        },
        { transaction }
      );
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('PhoneNumberBlockList');
  },
};
