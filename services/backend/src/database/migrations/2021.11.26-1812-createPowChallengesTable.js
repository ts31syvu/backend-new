module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PowChallenges',
        {
          uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          type: {
            type: DataTypes.STRING(255),
            allowNull: false,
          },
          difficulty: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
          n: {
            type: DataTypes.STRING(2048),
            allowNull: false,
          },
          t: {
            type: DataTypes.STRING(2048),
            allowNull: false,
          },
          w: {
            type: DataTypes.STRING(2048),
            allowNull: false,
          },
          isSolved: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
          },
          completedAt: {
            type: DataTypes.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('PowChallenges', {
        fields: ['createdAt'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('PowChallenges');
  },
};
