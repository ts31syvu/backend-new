module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'PowConfigs',
        {
          type: {
            type: DataTypes.STRING(255),
            allowNull: false,
            primaryKey: true,
          },
          a: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
          b: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
          c: {
            type: DataTypes.DOUBLE,
            allowNull: false,
          },
        },
        { transaction }
      );
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('PowConfigs');
  },
};
