module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn(
      'HealthDepartments',
      'connectEnabled',

      {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      }
    );
  },
  down: async queryInterface => {
    await queryInterface.removeColumn('HealthDepartments', 'connectEnabled');
  },
};
