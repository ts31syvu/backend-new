module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'IPAddressBlockList',
        'source',
        {
          allowNull: true,
          type: DataTypes.STRING(250),
          defaultValue: null,
        },
        {
          transaction,
        }
      );
    });
  },

  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('IPAddressBlockList', 'source', {
        transaction,
      });
    });
  },
};
