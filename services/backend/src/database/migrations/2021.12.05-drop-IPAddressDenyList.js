module.exports = {
  up: async queryInterface => {
    await queryInterface.dropTable('IPAddressDenyList');
  },

  down: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('IPAddressDenyList', {
      ipStart: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      ipEnd: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
    });
  },
};
