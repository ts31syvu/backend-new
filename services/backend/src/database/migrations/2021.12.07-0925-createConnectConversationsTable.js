module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ConnectConversations',
        {
          uuid: {
            type: DataTypes.UUID,
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          departmentId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'HealthDepartments',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          data: {
            type: DataTypes.STRING(4096),
            allowNull: false,
          },
          publicKey: {
            type: DataTypes.STRING(88),
            allowNull: false,
          },
          iv: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          mac: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
          },
          lastContactedAt: {
            type: DataTypes.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ConnectConversations', {
        fields: ['departmentId'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('ConnectConversations');
  },
};
