module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.createTable(
        'ConnectMessages',
        {
          messageId: {
            type: DataTypes.STRING(24),
            allowNull: false,
            primaryKey: true,
            defaultValue: DataTypes.UUIDV4,
          },
          conversationId: {
            type: DataTypes.UUID,
            allowNull: false,
            references: {
              model: 'ConnectConversations',
              key: 'uuid',
            },
            onUpdate: 'CASCADE',
            onDelete: 'CASCADE',
          },
          data: {
            type: DataTypes.STRING(4096),
            allowNull: false,
          },
          iv: {
            type: DataTypes.STRING(24),
            allowNull: false,
          },
          mac: {
            type: DataTypes.STRING(44),
            allowNull: false,
          },
          readSignature: {
            type: DataTypes.STRING(120),
          },
          createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
          },
          receivedAt: {
            type: DataTypes.DATE,
          },
          readAt: {
            type: DataTypes.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex('ConnectMessages', {
        fields: ['conversationId'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.dropTable('ConnectMessages');
  },
};
