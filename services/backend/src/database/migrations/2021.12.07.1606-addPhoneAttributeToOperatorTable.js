module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('Operators', 'phone', {
      type: DataTypes.STRING,
      defaultValue: null,
      allowNull: true,
    });
  },
  down: async queryInterface => {
    return queryInterface.removeColumn('Operators', 'phone');
  },
};
