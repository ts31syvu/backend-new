module.exports = {
  up: async queryInterface => {
    await queryInterface.removeColumn('Locations', 'shouldProvideGeoLocation');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('Locations', 'shouldProvideGeoLocation', {
      type: Sequelize.BOOLEAN,
    });
  },
};
