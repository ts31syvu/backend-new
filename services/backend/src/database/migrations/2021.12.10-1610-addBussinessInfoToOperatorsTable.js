module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'Operators',
        'businessEntityName',
        {
          type: Sequelize.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityStreetName',
        {
          type: Sequelize.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityStreetNumber',
        {
          type: Sequelize.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityZipCode',
        {
          type: Sequelize.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'Operators',
        'businessEntityCity',
        {
          type: Sequelize.STRING(255),
          defaultValue: null,
          allowNull: true,
        },
        { transaction }
      );
    });
  },
  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('Operators', 'businessEntityName', {
        transaction,
      });
      await queryInterface.removeColumn(
        'Operators',
        'businessEntityStreetName',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn(
        'Operators',
        'businessEntityStreetNumber',
        {
          transaction,
        }
      );
      await queryInterface.removeColumn('Operators', 'businessEntityZipCode', {
        transaction,
      });
      await queryInterface.removeColumn('Operators', 'businessEntityCity', {
        transaction,
      });
    });
  },
};
