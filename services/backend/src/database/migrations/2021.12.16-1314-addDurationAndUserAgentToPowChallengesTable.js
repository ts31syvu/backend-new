module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.addColumn(
        'PowChallenges',
        'duration',
        {
          type: DataTypes.INTEGER,
          defaultValue: null,
        },
        { transaction }
      );
      await queryInterface.addColumn(
        'PowChallenges',
        'userAgent',
        {
          type: DataTypes.STRING(255),
          defaultValue: null,
        },
        { transaction }
      );
      await queryInterface.addIndex('PowChallenges', {
        fields: ['userAgent'],
        transaction,
      });
    });
  },
  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      await queryInterface.removeColumn('PowChallenges', 'duration', {
        transaction,
      });
      await queryInterface.removeColumn('PowChallenges', 'userAgent', {
        transaction,
      });
    });
  },
};
