module.exports = {
  up: async queryInterface =>
    queryInterface.removeColumn('Operators', 'allowOperatorDevices'),

  down: (queryInterface, DataTypes) => {
    return queryInterface.addColumn('Operators', 'allowOperatorDevices', {
      allowNull: false,
      defaultValue: false,
      type: DataTypes.BOOLEAN,
    });
  },
};
