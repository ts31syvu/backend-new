module.exports = {
  up: queryInterface =>
    queryInterface.removeColumn('LocationTransferTraces', 'isHDEncrypted'),
  down: (queryInterface, Sequelize) =>
    queryInterface.addColumn('LocationTransferTraces', 'isHDEncrypted', {
      type: Sequelize.BOOLEAN,
      defaultValue: true,
    }),
};
