module.exports = {
  up: async queryInterface => {
    await queryInterface.addIndex('Users', ['updatedAt'], {
      name: 'Users_updatedAt_index',
    });
  },
  down: async queryInterface => {
    await queryInterface.removeIndex('Users', 'Users_updatedAt_index');
  },
};
