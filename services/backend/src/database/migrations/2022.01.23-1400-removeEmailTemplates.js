module.exports = {
  up: async queryInterface => {
    await queryInterface.dropTable('EmailTemplates');
  },
  down: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('EmailTemplates', {
      key: {
        type: DataTypes.CITEXT,
        primaryKey: true,
        allowNull: false,
      },
      templateId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      lang: {
        type: DataTypes.STRING(2),
        allowNull: false,
        primaryKey: true,
      },
    });
  },
};
