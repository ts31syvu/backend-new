module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.addColumn('Operators', 'languageOverwrite', {
      allowNull: true,
      defaultValue: null,
      type: DataTypes.STRING(5),
    });
  },

  down: async queryInterface => {
    await queryInterface.removeColumn('Operators', 'languageOverwrite');
  },
};
