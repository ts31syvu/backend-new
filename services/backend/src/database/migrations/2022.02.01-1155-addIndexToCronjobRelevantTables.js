const migrations = [
  {
    table: 'TracingProcesses',
    fields: ['createdAt'],
    fieldName: 'tracing_processes_created_at',
  },
  {
    table: 'Operators',
    fields: ['createdAt'],
    fieldName: 'operators_created_at',
  },
  {
    table: 'Operators',
    fields: ['deletedAt'],
    fieldName: 'operators_deleted_at',
  },
  {
    table: 'UserTransfers',
    fields: ['createdAt'],
    fieldName: 'user_transfers_created_at',
  },
  {
    table: 'LocationGroups',
    fields: ['deletedAt'],
    fieldName: 'location_groups_deleted_at',
  },
  {
    table: 'Locations',
    fields: ['deletedAt'],
    fieldName: 'locations_deleted_at',
  },
  {
    table: 'SMSChallenges',
    fields: ['createdAt'],
    fieldName: 'sms_challenges_created_at',
  },
  {
    table: 'Users',
    fields: ['deletedAt'],
    fieldName: 'users_deleted_at',
  },
  {
    table: 'HealthDepartments',
    fields: ['publicHDSKP'],
    fieldName: 'health_departments_public_hds_skp',
  },
  {
    table: 'TestRedeems',
    fields: ['createdAt'],
    fieldName: 'test_redeems_created_at',
  },
  {
    table: 'NotificationChunks',
    fields: ['createdAt'],
    fieldName: 'notification_chunks_created_at',
  },
  {
    table: 'Challenges',
    fields: ['createdAt'],
    fieldName: 'challenges_created_at',
  },
  {
    table: 'OperatorDevices',
    fields: ['createdAt'],
    fieldName: 'operator_devices_created_at',
  },
  {
    table: 'HealthDepartmentAuditLogs',
    fields: ['createdAt'],
    fieldName: 'health_department_audit_logs_created_at',
  },
  {
    table: 'ConnectConversations',
    fields: ['createdAt'],
    fieldName: 'connect_conversations_created_at',
  },
  {
    table: 'ConnectSearchProcesses',
    fields: ['createdAt'],
    fieldName: 'connect_search_processes_created_at',
  },
];

module.exports = {
  up: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      for (const migration of migrations) {
        await queryInterface.addIndex(migration.table, {
          fields: migration.fields,
          transaction,
        });
      }
    });
  },

  down: async queryInterface => {
    await queryInterface.sequelize.transaction(async transaction => {
      for (const migration of migrations) {
        await queryInterface.removeIndex(migration.table, migration.fieldName, {
          transaction,
        });
      }
    });
  },
};
