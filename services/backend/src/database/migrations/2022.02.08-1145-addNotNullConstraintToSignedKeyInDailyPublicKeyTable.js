module.exports = {
  up: (queryInterface, DataTypes) =>
    queryInterface.changeColumn('DailyPublicKeys', 'signedPublicDailyKey', {
      type: DataTypes.STRING(1024),
      allowNull: false,
    }),

  down: (queryInterface, DataTypes) =>
    queryInterface.changeColumn('DailyPublicKeys', 'signedPublicDailyKey', {
      type: DataTypes.STRING(1024),
      allowNull: true,
    }),
};
