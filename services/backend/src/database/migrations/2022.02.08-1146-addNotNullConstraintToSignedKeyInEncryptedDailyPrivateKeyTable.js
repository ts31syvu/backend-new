module.exports = {
  up: (queryInterface, DataTypes) =>
    queryInterface.changeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
        allowNull: false,
      }
    ),

  down: (queryInterface, DataTypes) =>
    queryInterface.changeColumn(
      'EncryptedDailyPrivateKeys',
      'signedEncryptedPrivateDailyKey',
      {
        type: DataTypes.STRING(1024),
        allowNull: true,
      }
    ),
};
