module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.changeColumn('CacheFallbacks', 'key', {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
    });
  },

  down: (queryInterface, DataTypes) => {
    queryInterface.changeColumn('CacheFallbacks', 'traceId', {
      type: DataTypes.STRING(50),
      allowNull: false,
      primaryKey: true,
    });
  },
};
