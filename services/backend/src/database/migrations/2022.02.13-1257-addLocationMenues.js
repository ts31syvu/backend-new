module.exports = {
  up: async (queryInterface, DataTypes) => {
    await queryInterface.createTable('LocationMenus', {
      locationId: {
        allowNull: false,
        type: DataTypes.UUID,
        primaryKey: true,
        references: {
          model: 'Locations',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      menu: {
        allowNull: true,
        type: DataTypes.JSON,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
      updatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.literal('CURRENT_TIMESTAMP'),
      },
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('LocationMenus');
  },
};
