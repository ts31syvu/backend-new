import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  key: string;
  content: string;
  etag: string;
}

interface CreationAttributes {
  key: string;
  content: string;
  etag: string;
}

export interface CacheFallbackInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initCachFallbacks = (
  sequelize: Sequelize
): ModelCtor<CacheFallbackInstance> => {
  return sequelize.define<CacheFallbackInstance>('CacheFallbacks', {
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    etag: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
  });
};
