import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';
import type { ConnectConversationInstance } from './connectConversation';

interface Attributes {
  messageId: string;
  conversationId: string;
  data: string;
  iv: string;
  mac: string;
  readSignature?: string;
  createdAt: Date;
  receivedAt?: Date;
  readAt?: Date;
}

type CreationAttributes = {
  messageId: string;
  conversationId: string;
  data: string;
  iv: string;
  mac: string;
};

export interface ConnectMessageInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  ConnectConversation?: ConnectConversationInstance;
}

export const initConnectMessage = (
  sequelize: Sequelize
): ModelCtor<ConnectMessageInstance> => {
  return sequelize.define<ConnectMessageInstance>(
    'ConnectMessage',
    {
      messageId: {
        type: DataTypes.STRING(24),
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      conversationId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'ConnectConversations',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      data: {
        type: DataTypes.STRING(4096),
        allowNull: false,
      },
      iv: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      mac: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      readSignature: {
        type: DataTypes.STRING(120),
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      receivedAt: {
        type: DataTypes.DATE,
      },
      readAt: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateConnectMessage = (models: Models): void => {
  models.ConnectMessage.belongsTo(models.ConnectConversation, {
    foreignKey: 'conversationId',
  });
};
