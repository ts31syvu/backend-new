import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import { PrefixType } from 'constants/prefixType';
import type { Models } from '..';

interface Attributes {
  uuid: string;
  processId: string;
  prefix: string;
  type: PrefixType;
  data: string;
  publicKey: string;
  mac: string;
  iv: string;
  completedAt?: Date;
}

type CreationAttributes = {
  processId: string;
};

export interface ConnectSearchInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initConnectSearch = (
  sequelize: Sequelize
): ModelCtor<ConnectSearchInstance> => {
  return sequelize.define<ConnectSearchInstance>(
    'ConnectSearch',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      processId: {
        type: DataTypes.UUID,
        allowNull: false,
        references: {
          model: 'ConnectSearchProcess',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      prefix: {
        type: DataTypes.STRING(12),
        allowNull: false,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      data: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
        allowNull: false,
      },
      iv: {
        type: DataTypes.STRING(24),
        allowNull: false,
      },
      mac: {
        type: DataTypes.STRING(44),
        allowNull: false,
      },
      completedAt: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateConnectSearch = (models: Models): void => {
  models.ConnectSearch.belongsTo(models.ConnectSearchProcess, {
    foreignKey: 'processId',
  });
};
