import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import { Moment } from 'moment';

interface Attributes {
  keyId: number;
  publicKey: string;
  issuerId: string;
  signature: string;
  signedPublicDailyKey: string;
  createdAt: Date | Moment;
  updatedAt: Date | Moment;
}

type CreationAttributes = Attributes;

export interface DailyPublicKeyInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initDailyPublicKeys = (
  sequelize: Sequelize
): ModelCtor<DailyPublicKeyInstance> => {
  // @ts-ignore, automatic timestamps
  return sequelize.define<DailyPublicKeyInstance>('DailyPublicKey', {
    keyId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      defaultValue: 0,
    },
    publicKey: {
      type: DataTypes.STRING(88),
      allowNull: false,
    },
    issuerId: {
      type: DataTypes.UUID,
      allowNull: false,
    },
    signature: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    signedPublicDailyKey: {
      type: DataTypes.STRING(1024),
      allowNull: false,
    },
  });
};
