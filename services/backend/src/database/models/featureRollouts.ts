import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';

interface Attributes {
  name: string;
  percentage: number;
  active: boolean;
}

type CreationAttributes = Attributes;

export interface FeatureRolloutInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initFeatureRollouts = (
  sequelize: Sequelize
): ModelStatic<FeatureRolloutInstance> => {
  return sequelize.define<FeatureRolloutInstance>('FeatureRollout', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true,
    },
    percentage: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: true,
    },
  });
};
