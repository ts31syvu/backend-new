import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import { hashPassword } from 'utils/hash';

interface Attributes {
  name: string;
  password: string;
  salt: string;
}

type CreationAttributes = Attributes;

export interface internalAccessUserInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
  checkPassword: (testPassword: string) => Promise<boolean>;
}

export const initInternalAccessUser = (
  sequelize: Sequelize
): ModelCtor<internalAccessUserInstance> => {
  const model = sequelize.define<internalAccessUserInstance>(
    'InternalAccessUser',
    {
      name: {
        type: DataTypes.STRING,
        primaryKey: true,
      },
      password: {
        type: DataTypes.STRING(255),
      },
      salt: {
        type: DataTypes.STRING(255),
      },
    }
  );

  model.prototype.checkPassword = async function checkPassword(
    testPassword: string
  ) {
    const hash = await hashPassword(testPassword, this.get('salt'));
    return hash.toString('base64') === this.get('password');
  };

  return model;
};
