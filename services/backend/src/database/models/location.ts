/* eslint-disable max-lines */
import {
  col,
  DataTypes,
  fn,
  Model,
  ModelCtor,
  Op,
  Sequelize,
  Transaction,
} from 'sequelize';
import {
  LocationEntryPolicyInfoTypes,
  LocationEntryPolicyTypes,
  LocationMaskTypes,
  LocationVentilationTypes,
} from 'constants/location';

import type { Models } from '..';
import type { TraceInstance } from './trace';
import type { OperatorInstance } from './operator';
import type { LocationGroupInstance } from './locationGroup';
import type { LocationTransferInstance } from './locationTransfer';
import type { AdditionalDataInstance } from './additionalDataSchema';

interface Attributes {
  uuid: string;
  radius: number;
  scannerId: string;
  isPrivate: boolean;
  publicKey: string;
  accessId: string;
  scannerAccessId: string;
  formId: string;
  isIndoor: boolean;
  groupId?: string;
  name?: string;
  firstName?: string;
  lastName?: string;
  phone?: string;
  streetName?: string;
  streetNr?: string;
  zipCode?: string;
  city?: string;
  state?: string;
  lat?: number;
  lng?: number;
  operator?: string;
  endsAt?: Date;
  tableCount?: number;
  type?: string;
  averageCheckinTime?: number;
  entryPolicy?: LocationEntryPolicyTypes;
  isContactDataMandatory: boolean;
  deletedAt?: Date | null;
  entryPolicyInfo?: LocationEntryPolicyInfoTypes;
  masks?: LocationMaskTypes;
  ventilation?: LocationVentilationTypes;
  roomHeight?: number;
  roomWidth?: number;
  roomDepth?: number;
}

type CreationAttributes = Attributes;

export interface LocationInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
  checkoutAllTraces: (payload: {
    location: LocationInstance;
    transaction: Transaction;
  }) => Promise<void>;
  removeAllTraces: (payload: {
    location: LocationInstance;
    transaction: Transaction;
  }) => Promise<void>;

  AdditionalDataSchemas?: Array<AdditionalDataInstance>;
  Operator?: OperatorInstance;
  LocationGroup?: LocationGroupInstance;
  Traces?: Array<TraceInstance>;
  LocationTransfers?: Array<LocationTransferInstance>;
}

export const initLocations = (
  sequelize: Sequelize
): ModelCtor<LocationInstance> => {
  const model = sequelize.define<LocationInstance>(
    'Location',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      scannerId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      accessId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      formId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      scannerAccessId: {
        type: DataTypes.UUID,
        allowNull: false,
        unique: true,
        defaultValue: DataTypes.UUIDV4,
      },
      name: {
        type: DataTypes.STRING,
      },
      firstName: {
        type: DataTypes.STRING,
      },
      lastName: {
        type: DataTypes.STRING,
      },
      phone: {
        type: DataTypes.STRING,
      },
      streetName: {
        type: DataTypes.STRING,
      },
      streetNr: {
        type: DataTypes.STRING,
      },
      zipCode: {
        type: DataTypes.STRING,
      },
      city: {
        type: DataTypes.STRING,
      },
      state: {
        type: DataTypes.STRING,
        defaultValue: null,
      },
      lat: {
        type: DataTypes.DOUBLE,
        defaultValue: null,
      },
      lng: {
        type: DataTypes.DOUBLE,
        defaultValue: null,
      },
      entryPolicy: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.ENUM({
          values: Object.values(LocationEntryPolicyTypes),
        }),
      },
      isContactDataMandatory: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      radius: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0,
      },
      endsAt: {
        type: DataTypes.DATE,
      },
      tableCount: {
        type: DataTypes.INTEGER,
      },
      isPrivate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      publicKey: {
        type: DataTypes.STRING(88),
      },
      isIndoor: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: true,
      },
      type: {
        type: DataTypes.STRING,
        allowNull: true,
        defaultValue: 'other',
      },
      averageCheckinTime: {
        type: DataTypes.INTEGER,
        allowNull: true,
        defaultValue: null,
      },
      entryPolicyInfo: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      ventilation: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      masks: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.STRING,
      },
      roomHeight: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
      roomWidth: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
      roomDepth: {
        allowNull: true,
        defaultValue: null,
        type: DataTypes.FLOAT,
      },
    },
    {
      paranoid: true,
    }
  );

  model.prototype.checkoutAllTraces = async function checkoutAllTraces(
    transaction?: Transaction
  ) {
    return sequelize.models.Trace.update(
      { time: fn('tstzrange', fn('lower', col('time')), fn('now')) },
      {
        where: {
          locationId: this.get('uuid'),
          // @ts-ignore typing on a range with Op.contains
          time: {
            [Op.contains]: fn('now'),
          },
        },
        transaction,
      }
    );
  };

  model.prototype.removeAllTraces = async function removeAllTraces(
    transaction?: Transaction
  ) {
    return sequelize.models.Trace.destroy({
      force: true,
      where: {
        locationId: this.get('uuid'),
      },
      transaction,
    });
  };

  return model;
};

export const associateLocation = (models: Models): void => {
  models.Location.belongsTo(models.Operator, {
    foreignKey: 'operator',
    onDelete: 'CASCADE',
  });

  models.Location.belongsTo(models.LocationGroup, {
    foreignKey: 'groupId',
    onDelete: 'CASCADE',
  });

  models.Location.hasMany(models.AdditionalDataSchema, {
    foreignKey: 'locationId',
  });

  models.Location.hasMany(models.Trace, {
    foreignKey: 'locationId',
  });

  models.Location.hasMany(models.LocationTransfer, {
    foreignKey: 'locationId',
  });
};
