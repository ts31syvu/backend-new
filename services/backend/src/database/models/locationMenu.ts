import { Sequelize, Model, ModelStatic, DataTypes } from 'sequelize';
import type { Models } from '..';

export type LocationMenuTranslatableInfo = {
  DE: string;
  EN: string;
};

export interface LocationMenu {
  categories: [
    {
      name: LocationMenuTranslatableInfo;
      description: LocationMenuTranslatableInfo;
      items: [
        {
          name: LocationMenuTranslatableInfo;
          description: LocationMenuTranslatableInfo;
          additionalInfo: LocationMenuTranslatableInfo;
          price: number;
          allergensAndAdditives: string[];
        }
      ];
    }
  ];
  allergenesAndAdditives: {
    [key: string]: LocationMenuTranslatableInfo;
  };
}
interface Attributes {
  locationId: string;
  menu: LocationMenu;
}

type CreationAttributes = Attributes;
export interface LocationMenuInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initLocationMenus = (
  sequelize: Sequelize
): ModelStatic<LocationMenuInstance> => {
  return sequelize.define<LocationMenuInstance>('LocationMenu', {
    locationId: {
      allowNull: false,
      type: DataTypes.UUID,
      primaryKey: true,
    },
    menu: {
      allowNull: true,
      type: DataTypes.JSON,
    },
  });
};

export const associateLocationMenu = (models: Models): void => {
  models.LocationMenu.belongsTo(models.Location, {
    foreignKey: 'locationId',
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  });
};
