import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';
import type { Models } from '..';

export enum UrlType {
  MAP = 'map',
  MENU = 'menu',
  SCHEDULE = 'schedule',
  GENERAL = 'general',
  WEBSITE = 'website',
}

interface Attributes {
  locationId: string;
  type: UrlType;
  url: string | null;
}

type CreationAttributes = Attributes;

export interface LocationURLInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initLocationURLs = (
  sequelize: Sequelize
): ModelCtor<LocationURLInstance> => {
  return sequelize.define<LocationURLInstance>(
    'LocationURLs',
    {
      locationId: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        references: {
          model: 'Locations',
          key: 'uuid',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      type: {
        type: DataTypes.ENUM({
          values: Object.values(UrlType),
        }),
        primaryKey: true,
        allowNull: false,
      },
      url: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {
      tableName: 'LocationURLs',
      timestamps: false,
    }
  );
};

export const associateLocationURLs = (models: Models): void => {
  models.LocationURL.belongsTo(models.Location, {
    foreignKey: 'locationId',
  });
};
