import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  operatorId: string;
  publicKey: boolean;
  privateKeySecret: boolean;
}

type CreationAttributes = Attributes;

export interface OperatorKeyHistoryInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initOperatorKeyHistories = (
  sequelize: Sequelize
): ModelCtor<OperatorKeyHistoryInstance> => {
  return sequelize.define<OperatorKeyHistoryInstance>('OperatorKeyHistory', {
    operatorId: {
      type: DataTypes.UUID,
      allowNull: false,
      primaryKey: true,
    },
    publicKey: {
      type: DataTypes.STRING(88),
      allowNull: false,
      primaryKey: true,
    },
    privateKeySecret: {
      type: DataTypes.STRING(44),
      allowNull: false,
    },
  });
};
