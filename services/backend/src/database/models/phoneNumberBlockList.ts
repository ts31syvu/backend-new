import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  phoneNumber: number;
}

type CreationAttributes = {
  phoneNumber: number;
};

export interface PhoneNumberBlockListInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initPhoneNumberBlockList = (
  sequelize: Sequelize
): ModelCtor<PhoneNumberBlockListInstance> => {
  return sequelize.define<PhoneNumberBlockListInstance>(
    'PhoneNumberBlockList',
    {
      phoneNumber: {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true,
        allowNull: false,
      },
    },
    {
      tableName: 'PhoneNumberBlockList',
    }
  );
};
