import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  uuid: string;
  type: string;
  difficulty: number;
  n: string;
  t: string;
  w: string;
  isSolved: boolean;
  duration?: number;
  userAgent?: string;
  createdAt: Date;
  completedAt?: Date;
}

type CreationAttributes = {
  type: string;
  difficulty: number;
  n: string;
  t: string;
  w: string;
  userAgent?: string;
};

export interface PowChallengeInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initPowChallenge = (
  sequelize: Sequelize
): ModelCtor<PowChallengeInstance> => {
  return sequelize.define<PowChallengeInstance>(
    'PowChallenge',
    {
      uuid: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
      },
      type: {
        type: DataTypes.STRING(255),
        allowNull: false,
      },
      difficulty: {
        type: DataTypes.DOUBLE,
        allowNull: false,
      },
      n: {
        type: DataTypes.STRING(2048),
        allowNull: false,
      },
      t: {
        type: DataTypes.STRING(2048),
        allowNull: false,
      },
      w: {
        type: DataTypes.STRING(2048),
        allowNull: false,
      },
      isSolved: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      duration: {
        type: DataTypes.INTEGER,
        defaultValue: null,
      },
      userAgent: {
        type: DataTypes.STRING(255),
        defaultValue: null,
      },
      createdAt: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: DataTypes.NOW,
      },
      completedAt: {
        type: DataTypes.DATE,
      },
    },
    {
      timestamps: false,
    }
  );
};
