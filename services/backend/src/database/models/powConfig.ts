import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';

interface Attributes {
  type: string;
  a: number;
  b: number;
  c: number;
}

type CreationAttributes = Attributes;

export interface PowConfigInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initPowConfig = (
  sequelize: Sequelize
): ModelCtor<PowConfigInstance> => {
  return sequelize.define<PowConfigInstance>(
    'PowConfig',
    {
      type: {
        type: DataTypes.STRING(255),
        allowNull: false,
        primaryKey: true,
      },
      a: {
        type: DataTypes.DOUBLE,
        allowNull: false,
      },
      b: {
        type: DataTypes.DOUBLE,
        allowNull: false,
      },
      c: {
        type: DataTypes.DOUBLE,
        allowNull: false,
      },
    },
    {
      timestamps: false,
    }
  );
};
