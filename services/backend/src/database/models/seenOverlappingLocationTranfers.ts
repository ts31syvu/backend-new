import { Sequelize, Model, DataTypes, ModelCtor } from 'sequelize';

interface Attributes {
  departmentId: string;
  locationTransferId: string;
  requestingDepartmentId: string;
}

type CreationAttributes = Attributes;

export interface SeenOverlappingLocationTranferInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {
  createdAt: Date;
  updatedAt: Date;
}

export const initSeenOverlappingLocationTranfers = (
  sequelize: Sequelize
): ModelCtor<SeenOverlappingLocationTranferInstance> => {
  return sequelize.define<SeenOverlappingLocationTranferInstance>(
    'SeenOverlappingLocationTranfers',
    {
      departmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      requestingDepartmentId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
      locationTransferId: {
        type: DataTypes.UUID,
        allowNull: false,
        primaryKey: true,
      },
    }
  );
};
