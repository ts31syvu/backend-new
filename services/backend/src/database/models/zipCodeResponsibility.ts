import { Sequelize, Model, ModelCtor, DataTypes } from 'sequelize';
import type { Models } from '..';

interface Attributes {
  zipCode: string;
  departmentId?: string;
  name: string;
}

type CreationAttributes = {
  zipCode: string;
  departmentId?: string;
  name: string;
};

export interface ZipCodeResponsibilityInstance
  extends Model<Attributes, CreationAttributes>,
    Attributes {}

export const initZipCodeResponsibility = (
  sequelize: Sequelize
): ModelCtor<ZipCodeResponsibilityInstance> => {
  return sequelize.define<ZipCodeResponsibilityInstance>(
    'ZipCodeResponsibility',
    {
      zipCode: {
        type: DataTypes.STRING(5),
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(255),
        allowNull: false,
        primaryKey: true,
      },
      departmentId: {
        type: DataTypes.UUID,
        allowNull: true,
        references: {
          model: 'HealthDepartments',
          key: 'uuid',
        },
      },
    },
    {
      timestamps: false,
    }
  );
};

export const associateZipCodeResponsibility = (models: Models): void => {
  models.ZipCodeResponsibility.belongsTo(models.HealthDepartment, {
    foreignKey: 'departmentId',
  });
};
