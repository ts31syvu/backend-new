const noCache = (request, response, next) => {
  if (request.method !== 'GET') return next();

  response.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
  response.setHeader('Pragma', ' no-cache');
  response.setHeader('Expires', '0');
  return next();
};

module.exports = {
  noCache,
};
