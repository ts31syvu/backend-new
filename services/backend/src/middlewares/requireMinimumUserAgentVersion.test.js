const chai = require('chai');

const { getApp } = require('../app');

const { DEFAULT_VALUES } = require('../utils/featureFlag');

const { expect } = chai;

const VALID_VERSION = JSON.parse(DEFAULT_VALUES.ios_minimum_semver);
const HEALTH_CHECK_URL = '/api/v3/health';
const UA_HEADER = 'User-Agent';
const VALID_UA = `luca/iOS ${VALID_VERSION}`;
const VALID_UA_BUSINESS = `luca/iOS/business ${VALID_VERSION}`;
const OUTDATED_UA = 'luca/iOS 1.0.0';
const OUTDATED_UA_BUSINESS = 'luca/Android/business v0.1.0';
const INVALID_VERSION = 'luca/iOS 100';
const INVALID_PLATFORM = `luca/operator-app ${VALID_VERSION}`;
const INVALID_BUSINESS = `luca/iOS/operator-app ${VALID_VERSION}`;

describe('Minimum User Agent Version Middleware', () => {
  it('Passes if User Agent is not given', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Passes if User Agent Version is high enough', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, VALID_UA)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Passes if User Agent Version is high enough for operator app', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, VALID_UA_BUSINESS)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Returns Upgrade Required if version is too low', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, OUTDATED_UA)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(426);
      });
  });

  it('Returns Upgrade Required if version is too low for operator app', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, OUTDATED_UA_BUSINESS)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(426);
      });
  });

  it('Ignore the User Agent if is not a qualified App UA', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, 'not-a-valid-ua')
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Ignore the User Agent if it does not contain a valid semver version', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, INVALID_VERSION)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Ignore the User Agent if it does not contain a valid platform', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, INVALID_PLATFORM)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });

  it('Ignore the User Agent if it does not contain a valid business', async () => {
    chai
      .request(getApp())
      .get(HEALTH_CHECK_URL)
      .set(UA_HEADER, INVALID_BUSINESS)
      .end((error, response) => {
        expect(error).to.be.null;
        expect(response).to.have.status(200);
      });
  });
});
