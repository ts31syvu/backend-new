import { RequestHandler } from 'express';
import { Op } from 'sequelize';
import config from 'config';
import moment from 'moment';
import status from 'http-status';

import { database, PowChallenge } from 'database';

const POW_MAX_AGE = config.get<number>('pow.validFor');

type powSolution = {
  pow: {
    id: string;
    w: string;
    duration?: number;
  };
};

export const requirePow = (
  powType: string
): RequestHandler<unknown, unknown, powSolution> => async (
  request,
  response,
  next
) => {
  let isSolved = false;
  let challenge;
  await database.transaction(async transaction => {
    challenge = await PowChallenge.findOne({
      where: {
        uuid: request.body.pow.id,
        type: powType,
        createdAt: {
          [Op.gt]: moment().subtract(POW_MAX_AGE, 'hours').toDate(),
        },
        completedAt: null,
      },
      transaction,
      lock: transaction.LOCK.UPDATE,
    });
    if (!challenge) {
      return;
    }
    isSolved = request.body.pow.w === challenge.w;
    await PowChallenge.update(
      {
        completedAt: moment().toDate(),
        isSolved,
        duration:
          request.body.pow.duration ||
          moment().diff(challenge.createdAt, 'seconds'),
      },
      {
        where: {
          uuid: request.body.pow.id,
        },
        transaction,
      }
    );
  });
  if (!challenge || !isSolved) {
    return response.sendStatus(status.FORBIDDEN);
  }

  return next();
};
