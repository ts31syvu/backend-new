/* eslint-disable promise/no-callback-in-promise */
const passportCustom = require('passport-custom');
const database = require('../database');
const { pseudoHashPassword } = require('../utils/hash');
const { z } = require('../utils/validation');

// eslint-disable-next-line consistent-return
const bearerStrategy = new passportCustom.Strategy(async (request, done) => {
  const token = request.headers['badge-generator-authorization'];

  const { success, data: parsedToken } = await z
    .bearerToken()
    .safeParseAsync(token);

  if (!success) {
    return done(null, null, new Error('Invalid Token'));
  }

  try {
    const decoded = Buffer.from(parsedToken, 'base64').toString('ascii');
    const [name, password] = decoded.split(':');
    const badgeGenerator = await database.BadgeGenerator.findByPk(name);

    if (!badgeGenerator) {
      // prevent timing issues
      await pseudoHashPassword();
      return done(null, false);
    }

    const isValidPassword = await badgeGenerator.checkPassword(password);

    if (!isValidPassword) {
      return done(null, false);
    }

    return done(null, badgeGenerator);
  } catch (error) {
    return done(error);
  }
});

module.exports = bearerStrategy;
