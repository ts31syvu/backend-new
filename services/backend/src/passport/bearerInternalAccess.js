/* eslint-disable promise/no-callback-in-promise */
const passportCustom = require('passport-custom');
const database = require('../database');
const { pseudoHashPassword } = require('../utils/hash');
const { z } = require('../utils/validation');

// eslint-disable-next-line consistent-return
const bearerStrategy = new passportCustom.Strategy(async (request, done) => {
  const token = request.headers['internal-access-authorization'];

  const { success, data: parsedToken } = await z
    .bearerToken()
    .safeParseAsync(token);

  if (!success) {
    return done(null, null, new Error('Invalid Token'));
  }

  try {
    const decoded = Buffer.from(parsedToken, 'base64').toString('ascii');
    const [name, password] = decoded.split(':');
    const internalAccessUser = await database.internalAccessUser.findByPk(name);

    if (!internalAccessUser) {
      // prevent timing issues
      await pseudoHashPassword();
      return done(null, false);
    }

    const isValidPassword = await internalAccessUser.checkPassword(password);

    if (!isValidPassword) {
      return done(null, false);
    }

    return done(null, internalAccessUser);
  } catch (error) {
    return done(error);
  }
});

module.exports = bearerStrategy;
