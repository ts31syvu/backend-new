/* eslint-disable promise/no-callback-in-promise */
/* eslint-disable no-param-reassign */

const LocalStrategy = require('passport-local').Strategy;
const { UserType } = require('../constants/user');
const { pseudoHashPassword } = require('../utils/hash');
const database = require('../database');
const { logEvent } = require('../utils/hdAuditLog');
const { AuditLogEvents, AuditStatusType } = require('../constants/auditLog');

const localStrategy = new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password',
  },
  async (username, password, done) => {
    const user = await database.HealthDepartmentEmployee.findOne({
      where: { email: username },
    });

    if (!user) {
      // prevent timing issues
      await pseudoHashPassword();
      return done(null, false);
    }

    const isValidPassword = await user.checkPassword(password);

    if (!isValidPassword) {
      logEvent(user, {
        type: AuditLogEvents.LOGIN,
        status: AuditStatusType.ERROR_INVALID_PASSWORD,
      });

      return done(null, false);
    }

    user.type = UserType.HEALTH_DEPARTMENT_EMPLOYEE;

    logEvent(user, {
      type: AuditLogEvents.LOGIN,
      status: AuditStatusType.SUCCESS,
    });

    await database.Session.destroy({
      where: { userId: user.uuid, type: UserType.HEALTH_DEPARTMENT_EMPLOYEE },
    });

    return done(null, user);
  }
);

module.exports = localStrategy;
