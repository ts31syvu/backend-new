/* eslint-disable max-lines */
const { performance } = require('perf_hooks');
const config = require('config');
const router = require('express').Router();
const moment = require('moment');
const status = require('http-status');
const crypto = require('crypto');
const { Op, fn, col } = require('sequelize');

const {
  GET_RANDOM_BYTES,
  hexToBase64,
  encodeUtf8,
  base64ToHex,
  SHA256,
  bytesToHex,
  VERIFY_EC_SHA256_IEEE_SIGNATURE,
  VERIFY_EC_SHA256_DER_SIGNATURE,
} = require('@lucaapp/crypto');

const database = require('../../database');

const { updateBlockList } = require('../../utils/ipBlockList');

const { logJobCompletion } = require('../../utils/logJobCompletion');
const featureFlag = require('../../utils/featureFlag').default;
const {
  generateActiveChunk,
  generateArchiveChunk,
} = require('../../utils/notifications/notificationsV4');
const { z } = require('../../utils/validation');
const { updateBloomFilter } = require('../../utils/bloomFilter');
const { cacheUrl } = require('../../utils/urlCache');

router.post('/updateBlockList', async (_, response) => {
  const t0 = performance.now();
  await updateBlockList();
  const responseData = { time: performance.now() - t0 };
  await logJobCompletion('updateBlockList', responseData);
  response.send(responseData);
});

router.post('/deleteOldTraces', async (request, response) => {
  const t0 = performance.now();
  let affectedRows = await database.Trace.destroy({
    where: {
      time: {
        [Op.strictLeft]: [
          moment().subtract(config.get('luca.traces.maxAge'), 'days'),
          null,
        ],
      },
    },
  });
  affectedRows += await database.Trace.destroy({
    where: {
      expiresAt: {
        [Op.lte]: moment(),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldTraces', responseData);
  response.send(responseData);
});

router.post('/deleteOldTransferTraces', async (request, response) => {
  const t0 = performance.now();
  const affectedRows = await database.LocationTransferTrace.destroy({
    where: {
      time: {
        [Op.strictLeft]: [
          moment().subtract(config.get('luca.traces.maxAge'), 'days'),
          null,
        ],
      },
    },
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldTransferTraces', responseData);
  response.send(responseData);
});

router.post('/deleteOldTracingProcesses', async (_, response) => {
  const t0 = performance.now();

  const affectedRows = await database.TracingProcess.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(
          config.get('luca.tracingProcess.maxAge'),
          'hours'
        ),
      },
    },
    paranoid: false,
    force: true,
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldTracingProcesses', responseData);
  response.send(responseData);
});

router.post('/checkoutOldTraces', async (request, response) => {
  const t0 = performance.now();
  const [affectedRows] = await database.Trace.update(
    {
      time: fn('tstzrange', fn('lower', col('time')), moment().toISOString()),
    },
    {
      where: {
        time: {
          [Op.contains]: [
            moment().subtract(config.get('luca.traces.maxDuration'), 'hours'),
            moment(),
          ],
        },
      },
    }
  );
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('checkoutOldTraces', responseData);
  response.send(responseData);
});

router.post('/deleteOldInactiveOperators', async (request, response) => {
  const t0 = performance.now();
  const affectedRows = await database.Operator.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(config.get('emails.expiry'), 'hours'),
      },
      activated: false,
    },
    force: true,
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldInactiveOperators', responseData);
  response.send(responseData);
});

router.post('/removeDeletedOperators', async (request, response) => {
  const t0 = performance.now();
  const earliestTimeToKeep = moment().subtract(
    config.get('luca.operators.deleted.maxAgeHours'),
    'hours'
  );
  const affectedRows = await database.Operator.destroy({
    where: {
      deletedAt: {
        [Op.lt]: earliestTimeToKeep,
      },
    },
    paranoid: false,
    force: true,
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('removeDeletedOperators', responseData);
  response.send(responseData);
});

router.post('/deleteUnusedUserTransfers', async (request, response) => {
  const t0 = performance.now();
  const affectedRows = await database.UserTransfer.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(
          config.get('luca.userTransfers.maxAgeUnused'),
          'hours'
        ),
      },
      departmentId: null,
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteUnusedUserTransfers', responseData);
  response.send(responseData);
});

router.post('/deleteOldUserTransfers', async (request, response) => {
  const t0 = performance.now();
  const affectedRows = await database.UserTransfer.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(
          config.get('luca.userTransfers.maxAge'),
          'hours'
        ),
      },
    },
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldUserTransfers', responseData);
  response.send(responseData);
});

router.post('/cleanUpLocations', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.locations.maxAge');
  const affectedGroupRows = await database.LocationGroup.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
    force: true,
  });
  const affectedLocationRows = await database.Location.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
    force: true,
  });

  const responseData = {
    time: performance.now() - t0,
    affectedRows: affectedGroupRows + affectedLocationRows,
  };
  await logJobCompletion('cleanUpLocations', responseData);
  response.send(responseData);
});

router.post('/cleanUpChallenges', async (request, response) => {
  const t0 = performance.now();
  const affectedRows = await database.SMSChallenge.update(
    { messageId: '' },
    {
      where: {
        createdAt: {
          [Op.lt]: moment().subtract(
            config.get('luca.smsChallenges.maxAge'),
            'hours'
          ),
        },
      },
    }
  );

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('cleanUpChallenges', responseData);
  response.send(responseData);
});

router.post('/cleanUpUsers', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.users.deleted.maxAge');
  const affectedRows = await database.User.destroy({
    where: {
      deletedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
    force: true,
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('cleanUpUsers', responseData);
  response.send(responseData);
});

router.post('/addDummyTraces', async (request, response) => {
  const t0 = performance.now();
  const healthDepartments = await database.HealthDepartment.findAll({
    where: {
      publicHDSKP: {
        [Op.not]: null,
      },
    },
  });
  const healthDepartment =
    healthDepartments[crypto.randomInt(0, healthDepartments.length)];

  for (
    let tracingIndex = 0;
    tracingIndex <
    crypto.randomInt(0, await featureFlag.get('dummy_max_tracings'));
    tracingIndex += 1
  ) {
    const traces = [];
    const baseTime = moment().subtract(
      crypto.randomInt(0, moment.duration(10, 'days').asSeconds()),
      's'
    );

    for (
      let traceIndex = 0;
      traceIndex <
      crypto.randomInt(0, await featureFlag.get('dummy_max_traces'));
      traceIndex += 1
    ) {
      traces.push({
        healthDepartmentId: healthDepartment.uuid,
        traceId: hexToBase64(GET_RANDOM_BYTES(16)),
        createdAt: moment(baseTime).subtract(
          crypto.randomInt(0, 720) - 360,
          'minutes'
        ),
      });
    }
    await database.DummyTrace.bulkCreate(traces);
  }
  const responseData = {
    time: performance.now() - t0,
  };
  await logJobCompletion('addDummyTraces', responseData);
  response.send(responseData);
});

router.post('/deleteOldTestRedeems', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.testRedeems.maxAge');
  const affectedRows = await database.TestRedeem.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldTestRedeems', responseData);
  response.send(responseData);
});

router.post(
  '/regenerateV4NotificationsActiveChunk',
  async (request, response) => {
    const t0 = performance.now();
    await generateActiveChunk();

    const responseData = {
      time: performance.now() - t0,
    };
    await logJobCompletion(
      'regenerateV4NotificationsActiveChunk',
      responseData
    );
    response.send(responseData);
  }
);

router.post(
  '/generateV4NotificationsArchiveChunk',
  async (request, response) => {
    const t0 = performance.now();
    await generateArchiveChunk();

    const responseData = {
      time: performance.now() - t0,
    };
    await logJobCompletion('generateV4NotificationsArchiveChunk', responseData);
    response.send(responseData);
  }
);

router.post('/deleteOldV4NotificationChunks', async (request, response) => {
  const t0 = performance.now();
  const earliestTimeToKeep = moment().subtract(
    config.get('luca.notificationChunks.maxAge'),
    'hours'
  );
  const affectedRows = await database.NotificationChunk.destroy({
    where: {
      createdAt: {
        [Op.lt]: earliestTimeToKeep,
      },
    },
    paranoid: false,
    force: true,
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldV4NotificationChunks', responseData);
  response.send(responseData);
});

router.post('/regenerateBloomFilter', async (request, response) => {
  updateBloomFilter();
  response.sendStatus(status.NO_CONTENT);
});

router.post('/deleteOldChallenges', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get(
    'luca.challenges.operatorDeviceCreation.maxAgeMinutes'
  );
  const affectedRows = await database.Challenge.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'minutes'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldChallenges', responseData);
  response.send(responseData);
});

router.post('/deleteUnactivatedDevices', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.operatorDevice.unactivated.maxAgeMinutes');
  const affectedRows = await database.OperatorDevice.destroy({
    where: {
      activated: false,
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'minutes'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteUnactivatedDevices', responseData);
  response.send(responseData);
});

router.post('/deleteOldAuditLogs', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.auditLogs.maxAge');
  const affectedRows = await database.HealthDepartmentAuditLog.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteOldAuditLogs', responseData);
  response.send(responseData);
});

router.post('/updateTrustList', async (request, response) => {
  const t0 = performance.now();

  const url = config.get('luca.trustList.source');

  const validator = async data => {
    const [signature, payload] = data.split('\n');

    const isValid = VERIFY_EC_SHA256_IEEE_SIGNATURE(
      base64ToHex(config.get('luca.trustList.trustAnchor')),
      bytesToHex(payload),
      base64ToHex(signature)
    );

    if (!isValid) {
      throw new Error('Verification failed');
    }

    const parsedPayload = JSON.parse(payload);

    await z.dsgcPayload().parseAsync(parsedPayload);

    return true;
  };

  await cacheUrl(url, validator);

  const responseData = {
    time: performance.now() - t0,
  };
  await logJobCompletion('updateTrustList', responseData);
  response.send(responseData);
});

const listValidator = async payload => {
  const parsedPayload = JSON.parse(payload);
  await z.dccListPayload().parseAsync(parsedPayload);
  return true;
};

router.post('/updateDCCRules', async (request, response) => {
  const t0 = performance.now();

  const url = config.get('luca.dcc.source');

  const data = await cacheUrl(url, listValidator);

  if (data) {
    const ruleValidator = async (payload, headers) => {
      const isValid = VERIFY_EC_SHA256_DER_SIGNATURE(
        base64ToHex(config.get('luca.dcc.trustAnchor')),
        bytesToHex(SHA256(bytesToHex(encodeUtf8(payload)))),
        base64ToHex(headers['x-signature'])
      );

      if (!isValid) {
        throw new Error('Verification failed');
      }

      const parsedPayload = JSON.parse(payload);

      await z.dccRulePayload().parseAsync(parsedPayload);

      return true;
    };

    for (const { hash } of JSON.parse(data)) {
      const ruleUrl = `${url}/${hash}`;
      await cacheUrl(ruleUrl, ruleValidator);
    }
  }

  const responseData = {
    time: performance.now() - t0,
  };
  await logJobCompletion('updateDCCList', responseData);
  response.send(responseData);
});

router.post('/deleteOldConnectContacts', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.connect.contacts.maxAge');
  const affectedRows = await database.ConnectContact.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };

  await logJobCompletion('deleteOldConnectContacts', responseData);

  response.send(responseData);
});

router.post('/deleteOldConnectConversations', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.connect.conversations.maxAge');
  const affectedRows = await database.ConnectConversation.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };

  await logJobCompletion('deleteOldConnectConversations', responseData);

  response.send(responseData);
});

router.post('/deleteOldConnectSearchProcesses', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.connect.searchProcesses.maxAge');
  const affectedRows = await database.ConnectSearchProcess.destroy({
    where: {
      createdAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
    },
  });

  const responseData = { affectedRows, time: performance.now() - t0 };

  await logJobCompletion('deleteOldConnectSearchProcesses', responseData);

  response.send(responseData);
});

router.post('/deleteOldPowUserAgents', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('pow.userAgentMaxAge');
  const affectedRows = await database.PowChallenge.update(
    {
      userAgent: null,
    },
    {
      where: {
        userAgent: { [Op.not]: null },
        createdAt: {
          [Op.lt]: moment().subtract(maxAge, 'hours'),
        },
      },
    }
  );
  const responseData = { affectedRows, time: performance.now() - t0 };

  await logJobCompletion('deleteOldPowUserAgents', responseData);
  response.send(responseData);
});

router.post('/deleteInactiveUsers', async (request, response) => {
  const t0 = performance.now();
  const maxAge = config.get('luca.users.inactive.maxAge');
  const affectedRows = await database.User.destroy({
    where: {
      updatedAt: {
        [Op.lt]: moment().subtract(maxAge, 'hours'),
      },
      deviceType: null,
    },
    force: true,
  });
  const responseData = { affectedRows, time: performance.now() - t0 };
  await logJobCompletion('deleteInactiveUsers', responseData);
  response.send(responseData);
});

module.exports = router;
