import config from 'config';
import { z } from 'utils/validation';
import {
  LocationType,
  LocationEntryPolicyTypes,
  LocationEntryPolicyInfoTypes,
  LocationMaskTypes,
  LocationVentilationTypes,
} from 'constants/location';

export const createSchema = z.object({
  type: z.nativeEnum(LocationType),
  name: z.safeString().max(255),
  firstName: z.safeString().max(255).optional(),
  lastName: z.safeString().max(255).optional(),
  phone: z.phoneNumber(),
  streetName: z.safeString().max(255),
  streetNr: z.safeString().max(255),
  zipCode: z.zipCode(),
  city: z.safeString().max(255),
  state: z.safeString().max(255).optional().nullable(),
  lat: z.number().optional().nullable(),
  lng: z.number().optional().nullable(),
  radius: z.number().int().nonnegative().optional().nullable(),
  entryPolicy: z.nativeEnum(LocationEntryPolicyTypes).optional().nullable(),
  tableCount: z.number().int().positive().max(1000).optional().nullable(),
  additionalData: z
    .array(
      z.object({
        key: z.safeString().max(255),
        label: z.safeString().max(255).optional(),
        isRequired: z.boolean().optional(),
      })
    )
    .max(config.get('luca.locations.maxAdditionalData'))
    .optional(),
  areas: z
    .array(
      z.object({
        name: z.safeString().max(255),
        isIndoor: z.boolean(),
      })
    )
    .max(20)
    .optional(),
  isIndoor: z.boolean(),
  masks: z.nativeEnum(LocationMaskTypes).optional().nullable(),
  ventilation: z.nativeEnum(LocationVentilationTypes).optional().nullable(),
  entryPolicyInfo: z
    .nativeEnum(LocationEntryPolicyInfoTypes)
    .optional()
    .nullable(),
  roomHeight: z.number().optional().nullable(),
  roomWidth: z.number().optional().nullable(),
  roomDepth: z.number().optional().nullable(),
  averageCheckinTime: z
    .number()
    .int()
    .positive()
    .max(1440)
    .min(15)
    .optional()
    .nullable(),
});

export const searchSchema = z.object({
  name: z.string().min(3).max(128).optional(),
  zipCode: z.zipCode().optional(),
  limit: z.integerString({ lt: 101, gt: 0 }).optional(),
  offset: z.integerString().optional(),
});

export const groupIdSchema = z.object({
  groupId: z.uuid(),
});

export const deleteSchema = z.object({
  password: z.string().max(255),
});

export const updateSchema = z.object({
  name: z.safeString().max(255).optional(),
  phone: z.phoneNumber().optional(),
});
