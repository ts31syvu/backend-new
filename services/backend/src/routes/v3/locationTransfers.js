/* eslint-disable max-lines */

const router = require('express').Router();
const moment = require('moment');
const config = require('config');
const status = require('http-status');

const {
  checkForAndAddLevel4RiskLevels,
} = require('../../utils/notifications/notificationsHelper');

const {
  database,
  Location,
  Operator,
  LocationTransfer,
  TracingProcess,
  LocationTransferTrace,
  LocationGroup,
  HealthDepartment,
  HealthDepartmentEmployee,
} = require('../../database');
const {
  sendShareDataRequestNotification,
  sendLocationTransferApprovalNotification,
  sendHdLocationTransferApprovalNotification,
} = require('../../utils/mailClient');
const {
  validateSchema,
  validateParametersSchema,
} = require('../../middlewares/validateSchema');
const {
  requireOperatorDeviceRole,
  requireOperatorOROperatorDevice,
} = require('../../middlewares/requireUser');

const {
  requireHealthDepartmentEmployee,
} = require('../../middlewares/requireUser');
const { formatLocationName } = require('../../utils/format');
const { OperatorDevice } = require('../../constants/operatorDevice');
const { AuditLogEvents, AuditStatusType } = require('../../constants/auditLog');
const { logEvent } = require('../../utils/hdAuditLog');

const {
  sendSchema,
  transferIdParametersSchema,
} = require('./locationTransfers.schemas');
const {
  limitRequestsByUserPerHour,
  limitRequestsByUserPerDay,
} = require('../../middlewares/rateLimit');

const logEmailError = (request, error) =>
  request.log.error(error, 'failed to send email');

/**
 * Send an email to the venue of the given location transfer request. The venue
 * has to specifically assist in the tracing process by removing the venue's
 * layer of encryption of contact data references
 * @see https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-contact-data-reference
 */
router.post(
  '/:transferId/contact',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerHour('locationtransfer_contact_post_ratelimit_hour'),
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const transfer = await LocationTransfer.findOne({
      where: {
        uuid: request.params.transferId,
      },
      include: [
        {
          required: true,
          model: HealthDepartment,
          attributes: ['name'],
          where: {
            uuid: request.user.departmentId,
          },
        },
        {
          required: true,
          model: Location,
          include: {
            model: Operator,
            attributes: ['uuid', 'email', 'lastName', 'firstName', 'language'],
            paranoid: false,
          },
          paranoid: false,
        },
      ],
    });

    if (!transfer) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const amount = await LocationTransferTrace.count({
      where: {
        locationTransferId: transfer.uuid,
      },
    });

    if (transfer.isCompleted) {
      return response.sendStatus(status.GONE);
    }

    logEvent(request.user, {
      type: AuditLogEvents.REQUEST_DATA,
      status: AuditStatusType.SUCCESS,
      meta: {
        processId: transfer.tracingProcessId,
        transferId: transfer.uuid,
        locationId: transfer.locationId,
        timeframe: transfer.time,
        amountOfTraces: amount,
      },
    });

    await database.transaction(async transaction => {
      await transfer.update({ contactedAt: moment() }, transaction);
      await checkForAndAddLevel4RiskLevels(transfer, transaction);
    });

    try {
      sendShareDataRequestNotification(
        transfer.Location.Operator.email,
        transfer.Location.Operator.fullName,
        transfer.Location.Operator.transactionalLanguage,
        {
          firstName: transfer.Location.Operator.firstName,
          departmentName: transfer.HealthDepartment.name,
          transferUrl: `https://${config.get('hostname')}/shareData/${
            transfer.uuid
          }`,
        }
      );
    } catch (error) {
      logEmailError(request, error);
    }
    return response.sendStatus(status.NO_CONTENT);
  }
);

/**
 * Fetch transferred traces of a given location transfer after the request has been fulfilled by a venue, meaning
 * the venue has provided encrypted contact data references without the venue's layer of encryption
 * @see https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html
 */
router.get(
  '/:transferId/traces',
  requireHealthDepartmentEmployee,
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const transfer = await LocationTransfer.findOne({
      where: {
        uuid: request.params.transferId,
        departmentId: request.user.departmentId,
      },
    });
    if (!transfer) {
      logEvent(request.user, {
        type: AuditLogEvents.VIEW_DATA,
        status: AuditStatusType.ERROR_TARGET_NOT_FOUND,
        meta: {
          transferId: request.params.transferId,
        },
      });

      return response.sendStatus(status.NOT_FOUND);
    }

    const traces = await LocationTransferTrace.findAll({
      where: {
        locationTransferId: request.params.transferId,
      },
    });

    logEvent(request.user, {
      type: AuditLogEvents.VIEW_DATA,
      status: AuditStatusType.SUCCESS,
      meta: {
        processId: transfer.tracingProcessId,
        transferId: request.params.transferId,
        locationId: transfer.locationId,
        timeframe: transfer.time,
        amountOfTraces: traces.length,
      },
    });

    return response.send({
      traces: traces.map(trace => ({
        traceId: trace.traceId,
        checkin: moment(trace.time[0].value).unix(),
        checkout: moment(trace.time[1].value).unix(),
        data: {
          data: trace.data,
          publicKey: trace.dataPublicKey,
          iv: trace.dataIV,
          mac: trace.dataMAC,
        },
        publicKey: trace.publicKey,
        keyId: trace.keyId,
        version: trace.version,
        verification: trace.verification,
        deviceType: trace.deviceType,
        isContactDataIncluded: trace.isContactDataIncluded,
        additionalData: trace.additionalData
          ? {
              data: trace.additionalData,
              iv: trace.additionalDataIV,
              mac: trace.additionalDataMAC,
              publicKey: trace.additionalDataPublicKey,
            }
          : null,
      })),
      initialTracesCount: transfer.initialTracesCount,
    });
  }
);

/**
 * Express middleware to validate a transferId before potentially accepting
 * large JSON payloads
 */
const validateTransferId = async (request, response, next) => {
  const transfer = await LocationTransfer.findByPk(request.params.transferId, {
    include: [
      {
        required: true,
        model: Location,
        where: {
          operator: request.user.uuid,
        },
        attributes: ['uuid', 'name'],
        include: [
          {
            model: LocationGroup,
            attributes: ['name'],
            paranoid: false,
          },
          {
            model: Operator,
            attributes: ['email', 'firstName', 'lastName', 'language'],
            paranoid: false,
          },
        ],
        paranoid: false,
      },
      {
        model: HealthDepartment,
        attributes: ['name', 'uuid', 'email'],
      },
      {
        model: TracingProcess,
        include: [
          {
            model: HealthDepartmentEmployee,
            attributes: ['email', 'firstName', 'lastName'],
          },
        ],
      },
    ],
  });

  if (!transfer) {
    return response.sendStatus(status.NOT_FOUND);
  }

  request.transfer = transfer;
  return next();
};

/**
 * Extract all necessary information from transfer object provided by middleware.
 */
const extractEmailProperties = transfer => {
  const toEmail = transfer.TracingProcess.HealthDepartmentEmployee
    ? transfer.TracingProcess.HealthDepartmentEmployee.email
    : transfer.HealthDepartment.email;

  const toName = transfer.TracingProcess.HealthDepartmentEmployee
    ? transfer.TracingProcess.HealthDepartmentEmployee.fullName
    : transfer.HealthDepartment.name;

  const assignee = transfer.TracingProcess.HealthDepartmentEmployee
    ? toName
    : 'nicht verfügbar';

  return {
    toEmail,
    toName,
    assignee,
  };
};

/**
 * Upload trace data associated with the given transfer request. This is done by a venue fulfilling the location transfer
 * request by a health department. The venue will remove its layer of encryption of the contact data references and upload
 * the resulting data to the luca server. Health departments still need to decrypt the references using their private key.
 * @see https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html
 * @see https://www.luca-app.de/securityoverview/properties/secrets.html#term-contact-data-reference
 */
router.post(
  '/:transferId',
  validateParametersSchema(transferIdParametersSchema),
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRole(OperatorDevice.manager),
  limitRequestsByUserPerDay('locationtransfer_post_ratelimit_day', {
    skipSuccessfulRequests: true,
  }),
  validateTransferId,
  validateSchema(sendSchema, '20mb'),
  async (request, response) => {
    const { transfer, body } = request;
    const {
      traces,
      masks,
      ventilation,
      roomHeight,
      roomWidth,
      roomDepth,
      entryPolicyInfo,
      isIndoor,
    } = body;

    const transferTraces = await LocationTransferTrace.findAll({
      where: {
        locationTransferId: transfer.uuid,
      },
    });

    const requestTracesById = new Map(
      traces.map(trace => [trace.traceId, trace])
    );
    const locationTransferTraceIds = new Set(
      transferTraces.map(trace => trace.traceId)
    );
    // reject request if invalid trace IDs are contained
    if (!traces.every(trace => locationTransferTraceIds.has(trace.traceId))) {
      return response.sendStatus(status.BAD_REQUEST);
    }

    const updatePromises = transferTraces
      .map(transferTrace => {
        const requestTraceData = requestTracesById.get(transferTrace.traceId);

        // skip traces that weren't contained in the request
        if (!requestTraceData) {
          return null;
        }
        const transferTracePayload = {
          publicKey: requestTraceData.publicKey,
          verification: requestTraceData.verification,
          keyId: requestTraceData.keyId,
          version: requestTraceData.version,
          data: requestTraceData.data.data,
          dataPublicKey: requestTraceData.data.publicKey,
          dataIV: requestTraceData.data.iv,
          dataMAC: requestTraceData.data.mac,
        };

        if (requestTraceData.additionalData) {
          transferTracePayload.additionalData =
            requestTraceData.additionalData.data;
          transferTracePayload.additionalDataPublicKey =
            requestTraceData.additionalData.publicKey;
          transferTracePayload.additionalDataIV =
            requestTraceData.additionalData.iv;
          transferTracePayload.additionalDataMAC =
            requestTraceData.additionalData.mac;
        }

        return transferTrace.update(transferTracePayload);
      })
      .filter(updateTransfer => updateTransfer !== null);

    await Promise.all(updatePromises);

    await transfer.update({
      isCompleted: true,
      approvedAt: moment(),
      masks,
      ventilation,
      roomHeight,
      roomWidth,
      roomDepth,
      entryPolicyInfo,
      isIndoor,
    });

    logEvent(
      {
        departmentId: transfer.HealthDepartment.uuid,
      },
      {
        type: AuditLogEvents.RECEIVE_DATA,
        status: AuditStatusType.SUCCESS,
        meta: {
          transferId: transfer.uuid,
          locationId: transfer.Location.uuid,
        },
      }
    );

    const dateFormat = 'DD.MM.YYYY HH:mm';

    try {
      await sendLocationTransferApprovalNotification(
        transfer.Location.Operator.email,
        transfer.Location.Operator.fullName,
        transfer.Location.Operator.transactionalLanguage,
        {
          id: transfer.uuid,
          createdAt: moment(transfer.contactedAt).format(dateFormat),
          updatedAt: moment(transfer.approvedAt).format(dateFormat),
          departmentName: transfer.HealthDepartment.name,
          timeFrameFrom: moment(transfer.time[0].value).format(dateFormat),
          timeFrameTo: moment(transfer.time[1].value).format(dateFormat),
          locationName: formatLocationName(
            transfer.Location,
            transfer.Location.LocationGroup
          ),
        }
      );
    } catch (error) {
      logEmailError(request, error);
    }

    try {
      const { toEmail, toName, assignee } = extractEmailProperties(transfer);

      if (toEmail) {
        await sendHdLocationTransferApprovalNotification(
          toEmail,
          toName,
          null,
          {
            id: transfer.uuid,
            createdAt: moment(transfer.contactedAt).format(dateFormat),
            updatedAt: moment(transfer.approvedAt).format(dateFormat),
            timeFrameFrom: moment(transfer.time[0].value).format(dateFormat),
            timeFrameTo: moment(transfer.time[1].value).format(dateFormat),
            locationName: formatLocationName(
              transfer.Location,
              transfer.Location.LocationGroup
            ),
            assignee,
          }
        );
      } else {
        throw new Error(
          'No email for hdLocationTransferApprovalNotification available'
        );
      }
    } catch (error) {
      logEmailError(request, error);
    }

    return response.sendStatus(status.NO_CONTENT);
  }
);

module.exports = router;
