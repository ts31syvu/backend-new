import config from 'config';
import { z } from 'utils/validation';
import {
  LocationMaskTypes,
  LocationVentilationTypes,
  LocationEntryPolicyInfoTypes,
} from '../../constants/location';

export const getSchema = z.object({
  completed: z.enum(['true', 'false']).optional(),
  deleted: z.enum(['true', 'false']).optional(),
});

export const createSchema = z.object({
  locations: z
    .array(
      z.object({
        time: z.array(z.unixTimestamp()).length(2),
        locationId: z.uuid(),
      })
    )
    .nonempty()
    .max(config.get('luca.locationTransfers.maxLocations')),
  userTransferId: z.uuid().optional(),
});

export const sendSchema = z.object({
  traces: z.array(
    z.object({
      traceId: z.traceId(),
      data: z.object({
        data: z.base64({ max: 44 }),
        publicKey: z.ecPublicKey(),
        mac: z.mac(),
        iv: z.iv(),
      }),
      publicKey: z.ecCompressedPublicKey(),
      keyId: z.dailyKeyId(),
      version: z.number().int().optional(),
      verification: z.base64({ rawLength: 8 }),
      additionalData: z
        .object({
          data: z.base64({ max: 1024 }),
          publicKey: z.ecPublicKey(),
          mac: z.mac(),
          iv: z.iv(),
        })
        .optional()
        .nullable(),
    })
  ),
  masks: z
    .enum([
      LocationMaskTypes.YES,
      LocationMaskTypes.PARTIAL,
      LocationMaskTypes.NO,
    ])
    .optional(),
  ventilation: z
    .enum([
      LocationVentilationTypes.ELECTRIC,
      LocationVentilationTypes.WINDOW,
      LocationVentilationTypes.PARTIAL,
      LocationVentilationTypes.NO,
    ])
    .optional(),
  rootHeight: z.number().optional(),
  rootWidth: z.number().optional(),
  rootDepth: z.number().optional(),
  entryPolicyInfo: z
    .enum([
      LocationEntryPolicyInfoTypes.TWO_G,
      LocationEntryPolicyInfoTypes.THREE_G,
      LocationEntryPolicyInfoTypes.TWO_G_PLUS,
      LocationEntryPolicyInfoTypes.NONE,
    ])
    .optional(),
  isIndoor: z.boolean().optional(),
});

export const transferIdParametersSchema = z.object({
  transferId: z.uuid(),
});
