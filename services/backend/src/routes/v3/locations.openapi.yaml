paths:
  /locations/private:
    post:
      tags:
        - Locations
      summary: ''
      operationId: post-locations-private
      responses:
        '201':
          description: Success
          content:
            application/json:
              schema:
                type: object
                properties:
                  scannerId:
                    type: string
                    format: uuid
                  locationId:
                    type: string
                    format: uuid
                  accessId:
                    type: string
                    format: uuid
        '400':
          $ref: '#/components/schemas/BadRequest'
        '429':
          $ref: '#/components/schemas/TooManyRequestsDay'
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                publicKey:
                  type: string
                  minLength: 88
                  maxLength: 88
              required:
                - publicKey
  '/locations/{locationId}':
    parameters:
      - schema:
          type: string
          format: uuid
        name: locationId
        in: path
        required: true
    get:
      tags:
        - Locations
      summary: Gets a location by ID
      operationId: get-locations-id
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: object
                properties:
                  locationId:
                    type: string
                  publicKey:
                    type: string
                  name:
                    type: string
                  groupName:
                    type: string
                  locationName:
                    type: string
                  lat:
                    type: number
                  lng:
                    type: number
                  radius:
                    type: number
                  isPrivate:
                    type: boolean
                  averageCheckinTime:
                    type: number
                  entryPolicy:
                    type: string
                required:
                  - locationId
                  - publicKey
                  - name
                  - groupName
                  - locationName
                  - lat
                  - lng
                  - radius
                  - isPrivate
                  - averageCheckinTime
        '400':
          $ref: '#/components/schemas/BadRequest'
        '404':
          description: Location not found
      description: |-
        Gets a location (also referred to as [venue](https://luca-app.de/securityconcept/processes/venue_registration.html)) by its primary ID.
  '/locations/traces/{accessId}':
    parameters:
      - schema:
          type: string
        name: accessId
        in: path
        required: true
    get:
      summary: Get traces for an access ID
      tags:
        - Locations
      operationId: get-locations-traces-accessId
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                description: ''
                minItems: 1
                uniqueItems: true
                items:
                  type: object
                  properties:
                    traceId:
                      type: string
                      minLength: 1
                    checkin:
                      type: number
                    checkout:
                      type: number
                    data:
                      type: object
                  required:
                    - traceId
                    - checkin
                    - checkout
        '400':
          $ref: '#/components/schemas/BadRequest'
        '404':
          description: Location not found or not private Location
        '429':
          $ref: '#/components/schemas/TooManyRequestsHour'
  '/locations/{accessId}':
    parameters:
      - schema:
          type: string
        name: accessId
        in: path
        required: true
    delete:
      tags:
        - Locations
      summary: Delete location
      operationId: delete-locations-accessId
      responses:
        '204':
          description: Success
        '400':
          $ref: '#/components/schemas/BadRequest'
        '404':
          description: Location not found or not private Location
        '429':
          $ref: '#/components/schemas/TooManyRequestsDay'
      description: Deletes a location (by accessId) and ends all ongoing traces.
