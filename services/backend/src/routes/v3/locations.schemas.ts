import { z } from 'utils/validation';

export const locationIdParametersSchema = z.object({
  locationId: z.uuid(),
});

export const privateEventCreateSchema = z.object({
  publicKey: z.ecPublicKey(),
});

export const locationTracesQuerySchema = z.object({
  duration: z.enum(['today', 'week']).optional(),
});

export const accessIdParametersSchema = z.object({
  accessId: z.uuid(),
});
