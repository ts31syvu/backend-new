import { Router, RequestHandler } from 'express';
import { z } from 'zod';
import status from 'http-status';
import { fromUrl, parseDomain, ParseResultType } from 'parse-domain';
import {
  LocationURL,
  Location,
  LocationURLAllowRules,
  LocationURLDenyRules,
  LocationURLDomainBlockList,
} from 'database';
import { UrlType, LocationURLInstance } from 'database/models/locationUrl';
import {
  requireOperator,
  requireNonDeletedUser,
} from 'middlewares/requireUser';
import {
  validateSchema,
  validateParametersSchema,
} from 'middlewares/validateSchema';
import {
  limitRequestsByUserPerDay,
  limitRequestsPerHour,
} from 'middlewares/rateLimit';
import { updateSchema, parameterSchema } from './urls.schemas';

const router = Router();

const requireLocationOwnedByOperator: RequestHandler = async (
  request,
  response,
  next
) => {
  const isOwnedByOperator =
    (await Location.count({
      where: {
        uuid: request.params.locationId,
        operator: request.user.uuid,
      },
    })) === 1;

  if (!isOwnedByOperator) {
    return response.sendStatus(status.FORBIDDEN);
  }
  return next();
};

const isValidURL = async (url: string | null): Promise<boolean> => {
  if (url === null) return true;
  const parseResult = parseDomain(fromUrl(url));
  if (parseResult.type !== ParseResultType.Listed) return false;

  const blockedDomain = await LocationURLDomainBlockList.findOne({
    where: {
      domain: [parseResult.domain, ...parseResult.topLevelDomains].join('.'),
    },
  });

  if (blockedDomain) {
    return false;
  }

  const locationUrlAllowRules = await LocationURLAllowRules.findAll();

  const allowRules = locationUrlAllowRules.map(entity => entity.rule);

  // eslint-disable-next-line security/detect-non-literal-regexp
  if (allowRules.some(rule => new RegExp(rule).test(url))) {
    return true;
  }

  const locationUrlDenyRules = await LocationURLDenyRules.findAll();

  const denyRules = locationUrlDenyRules.map(entity => entity.rule);

  // eslint-disable-next-line security/detect-non-literal-regexp
  if (denyRules.some(rule => new RegExp(rule).test(url))) {
    return false;
  }

  return true;
};

type UrlDto = Record<UrlType, string | null>;

const getUrlDto = (urls: LocationURLInstance[]): UrlDto => {
  const urlMap = new Map(urls.map(url => [url.type, url.url]));
  const dto: Partial<UrlDto> = {};
  Object.values(UrlType).forEach((type: UrlType) => {
    // eslint-disable-next-line security/detect-object-injection
    dto[type] = urlMap.get(type) || null;
  });
  return dto as UrlDto;
};

router
  .route('/:locationId/urls')
  .get<z.infer<typeof parameterSchema>>(
    limitRequestsPerHour('location_get_urls_hour'),
    validateParametersSchema(parameterSchema),
    async (request, response) => {
      const urls = await LocationURL.findAll({
        where: {
          locationId: request.params.locationId,
        },
        attributes: ['type', 'url'],
      });

      return response.send(getUrlDto(urls));
    }
  )
  .patch<
    z.infer<typeof parameterSchema>,
    unknown,
    z.infer<typeof updateSchema>
  >(
    requireOperator,
    requireNonDeletedUser,
    limitRequestsByUserPerDay('operator_update_location_url_user_hour'),
    validateParametersSchema(parameterSchema),
    validateSchema(updateSchema),
    requireLocationOwnedByOperator,
    async (request, response) => {
      const isValid = await isValidURL(request.body.url);
      if (!isValid) {
        return response.sendStatus(status.BAD_REQUEST);
      }

      await LocationURL.upsert({
        locationId: request.params.locationId,
        type: request.body.type,
        url: request.body.url,
      });

      return response.sendStatus(status.NO_CONTENT);
    }
  );

export default router;
