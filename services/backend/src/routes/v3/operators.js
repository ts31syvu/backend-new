/* eslint-disable max-lines */

const crypto = require('crypto');
const router = require('express').Router();
const status = require('http-status');
const moment = require('moment');
const config = require('config');

const {
  validateOperatorStrongPassword,
} = require('../../middlewares/validateOperatorStrongPassword');
const {
  database,
  Operator,
  EmailActivation,
  Location,
  OperatorKeyHistory,
  OperatorDevice: OperatorDeviceModel,
} = require('../../database');
const mailClient = require('../../utils/mailClient');
const { generateSupportCode } = require('../../utils/generators');
const { getPreferredLanguageFromRequest } = require('../../utils/language');
const { randomSleep } = require('../../utils/sleep');
const { validateSchema } = require('../../middlewares/validateSchema');

const {
  requireOperator,
  requireNonDeletedUser,
  requireOperatorDeviceRoles,
  requireOperatorOROperatorDevice,
} = require('../../middlewares/requireUser');
const {
  limitRequestsPerDay,
  limitRequestsByUserPerDay,
} = require('../../middlewares/rateLimit');

const { OperatorDevice } = require('../../constants/operatorDevice');

const {
  requireNonBlockedIp,
} = require('../../middlewares/requireNonBlockedIp');
const locationsRouter = require('./operators/locations');
const passwordRouter = require('./operators/password');
const emailsRouter = require('./operators/email');
const tracesRouter = require('./operators/traces');
const locationGroupsRouter = require('./operators/locationGroups');

const {
  createSchema,
  activationSchema,
  storePublicKeySchema,
  updateOperatorSchema,
  supportSchema,
  publicKeyResetSchema,
} = require('./operators.schemas');

// create operator
router.post(
  '/',
  limitRequestsPerDay('operators_post_ratelimit_day'),
  requireNonBlockedIp,
  validateSchema(createSchema),
  validateOperatorStrongPassword(),
  async (request, response) => {
    const existingOperator = await Operator.findOne({
      where: {
        username: request.body.email,
      },
      paranoid: false,
    });

    if (existingOperator) {
      mailClient.sendRegisterAttemptNotification(
        existingOperator.email,
        existingOperator.fullName,
        request.body.lang,
        {
          link: `https://${config.get('hostname')}/forgotPassword`,
        }
      );
      await randomSleep();
      return response.sendStatus(status.NO_CONTENT);
    }

    let operator;
    let activationMail;

    await database.transaction(async transaction => {
      operator = await Operator.create(
        {
          firstName: request.body.firstName,
          lastName: request.body.lastName,
          businessEntityName: request.body.businessEntityName,
          businessEntityStreetName: request.body.businessEntityStreetName,
          businessEntityStreetNumber: request.body.businessEntityStreetNumber,
          businessEntityZipCode: request.body.businessEntityZipCode,
          businessEntityCity: request.body.businessEntityCity,
          email: request.body.email,
          username: request.body.email,
          password: request.body.password,
          phone: request.body.phone,
          salt: crypto.randomBytes(16).toString('base64'),
          privateKeySecret: crypto.randomBytes(32).toString('base64'),
          supportCode: generateSupportCode(),
          avvAccepted: request.body.avvAccepted,
          lastVersionSeen: request.body.lastVersionSeen,
          language: getPreferredLanguageFromRequest(request),
        },
        { transaction }
      );

      activationMail = await EmailActivation.create(
        {
          operatorId: operator.uuid,
          email: operator.email,
          type: 'Registration',
        },
        { transaction }
      );
    });

    mailClient.sendActivationMail(
      activationMail.email,
      operator.fullName,
      operator.transactionalLanguage,
      {
        firstName: request.body.firstName,
        activationLink: `https://${config.get('hostname')}/activation/${
          activationMail.uuid
        }`,
      }
    );

    await randomSleep();
    return response.sendStatus(status.NO_CONTENT);
  }
);

// active operator account
router.post(
  '/activate',
  validateSchema(activationSchema),
  async (request, response) => {
    const { activationId } = request.body;

    const activationMail = await EmailActivation.findOne({
      where: {
        uuid: activationId,
        type: 'Registration',
      },
    });

    if (!activationMail) {
      return response.sendStatus(status.NOT_FOUND);
    }

    if (activationMail.closed) {
      return response.sendStatus(status.CONFLICT);
    }

    if (
      activationMail.createdAt <
      moment().subtract(config.get('emails.expiry'), 'hours')
    ) {
      return response.sendStatus(status.GONE);
    }

    const operator = await Operator.findByPk(activationMail.operatorId);

    if (!operator) {
      return response.sendStatus(status.NOT_FOUND);
    }

    await database.transaction(async transaction => {
      return Promise.all([
        activationMail.update(
          {
            closed: true,
          },
          { transaction }
        ),
        operator.update(
          {
            activated: true,
          },
          { transaction }
        ),
      ]);
    });

    mailClient.sendRegistrationConfirmation(
      activationMail.email,
      operator.fullName,
      operator.transactionalLanguage,
      {
        firstName: operator.firstName,
      }
    );

    return response.sendStatus(status.CREATED);
  }
);

// set operator public key
router.post(
  '/publicKey',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(storePublicKeySchema),
  async (request, response) => {
    if (request.user.publicKey) {
      return response.sendStatus(status.FORBIDDEN);
    }

    request.user.update({
      publicKey: request.body.publicKey,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// update operator
router.patch(
  '/',
  requireOperator,
  requireNonDeletedUser,
  validateSchema(updateOperatorSchema),
  async (request, response) => {
    const operator = request.user;
    await operator.update({
      firstName: request.body.firstName,
      lastName: request.body.lastName,
      phone: request.body.phone,
      businessEntityName: request.body.businessEntityName,
      businessEntityStreetName: request.body.businessEntityStreetName,
      businessEntityStreetNumber: request.body.businessEntityStreetNumber,
      businessEntityZipCode: request.body.businessEntityZipCode,
      businessEntityCity: request.body.businessEntityCity,
      avvAccepted: request.body.avvAccepted,
      lastVersionSeen: request.body.lastVersionSeen,
      languageOverwrite: request.body.languageOverwrite,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// get private key secret
router.get(
  '/privateKeySecret',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  (request, response) => {
    return response.send({
      privateKeySecret: request.user.privateKeySecret,
    });
  }
);

// get updated time for last key pair
router.get(
  '/lastKeyUpdate',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.employee, OperatorDevice.manager]),
  async (request, response) => {
    const latestKeyHistory = await OperatorKeyHistory.findOne({
      where: { operatorId: request.user.uuid },
      order: [['createdAt', 'DESC']],
    });

    return response.send({
      lastUpdated: latestKeyHistory?.createdAt,
    });
  }
);

router.post(
  '/confirmKeyImported',
  requireOperator,
  async (request, response) => {
    const operator = request.user;

    await operator.update({
      lastSeenPrivateKey: moment(),
    });

    return response.send(status.NO_CONTENT);
  }
);

// request account deactivation
router.delete('/', requireOperator, async (request, response) => {
  const operator = request.user;

  const locations = await operator.getLocations();

  await database.transaction(async transaction => {
    for (const location of locations) {
      await location.checkoutAllTraces(transaction);
    }
    await operator.destroy({ transaction });
  });

  const deletionScheduledAfter = moment()
    .add(config.get('luca.operators.deleted.maxAgeHours'), 'hours')
    .unix();

  response.send({
    deletionScheduledAfter,
  });
});

// support
router.post(
  '/support',
  requireOperator,
  limitRequestsPerDay('operators_support_email_post_ratelimit_day'),
  validateSchema(supportSchema),
  async (request, response) => {
    const { phone, requestText } = request.body;

    const requestTime = moment().format('DD.MM.YYYY HH:mm');
    mailClient.sendLocationsSupportMail(
      'locations@luca-app.de',
      'Locations Support Mail',
      null,
      {
        userPhone: phone,
        userEmail: request.user.email,
        userName: request.user.fullName,
        requestText,
        requestTime,
        supportCode: request.user.supportCode,
      }
    );

    return response.sendStatus(status.NO_CONTENT);
  }
);

// undo account deactivation
router.post('/restore', requireOperator, async (request, response) => {
  const operator = request.user;
  await operator.restore();
  response.sendStatus(status.NO_CONTENT);
});

router.patch(
  '/publicKey',
  requireOperator,
  limitRequestsByUserPerDay('operators_reset_public_key_user_ratelimit_day'),
  validateSchema(publicKeyResetSchema),
  async (request, response) => {
    const { publicKey, privateKeySecret, password } = request.body;
    const operator = request.user;

    const isPasswordCorrect = await operator.checkPassword(password);

    if (!isPasswordCorrect) {
      return response.sendStatus(status.FORBIDDEN);
    }

    await database.transaction(async transaction => {
      await OperatorKeyHistory.create(
        {
          operatorId: operator.uuid,
          publicKey: operator.publicKey,
          privateKeySecret: operator.privateKeySecret,
        },
        { transaction }
      );

      await operator.update(
        {
          publicKey,
          privateKeySecret,
        },
        { transaction }
      );

      await Location.update(
        {
          publicKey,
        },
        {
          where: {
            operator: operator.uuid,
          },
          transaction,
        }
      );
      await OperatorDeviceModel.destroy({
        where: {
          operatorId: operator.uuid,
        },
        transaction,
      });
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.use('/locations', locationsRouter);
router.use('/password', passwordRouter);
router.use('/email', emailsRouter);
router.use('/traces', tracesRouter.default);
router.use('/locationGroups', locationGroupsRouter.default);

module.exports = router;
