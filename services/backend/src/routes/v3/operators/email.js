/**
 * @overview Provides endpoints to venue operators to update their email, which they are required to provide
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html?highlight=email#process
 */

const router = require('express').Router();
const status = require('http-status');
const moment = require('moment');
const config = require('config');
const { Op } = require('sequelize');

const database = require('../../../database');
const mailClient = require('../../../utils/mailClient');
const { randomSleep } = require('../../../utils/sleep');
const { validateSchema } = require('../../../middlewares/validateSchema');

const {
  limitRequestsPerHour,
  limitRequestsPerDay,
  limitRequestsByUserPerDay,
} = require('../../../middlewares/rateLimit');
const {
  requireOperator,
  requireNonDeletedUser,
} = require('../../../middlewares/requireUser');

const { updateMailSchema, activationSchema } = require('./email.schemas');

// update email
router.patch(
  '/',
  requireOperator,
  requireNonDeletedUser,
  limitRequestsByUserPerDay('operator_email_patch_user_ratelimit_day'),
  limitRequestsPerDay('operator_email_patch_ratelimit_day'),
  validateSchema(updateMailSchema),
  async (request, response) => {
    const operator = request.user;

    await database.EmailActivation.update(
      {
        discarded: true,
      },
      {
        where: {
          operatorId: operator.uuid,
          type: 'EmailChange',
          discarded: false,
          closed: false,
        },
      }
    );

    const { email, password } = request.body;

    const isValidPassword = await operator.checkPassword(password);

    if (!isValidPassword) {
      return response.sendStatus(status.FORBIDDEN);
    }

    const existingUser = await database.Operator.findOne({
      where: { username: email },
    });

    if (existingUser) {
      await database.EmailActivation.create({
        operatorId: operator.uuid,
        email: null,
        type: 'EmailChange',
      });

      await randomSleep();
      return response.sendStatus(status.NO_CONTENT);
    }

    const activationMail = await database.EmailActivation.create({
      operatorId: operator.uuid,
      email,
      type: 'EmailChange',
    });

    mailClient.sendUpdateEmail(
      email,
      operator.fullName,
      operator.transactionalLanguage,
      {
        firstName: operator.firstName,
        activationLink: `https://${config.get('hostname')}/activateEmail/${
          activationMail.uuid
        }`,
      }
    );

    await randomSleep();
    return response.sendStatus(status.NO_CONTENT);
  }
);

// check if email change is in progress
router.get(
  '/isChangeActive',
  requireOperator,
  requireNonDeletedUser,
  async (request, response) => {
    const operator = request.user;

    const activationMail = await database.EmailActivation.findOne({
      where: {
        operatorId: operator.uuid,
        closed: false,
        discarded: false,
        createdAt: {
          [Op.gt]: moment().subtract(config.get('emails.expiry'), 'hours'),
        },
        type: 'EmailChange',
      },
    });

    if (!activationMail) {
      return response.sendStatus(status.NOT_FOUND);
    }

    return response.sendStatus(status.OK);
  }
);

// confirm email change
router.post(
  '/confirm',
  limitRequestsPerHour('operator_email_confirm_post_ratelimit_hour', {
    skipSuccessfulRequests: true,
  }),
  validateSchema(activationSchema),
  async (request, response) => {
    const { activationId } = request.body;

    const activationMail = await database.EmailActivation.findOne({
      where: {
        uuid: activationId,
        discarded: false,
        type: 'EmailChange',
      },
    });

    if (!activationMail || activationMail.email === null) {
      return response.sendStatus(status.NOT_FOUND);
    }

    if (activationMail.closed) {
      return response.sendStatus(status.CONFLICT);
    }

    if (
      activationMail.createdAt <
      moment().subtract(config.get('emails.expiry'), 'hours')
    ) {
      return response.sendStatus(status.GONE);
    }

    const operator = await database.Operator.findByPk(
      activationMail.operatorId
    );

    if (!operator) {
      return response.sendStatus(status.NOT_FOUND);
    }

    if (operator.deletedAt) {
      return response.status(status.FORBIDDEN).send({
        message: 'The account has been marked for deletion',
        errorCode: 'ACCOUNT_DEACTIVATED',
      });
    }

    const originalEmail = operator.email;
    const newEmail = activationMail.email;

    const emailAlreadyTaken = await database.Operator.findOne({
      where: {
        username: newEmail,
      },
    });

    if (emailAlreadyTaken) {
      return response.sendStatus(status.CONFLICT);
    }

    await database.database.transaction(transaction => {
      return Promise.all([
        activationMail.update(
          {
            closed: true,
          },
          { transaction }
        ),
        operator.update(
          {
            email: newEmail,
            username: newEmail,
          },
          { transaction }
        ),
      ]);
    });

    mailClient.sendUpdateEmailNotification(
      originalEmail,
      operator.fullName,
      operator.transactionalLanguage,
      {
        originalEmail,
        newEmail,
      }
    );

    return response.sendStatus(status.NO_CONTENT);
  }
);

module.exports = router;
