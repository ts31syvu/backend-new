const { z } = require('../../../utils/validation');

const updateMailSchema = z.object({
  email: z.email(),
  password: z.string().max(255),
});

const activationSchema = z.object({
  activationId: z.uuid(),
});

module.exports = {
  updateMailSchema,
  activationSchema,
};
