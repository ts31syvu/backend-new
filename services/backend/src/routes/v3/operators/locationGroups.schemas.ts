import { z } from 'utils/validation';

export const groupIdParametersSchema = z.object({
  groupId: z.uuid(),
});
