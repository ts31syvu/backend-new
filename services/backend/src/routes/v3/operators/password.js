/**
 * @overview Provides endpoints allowing venue operators to update their
 * respective passwords, required for operating the venue owner frontend
 *
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html?highlight=password#process
 */

const router = require('express').Router();
const status = require('http-status');
const config = require('config');
const moment = require('moment');
const { Op } = require('sequelize');
const {
  validateOperatorStrongPassword,
} = require('../../../middlewares/validateOperatorStrongPassword');
const { database, Operator, PasswordReset } = require('../../../database');
const mailClient = require('../../../utils/mailClient');
const { randomSleep } = require('../../../utils/sleep');
const { logoutCurrentUser } = require('../../../utils/authentication');
const {
  validateParametersSchema,
  validateSchema,
} = require('../../../middlewares/validateSchema');
const {
  requireNonBlockedIp,
} = require('../../../middlewares/requireNonBlockedIp');

const {
  limitRequestsPerHour,
  limitRequestsByEmailPerDay,
} = require('../../../middlewares/rateLimit');
const {
  requireOperator,
  requireNonDeletedUser,
} = require('../../../middlewares/requireUser');

const {
  changePasswordSchema,
  forgotPasswordSchema,
  resetPasswordSchema,
  resetRequestSchema,
} = require('./password.schemas');

const EMAIL_EXPIRY_HOURS = config.get('emails.expiry');

// change password
router.post(
  '/change',
  limitRequestsPerHour('password_change_post_ratelimit_hour', {
    skipSuccessfulRequests: true,
  }),
  requireOperator,
  requireNonDeletedUser,
  validateSchema(changePasswordSchema),
  validateOperatorStrongPassword('newPassword'),
  async (request, response) => {
    const operator = request.user;
    const { currentPassword, newPassword } = request.body;

    const isCurrentPasswordCorrect = await operator.checkPassword(
      currentPassword
    );

    if (!isCurrentPasswordCorrect) {
      return response.sendStatus(status.FORBIDDEN);
    }

    if (currentPassword === newPassword)
      return response.sendStatus(status.CONFLICT);

    await operator.update({
      password: newPassword,
    });

    mailClient.sendOperatorUpdatePasswordNotification(
      operator.email,
      `${operator.fullName}`,
      operator.transactionalLanguage,
      {
        email: operator.email,
      }
    );

    await logoutCurrentUser(request, response);

    return response.sendStatus(status.NO_CONTENT);
  }
);

// password forgot
router.post(
  '/forgot',
  requireNonBlockedIp,
  limitRequestsPerHour('password_forgot_post_ratelimit_hour'),
  validateSchema(forgotPasswordSchema),
  limitRequestsByEmailPerDay('password_forgot_post_ratelimit_email'),
  async (request, response) => {
    const operator = await Operator.findOne({
      where: {
        email: request.body.email,
      },
    });

    if (operator) {
      await PasswordReset.update(
        { closed: true },
        {
          where: {
            operatorId: operator.uuid,
            createdAt: {
              [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours'),
            },
          },
        }
      );

      const forgotPasswordRequest = await PasswordReset.create({
        operatorId: operator.uuid,
        email: operator.email,
      });

      mailClient.sendForgotPasswordMail(
        forgotPasswordRequest.email,
        operator.fullName,
        operator.transactionalLanguage,
        {
          firstName: operator.firstName,
          forgotPasswordLink: `https://${config.get(
            'hostname'
          )}/resetPassword/${forgotPasswordRequest.uuid}`,
        }
      );
    }
    await randomSleep();
    return response.sendStatus(status.NO_CONTENT);
  }
);

// reset password
router.post(
  '/reset',
  requireNonBlockedIp,
  limitRequestsPerHour('password_reset_post_ratelimit_hour', {
    skipSuccessfulRequests: true,
  }),
  validateSchema(resetPasswordSchema),
  validateOperatorStrongPassword('newPassword'),
  async (request, response) => {
    const { resetId, newPassword } = request.body;

    const resetRequest = await PasswordReset.findOne({
      where: {
        uuid: resetId,
        closed: false,
        createdAt: {
          [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours'),
        },
      },
    });
    if (!resetRequest) return response.sendStatus(status.NOT_FOUND);

    const operator = await Operator.findByPk(resetRequest.operatorId);
    if (!operator) return response.sendStatus(status.NOT_FOUND);

    if (await operator.checkPassword(newPassword))
      return response.sendStatus(status.CONFLICT);

    await database.transaction(async transaction => {
      return Promise.all([
        operator.update(
          {
            password: newPassword,
          },
          { transaction }
        ),
        PasswordReset.update(
          { closed: true },
          {
            where: {
              operatorId: operator.uuid,
              createdAt: {
                [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours'),
              },
            },
            transaction,
          }
        ),
      ]);
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

// get password reset
router.get(
  '/reset/:resetId',
  requireNonBlockedIp,
  limitRequestsPerHour('password_reset_get_ratelimit_hour', {
    skipSuccessfulRequests: true,
  }),
  validateParametersSchema(resetRequestSchema),
  async (request, response) => {
    const { resetId } = request.params;

    const resetRequest = await PasswordReset.findOne({
      where: {
        uuid: resetId,
        closed: false,
        createdAt: {
          [Op.gt]: moment().subtract(EMAIL_EXPIRY_HOURS, 'hours'),
        },
      },
    });

    if (!resetRequest) {
      return response.sendStatus(status.NOT_FOUND);
    }

    return response.send({
      uuid: resetRequest.uuid,
      operatorId: resetRequest.operatorId,
      email: resetRequest.email,
      closed: resetRequest.closed,
    });
  }
);

module.exports = router;
