const { z } = require('../../../utils/validation');

const changePasswordSchema = z.object({
  currentPassword: z.string().max(255),
  newPassword: z.zxcvbnPassword(),
});

const forgotPasswordSchema = z.object({
  email: z.email(),
});

const resetPasswordSchema = z.object({
  resetId: z.uuid(),
  newPassword: z.zxcvbnPassword(),
});

const resetRequestSchema = z.object({
  resetId: z.uuid(),
});

module.exports = {
  changePasswordSchema,
  forgotPasswordSchema,
  resetPasswordSchema,
  resetRequestSchema,
};
