import { Router } from 'express';
import status from 'http-status';
import { z } from 'zod';
import crypto from 'crypto';
import config from 'config';
import parsePhoneNumber from 'libphonenumber-js';
import { v4 as uuid } from 'uuid';

import { SMSChallenge } from 'database';
import { get as getFeatureFlag } from 'utils/featureFlag';
import { sendSMSTan as sendMMSMSTan } from 'utils/messagemobile';
import { sendSMSTan as sendSinchTan } from 'utils/sinch';
import { sendSMSTan as sendGTXTan } from 'utils/gtx';
import { isPhoneNumberBlocked } from 'utils/phoneNumber';
import { randomSleep } from 'utils/sleep';

import { requireNonBlockedIp } from 'middlewares/requireNonBlockedIp';
import { validateSchema } from 'middlewares/validateSchema';
import {
  limitRequestsPerMinute,
  limitRequestsPerHour,
  limitRequestsPerDay,
  limitRequestsByPhoneNumberPerDay,
  limitRequestsByFixedLinePhoneNumberPerDay,
} from 'middlewares/rateLimit';
import { requestSchema, verifySchema, bulkVerifySchema } from './sms.schemas';

const router = Router();

let requestCount = 0;

const getProvider = async () => {
  const providerList = [];
  const mmRate = await getFeatureFlag('sms_rate_mm');
  const sinchRate = await getFeatureFlag('sms_rate_sinch');
  const gtxRate = await getFeatureFlag('sms_rate_gtx');

  for (let count = 0; count < mmRate; count += 1) {
    providerList.push('mm');
  }

  for (let count = 0; count < sinchRate; count += 1) {
    providerList.push('sinch');
  }

  for (let count = 0; count < gtxRate; count += 1) {
    providerList.push('gtx');
  }

  requestCount = (requestCount + 1) % providerList.length;
  return providerList[Number(requestCount)];
};

router.post<unknown, unknown, z.infer<typeof requestSchema>>(
  '/request',
  requireNonBlockedIp,
  limitRequestsPerHour('sms_request_post_ratelimit_hour'),
  limitRequestsPerMinute('sms_request_post_ratelimit_minute', {
    skipSuccessfulRequests: false,
    global: true,
  }),
  validateSchema(requestSchema),
  limitRequestsByFixedLinePhoneNumberPerDay(
    'sms_request_post_ratelimit_fixed_phone_number'
  ),
  limitRequestsByPhoneNumberPerDay('sms_request_post_ratelimit_phone_number'),
  async (request, response) => {
    const { phone } = request.body;

    const [isBlocked] = await Promise.all([
      isPhoneNumberBlocked(phone),
      randomSleep(),
    ]);

    if (isBlocked) {
      return response.send({
        challengeId: uuid(),
      });
    }

    const tan = crypto.randomInt(1000000).toString().padStart(6, '0');
    const provider = config.get('skipSmsVerification')
      ? 'debug'
      : await getProvider();
    const phoneNumber = parsePhoneNumber(phone, 'DE');

    if (!phoneNumber) {
      request.log.error(`Could not parse phonenumber [${phone}]`);
      return response.sendStatus(status.BAD_REQUEST);
    }

    const challenge = await SMSChallenge.create({
      tan,
      provider,
    });

    let messageId;
    try {
      switch (provider) {
        case 'debug':
          messageId = 'debug';
          break;
        case 'mm':
          messageId = await sendMMSMSTan(phoneNumber.number, tan);
          break;
        case 'sinch':
          messageId = await sendSinchTan(phoneNumber.number, tan);
          break;
        case 'gtx':
          messageId = await sendGTXTan(phoneNumber.number, tan);
          break;
        default:
          break;
      }
    } catch (error) {
      request.log.error(error as Error, `failed to send sms [${provider}]`);
      return response.sendStatus(status.SERVICE_UNAVAILABLE);
    }

    await challenge.update({
      messageId,
    });

    return response.send({
      challengeId: challenge.uuid,
    });
  }
);

router.post<unknown, unknown, z.infer<typeof verifySchema>>(
  '/verify',
  limitRequestsPerDay('sms_verify_post_ratelimit_day'),
  validateSchema(verifySchema),
  async (request, response) => {
    if (config.get('skipSmsVerification')) {
      await SMSChallenge.update(
        { verified: true },
        {
          where: { uuid: request.body.challengeId },
        }
      );
      return response.sendStatus(status.NO_CONTENT);
    }

    const challenge = await SMSChallenge.findOne({
      where: {
        uuid: request.body.challengeId,
        tan: request.body.tan,
      },
    });

    if (!challenge) {
      return response.sendStatus(status.FORBIDDEN);
    }

    if (challenge.verified) {
      return response.sendStatus(status.NO_CONTENT);
    }

    if (challenge.isExpired) {
      return response.sendStatus(status.GONE);
    }

    await challenge.update({
      verified: true,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

router.post<unknown, unknown, z.infer<typeof bulkVerifySchema>>(
  '/verify/bulk',
  limitRequestsPerDay('sms_verify_bulk_post_ratelimit_day'),
  validateSchema(bulkVerifySchema),
  async (request, response) => {
    if (config.get('skipSmsVerification')) {
      await SMSChallenge.update(
        { verified: true },
        {
          where: { uuid: request.body.challengeIds },
        }
      );
      const challenge = await SMSChallenge.findOne({
        where: {
          uuid: request.body.challengeIds,
        },
      });

      if (!challenge) {
        return response.sendStatus(status.FORBIDDEN);
      }

      return response.send({ challengeId: challenge.uuid });
    }

    const challenge = await SMSChallenge.findOne({
      where: {
        uuid: request.body.challengeIds,
        tan: request.body.tan,
      },
    });

    if (!challenge) {
      return response.sendStatus(status.FORBIDDEN);
    }

    if (challenge.verified) {
      return response.send({ challengeId: challenge.uuid });
    }

    if (challenge.isExpired) {
      return response.sendStatus(status.GONE);
    }

    await challenge.update({
      verified: true,
    });

    return response.send({ challengeId: challenge.uuid });
  }
);

export default router;
