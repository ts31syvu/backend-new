const router = require('express').Router();

const logger = require('../../utils/logger').default;
const { constructSwaggerSpec } = require('../../utils/swagger');

const swaggerSpec = {
  openapi: '3.0.0',
  info: {
    title: 'luca API',
    version: '3.0.0',
    description: '',
  },
  servers: [
    { url: '/api/v3', description: 'This server' },
    { url: 'https://app.luca-app.de/api/v3', description: 'Production' },
  ],
};

(async () => {
  try {
    const { spec, middlewares } = await constructSwaggerSpec(
      './src/routes/{v3,swagger}/**/*.openapi.yaml',
      swaggerSpec
    );

    router.get('/swagger.json', (_request, response) => response.json(spec));
    router.use('/', ...middlewares);
  } catch (error) {
    logger.error(error);
  }
})();

module.exports = router;
