import { ChallengeInstance } from 'database/models/challenge';
import {
  ChallengeType,
  OperatorDeviceCreationChallengeState,
} from 'constants/challenges';

type ChallengeDeviceDTO = {
  uuid: string;
  type: ChallengeType;
  state: OperatorDeviceCreationChallengeState;
};

export const getChallengeDeviceDTO = (
  challenge: ChallengeInstance
): ChallengeDeviceDTO => {
  return {
    uuid: challenge.uuid,
    type: challenge.type,
    state: challenge.state,
  };
};
