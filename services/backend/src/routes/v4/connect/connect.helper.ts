import type { RequestHandler } from 'express';
import status from 'http-status';

export const requireConnectEnabled: RequestHandler<
  unknown,
  unknown,
  unknown
> = (request, response, next) => {
  if (!request.user.HealthDepartment.connectEnabled) {
    return response.sendStatus(status.FORBIDDEN);
  }
  return next();
};
