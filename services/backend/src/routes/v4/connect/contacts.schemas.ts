import { z } from 'utils/validation';

export const createSchema = z.object({
  namePrefix: z.base64({ rawLength: 3 }),
  phonePrefix: z.base64({ rawLength: 3 }),
  authPublicKey: z.ecCompressedPublicKey(),
  departmentId: z.uuid(),
  reference: z.object({
    data: z.base64({ max: 255 }),
    publicKey: z.ecCompressedPublicKey(),
    iv: z.iv(),
    mac: z.mac(),
  }),
  data: z.object({
    data: z.base64({ max: 4096 }),
    publicKey: z.ecCompressedPublicKey(),
    iv: z.iv(),
    mac: z.mac(),
  }),
  signature: z.ecSignature(),
  pow: z.object({
    id: z.uuid(),
    w: z.bigIntegerString(),
  }),
});

export const deleteSchema = z.object({
  contactId: z.uuid(),
  signature: z.ecSignature(),
});
