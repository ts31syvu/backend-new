import { z } from 'utils/validation';

export const createSchema = z.object({
  searchId: z.uuid(),
  contact: z.object({
    data: z.base64({ max: 4096 }),
    iv: z.iv(),
    mac: z.mac(),
    publicKey: z.ecPublicKey(),
  }),
});

export const conversationIdParameterSchema = z.object({
  conversationId: z.uuid(),
});
