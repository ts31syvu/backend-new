import { Router } from 'express';
import z from 'zod';
import moment from 'moment';
import status from 'http-status';

import { validateSchema } from 'middlewares/validateSchema';
import { requireHealthDepartmentEmployee } from 'middlewares/requireUser';
import { limitRequestsPerDay } from 'middlewares/rateLimit';

import { database, ConnectConversation, ConnectMessage } from 'database';

import { logEvent } from 'utils/hdAuditLog';
import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';
import { sendSchema, receiveSchema, readSchema } from './messages.schemas';
import { requireConnectEnabled } from './connect.helper';

const router = Router();

router.post<unknown, unknown, z.infer<typeof sendSchema>>(
  '/send',
  limitRequestsPerDay('sendConnectMessage_per_day'),
  requireHealthDepartmentEmployee,
  requireConnectEnabled,
  validateSchema(sendSchema),
  async (request, response) => {
    const conversation = await ConnectConversation.findOne({
      where: {
        uuid: request.body.conversationId,
        departmentId: request.user.departmentId,
      },
    });
    if (!conversation) {
      return response.sendStatus(status.NOT_FOUND);
    }

    try {
      await database.transaction(async transaction => {
        await ConnectMessage.create(
          {
            conversationId: conversation.uuid,
            messageId: request.body.messageId,
            data: request.body.data,
            iv: request.body.iv,
            mac: request.body.mac,
          },
          { transaction }
        );

        await conversation.update(
          {
            lastContactedAt: moment(),
          },
          { transaction }
        );

        logEvent(request.user, {
          type: AuditLogEvents.CONNECT_MESSAGE_SENT,
          status: AuditStatusType.SUCCESS,
          meta: {
            conversationId: conversation.uuid,
          },
        });
      });
    } catch {
      logEvent(request.user, {
        type: AuditLogEvents.CONNECT_MESSAGE_SENT,
        status: AuditStatusType.ERROR_MESSAGE_SENT_ERROR,
        meta: {
          conversationId: conversation.uuid,
        },
      });
      return response.sendStatus(status.CONFLICT);
    }
    return response.sendStatus(status.NO_CONTENT);
  }
);

router.post<unknown, unknown, z.infer<typeof receiveSchema>>(
  '/receive',
  limitRequestsPerDay('receiveConnectMessage_per_day'),
  validateSchema(receiveSchema),
  async (request, response) => {
    const messages = await ConnectMessage.findAll({
      where: {
        messageId: request.body.messageIds,
        receivedAt: null,
      },
    });

    await ConnectMessage.update(
      {
        receivedAt: moment(),
      },
      {
        where: {
          messageId: request.body.messageIds,
        },
      }
    );

    return response.send(
      messages.map(message => ({
        messageId: message.messageId,
        data: message.data,
        iv: message.iv,
        mac: message.mac,
        createdAt: moment(message.createdAt).unix(),
      }))
    );
  }
);

router.post<unknown, unknown, z.infer<typeof readSchema>>(
  '/read',
  limitRequestsPerDay('readConnectMessage_per_day'),
  validateSchema(readSchema),
  async (request, response) => {
    const message = await ConnectMessage.findOne({
      where: { messageId: request.body.messageId, readAt: null },
    });

    if (!message) {
      return response.sendStatus(status.NOT_FOUND);
    }

    message.update({
      readAt: moment.unix(request.body.timestamp).toDate(),
      readSignature: request.body.signature,
    });

    return response.sendStatus(status.NO_CONTENT);
  }
);

export default router;
