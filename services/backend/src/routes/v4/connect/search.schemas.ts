import { z } from 'utils/validation';
import { PrefixType } from 'constants/prefixType';

export const searchSchema = z.object({
  search: z
    .array(
      z.object({
        prefix: z.base64({ rawLength: 3 }),
        type: z.nativeEnum(PrefixType),
        term: z.object({
          data: z.base64({ max: 255 }),
          iv: z.iv(),
          mac: z.mac(),
          publicKey: z.ecPublicKey(),
        }),
      })
    )
    .min(1)
    .max(100),
});

export const processIdParameterSchema = z.object({
  processId: z.uuid(),
});

export const searchIdParameterSchema = z.object({
  searchId: z.uuid(),
});
