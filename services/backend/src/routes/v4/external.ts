import { Router } from 'express';
import {
  locationGroupIdSchema,
  locationIdSchema,
  operatorIdSchema,
} from 'routes/v4/external.schemas';
import { requireJwtOperator } from 'middlewares/requireUser';
import { validateParametersSchema } from 'middlewares/validateSchema';
import status from 'http-status';
import { pick } from 'lodash';
import { Location, LocationGroup, Operator } from 'database';

const router = Router();

router.get(
  '/operators/:operatorId',
  requireJwtOperator,
  validateParametersSchema(operatorIdSchema),
  async (request, response) => {
    const { user, params } = request;
    const { operatorId } = params;

    if (operatorId !== user.uuid) {
      return response.sendStatus(status.UNAUTHORIZED);
    }

    const operator = await Operator.findByPk(user.uuid, {
      include: [
        {
          model: LocationGroup,
          attributes: ['uuid'],
          required: false,
          include: [
            {
              model: Location,
              attributes: ['uuid'],
              required: false,
            },
          ],
        },
      ],
    });

    if (!operator) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const locationGroups = operator.LocationGroups?.map(locationGroup => ({
      uuid: locationGroup.uuid,
      locations: locationGroup.Locations?.map(location => ({
        uuid: location.uuid,
      })),
    }));

    response.status(status.OK);
    return response.send({
      ...pick(operator, [
        'uuid',
        'firstName',
        'lastName',
        'businessEntityName',
        'businessEntityStreetName',
        'businessEntityStreetNumber',
        'businessEntityZipCode',
        'businessEntityCity',
        'email',
        'username',
      ]),
      locationGroups,
    });
  }
);

router.get(
  '/locationGroups/:locationGroupId',
  requireJwtOperator,
  validateParametersSchema(locationGroupIdSchema),
  async (request, response) => {
    const { user, params } = request;
    const { locationGroupId } = params;

    const locationGroup = await LocationGroup.findOne({
      where: {
        uuid: locationGroupId,
        operatorId: user.uuid,
      },
      include: [
        {
          model: Location,
          attributes: ['uuid', 'name'],
          required: false,
        },
      ],
    });

    if (!locationGroup) {
      return response.sendStatus(status.NOT_FOUND);
    }

    const locations = locationGroup.Locations?.map(location => ({
      uuid: location.uuid,
      name: location.name,
    }));

    response.status(status.OK);
    return response.send({
      ...pick(locationGroup, ['uuid', 'name', 'type']),
      locations,
    });
  }
);

router.get(
  '/locations/:locationId',
  requireJwtOperator,
  validateParametersSchema(locationIdSchema),
  async (request, response) => {
    const { user, params } = request;
    const { locationId } = params;

    const location = await Location.findOne({
      where: {
        uuid: locationId,
        operator: user.uuid,
      },
    });

    if (!location) {
      return response.sendStatus(status.NOT_FOUND);
    }

    response.status(status.OK);
    return response.send({
      ...pick(location, [
        'uuid',
        'name',
        'firstName',
        'lastName',
        'phone',
        'streetName',
        'streetNr',
        'zipCode',
        'city',
        'state',
        'groupId',
      ]),
    });
  }
);

export default router;
