const router = require('express').Router();
const {
  validateParametersSchema,
} = require('../../../middlewares/validateSchema');

const database = require('../../../database');
const { ApiError, ApiErrorType } = require('../../../utils/apiError');

const { issuerIdParametersSchema } = require('./issuers.schemas');

// get all issuers
router.get('/', async (request, response) => {
  const healthDepartments = await database.HealthDepartment.findAll();

  const allIssuers = healthDepartments.map(department => ({
    issuerId: department.uuid,
    publicCertificate: department.publicCertificate,
    signedPublicHDEKP: department.signedPublicHDEKP,
    signedPublicHDSKP: department.signedPublicHDSKP,
  }));
  response.addEtag(allIssuers);
  return response.send(allIssuers);
});

// get single issuer
router.get(
  '/:issuerId',
  validateParametersSchema(issuerIdParametersSchema),
  async (request, response) => {
    const healthDepartment = await database.HealthDepartment.findByPk(
      request.params.issuerId
    );

    if (!healthDepartment) {
      throw new ApiError(ApiErrorType.HEALTH_DEPARTMENT_NOT_FOUND);
    }

    const issuer = {
      uuid: healthDepartment.uuid,
      publicCertificate: healthDepartment.publicCertificate,
      signedPublicHDEKP: healthDepartment.signedPublicHDEKP,
      signedPublicHDSKP: healthDepartment.signedPublicHDSKP,
    };
    response.addEtag(issuer);
    return response.send(issuer);
  }
);

module.exports = router;
