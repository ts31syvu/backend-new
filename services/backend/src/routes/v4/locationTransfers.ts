/* eslint-disable max-lines */
import { Router } from 'express';
import moment from 'moment';
import Sequelize, { Op } from 'sequelize';
import { z } from 'zod';
import config from 'config';
import {
  requireHealthDepartmentEmployee,
  requireOperatorDeviceRole,
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles,
} from 'middlewares/requireUser';

import {
  limitRequestsPerHour,
  limitRequestsByUserPerHour,
} from 'middlewares/rateLimit';
import {
  validateParametersSchema,
  validateQuerySchema,
  validateSchema,
} from 'middlewares/validateSchema';
import {
  database,
  Location,
  LocationTransfer,
  LocationTransferTrace,
  LocationGroup,
  Operator,
  UserTransfer,
  Trace,
  TracingProcess,
  TraceData,
} from 'database';

import { extractAndVerifyLocationTransfer } from 'utils/signedKeys';
import { OperatorDevice } from 'constants/operatorDevice';
import { AuditLogEvents, AuditStatusType } from 'constants/auditLog';
import { logEvent } from 'utils/hdAuditLog';
import { ApiError, ApiErrorType } from 'utils/apiError';
import {
  getSchema,
  createSchema,
  transferIdParametersSchema,
} from './locationTransfers.schemas';

import associatedRouter from './locationTransfers/associated';

const router = Router();

/**
 * Get all location transfers of the currently logged-in operator.
 */
router.get(
  '/',
  limitRequestsPerHour('location_transfers_get_ratelimit_hour'),
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRole(OperatorDevice.manager),
  validateQuerySchema(getSchema),
  async (request, response) => {
    const { completed, deleted } = request.query;
    const user = request.user as Operator;

    const whereClause: {
      contactedAt: { [Op.ne]: null };
      isCompleted?: boolean;
      deletedAt?: { [Op.ne]: null } | null;
    } = {
      contactedAt: {
        [Op.ne]: null,
      },
    };

    if (completed === 'true') {
      whereClause.isCompleted = true;
    } else if (completed === 'false') {
      whereClause.isCompleted = false;
    }

    if (deleted === 'true') {
      whereClause.deletedAt = { [Op.ne]: null };
    } else if (deleted === 'false') {
      whereClause.deletedAt = null;
    }

    const transfers = await LocationTransfer.findAll({
      where: whereClause,
      order: [['createdAt', 'DESC']],
      include: [
        {
          model: Location,
          include: [
            {
              model: LocationGroup,
              attributes: ['name'],
              paranoid: false,
            },
          ],
          where: {
            operator: user.uuid,
          },
          required: true,
          paranoid: false,
        },
      ],
    });

    return response.send(
      transfers.map(transfer => ({
        uuid: transfer.uuid,
        signedLocationTransfer: transfer.signedLocationTransfer,
        groupName: transfer.Location?.LocationGroup?.name,
        locationName: transfer.Location?.name,
        departmentId: transfer.departmentId,
        locationId: transfer.locationId,
        isCompleted: transfer.isCompleted,
        contactedAt: transfer.contactedAt,
        createdAt: moment(transfer.createdAt).unix(),
        deletedAt: transfer.deletedAt && moment(transfer.deletedAt).unix(),
        approvedAt: transfer.approvedAt && moment(transfer.approvedAt).unix(),
      }))
    );
  }
);

/**
 * Get a single location transfer by ID.
 */
router.get(
  '/:transferId',
  limitRequestsPerHour('location_transfer_get_ratelimit_hour'),
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRoles([OperatorDevice.manager]),
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const user = request.user as Operator;

    const transfer = await LocationTransfer.findByPk(
      request.params.transferId,
      {
        include: [
          {
            model: Location,
            include: [
              {
                model: LocationGroup,
                attributes: ['name'],
                paranoid: false,
              },
            ],
            where: { operator: user.uuid },
            required: true,
            paranoid: false,
          },
        ],
      }
    );

    if (!transfer) throw new ApiError(ApiErrorType.LOCATION_TRANSFER_NOT_FOUND);

    return response.send({
      uuid: transfer.uuid,
      signedLocationTransfer: transfer.signedLocationTransfer,
      groupName: transfer.Location?.LocationGroup?.name,
      locationName: transfer.Location?.name,
      departmentId: transfer.departmentId,
      deletedAt: moment(transfer.deletedAt).unix(),
      roomHeight: transfer.roomHeight,
      roomWidth: transfer.roomWidth,
      roomDepth: transfer.roomDepth,
      masks: transfer.masks,
      ventilation: transfer.ventilation,
      entryPolicyInfo: transfer.entryPolicyInfo,
      isIndoor: transfer.isIndoor,
    });
  }
);

router.get(
  '/:transferId/traces',
  requireOperatorOROperatorDevice,
  requireOperatorDeviceRole(OperatorDevice.manager),
  validateParametersSchema(transferIdParametersSchema),
  async (request, response) => {
    const user = request.user as Operator;
    const transfer = await LocationTransfer.findByPk(
      request.params.transferId,
      {
        include: {
          model: Location,
          where: { operator: user.uuid },
          required: true,
          paranoid: false,
        },
      }
    );

    if (!transfer) throw new ApiError(ApiErrorType.LOCATION_TRANSFER_NOT_FOUND);

    const transferTraces = await LocationTransferTrace.findAll({
      where: {
        locationTransferId: transfer?.uuid,
      },
      include: {
        model: Trace,
        required: true,
        include: [
          {
            model: TraceData,
          },
        ],
      },
    });
    return response.send(
      transferTraces.map(({ Trace: trace }) => ({
        traceId: trace?.traceId,
        time: [
          moment(trace?.time[0].value).unix(),
          trace?.time[1] !== null ? moment(trace?.time[1].value).unix() : null,
        ],
        data: trace?.data,
        publicKey: trace?.publicKey,
        iv: trace?.iv,
        mac: trace?.mac,
        isContactDataIncluded: trace?.isContactDataIncluded,
        additionalData: trace?.TraceData
          ? {
              data: trace.TraceData.data,
              publicKey: trace.TraceData.publicKey,
              mac: trace.TraceData.mac,
              iv: trace.TraceData.iv,
            }
          : null,
      }))
    );
  }
);

/**
 * Create a transfer request for venues traced by an infected guest. Preceded
 * by a user transfer of check-in history, this will check for venues an
 * infected guest has checked-in to in order to determine potential contact persons
 * @see https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html#process
 * @see https://www.luca-app.de/securityoverview/processes/tracing_access_to_history.html
 */
router.post<unknown, unknown, z.infer<typeof createSchema>>(
  '/',
  requireHealthDepartmentEmployee,
  limitRequestsByUserPerHour('location_transfer_post_ratelimit_hour'),
  validateSchema(createSchema),
  async (request, response) => {
    const {
      HealthDepartment: healthDepartment,
      departmentId,
    } = request.user as IHealthDepartmentEmployee;
    const { userTransferId, locations } = request.body;

    const isUserTransfer = !!userTransferId;
    let isStatic = false;

    const maxLocations: number = config.get(
      'luca.locationTransfers.maxLocations'
    );

    if (locations.length > maxLocations) {
      logEvent(request.user, {
        type: AuditLogEvents.CREATE_TRACING_PROCESS,
        status: AuditStatusType.ERROR_LIMIT_EXCEEDED,
        meta: {
          viaTan: isUserTransfer,
        },
      });

      throw new ApiError(ApiErrorType.TOO_MANY_LOCATIONS);
    }

    const tracingProcessId = await database
      .transaction(async (transaction: Sequelize.Transaction) => {
        const tracingProcess = await TracingProcess.create(
          {
            departmentId,
            userTransferId,
          },
          { transaction }
        );

        if (isUserTransfer) {
          const userTransfer = await UserTransfer.findByPk(userTransferId, {
            transaction,
          });

          if (!userTransfer) {
            logEvent(request.user, {
              type: AuditLogEvents.CREATE_TRACING_PROCESS,
              status: AuditStatusType.ERROR_INVALID_USER,
              meta: {
                viaTan: isUserTransfer,
              },
            });

            throw new ApiError(ApiErrorType.USER_TRANSFER_NOT_FOUND);
          }

          isStatic = !!userTransfer.tan && !userTransfer.tan.endsWith('1');

          await userTransfer.update(
            {
              departmentId,
              tan: null,
            },
            { transaction }
          );
        }

        let locationData;

        try {
          locationData = locations.map(
            ({ signedLocationTransfer, locationId, time }) => ({
              ...extractAndVerifyLocationTransfer({
                issuer: healthDepartment,
                signedLocationTransfer,
                locationId,
                time,
              }),
              signedLocationTransfer,
            })
          );
        } catch (error) {
          throw new ApiError(
            ApiErrorType.INVALID_SIGNATURE,
            (error as Error).message
          );
        }

        await Promise.all(
          locationData.map(async data => {
            const location = await Location.findByPk(data.locationId, {
              include: {
                required: true,
                model: Operator,
                paranoid: false,
              },
              paranoid: false,
            });

            if (!location) {
              request.log.error({
                message: 'Missing location for location transfer',
                locations,
              });
              logEvent(request.user, {
                type: AuditLogEvents.CREATE_TRACING_PROCESS,
                status: AuditStatusType.ERROR_TARGET_NOT_FOUND,
                meta: {
                  locationId: data?.locationId,
                  viaTan: isUserTransfer,
                  isStatic,
                },
              });
              return null;
            }

            const traces = await Trace.findAll({
              where: {
                locationId: location.uuid,
                time: {
                  [Op.overlap]: [
                    moment.unix(data.time[0]).toDate(),
                    moment.unix(data.time[1]).toDate(),
                  ],
                },
              },
            });

            const locationTransfer = await LocationTransfer.create(
              {
                departmentId,
                tracingProcessId: tracingProcess.uuid,
                locationId: location.uuid,
                time: [
                  moment.unix(data.time[0]).toDate(),
                  moment.unix(data.time[1]).toDate(),
                ],
                signedLocationTransfer: data.signedLocationTransfer,
                initialTracesCount: traces.length,
              },
              { transaction }
            );

            await LocationTransferTrace.bulkCreate(
              traces.map(trace => ({
                locationTransferId: locationTransfer.uuid,
                traceId: trace.traceId,
                time: trace.time,
                deviceType: trace.deviceType,
                isContactDataIncluded: trace.isContactDataIncluded,
              })),
              { transaction }
            );

            logEvent(request.user, {
              type: AuditLogEvents.CREATE_TRACING_PROCESS,
              status: AuditStatusType.SUCCESS,
              meta: {
                transferId: locationTransfer.uuid,
                viaTan: isUserTransfer,
                isStatic,
              },
            });

            return locationTransfer.uuid;
          })
        );
        return tracingProcess.uuid;
      })
      .catch((error: Error) => {
        logEvent(request.user, {
          type: AuditLogEvents.CREATE_TRACING_PROCESS,
          status: AuditStatusType.ERROR_UNKNOWN_SERVER_ERROR,
          meta: {
            viaTan: isUserTransfer,
            isStatic,
          },
        });

        if (error instanceof ApiError) throw error;

        throw new ApiError(ApiErrorType.UNKNOWN_API_ERROR);
      });

    return response.send({ tracingProcessId });
  }
);

router.use('/associated', associatedRouter);

export default router;
