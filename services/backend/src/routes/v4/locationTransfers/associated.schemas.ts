import { z } from 'utils/validation';

export const transferIdParametersSchema = z.object({
  transferId: z.uuid(),
});
