import { Router } from 'express';
import menuRouter from './locations/menu';

const router = Router();

router.use(menuRouter);

export default router;
