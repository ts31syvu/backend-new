import { z } from 'utils/validation';

const translatable = z
  .object({
    de: z.safeString(),
    en: z.safeString().optional(),
  })
  .optional();

export const locationParameterSchema = z.object({
  locationId: z.uuid(),
});

export const updateMenuSchema = z.object({
  categories: z
    .array(
      z.object({
        name: translatable,
        description: translatable,
        items: z
          .array(
            z.object({
              name: translatable,
              dscription: translatable,
              additionalInfo: translatable,
              price: z.number(),
              allergensAndAdditives: z.array(z.safeString()),
            })
          )
          .max(100),
      })
    )
    .max(40),
  allergenesAndAdditives: z.record(translatable),
});
