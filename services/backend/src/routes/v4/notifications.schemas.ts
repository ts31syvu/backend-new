import { z } from 'utils/validation';
import { RiskLevel } from 'constants/riskLevels';

export const chunkIdParametersSchema = z.object({
  chunkId: z.string().max(24),
});

export const hexChunkIdParametersSchema = z.object({
  chunkId: z.hex({ rawLength: 16 }),
});

export const notificationConfigPutSchema = z.object({
  level: z.nativeEnum(RiskLevel),
  showMail: z.boolean().optional(),
  showPhone: z.boolean().optional(),
  infoUrl: z.urlString().optional(),
});
