import { z } from 'utils/validation';

export const requestSchema = z.object({
  type: z.string(),
  userAgent: z.string().max(255).optional(),
});

export const solveSchema = z.object({
  pow: z.object({
    id: z.uuid(),
    w: z.bigIntegerString(),
    duration: z.number().int().optional(),
  }),
});
