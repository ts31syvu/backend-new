import config from 'config';
import { Router } from 'express';
import { z } from 'zod';
import { limitRequestsPerDay } from 'middlewares/rateLimit';
import { validateParametersSchema } from 'middlewares/validateSchema';
import { ApiError, ApiErrorType } from 'utils/apiError';
import { getCachedUrl } from 'utils/urlCache';

import { getRuleParameterSchema } from './rules.schemas';

const router = Router();

const baseSource = config.get<string>('luca.dcc.source');

router.get(
  '/dcc',
  limitRequestsPerDay('rules_dcc_get_ratelimit_day'),
  async (request, response) => {
    const [data, etag] = await getCachedUrl(baseSource);

    response.setHeader('Content-Type', 'application/json');
    if (etag) {
      response.addEtag(data, etag);
    }

    return response.send(data);
  }
);

router.get<z.infer<typeof getRuleParameterSchema>>(
  '/dcc/:hash',
  limitRequestsPerDay('rules_dcc_get_hash_ratelimit_day'),
  validateParametersSchema(getRuleParameterSchema),
  async (request, response) => {
    const { hash } = request.params;
    const url = `${baseSource}/${hash}`;

    const [data, etag] = await getCachedUrl(url);

    if (!data) {
      throw new ApiError(ApiErrorType.NOT_FOUND);
    }

    response.setHeader('Content-Type', 'application/json');
    if (etag) {
      response.addEtag(data, etag);
    }

    return response.send(data);
  }
);

export default router;
