import { Request, Response } from 'express';
import config from 'config';
import * as jose from 'jose';
import * as jwkStore from 'utils/jwkStore';

export async function logoutCurrentUser(
  request: Request,
  response: Response
): Promise<null> {
  const { session, logout } = request;

  return new Promise((resolve, reject) => {
    logout();

    session.destroy(error => {
      if (error) {
        reject(error);
      }
      response.clearCookie(config.get('cookies.name'));
      resolve(null);
    });
  });
}

export const createJWT = async (request: Request): Promise<string> => {
  const { user } = request;
  const keyPair = await jwkStore.getCurrentKeyPair();
  return new jose.SignJWT({ role: user.type })
    .setProtectedHeader({
      alg: 'ES256',
      typ: 'JWT',
      kid: keyPair.keyIdentifier,
    })
    .setSubject(user.uuid)
    .setIssuer('luca-backend')
    .setIssuedAt()
    .setNotBefore('0s')
    .setExpirationTime(config.get<string>('jwt.expiration'))
    .sign(keyPair.privateKey);
};
