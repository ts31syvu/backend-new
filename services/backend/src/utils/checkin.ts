import { Base64 } from 'node-forge';
import { base64ToHex } from '@lucaapp/crypto';

/**
 * Checks whether contact data is included in the data object
 * @param data the data to check
 * @returns true if there is contact data (so not just zeros)
 */
export const isContactDataIncluded = (data: Base64): boolean => {
  return ![...base64ToHex(data)].every(char => char === '0');
};
