import { FeatureFlag } from 'database';

export const DEFAULT_VALUES = {
  sms_rate_mm: 1,
  sms_rate_sinch: 1,
  sms_rate_gtx: 1,
  dummy_max_tracings: 10,
  dummy_max_traces: 20,
  android_minimum_version: 57,
  ios_minimum_version: 24,
  lst_minimum_version: '"v1.0.0"',
  enable_level_4_notifications: false,
  intervalled_mandatory_private_key_upload_enabled: false,
  enable_two_g_plus: false,
  android_minimum_semver: '"2.3.0"',
  ios_minimum_semver: '"2.3.0"',
  operator_android_minimum_semver: '"v1.1.0"',
  operator_ios_minimum_semver: '"v1.1.0"',
};

export type FeatureFlags = keyof typeof DEFAULT_VALUES;

export async function get<T = number | boolean | string>(
  key: FeatureFlags
): Promise<T> {
  const flag = await FeatureFlag.findByPk(key);
  if (!flag) {
    return JSON.parse(
      DEFAULT_VALUES[String(key) as FeatureFlags] as string
    ) as T;
  }

  return JSON.parse(flag.value) as T;
}

export default {
  get,
};
