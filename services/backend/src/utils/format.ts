import { LocationInstance } from 'database/models/location';
import { LocationGroupInstance } from 'database/models/locationGroup';

export const formatLocationName = (
  location: LocationInstance,
  group?: LocationGroupInstance
): string => {
  if (!group) {
    return location.name || '';
  }
  return location.name ? `${group.name} - ${location.name}` : `${group.name}`;
};

export default formatLocationName;
