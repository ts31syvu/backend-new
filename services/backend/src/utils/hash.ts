import crypto from 'crypto';
import { promisify } from 'util';
import config from 'config';

const scrypt = promisify(crypto.scrypt);
const KEY_LENGTH = 64;

export const hashPassword = async (
  password: string,
  salt: string
): Promise<Buffer> => {
  const hash = await scrypt(password, salt, KEY_LENGTH);
  return hash as Buffer;
};

export const pseudoHashPassword = async (): Promise<Buffer> => {
  const hash = await scrypt('pseudo', 'pseudo', KEY_LENGTH);
  return hash as Buffer;
};

export const concatSha256 = (dataArray: string[]): string => {
  let result = '';
  dataArray.forEach(data => {
    const hash = crypto.createHash('sha256');
    hash.update(data);
    result += hash.digest('hex');
  });
  return result;
};

export const hashPhoneNumber = async (phoneNumber: string): Promise<string> => {
  const hash = await scrypt(
    phoneNumber,
    config.get('phoneNumber.salt'),
    KEY_LENGTH
  );
  return (hash as Buffer).toString('base64');
};
