import * as jose from 'jose';
import config from 'config';
import { createHash, createPublicKey } from 'crypto';
import logger from './logger';

const CONFIG_JWK_PRIVATE = 'jwk.privateKey';
const CONFIG_JWK_PUBLIC = 'jwk.publicKey';

const KEY_ALG = 'ES256';

interface KeyPair {
  privateKey: jose.KeyLike;
  publicKey: jose.KeyLike;
  keyIdentifier: string;
}

/**
 * The `kid` will be attached to the public keys which other services pull from us (i.e. for
 * verifying signatures of JWTs). For instance, jose uses the `kid` JWT header field to determine if
 * it already knows that key or needs to refetch the keys via remote JWKS endpoint.
 * @param publicKeyPEM
 * @returns
 */
const deriveKeyIdentifierFromKeyPair = (publicKeyPEM: string): string => {
  const normalizedPublicKeyPEM = publicKeyPEM.replace(/^ */gm, '');
  const publicKeyBytes = createPublicKey(normalizedPublicKeyPEM).export({
    type: 'spki',
    format: 'der',
  });
  return createHash('sha256').update(publicKeyBytes).digest('hex');
};

let currentKeyPair: KeyPair;

const loadKeyPair = async ({
  publicKeyPEM,
  privateKeyPEM,
}: {
  publicKeyPEM: string;
  privateKeyPEM: string;
}): Promise<KeyPair> => {
  if (privateKeyPEM && publicKeyPEM) {
    const privKey = await jose.importPKCS8(privateKeyPEM, KEY_ALG);
    const pubKey = await jose.importSPKI(publicKeyPEM, KEY_ALG);
    const keyIdentifier = deriveKeyIdentifierFromKeyPair(publicKeyPEM);

    logger.info(
      `Loaded configured JWK keys (key identifier is "${keyIdentifier})"`
    );

    return {
      privateKey: privKey,
      publicKey: pubKey,
      keyIdentifier,
    };
  }

  throw new Error(
    'No valid keypair configured, please check jwk.privateKey jwk.publicKey'
  );
};

export const getCurrentKeyPair = async (): Promise<KeyPair> => {
  if (!currentKeyPair) {
    currentKeyPair = await loadKeyPair({
      privateKeyPEM: config.get<string>(CONFIG_JWK_PRIVATE),
      publicKeyPEM: config.get<string>(CONFIG_JWK_PUBLIC),
    });
  }
  return currentKeyPair;
};

export const getPublicKeys = async (): Promise<jose.JSONWebKeySet> => {
  const { publicKey, keyIdentifier } = await getCurrentKeyPair();
  const currentPublicKeyJWK = await jose.exportJWK(publicKey);
  const currentPublicKeyWithKeyId = {
    ...currentPublicKeyJWK,
    kid: keyIdentifier,
  };
  return {
    keys: [currentPublicKeyWithKeyId],
  };
};
