import { Request } from 'express';
import resolveAcceptLanguage from 'resolve-accept-language';

export type InternalLanguages = 'de' | 'en';

export const DEFAULT_LANGUAGE = 'de-DE';
export const AVAILABLE_LANGUAGES = [DEFAULT_LANGUAGE, 'en-US', 'en-GB'];

const parseAcceptLanguageHeader = (header: string): string => {
  return resolveAcceptLanguage(header, AVAILABLE_LANGUAGES, DEFAULT_LANGUAGE);
};

export const getPreferredLanguageFromRequest = (request: Request): string => {
  const acceptLanguageHeader = request.headers['accept-language'];
  if (!acceptLanguageHeader) {
    return DEFAULT_LANGUAGE;
  }
  return parseAcceptLanguageHeader(acceptLanguageHeader);
};
