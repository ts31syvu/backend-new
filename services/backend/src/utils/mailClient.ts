import SibApiV3Sdk from 'sib-api-v3-sdk';
import config from 'config';
import escapeHTML from 'escape-html';
import { proxyAgent } from './proxy';
import { getMailId, getMailTitle, ValidTemplateIds } from './mailClient.helper';
import logger from './logger';
import { InternalLanguages } from './language';

type SendEmailParameters = {
  templateId?: number;
  textContent?: string;
  subject: string;
  toEmail: string;
  toName: string;
  variables?: Record<string, string>;
};

type SendInBlueSuccessResponse = {
  messageId: string;
  messageIds: string[];
};

type SendInBlueErrorResponse = {
  code: string;
  message: string;
};

type SendInBlueResponse =
  | SendInBlueSuccessResponse
  | SendInBlueErrorResponse
  | void;

type SendPlainFunction = (
  content: string,
  subject: string,
  toEmail: string,
  toName: string
) => Promise<SendInBlueResponse>;

type SendTemplateFunction<V extends string | null = null> = (
  toEmail: string,
  toName: string,
  lang: InternalLanguages | null,
  variables: Record<V extends string ? V : string, string>
) => Promise<SendInBlueResponse>;

const defaultClient = SibApiV3Sdk.ApiClient.instance;
defaultClient.requestAgent = proxyAgent;
defaultClient.authentications['api-key'].apiKey = config.get('mailer.apiKey');

const mailClient = new SibApiV3Sdk.TransactionalEmailsApi();

const FROM_HELLO_LUCA = {
  email: config.get('mailer.sender.from'),
  name: config.get('mailer.sender.name'),
};
const MAX_CHARACTERS_ALLOWED_FOR_NAME = 70;

const escapeVariables = (variables: Record<string, string>) => {
  const escapedVariables: typeof variables = {};
  Object.keys(variables).forEach((key: string) => {
    escapedVariables[String(key)] = escapeHTML(variables[String(key)]);
  });

  return escapedVariables;
};

const truncate = (text: string, length: number) => {
  return text.length > length
    ? `${text.slice(0, Math.max(0, length - 3))}...`
    : text;
};

const sendEmail = ({
  templateId,
  textContent = '',
  subject,
  toEmail,
  toName,
  variables,
}: SendEmailParameters): Promise<SendInBlueResponse> => {
  if (!config.get('mailer.apiKey')) {
    logger.warn('email not configured', {
      templateId,
      subject,
      variables,
    });
    return Promise.resolve();
  }

  try {
    const sendSmtpEmail = new SibApiV3Sdk.SendSmtpEmail();

    sendSmtpEmail.subject = subject;
    sendSmtpEmail.sender = FROM_HELLO_LUCA;
    sendSmtpEmail.to = [
      {
        email: toEmail,
        name: truncate(toName, MAX_CHARACTERS_ALLOWED_FOR_NAME),
      },
    ];

    if (templateId) {
      sendSmtpEmail.templateId = templateId;
      sendSmtpEmail.params = escapeVariables(variables || {});
      return mailClient.sendTransacEmail(sendSmtpEmail);
    }

    sendSmtpEmail.textContent = textContent;

    return textContent
      ? mailClient.sendTransacEmail(sendSmtpEmail)
      : Promise.resolve();
  } catch (error) {
    logger.warn('email not sent', {
      error,
      templateId,
      subject,
      variables,
    });
    return Promise.resolve();
  }
};

export const sendPlain: SendPlainFunction = (
  content,
  subject,
  toEmail,
  toName
) =>
  sendEmail({
    textContent: content,
    subject,
    toEmail,
    toName,
  });

const sendTemplate = (key: string): SendTemplateFunction => async (
  toEmail,
  toName,
  lang,
  variables
) =>
  sendEmail({
    templateId: await getMailId(key as ValidTemplateIds, lang),
    subject: getMailTitle(key as ValidTemplateIds, lang),
    toEmail,
    toName,
    variables,
  });

export const sendShareDataRequestNotification: SendTemplateFunction<
  'firstName' | 'departmentName' | 'transferUrl'
> = sendTemplate('shareData');

export const sendRegistrationConfirmation: SendTemplateFunction<'firstName'> = sendTemplate(
  'register'
);

export const sendForgotPasswordMail: SendTemplateFunction<
  'firstName' | 'forgotPasswordLink'
> = sendTemplate('forgotPassword');

export const sendLocationsSupportMail: SendTemplateFunction<
  | 'userPhone'
  | 'userEmail'
  | 'userName'
  | 'requestText'
  | 'requestTime'
  | 'supportCode'
> = sendTemplate('locationsSupport');

export const sendActivationMail: SendTemplateFunction<
  'firstName' | 'activationLink'
> = sendTemplate('activateAccount');

export const sendUpdateEmail: SendTemplateFunction<
  'firstName' | 'activationLink'
> = sendTemplate('updateMail');

export const sendUpdateEmailNotification: SendTemplateFunction<
  'originalEmail' | 'newEmail'
> = sendTemplate('updateMailNotification');

export const sendOperatorUpdatePasswordNotification: SendTemplateFunction<'email'> = sendTemplate(
  'operatorUpdatePasswordNotification'
);
export const sendHdUpdatePasswordNotification: SendTemplateFunction<'email'> = sendTemplate(
  'hdUpdatePasswordNotification'
);

export const sendHdSupportMail: SendTemplateFunction<
  | 'departmentName'
  | 'userPhone'
  | 'userEmail'
  | 'userName'
  | 'requestText'
  | 'requestTime'
> = sendTemplate('hdSupport');

export const sendRegisterAttemptNotification: SendTemplateFunction<'link'> = sendTemplate(
  'registerAttemptNotification'
);

export const sendLocationTransferApprovalNotification: SendTemplateFunction<
  | 'id'
  | 'createdAt'
  | 'updatedAt'
  | 'departmentName'
  | 'timeFrameFrom'
  | 'timeFrameTo'
  | 'locationName'
> = async (toEmail, toName, lang, variables) => {
  const { departmentName } = variables;
  return sendEmail({
    templateId: getMailId('locationTransferApprovalNotification', lang),
    subject: getMailTitle('locationTransferApprovalNotification', lang, {
      departmentName,
    }),
    toEmail,
    toName,
    variables,
  });
};

export const sendHdLocationTransferApprovalNotification: SendTemplateFunction<'assignee'> = sendTemplate(
  'hdLocationTransferApprovalNotification'
);

export const sendDeviceActivatedNotification: SendTemplateFunction<
  'name' | 'deviceCount'
> = sendTemplate('deviceActivatedNotification');

export const sendHdWelcome: SendTemplateFunction<
  'adminFullName' | 'adminEmail'
> = sendTemplate('hdWelcome');
