const sjson = require('secure-json-parse');
const database = require('../../database');

const getMaxLevel = config => {
  const defaultLevels = Object.keys(config.default);
  const departmentLevels = config.departments.flatMap(department =>
    Object.keys(department.config)
  );

  return Math.max(
    ...[...defaultLevels, ...departmentLevels]
      .map(level => Number.parseInt(level, 10))
      .filter(Boolean)
  );
};

const getConfig = (messages, configs) => {
  const config = {};
  messages.forEach(message => {
    if (!config[message.level]) {
      config[message.level] = {};
    }

    if (!config[message.level].messages) {
      config[message.level].messages = {};
    }

    if (!config[message.level].messages[message.language]) {
      config[message.level].messages[message.language] = {};
    }

    config[message.level].messages[message.language][message.key] =
      message.content;
  });

  configs.forEach(nConfig => {
    if (!config[nConfig.level]) {
      config[nConfig.level] = {};
    }

    config[nConfig.level][nConfig.key] = sjson.safeParse(nConfig.value);
  });

  return config;
};

const getNotificationConfig = async () => {
  const healthDepartments = await database.HealthDepartment.findAll({
    attributes: ['uuid', 'name', 'email', 'phone'],
    include: [
      { model: database.NotificationMessage },
      { model: database.NotificationConfig },
    ],
  });

  const defaultMessages = await database.NotificationMessage.findAll({
    where: {
      departmentId: null,
    },
  });

  const defaultConfigs = await database.NotificationConfig.findAll({
    where: {
      departmentId: null,
    },
  });

  const config = {
    default: getConfig(defaultMessages, defaultConfigs),
    departments: healthDepartments.map(department => ({
      uuid: department.uuid,
      name: department.name,
      phone: department.phone,
      email: department.email,
      config: getConfig(
        department.NotificationMessages,
        department.NotificationConfigs
      ),
    })),
  };

  return {
    ...config,
    levels: getMaxLevel(config),
  };
};

module.exports = { getNotificationConfig };
