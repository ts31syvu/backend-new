import config from 'config';
import bigInt, { BigInteger } from 'big-integer';
import crypto from 'isomorphic-webcrypto';

// @ts-ignore wrong ts
const { subtle } = crypto as unknown;

const MODULUS_LENGTH = config.get<number>('pow.modulusLength');
const PUBLIC_EXPONENT = new Uint8Array([0x01, 0x00, 0x01]);
const TWO = bigInt(2);

const base64urlToHex = (encoded: string): string => {
  const base64encoded = encoded
    .replace(/-/g, '+')
    .replace(/_/g, '/')
    .replace(/\s/g, '');
  return Buffer.from(base64encoded, 'base64').toString('hex');
};

export const createChallenge = async (
  t: BigInteger
): Promise<{
  t: string;
  n: string;
  w: string;
}> => {
  const key = await subtle.generateKey(
    {
      name: 'RSA-PSS',
      modulusLength: MODULUS_LENGTH,
      publicExponent: PUBLIC_EXPONENT,
      hash: 'SHA-512',
    },
    true,
    ['sign']
  );

  const privateKey = await subtle.exportKey('jwk', key.privateKey);
  const p = bigInt(base64urlToHex(privateKey.p), 16);
  const q = bigInt(base64urlToHex(privateKey.q), 16);
  const n = bigInt(base64urlToHex(privateKey.n), 16);
  const phi = p.subtract(1).multiply(q.subtract(1));

  const u = TWO.modPow(t, phi);
  const w = TWO.modPow(u, n);

  return { t: t.toString(10), n: n.toString(10), w: w.toString(10) };
};
