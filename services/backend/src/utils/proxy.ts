import axios from 'axios';
import config from 'config';
import HttpsProxyAgent from 'https-proxy-agent';

const httpsProxy = config.get('proxy.https');

export const proxyAgent = httpsProxy
  ? // @ts-ignore HttpsProxyAgent is missing constructor type
    new HttpsProxyAgent(httpsProxy)
  : undefined;

export const proxyClient = axios.create({
  httpsAgent: proxyAgent,
  proxy: false,
});
