import config from 'config';
import { isInternalIp, isRateLimitExemptIp } from './ipChecks';

export const rateLimitSkip = async (
  headers: Record<string, string>,
  ip: string
): Promise<boolean> => {
  const xRateLimitBypassHeader = headers && headers['x-rate-limit-bypass'];
  const allowRateLimitBypass = config.get<boolean>('allowRateLimitBypass');
  return (
    (allowRateLimitBypass && !!xRateLimitBypassHeader) ||
    isInternalIp(ip) ||
    isRateLimitExemptIp(ip)
  );
};
