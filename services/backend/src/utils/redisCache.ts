import etag from 'etag';
import redis from './redis';

const getDataKey = (key: string | Buffer) => `cache:data:${key}`;
const getEtagKey = (key: string | Buffer) => `cache:etag:${key}`;

const cacheEtagStore = new Map();
const cacheDataStore = new Map();

interface GetPayload {
  key: string;
  bufferStore?: boolean;
}

const get = async ({
  key,
  bufferStore = false,
}: GetPayload): Promise<[string | Buffer | null, string | null]> => {
  const localEtag = cacheEtagStore.get(key);
  const remoteEtag = await redis.get(getEtagKey(key));
  if (localEtag === remoteEtag) {
    return [cacheDataStore.get(key), localEtag];
  }

  // If the key is passed as buffer the response will also be a buffer.
  const dataKey = bufferStore ? Buffer.from(getDataKey(key)) : getDataKey(key);
  const remoteValue = await redis.get(dataKey);

  cacheDataStore.set(key, remoteValue);
  cacheEtagStore.set(key, remoteEtag);
  return [remoteValue, remoteEtag];
};

interface SetPayload {
  key: string;
  value: string | Buffer;
  etag?: string;
}

const set = ({ key, value, etag: fixedEtag }: SetPayload): void => {
  const cacheEtag = fixedEtag || etag(value);

  redis.set(getDataKey(key), value);
  cacheDataStore.set(key, value);

  redis.set(getEtagKey(key), cacheEtag);
  cacheEtagStore.set(key, cacheEtag);
};

const cache = { get, set };
export default cache;
