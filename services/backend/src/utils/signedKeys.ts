import forge from 'node-forge';
import jwt from 'jsonwebtoken';
import assert from 'assert';
import {
  verifySignedLocationTransfer,
  verifyCertificateChain,
} from '@lucaapp/crypto';
import { certificateChain } from 'constants/certificateChain';
import { HealthDepartmentInstance } from 'database/models/healthDepartment';
import { z } from './validation';

const signedKeySchema = z
  .object({
    sub: z.uuid(),
    iss: z.string().length(40),
    name: z.string().max(255),
    key: z.ecPublicKey(),
    type: z.enum(['publicHDEKP', 'publicHDSKP']),
    iat: z.unixTimestamp(),
  })
  .strict();

const getFingerprint = (certificate: forge.pki.Certificate) => {
  const certDer = forge.asn1.toDer(forge.pki.certificateToAsn1(certificate));
  const md = forge.md.sha1.create();
  md.update(certDer.data);
  return md.digest().toHex();
};

const verifySignedJwt = ({
  token,
  publicKeyPem,
  issuer,
  subject,
  name,
  type,
  key,
}: {
  token: string;
  publicKeyPem: string;
  issuer: string;
  subject: string;
  name: string;
  type: string;
  key?: string;
}) => {
  const content = jwt.verify(token, publicKeyPem, {
    algorithms: ['RS512'],
    subject,
    issuer,
    maxAge: '10 minutes',
  });

  const validatedContent = signedKeySchema.parse(content);
  assert.equal(validatedContent.iss, issuer, 'Invalid issuer.');
  assert.equal(validatedContent.sub, subject, 'Invalid subject.');
  assert.equal(validatedContent.name, name, 'Invalid name.');
  assert.equal(validatedContent.type, type, 'Invalid type.');
  assert.equal(validatedContent.key, key, 'Invalid key.');
};

export const verifySignedPublicKeys = (
  healthDepartment: HealthDepartmentInstance,
  certificatePem: string,
  signedPublicHDSKP: string,
  signedPublicHDEKP: string
): void => {
  const certificate = forge.pki.certificateFromPem(certificatePem);
  const publicKeyPem = forge.pki.publicKeyToPem(certificate.publicKey);
  const fingerprint = getFingerprint(certificate);
  const commonName = certificate.subject.getField('CN')?.value;

  // verify against D-Trust Chain
  const isChainValid = verifyCertificateChain([
    ...certificateChain,
    certificatePem,
  ]);

  assert.ok(isChainValid, 'Certificate chain is invalid.');

  assert.equal(
    commonName,
    healthDepartment.commonName,
    'Common name mismatch.'
  );

  // verify signedPublicHDSKP
  verifySignedJwt({
    token: signedPublicHDSKP,
    publicKeyPem,
    issuer: fingerprint,
    subject: healthDepartment.uuid,
    name: healthDepartment.name,
    type: 'publicHDSKP',
    key: healthDepartment.publicHDSKP,
  });

  // verify signedPublicHDEKP
  verifySignedJwt({
    token: signedPublicHDEKP,
    publicKeyPem,
    issuer: fingerprint,
    subject: healthDepartment.uuid,
    name: healthDepartment.name,
    type: 'publicHDEKP',
    key: healthDepartment.publicHDEKP,
  });
};

const locationTransferSigningSchema = z
  .object({
    iss: z.uuid(),
    type: z.literal('locationTransfer'),
    locationId: z.uuid(),
    time: z.array(z.unixTimestamp()).length(2),
    iat: z.unixTimestamp(),
  })
  .strict();

export const extractAndVerifyLocationTransfer = ({
  issuer,
  signedLocationTransfer,
  locationId,
  time,
}: {
  issuer: HealthDepartmentInstance;
  signedLocationTransfer: string;
  locationId: string;
  time: number[];
}): {
  iss: string;
  locationId: string;
  type: string;
  time: number[];
} => {
  const content = verifySignedLocationTransfer({
    certificateChain,
    // @ts-ignore undefined commonName and signedPublicHDSKP will fail validation
    issuer,
    signedLocationTransfer,
    isIssuing: true,
  });

  const validatedContent = locationTransferSigningSchema.parse(content);

  assert.equal(validatedContent.iss, issuer.uuid, 'Invalid issuer.');
  assert.equal(validatedContent.locationId, locationId, 'Invalid location.');
  assert.equal(validatedContent.type, 'locationTransfer', 'Invalid type.');
  assert.deepEqual(validatedContent.time, time, 'Invalid timeframe.');

  return validatedContent;
};
