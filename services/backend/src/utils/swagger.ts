import { mergeWith, isArray } from 'lodash';
import fs from 'fs';
import YAML from 'js-yaml';
import swaggerUi from 'swagger-ui-express';
import { promisify } from 'util';
import { RequestHandler } from 'express';
import glob from 'glob';

const asyncGlob = promisify(glob);

export const mergeSpecFragment = (
  fragment: Record<string, unknown>,
  swaggerSpec: Record<string, unknown>
): void => {
  mergeWith(swaggerSpec, fragment, (objectValue, sourceValue) => {
    return isArray(objectValue) ? [...objectValue, ...sourceValue] : undefined;
  });
};

export const constructSwaggerSpec = async (
  documentationPath: string,
  rootSpec: Record<string, unknown>
): Promise<{
  spec: Record<string, unknown>;
  middlewares: RequestHandler[];
}> => {
  const paths = (await asyncGlob(documentationPath, {})) as string[];

  const swaggerSpec = { ...rootSpec };

  paths.forEach(path => {
    // filename is not user controlled
    // eslint-disable-next-line security/detect-non-literal-fs-filename
    const fileContents = fs.readFileSync(path, 'utf8');
    const singleSpec = YAML.load(fileContents) as Record<string, string>;

    mergeSpecFragment(singleSpec, swaggerSpec);
  });

  return {
    spec: swaggerSpec,
    middlewares: [
      ...swaggerUi.serveFiles(swaggerSpec),
      swaggerUi.setup(swaggerSpec),
    ],
  };
};
