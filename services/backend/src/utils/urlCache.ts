import etag from 'etag';
import httpStatus from 'http-status';
import redisCache from 'utils/redisCache';
import { CacheFallback } from 'database';
import { proxyClient } from 'utils/proxy';
import { AxiosResponseHeaders } from 'axios';

export function transformWeakEtag(value?: string): string {
  if (!value) return '';
  if (value.startsWith('W/')) {
    return value.split('W/', 1).pop() as string;
  }
  return value;
}

export async function cacheUrl<R = unknown>(
  url: string,
  validator?: (data: string, headers: AxiosResponseHeaders) => Promise<boolean>
): Promise<R | null> {
  const [, redisEtag] = await redisCache.get({ key: url });

  const { data: remoteData, headers, status } = await proxyClient.get(url, {
    headers: {
      ...(redisEtag && {
        'if-none-match': redisEtag,
      }),
    },
    transformResponse: r => r,
    validateStatus: axiosStatus =>
      (axiosStatus >= 200 && axiosStatus < 300) || axiosStatus === 304,
  });

  if (status === httpStatus.NOT_MODIFIED) {
    return null;
  }

  if (validator) {
    await validator(remoteData, headers);
  }

  const remoteEtag = headers.etag || etag(remoteData, { weak: true });

  if (!!redisEtag && remoteEtag === redisEtag) {
    return remoteData;
  }

  await redisCache.set({ key: url, value: remoteData, etag: remoteEtag });

  await CacheFallback.upsert({
    key: url,
    content: remoteData,
    etag: remoteEtag,
  });

  return remoteData;
}

export async function getCachedUrl(
  url: string
): Promise<[string | null, string | null]> {
  const [redisData, redisEtag] = await redisCache.get({ key: url });

  if (!redisData) {
    const fallback = await CacheFallback.findOne({
      where: {
        key: url,
      },
    });

    if (!fallback) {
      throw new Error('No data present');
    }

    return [fallback.content, fallback.etag];
  }

  return [redisData as string, redisEtag];
}
