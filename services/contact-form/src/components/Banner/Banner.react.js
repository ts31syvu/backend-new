import React from 'react';
import { BannerBox } from './Banner.styled';

export const Banner = ({ title }) => (
  <BannerBox data-cy="banner">{title}</BannerBox>
);
