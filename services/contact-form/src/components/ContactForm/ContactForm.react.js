import React from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';

import { InputForm } from 'components/InputForm';

import { Banner } from 'components/Banner';
import { useDailyKey } from 'components/hooks/queries';
import {
  RegistrationWrapper,
  RegistrationCard,
  Headline,
} from './ContactForm.styled';

export const ContactForm = ({ scanner, formId }) => {
  const intl = useIntl();

  const { isLoading, error: errorDailyKey, data: dailyKey } = useDailyKey();

  if (isLoading) return null;

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'contactForm.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'contactForm.site.meta' })}
        />
      </Helmet>
      <RegistrationWrapper>
        {errorDailyKey || !dailyKey ? (
          <Banner
            title={intl.formatMessage({ id: 'contactForm.banner.noDailyKey' })}
          />
        ) : (
          <RegistrationCard>
            <Headline>{scanner.name}</Headline>
            <InputForm scanner={scanner} formId={formId} dailyKey={dailyKey} />
          </RegistrationCard>
        )}
      </RegistrationWrapper>
    </>
  );
};
