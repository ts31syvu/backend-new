import styled from 'styled-components';

export const LinkWrapper = styled.div`
  padding: 10px;
`;

export const LinkVersion = styled.a`
  color: rgb(255, 255, 255);
  font-size: 14px;
  text-decoration: underline;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 16px;
`;

export const Description = styled.div`
  margin-bottom: 24px;
`;

export const Version = styled.span`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0px;
  line-height: 20px;
`;
