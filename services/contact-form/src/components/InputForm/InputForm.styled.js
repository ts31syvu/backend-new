import styled from 'styled-components';
import { Divider } from 'antd';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 40px;
`;

export const Link = styled.a`
  color: rgb(78, 97, 128);
  &:hover {
    color: rgb(151, 165, 187);
  }
`;

export const DividerStyled = styled(Divider)`
  @media (max-width: 768px) {
    white-space: pre-wrap !important;
  }
`;
