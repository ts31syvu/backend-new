import { useQuery } from 'react-query';
import { getValidatedExtractedDailyKey } from 'utils/dailyKey';

const QUERY_KEYS = {
  DAILY_KEY: 'DAILY_KEY',
};

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedDailyKey, {
    cacheTime: 0,
    retry: 0,
  });
