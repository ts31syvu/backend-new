import validator from 'validator';
import { isValidCharacter } from './checkCharacter';
import { checkPhoneNumber } from './checkPhoneNumber';

export const validateStrings = (_, value) => {
  if (value?.trim() && !isValidCharacter(value)) {
    return Promise.reject();
  }
  if (value?.trim() && validator.isNumeric(value.replace(/\s/g, ''))) {
    return Promise.reject();
  }
  return Promise.resolve();
};

export function validateZipCode(_, value) {
  if (value?.trim() && !validator.isPostalCode(value, 'any')) {
    return Promise.reject();
  }
  return Promise.resolve();
}

export function validatePhoneNumber(_, value) {
  if (value?.trim() && !checkPhoneNumber(value)) {
    return Promise.reject();
  }
  return Promise.resolve();
}

export const validateHouseNo = (_, value) => {
  if (value?.trim() && !isValidCharacter(value)) {
    return Promise.reject();
  }
  return Promise.resolve();
};
