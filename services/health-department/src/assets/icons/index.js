// SVGs
import LucaLogoBlackIconSVG from './LucaLogoBlackIcon.svg';
import ErrorIconSVG from './ErrorIcon.svg';
import ErrorBrowserIconSVG from './ErrorBrowserIcon.svg';

// Icons
export { ReactComponent as BackIcon } from './BackIcon.svg';
export { ReactComponent as BellIcon } from './BellIcon.svg';
export { ReactComponent as BellNotifiedIcon } from './BellNotifiedIcon.svg';
export { ReactComponent as CrossIcon } from './CrossIcon.svg';
export { ReactComponent as DownloadIcon } from './DownloadIcon.svg';
export { ReactComponent as ErrorIcon } from './ErrorIcon.svg';
export { ReactComponent as EditIcon } from './EditIcon.svg';
export { ReactComponent as LockIcon } from './LockIcon.svg';
export { ReactComponent as ExternalIcon } from './ExternalIcon.svg';
export { ReactComponent as PhoneIcon } from './PhoneIcon.svg';
export { ReactComponent as PersonIcon } from './PersonIcon.svg';
export { ReactComponent as ProfileIcon } from './ProfileIcon.svg';
export { ReactComponent as LucaLogoBlackIcon } from './LucaLogoBlackIcon.svg';
export { ReactComponent as ListIcon } from './ListIcon.svg';
export { ReactComponent as ProfileActiveIcon } from './ProfileActiveIcon.svg';
export { ReactComponent as VerificationIcon } from './VerificationIcon.svg';
export { ReactComponent as SimilarDataRequestActiveIcon } from './SimilarDataRequestActiveIcon.svg';
export { ReactComponent as SimilarDataRequestDefaultIcon } from './SimilarDataRequestDefaultIcon.svg';
export { ReactComponent as ErrorBrowserIcon } from './ErrorBrowserIcon.svg';
export { ReactComponent as ContactDefaultIcon } from './ContactDefaultIcon.svg';
export { ReactComponent as ContactActiveIcon } from './ContactActiveIcon.svg';
export { ReactComponent as HelpActiveIcon } from './HelpActiveIcon.svg';
export { ReactComponent as HelpDefaultIcon } from './HelpDefaultIcon.svg';
export { ReactComponent as MenuActiveIcon } from './MenuActiveIcon.svg';
export { ReactComponent as MenuInactiveIcon } from './MenuInactiveIcon.svg';
export { ReactComponent as SearchIcon } from './SearchIcon.svg';
export { ReactComponent as SearchActiveIcon } from './SearchActiveIcon.svg';
export { ReactComponent as SearchDefaultIcon } from './SearchDefaultIcon.svg';
export { ReactComponent as SearchBigIcon } from './SearchBigIcon.svg';
export { ReactComponent as KeycardIcon } from './KeycardIcon.svg';
export { ReactComponent as PenIcon } from './PenIcon.svg';
export { ReactComponent as MobilePhoneIcon } from './MobilePhoneIcon.svg';

export { LucaLogoBlackIconSVG, ErrorIconSVG, ErrorBrowserIconSVG };
