import styled from 'styled-components';
import { Button } from 'antd';

export const ContactWrapper = styled.div`
  padding: 40px 32px 80px 32px;
  background-color: rgb(218, 224, 231);
  position: relative;
  height: auto;
  min-height: 100vh !important;
`;

export const VersionFooterWrapper = styled.div`
  padding-right: 24px;
  float: right;
`;

export const SubMenu = styled.div`
  display: flex;
  justify-content: flex-start;
  margin-bottom: 32px;
`;

export const StyledBackButton = styled(Button)`
  background-color: transparent;
  font-weight: bold;
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  text-transform: uppercase;
  &:hover {
    background-color: transparent;
  }
`;
