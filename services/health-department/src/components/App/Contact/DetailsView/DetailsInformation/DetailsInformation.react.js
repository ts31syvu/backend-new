import React from 'react';
import { useIntl } from 'react-intl';
import { useModal } from 'components/hooks/useModal';
import { CertificateModal } from 'components/App/modals/CertificateModal';
import {
  Wrapper,
  DetailsRow,
  DetailsContainer,
  Headline,
  DetailsText,
  VaccinationCertificateButton,
} from './DetailsInformation.styled';

import { OccupationDetails } from './OccupationDetails';
import { VaccinationCertificateInfo } from '../../VaccinationCertificateInfo';

export const DetailsInformation = ({ entry }) => {
  const intl = useIntl();
  const [openModal] = useModal();

  const openCertificateModal = () => {
    openModal({
      content: <CertificateModal entry={entry} />,
    });
  };

  return (
    <Wrapper>
      <DetailsRow>
        <DetailsContainer>
          <Headline>
            {intl.formatMessage({ id: 'contactDetails.address' })}
          </Headline>
          <DetailsText>{`${entry.address.street} ${entry.address.streetNo}`}</DetailsText>
          <DetailsText>{`${entry.address.zipCode} ${entry.address.city}`}</DetailsText>
        </DetailsContainer>
        <DetailsContainer>
          <Headline>
            {intl.formatMessage({ id: 'contactDetails.contactData' })}
          </Headline>
          <DetailsText>
            {intl.formatMessage(
              { id: 'contactDetails.contactData.phone' },
              { phone: entry.phone }
            )}
          </DetailsText>
          <DetailsText>
            {intl.formatMessage(
              { id: 'contactDetails.contactData.mail' },
              { email: entry.email }
            )}
          </DetailsText>
        </DetailsContainer>
        <DetailsContainer>
          <Headline>
            {intl.formatMessage({ id: 'contactDetails.birthday' })}
          </Headline>
          <DetailsText>{entry.certificate.bd}</DetailsText>
        </DetailsContainer>
        <DetailsContainer>
          <Headline>
            {intl.formatMessage({ id: 'contactDetails.vaccinationStatus' })}
          </Headline>
          <VaccinationCertificateInfo certificate={entry.certificate} />
          <VaccinationCertificateButton onClick={openCertificateModal}>
            {intl.formatMessage({ id: 'contactDetails.certificate' })}
          </VaccinationCertificateButton>
        </DetailsContainer>
      </DetailsRow>
      <DetailsRow>
        <OccupationDetails entry={entry} />
      </DetailsRow>
    </Wrapper>
  );
};
