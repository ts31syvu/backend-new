import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid black;
`;

export const DetailsRow = styled.div`
  margin-bottom: 32px;
  display: flex;
  width: 60%;
`;

export const DetailsContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 60px;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const DetailsText = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
`;

export const VaccinationCertificateButton = styled.a`
  font-weight: bold;
  text-transform: uppercase;
  text-decoration: none;
`;
