import React from 'react';
import { useIntl } from 'react-intl';

export const useOccupationDetails = (fieldOption, value) => {
  const intl = useIntl();
  const getInfoRow = (fieldName, localesValue) => (
    <>
      {intl.formatMessage({
        id: `contactDetails.occupationCard.${fieldName}.${localesValue}`,
      })}
    </>
  );

  const renderDetails = () => {
    switch (fieldOption) {
      case 'CRITICAL_INFRA':
        if (value === true) return getInfoRow('ci', 'true');
        if (value === false) return getInfoRow('ci', 'false');
        return getInfoRow('ci', 'notSpecified');
      case 'VULNERABLE_GROUP':
        if (value === true) return getInfoRow('vg', 'true');
        if (value === false) return getInfoRow('vg', 'false');
        return getInfoRow('vg', 'notSpecified');
      case 'JOB':
        return value ? <>{value}</> : getInfoRow('job', 'notSpecified');
      case 'EMPLOYER':
        return value ? <>{value}</> : getInfoRow('employer', 'notSpecified');
      default:
        return getInfoRow('everywhere', 'notSpecified');
    }
  };
  return { renderDetails };
};
