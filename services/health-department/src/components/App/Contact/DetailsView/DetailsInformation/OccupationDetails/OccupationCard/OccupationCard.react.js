import React from 'react';
import {
  InformationWrapper,
  Information,
  Label,
} from './OccupationCard.styled';
import { useOccupationDetails } from './OccupationCard.helper';

export const OccupationCard = ({ field, label, value }) => {
  const { renderDetails } = useOccupationDetails(field, value);
  return (
    <InformationWrapper>
      <Label>{label}</Label>
      <Information>{renderDetails()}</Information>
    </InformationWrapper>
  );
};
