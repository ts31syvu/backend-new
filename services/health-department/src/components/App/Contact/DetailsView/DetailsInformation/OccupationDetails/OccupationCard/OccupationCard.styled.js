import styled from 'styled-components';

export const Label = styled.div`
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const Information = styled.div`
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
`;

export const InformationWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
