import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 40px 32px;
  background-color: white;
  margin-bottom: 24px;
  min-height: 100vh;
`;
