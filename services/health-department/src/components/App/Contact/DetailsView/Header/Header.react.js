import React from 'react';
import { useIntl } from 'react-intl';

import { useModal } from 'components/hooks/useModal';

import { PrimaryButton } from 'components/general';

import { SendMessageModal } from 'components/App/modals/SendMessageModal';

import { Popconfirm } from 'antd';
import { HeaderComp, Name, NameWrapper } from './Header.styled';

export const Header = ({ entry, setUpdateMessages }) => {
  const intl = useIntl();
  const [openModal, closeModal] = useModal();

  const contact = () => {
    openModal({
      content: (
        <SendMessageModal
          entry={entry}
          isMultipleEntries={false}
          closeModal={closeModal}
          setUpdateMessages={setUpdateMessages}
        />
      ),
    });
  };

  return (
    <HeaderComp>
      <NameWrapper>
        <Name>{`${entry.firstName} ${entry.lastName}`}</Name>
      </NameWrapper>
      <Popconfirm
        placement="bottomRight"
        title={intl.formatMessage({
          id: 'modal.lucaConnect.contacting',
        })}
        okText={intl.formatMessage({
          id: 'modal.lucaConnect.contacting.confirm',
        })}
        cancelText={intl.formatMessage({
          id: 'modal.lucaConnect.contacting.cancel',
        })}
        onConfirm={contact}
      >
        <PrimaryButton>
          {intl.formatMessage({ id: 'contact.contactPerson' })}
        </PrimaryButton>
      </Popconfirm>
    </HeaderComp>
  );
};
