import styled from 'styled-components';

export const Name = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 24px;
  font-weight: bold;
  margin-bottom: 32px;
`;

export const HeaderComp = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const NameWrapper = styled.div`
  display: flex;
`;
