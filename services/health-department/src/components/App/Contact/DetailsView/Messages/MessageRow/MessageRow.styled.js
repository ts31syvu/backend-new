import styled from 'styled-components';

export const MessageRowWrapper = styled.div`
  display: flex;
  flex-direction: column;
  border-bottom: 0.5px solid rgb(0, 0, 0);
  padding: 16px 0;
  &:hover {
    background: rgb(243, 245, 247);
  }
  &:active {
    background: rgb(218, 224, 231);
  }
  pointer-events: painted;
  cursor: pointer;
`;

export const PaddingWrapper = styled.div``;

export const MessageSubject = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  margin-bottom: 10px;
`;
