import React from 'react';
import { MESSAGE_STATUS } from 'constants/messageStatus';
import { TimeStatusRow } from './TimeStatusRow';

export const TimeStatusContainer = ({
  createdTime,
  receivedTime,
  readTime,
  isMessageDelivered,
  isMessageRead,
}) => {
  return (
    <>
      <TimeStatusRow time={createdTime} status={MESSAGE_STATUS.SENT} />
      {isMessageDelivered && (
        <TimeStatusRow time={receivedTime} status={MESSAGE_STATUS.DELIVERED} />
      )}
      {isMessageRead && (
        <TimeStatusRow time={readTime} status={MESSAGE_STATUS.READ} />
      )}
    </>
  );
};
