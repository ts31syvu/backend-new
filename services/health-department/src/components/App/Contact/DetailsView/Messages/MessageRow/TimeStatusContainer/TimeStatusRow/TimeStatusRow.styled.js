import styled from 'styled-components';
import { MESSAGE_STATUS } from 'constants/messageStatus';

const SENT_COLOR = 'rgba(0, 0, 0, 0.5)';
const DELIVERED_COLOR = 'rgb(48, 152, 228)';
const READ_COLOR = 'rgb(129, 158, 87)';

const handleMessageColor = status => {
  switch (status) {
    case MESSAGE_STATUS.SENT:
      return SENT_COLOR;
    case MESSAGE_STATUS.DELIVERED:
      return DELIVERED_COLOR;
    case MESSAGE_STATUS.READ:
      return READ_COLOR;
    default:
      return SENT_COLOR;
  }
};

export const StatusLabel = styled.div`
  color: ${({ $status }) => handleMessageColor($status)};
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const Time = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  padding-right: 24px;
  min-width: 160px;
`;

export const RowContainer = styled.div`
  display: flex;
`;
