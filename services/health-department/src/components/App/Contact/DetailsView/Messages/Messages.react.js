import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import { getConversationMessages } from 'network/api';
import { decryptConversationMessage } from 'utils/reachableContacts';
import { Spin } from 'antd';
import { Wrapper, Title } from './Messages.styled';
import { MessageRow } from './MessageRow';

export const Messages = ({ entry, updateMessages }) => {
  const intl = useIntl();

  const {
    data: encryptedMessages,
    isLoading: isEncryptedMessagesLoading,
    error: isEncryptedMessagesError,
  } = useQuery(['conversationMessages', entry.uuid, { updateMessages }], () =>
    getConversationMessages(entry.uuid)
  );

  const {
    data: messages,
    isLoading: isMessagesLoading,
    error: isMessagesError,
  } = useQuery(
    ['decryptedMessages'],
    async () => {
      const results = await Promise.all(
        encryptedMessages.map(async message => {
          try {
            const decryptedData = await decryptConversationMessage({
              messageId: message.messageId,
              data: message.data,
              iv: message.iv,
              mac: message.mac,
              mekp: entry.mekp,
            });
            return {
              decryptedData,
              messageId: message.messageId,
              createdAt: message.createdAt,
              receivedAt: message.receivedAt,
              readAt: message.readAt,
            };
          } catch {
            return null;
          }
        })
      );
      return results.filter(result => !!result.decryptedData);
    },
    {
      enabled: !!encryptedMessages,
    }
  );

  if (isEncryptedMessagesLoading || isMessagesLoading) return <Spin />;
  if (isEncryptedMessagesError || isMessagesError) return null;

  return (
    <Wrapper>
      {messages?.length > 0 ? (
        <>
          <Title>
            {intl.formatMessage({ id: 'contactDetails.messages.title' })}
          </Title>
          {messages?.map(message => (
            <MessageRow message={message} key={message.messageId} />
          ))}
        </>
      ) : (
        <Title>
          {intl.formatMessage({
            id: 'contactDetails.messages.title.emptyMessage',
          })}
        </Title>
      )}
    </Wrapper>
  );
};
