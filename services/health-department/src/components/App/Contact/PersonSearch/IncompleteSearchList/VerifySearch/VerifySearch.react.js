import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { CONTACT_ROUTE } from 'constants/routes';
import { StyledVerifyButton } from './VerifySearch.styled';

export const VerifySearch = ({
  searchProcessId,
  verifiedCount,
  totalCount,
}) => {
  const intl = useIntl();
  const history = useHistory();

  const onClick = () => history.push(`${CONTACT_ROUTE}/${searchProcessId}`);
  const isCompleted = verifiedCount === totalCount;

  return isCompleted ? (
    <StyledVerifyButton disabled>
      {intl.formatMessage({
        id: 'contact.incompleteSearchList.button.completed',
      })}
    </StyledVerifyButton>
  ) : (
    <StyledVerifyButton onClick={onClick}>
      {intl.formatMessage({ id: 'contact.incompleteSearchList.button' })}
    </StyledVerifyButton>
  );
};
