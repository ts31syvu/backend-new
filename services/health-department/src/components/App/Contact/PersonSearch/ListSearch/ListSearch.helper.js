import * as XLSX from 'xlsx';
import { convertToJson } from 'utils/formatData/convertToJson';

export const ERROR_TYPE = {
  INVALID_ATTRIBUTE: 'INVALID_ATTRIBUTE',
  ROW_LIMIT: 'ROW_LIMIT',
  EMPTY_DATA: 'EMPTY_DATA',
};

export const ErrorMessages = {
  INVALID_ATTRIBUTE: 'contact.listSearch.uploadFileMessage.invalidFormat',
  ROW_LIMIT: 'contact.listSearch.uploadFileMessage.rowLimit',
  EMPTY_DATA: 'contact.listSearch.uploadFileMessage.emptyData',
  GENERAL: 'contact.listSearch.uploadFileMessage.error',
};

export const getTrimmedData = data =>
  data
    .filter(
      userData => (userData.firstName && userData.lastName) || userData.phone
    )
    .map(filteredUserData => {
      if (!filteredUserData.phone) {
        const trimmedFirstName = filteredUserData.firstName?.trim();
        const trimmedLastName = filteredUserData.lastName?.trim();
        return {
          firstName: trimmedFirstName,
          lastName: trimmedLastName,
        };
      }
      const trimmedPhone = filteredUserData.phone?.trim();
      return {
        phone: trimmedPhone,
      };
    });

export const hasValidProperties = userData =>
  // eslint-disable-next-line no-prototype-builtins
  userData[0]?.hasOwnProperty('phone') ||
  // eslint-disable-next-line no-prototype-builtins
  (userData[0]?.hasOwnProperty('firstName') &&
    // eslint-disable-next-line no-prototype-builtins
    userData[0]?.hasOwnProperty('lastName'));

export const parseFile = file => {
  return new Promise((resolve, reject) => {
    if (!file) reject();
    const reader = new FileReader();
    reader.addEventListener('load', event => {
      /* Parse data */
      const bstr = event.target.result;
      const workBook = XLSX.read(bstr, { type: 'binary' });
      /* Get first worksheet */
      const workSheetName = workBook.SheetNames[0];
      const workSheet = workBook.Sheets[workSheetName];
      /* Convert array of arrays */
      const data = XLSX.utils.sheet_to_csv(workSheet, { header: 1 });
      /* Convert data to json */
      const userData = convertToJson(data);
      /* Check file format */
      if (userData.length <= 1) {
        reject(ERROR_TYPE.EMPTY_DATA);
        return;
      }
      if (userData.length > 101) {
        reject(ERROR_TYPE.ROW_LIMIT);
        return;
      }
      if (hasValidProperties(userData)) {
        resolve(getTrimmedData(userData));
      } else {
        reject(ERROR_TYPE.INVALID_ATTRIBUTE);
      }
    });
    reader.readAsBinaryString(file);
  });
};

export const downloadTemplate = type => {
  const workbook = XLSX.utils.book_new();
  const data = type === 'name' ? [['firstName', 'lastName']] : [['phone']];

  const worksheet = XLSX.utils.aoa_to_sheet(data);
  XLSX.utils.book_append_sheet(workbook, worksheet, '1');
  XLSX.writeFile(workbook, 'template.xlsx');
};

export const handleErrorMessages = (error, setUploadMessageId) => {
  switch (error) {
    case ERROR_TYPE.INVALID_ATTRIBUTE:
      setUploadMessageId(ErrorMessages.INVALID_ATTRIBUTE);
      break;
    case ERROR_TYPE.ROW_LIMIT:
      setUploadMessageId(ErrorMessages.ROW_LIMIT);
      break;
    case ERROR_TYPE.EMPTY_DATA:
      setUploadMessageId(ErrorMessages.EMPTY_DATA);
      break;
    default:
      setUploadMessageId(ErrorMessages.GENERAL);
  }
};
