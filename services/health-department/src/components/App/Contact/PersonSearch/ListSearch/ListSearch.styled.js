import styled from 'styled-components';
import { Button } from 'antd';

export const Wrapper = styled.div`
  width: 60%;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const TemplateWrapper = styled.div`
  display: flex;
  margin: 16px 0;
`;

export const SearchPersonTemplate = styled.div``;

export const SearchPhoneNumberTemplate = styled.div``;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 48px 0;
`;

export const StyledButtonLink = styled(Button)`
  color: rgb(80, 102, 124);
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 0px;
`;
