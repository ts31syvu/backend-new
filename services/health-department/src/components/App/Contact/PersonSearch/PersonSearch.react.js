import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { ListIcon, PhoneIcon, PersonIcon } from 'assets/icons';

import { ListSearch } from './ListSearch';
import { SinglePersonSearch } from './SinglePersonSearch';
import { PhoneSearch } from './PhoneSearch';

import {
  Wrapper,
  Headline,
  Description,
  iconStyles,
  OptionWrapper,
  Option,
  OptionName,
  OptionArea,
} from './PersonSearch.styled';

import { LIST, PERSON, PHONE } from './PersonSearch.helper';
import { IncompleteSearchList } from './IncompleteSearchList';

export const PersonSearch = ({ publicHDEKP }) => {
  const intl = useIntl();
  const [activeOption, setActiveOption] = useState(LIST);

  const options = [
    {
      name: LIST,
      intl: 'contact.personSearch.option.list',
      icon: <Icon style={iconStyles} component={ListIcon} />,
      action: () => setActiveOption(LIST),
    },
    {
      name: PERSON,
      intl: 'contact.personSearch.option.person',
      icon: <Icon style={iconStyles} component={PersonIcon} />,
      action: () => setActiveOption(PERSON),
    },
    {
      name: PHONE,
      intl: 'contact.personSearch.option.phone',
      icon: <Icon style={iconStyles} component={PhoneIcon} />,
      action: () => setActiveOption(PHONE),
    },
  ];

  if (!publicHDEKP) return null;

  const renderActiveSearchOption = () => {
    switch (activeOption) {
      case LIST:
        return <ListSearch publicHDEKP={publicHDEKP} />;
      case PERSON:
        return <SinglePersonSearch publicHDEKP={publicHDEKP} />;
      case PHONE:
        return <PhoneSearch publicHDEKP={publicHDEKP} />;
      default:
        return null;
    }
  };

  return (
    <>
      <Wrapper>
        <Headline>
          {intl.formatMessage({ id: 'contact.personSearch' })}
        </Headline>
        <Description>
          {intl.formatMessage({ id: 'contact.personSearch.description' })}
        </Description>
        <OptionArea>
          {options.map(option => (
            <OptionWrapper key={option.name}>
              <Option
                onClick={option.action}
                active={activeOption === option.name}
              >
                {option.icon}
                <OptionName>
                  {intl.formatMessage(
                    {
                      id: option.intl,
                    },
                    { br: <br /> }
                  )}
                </OptionName>
              </Option>
            </OptionWrapper>
          ))}
        </OptionArea>
        {renderActiveSearchOption()}
      </Wrapper>
      <Wrapper>
        <IncompleteSearchList />
      </Wrapper>
    </>
  );
};
