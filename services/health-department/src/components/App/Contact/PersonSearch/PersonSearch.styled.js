import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 40px 32px;
  background-color: white;
  margin-bottom: 24px;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 32px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const Option = styled.div`
  display: flex;
  flex-direction: column;
  background: ${({ active }) => (active ? 'white' : 'rgb(195, 206, 217)')};
  border: ${({ active }) => (active ? '2px solid rgb(80, 102, 124)' : '')};
  border-radius: 4px;
  box-shadow: 0 2px 4px 0 rgba(143, 143, 143, 0.5);
  height: 132px;
  width: 180px;
  cursor: pointer;
`;

export const OptionWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 48px 48px 0;
`;

export const OptionName = styled.div`
  margin: 0 auto;
  text-align: center;
  width: 85%;
  color: rgba(0, 0, 0, 0.87);
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
  margin-bottom: 16px;
`;

export const OptionArea = styled.div`
  display: flex;
`;

export const iconStyles = { margin: '19px 0 16px 0', fontSize: 40 };
