import styled from 'styled-components';
import { Input } from 'antd';

export const Wrapper = styled.div`
  width: 60%;
`;

export const Headline = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const StyledInput = styled(Input)`
  width: 100%;
  border-top: none;
  border-left: none;
  border-right: none;
  border-color: black;
  border-radius: 0;
  margin-right: 48px;
  &:focus,
  &:active {
    width: 100%;
    border-top: none;
    border-left: none;
    border-right: none;
    border-color: black;
    border-radius: 0;
    margin-right: 48px;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 48px 0;
`;
