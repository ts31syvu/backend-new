import React from 'react';
import { useIntl } from 'react-intl';
import { Wrapper, Text } from './VaccinationCertificateInfo.styled';

export const VaccinationCertificateInfo = ({ certificate }) => {
  const intl = useIntl();

  if (!certificate) return null;

  let vaccinationStatusId;

  if (certificate.vc)
    vaccinationStatusId = 'contact.vaccinationStatus.vaccinated';

  if (certificate.r)
    vaccinationStatusId = 'contact.vaccinationStatus.recovered';

  if (certificate.vc && certificate.r)
    vaccinationStatusId = 'contact.vaccinationStatus.vaccinatedAndRecovered';

  return (
    <Wrapper>
      <Text>
        {intl.formatMessage(
          { id: vaccinationStatusId },
          {
            currentDose: certificate.vc?.cd,
            totalDoses: certificate.vc?.td,
          }
        )}
      </Text>
      <Text>{certificate.vc?.d || certificate.r?.d}</Text>
    </Wrapper>
  );
};
