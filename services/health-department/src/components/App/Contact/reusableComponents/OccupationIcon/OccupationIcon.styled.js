import styled from 'styled-components';
import { ReactComponent as VulnerableGroupIcon } from 'assets/vg.svg';
import { ReactComponent as CriticalInfraIcon } from 'assets/ci.svg';

export const StyledVulnerableGroupIcon = styled(VulnerableGroupIcon)`
  height: 20px;
  width: 20px;
  font-weight: bold;
`;

export const StyledCriticalInfraIcon = styled(CriticalInfraIcon)`
  height: 20px;
  width: 20px;
  font-weight: bold;
`;
