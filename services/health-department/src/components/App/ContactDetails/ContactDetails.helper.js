import { hexToBytes, decodeUtf8 } from '@lucaapp/crypto';
import { decryptWithHDEKP } from 'utils/cryptoKeyOperations';

export const getDecryptedSearches = searches => {
  const decryptedSearches = searches.map(
    ({ uuid, term, type, done, available }) => {
      try {
        const decryptedSearchTerm = decodeUtf8(
          hexToBytes(
            decryptWithHDEKP(term.publicKey, term.iv, term.mac, term.data)
          )
        );
        return {
          uuid,
          type,
          done,
          available,
          searchTerm: decryptedSearchTerm,
        };
      } catch (error) {
        console.error(error);
        return null;
      }
    }
  );

  return decryptedSearches
    .filter(decryptionResult => !!decryptionResult)
    .sort((a, b) => a.searchTerm.localeCompare(b.searchTerm));
};
