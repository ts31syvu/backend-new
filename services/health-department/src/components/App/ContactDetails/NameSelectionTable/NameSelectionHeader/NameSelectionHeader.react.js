import React from 'react';
import { useIntl } from 'react-intl';
import {
  Wrapper,
  SearchIndex,
  NameWrapper,
  Name,
  Results,
} from './NameSelectionHeader.styled';

export const NameSelectionHeader = ({
  currentSearchCount,
  potentialFoundCount,
  potentialUserCount,
  searchInfo,
}) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <SearchIndex>
        {intl.formatMessage(
          { id: 'nameSelection.index' },
          { current: currentSearchCount, total: potentialFoundCount }
        )}
      </SearchIndex>
      <NameWrapper>
        <Name>{searchInfo?.searchTerm}</Name>
        <Results>
          {intl.formatMessage(
            { id: 'nameSelection.results' },
            { count: potentialUserCount }
          )}
        </Results>
      </NameWrapper>
    </Wrapper>
  );
};
