import React from 'react';
import { useIntl } from 'react-intl';
import { useQueries, useQuery } from 'react-query';
import { filterAndDecryptSearchResults } from 'utils/reachableContacts';
import { getSearchById } from 'network/api';
import { SearchBigIcon } from 'assets/icons';
import { validateAndParseCertData, getEUDGCIssuers } from 'utils/eudgc';
import { getDecryptedSearches } from '../ContactDetails.helper';
import { StyledText } from './NameSelectionTable.styled';
import { VaccinationCertificateInfo } from '../../Contact/VaccinationCertificateInfo';

const getCertificateData = async ({
  certificates,
  issuers,
  firstName,
  lastName,
}) => {
  const validatedCertificates = await Promise.all(
    certificates.map(certificate =>
      validateAndParseCertData({
        certificate,
        issuers,
        firstName,
        lastName,
      }).catch(error => {
        console.error(error);
        return null;
      })
    )
  ).then(results => results.filter(result => !!result));

  if (validatedCertificates.length === 0) return null;

  const certificateData = { v: 1 };

  validatedCertificates.forEach(validatedCertificate => {
    certificateData.bd = validatedCertificate.bd;
    certificateData.vc ??= validatedCertificate.vc;
    certificateData.r ??= validatedCertificate.r;
  });

  return certificateData;
};

export const useSearchPossibleUsers = searches => {
  const { data: issuers } = useQuery('eudgcIssuers', getEUDGCIssuers);

  const { data: decryptedSearches } = useQuery(['decryptedSearches'], () =>
    getDecryptedSearches(searches)
  );

  const searchQueries = decryptedSearches?.map(search => ({
    queryKey: ['search', { userId: search.uuid }],
    queryFn: () =>
      getSearchById(search.uuid).then(async results => {
        const decryptedResults = filterAndDecryptSearchResults(
          results,
          search?.searchTerm,
          search?.type
        );

        const resultsWithCertificate = await Promise.all(
          decryptedResults.map(async decryptedResult => ({
            ...decryptedResult,
            vct: await getCertificateData({
              certificates: decryptedResult.ct,
              issuers,
              firstName: decryptedResult.cd.fn,
              lastName: decryptedResult.cd.ln,
            }),
          }))
        );

        return resultsWithCertificate.filter(
          resultWithCertificate => !!resultWithCertificate.vct
        );
      }),
    enabled: !!decryptedSearches && !!issuers,
  }));

  const searchesWithPossibleUsers = useQueries(searchQueries || []);

  const isLoading =
    !searchQueries ||
    searchesWithPossibleUsers.some(result => result.isLoading || result.isIdle);

  return {
    searchesWithPossibleUsers: searchesWithPossibleUsers.map(
      searchWithPossibleUsers => searchWithPossibleUsers.data
    ),
    decryptedSearches,
    isLoading,
  };
};

export const useNameSelectionTableColumns = () => {
  const intl = useIntl();

  const columns = [
    {
      title: intl.formatMessage({ id: 'contact.table.header.firstName' }),
      key: 'firstName',
      render: function renderLastName({ firstName }) {
        return <>{firstName}</>;
      },
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.lastName' }),
      key: 'lastName',
      render: function renderFirstName({ lastName }) {
        return <>{lastName}</>;
      },
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.address' }),
      key: 'address',
      render: function renderAddress({
        streetName,
        streetNumber,
        postCode,
        city,
      }) {
        return (
          <>
            <div>{`${streetName} ${streetNumber},`}</div>
            <div>{`${postCode} ${city}`}</div>
          </>
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.phone' }),
      key: 'phone',
      render: function renderPhoneNumber({ phone }) {
        return <>{phone}</>;
      },
    },
    {
      title: intl.formatMessage({ id: 'contact.table.header.birthdate' }),
      key: 'birthdate',
      render: function renderBirthdate({ birthdate }) {
        return <>{birthdate}</>;
      },
    },
    {
      title: intl.formatMessage({
        id: 'contact.table.header.vaccinationStatus',
      }),
      key: 'vaccinationStatus',
      render: function renderVaccinationStatus({ certificate }) {
        return <VaccinationCertificateInfo certificate={certificate} />;
      },
    },
  ];

  return { columns };
};

export const getEmptyText = intl => (
  <>
    <StyledText>
      {intl.formatMessage({
        id: 'contact.nameSelectionTable.notSureNoMatches',
      })}
    </StyledText>
    <SearchBigIcon />
  </>
);
