import styled from 'styled-components';

export const ButtonWrapper = styled.div`
  margin-top: 40px;
  display: flex;
  justify-content: flex-end;
`;

export const StyledText = styled.div`
  padding-bottom: 24px;
`;
