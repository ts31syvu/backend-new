import React from 'react';

import { useIntl } from 'react-intl';
import { useHistory, useLocation } from 'react-router';
import Icon from '@ant-design/icons';

// CONSTANTS
import { HELP_CENTER_ROUTE } from 'constants/routes';

import { HelpActiveIcon, HelpDefaultIcon } from 'assets/icons';

import { HelpCenterComp } from './HelpCenter.styled';

const HelpCenterIcon = isActive => (
  <Icon
    data-cy="helpCenterIcon"
    component={isActive ? HelpActiveIcon : HelpDefaultIcon}
    style={{ fontSize: 32 }}
  />
);

export const HelpCenter = () => {
  const intl = useIntl();
  const history = useHistory();
  const currentRoute = useLocation();

  const isHelpCenterRoute = currentRoute.pathname === HELP_CENTER_ROUTE;

  const handleClick = () => history.push(HELP_CENTER_ROUTE);

  return (
    <HelpCenterComp
      onClick={handleClick}
      title={intl.formatMessage({ id: 'helpCenter.title' })}
    >
      {HelpCenterIcon(isHelpCenterRoute)}
    </HelpCenterComp>
  );
};
