import styled from 'styled-components';

export const IconWrapper = styled.div`
  margin-right: 24px;
  cursor: pointer;
`;
export const badgeStyle = {
  top: '2px',
  right: '20px',
  width: '10px',
  height: '10px',
  backgroundColor: 'rgb(253, 172, 114)',
};
