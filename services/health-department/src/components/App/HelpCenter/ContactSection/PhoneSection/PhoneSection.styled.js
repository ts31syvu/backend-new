import styled from 'styled-components';
import { PhoneOutlined } from '@ant-design/icons';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
`;

export const StyledPhoneOutlined = styled(PhoneOutlined)`
  font-size: 32px;
  margin-bottom: 20px;
`;
