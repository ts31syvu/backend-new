import styled from 'styled-components';
import { SecondaryButton } from 'components/general';

export const StyledButton = styled(SecondaryButton)`
  margin-right: 20px;
`;
