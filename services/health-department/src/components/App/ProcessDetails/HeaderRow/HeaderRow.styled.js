import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 32px 0 40px 0;
`;

export const ProcessName = styled.div`
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 34px;
  font-weight: 600;
`;

export const ButtonRow = styled.div`
  display: flex;
`;
