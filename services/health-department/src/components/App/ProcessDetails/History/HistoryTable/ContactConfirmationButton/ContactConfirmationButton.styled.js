import styled from 'styled-components';

export const Expiry = styled.div`
  margin-top: 4px;
  margin-left: 12px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
`;

export const ButtonWrapper = styled.div`
  text-align: left;
  display: inline-block;
`;

export const Wrapper = styled.div`
  display: flex;
`;
