import React from 'react';
import { useIntl } from 'react-intl';
import { Tooltip } from 'antd';
import { useQuery } from 'react-query';

import { getAssociatedDataRequestAvailability } from 'network/api';

import { useModal } from 'components/hooks/useModal';
import { SimilarDataRequestModal } from 'components/App/modals/SimilarDataRequestModal';

import {
  ButtonWrapper,
  ActiveIcon,
  DefaultIcon,
} from './SimilarDataRequestButton.styled';

export const SimilarDataRequestButton = ({ location }) => {
  const intl = useIntl();
  const [openModal, closeModal] = useModal();

  const { transferId } = location;

  const { data: associatedDataRequestAvailability, refetch } = useQuery(
    ['associatedDataRequestAvailability', transferId],
    () => getAssociatedDataRequestAvailability(transferId)
  );

  const openSimilarDataRequestsModal = () =>
    openModal({
      closable: false,
      title: intl.formatMessage({
        id: 'modal.similarDataRequest.title',
      }),
      content: (
        <SimilarDataRequestModal
          location={location}
          closeModal={() => {
            closeModal();
            refetch();
          }}
        />
      ),
    });

  if (
    !associatedDataRequestAvailability ||
    associatedDataRequestAvailability.available === 0
  )
    return null;

  return (
    <ButtonWrapper onClick={openSimilarDataRequestsModal}>
      <Tooltip title={intl.formatMessage({ id: 'similarDataRequest.tooltip' })}>
        {associatedDataRequestAvailability.unread === 0 ? (
          <DefaultIcon />
        ) : (
          <ActiveIcon />
        )}
      </Tooltip>
    </ButtonWrapper>
  );
};
