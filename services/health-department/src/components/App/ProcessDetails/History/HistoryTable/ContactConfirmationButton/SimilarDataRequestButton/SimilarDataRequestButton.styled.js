import styled from 'styled-components';
import Icon from '@ant-design/icons';

import {
  SimilarDataRequestActiveIcon,
  SimilarDataRequestDefaultIcon,
} from 'assets/icons';

export const ButtonWrapper = styled.div`
  text-align: left;
`;

export const ActiveIcon = styled(Icon).attrs({
  component: SimilarDataRequestActiveIcon,
})`
  font-size: 32px;
  margin-left: 32px;
  cursor: pointer;
`;

export const DefaultIcon = styled(Icon).attrs({
  component: SimilarDataRequestDefaultIcon,
})`
  font-size: 32px;
  margin-left: 32px;
  cursor: pointer;
`;
