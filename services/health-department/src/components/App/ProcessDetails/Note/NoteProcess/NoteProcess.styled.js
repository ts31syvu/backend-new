import styled from 'styled-components';

export const NoteWrapper = styled.div`
  margin-top: 32px;
  width: 50%;
`;

export const NoteHeader = styled.div`
  display: flex;
  flex-direction: row;
  margin-bottom: 16px;
`;

export const Title = styled.div`
  font-size: 16px;
  font-weight: bold;
`;

export const TextNote = styled.p`
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  word-break: break-all;
`;
