import React from 'react';
import { useIntl } from 'react-intl';
import XLSX from 'xlsx';

import { sanitizeForCSV } from 'utils/sanitizer';

import { SecondaryButton } from 'components/general';

const columnIds = ['profile.auditLog.email', 'profile.auditLog.uuid'];

export const EmailToUuidListDownload = ({ employees }) => {
  const intl = useIntl();

  const download = () => {
    const columns = columnIds.map(id => intl.formatMessage({ id }));
    const worksheet = XLSX.utils.aoa_to_sheet([
      columns,
      ...employees.map(employee => [
        sanitizeForCSV(employee.email),
        sanitizeForCSV(employee.uuid),
      ]),
    ]);
    const workbook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(
      workbook,
      worksheet,
      intl.formatMessage({ id: 'profile.auditLog.downloadFileName' })
    );
    XLSX.writeFile(
      workbook,
      `${intl.formatMessage({ id: 'profile.auditLog.downloadFileName' })}.xlsx`
    );
  };

  return (
    <SecondaryButton onClick={download}>
      {intl.formatMessage({
        id: 'profile.auditLogs.downloadUserList',
      })}
    </SecondaryButton>
  );
};
