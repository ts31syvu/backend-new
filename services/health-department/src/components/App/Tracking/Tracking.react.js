import React, { useEffect } from 'react';

import { getDailyPrivateKey } from 'utils/cryptoKeyOperations';
import { getValidatedExtractedCurrentPublicDailyKey } from 'utils/dailyKeys';

import { VersionFooter } from 'components/App/VersionFooter';
import { NewTrackingButton } from './NewTrackingButton';
import { ManualSearchButton } from './ManualSearchButton';
import { TrackingList } from './TrackingList';
import {
  ButtonWrapper,
  TrackingWrapper,
  VersionFooterWrapper,
} from './Tracking.styled';
import { useKeyLoader } from '../../hooks/useKeyLoader';

export const Tracking = ({ isHealthDepartmentSigned }) => {
  const { isLoading, error } = useKeyLoader();

  // Check the validity of the daily key when mounting the component
  useEffect(() => {
    if (isLoading || error) return;
    getValidatedExtractedCurrentPublicDailyKey()
      .then(currentDailyKey => getDailyPrivateKey(currentDailyKey.keyId))
      .catch(keyError =>
        console.error('Error in checking key validity:', keyError)
      );
  }, [isLoading, error]);

  if (error || isLoading) return null;

  return (
    <TrackingWrapper>
      {isHealthDepartmentSigned && (
        <ButtonWrapper>
          <ManualSearchButton />
          <NewTrackingButton />
        </ButtonWrapper>
      )}
      <TrackingList isHealthDepartmentSigned={isHealthDepartmentSigned} />
      <VersionFooterWrapper>
        <VersionFooter />
      </VersionFooterWrapper>
    </TrackingWrapper>
  );
};
