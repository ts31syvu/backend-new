import styled from 'styled-components';

export const TrackingWrapper = styled.div`
  padding: 80px 32px;
  background-color: rgb(195, 206, 217);
  position: relative;
  height: 100vh;
`;

export const ButtonWrapper = styled.div`
  position: absolute;
  top: 32px;
  right: 32px;
`;

export const VersionFooterWrapper = styled.div`
  padding-right: 24px;
  float: right;
`;
