import styled from 'styled-components';

export const TableHeader = styled.div`
  display: flex;
  border-bottom: 2px solid black;
  padding: 24px 24px;
  justify-content: space-bewteen;
  font-weight: bold;
`;
