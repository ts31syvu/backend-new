import React from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { SearchIcon } from 'assets/icons';

import { StyledInput } from './EmployeeSearch.styled';

const SearchIconComp = () => (
  <Icon component={SearchIcon} style={{ color: 'black' }} />
);

export const EmployeeSearch = ({ onSearch }) => {
  const intl = useIntl();

  const search = event => {
    onSearch(event.target.value);
  };
  return (
    <StyledInput
      id="employeeSearch"
      placeholder={intl.formatMessage({
        id: 'userManagement.search.placeholder',
      })}
      prefix={<SearchIconComp />}
      onChange={search}
    />
  );
};
