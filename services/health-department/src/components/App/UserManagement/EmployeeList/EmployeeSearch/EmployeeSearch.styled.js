import styled from 'styled-components';
import { Input } from 'antd';

export const StyledInput = styled(Input)`
  width: 50%;
  border-top: none;
  border-left: none;
  border-right: none;
  border-color: black;
  border-radius: 0;
  &:hover,
  &:focus,
  &:active {
    width: 50%;
    border-top: none;
    border-left: none;
    border-right: none;
    border-color: black;
    border-radius: 0;
  }
`;
