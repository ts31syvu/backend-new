import styled from 'styled-components';
import BaseIcon from '@ant-design/icons';

export const IconWrapper = styled.div``;

export const Icon = styled(BaseIcon)`
  color: black;
  margin: 0 4px;
  font-size: 16px;
  cursor: pointer;
`;
