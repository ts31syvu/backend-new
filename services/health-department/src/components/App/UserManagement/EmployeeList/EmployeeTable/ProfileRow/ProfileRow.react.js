import React from 'react';
import { useIntl } from 'react-intl';

// Components
import { Row, Column } from '../../EmployeeList.styled';

export const ProfileRow = ({ searchTerm, profileData }) => {
  const intl = useIntl();
  return (
    <>
      {!searchTerm ? (
        <Row
          key={profileData.employeeId}
          style={{ backgroundColor: '#e8e7e5' }}
        >
          <Column flex="25%">{`${profileData.firstName} ${profileData.lastName}`}</Column>
          <Column flex="25%">{profileData.email}</Column>
          <Column flex="20%">{profileData.phone}</Column>
          <Column flex="10%">
            {intl.formatMessage({
              id: `employeeTable.selectRole.${profileData.isAdmin}`,
            })}
          </Column>
          <Column flex="20%" align="flex-end" />
        </Row>
      ) : null}
    </>
  );
};
