import React from 'react';
import { useIntl } from 'react-intl';
import { TableHeaderComp, Column } from '../EmployeeList.styled';

export const TableHeader = () => {
  const intl = useIntl();

  return (
    <TableHeaderComp>
      <Column flex="25%">
        {intl.formatMessage({ id: 'employeeTable.name' })}
      </Column>
      <Column flex="25%">
        {intl.formatMessage({ id: 'employeeTable.email' })}
      </Column>
      <Column flex="20%">
        {intl.formatMessage({ id: 'employeeTable.phone' })}
      </Column>
      <Column flex="10%">
        {intl.formatMessage({ id: 'employeeTable.role' })}
      </Column>
      <Column flex="20%" />
    </TableHeaderComp>
  );
};
