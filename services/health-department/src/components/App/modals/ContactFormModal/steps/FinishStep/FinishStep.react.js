import React from 'react';
import { useIntl } from 'react-intl';

import { HEALTH_DEPARTMENT_SUPPORT_PHONE_NUMBER } from 'constants/environment';
import { Success, PrimaryButton } from 'components/general';

import { Wrapper, Heading, Text, ButtonWrapper } from './FinishStep.styled';

export const FinishStep = ({ done }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <Success />
      <Heading>
        {intl.formatMessage({ id: 'contactForm.modal.success.heading' })}
      </Heading>
      <Text>
        {intl.formatMessage(
          { id: 'contactForm.modal.success.text' },
          { phone: HEALTH_DEPARTMENT_SUPPORT_PHONE_NUMBER }
        )}
      </Text>
      <ButtonWrapper>
        <PrimaryButton onClick={done}>
          {intl.formatMessage({ id: 'contactForm.modal.success.button' })}
        </PrimaryButton>
      </ButtonWrapper>
    </Wrapper>
  );
};
