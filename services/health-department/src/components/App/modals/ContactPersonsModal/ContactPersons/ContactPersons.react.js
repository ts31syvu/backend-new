import React, { useMemo, useState } from 'react';
import { useQuery } from 'react-query';

import { useIntl } from 'react-intl';
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { Header } from './Header';
import { Overview } from './Overview';
import { GuestCount } from './GuestCount';
import { CorruptedTracesCount } from './CorruptedTracesCount';
import { GuestList } from './GuestList';
import { Export } from './Export';
import { Notify } from './Notify';

import {
  ContactPersonsWrapper,
  Heading,
  FlexWrapper,
} from './ContactPersons.styled';
import { getDecryptedTraces } from './ContactPersons.helper';

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

export const ContactPersons = ({ location, indexPersonData }) => {
  const intl = useIntl();
  const [selectedTraces, setSelectedTraces] = useState([]);

  const {
    data: traces,
    isLoading: isDecrypting,
    error: decryptingError,
  } = useQuery(
    ['decryptedTraces', { transferId: location.transferId }],
    async () => {
      const {
        corruptedTracesOperator,
        decryptedTraces,
      } = await getDecryptedTraces(location);

      const isCorrupted = trace => trace.isInvalid || trace.isUnregistered;

      return {
        decryptedTracesHD: decryptedTraces.filter(trace => !isCorrupted(trace)),
        corruptedTracesHD: decryptedTraces.filter(trace => isCorrupted(trace)),
        corruptedTracesOperator,
      };
    },
    { staleTime: Number.POSITIVE_INFINITY }
  );
  const amountOfAnonymousCheckins = useMemo(
    () =>
      traces?.decryptedTracesHD?.filter(
        trace => trace.isContactDataIncluded === false
      )?.length || 0,
    [traces]
  );

  if (isDecrypting)
    return (
      <Spin
        indicator={antIcon}
        tip={intl.formatMessage({ id: 'message.decryptingData' })}
      />
    );
  if (decryptingError) return null;

  const corruptedTracesCount =
    traces.corruptedTracesHD.length + traces.corruptedTracesOperator.length;

  return (
    <ContactPersonsWrapper>
      <FlexWrapper>
        <Header location={location} indexPersonData={indexPersonData} />
        <Notify traces={selectedTraces} location={location} />
        <Export traces={selectedTraces} location={location} />
      </FlexWrapper>
      <Overview location={location} />
      <Heading>
        {intl.formatMessage({ id: 'contactPersons.guestList' })}
      </Heading>

      <FlexWrapper>
        <GuestCount
          guestCount={traces.decryptedTracesHD.length}
          amountOfAnonymousCheckins={amountOfAnonymousCheckins}
        />
        {corruptedTracesCount > 0 && (
          <CorruptedTracesCount corruptedTracesCount={corruptedTracesCount} />
        )}
      </FlexWrapper>
      <GuestList
        location={location}
        indexPersonData={indexPersonData}
        setSelectedTraces={setSelectedTraces}
        decryptedTraces={traces.decryptedTracesHD}
        emptyTracesCount={traces.corruptedTracesOperator.length}
      />
    </ContactPersonsWrapper>
  );
};
