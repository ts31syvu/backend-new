import React from 'react';
import { Tooltip } from 'antd';
import { useIntl } from 'react-intl';
import {
  StyledInfoCircleOutlined,
  StyledText,
  StyledToolTip,
  ToolTipWrapper,
} from './CorruptedTracesCount.styled';

export const CorruptedTracesCount = ({ corruptedTracesCount }) => {
  const intl = useIntl();
  const toolTipText = (
    <StyledText>
      {intl.formatMessage(
        {
          id: 'contactPersons.corruptedTracesCount',
        },
        { corruptedTracesCount }
      )}
    </StyledText>
  );
  return (
    <ToolTipWrapper>
      <StyledToolTip>
        <Tooltip
          placement="right"
          title={toolTipText}
          getPopupContainer={triggerNode => triggerNode}
        >
          <StyledInfoCircleOutlined />
        </Tooltip>
      </StyledToolTip>
    </ToolTipWrapper>
  );
};
