import React from 'react';
import { useIntl } from 'react-intl';
import XLSX from 'xlsx';

import { logDownload } from 'network/api';

import { sanitizeForCSV } from 'utils/sanitizer';
import { getSanitizedFilename } from './helpers';
import {
  columnIds,
  getSanitizedTraces,
  getSanitizedLocation,
} from './ExcelDownload.helper';
import { DownloadButton } from '../Export.styled';

export const ExcelDownload = ({ traces, location }) => {
  const intl = useIntl();

  const download = () => {
    const columns = columnIds.map(id => intl.formatMessage({ id }));
    const data = getSanitizedTraces(traces, intl).map(trace => [
      ...getSanitizedLocation(location, intl),
      ...trace,
    ]);
    const worksheet = XLSX.utils.aoa_to_sheet([columns, ...data]);
    const workbook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(
      workbook,
      worksheet,
      sanitizeForCSV(location.name).slice(0, 31)
    );
    XLSX.writeFile(
      workbook,
      `${getSanitizedFilename(location.name, 'luca')}.xlsx`
    );

    logDownload({
      type: 'excel',
      transferId: location.transferId,
      amount: traces.length,
    });
  };

  return (
    <DownloadButton onClick={download}>
      {intl.formatMessage({ id: 'download.excel' })}
    </DownloadButton>
  );
};
