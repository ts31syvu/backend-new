import {
  LOCATION_TYPE_INDOOR,
  LOCATION_TYPE_OUTDOOR,
  MAX_LENGTH_ADDITIONAL_INFORMATION,
  TN_LOCATION_CATEGORY,
  TN_LOCATION_INFO_COLUMNS,
  TN_PERSON_INFO_COLUMNS,
} from 'constants/octowareTNFields';
import { sanitizeForCSV } from 'utils/sanitizer';
import {
  getFormattedDate,
  getFormattedTime,
  isFormattedDateValid,
  isFormattedTimeValid,
} from 'utils/time';
import { formatAdditionalDataKey } from './helpers';

export const columns = [
  ...Object.values(TN_LOCATION_INFO_COLUMNS),
  ...Object.values(TN_PERSON_INFO_COLUMNS),
];

export const matchLucaGroupTypeWithOctowareTN = groupType => {
  switch (groupType) {
    case 'base':
      return TN_LOCATION_CATEGORY.base;
    case 'restaurant':
      return TN_LOCATION_CATEGORY.restaurant;
    case 'nursing_home':
      return TN_LOCATION_CATEGORY.nursing_home;
    case 'hotel':
      return TN_LOCATION_CATEGORY.hotel;
    case 'store':
      return TN_LOCATION_CATEGORY.store;
    default:
      return TN_LOCATION_CATEGORY.base;
  }
};

export const getLocationType = isIndoor =>
  isIndoor ? LOCATION_TYPE_INDOOR : LOCATION_TYPE_OUTDOOR;

export const getSanitizedData = (traces, location, intl) =>
  traces.map(({ userData = '', additionalData, checkin, checkout }) => ({
    additionalData: additionalData
      ? Object.keys(additionalData)
          .map(
            key =>
              `${sanitizeForCSV(
                formatAdditionalDataKey(key, intl)
              )}: ${sanitizeForCSV(additionalData[key])}`
          )
          .join()
          .slice(0, MAX_LENGTH_ADDITIONAL_INFORMATION)
      : '',
    lastName: userData
      ? sanitizeForCSV(userData.ln)
      : intl.formatMessage({
          id: 'contactPersonTable.octoWareTN.unregisteredBadgeUser',
        }),
    firstName: userData
      ? sanitizeForCSV(userData.fn)
      : intl.formatMessage({
          id: 'contactPersonTable.octoWareTN.unregisteredBadgeUser',
        }),
    firstContactDateTime: `${
      isFormattedDateValid(checkin)
        ? sanitizeForCSV(getFormattedDate(checkin))
        : ''
    } ${
      isFormattedTimeValid(checkin)
        ? sanitizeForCSV(getFormattedTime(checkin))
        : ''
    }`,
    lastContactDateTime: `${
      isFormattedDateValid(checkout)
        ? sanitizeForCSV(getFormattedDate(checkout))
        : ''
    } ${
      isFormattedTimeValid(checkout)
        ? sanitizeForCSV(getFormattedTime(checkout))
        : ''
    }`,
    street: sanitizeForCSV(userData.st),
    houseNumber: sanitizeForCSV(userData.hn),
    postalCode: sanitizeForCSV(userData.pc),
    city: sanitizeForCSV(userData.c),
    phone: sanitizeForCSV(userData.pn),
    email: sanitizeForCSV(userData.e),
  }));
