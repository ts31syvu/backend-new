import React from 'react';
import { useIntl } from 'react-intl';
import XLSX from 'xlsx';
import moment from 'moment';

import {
  MAX_LENGTH_ENVIRONMENT,
  PLATFORM_NAME_LUCA,
  TN_PERSON_INFO_COLUMNS,
} from 'constants/octowareTNFields';
import { logDownload } from 'network/api';
import { DATE_FORMAT } from 'utils/time';
import { sanitizeForCSV } from 'utils/sanitizer';

import { getSanitizedFilename } from './helpers';
import { DownloadButton } from '../Export.styled';
import {
  columns,
  getLocationType,
  getSanitizedData,
  matchLucaGroupTypeWithOctowareTN,
} from './OctoWareTNDownload.helper';

export const OctoWareTNDownload = ({ traces, location }) => {
  const intl = useIntl();

  const download = () => {
    const sharedDataDate = moment(location.contactedAt, DATE_FORMAT, true);
    const worksheet = XLSX.utils.aoa_to_sheet([
      columns,
      ...getSanitizedData(traces, location, intl).map(trace => [
        PLATFORM_NAME_LUCA,
        matchLucaGroupTypeWithOctowareTN(location.groupType),
        `${sanitizeForCSV(`${location.name}`).slice(
          0,
          MAX_LENGTH_ENVIRONMENT
        )} | ${getLocationType(location.isIndoor)}`,
        trace.additionalData,
        sharedDataDate.isValid() ? sharedDataDate.format() : '',

        ...Object.keys(TN_PERSON_INFO_COLUMNS).map(key =>
          trace[key] ? trace[key] : ''
        ),
      ]),
    ]);
    const workbook = XLSX.utils.book_new();

    XLSX.utils.book_append_sheet(
      workbook,
      worksheet,
      sanitizeForCSV(location.name).slice(0, 31)
    );
    XLSX.writeFile(
      workbook,
      `${getSanitizedFilename(location.name, 'octoware')}.xlsx`
    );

    logDownload({
      type: 'octoware',
      transferId: location.transferId,
      amount: traces.length,
    });
  };

  return (
    <DownloadButton onClick={download}>
      {intl.formatMessage({ id: 'download.octoWareTN' })}
    </DownloadButton>
  );
};
