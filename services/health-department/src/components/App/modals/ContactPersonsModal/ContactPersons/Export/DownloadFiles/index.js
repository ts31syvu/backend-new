import { CSVDownload } from './CSVDownload';
import { ExcelDownload } from './ExcelDownload';
import { SormasDownload } from './SormasDownload';
import { OctoWareTNDownload } from './OctoWareTNDownload';

export const FILES = {
  CSVDownload,
  ExcelDownload,
  SormasDownload,
  OctoWareTNDownload,
};
