import React from 'react';
import { useIntl } from 'react-intl';
import { Tooltip } from 'antd';
import {
  Count,
  AnonymousCheckins,
  StyledInfoCircleOutlined,
} from './GuestCount.styled';

export const GuestCount = ({ guestCount, amountOfAnonymousCheckins }) => {
  const intl = useIntl();

  return (
    <Count>
      {intl.formatMessage(
        {
          id: 'contactPersons.guests',
        },
        { count: guestCount }
      )}
      {amountOfAnonymousCheckins > 0 && (
        <>
          <AnonymousCheckins>
            {intl.formatMessage(
              {
                id: 'contactPersons.amountOfAnonymousCheckins',
              },
              { amount: <b>{amountOfAnonymousCheckins}</b> }
            )}
          </AnonymousCheckins>
          <Tooltip
            placement="top"
            title={intl.formatMessage({
              id: 'contactPersons.anonymousCheckinsInfo',
            })}
          >
            <StyledInfoCircleOutlined />
          </Tooltip>
        </>
      )}
    </Count>
  );
};
