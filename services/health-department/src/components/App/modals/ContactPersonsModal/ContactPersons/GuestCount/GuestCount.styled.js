import styled from 'styled-components';
import { InfoCircleOutlined } from '@ant-design/icons';

export const Count = styled.div`
  display: flex;
  align-items: center;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-top: 8px;
`;

export const AnonymousCheckins = styled.span`
  margin-left: 56px;
`;

export const StyledInfoCircleOutlined = styled(InfoCircleOutlined)`
  font-size: 14px;
  line-height: 1.5715;
  margin-left: 16px;
`;
