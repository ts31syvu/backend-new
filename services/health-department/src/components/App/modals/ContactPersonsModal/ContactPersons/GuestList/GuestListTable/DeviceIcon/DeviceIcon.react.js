import React from 'react';
import { useIntl } from 'react-intl';

import { KeycardIcon, PenIcon, MobilePhoneIcon } from 'assets/icons';
import { StyledTooltip } from 'components/general';
import {
  DEVICE_TYPE_ANDROID,
  DEVICE_TYPE_IOS,
  DEVICE_TYPE_STATIC,
  DEVICE_TYPE_WEBAPP,
  DEVICE_TYPE_FORM,
  DEVICE_APP,
  DEVICE_BADGE,
  DEVICE_CONTACT_FORM,
} from 'constants/deviceTypes';
import { StyledIcon, Wrapper } from './DeviceIcon.styled';

export const DeviceIcon = ({ deviceType }) => {
  const intl = useIntl();

  const guestTooltip = (deviceName, iconComponent) => {
    return (
      <Wrapper>
        <StyledTooltip
          title={intl.formatMessage({
            id: `modal.guestList.deviceType.${deviceName}`,
          })}
        >
          <StyledIcon component={iconComponent} />
        </StyledTooltip>
      </Wrapper>
    );
  };

  const mappedTooltips = () => {
    switch (deviceType) {
      case DEVICE_TYPE_IOS:
        return guestTooltip(DEVICE_APP, MobilePhoneIcon);
      case DEVICE_TYPE_ANDROID:
        return guestTooltip(DEVICE_APP, MobilePhoneIcon);
      case DEVICE_TYPE_WEBAPP:
        return guestTooltip(DEVICE_APP, MobilePhoneIcon);
      case DEVICE_TYPE_STATIC:
        return guestTooltip(DEVICE_BADGE, KeycardIcon);
      case DEVICE_TYPE_FORM:
        return guestTooltip(DEVICE_CONTACT_FORM, PenIcon);
      default:
        return guestTooltip(DEVICE_CONTACT_FORM, PenIcon);
    }
  };

  return mappedTooltips();
};
