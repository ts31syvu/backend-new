import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const StyledIcon = styled(Icon)`
  font-size: 16px;
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
`;
