import styled from 'styled-components';
import { CheckCircleOutlined } from '@ant-design/icons';

export const StyledCheckIcon = styled(CheckCircleOutlined)`
  font-size: 65px;
  color: #819e57;
  margin-bottom: 35px;
`;

export const StyledHeadline = styled.div`
  color: rgb(0, 0, 0);
  font-size: 34px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  text-align: center;
  margin-bottom: 24px;
`;

export const StyledText = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  text-align: center;
  margin-bottom: 40px;
`;

export const InfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: space-around;
  margin-bottom: 20px;
`;
