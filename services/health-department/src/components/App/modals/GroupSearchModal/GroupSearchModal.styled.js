import styled from 'styled-components';
import { Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

export const StyledSearchOutlined = styled(SearchOutlined)`
  color: gray;
`;
export const DescriptionText = styled.div`
  font-size: 16px;
  font-weight: bold;
  padding-bottom: 24px;
`;

export const GroupSearchInput = styled(Input)`
  border-top: none;
  border-left: none;
  border-right: none;
  border-bottom: 1px solid #000000;
  border-radius: 0;
  box-shadow: none;
  &:hover,
  &:focus,
  &:active {
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid #000000;
    border-radius: 0;
    box-shadow: none;
  }
`;

export const ZipCodeInput = styled(Input)`
  border-top: none;
  border-left: none;
  border-right: none;
  border-bottom: 1px solid #000000;
  border-radius: 0;
  box-shadow: none;
  &:hover,
  &:focus,
  &:active {
    border-top: none;
    border-left: none;
    border-right: none;
    border-bottom: 1px solid #000000;
    border-radius: 0;
    box-shadow: none;
  }
`;

export const GroupSearchWrapper = styled.div`
  width: 100%;
  background-color: #ffffff;
  padding-bottom: 24px;
`;

export const InputWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
