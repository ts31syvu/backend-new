import React from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton } from 'components/general';
import { TimeStatusContainer } from 'components/App/Contact/DetailsView/Messages/MessageRow/TimeStatusContainer';
import {
  Headline,
  MessageSubject,
  MessageContent,
  ModalWrapper,
  ButtonWrapper,
} from './MessageDetailsModal.styled';

export const MessageDetailsModal = ({
  createdTime,
  receivedTime,
  readTime,
  isMessageDelivered,
  isMessageRead,
  message,
  closeModal,
}) => {
  const intl = useIntl();

  return (
    <ModalWrapper>
      <Headline>
        {intl.formatMessage({
          id: 'contactDetails.messageDetailsModal.headline',
        })}
      </Headline>
      <TimeStatusContainer
        createdTime={createdTime}
        receivedTime={receivedTime}
        readTime={readTime}
        isMessageDelivered={isMessageDelivered}
        isMessageRead={isMessageRead}
      />
      <MessageSubject>{message.decryptedData.sub}</MessageSubject>
      <MessageContent>{message.decryptedData.msg}</MessageContent>
      <ButtonWrapper>
        <PrimaryButton onClick={closeModal}>
          {intl.formatMessage({
            id: 'contact.details.modal.close',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </ModalWrapper>
  );
};
