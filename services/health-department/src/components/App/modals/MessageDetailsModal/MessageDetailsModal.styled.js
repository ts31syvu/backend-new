import styled from 'styled-components';

export const ModalWrapper = styled.div``;

export const Headline = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 34px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  margin-bottom: 30px;
`;

export const MessageSubject = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  margin-top: 20px;
`;

export const MessageContent = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  height: 216px;
  min-width: 674px;
  margin: 26px 0 32px 0;
  overflow-y: scroll;
  text-align: justify;
  text-justify: inter-word;
  padding: 10px 0;
  background-color: #f7f7f7;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 30px;
`;
