import { NOTIFIABLE_DEVICE_TYPES } from 'constants/deviceTypes';

export const filterByDeviceType = contactPersons =>
  contactPersons.filter(
    contact =>
      contact.deviceType === NOTIFIABLE_DEVICE_TYPES.IOS ||
      contact.deviceType === NOTIFIABLE_DEVICE_TYPES.ANDROID
  );

export const filterRiskLevels = (traceIdsToFilter, riskLevels, level) =>
  traceIdsToFilter.filter(traceId =>
    riskLevels.some(
      riskLevel =>
        riskLevel.traceId === traceId && !riskLevel.riskLevels.includes(level)
    )
  );
