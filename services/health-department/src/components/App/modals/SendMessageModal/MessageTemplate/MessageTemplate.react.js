import React, { useState, useEffect } from 'react';
import { Form, Input, Select } from 'antd';
import { useIntl } from 'react-intl';

import { getHealthDepartment } from 'network/api';

import { PrimaryButton } from 'components/general';
import {
  ButtonWrapper,
  StyledFormItem,
  StyledSelect,
  TemplatLabel,
} from './MessageTemplate.styled';

import { getTemplateOptions, POSITIVE_TEST } from './MessageTemplate.helper';

const maxMessageLength = 2000;
const maxTitleLength = 100;
const { Option } = Select;

export const MessageTemplate = ({
  isMultipleEntries,
  sendMultipleMessages,
  sendSingleMessage,
  hdEmployee,
}) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [department, setDepartment] = useState(null);

  useEffect(() => {
    if (!hdEmployee) return;
    getHealthDepartment(hdEmployee.departmentId)
      .then(newDepartment => {
        setDepartment(newDepartment);
      })
      .catch(error => console.error(error));
  }, [hdEmployee]);

  if (!department) return null;

  const options = getTemplateOptions(intl, department.name);

  const changeTemplate = value => {
    const selectedOption = options.find(option => option.value === value);
    form.setFieldsValue({
      sub: selectedOption.templateTitle,
      msg: selectedOption.templateMessage,
    });
  };

  const onSubmit = () => form.submit();

  return (
    <>
      <TemplatLabel>
        {intl.formatMessage({
          id: 'sendMessageModal.template.title',
        })}
      </TemplatLabel>
      <StyledSelect defaultValue={POSITIVE_TEST} onSelect={changeTemplate}>
        {options.map(option => (
          <Option key={option.value} value={option.value}>
            {option.name}
          </Option>
        ))}
      </StyledSelect>
      <Form
        form={form}
        onFinish={isMultipleEntries ? sendMultipleMessages : sendSingleMessage}
        initialValues={{
          sub: intl.formatMessage({
            id: 'sendMessageModal.option.positive_test.title',
          }),
          msg: intl.formatMessage(
            {
              id: 'sendMessageModal.option.positive_test.message',
            },
            { departmentName: department.name }
          ),
        }}
      >
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'sendMessageModal.messageTitle',
          })}
          name="sub"
          required
        >
          <Input maxLength={maxTitleLength} />
        </StyledFormItem>
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'sendMessageModal.message',
          })}
          name="msg"
          required
        >
          <Input.TextArea
            maxLength={maxMessageLength}
            autoSize={{ minRows: 2, maxRows: 10 }}
            showCount
          />
        </Form.Item>
        <ButtonWrapper>
          <PrimaryButton onClick={onSubmit}>
            {intl.formatMessage({
              id: 'contactForm.modal.submit',
            })}
          </PrimaryButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
