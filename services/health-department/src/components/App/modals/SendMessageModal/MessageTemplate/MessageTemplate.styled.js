import styled from 'styled-components';
import { Form, Select } from 'antd';

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 40px;
`;

export const StyledFormItem = styled(Form.Item)``;

export const TemplatLabel = styled.div`
  margin-bottom: 8px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const StyledSelect = styled(Select)`
  margin-bottom: 16px;
  width: 50%;
`;
