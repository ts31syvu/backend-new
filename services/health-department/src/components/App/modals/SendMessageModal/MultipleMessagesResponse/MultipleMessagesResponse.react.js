import React from 'react';
import { Table } from 'antd';
import { useIntl } from 'react-intl';
import { SuccessCheckIcon, UnsuccessCheckIcon } from 'components/general';
import { ErrorMessage } from './MultipleMessagesResponse.styled';

export const MultipleMessagesResponse = ({
  multipleEntries,
  sendMessageResponses,
}) => {
  const intl = useIntl();

  const columns = [
    {
      title: intl.formatMessage({
        id: 'contact.messageResponse.tableHeader.name',
      }),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: intl.formatMessage({
        id: 'contact.messageResponse.tableHeader.address',
      }),
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: intl.formatMessage({
        id: 'contact.messageResponse.tableHeader.phone',
      }),
      dataIndex: 'phone',
      key: 'phone',
    },
    {
      title: intl.formatMessage({
        id: 'contact.messageResponse.tableHeader.sendMessageStatus',
      }),
      key: 'sendMessageStatus',
      align: 'center',
      render: ({ sendMessageStatus }) => {
        return sendMessageStatus === 204 ? (
          <SuccessCheckIcon />
        ) : (
          <UnsuccessCheckIcon />
        );
      },
    },
  ];

  const data = multipleEntries.map((person, index) => ({
    key: person.key,
    name: `${person.firstName} ${person.lastName}`,
    address: `${person.address.street} ${person.address.streetNo}, ${person.address.zipCode} ${person.address.city}`,
    phone: person.phone,
    sendMessageStatus: sendMessageResponses[index].status,
  }));

  return (
    <>
      <Table dataSource={data} columns={columns} pagination={false} />
      <ErrorMessage>
        {intl.formatMessage({
          id: 'contact.messageResponse.tableHeader.errorMessage',
        })}
      </ErrorMessage>
    </>
  );
};
