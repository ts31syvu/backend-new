import styled from 'styled-components';

export const ErrorMessage = styled.div`
  color: #5b5b5b;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin: 24px 0;
`;
