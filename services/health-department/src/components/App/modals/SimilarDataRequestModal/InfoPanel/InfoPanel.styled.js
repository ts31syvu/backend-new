import styled from 'styled-components';

export const InfoFieldWrapper = styled.div`
  padding-bottom: 8px;
`;

export const InfoFieldName = styled.span`
  font-size: 16px;
  font-weight: bold;
  display: inline-block;
  width: 45%;
`;

export const InfoFieldData = styled.span`
  font-size: 16px;
  font-weight: 500;
`;

export const InfoText = styled.div`
  font-size: 16px;
  font-weight: 500;
  padding-bottom: 32px;
  padding-top: 32px;
`;

export const NotificationTextContentHeader = styled.div`
  font-size: 16px;
  font-weight: bold;
  padding-bottom: 8px;
`;

export const NotificationTextTitle = styled.div`
  font-size: 16px;
  font-style: italic;
  font-weight: 500;
  padding-bottom: 32px;
`;

export const NotificationTextMessage = styled.div`
  font-size: 16px;
  font-style: italic;
  font-weight: 500;
`;

export const SeperationBorder = styled.div`
  border-bottom: 0.5px solid rgb(0, 0, 0);
  padding-top: 16px;
`;
