import React from 'react';
import { useQuery } from 'react-query';

import { getHealthDepartment } from 'network/api';

import { formattedTimeLabel } from 'utils/time';
import {
  HeaderWrapper,
  HeaderDateTime,
  HeaderHDName,
} from './PanelHeader.styled';

export const PanelHeader = ({ associatedRequest }) => {
  const { data: healthDepartment } = useQuery(
    ['healthDepartment', associatedRequest.departmentId],
    () => getHealthDepartment(associatedRequest.departmentId)
  );
  if (!healthDepartment) return null;

  return (
    <HeaderWrapper>
      <HeaderDateTime>
        {formattedTimeLabel(associatedRequest.contactedAt)}
      </HeaderDateTime>
      <HeaderHDName>{healthDepartment.name}</HeaderHDName>
    </HeaderWrapper>
  );
};
