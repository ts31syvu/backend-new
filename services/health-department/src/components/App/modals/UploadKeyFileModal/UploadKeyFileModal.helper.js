export const uploadMessages = {
  initial: 'modal.privateKey.uploadMessage',
  size: 'login.keyFileSize.error.description',
  error: 'login.keyFile.error.title',
  done: 'login.keyFile.success.title',
};

export const statusProgress = {
  initial: '',
  success: 'success',
  exception: 'exception',
};
