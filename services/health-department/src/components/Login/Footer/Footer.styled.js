import styled from 'styled-components';

export const FooterWrapper = styled.footer`
  display: flex;
  flex-direction: column;
`;

export const InfoWrapper = styled.span`
  padding-right: 32px;
`;

export const GitlabLink = styled.a`
  color: black;
`;
