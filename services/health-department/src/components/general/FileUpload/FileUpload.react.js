import React from 'react';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { Progress } from 'antd';
import { FileUpload, UploadMessage, UploadProgress } from './FileUpload.styled';

export const FileUploadComp = ({
  type,
  accept,
  customRequest,
  buttonType,
  buttonTextId,
  progressPercent,
  uploadStatus,
  uploadMessageId,
}) => {
  const intl = useIntl();

  return (
    <FileUpload
      type={type}
      accept={accept}
      customRequest={customRequest}
      showUploadList={false}
    >
      <UploadMessage>
        {intl.formatMessage({ id: uploadMessageId })}
      </UploadMessage>

      {progressPercent > 0 && (
        <UploadProgress>
          <Progress
            percent={progressPercent}
            status={uploadStatus}
            trailColor="#fff"
          />
        </UploadProgress>
      )}

      {buttonType === 'primary' ? (
        <PrimaryButton>
          {intl.formatMessage({
            id: buttonTextId,
          })}
        </PrimaryButton>
      ) : (
        <SecondaryButton>
          {intl.formatMessage({
            id: buttonTextId,
          })}
        </SecondaryButton>
      )}
    </FileUpload>
  );
};
