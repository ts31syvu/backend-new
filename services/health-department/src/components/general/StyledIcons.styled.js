import styled from 'styled-components';
import { CloseOutlined, ArrowLeftOutlined } from '@ant-design/icons';

export const StyledCloseIcon = styled(CloseOutlined)`
  font-size: 18px;
  font-weight: bold;
`;

export const StyledBackIcon = styled(ArrowLeftOutlined)`
  font-size: 18px;
  font-weight: bold;
`;
