import { useState, useEffect } from 'react';
import { useQuery, useQueries } from 'react-query';
import { getLocation, getLocationTransfers } from 'network/api';

/**
 * Enriches location transfers with location data
 * @param transfers to be enriched
 * @param locations to search from
 * @returns array of enriched location transfers
 */
const enrichTransfersWithLocations = (transfers, locations) => {
  return transfers?.map(transfer => {
    // get matching location for current location transfer
    const matchingLocation = locations.find(locationQuery => {
      if (locationQuery?.data) {
        return locationQuery.data.locationId === transfer.locationId;
      }

      return locationQuery.locationId === transfer.locationId;
    });

    const matchingLocationFormatted =
      matchingLocation?.data || matchingLocation;

    return {
      ...matchingLocationFormatted,
      time: transfer.time,
      transferId: transfer.uuid,
      isCompleted: transfer.isCompleted,
      contactedAt: transfer.contactedAt,
      approvedAt: transfer.approvedAt,
    };
  });
};

/**
 * Queries backend to retrieve all locationTransfers for the given process id
 * and enriches the location transfers with the location information.
 * @param processUuid
 * @returns array of location transfers (incl. location data)
 */
export const useLocationTransfers = processUuid => {
  const [locationTransfers, setLocationTransfers] = useState([]);

  const { data: transfers } = useQuery(
    ['transfers', { processId: processUuid }],
    () => getLocationTransfers(processUuid)
  );

  // enrich locationTransfers with location data
  const locationIds = transfers?.map(transfer => transfer.locationId);
  const locationQueries = locationIds?.map(locationId => ({
    queryKey: `location${locationId}`,
    queryFn: () => getLocation(locationId),
    staleTime: Number.POSITIVE_INFINITY,
  }));

  const locations = useQueries(locationQueries || []);
  const locationsLoaded = locations.every(query => !!query?.isSuccess);

  useEffect(() => {
    if (locationsLoaded) {
      setLocationTransfers(enrichTransfersWithLocations(transfers, locations));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [transfers, locationsLoaded]);

  return locationTransfers || [];
};

const getDataLocations = locationIds => {
  return Promise.all(
    locationIds.map(locationId => Promise.resolve(getLocation(locationId)))
  );
};

export const useGetEnrichedLocationTransfers = processUuid => {
  const [enrichedLocationTransfers, setEnrichedLocationTransfers] = useState(
    []
  );

  const getEnrichedLocationTransfers = async () => {
    const transfers = await getLocationTransfers(processUuid);

    const locationIds = transfers?.map(transfer => transfer.locationId);

    const locations = await getDataLocations(locationIds);

    return enrichTransfersWithLocations(transfers, locations);
  };

  useEffect(() => {
    getEnrichedLocationTransfers()
      .then(data => {
        setEnrichedLocationTransfers(data);
      })
      .catch(() => {});
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return [enrichedLocationTransfers];
};
