import jwt from 'jsonwebtoken';
import {
  verifySignedPublicDailyKey,
  verifySignedEncryptedPrivateDailyKey,
} from '@lucaapp/crypto';
import { getRootCA, getBasicCA } from 'network/static';
import {
  getIssuer,
  getIssuersWithSignature,
  getDailyKey,
  getAllDailyKeys,
  getCurrentDailyKey,
  getEncryptedDailyPrivateKey,
} from 'network/api';

const validateAndExtractDailyKey = async signedPublicDailyKey => {
  const rootCA = await getRootCA();
  const basicCA = await getBasicCA();
  const decodedJWT = jwt.decode(signedPublicDailyKey);
  const issuerId = decodedJWT.iss;

  const issuer = await getIssuer(issuerId);

  return verifySignedPublicDailyKey({
    certificateChain: [rootCA, basicCA],
    issuer,
    signedPublicDailyKey,
  });
};

export const getValidatedExtractedEncryptedPrivateDailyKey = async id => {
  const { signedEncryptedPrivateDailyKey } = await getEncryptedDailyPrivateKey(
    id
  );
  const rootCA = await getRootCA();
  const basicCA = await getBasicCA();
  const decodedJWT = jwt.decode(signedEncryptedPrivateDailyKey);
  const issuerId = decodedJWT.iss;

  const issuer = await getIssuer(issuerId);

  return verifySignedEncryptedPrivateDailyKey({
    certificateChain: [rootCA, basicCA],
    issuer,
    signedEncryptedPrivateDailyKey,
  });
};

export const getAllValidatedExtractedCurrentPublicDailyKeys = async () => {
  const dailyKeys = await getAllDailyKeys();
  const rootCA = await getRootCA();
  const basicCA = await getBasicCA();
  const issuers = await getIssuersWithSignature();

  return dailyKeys.map(({ signedPublicDailyKey }) => {
    const decodedJWT = jwt.decode(signedPublicDailyKey);
    const issuerId = decodedJWT.iss;
    return verifySignedPublicDailyKey({
      certificateChain: [rootCA, basicCA],
      issuer: issuers.find(issuer => issuer.issuerId === issuerId),
      signedPublicDailyKey,
    });
  });
};

export const getValidatedExtractedPublicDailyKey = async id => {
  const { signedPublicDailyKey } = await getDailyKey(id);
  return validateAndExtractDailyKey(signedPublicDailyKey);
};

export const getValidatedExtractedCurrentPublicDailyKey = async () => {
  const { signedPublicDailyKey } = await getCurrentDailyKey();
  return validateAndExtractDailyKey(signedPublicDailyKey);
};
