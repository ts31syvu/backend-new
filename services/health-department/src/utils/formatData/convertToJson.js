export const convertToJson = csv => {
  const lines = csv.split('\n');
  const result = [];
  const headers = lines[0].split(',');
  for (let index = 1; index < lines.length; index++) {
    const object = {};
    const currentline = lines[index].split(',');
    for (const [index_, header] of headers.entries()) {
      // eslint-disable-next-line no-underscore-dangle
      object[header] = currentline[index_];
    }
    result.push(object);
  }
  return result;
};
