import { normalizeName } from './normalizeName';

it('removes titles from the name', () => {
  const testNames = [
    {
      input: 'Erika-Maria',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Erika Maria',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Erika Maria Ina',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Prof Dr Dr. Erika',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Mustermann',
      expectedResult: 'MUSTERMANN',
    },
    {
      input: 'Erika',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Prof.Dr.Dr.Dr.Prof.Mustermann',
      expectedResult: 'MUSTERMANN',
    },
    {
      input: '    Dr.   Erika Maria ',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Prof Dr Dr. Erika',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Mustermann, Prof. Dr.',
      expectedResult: 'MUSTERMANN',
    },
    {
      input: 'Dr Erika',
      expectedResult: 'ERIKA',
    },
    {
      input: 'Mustermann, Dr',
      expectedResult: 'MUSTERMANN',
    },
    {
      input: 'Mustermann, DR',
      expectedResult: 'MUSTERMANN',
    },
    {
      input: 'DR Erika',
      expectedResult: 'ERIKA',
    },
    {
      input: 'PROF DR Dr. Erika',
      expectedResult: 'ERIKA',
    },
  ];

  testNames.map(testName =>
    expect(normalizeName(testName.input)).toBe(testName.expectedResult)
  );
});
