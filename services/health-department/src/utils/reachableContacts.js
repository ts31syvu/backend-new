import {
  ENCRYPT_DLIES,
  HKDF_SHA256,
  HMAC_SHA256,
  SHA256,
  GET_RANDOM_BYTES,
  decodeUtf8,
  hexToBytes,
  hexToBase64,
  bytesToHex,
  base64ToHex,
  encodeUtf8,
  int32ToHex,
  uuidToHex,
  ENCRYPT_AES_CTR,
  DECRYPT_AES_CTR,
} from '@lucaapp/crypto';
import safeCompare from 'safe-compare';
import moment from 'moment';
import { uniqBy } from 'lodash';

import { openSearchProcess, startConversation, sendMessage } from 'network/api';
import { decryptWithHDEKP, getMessageIKM } from './cryptoKeyOperations';
import {
  getHashAndPrefix,
  getNameTerm,
  getPhoneTerm,
  removeMultipleNames,
} from './reachableContactsHashing';
import { z } from './zod';

const reachableContactDataScheme = z
  .object({
    cd: z
      .object({
        fn: z.string(),
        ln: z.string(),
        st: z.string(),
        hn: z.string(),
        c: z.string(),
        pc: z.string(),
        pn: z.string(),
        e: z.string().email().nullable().optional(),
        v: z.number(),
      })
      .strict(),
    ct: z.array(z.string()).min(1).max(2),
    ns: z.base64({ length: 24, rawLength: 16 }),
    ep: z.ecCompressedPublicKey(),
    sp: z.ecCompressedPublicKey(),
    dt: z.deviceType(),
    ci: z.boolean().nullable().optional(),
    vg: z.boolean().nullable().optional(),
    jb: z.string().max(100).nullable().optional(),
    em: z.string().max(100).nullable().optional(),
    v: z.number(),
  })
  .strict();

const hdReencryptedContactDataScheme = reachableContactDataScheme.extend({
  vct: z
    .object({
      v: z.number(),
      bd: z.string(),
      vc: z
        .object({
          cd: z.number(),
          td: z.number(),
          d: z.string(),
        })
        .optional(),
      r: z
        .object({
          d: z.string(),
        })
        .optional(),
    })
    .strict(),
});

const messageDataSchema = z
  .object({
    sub: z.string(),
    msg: z.string(),
  })
  .strict();

const decryptAndValidateReachableContactData = encryptedContactData => {
  const decryptedUserData = JSON.parse(
    decodeUtf8(
      hexToBytes(
        decryptWithHDEKP(
          encryptedContactData.publicKey,
          encryptedContactData.iv,
          encryptedContactData.mac,
          encryptedContactData.data
        )
      )
    )
  );
  return reachableContactDataScheme.parse(decryptedUserData);
};

export const decryptAndValidateHDReencryptedContactData = encryptedContactData => {
  const decryptedUserData = JSON.parse(
    decodeUtf8(
      hexToBytes(
        decryptWithHDEKP(
          encryptedContactData.publicKey,
          encryptedContactData.iv,
          encryptedContactData.mac,
          encryptedContactData.data
        )
      )
    )
  );
  return hdReencryptedContactDataScheme.parse(decryptedUserData);
};

export const createSearch = async (publicHDEKP, searchTerms) => {
  const searchItems = [];

  searchTerms.forEach(searchTerm => {
    const { firstName, lastName, phone } = searchTerm;

    if (phone) {
      const { prefix } = getHashAndPrefix({ phone });
      const encryptedTerm = ENCRYPT_DLIES(
        base64ToHex(publicHDEKP),
        bytesToHex(encodeUtf8(phone))
      );
      searchItems.push({
        prefix: hexToBase64(prefix),
        type: 'phone',
        term: {
          data: hexToBase64(encryptedTerm.data),
          iv: hexToBase64(encryptedTerm.iv),
          mac: hexToBase64(encryptedTerm.mac),
          publicKey: hexToBase64(encryptedTerm.publicKey),
        },
      });
    }

    if (firstName && lastName) {
      const { prefix } = getHashAndPrefix({ firstName, lastName });
      const encryptedTerm = ENCRYPT_DLIES(
        base64ToHex(publicHDEKP),
        bytesToHex(encodeUtf8(`${removeMultipleNames(firstName)} ${lastName}`))
      );
      searchItems.push({
        prefix: hexToBase64(prefix),
        type: 'name',
        term: {
          data: hexToBase64(encryptedTerm.data),
          iv: hexToBase64(encryptedTerm.iv),
          mac: hexToBase64(encryptedTerm.mac),
          publicKey: hexToBase64(encryptedTerm.publicKey),
        },
      });
    }
  });

  const { searchProcessId } = await openSearchProcess({
    search: searchItems,
  }).then(response => response.json());

  return searchProcessId;
};

export const createConversation = (publicHDEKP, searchId, userObject) => {
  const encryptedContactData = ENCRYPT_DLIES(
    base64ToHex(publicHDEKP),
    bytesToHex(encodeUtf8(JSON.stringify(userObject)))
  );

  return startConversation({
    searchId,
    contact: {
      data: hexToBase64(encryptedContactData.data),
      iv: hexToBase64(encryptedContactData.iv),
      publicKey: hexToBase64(encryptedContactData.publicKey),
      mac: hexToBase64(encryptedContactData.mac),
    },
  });
};

export const decryptReference = encryptedSearchReference =>
  decryptWithHDEKP(
    encryptedSearchReference.publicKey,
    encryptedSearchReference.iv,
    encryptedSearchReference.mac,
    encryptedSearchReference.data
  );

const checkFalsePositive = (reference, searchTerm, type) => {
  if (type === 'phone')
    return (
      reference.slice(66, 130) === SHA256(bytesToHex(getPhoneTerm(searchTerm)))
    );

  return (
    reference.slice(2, 66) ===
    SHA256(
      bytesToHex(
        getNameTerm(searchTerm.split(' ')[0], searchTerm.split(' ')[1])
      )
    )
  );
};

export const filterAndDecryptSearchResults = (
  results,
  searchTerm,
  resultType
) => {
  const uniqueFilteredResults = uniqBy(
    results
      .map(result => ({
        ...result,
        decryptedReference: decryptReference(result.reference),
      }))
      .filter(({ decryptedReference }) =>
        checkFalsePositive(decryptedReference, searchTerm, resultType)
      ),
    'decryptedReference'
  );
  return uniqueFilteredResults.map(({ data }) =>
    decryptAndValidateReachableContactData(data)
  );
};

export const sendCoversationMessage = async ({
  messageObject,
  conversationId,
  hdId,
  seed,
  mekp,
}) => {
  const timeslice = Math.floor(moment().unix() / 300) * 300;
  const salt = `${int32ToHex(timeslice)}${uuidToHex(hdId)}`;
  const ikm = getMessageIKM(mekp);

  const messageId = await HKDF_SHA256(
    base64ToHex(seed),
    16,
    bytesToHex('messageId'),
    salt
  );
  const iv = GET_RANDOM_BYTES(16);
  const data = ENCRYPT_AES_CTR(
    bytesToHex(encodeUtf8(JSON.stringify(messageObject))),
    await HKDF_SHA256(ikm, 16, bytesToHex('encryption'), messageId),
    iv
  );
  const mac = HMAC_SHA256(
    messageId + data + iv,
    await HKDF_SHA256(ikm, 16, bytesToHex('authentication'), messageId)
  );

  return sendMessage({
    conversationId,
    messageId: hexToBase64(messageId),
    data: hexToBase64(data),
    iv: hexToBase64(iv),
    mac: hexToBase64(mac),
  });
};

export const decryptConversationMessage = async ({
  messageId,
  mekp,
  data,
  iv,
  mac,
}) => {
  const dataHex = base64ToHex(data);
  const messageIdHex = base64ToHex(messageId);
  const ivHex = base64ToHex(iv);
  const macHex = base64ToHex(mac);

  try {
    const ikm = getMessageIKM(mekp);

    const macCheck = HMAC_SHA256(
      messageIdHex + dataHex + ivHex,
      await HKDF_SHA256(ikm, 16, bytesToHex('authentication'), messageIdHex)
    );

    if (!safeCompare(macHex, macCheck)) {
      throw new Error('mac error');
    }

    const decryptedData = DECRYPT_AES_CTR(
      dataHex,
      await HKDF_SHA256(ikm, 16, bytesToHex('encryption'), messageIdHex),
      ivHex
    );

    return messageDataSchema.parse(
      JSON.parse(decodeUtf8(hexToBytes(decryptedData)))
    );
  } catch (error) {
    console.error('error decrypting messages', error);
    return null;
  }
};
