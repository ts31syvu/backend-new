import parsePhoneNumber from 'libphonenumber-js';
import { SHA256, bytesToHex, hexToInt8, int8ToHex } from '@lucaapp/crypto';

const removeAcademicTitles = name =>
  name.replace(/prof\. /gi, '').replace(/dr\. /gi, '');

export const removeMultipleNames = name =>
  name.slice(
    0,
    Math.max(0, name.includes(' ') ? name.indexOf(' ') : name.length)
  );

const simplify = name => name.toUpperCase().replace(/[^A-Z]/g, '');

const getSimplifiedFirstName = firstName =>
  simplify(removeMultipleNames(removeAcademicTitles(firstName))).trim();

const getSimplifiedLastName = lastName => simplify(lastName).trim();

export const getNameTerm = (firstName, lastName) =>
  `${getSimplifiedFirstName(firstName)} ${getSimplifiedLastName(lastName)}`;

export const getPhoneTerm = phoneNumber =>
  parsePhoneNumber(phoneNumber, 'DE').format('E.164');

const getHashPrefix = hash => {
  const trimmedHash = hash.slice(0, 6);
  const lastByte = hexToInt8(trimmedHash.slice(4, 6));
  // eslint-disable-next-line no-bitwise
  const maskedLastByte = lastByte & 0xe0;
  return `${trimmedHash.slice(0, 4)}${int8ToHex(maskedLastByte)}`;
};

export const getHashAndPrefix = ({ firstName, lastName, phone }) => {
  if (firstName && lastName) {
    const term = getNameTerm(firstName, lastName);
    const hash = SHA256(bytesToHex(term));
    const prefix = getHashPrefix(hash);
    return {
      hash,
      prefix,
    };
  }

  const term = getPhoneTerm(phone);
  const hash = SHA256(bytesToHex(term));
  const prefix = getHashPrefix(hash);
  return {
    hash,
    prefix,
  };
};
