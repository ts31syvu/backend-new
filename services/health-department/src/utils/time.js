import moment from 'moment';

export const DATE_FORMAT = 'DD.MM.YYYY';
export const TIME_FORMAT = 'HH:mm';

export const getFormattedDate = timestamp =>
  moment.unix(timestamp).format(DATE_FORMAT);

export const isFormattedDateValid = timestamp => {
  if (!timestamp) return false;
  return moment(moment.unix(timestamp), DATE_FORMAT, true).isValid();
};

export const getFormattedTime = timestamp =>
  moment.unix(timestamp).format(TIME_FORMAT);

export const isFormattedTimeValid = timestamp => {
  if (!timestamp) return false;
  return moment(moment.unix(timestamp), TIME_FORMAT, true).isValid();
};

export const getFormattedDateTime = timestamp =>
  `${getFormattedDate(timestamp)} ${getFormattedTime(timestamp)}`;

export const formattedTimeLabel = (
  timestamp,
  format = `${DATE_FORMAT} - ${TIME_FORMAT}`
) => {
  return `${moment.unix(timestamp).format(format)}`;
};

export const sortByTimeAsc = locations =>
  locations.sort((a, b) => {
    if (a.time[0] === b.time[0]) return 0;
    return a.time[0] < b.time[0] ? 1 : -1;
  });

export const formatDateTimeframe = timeframe =>
  `${moment.unix(timeframe[0]).format('DD.MM.YYYY HH:mm')} - ${moment
    .unix(timeframe[1])
    .format('DD.MM.YYYY HH:mm')}`;
