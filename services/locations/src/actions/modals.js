// Action Types
export const OPEN_MODAL = 'OPEN_MODAL';
export const CLOSE_MODAL = 'CLOSE_MODAL';
export const EDIT_MODAL = 'EDIT_MODAL';

// Actions
export const openModal = ({
  title,
  content,
  closable = true,
  emphasis = '',
}) => {
  return {
    type: OPEN_MODAL,
    payload: {
      title,
      content,
      closable,
      emphasis,
    },
  };
};

// Actions
export const editModal = ({ title, closable = true, emphasis = '' }) => {
  return {
    type: EDIT_MODAL,
    payload: {
      title,
      closable,
      emphasis,
    },
  };
};

export const closeModal = () => {
  return {
    type: CLOSE_MODAL,
  };
};
