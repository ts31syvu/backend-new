// SVGs
import LucaLogoWhiteIconSVG from './LucaLogoWhiteIcon.svg';
import LucaLogoBlackIconSVG from './LucaLogoBlackIcon.svg';
import EditBlackIconSVG from './EditBlackIcon.svg';
import EditWhiteIconSVG from './EditWhiteIcon.svg';
import BinIconSVG from './BinIcon.svg';
import ErrorIconSVG from './ErrorIcon.svg';
import QrPrintLogoIconSVG from './QrPrintLogoIcon.svg';
import NewsIconSVG from './NewsIcon.svg';
import MobileUsageIconSVG from './MobileUsageIcon.svg';
import ErrorBrowserIconSVG from './ErrorBrowserIcon.svg';

// Icons
export { ReactComponent as DownloadIcon } from './DownloadIcon.svg';
export { ReactComponent as ExternalLinkIcon } from './ExternalLinkIcon.svg';
export { ReactComponent as AddCircleBlueIcon } from './AddCircleBlueIcon.svg';
export { ReactComponent as ArrowDownBlueIcon } from './ArrowDownBlueIcon.svg';
export { ReactComponent as ArrowRightBlueIcon } from './ArrowRightBlueIcon.svg';
export { ReactComponent as BinIcon } from './BinIcon.svg';
export { ReactComponent as ContactFormIcon } from './ContactFormIcon.svg';
export { ReactComponent as CreateGroupIcon } from './CreateGroupIcon.svg';
export { ReactComponent as DataRequestsDefaultIcon } from './DataRequestsDefaultIcon.svg';
export { ReactComponent as DataRequestsActiveIcon } from './DataRequestsActiveIcon.svg';
export { ReactComponent as DeviceActiveIcon } from './DeviceActiveIcon.svg';
export { ReactComponent as DeviceDefaultIcon } from './DeviceDefaultIcon.svg';
export { ReactComponent as EditBlackIcon } from './EditBlackIcon.svg';
export { ReactComponent as EditWhiteIcon } from './EditWhiteIcon.svg';
export { ReactComponent as ErrorIcon } from './ErrorIcon.svg';
export { ReactComponent as ErrorBrowserIcon } from './ErrorBrowserIcon.svg';
export { ReactComponent as ExternalIcon } from './ExternalIcon.svg';
export { ReactComponent as HandScannerIcon } from './HandScannerIcon.svg';
export { ReactComponent as HelpCenterActiveIcon } from './HelpCenterActiveIcon.svg';
export { ReactComponent as HelpCenterDefaultIcon } from './HelpCenterDefaultIcon.svg';
export { ReactComponent as KeycardIcon } from './KeycardIcon.svg';
export { ReactComponent as LockIcon } from './LockIcon.svg';
export { ReactComponent as LucaLogoPaddingIcon } from './LucaLogoPaddingIcon.svg';
export { ReactComponent as LucaLogoBlackIcon } from './LucaLogoBlackIcon.svg';
export { ReactComponent as LucaLogoWhiteIcon } from './LucaLogoWhiteIcon.svg';
export { ReactComponent as MenuActiveIcon } from './MenuActiveIcon.svg';
export { ReactComponent as MenuDefaultIcon } from './MenuDefaultIcon.svg';
export { ReactComponent as MobileUsageIcon } from './MobileUsageIcon.svg';
export { ReactComponent as NewsIcon } from './NewsIcon.svg';
export { ReactComponent as OperatorAppIOSIcon } from './OperatorAppIOSIcon.svg';
export { ReactComponent as OperatorAppAndroidIcon } from './OperatorAppAndroidIcon.svg';
export { ReactComponent as PenIcon } from './PenIcon.svg';
export { ReactComponent as PhoneIcon } from './PhoneIcon.svg';
export { ReactComponent as SearchIcon } from './SearchIcon.svg';
export { ReactComponent as QrPrintLogoIcon } from './QrPrintLogoIcon.svg';
export { ReactComponent as RefreshIcon } from './RefreshIcon.svg';
export { ReactComponent as ReturnIcon } from './Return.svg';
export { ReactComponent as TabletIcon } from './TabletIcon.svg';
export { ReactComponent as VerificationIcon } from './VerificationIcon.svg';
export { ReactComponent as WarningIcon } from './WarningIcon.svg';
export { ReactComponent as BinBlueIcon } from './BinBlueIcon.svg';
export { ReactComponent as CertificateBlackIcon } from './CertificateBlackIcon.svg';
export { ReactComponent as QuestionmarkBlueIcon } from './QuestionmarkBlueIcon.svg';
export { ReactComponent as MobilePhoneIcon } from './MobilePhoneIcon.svg';
export { ReactComponent as OtherBlackIcon } from './OtherBlackIcon.svg';
export { ReactComponent as GastroBlackIcon } from './GastroBlackIcon.svg';
export { ReactComponent as HotelBlackIcon } from './HotelBlackIcon.svg';
export { ReactComponent as NursingBlackIcon } from './NursingBlackIcon.svg';
export { ReactComponent as StoreBlackIcon } from './StoreBlackIcon.svg';
export { ReactComponent as LensBlackIcon } from './LensIcon.svg';
export { ReactComponent as CheckIcon } from './CheckIcon.svg';
export { ReactComponent as HouseBlackIcon } from './HouseBlackIcon.svg';
export { ReactComponent as RoomBlackIcon } from './RoomBlackIcon.svg';
export { ReactComponent as DisabledContactFormIcon } from './DisabledContactFormIcon.svg';
export { ReactComponent as DisabledTabletIcon } from './DisabledTabletIcon.svg';
export { ReactComponent as DisabledHandScannerIcon } from './DisabledHandScannerIcon.svg';

export {
  LucaLogoWhiteIconSVG,
  LucaLogoBlackIconSVG,
  EditBlackIconSVG,
  EditWhiteIconSVG,
  BinIconSVG,
  QrPrintLogoIconSVG,
  NewsIconSVG,
  ErrorIconSVG,
  MobileUsageIconSVG,
  ErrorBrowserIconSVG,
};
