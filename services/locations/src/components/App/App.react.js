import React from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';
import { Route, Switch, Redirect, useHistory, useLocation } from 'react-router';
import { useQueryClient } from 'react-query';
import { usePrivateKey } from 'utils/privateKey';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { useGetMe } from 'components/hooks/queries';

import {
  LOGIN_ROUTE,
  PROFILE_ROUTE,
  ARCHIVE_ROUTE,
  HELP_CENTER_ROUTE,
  APP_ROUTE,
  GROUP_SETTINGS_ROUTE,
  BASE_DATA_TRANSFER_ROUTE,
  DEVICES_ROUTE,
} from 'constants/routes';

import { ModalArea } from 'components/App/modals/ModalArea';
import { Header } from './Header';
import { Profile } from './Profile';
import { Archive } from './Archive';
import { Devices } from './Devices';
import { Dashboard } from './Dashboard';
import { HelpCenter } from './HelpCenter';
import { GroupSettings } from './GroupSettings';
import { DataTransfers } from './DataTransfers';

import { AppWrapper } from './App.styled';
import { HistoryContextProvider } from '../context/HistoryContext';

export const App = () => {
  const intl = useIntl();
  const history = useHistory();
  const { pathname } = useLocation();
  const queryClient = useQueryClient();
  const [, clearPrivateKey] = usePrivateKey(null);

  const {
    isLoading: isOperatorLoading,
    error: operatorError,
    data: operator,
    refetch,
  } = useGetMe();

  if (operatorError) {
    queryClient.clear();
    clearPrivateKey(null);
    clearHasSeenPrivateKeyModal();

    const location = `${LOGIN_ROUTE}?redirectTo=${pathname}`;
    history.push(location);
  }

  if (isOperatorLoading || operatorError) return null;

  return (
    <HistoryContextProvider>
      <Helmet>
        <title>
          {intl.formatMessage({
            id: 'locations.site.title',
          })}
        </title>
        <meta
          name="description"
          content={intl.formatMessage({
            id: 'locations.site.meta',
          })}
        />
      </Helmet>
      <AppWrapper>
        <Header />
        <ModalArea />
        <Switch>
          <Route path={PROFILE_ROUTE}>
            <Profile operator={operator} refetch={refetch} />
          </Route>
          <Route path={ARCHIVE_ROUTE}>
            <Archive />
          </Route>
          <Route path={DEVICES_ROUTE}>
            <Devices />
          </Route>
          <Route path={HELP_CENTER_ROUTE}>
            <HelpCenter operator={operator} />
          </Route>
          <Route path={GROUP_SETTINGS_ROUTE}>
            <GroupSettings operator={operator} />
          </Route>
          <Route path={BASE_DATA_TRANSFER_ROUTE}>
            <DataTransfers />
          </Route>
          <Route path={APP_ROUTE}>
            <Dashboard operator={operator} />
          </Route>
          <Redirect to={APP_ROUTE} />
        </Switch>
      </AppWrapper>
    </HistoryContextProvider>
  );
};
