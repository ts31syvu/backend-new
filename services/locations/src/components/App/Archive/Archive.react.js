import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';
import { NavigationButton } from 'components/general';

import { LocationFooter } from 'components/App/LocationFooter';

import { ArchiveContent } from './ArchiveContent';
import { contentStyles, sliderStyles, Wrapper, Header } from './Archive.styled';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';

const { Content, Sider } = Layout;

export const Archive = () => {
  const intl = useIntl();
  return (
    <Layout>
      <Sider style={sliderStyles}>
        <NavigationButton />
        <LanguageSwitcher />
      </Sider>
      <Layout>
        <Content style={contentStyles}>
          <Wrapper>
            <Header>{intl.formatMessage({ id: 'header.archive' })}</Header>

            <ArchiveContent />
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
