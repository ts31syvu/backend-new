import React from 'react';
import { useIntl } from 'react-intl';

import { DeletedGroupsList } from './DeletedGroupsList';
import { Wrapper, StyledCard, Info } from './ArchiveContent.styled';

export const ArchiveContent = () => {
  const intl = useIntl();
  return (
    <Wrapper>
      <StyledCard>
        <Info>{intl.formatMessage({ id: 'archive.info' })}</Info>
        <DeletedGroupsList />
      </StyledCard>
    </Wrapper>
  );
};
