import React from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { notification } from 'antd';

import { reactivateGroup } from 'network/api';
import { QUERY_KEYS } from 'components/hooks/queries';

import { getFormattedDate } from 'utils/time';
import { PrimaryButton } from 'components/general';

import { StyledTable, Wrapper } from './DeletedGroupsTable.styled';

export const DeletedGroupsTable = ({ deletedGroups }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const showError = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'notification.deleteGroup.error',
      }),
    });

  const reactivate = groupId => {
    reactivateGroup(groupId)
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(QUERY_KEYS.DELETED_GROUPS);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.reactivateGroup.success',
            }),
          });
          return;
        }
        showError();
      })
      .catch(showError);
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'archive.name' }),
      key: 'name',
      render: function renderName(deletedGroup) {
        return <div>{deletedGroup.name}</div>;
      },
    },
    {
      title: intl.formatMessage({ id: 'archive.availableUntil' }),
      key: 'availableUntil',
      render: function renderAvailableUntil(deletedGroup) {
        return <div>{getFormattedDate(deletedGroup.availableUntil)}</div>;
      },
    },
    {
      title: '',
      key: 'reactivate',
      render: function renderReactivationButton(deletedGroup) {
        return (
          <Wrapper>
            <PrimaryButton onClick={() => reactivate(deletedGroup.groupId)}>
              {intl.formatMessage({ id: 'archive.reactivate' })}
            </PrimaryButton>
          </Wrapper>
        );
      },
    },
  ];

  return (
    <StyledTable
      columns={columns}
      dataSource={deletedGroups}
      rowKey={record => record.groupId}
      pagination={false}
    />
  );
};
