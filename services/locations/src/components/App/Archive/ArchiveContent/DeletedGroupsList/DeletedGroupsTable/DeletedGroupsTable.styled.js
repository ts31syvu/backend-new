import styled from 'styled-components';
import { Table } from 'antd';

export const StyledTable = styled(Table)`
  width: 100%;
  .ant-table-thead > tr > th {
    background: white;
    font-family: Montserrat-SemiBold, sans-serif;
    font-size: 14px;
    font-weight: 600;
  }
`;

export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
