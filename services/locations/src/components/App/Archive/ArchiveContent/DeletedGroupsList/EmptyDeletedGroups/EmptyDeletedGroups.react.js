import React from 'react';
import { useIntl } from 'react-intl';

import { Info } from './EmptyDeletedGroups.styled';

export const EmptyDeletedGroups = () => {
  const intl = useIntl();
  return (
    <Info>{intl.formatMessage({ id: 'archive.emptyDeletedGroups' })}</Info>
  );
};
