import React, { useEffect, useCallback } from 'react';
import { Redirect, Route, Switch, useHistory } from 'react-router';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';

import { useWhatsNew } from 'components/hooks/useWhatsNew';

import {
  LOGIN_ROUTE,
  BASE_GROUP_ROUTE,
  GROUP_ROUTE,
  LOCATION_ROUTE,
} from 'constants/routes';
import { useGetPrivateKeySecret } from 'components/hooks/queries';
import {
  hasSeenPrivateKeyModal,
  setHasSeenPrivateKeyModal,
} from 'utils/storage';
import { usePrivateKey, getRemainingDaysToUploadKey } from 'utils/privateKey';

import { useModal } from 'components/hooks/useModal';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { GroupList } from 'components/App/GroupList';
import { RegisterOperatorModal } from 'components/App/modals/RegisterOperatorModal';
import { BusinessAddressModal } from 'components/App/modals/BusinessAddressModal';
import {
  SecondaryButton,
  siderStyles,
  contentStyles,
} from 'components/general';

import { Location } from './Location';
import { EmptyGroup } from './EmptyGroup';
import { DeletionMessageLayout } from './DeletionMessage/DeletionMessage.react';
import { LanguageSwitcher } from './LanguageSwitcher';

const { Content, Sider } = Layout;

const KEY_UPLOAD_THRESHOLD = 4;

export const Dashboard = ({ operator }) => {
  const intl = useIntl();
  const history = useHistory();

  const [openModal, closeModal] = useModal();
  const { avvAccepted } = useWhatsNew(operator);

  const isBusinessEmpty = useCallback(() => {
    return (
      !operator?.businessEntityName ||
      !operator?.businessEntityStreetName ||
      !operator?.businessEntityStreetNumber ||
      !operator?.businessEntityZipCode ||
      !operator?.businessEntityCity
    );
  }, [operator]);

  const {
    data: privateKeySecret,
    isLoading: isPrivateKeyLoading,
    error,
  } = useGetPrivateKeySecret();

  const [privateKey] = usePrivateKey(privateKeySecret);

  const { isTrusted: isOperatorTrusted, lastSeenPrivateKey } = operator;

  const remainingDaysToUploadKey = getRemainingDaysToUploadKey(
    lastSeenPrivateKey
  );

  const shouldOpenKeyLoader = useCallback(
    () =>
      !privateKey &&
      avvAccepted &&
      !isPrivateKeyLoading &&
      !hasSeenPrivateKeyModal() &&
      remainingDaysToUploadKey < KEY_UPLOAD_THRESHOLD,
    [privateKey, avvAccepted, isPrivateKeyLoading, remainingDaysToUploadKey]
  );

  useEffect(() => {
    if (isPrivateKeyLoading || error) return;
    if (!operator) {
      history.push(LOGIN_ROUTE);
    }

    if (isBusinessEmpty()) {
      openModal({
        content: <BusinessAddressModal closeModal={closeModal} />,
        closable: false,
      });
    }

    if (operator && !operator.publicKey && !isBusinessEmpty()) {
      openModal({
        content: (
          <RegisterOperatorModal
            privateKeySecret={privateKeySecret}
            operator={operator}
          />
        ),
        closable: false,
      });
    } else if (shouldOpenKeyLoader() && !isBusinessEmpty()) {
      setHasSeenPrivateKeyModal(true);
      openModal({
        content: (
          <PrivateKeyLoader
            publicKey={operator.publicKey}
            showSuccessScreen
            footerItem={
              remainingDaysToUploadKey > 0 && (
                <SecondaryButton
                  type="link"
                  onClick={closeModal}
                  data-cy="skipPrivateKeyUpload"
                >
                  {intl.formatMessage({ id: 'privateKey.modal.skip' })}
                </SecondaryButton>
              )
            }
            remainingDaysToUploadKey={remainingDaysToUploadKey}
            privateKeySecret={privateKeySecret}
            operator={operator}
          />
        ),
        closable: remainingDaysToUploadKey > 0,
      });
    }
  }, [
    isPrivateKeyLoading,
    error,
    operator,
    history,
    openModal,
    privateKeySecret,
    remainingDaysToUploadKey,
    closeModal,
    intl,
    isBusinessEmpty,
    shouldOpenKeyLoader,
  ]);

  if (operator.deletedAt) {
    return <DeletionMessageLayout operator={operator} />;
  }

  return (
    <Layout>
      <Sider width={300} style={siderStyles}>
        <GroupList />
        <LanguageSwitcher />
      </Sider>
      <Content style={contentStyles}>
        <Switch>
          <Route path={LOCATION_ROUTE}>
            <Location isOperatorTrusted={isOperatorTrusted} />
          </Route>
          <Route path={GROUP_ROUTE}>
            <EmptyGroup />
          </Route>
          <Route path={BASE_GROUP_ROUTE}>
            <EmptyGroup />
          </Route>
          <Redirect to={BASE_GROUP_ROUTE} />
        </Switch>
      </Content>
    </Layout>
  );
};
