import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton } from 'components/general';

import { CreateGroupModal } from 'components/App/modals/CreateGroupModal';

import { ButtonWrapper } from './CreateGroup.styled';

export const CreateGroup = () => {
  const intl = useIntl();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const onCloseModal = () => setIsModalOpen(false);
  const onOpenModal = () => setIsModalOpen(true);

  return (
    <>
      <ButtonWrapper>
        <PrimaryButton onClick={onOpenModal} data-cy="createGroup">
          {intl.formatMessage({ id: 'groupList.createGroup' })}
        </PrimaryButton>
      </ButtonWrapper>
      {isModalOpen && <CreateGroupModal onClose={onCloseModal} />}
    </>
  );
};
