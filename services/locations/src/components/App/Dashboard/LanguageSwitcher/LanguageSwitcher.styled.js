import styled from 'styled-components';

import { Button, Dropdown } from 'antd';

export const StyledButton = styled(Button)`
  background: none;
  border: 0;
  bottom: 2%;
  box-shadow: none;
  font-weight: bold;
  left: 0;
  margin: auto;
  position: absolute;
  right: 0;

  &:hover,
  &:focus {
    color: #000;
    background: none;
    border: none;
  }
`;

export const StyledDropdown = styled(Dropdown)`
  width: 70px;
`;
