export const getIsVisited = location => {
  return (
    location.ventilation ??
    location.masks ??
    location.entryPolicyInfo ??
    location.roomDepth ??
    location.roomHeight ??
    location.roomHeight
  );
};
