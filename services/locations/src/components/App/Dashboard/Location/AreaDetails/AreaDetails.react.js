import React from 'react';

import { LocationCard } from '../LocationCard';
import {
  SelectAreaMedicalProtection,
  SelectAreaType,
  SelectRoomSize,
  SelectEntryPolicyInfo,
  SelectRoomVentilation,
} from './sections';
import {
  AreaDetailsCardSection,
  HighlightStyledContainer,
} from './AreaDetails.styled';
import { AreaDetailsProvider } from './context';
import { getIsVisited } from './AreaDetails.helper';
import { useTranslation } from './hooks';

export const AreaDetails = ({ location }) => {
  const translate = useTranslation('card');

  const isVisited = getIsVisited(location);

  const highlightContainer = !isVisited ? HighlightStyledContainer : null;

  return (
    <LocationCard
      description={translate('description')}
      isCollapse
      title={translate('title')}
      component={highlightContainer}
      testId="areaDetails"
    >
      <AreaDetailsCardSection>
        <AreaDetailsProvider isVisited={isVisited} location={location}>
          <SelectAreaType />
          <SelectAreaMedicalProtection />
          <SelectEntryPolicyInfo />
          <SelectRoomVentilation />
          <SelectRoomSize />
        </AreaDetailsProvider>
      </AreaDetailsCardSection>
    </LocationCard>
  );
};
