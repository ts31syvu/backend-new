import styled from 'styled-components';

import { CardSection } from '../LocationCard';
import { StyledContainer } from '../LocationCard/LocationCard.styled';

export const StyledFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: baseline;
  justify-content: left;
  gap: 10px;

  span,
  input[disabled] {
    background-color: #fff !important;
  }
`;

export const AreaDetailsCardSection = styled(CardSection)`
  padding: 0;
  border: none;
`;

export const HighlightStyledContainer = styled(StyledContainer)`
  border: 3px solid rgb(129, 158, 87);
  border-radius: 8px;
`;
