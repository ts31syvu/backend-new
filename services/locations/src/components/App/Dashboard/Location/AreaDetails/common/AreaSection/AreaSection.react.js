import React from 'react';

import { CardSectionDescription } from 'components/App/Dashboard/Location/LocationCard';
import { CardSection } from 'components/general';
import { StyledAreaSection } from './AreaSection.styled';
import { StyledFooter } from '../../AreaDetails.styled';

export const AreaSection = ({
  children,
  description,
  icon: Icon,
  title,
  tooltip,
}) => {
  return (
    <StyledAreaSection>
      <CardSection title={title} tooltip={tooltip}>
        {Icon && <Icon />}
      </CardSection>
      <CardSectionDescription>{description}</CardSectionDescription>
      <StyledFooter>{children}</StyledFooter>
    </StyledAreaSection>
  );
};
