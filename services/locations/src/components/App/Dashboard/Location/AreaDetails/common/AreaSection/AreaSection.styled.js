import styled from 'styled-components';

export const StyledAreaSection = styled.div`
  border-top: 1px solid rgb(151, 151, 151);
  padding: 24px 32px 40px 32px;
  width: 100%;

  &:last-child {
    padding-bottom: 10px;
  }
`;
