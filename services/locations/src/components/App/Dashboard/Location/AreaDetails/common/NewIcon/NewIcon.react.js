import React from 'react';
import { useTranslation } from '../../hooks';

import { StyledNewIcon } from './NewIcon.styled';

export const NewIcon = () => {
  const translate = useTranslation('areaType');

  return <StyledNewIcon>{translate('new')}</StyledNewIcon>;
};
