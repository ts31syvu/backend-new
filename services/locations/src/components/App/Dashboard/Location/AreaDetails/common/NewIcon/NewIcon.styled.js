import styled from 'styled-components';

export const StyledNewIcon = styled.span`
  background-color: rgb(211, 222, 195);
  border-radius: 8px;
  padding: 0 10px;
  font-size: 11px;
  margin-left: 10px;
  font-weight: bold;
  animation-name: color;
  animation-duration: 2s;
  animation-iteration-count: infinite;
  text-transform: uppercase;

  @keyframes color {
    0% {
      background-color: rgb(211, 222, 195);
    }
    50% {
      background-color: rgb(241, 246, 234);
    }
    100% {
      background-color: rgb(211, 222, 195);
    }
  }
`;
