import React, { useEffect } from 'react';
import { Col, Form } from 'antd';
import { useFormikContext } from 'formik';

import { useAreaDetails } from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { AppFormField } from 'components/general/Form';
import { StyledInput } from './StyledInput.styled';

export const RoomSizeInputField = ({ label, name, placeholder }) => {
  const { area } = useAreaDetails();
  const { resetForm } = useFormikContext();

  const isOutdoor = !area.isIndoor;

  useEffect(() => {
    if (!isOutdoor) return;

    resetForm();
  }, [isOutdoor, resetForm]);

  return (
    <Col flex="1">
      <Form.Item label={label} name={name}>
        <StyledInput isOutdoor={isOutdoor}>
          <AppFormField
            disabled={isOutdoor}
            name={name}
            placeholder={placeholder}
            suffix="m"
          />
        </StyledInput>
      </Form.Item>
    </Col>
  );
};
