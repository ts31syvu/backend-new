import React, { useState } from 'react';
import { updateLocation } from 'network/api';
import { Form } from 'antd';
import isEqual from 'lodash/isEqual';

import { AreaDetailsContext } from './AreaDetailsContext.react';
import { useSelectOptionsFromJSON, useToastify } from '../hooks';
import { NewIcon } from '../common';

export const AreaDetailsProvider = ({ children, isVisited, location }) => {
  const toast = useToastify();
  const [form] = Form.useForm();
  const options = useSelectOptionsFromJSON();

  const [area, setArea] = useState(location);

  const dataAlreadyExists = data => {
    const existingObject = Object.entries(data).reduce((accumulator, [key]) => {
      if (key in area) accumulator[key] = area[key];

      return accumulator;
    }, {});

    return isEqual(data, existingObject);
  };

  const updateAreaDetails = async data => {
    if (dataAlreadyExists(data)) return;

    try {
      const newData = {
        locationId: area.uuid,
        data,
      };

      const response = await updateLocation(newData);
      const body = await response.json();

      setArea(body);
      toast.success();
    } catch {
      toast.error();
    }
  };

  const icon = !isVisited ? NewIcon : null;

  const values = {
    area,
    form,
    icon,
    options,
    updateAreaDetails,
  };

  return (
    <AreaDetailsContext.Provider value={values}>
      {children}
    </AreaDetailsContext.Provider>
  );
};
