import { useIntl } from 'react-intl';

import { isJSON } from 'utils/checkDataFormat';

export const useSelectOptionsFromJSON = () => {
  const intl = useIntl();

  const getOptions = key => {
    const options = intl.formatMessage({
      id: `settings.location.areaDetails.${key}.options`,
    });

    if (!isJSON(options)) {
      return [];
    }

    return JSON.parse(options);
  };

  return { getOptions };
};
