import { useIntl } from 'react-intl';
import { notification } from 'antd';

export const useToastify = () => {
  const intl = useIntl();

  const path = 'notification.updateLocation';

  const getMessage = key => {
    return intl.formatMessage({
      id: `${path}.${key}`,
    });
  };

  const success = () => {
    const message = getMessage('success');

    notification.success({ className: 'editLocationSuccess', message });
  };

  const error = () => {
    const message = getMessage('error');

    notification.error({ message });
  };

  return { success, error };
};
