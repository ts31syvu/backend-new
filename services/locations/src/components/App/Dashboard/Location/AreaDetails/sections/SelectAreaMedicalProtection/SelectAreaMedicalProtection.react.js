import React from 'react';

import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';
import { useOptions } from 'components/App/modals/generalOnboarding/AreaDetails/common/useOptions';
import { SelectInput } from 'components/App/modals/generalOnboarding/AreaDetails/common/SelectInput';
import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { Form } from 'antd';

export const SelectAreaMedicalProtection = () => {
  const { form, area, icon, updateAreaDetails } = useAreaDetails();
  const translate = useTranslation('medicalGear.masks');

  const handleAreaMedicalProtectionChange = value => {
    const data = {
      masks: value,
    };

    updateAreaDetails(data);
  };

  const { medicalProtectionOptions } = useOptions();

  return (
    <Form
      form={form}
      style={{ width: '100%' }}
      initialValues={{ masks: area.masks ?? medicalProtectionOptions[0].value }}
    >
      <AreaSection
        description={translate('description')}
        icon={icon}
        title={translate('title')}
      >
        <SelectInput
          dataCy="dashboard-medicalMasksSelection"
          name="masks"
          handleSelect={handleAreaMedicalProtectionChange}
          options={medicalProtectionOptions}
        />
      </AreaSection>
    </Form>
  );
};
