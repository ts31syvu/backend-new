import React from 'react';
import { transform } from 'lodash';

import { AreaSection } from 'components/App/Dashboard/Location/AreaDetails/common';
import { RoomSizeInputField } from 'components/App/Dashboard/Location/AreaDetails/common/form';
import {
  useAreaDetails,
  useTranslation,
} from 'components/App/Dashboard/Location/AreaDetails/hooks';
import { useRoomSizeValidationSchema } from 'components/App/Dashboard/Location/AreaDetails/hooks/formValidationSchemas';
import { AppForm, AutoSubmitForm } from 'components/general/Form';

export const SelectRoomSize = () => {
  const translate = useTranslation('roomSize');
  const validationSchema = useRoomSizeValidationSchema();
  const { area, form, icon, updateAreaDetails } = useAreaDetails();

  const handleSubmit = data => {
    const castToNumber = (accumulator, value, key) => {
      accumulator[key] = Number(value);
    };

    const roomSize = transform(data, castToNumber, {});

    updateAreaDetails(roomSize);
  };

  const { roomWidth, roomHeight, roomDepth } = area;

  const initialRoomSize = {
    roomDepth: roomDepth ?? '',
    roomHeight: roomHeight ?? '',
    roomWidth: roomWidth ?? '',
  };

  return (
    <>
      <AppForm
        form={form}
        initialValues={initialRoomSize}
        onSubmit={handleSubmit}
        validationSchema={validationSchema}
        enableReinitialize
      >
        <AreaSection
          description={translate('description')}
          icon={icon}
          title={translate('title')}
        >
          <RoomSizeInputField
            label={translate('width.label')}
            name="roomWidth"
            placeholder="0.0"
          />
          <RoomSizeInputField
            label={translate('depth.label')}
            name="roomDepth"
            placeholder="0.0"
          />
          <RoomSizeInputField
            label={translate('height.label')}
            name="roomHeight"
            placeholder="0.0"
          />
          <AutoSubmitForm wait={0.5} resetErrors={!area.isIndoor} />
        </AreaSection>
      </AppForm>
    </>
  );
};
