import React from 'react';
import { useIntl } from 'react-intl';

import { BinBlueIcon } from 'assets/icons';

import { useAdditionalDataValidator } from 'components/hooks/useValidators';
import Icon from '@ant-design/icons';
import {
  StyledButton,
  StyledInputContainer,
  StyledForm,
  StyledFormItem,
  StyledTextArea,
} from './AdditionalData.styled';

export const AdditionalData = ({
  additionalData,
  information,
  removeAdditionalData,
  onBlur,
}) => {
  const intl = useIntl();
  const additionalDataValidator = useAdditionalDataValidator(
    'additionalData',
    additionalData
  );

  return (
    <StyledForm>
      <StyledFormItem
        name="key"
        colon={false}
        label={intl.formatMessage({
          id: 'settings.location.checkin.additionalData.input',
        })}
        onBlur={event => onBlur(event, information.uuid)}
        rules={additionalDataValidator}
      >
        <StyledInputContainer>
          <StyledTextArea
            rows={3}
            defaultValue={information.key}
            placeholder={intl.formatMessage({
              id: 'settings.location.checkin.additionalData.inputPlaceholder',
            })}
          />
          <StyledButton
            data-cy="removeAdditionalData"
            onClick={() => removeAdditionalData(information.uuid)}
          >
            <Icon component={BinBlueIcon} />
          </StyledButton>
        </StyledInputContainer>
      </StyledFormItem>
    </StyledForm>
  );
};
