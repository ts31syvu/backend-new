import { Button, Form, Input } from 'antd';
import styled from 'styled-components';

const { TextArea } = Input;

export const StyledInputContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;

export const StyledButton = styled(Button)`
  border: none;
  outline: none;
`;

export const StyledForm = styled(Form)`
  width: 100%;
`;

export const StyledFormItem = styled(Form.Item)`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  font-size: 14px;
`;

export const StyledTextArea = styled(TextArea)`
  border: 0.5px solid rgb(0, 0, 0);
  border-radius: 0;
  padding: 16px;

  &:hover {
    border: 0.5px solid rgb(0, 0, 0);
  }
`;
