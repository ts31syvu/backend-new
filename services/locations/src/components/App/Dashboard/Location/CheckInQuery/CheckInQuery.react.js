import React, { useCallback, useEffect, useState } from 'react';
import { notification } from 'antd';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import {
  createAdditionalData,
  deleteAdditionalData,
  updateAdditionalData,
} from 'network/api';
import { useGetAdditionalData } from 'components/hooks/queries';
import { isValidCharacter } from 'utils/checkCharacter';
import { AddCircleBlueIcon } from 'assets/icons';
import {
  StyledFooter,
  StyledIconButton,
  StyledIconButtonText,
} from './CheckInQuery.styled';
import { AdditionalData } from './AdditionalData';
import {
  CardSection,
  CardSectionDescription,
  LocationCard,
} from '../LocationCard';

export const CheckInQuery = ({ location }) => {
  const intl = useIntl();
  const [additionalData, setAdditionalData] = useState([]);
  const { isLoading, error, data, refetch } = useGetAdditionalData(
    location.uuid
  );

  const addAdditionalData = useCallback(() => {
    createAdditionalData(location.uuid, {
      key: '',
    })
      .then(response => {
        if (response.status === 403)
          notification.error({
            message: intl.formatMessage({
              id:
                'settings.location.checkin.additionalData.notification.maxReachedError.title',
            }),
            description: intl.formatMessage({
              id:
                'settings.location.checkin.additionalData.notification.maxReachedError.desc',
            }),
          });
        refetch();
      })
      .catch(refetch);
  }, [location.uuid, intl, refetch]);

  const removeAdditionalData = useCallback(
    id => {
      deleteAdditionalData(id).then(refetch).catch(refetch);
    },
    [refetch]
  );

  useEffect(() => {
    if (isLoading || error) return;

    setAdditionalData(data.additionalData);
  }, [isLoading, data, error, additionalData.length]);

  const handleUpdateAdditionalData = (additionalDataId, value) => {
    updateAdditionalData(additionalDataId, {
      key: value,
      isRequired: true,
    })
      .then(() => {
        const newStateAdditionalInfo = additionalData.map(information =>
          information.uuid === additionalDataId
            ? { ...information, key: value }
            : information
        );

        setAdditionalData(newStateAdditionalInfo);
      })
      .catch(refetch);
  };

  const onBlur = (event, additionalDataId) => {
    const { value } = event.target;
    const keyAlreadyExists = additionalData.some(
      additionalInfo => additionalInfo.key === value
    );

    if (value.length === 0) {
      removeAdditionalData(additionalDataId);
      return;
    }

    if (
      value.startsWith('_') ||
      !value.trim() ||
      !isValidCharacter(value.trim()) ||
      keyAlreadyExists
    ) {
      return;
    }

    handleUpdateAdditionalData(additionalDataId, value);
  };

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'settings.location.checkin.headline' })}
      testId="additionalData"
    >
      <CardSection isLast>
        <CardSectionDescription>
          {intl.formatMessage({
            id: 'settings.location.checkin.additionalData.description',
          })}
        </CardSectionDescription>
        {additionalData.map(information => (
          <AdditionalData
            key={information.uuid}
            information={information}
            additionalData={additionalData}
            removeAdditionalData={removeAdditionalData}
            onBlur={onBlur}
          />
        ))}
        <StyledFooter>
          <StyledIconButton
            data-cy="addRequestButton"
            onClick={addAdditionalData}
          >
            <Icon component={AddCircleBlueIcon} style={{ fontSize: 16 }} />
            <StyledIconButtonText>
              {intl.formatMessage({
                id: 'settings.location.checkin.additionalData.add',
              })}
            </StyledIconButtonText>
          </StyledIconButton>
        </StyledFooter>
      </CardSection>
    </LocationCard>
  );
};
