import styled from 'styled-components';

export const StyledFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: flex-end;
`;

export const StyledIconButton = styled.div`
  display: flex;
  align-items: center;
  padding: 0 15px;
`;

export const StyledIconButtonText = styled.div`
  cursor: pointer;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-left: 5px;
`;
