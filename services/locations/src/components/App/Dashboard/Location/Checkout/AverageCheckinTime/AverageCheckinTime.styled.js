import styled from 'styled-components';
import { TimePicker } from 'antd';

export const Wrapper = styled.div`
  padding: 0 32px;
`;

export const PickerWrapper = styled.div``;

export const StyledTimePicker = styled(TimePicker)`
  width: 50%;
`;
