import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { InputNumber } from 'antd';
import React, { useCallback, useState } from 'react';

import { useCheckoutRadiusValidator } from 'components/hooks/useValidators';

import {
  DEFAULT_CHECKOUT_RADIUS,
  MAX_CHECKOUT_RADIUS,
} from 'constants/checkout';

import { updateLocation } from 'network/api';
import { Switch } from 'components/general/Switch';
import { InformationIcon, StyledTooltip } from 'components/general';
import { QUERY_KEYS } from 'components/hooks/queries';
import {
  CardSection,
  CardSectionDescription,
  CardSectionTitle,
} from '../../LocationCard';

import { StyledSwitchContainer } from '../../GenerateQRCodes/GenerateQRCodes.styled';
import { StyledForm, StyledFormItem } from './CheckoutRadius.styled';

export const CheckoutRadius = ({ location }) => {
  const intl = useIntl();
  const checkoutRadiusValidator = useCheckoutRadiusValidator();
  const queryClient = useQueryClient();
  const [isAutoCheckoutActive, setIsAutoCheckoutActive] = useState(
    location.radius >= DEFAULT_CHECKOUT_RADIUS
  );

  const refetch = useCallback(() => {
    queryClient.invalidateQueries([QUERY_KEYS.LOCATION, location.uuid]);
  }, [location, queryClient]);

  const onIsAutoCheckoutChanged = useCallback(() => {
    setIsAutoCheckoutActive(!isAutoCheckoutActive);
    if (isAutoCheckoutActive) {
      updateLocation({ locationId: location.uuid, data: { radius: 0 } })
        .then(refetch)
        .catch(refetch);
    } else {
      updateLocation({
        locationId: location.uuid,
        data: { radius: DEFAULT_CHECKOUT_RADIUS },
      })
        .then(refetch)
        .catch(refetch);
    }
  }, [isAutoCheckoutActive, location.uuid, refetch]);

  return (
    location.lat &&
    location.lng && (
      <CardSection isLast>
        <CardSectionTitle>
          {intl.formatMessage({ id: 'settings.location.checkout.title' })}
          <StyledTooltip
            title={intl.formatMessage({
              id: 'settings.location.checkout.tooltip',
            })}
          >
            <InformationIcon />
          </StyledTooltip>
          <StyledSwitchContainer>
            <Switch
              data-cy="activateCheckoutRadius"
              checked={isAutoCheckoutActive}
              onChange={onIsAutoCheckoutChanged}
            />
          </StyledSwitchContainer>
        </CardSectionTitle>
        <CardSectionDescription>
          {intl.formatMessage({
            id: 'settings.location.checkout.automatic.description',
          })}
        </CardSectionDescription>
        {isAutoCheckoutActive && location.radius > 0 && (
          <StyledForm
            step={1}
            initialValues={location}
            onValuesChange={({ radius }) =>
              updateLocation({
                locationId: location.uuid,
                data: { radius },
              })
            }
          >
            <StyledFormItem
              name="radius"
              label={intl.formatMessage({
                id: 'settings.location.checkout.automatic.radius',
              })}
              rules={checkoutRadiusValidator}
            >
              <InputNumber
                min={DEFAULT_CHECKOUT_RADIUS}
                max={MAX_CHECKOUT_RADIUS}
                placeholder={intl.formatMessage({
                  id: 'settings.location.checkout.automatic.radius.placeholder',
                })}
                addonAfter="m"
              />
            </StyledFormItem>
          </StyledForm>
        )}
      </CardSection>
    )
  );
};
