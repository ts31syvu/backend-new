import { Form } from 'antd';
import styled from 'styled-components';

export const StyledForm = styled(Form)`
  width: 100%;
`;

export const StyledFormItem = styled(Form.Item)`
  width: 100%;

  .ant-input-number-group-wrapper {
    width: 50%;
  }
`;
