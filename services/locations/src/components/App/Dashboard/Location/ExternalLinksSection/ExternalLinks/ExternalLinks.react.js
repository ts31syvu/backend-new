import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form, notification } from 'antd';
import { useQueryClient } from 'react-query';

import { useDomainValidator } from 'components/hooks/useValidators';

import { updateLocationLink } from 'network/api';
import { PrimaryButton } from 'components/general';
import { useSwitchLanguageContext } from 'components/hooks/useSwitchLanguageContext';
import { QUERY_KEYS, useGetLocationLinks } from 'components/hooks/queries';
import { getFormFields, getFieldsValaueChanged } from './ExternalLinks.helper';
import {
  StyledForm,
  StyledInput,
  ButtonWrapper,
  Wrapper,
} from './ExternalLinks.styled';

export const ExternalLinks = ({ location }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const queryClient = useQueryClient();
  const domainValidator = useDomainValidator();
  const { currentLanguage } = useSwitchLanguageContext();
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);

  const fields = getFormFields();

  useEffect(() => {
    form.resetFields();
  }, [currentLanguage, form]);

  const { isLoading, error: linksError, data: links } = useGetLocationLinks(
    location.uuid
  );

  const resetForm = () => {
    queryClient
      .invalidateQueries([QUERY_KEYS.LOCATION_LINKS, location.uuid])
      .then(() => {
        form.resetFields();
        setIsButtonDisabled(true);
      })
      .catch(error => console.error(error));
  };

  const showError = key => {
    resetForm();
    notification.error({
      message: intl.formatMessage({
        id: `notification.externalLinks.${key}.error`,
      }),
    });
  };

  const checkResponse = (response, key) => {
    if (response.status !== 204) {
      showError(key);
    } else {
      resetForm();
    }
  };

  const onFinish = values => {
    const linkList = Object.keys(values).map(key => ({ [key]: values[key] }));

    // eslint-disable-next-line array-callback-return
    linkList.forEach(link => {
      const entries = Object.entries(link);
      const keyPair = entries[0];
      const key = keyPair[0];
      const value = keyPair[1];

      if (value === undefined) return;
      updateLocationLink(location.uuid, {
        type: key,
        url: value === '' ? null : value,
      })
        .then(response => {
          checkResponse(response, key);
        })
        .catch(showError);
    });
  };

  const onChange = () => {
    const isActiveChange = getFieldsValaueChanged(form.getFieldsValue(), links);
    setIsButtonDisabled(!isActiveChange);
  };

  if (isLoading || linksError) return null;

  return (
    <Wrapper data-cy="websiteMenuLinksSection">
      <StyledForm
        form={form}
        onFinish={onFinish}
        onFieldsChange={onChange}
        initialValues={links}
      >
        {fields.map(field => (
          <Form.Item
            key={field.id}
            colon={false}
            label={intl.formatMessage({
              id: field.label,
            })}
            name={field.id}
            rules={domainValidator}
          >
            <StyledInput
              data-cy={`linkInput-${field.id}`}
              placeholder={intl.formatMessage({ id: field.placeholder })}
            />
          </Form.Item>
        ))}
        <ButtonWrapper>
          <PrimaryButton
            data-cy="websiteLinksSaveChangesButton"
            htmlType="submit"
            disabled={isButtonDisabled}
          >
            {intl.formatMessage({ id: 'externalLinks.save' })}
          </PrimaryButton>
        </ButtonWrapper>
      </StyledForm>
    </Wrapper>
  );
};
