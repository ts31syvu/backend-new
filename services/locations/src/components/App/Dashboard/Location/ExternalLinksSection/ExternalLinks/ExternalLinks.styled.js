import styled from 'styled-components';
import { Form, Input } from 'antd';

export const StyledForm = styled(Form)`
  width: 100%;
`;

export const StyledInput = styled(Input)`
  border: 1px solid #696969;
  background-color: transparent;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 40px;
  width: 100%;
`;

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;
