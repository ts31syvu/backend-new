import React from 'react';
import { useIntl } from 'react-intl';

import { InformationIcon, StyledTooltip } from 'components/general';

import {
  CardSection,
  CardSectionDescription,
  LocationCard,
} from '../LocationCard';

import { ExternalLinks } from './ExternalLinks';
import { StyledFooter } from './ExternalLinksSection.styled';

export const ExternalLinksSection = ({ location }) => {
  const intl = useIntl();
  return (
    <LocationCard
      testId="websiteMenuAndLinks"
      isCollapse
      title={intl.formatMessage({
        id: 'settings.location.externalLinks.headline',
      })}
    >
      <CardSection isLast>
        <CardSectionDescription>
          {intl.formatMessage({
            id: 'settings.location.externalLinks.description',
          })}
          <StyledTooltip
            title={intl.formatMessage({
              id: 'settings.location.externalLinks.tooltip',
            })}
          >
            <InformationIcon data-cy="linkContentInformationIcon" />
          </StyledTooltip>
        </CardSectionDescription>
        <StyledFooter>
          <ExternalLinks location={location} />
        </StyledFooter>
      </CardSection>
    </LocationCard>
  );
};
