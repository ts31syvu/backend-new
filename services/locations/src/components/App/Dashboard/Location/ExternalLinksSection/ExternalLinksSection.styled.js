import styled from 'styled-components';

export const StyledFooter = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: left;
`;
