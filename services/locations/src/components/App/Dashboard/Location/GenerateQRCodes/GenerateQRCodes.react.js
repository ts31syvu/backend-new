import React, { useCallback, useEffect, useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import { message } from 'antd';

import { useWorker } from 'components/hooks/useWorker';
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';

import { LOADING_MESSAGE } from 'components/notifications';
import { InformationIcon, StyledTooltip } from 'components/general';

import { LocationCard } from '../LocationCard';

import { QrCodeLocation } from './QrCodeLocation';
import { QrCodeTable } from './QrCodeTable';
import { QrCwaCompatible } from './QrCwaCompatible';

import { QRCodeCSVDownload, TABLE_QR_CODE } from './GenerateQRCodes.helper';
import { QrPrint } from './QrPrint';
import {
  CollapsableWrapper,
  StyledPrimaryButton,
  ButtonWrapper,
  StyledCSVWrapper,
  StyledButtonSection,
} from './GenerateQRCodes.styled';

export const GenerateQRCodes = ({ location }) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);
  const [isCWAEventEnabled, setIsCWAEventEnabled] = useState(true);
  const [isLocationQRCodeEnabled, setIsLocationQRCodeEnabled] = useState(
    !location.tableCount
  );
  const [isTableQRCodeEnabled, setIsTableQRCodeEnabled] = useState(
    !!location.tableCount
  );

  const worker = useRef(getPDFWorker());
  const cleanup = useCallback(() => {
    message.destroy(LOADING_MESSAGE);
  }, []);
  const pdfWorkerApiReference = useWorker(worker.current, cleanup);

  useEffect(() => {
    setIsLocationQRCodeEnabled(!location.tableCount);
    setIsTableQRCodeEnabled(!!location.tableCount);
  }, [location.tableCount]);

  const switchQRCodeSettings = useCallback(
    qrCodeType => {
      if (qrCodeType === TABLE_QR_CODE) {
        if (isTableQRCodeEnabled) {
          setIsTableQRCodeEnabled(false);
          setIsLocationQRCodeEnabled(false);
          return;
        }

        setIsTableQRCodeEnabled(true);
        setIsLocationQRCodeEnabled(false);
        return;
      }

      if (isLocationQRCodeEnabled) {
        setIsTableQRCodeEnabled(false);
        setIsLocationQRCodeEnabled(false);
        return;
      }

      setIsTableQRCodeEnabled(false);
      setIsLocationQRCodeEnabled(true);
    },
    [isLocationQRCodeEnabled, isTableQRCodeEnabled]
  );
  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location,
    intl,
    isTableQRCodeEnabled,
    isCWAEventEnabled,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  return (
    <LocationCard
      isCollapse
      title={intl.formatMessage({ id: 'settings.location.qrcode.headline' })}
      testId="generateQRCodes"
    >
      <CollapsableWrapper>
        <QrCodeLocation
          location={location}
          switchQRCodeSettings={switchQRCodeSettings}
          isLocationQRCodeEnabled={isLocationQRCodeEnabled}
        />
        {location.tableCount && (
          <QrCodeTable
            location={location}
            isTableQRCodeEnabled={isTableQRCodeEnabled}
            switchQRCodeSettings={switchQRCodeSettings}
          />
        )}
        <QrCwaCompatible
          setIsCWAEventEnabled={setIsCWAEventEnabled}
          isCWAEventEnabled={isCWAEventEnabled}
        />
      </CollapsableWrapper>
      <StyledButtonSection direction="end" isLast>
        <ButtonWrapper>
          <StyledPrimaryButton
            loading={isDownloading}
            onClick={triggerDownload}
            disabled={!isLocationQRCodeEnabled && !isTableQRCodeEnabled}
            data-cy="qrCodeDownload"
          >
            {intl.formatMessage({ id: 'settings.location.qrcode.generate' })}
          </StyledPrimaryButton>
          <StyledCSVWrapper>
            <QRCodeCSVDownload
              location={location}
              isCWAEventEnabled={isCWAEventEnabled}
              isTableQRCodeEnabled={isTableQRCodeEnabled}
            />
            <StyledTooltip
              children={<InformationIcon data-cy="qrCodeCsvInfoIcon" />}
              title={intl.formatMessage({
                id: 'settings.location.qrcode.infoText',
              })}
            />
          </StyledCSVWrapper>
        </ButtonWrapper>
      </StyledButtonSection>
      <QrPrint />
    </LocationCard>
  );
};
