import React from 'react';
import { useIntl } from 'react-intl';

import { Switch } from 'components/general';

import {
  StyledSwitchContainer,
  StyledCardSection,
  StyledCardSectionTitle,
} from '../GenerateQRCodes.styled';

import { LOCATION_QR_CODE } from '../GenerateQRCodes.helper';

export const QrCodeLocation = ({
  switchQRCodeSettings,
  isLocationQRCodeEnabled,
}) => {
  const intl = useIntl();

  return (
    <StyledCardSection data-cy="qrCodeForEntireAreaSection">
      <StyledCardSectionTitle>
        {intl.formatMessage({
          id: 'settings.location.qrcode.location.headline',
        })}
        <StyledSwitchContainer>
          <Switch
            checked={isLocationQRCodeEnabled}
            onChange={() => switchQRCodeSettings(LOCATION_QR_CODE)}
            data-cy="qrCodeForEntireAreaToggle"
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
