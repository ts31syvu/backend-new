import React from 'react';
import { useIntl } from 'react-intl';

import { Switch } from 'components/general';

import {
  StyledSwitchContainer,
  StyledCardSection,
  StyledCardSectionTitle,
} from '../GenerateQRCodes.styled';

import { TABLE_QR_CODE } from '../GenerateQRCodes.helper';

export const QrCodeTable = ({
  location,
  isTableQRCodeEnabled,
  switchQRCodeSettings,
}) => {
  const intl = useIntl();

  return (
    <StyledCardSection data-cy="qrCodesForTablesSection">
      <StyledCardSectionTitle>
        {intl.formatMessage({
          id: 'settings.location.qrcode.tables.headline',
        })}
        <StyledSwitchContainer>
          <Switch
            checked={isTableQRCodeEnabled}
            disabled={!location.tableCount}
            onChange={() => switchQRCodeSettings(TABLE_QR_CODE)}
            data-cy="qrCodesForTablesToggle"
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
