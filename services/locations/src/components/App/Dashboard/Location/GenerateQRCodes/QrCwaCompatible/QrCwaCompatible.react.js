import React from 'react';
import { useIntl } from 'react-intl';

import { Switch, StyledTooltip, InformationIcon } from 'components/general';

import {
  StyledSwitchContainer,
  StyledCardSection,
  StyledCardSectionTitle,
} from '../GenerateQRCodes.styled';

export const QrCwaCompatible = ({
  setIsCWAEventEnabled,
  isCWAEventEnabled,
}) => {
  const intl = useIntl();

  return (
    <StyledCardSection data-cy="qrCodeCompatabilitySection" isLast>
      <StyledCardSectionTitle>
        {intl.formatMessage({ id: 'location.setting.qr.compatibility' })}
        <StyledTooltip
          children={<InformationIcon data-cy="qrCodeCompatabilityInfoIcon" />}
          title={intl.formatMessage({
            id: 'settings.location.qrcode.cwaInfoText',
          })}
        />
        <StyledSwitchContainer>
          <Switch
            data-cy="qrCodeCompatabilityToggle"
            checked={isCWAEventEnabled}
            onChange={() => setIsCWAEventEnabled(!isCWAEventEnabled)}
          />
        </StyledSwitchContainer>
      </StyledCardSectionTitle>
    </StyledCardSection>
  );
};
