import { useIntl } from 'react-intl';
import React from 'react';

import { QrPrintLogoIconSVG } from 'assets/icons';
import { QR_PRINT_LINK } from 'constants/links';
import {
  QrPrintWrapper,
  LinkWrapper,
  QrPrintLogo,
  QrPrintText,
  StyledPrintLink,
  StyledRightOutlined,
  QrPrintStep,
} from './QrPrint.styled';

export const QrPrint = () => {
  const intl = useIntl();

  return (
    <QrPrintWrapper data-cy="qrPrintSection">
      <QrPrintLogo src={QrPrintLogoIconSVG} />
      <QrPrintText>{intl.formatMessage({ id: 'qrPrint.info' })}</QrPrintText>
      <QrPrintStep>{intl.formatMessage({ id: 'qrPrint.step1' })}</QrPrintStep>
      <QrPrintStep>
        {intl.formatMessage(
          { id: 'qrPrint.step2' },
          {
            link: intl.formatMessage({ id: 'qrPrint.link' }),
          }
        )}
      </QrPrintStep>
      <QrPrintStep>{intl.formatMessage({ id: 'qrPrint.step3' })}</QrPrintStep>
      <LinkWrapper
        data-cy="qrPrintLink"
        href={QR_PRINT_LINK}
        target="_blank"
        rel="noopener noreferrer"
      >
        <StyledPrintLink>
          {intl.formatMessage({ id: 'qrPrint.link' })}
        </StyledPrintLink>
        <StyledRightOutlined />
      </LinkWrapper>
    </QrPrintWrapper>
  );
};
