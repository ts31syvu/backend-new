import styled from 'styled-components';
import { RightOutlined } from '@ant-design/icons';

export const QrPrintWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 24px 0;
  width: 100%;
  border-bottom-left-radius: 8px;
  border-bottom-right-radius: 8px;
  background-color: rgba(248, 249, 250);
  margin-bottom: -32px;
`;

export const QrPrintLogo = styled.img`
  height: 16px;
  width: 110px;
  margin-left: 32px;
`;

export const LinkWrapper = styled.a`
  display: flex;
  justify-content: flex-end;
  margin-top: 24px;
  margin-right: 32px;
  width: auto;
  cursor: pointer;
  text-decoration: none;
`;

export const QrPrintText = styled.div`
  margin: 16px 0 8px 32px;
  font-size: 14px;
  font-weight: 500;
`;

export const QrPrintStep = styled(QrPrintText)`
  margin: 0 0 0 32px;
  display: flex;
`;

export const PrintLink = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  font-size: 14px;
  margin-left: 4px;
  white-space: nowrap;
`;

export const StyledPrintLink = styled(PrintLink)`
  color: rgba(80, 102, 124, 0.87);
  text-transform: uppercase;
`;

export const StyledRightOutlined = styled(RightOutlined)`
  margin-left: 16px;
  align-self: center;
  color: #50667c;
`;
