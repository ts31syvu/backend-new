import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useParams } from 'react-router-dom';
import { useQuery } from 'react-query';
import { getOperatorPaymentEnabled } from 'network/payment';
import { LucaLogoPaddingIcon } from 'assets/icons';
import { LocationFooter } from 'components/App/LocationFooter';
import { LocationSettings } from 'components/App/LocationSettings';
import { useGetLocationById, useDailyKey } from 'components/hooks/queries';
import { InfoBanner } from 'components/general/InfoBanner';
import { NO_DAILY_KEY_BLOG_POST_LINK } from 'constants/links';
import { getHasSeenDailyKeyBannerFromStorage } from 'utils/storage';
import { Checkout } from './Checkout';
import { CheckInQuery } from './CheckInQuery';
import { ScannerSelection } from './ScannerSelection';
import { AreaDetails } from './AreaDetails';
import { GenerateQRCodes } from './GenerateQRCodes';
import { TableSubdivision } from './TableSubdivision';
import { LocationOverview } from './LocationOverview';
import { ExternalLinksSection } from './ExternalLinksSection';
import { RegisterBadges } from './RegisterBadges';
import {
  Header,
  Wrapper,
  ButtonWrapper,
  HiddenImage,
  GroupName,
  HeaderWrapper,
  StyledTabs,
  StyledTabPane,
  BannerText,
  BannerReadMore,
} from './Location.styled';
import { LocationGuests } from './LocationOverview/LocationGuests';
import { Payment } from './Payment';

// eslint-disable-next-line complexity
export const Location = ({ isOperatorTrusted }) => {
  const intl = useIntl();
  const [showBanner, setShowBanner] = useState(false);
  const [disableCheckins, setDisableCheckins] = useState(false);
  const hasSeenDailyKeyBanner = getHasSeenDailyKeyBannerFromStorage();

  const { locationId } = useParams();

  const { isLoading, error, data: location } = useGetLocationById(locationId);

  // This is a dependent query and therefore not moved into the queries.js
  const {
    isLoading: isPaymentLoading,
    error: paymentError,
    data: paymentEnabled,
  } = useQuery(
    'paymentEnabled',
    () => getOperatorPaymentEnabled(location.operator),
    {
      enabled: !!location,
    }
  );

  const {
    isLoading: isLoadingDailyKey,
    error: errorDailyKey,
    data: dailyKey,
  } = useDailyKey();

  useEffect(() => {
    if (isLoadingDailyKey) return;
    setShowBanner(errorDailyKey || !dailyKey);
    setDisableCheckins(errorDailyKey || !dailyKey);
  }, [errorDailyKey, dailyKey, isLoadingDailyKey]);

  if (isLoading || error || isPaymentLoading || paymentError) {
    return null;
  }

  const isPaymentEnabled = paymentEnabled && paymentEnabled.paymentEnabled;

  return (
    <>
      {showBanner && !hasSeenDailyKeyBanner && (
        <InfoBanner setShowBanner={setShowBanner}>
          <div>
            <BannerText>
              {intl.formatMessage({ id: 'banner.noDailyKey' })}
            </BannerText>
            <BannerReadMore href={NO_DAILY_KEY_BLOG_POST_LINK} target="_blank">
              {intl.formatMessage({ id: 'banner.noDailyKey.readMore' })}
            </BannerReadMore>
          </div>
        </InfoBanner>
      )}
      <Wrapper>
        <HeaderWrapper>
          <GroupName data-cy="groupName">{location.groupName}</GroupName>
          <Header data-cy="locationDisplayName">
            {location.name ||
              intl.formatMessage({ id: 'location.defaultName' })}
          </Header>
        </HeaderWrapper>
        {isOperatorTrusted && (
          <ButtonWrapper>
            <RegisterBadges />
          </ButtonWrapper>
        )}
        <StyledTabs defaultActiveKey="openAreaCheckin">
          <StyledTabPane
            tab={intl.formatMessage({ id: 'location.tabs.profile' })}
            key="openAreaSettings"
          >
            <LocationSettings />
          </StyledTabPane>
          <StyledTabPane
            key="openAreaCheckin"
            tab={intl.formatMessage({ id: 'location.tabs.checkin' })}
          >
            <LocationGuests location={location} />
            <ScannerSelection
              location={location}
              isDisabled={disableCheckins}
            />
            <LocationOverview location={location} />
            <Checkout location={location} />
            <AreaDetails location={location} />
            <ExternalLinksSection location={location} />
            <CheckInQuery location={location} />
            <TableSubdivision location={location} />
            <GenerateQRCodes location={location} />
            <LocationFooter />
            {/* We need to pre load the Luca logo for QR-Code generation */}
            <HiddenImage src={LucaLogoPaddingIcon} />
          </StyledTabPane>
          {isPaymentEnabled && (
            <StyledTabPane
              tab={intl.formatMessage({ id: 'location.tabs.payment' })}
              key="openPaymentSettings"
            >
              <Payment location={location} />
            </StyledTabPane>
          )}
        </StyledTabs>
      </Wrapper>
    </>
  );
};
