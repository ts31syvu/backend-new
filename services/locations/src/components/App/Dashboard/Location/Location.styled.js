import styled from 'styled-components';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

export const StyledTabs = styled(Tabs)`
  .ant-tabs-tab.ant-tabs-tab-active > .ant-tabs-tab-btn {
    color: rgb(0, 0, 0);
    font-family: Montserrat-Bold, sans-serif;
    font-size: 16px;
    font-weight: bold;
  }
  .ant-tabs-tab {
    color: rgb(0, 0, 0);
    font-family: Montserrat-Medium, sans-serif;
    font-size: 16px;
    font-weight: 500;
  }
  .ant-tabs-nav {
    border-bottom: 0.5px solid rgb(0, 0, 0);
  }
  .ant-tabs-ink-bar {
    background: #f1743c;
    border: 3px solid rgb(241, 116, 60);
    border-radius: 4px;
  }
  .ant-tabs-tab + .ant-tabs-tab {
    margin: 0 0 0 80px;
  }
`;

export const StyledTabPane = styled(TabPane)``;

export const Wrapper = styled.div`
  margin: 40px 80px;
  display: flex;
  flex-direction: column;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Header = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const GroupName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 48px 0 0 0;
`;

export const HiddenImage = styled.img`
  display: none;
`;

export const BannerText = styled.div`
  color: #fff;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 24px;
  height: 100%;
  display: inline-block;
`;

export const BannerReadMore = styled.a`
  padding-left: 8px;
  color: #fff;
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 24px;
  height: 100%;
  display: inline-block;
`;
