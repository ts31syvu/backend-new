import React, { useState } from 'react';
import { Collapse } from 'antd';

import { ArrowDownBlueIcon } from 'assets/icons';

import {
  StyledTitle,
  StyledContainer,
  StyledPlaceholder,
  StyledIcon,
} from './LocationCard.styled';

const { Panel } = Collapse;
const CONTENT_KEY = 'CONTENT';

const ArrowIcon = ({ isActive }) => (
  <StyledIcon component={ArrowDownBlueIcon} rotate={isActive ? 180 : 0} />
);

export const LocationCard = ({
  component: Component,
  title,
  testId,
  children,
  description,
  open = false,
  isCollapse = false,
}) => {
  const [isOpen, setIsOpen] = useState(open);
  if (!isCollapse) {
    return (
      <StyledContainer>
        <StyledTitle showBorder>{title}</StyledTitle>
        {children}
        <StyledPlaceholder />
      </StyledContainer>
    );
  }

  const collapseClassName = isOpen ? ' locationCard-open' : '';
  const extendedClass =
    collapseClassName && description
      ? `${collapseClassName}-with-description`
      : collapseClassName;

  const Container = Component ?? StyledContainer;

  return (
    <Container data-cy={`locationCard-${testId}`} isCollapse>
      <Collapse
        bordered={false}
        className={`locationCard${extendedClass}`}
        defaultActiveKey={open ? [CONTENT_KEY] : []}
        expandIconPosition="right"
        style={{
          background: 'transparent',
        }}
        onChange={key => setIsOpen(key.length > 0)}
        expandIcon={({ isActive }) => <ArrowIcon isActive={isActive} />}
      >
        <Panel
          className="site-collapse-custom-panel"
          key={CONTENT_KEY}
          header={<StyledTitle>{title}</StyledTitle>}
        >
          {description && (
            <div className="locationCard-panel-description">{description}</div>
          )}
          {children}
          <StyledPlaceholder />
        </Panel>
      </Collapse>
    </Container>
  );
};
