import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const StyledIcon = styled(Icon)`
  font-size: 16px;
  color: rgb(80, 102, 124);
  top: 50%;
  right: 32px;
  position: absolute;
  transform: translateY(-50%);
`;

export const StyledContainer = styled.div`
  display: flex;
  flex-direction: column;
  background: rgb(255, 255, 255);
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 32px;
  padding: ${({ isCollapse }) => (isCollapse ? 'none' : '20px 16px 0 16px')};
`;

export const StyledTitle = styled.div`
  align-items: center;
  display: flex;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  padding: 4px 16px;
  white-space: nowrap;
`;

export const StyledPlaceholder = styled.div`
  padding-bottom: 32px;
`;

export const CardSection = styled.div`
  width: 100%;
  display: flex;
  padding: 24px 32px 0 32px;
  flex-direction: column;
  align-items: ${({ direction }) =>
    direction === 'end' ? `flex-end` : `flex-start`};
  border-bottom: ${({ isLast }) => (isLast ? 0 : 1)}px solid rgb(151, 151, 151);
`;
export const CardSectionTitle = styled.div`
  align-items: center;
  color: rgba(0, 0, 0, 0.87);
  display: flex;
  font-family: Montserrat-SemiBold, sans-serif;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 5px;
  width: 100%;
`;

export const CardSectionSubTitle = styled(CardSectionTitle)`
  width: auto;
  font-weight: 400;
`;

export const CardSectionDescription = styled.div`
  width: 100%;
  font-size: 14px;
  margin-bottom: 24px;
  font-weight: 500;
  color: rgba(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;
