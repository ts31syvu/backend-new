import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';

import { updateLocation } from 'network/api';

import { Switch } from 'components/general';
import {
  Wrapper,
  Header,
  HeaderWrapper,
  StyledSwitchContainer,
  Description,
} from './AnonymousCheckins.styled';

export const AnonymousCheckins = ({ location }) => {
  const intl = useIntl();
  const [isContactDataMandatory, setIsContactDataMandatory] = useState(
    location.isContactDataMandatory
  );

  const toggleMandatoryCheckin = () => {
    updateLocation({
      locationId: location.uuid,
      data: { isContactDataMandatory: !isContactDataMandatory },
    })
      .then(() => {
        setIsContactDataMandatory(!isContactDataMandatory);
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'notification.updateLocation.error',
          }),
        });
      });
  };

  return (
    <Wrapper>
      <HeaderWrapper>
        <Header>
          {intl.formatMessage({
            id: 'group.view.overview.anonymousCheckins.title',
          })}
        </Header>
        <StyledSwitchContainer>
          <Switch
            checked={isContactDataMandatory}
            onChange={toggleMandatoryCheckin}
            customCheckedLabel={intl.formatMessage({ id: 'switch.mandatory' })}
          />
        </StyledSwitchContainer>
      </HeaderWrapper>
      <Description>
        {intl.formatMessage({
          id: 'group.view.overview.anonymousCheckins.description',
        })}
      </Description>
    </Wrapper>
  );
};
