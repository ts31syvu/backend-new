import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Wrapper = styled.div`
  display: flex;
  align-items: center;
  border-right: 1px solid rgb(151, 151, 151);
`;

export const Counter = styled.div`
  font-size: 34px;
  font-weight: 500;
  margin-right: 18px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
`;

export const Refresh = styled.div`
  color: rgb(80, 102, 124);
  cursor: pointer;
  margin-right: 32px;
`;

export const StyledIcon = styled(Icon)`
  polyline {
    stroke: rgb(80, 102, 124);
  }
  path {
    fill: rgb(80, 102, 124);
  }
`;
