import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';

import { updateLocation } from 'network/api';
import {
  getEntryPolicyOptions,
  LocationEntryPolicyTypes,
} from 'utils/entryPolicy';
import { Switch, InformationIcon, StyledTooltip } from 'components/general';

import { useFeatureFlags } from 'components/hooks/useFeatureFlags';
import {
  Wrapper,
  Header,
  HeaderWrapper,
  StyledSwitchContainer,
  Description,
  StyledSelect,
  StyledOption,
} from './EntryPolicy.styled';

export const EntryPolicy = ({ location }) => {
  const intl = useIntl();
  const [isEntryPolicyActive, setIsEntryPolicyActive] = useState(
    !!location.entryPolicy
  );

  const featureFlags = useFeatureFlags();
  const entryPolicies = getEntryPolicyOptions(featureFlags?.enable_two_g_plus);
  const defaultValue =
    location.entryPolicy ??
    entryPolicies?.[0].value ??
    LocationEntryPolicyTypes.TWO_G;

  const showError = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'notification.updateLocation.error',
      }),
    });

  const toggleEntryPolicy = () => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: isEntryPolicyActive ? null : defaultValue,
      },
    })
      .then(() => {
        setIsEntryPolicyActive(!isEntryPolicyActive);
      })
      .catch(showError);
  };

  const handlePolicyChange = value => {
    updateLocation({
      locationId: location.uuid,
      data: {
        entryPolicy: value,
      },
    }).catch(showError);
  };

  return (
    <Wrapper>
      <HeaderWrapper>
        <Header>
          {intl.formatMessage({
            id: 'group.view.overview.entryPolicy.title',
          })}
        </Header>
        <StyledSwitchContainer>
          <Switch checked={isEntryPolicyActive} onChange={toggleEntryPolicy} />
        </StyledSwitchContainer>
      </HeaderWrapper>
      <Description>
        {intl.formatMessage({
          id: 'group.view.overview.entryPolicy.info',
        })}
        <StyledTooltip
          children={<InformationIcon />}
          title={intl.formatMessage(
            {
              id: 'entryPolicy.tooltip',
            },
            { br: <br /> }
          )}
        />
      </Description>
      {isEntryPolicyActive && (
        <StyledSelect defaultValue={defaultValue} onChange={handlePolicyChange}>
          {entryPolicies.map(policy => (
            <StyledOption key={policy.id} value={policy.value}>
              {intl.formatMessage({ id: policy.locale })}
            </StyledOption>
          ))}
        </StyledSelect>
      )}
    </Wrapper>
  );
};
