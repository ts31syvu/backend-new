import styled from 'styled-components';
import { Select } from 'antd';

const { Option } = Select;

export const Wrapper = styled.div`
  padding: 0 16px 0;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  font-size: 16px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const StyledSwitchContainer = styled.div``;

export const Description = styled.div`
  margin-top: 16px;
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const StyledSelect = styled(Select)`
  margin-top: 24px;
  width: 348px;
`;

export const StyledOption = styled(Option)``;
