import React from 'react';
import { useFeatureFlags } from 'components/hooks/useFeatureFlags';

import { AnonymousCheckins } from './AnonymousCheckins';
import { EntryPolicy } from './EntryPolicy';
import { LocationCard } from '../LocationCard';

export const LocationOverview = ({ location }) => {
  const featureFlags = useFeatureFlags();
  return (
    <LocationCard>
      <EntryPolicy location={location} />
      {featureFlags.anonymous_checkins && (
        <AnonymousCheckins location={location} />
      )}
    </LocationCard>
  );
};
