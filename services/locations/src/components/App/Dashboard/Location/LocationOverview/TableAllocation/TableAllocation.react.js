import React from 'react';
import { useIntl } from 'react-intl';

// Utils
import { getRemainingDaysToUploadKey, usePrivateKey } from 'utils/privateKey';

// Components
import { useModal } from 'components/hooks/useModal';
import { useGetMe, useGetPrivateKeySecret } from 'components/hooks/queries';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { TableAllocationModal } from 'components/App/modals/TableAllocationModal';
import { Spin } from 'antd';
import { SecondaryButton } from 'components/general';
import { TableAllocationWrapper } from './TableAllocation.styled';

export const TableAllocation = ({ location }) => {
  const intl = useIntl();
  const [openModal, closeModal] = useModal();

  const {
    data: privateKeySecret,
    isLoading: isPrivateKeySecretLoading,
  } = useGetPrivateKeySecret();

  const { data: operator, isLoading: isOperatorLoading } = useGetMe();
  const [privateKey] = usePrivateKey(privateKeySecret);

  const remainingDaysToUploadKey = getRemainingDaysToUploadKey(
    operator.lastSeenPrivateKey
  );

  const openTableAllocation = selectedPrivateKey => {
    openModal({
      content: (
        <TableAllocationModal
          location={location}
          privateKey={selectedPrivateKey}
        />
      ),
    });
  };

  const openPrivateKeyLoader = onSuccess => {
    openModal({
      content: (
        <PrivateKeyLoader
          onSuccess={onSuccess}
          publicKey={operator.publicKey}
          infoTextId="privateKey.tableAllocation.modal.info"
          footerItem={
            remainingDaysToUploadKey > 0 && (
              <SecondaryButton
                type="link"
                onClick={closeModal}
                data-cy="skipPrivateKeyUpload"
              >
                {intl.formatMessage({ id: 'privateKey.modal.skip' })}
              </SecondaryButton>
            )
          }
          remainingDaysToUploadKey={remainingDaysToUploadKey}
          privateKeySecret={privateKeySecret}
          operator={operator}
        />
      ),
      closable: remainingDaysToUploadKey > 0,
    });
  };

  const handleTableAllocation = () => {
    if (privateKey) {
      openTableAllocation(privateKey);
    } else {
      openPrivateKeyLoader(openTableAllocation);
    }
  };

  if (isOperatorLoading || isPrivateKeySecretLoading) return <Spin />;
  if (!location.tableCount || location.tableCount === 0) return null;

  return (
    <TableAllocationWrapper
      onClick={handleTableAllocation}
      data-cy="showTableAllocation"
    >
      {intl.formatMessage({ id: 'group.view.overview.tableAllocation' })}
    </TableAllocationWrapper>
  );
};
