import React from 'react';
import { useIntl } from 'react-intl';
import { PAYMENT_TAB } from 'components/App/GroupSettings/GroupSettings.helper';
import { Link } from 'react-router-dom';
import { BASE_GROUP_SETTINGS_ROUTE } from 'constants/routes';
import {
  Description,
  SetupPaymentTitle,
  SetupWrapper,
  StyledPaymentWrapper,
  StyledRightCircleFilled,
  Title,
} from './AreaInactivePayment.styled';

export const AreaInactivePayment = ({ location }) => {
  const intl = useIntl();
  const locationName =
    location?.name || intl.formatMessage({ id: 'location.defaultName' });

  const LinkToPaymentTab = `${BASE_GROUP_SETTINGS_ROUTE}${location.groupId}?tab=${PAYMENT_TAB}`;

  return (
    <StyledPaymentWrapper>
      <Title>
        {intl.formatMessage({ id: 'payment.area.title' }, { locationName })}
      </Title>
      <Description>
        {intl.formatMessage({ id: 'payment.area.description' })}
      </Description>
      <Link to={LinkToPaymentTab}>
        <SetupWrapper>
          <SetupPaymentTitle>
            {intl.formatMessage({ id: 'payment.area.setupTitle' })}
          </SetupPaymentTitle>
          <StyledRightCircleFilled />
        </SetupWrapper>
      </Link>
    </StyledPaymentWrapper>
  );
};
