import styled from 'styled-components';
import { RightCircleFilled } from '@ant-design/icons';
import { PaymentWrapper } from '../Payment.styled';

export const StyledPaymentWrapper = styled(PaymentWrapper)`
  padding: 24px 32px;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: bold;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  margin: 16px 0 26px 0;
`;

export const SetupPaymentTitle = styled.div`
  color: rgb(80, 102, 124);
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  margin-right: 8px;
`;

export const SetupWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 18px;
  cursor: pointer;
`;

export const StyledRightCircleFilled = styled(RightCircleFilled)`
  font-size: 24px;
  color: rgb(195, 206, 217);
`;
