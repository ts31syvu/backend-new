import React from 'react';
import { useIntl } from 'react-intl';
import { patchLocation } from 'network/payment';
import { useQueryClient } from 'react-query';
import { Switch } from 'components/general';
import { LocationCard } from 'components/App/Dashboard/Location/LocationCard';
import { notification } from 'antd';
import { QUERY_KEYS, useGetPaymentActive } from 'components/hooks/queries';
import { Title, Wrapper } from './EnablePayment.styled';

export const EnablePayment = ({ location }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const { isLoading, data: paymentEnabled } = useGetPaymentActive(
    location.uuid
  );

  if (isLoading || !location || !location.uuid) {
    return null;
  }

  const showErrorNotification = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'payment.location.enablePayment.error',
      }),
    });

  const toggleOnEnablePayment = () => {
    patchLocation(location.uuid, !paymentEnabled.paymentActive)
      .then(response => {
        if (response.status !== 202) {
          showErrorNotification();
          return;
        }
        queryClient.invalidateQueries(QUERY_KEYS.LOCATION_PAYMENT_ACTIVE);
      })
      .catch(() => {
        showErrorNotification();
      });
  };

  return (
    <LocationCard>
      <Wrapper>
        <Title>
          {intl.formatMessage({ id: 'groupSettings.tabs.payment' })}
        </Title>
        <Switch
          checked={paymentEnabled.paymentActive}
          onChange={toggleOnEnablePayment}
        />
      </Wrapper>
    </LocationCard>
  );
};
