import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: -8px;
`;

export const Title = styled.p`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 24px;
  margin-bottom: 0;
  padding: 0 16px;
`;
