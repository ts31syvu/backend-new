import React from 'react';
import { useQuery } from 'react-query';
import { getOnboardingStatus } from 'network/payment';
import { PaymentHistory } from 'components/general/PaymentHistory';
import { ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import { queryDoNotRetryOnCode } from 'network/utlis';
import { AreaInactivePayment } from './AreaInactivePayment';
import { EnablePayment } from './EnablePayment';
import { PaymentWrapper } from './Payment.styled';

export const Payment = ({ location }) => {
  const { isLoading, error, data: onboardingStatus } = useQuery(
    'getOnboardingStatus',
    () => getOnboardingStatus(location.groupId),
    {
      retry: queryDoNotRetryOnCode([401, 404]),
    }
  );

  if (isLoading || error) return null;

  if (onboardingStatus.status === ONBOARDING_STATUS.DONE)
    return (
      <PaymentWrapper>
        <EnablePayment location={location} />
        <PaymentHistory
          locationGroupId={location.groupId}
          locationId={location.uuid}
        />
      </PaymentWrapper>
    );

  return <AreaInactivePayment location={location} />;
};
