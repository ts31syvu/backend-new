export const buttonStyles = {
  color: 'rgba(0, 0, 0, 0.87)',
  background: 'rgb(195, 206, 217)',
  fontFamily: 'Montserrat-Bold, sans-serif',
  fontSize: 14,
  fontWeight: 'bold',
  padding: '0 40px',
  marginRight: 16,
  textTransform: 'uppercase',
};
