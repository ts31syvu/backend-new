import React from 'react';
import { useIntl } from 'react-intl';

import {
  ContactFormIcon,
  HandScannerIcon,
  TabletIcon,
  DisabledContactFormIcon,
  DisabledHandScannerIcon,
  DisabledTabletIcon,
} from 'assets/icons';

import { HOSTNAME } from 'constants/routes';
import { useModal } from 'components/hooks/useModal';
import { StyledTooltip } from 'components/general';

import { CheckinQrModal } from 'components/App/modals/CheckinQrModal';
import {
  ServiceArea,
  Service,
  ServiceName,
  ServiceLink,
  LinkContent,
  ServiceWrapper,
  DisabledLink,
  StyledIcon,
} from './CheckinOptions.styled';

export const CheckinOptions = ({ location, isDisabled }) => {
  const intl = useIntl();
  const [openModal] = useModal();

  const openContactForm = () => {
    window.open(`${HOSTNAME}/contact-form/${location.formId}`);
  };

  const openScanner = () => {
    window.open(`${HOSTNAME}/scanner/${location.scannerAccessId}`);
  };

  const openCamScanner = () => {
    window.open(`${HOSTNAME}/scanner/cam/${location.scannerAccessId}`);
  };

  const services = [
    {
      name: 'camScanner',
      url: `${HOSTNAME}/scanner/cam/${location.scannerAccessId}`,
      intl: 'modal.checkInOptions.camScanner',
      action: () => openCamScanner(),
      icon: (
        <StyledIcon
          component={isDisabled ? DisabledTabletIcon : TabletIcon}
          data-cy={isDisabled ? 'disabledTabletIcon' : 'tabletIcon'}
        />
      ),
    },
    {
      name: 'scanner',
      url: `${HOSTNAME}/scanner/${location.scannerAccessId}`,
      intl: 'modal.checkInOptions.scanner',
      action: () => openScanner(),
      icon: (
        <StyledIcon
          component={isDisabled ? DisabledHandScannerIcon : HandScannerIcon}
          data-cy={isDisabled ? 'disabledHandScannerIcon' : 'handScannerIcon'}
        />
      ),
    },
    {
      name: 'contactForm',
      url: `${HOSTNAME}/contact-form/${location.formId}`,
      intl: 'modal.checkInOptions.contactForm',
      action: () => openContactForm(),
      icon: (
        <StyledIcon
          component={isDisabled ? DisabledContactFormIcon : ContactFormIcon}
          data-cy={isDisabled ? 'disabledContactFormIcon' : 'contactFormIcon'}
        />
      ),
    },
  ];

  const onCopy = link => {
    navigator.clipboard.writeText(link);
  };

  const openQrModal = service =>
    openModal({
      content: <CheckinQrModal service={service} />,
      title: intl.formatMessage({
        id: `modal.checkInQrModal.title_${service.name}`,
      }),
    });

  return (
    <ServiceArea>
      {services.map(service => (
        <ServiceWrapper
          key={service.name}
          style={service.style ? service.style : {}}
        >
          <Service
            isDisabled={isDisabled}
            onClick={() => !isDisabled && service.action()}
            key={service.intl}
            data-cy={service.name}
          >
            {service.icon}
            <ServiceName isDisabled={isDisabled}>
              {intl.formatMessage({
                id: service.intl,
              })}
            </ServiceName>
          </Service>
          <ServiceLink>
            {isDisabled && (
              <DisabledLink>
                {intl.formatMessage({
                  id: 'modal.checkInOptions.copyLink',
                })}
              </DisabledLink>
            )}
            {!isDisabled && (
              <StyledTooltip
                title={intl.formatMessage({
                  id: 'tooltip.copy',
                })}
                trigger="click"
                small
              >
                <LinkContent onClick={() => onCopy(service.url)}>
                  {intl.formatMessage({
                    id: 'modal.checkInOptions.copyLink',
                  })}
                </LinkContent>
              </StyledTooltip>
            )}
            <LinkContent
              isDisabled={isDisabled}
              onClick={() => !isDisabled && openQrModal(service)}
            >
              {intl.formatMessage({
                id: 'modal.checkInOptions.openViaQrCode',
              })}
            </LinkContent>
          </ServiceLink>
        </ServiceWrapper>
      ))}
    </ServiceArea>
  );
};
