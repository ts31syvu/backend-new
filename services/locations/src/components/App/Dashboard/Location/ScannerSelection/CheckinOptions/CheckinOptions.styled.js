import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const ServiceArea = styled.div`
  display: flex;
`;

export const ServiceWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-right: 24px;
`;

export const Service = styled.div`
  display: flex;
  flex-direction: column;
  background: ${({ isDisabled }) =>
    isDisabled ? 'rgb(245, 245, 244)' : 'rgb(195, 206, 217)'};
  border-radius: 4px;
  box-shadow: 0 2px 4px 0 rgba(143, 143, 143, 0.5);
  height: 144px;
  width: 216px;
  cursor: ${({ isDisabled }) => (isDisabled ? 'unset' : 'pointer')};
`;

export const ServiceName = styled.div`
  margin: 0 auto;
  text-align: center;
  width: 85%;
  color: ${({ isDisabled }) =>
    isDisabled ? 'rgb(129, 129, 129)' : 'rgba(0, 0, 0, 0.87)'};
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
`;

export const ServiceLink = styled.div`
  margin-top: 16px;
`;

export const LinkContent = styled.div`
  cursor: ${({ isDisabled }) => (isDisabled ? 'unset' : 'pointer')};
  color: ${({ isDisabled }) =>
    isDisabled ? 'rgb(129, 129, 129)' : 'rgb(80, 102, 124)'};
  font-family: Montserrat-Bold, sans-serif;
  font-size: 12px;
  font-weight: bold;
  text-align: center;
  margin-top: 8px;
`;

export const DisabledLink = styled.div`
  color: rgb(129, 129, 129);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  height: 20px;
  text-align: center;
  margin-top: 8px;
`;

export const StyledIcon = styled(Icon)`
  margin: 32px 0 16px 0;
  font-size: 40px;
`;
