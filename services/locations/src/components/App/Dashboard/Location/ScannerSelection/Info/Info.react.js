import React from 'react';
import { useIntl } from 'react-intl';

import { InformationIcon, StyledTooltip } from 'components/general';

import { Description, StyledQuestionCircleFilled } from './Info.styled';

export const Info = ({ isDisabled }) => {
  const intl = useIntl();

  return (
    <Description>
      {intl.formatMessage({
        id: 'modal.checkInOptions.description',
      })}
      {isDisabled && <StyledQuestionCircleFilled />}
      {!isDisabled && (
        <StyledTooltip
          title={intl.formatMessage({
            id: 'modal.checkInOptions.info.tooltip',
          })}
        >
          <InformationIcon />
        </StyledTooltip>
      )}
    </Description>
  );
};
