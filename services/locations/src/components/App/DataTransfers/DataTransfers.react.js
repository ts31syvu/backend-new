import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';
import { SHARE_ALL_DATA_ROUTE } from 'constants/routes';

import {
  WarningButton,
  contentStyles,
  siderStyles,
  NavigationButton,
} from 'components/general';

import { LocationFooter } from 'components/App/LocationFooter';
import { useGetAllTransfersWithValidity } from 'components/hooks/queries';
import { TransferList } from './TransferList';
import {
  DataTransfersWrapper,
  Wrapper,
  Header,
  ButtonWrapper,
} from './DataTransfers.styled';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';

const { Content, Sider } = Layout;

export const DataTransfers = () => {
  const intl = useIntl();

  const {
    isLoading,
    error,
    data: transfers,
  } = useGetAllTransfersWithValidity();

  const uncompletedTransfers = (transfers || [])
    .filter(transfer => !!transfer.contactedAt && !transfer.isCompleted)
    .map(transfer => transfer.uuid);

  const shareAll = () => window.open(SHARE_ALL_DATA_ROUTE);

  if (error || isLoading) return null;

  return (
    <Layout>
      <Sider width={300} style={siderStyles}>
        <NavigationButton />
        <LanguageSwitcher />
      </Sider>
      <Layout data-cy="dataTransfers">
        <Content style={contentStyles}>
          <Wrapper>
            <Header data-cy="dataTransfersTitle">
              {intl.formatMessage({
                id: 'shareData.title',
              })}
            </Header>
            <ButtonWrapper>
              {!!uncompletedTransfers.length && (
                <WarningButton
                  onClick={shareAll}
                  disabled={!uncompletedTransfers.length}
                >
                  {intl.formatMessage({ id: 'shareData.shareAll' })}
                </WarningButton>
              )}
            </ButtonWrapper>
            <DataTransfersWrapper>
              <TransferList transfers={transfers} />
            </DataTransfersWrapper>
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
