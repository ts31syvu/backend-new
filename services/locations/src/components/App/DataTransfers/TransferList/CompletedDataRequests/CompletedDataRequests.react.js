import React from 'react';
import { useIntl } from 'react-intl';

import { Wrapper, Header, Title } from '../TransferList.styled';
import { Transfer } from './Transfer';
import { TableHeader, TableHeaderEntry } from './CompletedDataRequests.styled';

export const CompletedDataRequests = ({ tracingProcesses }) => {
  tracingProcesses.sort((a, b) => b.createdAt - a.createdAt);

  const intl = useIntl();

  return (
    <Wrapper>
      <Header>
        <Title>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.header',
          })}
        </Title>
        <div>{tracingProcesses.length}</div>
      </Header>
      <TableHeader>
        <TableHeaderEntry style={{ flex: '14%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.date',
          })}
        </TableHeaderEntry>
        <TableHeaderEntry style={{ flex: '14%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.timeFrom',
          })}
        </TableHeaderEntry>
        <TableHeaderEntry style={{ flex: '14%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.timeUntil',
          })}
        </TableHeaderEntry>
        <TableHeaderEntry style={{ flex: '28%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.location',
          })}
        </TableHeaderEntry>
        <TableHeaderEntry style={{ flex: '16%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.healthDepartment',
          })}
        </TableHeaderEntry>
        <TableHeaderEntry style={{ flex: '14%' }}>
          {intl.formatMessage({
            id: 'dataTransfers.list.complete.table.header.approvedAt',
          })}
        </TableHeaderEntry>
      </TableHeader>
      {tracingProcesses.map(tracingProcess => (
        <Transfer key={tracingProcess.uuid} transfer={tracingProcess} />
      ))}
    </Wrapper>
  );
};
