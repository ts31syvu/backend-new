import React from 'react';
import { useIntl } from 'react-intl';

import { Wrapper, Header } from '../TransferList.styled';
import { Transfer } from './Transfer';
import {
  DataTransferTitle,
  DataTransfersCount,
} from './IncompletedDataRequests.styled';

export const IncompletedDataRequests = ({ tracingProcesses }) => {
  const intl = useIntl();

  return (
    <Wrapper inComplete>
      <Header>
        <DataTransferTitle>
          {intl.formatMessage({
            id: 'dataTransfers.list.inComplete.header',
          })}
        </DataTransferTitle>
        <DataTransfersCount>{tracingProcesses.length}</DataTransfersCount>
      </Header>
      {tracingProcesses.map(tracingProcess => (
        <Transfer key={tracingProcess.uuid} transfer={tracingProcess} />
      ))}
    </Wrapper>
  );
};
