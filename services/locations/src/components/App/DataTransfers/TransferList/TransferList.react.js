import React from 'react';
import { useIntl } from 'react-intl';

import { getIncompleteTransfers, getCompleteTransfers } from 'utils/shareData';

import { IncompletedDataRequests } from './IncompletedDataRequests';
import { CompletedDataRequests } from './CompletedDataRequests';

import { NoRequests } from './TransferList.styled';

export const TransferList = ({ transfers }) => {
  const intl = useIntl();

  const incompleteTransfers = getIncompleteTransfers(transfers);

  const completeTransfers = getCompleteTransfers(transfers);

  return (
    <>
      {transfers?.length === 0 ? (
        <NoRequests>
          {intl.formatMessage({ id: 'dataTransfers.noRequests' })}
        </NoRequests>
      ) : (
        <>
          {incompleteTransfers.length > 0 && (
            <IncompletedDataRequests tracingProcesses={incompleteTransfers} />
          )}

          {completeTransfers.length > 0 && (
            <CompletedDataRequests tracingProcesses={completeTransfers} />
          )}
        </>
      )}
    </>
  );
};
