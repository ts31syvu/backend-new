import styled from 'styled-components';

export const NoRequests = styled.div`
  height: 65vh;
`;

export const Wrapper = styled.div`
  background-color: ${({ inComplete }) =>
    inComplete ? 'rgb(254, 215, 187)' : 'white'};
  margin-bottom: 24px;
  padding: 24px 32px 40px 32px;
  border-radius: 8px;
`;

export const Header = styled.div`
  display: flex;
  justify-content: space-between;
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
`;

export const Title = styled.div`
  padding: 0 8px;
`;
