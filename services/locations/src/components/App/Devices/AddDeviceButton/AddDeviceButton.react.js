import React from 'react';
import { useIntl } from 'react-intl';

import { usePrivateKey } from 'utils/privateKey';

import { PrimaryButton } from 'components/general';
import { useModal } from 'components/hooks/useModal';
import { useGetMe, useGetPrivateKeySecret } from 'components/hooks/queries';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { CreateDeviceModal } from 'components/App/modals/CreateDeviceModal';

import { Spin } from 'antd';
import { AddDeviceWrapper } from './AddDevice.styled';

export function AddDeviceButton({ isCentered = false }) {
  const intl = useIntl();
  const {
    data: privateKeySecret,
    isLoading: isPrivateKeySecretLoading,
  } = useGetPrivateKeySecret();

  const { data: operator, isLoading: isOperatorLoading } = useGetMe();
  const [openModal] = useModal();
  const [privateKey] = usePrivateKey(privateKeySecret);

  const openCreateDevice = operatorPrivateKey => {
    openModal({
      content: <CreateDeviceModal privateKey={operatorPrivateKey} />,
      emphasis: 'noHeader',
    });
  };

  const openPrivateKeyLoader = onSuccess => {
    openModal({
      content: (
        <PrivateKeyLoader
          onSuccess={onSuccess}
          publicKey={operator.publicKey}
          privateKeySecret={privateKeySecret}
          operator={operator}
        />
      ),
      closable: true,
    });
  };

  const onCreateDevice = () => {
    if (privateKey) {
      openCreateDevice(privateKey);
    } else {
      openPrivateKeyLoader(openCreateDevice);
    }
  };

  if (isPrivateKeySecretLoading || isOperatorLoading) return <Spin />;

  return (
    <AddDeviceWrapper isCentered={isCentered}>
      <PrimaryButton onClick={onCreateDevice}>
        {intl.formatMessage({ id: 'device.addDevice' })}
      </PrimaryButton>
    </AddDeviceWrapper>
  );
}
