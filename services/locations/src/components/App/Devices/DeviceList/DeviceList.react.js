import React, { useState } from 'react';
import { Badge } from 'antd';
import { useIntl } from 'react-intl';
import moment from 'moment';

import { InformationIcon, StyledTooltip } from 'components/general';
import { ReactivateDeviceModal } from 'components/App/modals/ReactivateDeviceModal';
import { useModal } from 'components/hooks/useModal';
import { DevicesDropdown } from '../DevicesDropdown';

import { ACTIVATED_COLOR, UNACTIVATED_COLOR } from './DeviceList.helper';

import {
  DeviceValue,
  StyledTable,
  ReactivateDeviceAction,
} from './DeviceList.styled';

export const DeviceList = ({ devices }) => {
  const intl = useIntl();
  const [openModal] = useModal();

  const [operatorDevices, setOperatorDevices] = useState(devices);
  const [selectedDevices, setSelectedDevices] = useState([]);

  const unknownDeviceName = intl.formatMessage({ id: 'device.unknown' });

  const onReactivate = deviceId => {
    openModal({
      content: <ReactivateDeviceModal deviceId={deviceId} />,
      emphasis: 'noHeader',
    });
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'device.list.deviceName' }),
      key: 'deviceName',
      render: function renderDeviceName(device) {
        return <DeviceValue>{device.name || unknownDeviceName}</DeviceValue>;
      },
    },
    {
      title: (
        <>
          {intl.formatMessage({ id: 'device.list.status' })}
          <StyledTooltip
            title={intl.formatMessage(
              {
                id: 'device.list.active.information',
              },
              { br: <br /> }
            )}
          >
            <InformationIcon />
          </StyledTooltip>
        </>
      ),
      key: 'deviceStatus',
      render: function renderDeviceStatus(device) {
        return (
          <DeviceValue>
            <Badge
              color={device.isExpired ? UNACTIVATED_COLOR : ACTIVATED_COLOR}
              text={intl.formatMessage({
                id: device.isExpired ? 'device.unactive' : 'device.active',
              })}
            />
          </DeviceValue>
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'device.list.lastActive' }),
      key: 'deviceLastActive',
      render: function renderDeviceLastActive(device) {
        return (
          <DeviceValue>
            {moment(device.refreshedAt).format('DD.MM.YYYY - HH:mm')}
          </DeviceValue>
        );
      },
    },
    {
      title: (
        <>
          {intl.formatMessage({ id: 'device.list.role' })}
          <StyledTooltip
            title={intl.formatMessage(
              {
                id: 'device.list.role.information',
              },
              { br: <br /> }
            )}
          >
            <InformationIcon />
          </StyledTooltip>
        </>
      ),
      key: 'deviceRole',
      render: function renderDeviceRole(device) {
        return (
          <DeviceValue>
            {intl.formatMessage({
              id: `device.role.${device.role}`,
            })}
          </DeviceValue>
        );
      },
    },
    {
      render: function renderDeviceReactivate(device) {
        return (
          device.isExpired ||
          (!device.activated && (
            <ReactivateDeviceAction
              onClick={() => onReactivate(device.deviceId)}
            >
              {intl.formatMessage({ id: 'device.list.reactivate' })}
            </ReactivateDeviceAction>
          ))
        );
      },
    },
  ];

  const onChange = selectedRowKeys => {
    setSelectedDevices(selectedRowKeys);
  };

  const rowSelection = {
    onChange,
    selectedRowKeys: selectedDevices,
  };

  const isSelectedDevice = deviceId => selectedDevices.includes(deviceId);

  const resetSelectedDevices = () => {
    setSelectedDevices([]);
  };

  const addOperatorDevices = newDevices => {
    setOperatorDevices([...newDevices]);
    resetSelectedDevices();
  };

  const handleLogoutSelectedDevices = () => {
    operatorDevices.forEach((operatorDevice, index) => {
      const logoutDevice = isSelectedDevice(operatorDevice.deviceId);

      if (logoutDevice) operatorDevices[index].activated = false;
    });

    addOperatorDevices(operatorDevices);
  };

  const handleDeleteSelectedDevices = () => {
    const filtered = operatorDevices.filter(
      ({ deviceId }) => !isSelectedDevice(deviceId)
    );

    addOperatorDevices(filtered);
  };

  const handleCancelAction = () => {
    resetSelectedDevices();
  };

  return (
    <>
      <StyledTable
        columns={columns}
        dataSource={operatorDevices}
        id="deviceListTable"
        pagination={false}
        rowKey={device => device.deviceId}
        rowSelection={rowSelection}
        showSorterTooltip={false}
      />
      {selectedDevices.length > 0 && (
        <DevicesDropdown
          onCancelAction={handleCancelAction}
          onDeleteSelectedDevices={handleDeleteSelectedDevices}
          onLogoutSelectedDevices={handleLogoutSelectedDevices}
          selectedDevices={selectedDevices}
        />
      )}
    </>
  );
};
