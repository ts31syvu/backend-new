import styled from 'styled-components';
import { Table } from 'antd';
import { PrimaryButton } from 'components/general';

export const DeviceValue = styled.div`
  flex: 1;
  font-size: 14px;
  font-weight: 500;
  overflow: hidden;
  text-overflow: ellipsis;
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-Medium, sans-serif;
`;

export const StyledTable = styled(Table)`
  margin-top: 24px;
  & .ant-checkbox-checked {
    & .ant-checkbox-inner {
      background-color: #000;
    }
  }

  & .ant-checkbox-indeterminate {
    & .ant-checkbox-inner {
      &::after {
        background-color: #000;
      }
    }
  }
`;

export const ReactivateDeviceAction = styled(PrimaryButton)`
  border: none;
  outline: none;
  cursor: pointer;
  font-size: 14px;
  box-shadow: none;
  font-weight: bold;
  color: rgb(80, 102, 124);
  background-color: transparent;
  font-family: Montserrat-Bold, sans-serif;
`;
