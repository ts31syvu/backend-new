import React, { useEffect } from 'react';
import { gt } from 'semver';
import { Layout } from 'antd';
import { useIntl } from 'react-intl';

import { RELEASE_OPERATOR_APP_NOTIFICATION } from 'constants/releaseVersions';
import { updateOperator } from 'network/api';
import { useGetMe, useGetOperatorDevices } from 'components/hooks/queries';
import { LocationFooter } from 'components/App/LocationFooter';

import {
  contentStyles,
  NavigationButton,
  siderStyles,
} from 'components/general';

// Components
import { NoDevices } from './NoDevices';
import { DeviceList } from './DeviceList';
import { AddDeviceButton } from './AddDeviceButton';

import { Wrapper, Header } from './Devices.styled';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';

const { Content, Sider } = Layout;

export const Devices = () => {
  const intl = useIntl();

  const { data: devices, isError, isLoading } = useGetOperatorDevices();

  const {
    data: operator,
    isError: operatorError,
    isLoading: operatorLoading,
  } = useGetMe();

  useEffect(() => {
    if (gt(RELEASE_OPERATOR_APP_NOTIFICATION, operator.lastVersionSeen)) {
      updateOperator({
        lastVersionSeen: process.env.REACT_APP_VERSION,
      });
    }
  }, [operator]);

  if (isError || isLoading || operatorError || operatorLoading) {
    return null;
  }

  const activatedDevices = devices?.filter(device => device.activated);

  return (
    <Layout>
      <Sider width={300} style={siderStyles}>
        <NavigationButton />
        <LanguageSwitcher />
      </Sider>
      <Layout>
        <Content style={contentStyles}>
          <Wrapper>
            {!activatedDevices.length ? (
              <NoDevices />
            ) : (
              <>
                <Header>{intl.formatMessage({ id: 'device.title' })}</Header>
                <AddDeviceButton />
                <DeviceList devices={activatedDevices} />
              </>
            )}
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
