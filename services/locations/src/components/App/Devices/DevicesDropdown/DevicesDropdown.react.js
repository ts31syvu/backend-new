import React from 'react';
import { Dropdown, Menu, notification, Popconfirm } from 'antd';

import { deleteDevices, logoutDevices } from 'network/api';
import { useDeviceTranslate } from './hooks';
import {
  Wrapper,
  StyledLink,
  StyledDropDownLink,
  StyledDownOutlined,
} from './DevicesDropdown.styled';

const ACTION_DELETE = 'delete';
const ACTION_LOGOUT = 'logout';

const http = {
  deleteDevices,
  logoutDevices,
};

export const DevicesDropdown = ({
  onCancelAction,
  onLogoutSelectedDevices,
  onDeleteSelectedDevices,
  selectedDevices,
}) => {
  const translate = useDeviceTranslate(selectedDevices.length);

  const apiService = async (action, callback) => {
    const message = status => ({
      message: translate(`${action}.notification.${status}`),
    });

    const notify = status => notification.success(message(status));

    try {
      const method = `${action}Devices`;
      await http[method](selectedDevices);

      notify('success');

      callback();
    } catch {
      notify('error');
    }
  };

  const handleDeleteDevices = () => {
    apiService(ACTION_DELETE, onDeleteSelectedDevices);
  };

  const handleLogoutDevices = () => {
    apiService(ACTION_LOGOUT, onLogoutSelectedDevices);
  };

  const menuItems = [
    { name: ACTION_LOGOUT, onConfirm: handleLogoutDevices },
    { name: ACTION_DELETE, onConfirm: handleDeleteDevices },
  ];

  const menu = (
    <Menu>
      {menuItems.map(({ name, onConfirm }) => (
        <Menu.Item key={name}>
          <Popconfirm
            cancelText={translate(`${name}.declineButton`)}
            okText={translate(`${name}.confirmButton`)}
            onCancel={onCancelAction}
            onConfirm={onConfirm}
            placement="leftTop"
            title={translate(`${name}.confirmation`)}
          >
            <StyledLink>{translate(name)}</StyledLink>
          </Popconfirm>
        </Menu.Item>
      ))}
    </Menu>
  );

  return (
    <Wrapper>
      <Dropdown overlay={menu}>
        <StyledDropDownLink onClick={event => event.preventDefault()}>
          {translate('select')}
          <StyledDownOutlined />
        </StyledDropDownLink>
      </Dropdown>
    </Wrapper>
  );
};
