import styled from 'styled-components';

import { DownOutlined } from '@ant-design/icons';

export const StyledDropDownLink = styled.a`
  font-family: Montserrat-Bold, sans-serif;
  margin-top: 24px;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  height: 32px;
  width: auto;
  text-decoration: none;
  padding-left: 40px;
  padding-right: 40px;
  padding-top: 3px;
  color: rgba(0, 0, 0, 0.87);
  border-radius: 24px;
  border: 2px solid rgb(80, 102, 124);
  transition: 0.1s ease-in-out all;

  span.anticon {
    font-size: 16px !important;
    margin-left: 8px;
    position: relative;
    top: 3px;
  }

  &:hover,
  &:focus {
    color: rgba(0, 0, 0, 0.87);
    background: rgb(218, 224, 231);
    border: 2px solid rgb(80, 102, 124);
  }
`;

export const StyledDownOutlined = styled(DownOutlined)`
  font-size: 16px !important;
  margin-left: 8px;
  position: relative;
  top: 3px;
`;

export const Wrapper = styled.div`
  position: relative;
  display: flex;
  justify-content: flex-end;
`;

export const OverlayContainter = styled.div`
  width: 172px;
  background: rgb(255, 255, 255);
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.23);
  border-radius: 4px;
  display: flex;
  position: absolute;
  flex-direction: column;
`;

export const StyledLink = styled.span`
  cursor: pointer;
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  padding: 8px;
  line-height: 20px;
`;
