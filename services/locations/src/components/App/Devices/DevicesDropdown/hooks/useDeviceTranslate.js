import { useCallback } from 'react';
import { useIntl } from 'react-intl';

export const useDeviceTranslate = count => {
  const intl = useIntl();

  return useCallback(
    key => {
      const id = `device.list.${key}`;

      const descriptor = { id };
      const options = { count };

      return intl.formatMessage(descriptor, options);
    },
    [count, intl]
  );
};
