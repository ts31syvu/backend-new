import React from 'react';
import { useIntl } from 'react-intl';
import Icon, { AppleFilled, AndroidFilled } from '@ant-design/icons';

import {
  OperatorAppIOSIcon,
  OperatorAppAndroidIcon,
  SearchIcon,
} from 'assets/icons';

import { StyledTooltip } from 'components/general';

import {
  OPERATOR_APP_ANDROID_LINK,
  OPERATOR_APP_IOS_LINK,
} from 'constants/links';

import { AddDeviceButton } from '../AddDeviceButton';

import {
  Wrapper,
  ContentWrapper,
  Title,
  SubTitle,
  Description,
  ButtonWrapper,
  StoresWrapper,
  StoreWrapper,
  NameWrapper,
  LinkContent,
} from './NoDevices.styled';

const SearchImage = () => (
  <Icon
    component={SearchIcon}
    style={{ display: 'flex', justifyContent: 'center', fontSize: 250 }}
  />
);

const IOSImage = () => (
  <Icon component={OperatorAppIOSIcon} style={{ fontSize: 160 }} />
);

const AndroidImage = () => (
  <Icon component={OperatorAppAndroidIcon} style={{ fontSize: 160 }} />
);

export const NoDevices = () => {
  const intl = useIntl();

  const onCopy = link => {
    navigator.clipboard.writeText(link);
  };

  return (
    <Wrapper>
      <ContentWrapper>
        <Title>{intl.formatMessage({ id: 'devices.noDevices.title' })}</Title>
        <Description>
          {intl.formatMessage({ id: 'devices.noDevices.description' })}
        </Description>
        <ButtonWrapper>
          <AddDeviceButton />
        </ButtonWrapper>
        <SubTitle>
          {intl.formatMessage({ id: 'devices.noDevices.subTitle' })}
        </SubTitle>
        <Description>
          {intl.formatMessage({ id: 'devices.noDevices.descriptionDownload' })}
        </Description>
        <StoresWrapper>
          <StoreWrapper>
            <NameWrapper>
              <AppleFilled style={{ marginRight: 10 }} />
              {intl.formatMessage({ id: 'devices.noDevices.iOS' })}
            </NameWrapper>
            <IOSImage />
            <StyledTooltip
              title={intl.formatMessage({
                id: 'tooltip.copy',
              })}
              trigger="click"
              small
            >
              <LinkContent onClick={() => onCopy(OPERATOR_APP_IOS_LINK)}>
                {intl.formatMessage({
                  id: 'modal.checkInOptions.copyLink',
                })}
              </LinkContent>
            </StyledTooltip>
          </StoreWrapper>
          <StoreWrapper>
            <NameWrapper>
              <AndroidFilled style={{ marginRight: 10 }} />
              {intl.formatMessage({ id: 'devices.noDevices.android' })}
            </NameWrapper>
            <AndroidImage />
            <StyledTooltip
              title={intl.formatMessage({
                id: 'tooltip.copy',
              })}
              trigger={['click']}
              small
            >
              <LinkContent onClick={() => onCopy(OPERATOR_APP_ANDROID_LINK)}>
                {intl.formatMessage({
                  id: 'modal.checkInOptions.copyLink',
                })}
              </LinkContent>
            </StyledTooltip>
          </StoreWrapper>
        </StoresWrapper>
        <SearchImage />
      </ContentWrapper>
    </Wrapper>
  );
};
