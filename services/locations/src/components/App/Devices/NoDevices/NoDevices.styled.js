import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 40px;
  margin-bottom: auto;
`;

export const ContentWrapper = styled.div`
  width: 450px;
  margin: auto;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
  display: flex;
  justify-content: center;
`;

export const SubTitle = styled(Title)`
  font-size: 14px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 32px;
`;

export const ButtonWrapper = styled.div`
  margin-bottom: 32px;
  > div {
    justify-content: center;
  }
`;

export const StoresWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 80px;
`;

export const StoreWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const NameWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 8px;
`;

export const LinkContent = styled.div`
  cursor: pointer;
  color: rgb(80, 102, 124);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 12px;
  font-weight: bold;
  text-align: center;
  margin-top: 8px;
`;
