import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { AddCircleBlueIcon } from 'assets/icons';

import { CreateGroupModal } from 'components/App/modals/CreateGroupModal';

import { Wrapper, CreateText } from './CreateGroup.styled';

export const CreateGroup = () => {
  const intl = useIntl();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const onCloseModal = () => setIsModalOpen(false);
  const onOpenModal = () => setIsModalOpen(true);

  return (
    <>
      <Wrapper data-cy="createGroup" onClick={onOpenModal}>
        <Icon component={AddCircleBlueIcon} style={{ fontSize: 14 }} />
        <CreateText>
          {intl.formatMessage({ id: 'groupList.createGroup' })}
        </CreateText>
      </Wrapper>
      {isModalOpen && <CreateGroupModal onClose={onCloseModal} />}
    </>
  );
};
