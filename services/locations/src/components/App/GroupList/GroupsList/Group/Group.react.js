import React from 'react';

import { ArrowDownBlueIcon } from 'assets/icons';
import {
  BASE_GROUP_SETTINGS_ROUTE,
  BASE_GROUP_ROUTE,
  BASE_LOCATION_ROUTE,
} from 'constants/routes';

import { StyledIcon } from '../../GroupList.styled';
import { useGroupsNavigation } from '../../hooks';
import { GroupLocations } from './GroupLocations';
import { GroupName, GroupNameWrapper } from './Group.styled';

const ArrowIcon = ({ isActive }) => (
  <StyledIcon component={ArrowDownBlueIcon} rotate={isActive ? 0 : -90} />
);

export const Group = ({ group: { locations, groupId, name } }) => {
  const { open, group: activeGroup } = useGroupsNavigation();

  const openGroup = () => {
    open(`${BASE_GROUP_SETTINGS_ROUTE}${groupId}`);
  };

  const openLocation = location => {
    const { uuid } = location;

    open(`${BASE_GROUP_ROUTE}${groupId}${BASE_LOCATION_ROUTE}${uuid}`);
  };

  return (
    <>
      <GroupNameWrapper
        onClick={openGroup}
        isActiveGroup={
          groupId === activeGroup.groupId && !activeGroup.locationId
        }
        data-cy={`groupItem-${groupId}`}
      >
        <ArrowIcon isActive={groupId === activeGroup.groupId} />
        <GroupName data-cy={`location-${name}`}>{name}</GroupName>
      </GroupNameWrapper>
      <GroupLocations
        openLocation={openLocation}
        groupId={groupId}
        locations={locations}
      />
    </>
  );
};
