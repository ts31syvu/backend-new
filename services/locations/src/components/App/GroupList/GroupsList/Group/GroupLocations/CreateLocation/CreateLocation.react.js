import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import Icon from '@ant-design/icons';

import { AddCircleBlueIcon } from 'assets/icons';

import { CreateLocationModal } from 'components/App/modals/CreateLocationModal';

import { Wrapper, CreateText } from './CreateLocation.styled';

export const CreateLocation = ({ groupId }) => {
  const intl = useIntl();
  const [isModalOpen, setIsModalOpen] = useState(false);

  const onCloseModal = () => setIsModalOpen(false);
  const onOpenModal = () => setIsModalOpen(true);

  return (
    <>
      <Wrapper data-cy={`createLocation-${groupId}`} onClick={onOpenModal}>
        <Icon component={AddCircleBlueIcon} style={{ fontSize: 14 }} />
        <CreateText>
          {intl.formatMessage({ id: 'groupList.createLocation' })}
        </CreateText>
      </Wrapper>
      {isModalOpen && (
        <CreateLocationModal onClose={onCloseModal} groupId={groupId} />
      )}
    </>
  );
};
