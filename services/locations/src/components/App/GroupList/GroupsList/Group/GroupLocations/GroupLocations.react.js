import React, { useMemo } from 'react';

import { useIntl } from 'react-intl';

import { useGroupsNavigation } from '../../../hooks';
import { CreateLocation } from './CreateLocation';
import { sortLocations } from './GroupLocations.helper';
import { Location } from './GroupLocations.styled';

export const GroupLocations = ({ locations, groupId, openLocation }) => {
  const intl = useIntl();

  const { group } = useGroupsNavigation();

  const memoLocations = useMemo(() => sortLocations(locations), [locations]);

  if (groupId !== group.groupId) return null;

  return (
    <>
      {memoLocations.map(location => (
        <Location
          key={location.uuid}
          data-cy={`location-${location.name || 'General'}`}
          onClick={() => openLocation(location)}
          isActiveLocation={location.uuid === group.locationId}
        >
          {location.name || intl.formatMessage({ id: 'location.defaultName' })}
        </Location>
      ))}
      <CreateLocation groupId={groupId} />
    </>
  );
};
