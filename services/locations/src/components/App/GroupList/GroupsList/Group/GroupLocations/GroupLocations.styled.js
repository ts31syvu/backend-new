import styled from 'styled-components';

export const Location = styled.div`
  cursor: pointer;
  overflow: hidden;
  text-overflow: ellipsis;
  background: ${({ isActiveLocation }) =>
    isActiveLocation ? 'rgb(243, 245, 247)' : 'rgba(243, 245, 247, 0.45)'};
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  border-bottom: 1px solid rgb(248, 251, 255);
  padding: 15px 0 15px 50px;

  &:active,
  &:hover {
    background: rgb(243, 245, 247);
  }
`;
