import React from 'react';

import { Group } from './Group';
import { GroupComp, GroupContainer, ListWrapper } from './GroupsList.styled';

export const GroupsList = ({ groups }) => {
  return (
    <ListWrapper>
      {groups.map(group => {
        return (
          <GroupContainer key={group.groupId}>
            <GroupComp data-cy={group.name}>
              <Group group={group} />
            </GroupComp>
          </GroupContainer>
        );
      })}
    </ListWrapper>
  );
};
