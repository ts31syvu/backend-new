import styled from 'styled-components';

export const GroupComp = styled.div`
  cursor: pointer;
  overflow: hidden;
  text-overflow: ellipsis;
  border-bottom: 1px solid rgb(248, 251, 255);
`;

export const GroupContainer = styled.div``;

export const ListWrapper = styled.div`
  padding: 8px 0;
  height: auto;
  max-height: calc(100vh - 120px);
  overflow-y: auto;
`;
