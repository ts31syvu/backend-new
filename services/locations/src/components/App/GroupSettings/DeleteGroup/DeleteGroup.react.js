import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { useHistory } from 'react-router';
import { notification, Popconfirm } from 'antd';
import { DangerButton } from 'components/general';
import { QuestionCircleOutlined } from '@ant-design/icons';

import { BASE_GROUP_ROUTE } from 'constants/routes';
import { deleteGroup } from 'network/api';

import { PasswordModal } from 'components/App/modals/PasswordModal';

import { useGetPaymentOnboardingStatus } from 'components/hooks/queries';
import { Wrapper, Heading, ButtonWrapper, Info } from './DeleteGroup.styled';

export const DeleteGroup = ({ group }) => {
  const intl = useIntl();
  const history = useHistory();
  const queryClient = useQueryClient();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const { data: paymentOnboardingStatus } = useGetPaymentOnboardingStatus(
    group.groupId
  );

  const handleError = errorString => {
    notification.error({
      message: intl.formatMessage({
        id: errorString,
      }),
    });
  };

  const onDelete = password => {
    deleteGroup(group.groupId, { password })
      .then(async response => {
        if (response.status === 204) {
          setIsModalOpen(false);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.deleteGroup.success',
            }),
          });
          await queryClient.invalidateQueries('groups');
          history.push(BASE_GROUP_ROUTE);
        } else if (response.status === 403) {
          handleError('notification.deleteLocation.error.incorrectPassword');
        } else {
          handleError('notification.deleteGroup.error');
        }
      })
      .catch(() => {
        setIsModalOpen(false);
        handleError('notification.deleteGroup.error');
      });
  };

  const isPaymentEnabled = paymentOnboardingStatus?.status === 'DONE';

  return (
    <>
      <Wrapper>
        <Heading>
          {intl.formatMessage({
            id: isPaymentEnabled
              ? 'settings.group.delete.paymentEnabled'
              : 'settings.group.delete',
          })}
        </Heading>
        <Info>
          {intl.formatMessage({
            id: isPaymentEnabled
              ? 'settings.group.delete.info.paymentEnabled'
              : 'settings.group.delete.info',
          })}
        </Info>
        <ButtonWrapper>
          <Popconfirm
            placement="topLeft"
            onConfirm={() => setIsModalOpen(true)}
            title={intl.formatMessage({
              id: isPaymentEnabled
                ? 'group.delete.confirmText.paymentEnabled'
                : 'group.delete.confirmText',
            })}
            okText={intl.formatMessage({ id: 'location.delete.confirmButton' })}
            cancelText={intl.formatMessage({
              id: 'location.delete.declineButton',
            })}
            icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
          >
            <DangerButton data-cy="deleteGroup">
              {intl.formatMessage({ id: 'settings.group.delete.submit' })}
            </DangerButton>
          </Popconfirm>
        </ButtonWrapper>
      </Wrapper>
      {isModalOpen && (
        <PasswordModal
          callback={onDelete}
          onClose={() => setIsModalOpen(false)}
        />
      )}
    </>
  );
};
