import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';

import { Switch } from 'components/general';
import { AreaRow } from '../AnonymousCheckins.styled';

export const AnonymousCheckinRow = ({
  locationUuid,
  toggleMandatoryCheckin,
  switchMandatoryLabel,
  location,
  isLabelVisible,
}) => {
  const intl = useIntl();

  const [isContactDataMandatory, setIsContactDataMandatory] = useState(
    location?.isContactDataMandatory
  );

  useEffect(() => {
    setIsContactDataMandatory(location?.isContactDataMandatory);
  }, [location]);

  return (
    <AreaRow key={locationUuid}>
      <div>
        {location.name || intl.formatMessage({ id: 'location.defaultName' })}
      </div>
      <Switch
        checked={isContactDataMandatory}
        onChange={() => {
          setIsContactDataMandatory(!isContactDataMandatory);
          toggleMandatoryCheckin(location);
        }}
        customCheckedLabel={intl.formatMessage({
          id: switchMandatoryLabel,
        })}
        isLabelVisible={isLabelVisible}
      />
    </AreaRow>
  );
};
