import React from 'react';
import { useIntl } from 'react-intl';
import { notification, Checkbox } from 'antd';
import { useQueryClient } from 'react-query';

import { updateLocation } from 'network/api';

import { QUERY_KEYS } from 'components/hooks/queries';

import {
  Wrapper,
  Header,
  HeaderWrapper,
  StyledAreaContainer,
  StyledCheckboxContainer,
  StyledCheckboxLabel,
  Description,
} from './AnonymousCheckins.styled';
import { AnonymousCheckinRow } from './AnonymousCheckinRow';

export const AnonymousCheckins = ({
  group,
  locations,
  allChecked,
  indeterminate,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const switchMandatoryLabel = 'switch.allMandatory';

  const toggleMandatoryCheckin = (location, value = null) => {
    updateLocation({
      locationId: location.uuid,
      data: {
        isContactDataMandatory: value || !location.isContactDataMandatory,
      },
    })
      .then(() => {
        queryClient.invalidateQueries([QUERY_KEYS.GROUP, group.groupId]);
        queryClient.invalidateQueries([QUERY_KEYS.LOCATION, location.uuid]);
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'notification.updateLocation.error',
          }),
        });
      });
  };

  const onCheckAllChange = () => {
    const noneChecked = locations.every(
      location => !location.isContactDataMandatory
    );

    if (noneChecked) {
      locations.map(location => toggleMandatoryCheckin(location, true));
      return;
    }

    locations.forEach(location => {
      if (location.isContactDataMandatory) {
        toggleMandatoryCheckin(location, false);
      }
    });
  };

  return (
    <Wrapper>
      <HeaderWrapper>
        <Header>
          {intl.formatMessage({
            id: 'group.view.overview.anonymousCheckins.title',
          })}
        </Header>
      </HeaderWrapper>
      <Description>
        {intl.formatMessage({
          id: 'group.view.overview.anonymousCheckins.description',
        })}
      </Description>
      <StyledAreaContainer>
        <StyledCheckboxContainer>
          <StyledCheckboxLabel>
            {intl.formatMessage({ id: switchMandatoryLabel })}
          </StyledCheckboxLabel>
          <Checkbox
            onChange={onCheckAllChange}
            checked={allChecked}
            indeterminate={indeterminate && !allChecked}
          />
        </StyledCheckboxContainer>
        {locations.map(location => (
          <AnonymousCheckinRow
            key={location.uuid}
            locationUuid={location.uuid}
            location={location}
            toggleMandatoryCheckin={toggleMandatoryCheckin}
            switchMandatoryLabel={switchMandatoryLabel}
            isLabelVisible
          />
        ))}
      </StyledAreaContainer>
    </Wrapper>
  );
};
