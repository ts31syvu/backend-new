import styled from 'styled-components';
import { Select } from 'antd';

const { Option } = Select;

export const Wrapper = styled.div`
  padding: 0 32px 0;
`;

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  margin-top: 32px;
`;

export const StyledAreaContainer = styled.div`
  margin-top: 24px;
`;

export const AreaRow = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid black;
  padding: 16px 0;
`;

export const StyledSelect = styled(Select)`
  width: 200px;
`;

export const StyledOption = styled(Option)``;

export const Name = styled.div`
  width: 40%;
`;

export const StyledSwitchWrapper = styled.div`
  width: 10%;
  & > div {
    justify-content: end;
  }
`;
