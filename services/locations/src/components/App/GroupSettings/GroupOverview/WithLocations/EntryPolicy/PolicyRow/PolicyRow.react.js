import React, { useEffect, useState } from 'react';
import { Switch } from 'components/general';
import { useIntl } from 'react-intl';

import { useGetLocationById } from 'components/hooks/queries';
import {
  AreaRow,
  Name,
  StyledOption,
  StyledSelect,
  StyledSwitchWrapper,
} from '../EntryPolicy.styled';

export const PolicyRow = ({
  locationUuid,
  getDefaultValue,
  handlePolicyChange,
  entryPolicies,
  toggleEntryPolicy,
}) => {
  const intl = useIntl();

  const { isLoading, error, data: location } = useGetLocationById(locationUuid);

  const [isEntryPolicyActive, setIsEntryPolicyActive] = useState(
    !!location?.entryPolicy || false
  );

  useEffect(() => {
    setIsEntryPolicyActive(!!location?.entryPolicy);
  }, [location]);

  if (isLoading || error) return null;

  return (
    <AreaRow key={locationUuid}>
      <Name>
        {location.name || intl.formatMessage({ id: 'location.defaultName' })}
      </Name>
      <StyledSelect
        disabled={!isEntryPolicyActive}
        defaultValue={getDefaultValue(location)}
        onChange={value => handlePolicyChange(location, value)}
      >
        {entryPolicies.map(policy => (
          <StyledOption key={policy.id} value={policy.value}>
            {intl.formatMessage({ id: policy.locale })}
          </StyledOption>
        ))}
      </StyledSelect>
      <StyledSwitchWrapper>
        <Switch
          checked={isEntryPolicyActive}
          onChange={() => {
            setIsEntryPolicyActive(!isEntryPolicyActive);

            toggleEntryPolicy(location);
          }}
        />
      </StyledSwitchWrapper>
    </AreaRow>
  );
};
