import styled from 'styled-components';

const AreaRow = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid black;
  padding: 16px 0;
`;

export const HeaderRow = styled(AreaRow)`
  display: flex;
  justify-content: space-between;
  border-bottom: 2px solid black;
  padding: 16px 0;
`;

const RowTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  width: 200px;
`;

export const RowTitleName = styled(RowTitle)`
  width: 40%;
`;

export const RowTitlePolicy = styled(RowTitle)`
  width: 200px;
`;

export const RowTitleSwitch = styled(RowTitle)`
  width: 10%;
`;
