import styled from 'styled-components';

export const Counter = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 34px;
  font-weight: 500;
`;

export const InfoWrapper = styled.div`
  padding-right: 32px;
`;

export const HorizontalLine = styled.div`
  height: 40px;
  width: 1px;
  background-color: black;
  margin-top: 32px;
`;

export const ButtonWrapper = styled.div`
  margin-left: auto;
  margin-top: 32px;
`;
