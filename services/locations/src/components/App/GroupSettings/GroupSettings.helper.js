export const PROFILE_TAB = 'openLocationSettings';
export const CHECK_IN_TAB = 'openLocationCheckin';
export const PAYMENT_TAB = 'openLocationPayment';
export const validTabs = new Set([PROFILE_TAB, CHECK_IN_TAB, PAYMENT_TAB]);
