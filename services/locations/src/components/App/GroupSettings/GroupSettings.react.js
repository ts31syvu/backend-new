import React, { useMemo, useState } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import { Layout } from 'antd';
import { useIntl } from 'react-intl';
import { getOperatorPaymentEnabled } from 'network/payment';

import { LocationFooter } from 'components/App/LocationFooter';
import { GroupList } from 'components/App/GroupList';
import { contentStyles, siderStyles } from 'components/general';
import { useGetGroups } from 'components/hooks/queries';
import { useQuery } from 'react-query';
import { SettingsOverview } from './SettingsOverview';
import { GroupOverview } from './GroupOverview';
import { DeleteGroup } from './DeleteGroup';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';
import {
  Wrapper,
  Header,
  SettingsContent,
  StyledTabs,
  StyledTabPane,
  StyledSider,
} from './GroupSettings.styled';
import { Payment } from './Payment';
import { validTabs } from './GroupSettings.helper';

const { Content } = Layout;

export const GroupSettings = ({ operator }) => {
  const intl = useIntl();
  const { groupId } = useParams();
  const { search } = useLocation();

  const queryParameters = React.useMemo(() => new URLSearchParams(search), [
    search,
  ]);

  const tabQueryParameter = queryParameters.get('tab');

  const defaultTab =
    tabQueryParameter && validTabs.has(tabQueryParameter)
      ? tabQueryParameter
      : 'openLocationCheckin';

  const [activeTab, setActiveTab] = useState(defaultTab);

  const { isLoading, error, data: paymentEnabled } = useQuery(
    ['paymentEnabled', operator],
    () => getOperatorPaymentEnabled(operator.operatorId)
  );

  const isPaymentEnabled = paymentEnabled && paymentEnabled.paymentEnabled;

  const { data: groups } = useGetGroups();

  const group = useMemo(() => groups?.find(item => item.groupId === groupId), [
    groups,
    groupId,
  ]);

  if (!group || isLoading || error) return null;

  return (
    <Layout>
      <StyledSider width={300} style={siderStyles}>
        <GroupList />
        <LanguageSwitcher />
      </StyledSider>

      <Layout>
        <Content style={contentStyles}>
          <Wrapper>
            <Header data-cy="groupSettingsHeader">{group.name}</Header>
            <StyledTabs
              activeKey={activeTab}
              onTabClick={key => setActiveTab(key)}
            >
              <StyledTabPane
                tab={intl.formatMessage({ id: 'location.tabs.profile' })}
                key="openLocationSettings"
              >
                <SettingsContent>
                  <SettingsOverview group={group} />
                  <DeleteGroup group={group} />
                </SettingsContent>
              </StyledTabPane>
              <StyledTabPane
                tab={intl.formatMessage({ id: 'location.tabs.checkin' })}
                key="openLocationCheckin"
              >
                <GroupOverview group={group} />
              </StyledTabPane>
              ,
              {isPaymentEnabled && (
                <StyledTabPane
                  tab={intl.formatMessage({ id: 'groupSettings.tabs.payment' })}
                  key="openLocationPayment"
                >
                  <Payment locationGroup={group} operator={operator} />
                </StyledTabPane>
              )}
            </StyledTabs>
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
