import styled from 'styled-components';
import { Layout, Tabs } from 'antd';

const { Sider } = Layout;

const { TabPane } = Tabs;

export const Wrapper = styled.div`
  margin: 40px 80px;
  display: flex;
  overflow-x: hidden;
  flex-direction: column;
  .ant-tabs-content {
    padding-bottom: 16px;
  }
`;

export const Header = styled.div`
  color: rgb(0, 0, 0);
  font-size: 34px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const SettingsContent = styled.div`
  background-color: white;
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 16px;
  padding: 24px 32px;
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;

export const Overview = styled.div`
  border-bottom: 1px solid rgb(151, 151, 151);
`;

export const StyledTabs = styled(Tabs)`
  .ant-tabs-tab.ant-tabs-tab-active > .ant-tabs-tab-btn {
    color: rgb(0, 0, 0);
    font-size: 16px;
    font-weight: bold;
  }
  .ant-tabs-tab {
    color: rgb(0, 0, 0);
    font-size: 16px;
    font-weight: 500;
  }
  .ant-tabs-nav {
    border-bottom: 0.5px solid rgb(0, 0, 0);
  }
  .ant-tabs-ink-bar {
    background: #f1743c;
    border: 3px solid rgb(241, 116, 60);
    border-radius: 4px;
  }
  .ant-tabs-tab + .ant-tabs-tab {
    margin: 0 0 0 80px;
  }
  .ant-tabs-tab-disabled {
    color: rgba(0, 0, 0, 0.25);
  }
`;

export const StyledTabPane = styled(TabPane)``;

export const StyledSider = styled(Sider)`
  width: 300px;
`;
