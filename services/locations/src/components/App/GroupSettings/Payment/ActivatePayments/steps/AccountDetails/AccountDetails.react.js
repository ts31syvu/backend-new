import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import {
  Content,
  ExternalLinkButton,
} from 'components/App/GroupSettings/Payment/ActivatePayments/ActivatePayments.styled';
import { initiatePayoutOnboarding } from 'network/payment';
import { notification, Popconfirm } from 'antd';
import {
  ButtonSubline,
  ButtonWrapper,
  ContentWrapper,
} from './AccountDetails.styled';

export const AccountDetails = ({ locationGroup, payoutUrl }) => {
  const intl = useIntl();
  const [isLoading, setIsLoading] = useState(false);

  const displayErrorNotification = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'payment.location.paymentAccountDetails.error',
      }),
    });

  const accountDetailsURLHandler = () => {
    if (payoutUrl) {
      window.open(payoutUrl);
      return;
    }
    setIsLoading(true);
    initiatePayoutOnboarding(locationGroup.groupId)
      .then(response => response.json())
      .then(data => {
        setIsLoading(false);
        if (data && data.payoutUrl) {
          window.open(data.payoutUrl);
        } else {
          displayErrorNotification();
        }
      })
      .catch(() => {
        setIsLoading(false);
        displayErrorNotification();
      });
  };

  return (
    <>
      <Content>
        {intl.formatMessage(
          { id: 'payment.location.accountDetails.text' },
          { br: <br /> }
        )}
      </Content>
      <ContentWrapper>
        <ButtonWrapper>
          <Popconfirm
            placement="bottomRight"
            title={intl.formatMessage(
              {
                id:
                  'payment.location.accountDetails.button.popConfirm.addBankDetails.text',
              },
              {
                br: <br />,
              }
            )}
            okText={intl.formatMessage({
              id:
                'payment.location.accountDetails.button.popConfirm.addBankDetails.ok',
            })}
            cancelText={intl.formatMessage({
              id:
                'payment.location.accountDetails.button.popConfirm.addBankDetails.cancel',
            })}
            onConfirm={accountDetailsURLHandler}
          >
            <ExternalLinkButton loading={isLoading}>
              {intl.formatMessage({
                id: 'payment.location.accountDetails.button',
              })}
            </ExternalLinkButton>
          </Popconfirm>
          <ButtonSubline>
            {intl.formatMessage({
              id: 'payment.location.accountDetails.button.subline',
            })}
          </ButtonSubline>
        </ButtonWrapper>
      </ContentWrapper>
    </>
  );
};
