import styled from 'styled-components';

export const ButtonSubline = styled.p`
  color: rgb(0, 0, 0);
  font-size: 12px;
  font-weight: 500;
  font-style: italic;
  text-align: center;
  letter-spacing: 0;
  line-height: 20px;
  margin-top: 8px;
`;

export const ButtonWrapper = styled.div`
  width: auto;
`;

export const ContentWrapper = styled.div`
  display: flex;
  justify-content: end;
`;
