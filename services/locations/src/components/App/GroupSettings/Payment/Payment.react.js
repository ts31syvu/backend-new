import React from 'react';

import { useQuery } from 'react-query';
import { getOnboardingStatus } from 'network/payment';
import { queryDoNotRetryOnCode } from 'network/utlis';
import { PaymentHistory } from 'components/general/PaymentHistory';
import { ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import { PayoutHistory } from 'components/general/PayoutHistory';
import { QUERY_KEYS } from 'components/hooks/queries';
import { ActivatePayments } from './ActivatePayments';
import { PayoutConfiguration } from './PayoutConfiguration';
import { PayoutBalance } from './PayoutBalance';
import { PaymentOnboardingRejected } from './PaymentOnboardingRejected';
import { PaymentOnboarding } from './PaymentOnboarding';

export const Payment = ({ operator, locationGroup }) => {
  const { isLoading, error, data: onboardingStatus } = useQuery(
    [QUERY_KEYS.PAYMENT_ONBOARDING_STATUS, locationGroup.groupId],
    () => getOnboardingStatus(locationGroup.groupId),
    {
      retry: queryDoNotRetryOnCode([401, 404]),
    }
  );

  if (isLoading || error) return null;

  const PaymentStep = () => {
    switch (onboardingStatus.status) {
      case ONBOARDING_STATUS.KYB_SUBMITTED: // missing on backend
        return (
          <ActivatePayments currentStep={0} locationGroup={locationGroup} />
        );
      case ONBOARDING_STATUS.KYB_COMPLETED:
      case ONBOARDING_STATUS.PAYOUT_PENDING:
        return (
          <ActivatePayments
            currentStep={1}
            locationGroup={locationGroup}
            payoutUrl={onboardingStatus.payoutUrl}
          />
        );
      case ONBOARDING_STATUS.KYB_REJECTED:
        return <PaymentOnboardingRejected operator={operator} />;
      case ONBOARDING_STATUS.DONE:
        return (
          <>
            <PayoutBalance locationGroupId={locationGroup.groupId} />
            <PayoutConfiguration locationGroup={locationGroup} />
            <PaymentHistory locationGroupId={locationGroup.groupId} />
            <PayoutHistory locationGroupId={locationGroup.groupId} />
          </>
        );
      default:
      case ONBOARDING_STATUS.GROUP_NOT_FOUND: // location group does not exist
      case ONBOARDING_STATUS.NOT_STARTED: // kyb url not created
      case ONBOARDING_STATUS.KYB_PENDING: // kyb url exi
        return (
          <PaymentOnboarding
            onboardingStatus={onboardingStatus}
            operator={operator}
            locationGroup={locationGroup}
          />
        );
    }
  };

  return <PaymentStep />;
};
