import React from 'react';
import { Form, Checkbox } from 'antd';
import { useIntl } from 'react-intl';
import {
  LOCATION_LUCA_PAY_DPA_LINK,
  LOCATION_LUCA_PAY_TERMS_CONDITIONS_LINK,
} from 'constants/links';

export const ConsentCheckbox = ({
  acceptedDPA,
  setAcceptedDPA,
  acceptedTermsAndConditions,
  setAcceptedTermsAndConditions,
}) => {
  const intl = useIntl();

  return (
    <Form>
      <Form.Item
        name="termsAndConditions"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(
                    intl.formatMessage({
                      id: 'error.termsAndConditions',
                    })
                  ),
          },
        ]}
      >
        <Checkbox
          data-cy="termsAndConditionsCheckbox"
          onChange={() => {
            setAcceptedTermsAndConditions(!acceptedTermsAndConditions);
          }}
        >
          {intl.formatMessage(
            {
              id:
                'payment.activePayment.consentCheckbox.acceptTermsAndConditions',
            },
            {
              // eslint-disable-next-line react/display-name
              a: (...chunks) => (
                <a
                  href={LOCATION_LUCA_PAY_TERMS_CONDITIONS_LINK}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {chunks}
                </a>
              ),
            }
          )}
        </Checkbox>
      </Form.Item>
      <Form.Item
        name="dpa"
        valuePropName="checked"
        rules={[
          {
            validator: (_, value) =>
              value
                ? Promise.resolve()
                : Promise.reject(
                    intl.formatMessage({
                      id: 'error.dpa',
                    })
                  ),
          },
        ]}
      >
        <Checkbox
          data-cy="dpaCheckbox"
          onChange={() => {
            setAcceptedDPA(!acceptedDPA);
          }}
        >
          {intl.formatMessage(
            { id: 'payment.activePayment.consentCheckbox.acceptDPA' },
            {
              // eslint-disable-next-line react/display-name
              a: (...chunks) => (
                <a
                  href={LOCATION_LUCA_PAY_DPA_LINK}
                  download
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  {chunks}
                </a>
              ),
            }
          )}
        </Checkbox>
      </Form.Item>
    </Form>
  );
};
