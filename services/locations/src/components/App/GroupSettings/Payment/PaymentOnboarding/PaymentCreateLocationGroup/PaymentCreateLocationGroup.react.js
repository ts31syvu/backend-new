import { useIntl } from 'react-intl';
import React, { useState } from 'react';
import { whitespaceTrim } from 'utils/stringHelper';
import { createLocationGroup } from 'network/payment';
import { SubmitButtonPayment } from 'components/general/Form/SubmitButtonPayment';
import { Form } from 'antd';
import { AppForm, AppFormField } from 'components/general/Form';
import * as Yup from 'yup';
import { getE164PhoneValidationSchema } from 'components/general/Form/ValidationHooks';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS } from 'components/hooks/queries';
import {
  ButtonWrapper,
  ErrorText,
  FieldRow,
  PoweredBy,
  PoweredByWrapper,
  styledInput,
  StyledTitle,
} from './PaymentCreateLocationGroup.styled';
import { ConsentCheckbox } from './ConsentCheckbox';

const FormItemByKey = ({ namedKey, translationId }) => {
  const intl = useIntl();

  if (!namedKey) {
    return null;
  }

  return (
    <Form.Item
      label={intl.formatMessage({
        id: translationId,
      })}
      name={namedKey}
    >
      <AppFormField name={namedKey} className={styledInput} />
    </Form.Item>
  );
};

// eslint-disable-next-line complexity
export const PaymentCreateLocationGroup = ({ operator, locationGroup }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [hasError, setHasError] = useState(false);
  const [isLoading, setLoading] = useState(false);
  const [acceptedDPA, setAcceptedDPA] = useState(false);
  const [acceptedTermsAndConditions, setAcceptedTermsAndConditions] = useState(
    false
  );

  if (!operator) {
    return null;
  }

  const errorMissingFiled = () =>
    intl.formatMessage({
      id: 'createGroup.errorMissingField',
    });
  const errorEmail = () =>
    intl.formatMessage({
      id: 'error.email',
    });

  const errorMissingTaxId = () =>
    intl.formatMessage({
      id: 'createGroup.errorMissingTaxId',
    });

  const validatorSchema = () =>
    Yup.object().shape({
      phone: getE164PhoneValidationSchema(intl),
      companyName: Yup.string().required(errorMissingFiled()),
      businessFirstName: Yup.string().required(errorMissingFiled()),
      businessLastName: Yup.string().required(errorMissingFiled()),
      street: Yup.string().required(errorMissingFiled()),
      streetNumber: Yup.string().required(errorMissingFiled()),
      zipCode: Yup.string().required(errorMissingFiled()),
      city: Yup.string().required(errorMissingFiled()),
      invoiceEmail: Yup.string().email(errorEmail()).required(errorEmail()),
      vatNumber: Yup.string()
        .min(3, errorMissingTaxId())
        .max(12, errorMissingTaxId())
        .required(errorMissingTaxId()), // At least something, international rule is close to be important
    });

  const initialFormValues = {
    phone: operator.phone ?? '',
    companyName: operator.businessEntityName ?? '',
    businessFirstName: operator.firstName ?? '',
    businessLastName: operator.lastName ?? '',
    street: operator.businessEntityStreetName ?? '',
    streetNumber: operator.businessEntityStreetNumber ?? '',
    zipCode: operator.businessEntityZipCode ?? '',
    city: operator.businessEntityCity ?? '',
    invoiceEmail: operator.email ?? '',
    vatNumber: '',
  };

  const startCreatingLocationGroup = (
    phone,
    companyName,
    businessFirstName,
    businessLastName,
    street,
    streetNumber,
    zipCode,
    city,
    invoiceEmail,
    vatNumber
  ) => {
    setLoading(true);
    const phoneNumberTrim = whitespaceTrim(phone);
    createLocationGroup(
      locationGroup.groupId,
      phoneNumberTrim,
      companyName,
      businessFirstName,
      businessLastName,
      street,
      streetNumber,
      zipCode,
      city,
      invoiceEmail,
      vatNumber
    )
      .then(response => {
        setLoading(false);
        if (response.status === 201) {
          queryClient.invalidateQueries(QUERY_KEYS.PAYMENT_ONBOARDING_STATUS);
        }
      })
      .catch(() => {
        setLoading(false);
        setHasError(true);
      });
  };

  return (
    <AppForm
      initialValues={initialFormValues}
      onSubmit={values =>
        startCreatingLocationGroup(
          values.phone,
          values.companyName,
          values.businessFirstName,
          values.businessLastName,
          values.street,
          values.streetNumber,
          values.zipCode,
          values.city,
          values.invoiceEmail,
          values.vatNumber
        )
      }
      validationSchema={validatorSchema()}
    >
      <FormItemByKey namedKey="phone" translationId="createGroup.phone" />

      <StyledTitle>
        {intl.formatMessage({
          id: 'payment.activate.invoiceDetailsHeadline',
        })}
      </StyledTitle>

      <FieldRow>
        <FormItemByKey
          namedKey="companyName"
          translationId="createGroup.companyName"
        />
        <FormItemByKey
          namedKey="vatNumber"
          translationId="createGroup.vatNumber"
        />
      </FieldRow>

      <FieldRow>
        <FormItemByKey
          namedKey="businessFirstName"
          translationId="createGroup.businessFirstName"
        />
        <FormItemByKey
          namedKey="businessLastName"
          translationId="createGroup.businessLastName"
        />
      </FieldRow>

      <FieldRow>
        <FormItemByKey namedKey="street" translationId="createGroup.street" />
        <FormItemByKey
          namedKey="streetNumber"
          translationId="createGroup.streetNumber"
        />
      </FieldRow>

      <FieldRow>
        <FormItemByKey namedKey="zipCode" translationId="createGroup.zipCode" />
        <FormItemByKey namedKey="city" translationId="createGroup.city" />
      </FieldRow>

      <FormItemByKey
        namedKey="invoiceEmail"
        translationId="createGroup.invoiceEmail"
      />

      {hasError && (
        <ErrorText>
          {intl.formatMessage({
            id: 'payment.location.startOnboarding.error',
          })}
        </ErrorText>
      )}

      <ConsentCheckbox
        acceptedDPA={acceptedDPA}
        setAcceptedDPA={setAcceptedDPA}
        acceptedTermsAndConditions={acceptedTermsAndConditions}
        setAcceptedTermsAndConditions={setAcceptedTermsAndConditions}
      />

      <ButtonWrapper>
        <PoweredByWrapper>
          <SubmitButtonPayment
            titleId="payment.activate.btn"
            dataCy="activatePayment"
            loading={isLoading}
            disabled={!acceptedDPA || !acceptedTermsAndConditions}
          />
          <PoweredBy>
            {intl.formatMessage({
              id: 'payment.poweredBy',
            })}
          </PoweredBy>
        </PoweredByWrapper>
      </ButtonWrapper>
    </AppForm>
  );
};
