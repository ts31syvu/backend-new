import styled from 'styled-components';

export const PoweredByWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
  width: auto;
  align-items: center;
  max-width: fit-content;
`;

export const PoweredBy = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-MediumItalic, sans-serif;
  font-size: 12px;
  font-style: italic;
  font-weight: 500;
  text-align: right;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
`;

export const styledInput = {
  borderRadius: 0,
  border: '0.5px solid rgb(0, 0, 0)',
  height: '40px',
};

export const FieldRow = styled.div`
  display: flex;
  flex-direction: row;
  gap: 40px;
  width: 100%;
  & > * {
    width: 100%;
  }
`;

export const StyledTitle = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const ErrorText = styled.p`
  color: red;
`;
