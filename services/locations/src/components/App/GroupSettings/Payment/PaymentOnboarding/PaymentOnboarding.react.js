import React from 'react';
import { useIntl } from 'react-intl';
import { ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import { PaymentInfo } from './PaymentInfo';
import { PaymentCreateLocationGroup } from './PaymentCreateLocationGroup';
import { PaymentStartKybOnboarding } from './PaymentStartKybOnboarding';
import {
  Wrapper,
  Container,
  StyledDivider,
  Title,
  Description,
} from './PaymentOnboarding.styled';

export const PaymentOnboarding = ({
  operator,
  locationGroup,
  onboardingStatus,
}) => {
  const intl = useIntl();

  const needsGroupCreation =
    onboardingStatus.status === ONBOARDING_STATUS.GROUP_NOT_FOUND;

  return (
    <Wrapper>
      <PaymentInfo />
      <StyledDivider />
      <Container>
        <Title>{intl.formatMessage({ id: 'payment.activate.title' })}</Title>
        <Description>
          {intl.formatMessage({
            id: needsGroupCreation
              ? 'payment.activate.description'
              : 'payment.activate.descriptionActivated',
          })}
        </Description>
        {needsGroupCreation ? (
          <PaymentCreateLocationGroup
            operator={operator}
            locationGroup={locationGroup}
          />
        ) : (
          <PaymentStartKybOnboarding
            locationGroupId={locationGroup.groupId}
            kybUrl={onboardingStatus?.kybUrl || null}
          />
        )}
      </Container>
    </Wrapper>
  );
};
