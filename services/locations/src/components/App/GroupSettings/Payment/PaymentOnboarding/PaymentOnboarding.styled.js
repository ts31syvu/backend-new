import styled from 'styled-components';
import { Divider } from 'antd';

export const Wrapper = styled.div`
  background: rgb(255, 255, 255);
  border-radius: 8px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.15);
  margin-top: 30px;
`;

export const Container = styled.div`
  padding: 24px 32px;
`;

export const StyledDivider = styled(Divider)`
  border: 0.5px solid rgb(0, 0, 0);
  margin: 0;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 16px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 16px;
`;
