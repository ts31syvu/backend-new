import { Link } from 'react-router-dom';
import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { startRapydKybProcess } from 'network/payment';
import { notification } from 'antd';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS } from 'components/hooks/queries';
import {
  ButtonWrapper,
  PoweredBy,
  PoweredByWrapper,
  StyledPrimaryButton,
} from './PaymentStartKybOnboarding.styled';

export const PaymentStartKybOnboarding = ({ locationGroupId, kybUrl }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [isLoading, setLoading] = useState(false);

  const errorNotification = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'payment.location.startOnboarding.error',
      }),
    });

  const startKybProcess = () => {
    setLoading(true);
    startRapydKybProcess(locationGroupId)
      .then(async response => {
        if (response.status === 201) {
          const { kybUrl: responseKybUrl } = await response.json();
          if (responseKybUrl) {
            window.open(responseKybUrl);
          }
          await queryClient.invalidateQueries(
            QUERY_KEYS.PAYMENT_ONBOARDING_STATUS
          );
        } else {
          errorNotification();
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
        errorNotification();
      });
  };

  return (
    <ButtonWrapper>
      <PoweredByWrapper>
        {kybUrl ? (
          <Link to={{ pathname: kybUrl }} target="_blank" rel="noopener">
            <StyledPrimaryButton data-cy="continueActivatePayment">
              {intl.formatMessage({
                id: 'payment.activate.continueBtn',
              })}
            </StyledPrimaryButton>
          </Link>
        ) : (
          <StyledPrimaryButton loading={isLoading} onClick={startKybProcess}>
            {intl.formatMessage({
              id: 'payment.activate.continueBtn',
            })}
          </StyledPrimaryButton>
        )}

        <PoweredBy>
          {intl.formatMessage({
            id: 'payment.poweredBy',
          })}
        </PoweredBy>
      </PoweredByWrapper>
    </ButtonWrapper>
  );
};
