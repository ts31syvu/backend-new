import styled from 'styled-components';
import { PrimaryButton } from 'components/general';

export const StyledPrimaryButton = styled(PrimaryButton)`
  background: rgb(255, 220, 95);
  border: 2px solid rgb(255, 220, 95);

  &:hover,
  &:active,
  &:focus,
  &:disabled,
  &:focus-visible {
    background: rgb(255, 220, 95) !important;
    border: 2px solid rgb(255, 220, 95) !important;
  }
`;

export const PoweredByWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
  width: auto;
  align-items: center;
  max-width: fit-content;
`;

export const PoweredBy = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-MediumItalic, sans-serif;
  font-size: 12px;
  font-style: italic;
  font-weight: 500;
  text-align: right;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 20px;
`;
