import { useIntl } from 'react-intl';
import React from 'react';
import { LocationCard } from 'components/App/Dashboard/Location/LocationCard';
import { ArrowRightOutlined } from '@ant-design/icons';
import { ContactFormModal } from 'components/App/modals/ContactFormModal';
import { PrimaryButton } from 'components/general';
import { useModal } from 'components/hooks/useModal';
import { Content, ButtonWrapper } from './PaymentOnboardingRejected.styled';

export const PaymentOnboardingRejected = ({ operator }) => {
  const intl = useIntl();
  const [openModal] = useModal();

  const openContactForm = () => {
    openModal({
      content: <ContactFormModal operator={operator} />,
    });
  };

  return (
    <LocationCard
      title={intl.formatMessage({
        id: 'payment.onboarding.rejected.title',
      })}
    >
      <Content>
        {intl.formatMessage({ id: 'payment.onboarding.rejected.description' })}
      </Content>
      <ButtonWrapper>
        <PrimaryButton
          onClick={openContactForm}
          data-cy="helpCenterModalTrigger"
        >
          {intl.formatMessage({ id: 'payment.onboarding.rejected.contact' })}
          <ArrowRightOutlined />
        </PrimaryButton>
      </ButtonWrapper>
    </LocationCard>
  );
};
