import styled from 'styled-components';

export const Content = styled.p`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  padding: 16px 16px;
`;

export const ButtonWrapper = styled.span`
  padding-left: 16px;
`;
