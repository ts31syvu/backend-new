import React from 'react';
import { useIntl } from 'react-intl';
import { LocationCard } from 'components/App/Dashboard/Location/LocationCard';
import { PrimaryButton } from 'components/general';
import { useQueryClient } from 'react-query';
import { triggerPayout } from 'network/payment';
import { queryDoNotRetryOnCode } from 'network/utlis';
import { notification, Popconfirm, Spin } from 'antd';
import { QUERY_KEYS, useGetBalance } from 'components/hooks/queries';
import {
  CollapsableWrapper,
  DateTitle,
  MoneyAmount,
  Row,
  StyledQuestionCircleOutlined,
} from './PayoutBalance.styled';

export const PayoutBalance = ({ locationGroupId }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const { isLoading, error, data: balanceResponse } = useGetBalance(
    locationGroupId,
    {
      enabled: !!locationGroupId,
      retry: queryDoNotRetryOnCode([401, 403, 404]),
    }
  );

  if (error || !balanceResponse) {
    return null;
  }

  const onClickedPayout = () => {
    const onFail = () =>
      notification.error({
        message: intl.formatMessage({
          id: 'payment.location.payoutBalance.payoutError',
        }),
      });
    const onSuccess = () =>
      notification.success({
        message: intl.formatMessage({
          id: 'payment.location.payoutBalance.payoutSuccess',
        }),
      });
    triggerPayout(locationGroupId)
      .then(success => {
        if (success) {
          onSuccess();
          queryClient.invalidateQueries(QUERY_KEYS.BALANCE);
          queryClient.invalidateQueries(QUERY_KEYS.PAYOUTS);
        } else {
          onFail();
        }
      })
      .catch(() => onFail());
  };

  const isDisabled = () => parseFloat(balanceResponse.balance) === 0;

  return (
    <LocationCard
      title={intl.formatMessage({ id: 'payment.location.payoutBalance.title' })}
    >
      <CollapsableWrapper>
        <Row>
          <DateTitle>
            {intl.formatMessage({
              id: 'payment.location.payoutBalance.description',
            })}
          </DateTitle>
        </Row>
        <Row>
          {isLoading ? (
            <Spin />
          ) : (
            <MoneyAmount>{`${balanceResponse.balance} €`}</MoneyAmount>
          )}
          <Popconfirm
            disabled={isDisabled()}
            placement="topLeft"
            onConfirm={() => onClickedPayout()}
            title={intl.formatMessage({
              id: 'payment.location.payoutBalance.confirm.title',
            })}
            okText={intl.formatMessage({
              id: 'payment.location.payoutBalance.confirm.okText',
            })}
            cancelText={intl.formatMessage({
              id: 'payment.location.payoutBalance.confirm.cancelText',
            })}
            icon={<StyledQuestionCircleOutlined />}
          >
            <PrimaryButton disabled={isDisabled()}>
              {intl.formatMessage({
                id: 'payment.location.payoutBalance.ctaButton',
              })}
            </PrimaryButton>
          </Popconfirm>
        </Row>
      </CollapsableWrapper>
    </LocationCard>
  );
};
