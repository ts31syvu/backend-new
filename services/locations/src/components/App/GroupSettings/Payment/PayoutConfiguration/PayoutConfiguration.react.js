import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { notification, Popconfirm } from 'antd';
import { LocationCard } from 'components/App/Dashboard/Location/LocationCard';
import {
  changePayoutOnboarding,
  getPayoutDetails,
  patchLocation,
} from 'network/payment';
import { useQuery } from 'react-query';

import { DangerButton } from 'components/general';
import {
  CollapsableWrapper,
  Content,
  ContentWrapper,
  DetailsContent,
  ExternalLinkButton,
  ExternalLinkButtonSubline,
  Title,
} from './PayoutConfiguration.styled';

export const PayoutConfiguration = ({ locationGroup }) => {
  const intl = useIntl();
  const [isInProgress, setInProgress] = useState(false);

  const { data: payoutDetails, error, isLoading } = useQuery(
    'payoutDetails',
    () => getPayoutDetails(locationGroup.groupId)
  );

  if (!locationGroup || error || isLoading) {
    return null;
  }

  const onDeactivateHandler = () => {
    const promises = [];
    for (const location of locationGroup.locations) {
      promises.push(patchLocation(location.uuid, false));
    }

    setInProgress(true);
    Promise.all(promises)
      .then(() => setInProgress(false))
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'payment.location.payoutConfiguration.deactivationError',
          }),
        });
        setInProgress(false);
      });
  };

  const onChangeDetailsHandler = () => {
    setInProgress(true);
    changePayoutOnboarding(locationGroup.groupId)
      .then(response => response.json())
      .then(data => {
        window.open(data.payoutUrl);
        setInProgress(false);
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'payment.location.payoutConfiguration.changeBankDetails.error',
          }),
        });
        setInProgress(false);
      });
  };
  return (
    <LocationCard
      isCollapse
      open
      title={intl.formatMessage({
        id: 'payment.location.payoutConfiguration.title',
      })}
    >
      <CollapsableWrapper>
        <ContentWrapper>
          <Content>
            {intl.formatMessage({
              id: 'payment.location.payoutConfiguration.text',
            })}
          </Content>
        </ContentWrapper>
        <ContentWrapper>
          <Title>
            {intl.formatMessage({
              id: 'payment.location.payoutConfiguration.label.iban',
            })}
          </Title>
          <DetailsContent>
            {payoutDetails.payoutDetails.iban || ''}
          </DetailsContent>
        </ContentWrapper>
        <ContentWrapper isButtonWrapper>
          <Popconfirm
            placement="bottomRight"
            title={intl.formatMessage(
              {
                id:
                  'payment.location.payoutConfiguration.button.popConfirm.deactivateBankAccount.text',
              },
              {
                br: <br />,
              }
            )}
            okText={intl.formatMessage({
              id:
                'payment.location.payoutConfiguration.button.popConfirm.deactivateBankAccount',
            })}
            cancelText={intl.formatMessage({
              id:
                'payment.location.payoutConfiguration.button.popConfirm.cancel.deactivateBankAccount',
            })}
            onConfirm={onDeactivateHandler}
          >
            <DangerButton loading={isLoading}>
              {intl.formatMessage({
                id:
                  'payment.location.payoutConfiguration.button.deactivateBankAccount',
              })}
            </DangerButton>
          </Popconfirm>
          <ContentWrapper>
            <Popconfirm
              placement="bottomRight"
              title={intl.formatMessage(
                {
                  id:
                    'payment.location.payoutConfiguration.button.popConfirm.changeBankDetails.text',
                },
                {
                  br: <br />,
                }
              )}
              okText={intl.formatMessage({
                id:
                  'payment.location.payoutConfiguration.button.popConfirm.changeBankDetails.ok',
              })}
              cancelText={intl.formatMessage({
                id:
                  'payment.location.payoutConfiguration.button.popConfirm.changeBankDetails.cancel',
              })}
              onConfirm={onChangeDetailsHandler}
            >
              <ExternalLinkButton loading={isInProgress}>
                {intl.formatMessage({
                  id:
                    'payment.location.payoutConfiguration.button.changeBankDetails',
                })}
              </ExternalLinkButton>
              <ExternalLinkButtonSubline>
                {intl.formatMessage({
                  id: 'payment.location.accountDetails.button.subline',
                })}
              </ExternalLinkButtonSubline>
            </Popconfirm>
          </ContentWrapper>
        </ContentWrapper>
      </CollapsableWrapper>
    </LocationCard>
  );
};
