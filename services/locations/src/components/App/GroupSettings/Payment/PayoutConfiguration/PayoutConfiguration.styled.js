import styled from 'styled-components';
import { WarningButton } from 'components/general/Buttons.styled';

export const CollapsableWrapper = styled.div`
  padding: 0 32px;
`;

export const ContentWrapper = styled.div`
  display: flex;
  flex-direction: ${({ isButtonWrapper }) =>
    isButtonWrapper ? 'row' : 'column'};
  ${({ isButtonWrapper }) =>
    isButtonWrapper ? 'justify-content: space-between' : ''};
`;

export const Title = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 20px;
  margin-bottom: 4px;
`;

export const Content = styled.p`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  letter-spacing: 0;
  line-height: 20px;
  margin-bottom: 40px;
`;

export const DetailsContent = styled(Content)`
  font-size: 16px;
`;

export const ExternalLinkButton = styled(WarningButton)`
  background-color: #ffdc5f;
  border-color: #ffdc5f;
`;

export const ExternalLinkButtonSubline = styled.p`
  color: rgb(0, 0, 0);
  font-size: 12px;
  font-weight: 500;
  font-style: italic;
  text-align: center;
  letter-spacing: 0;
  line-height: 20px;
  margin-top: 5px;
`;
