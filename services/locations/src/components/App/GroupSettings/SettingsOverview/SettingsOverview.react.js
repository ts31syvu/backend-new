import React, { useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form, Input, notification, Spin } from 'antd';
import { PrimaryButton } from 'components/general';

import { updateGroup } from 'network/api';

// hooks
import {
  useNameValidator,
  usePhoneValidator,
} from 'components/hooks/useValidators';

import { getFormattedPhoneNumber } from 'utils/parsePhoneNumber';

import { Address } from 'components/general/Address';
import { useQueryClient } from 'react-query';
import { EditAddressModal } from 'components/App/modals/EditAddressModal';
import { useModal } from 'components/hooks/useModal';
import { QUERY_KEYS, useGetLocationById } from 'components/hooks/queries';
import {
  Overview,
  Heading,
  ButtonWrapper,
  StyledDivider,
  OverviewHeading,
  OverviewValue,
} from './SettingsOverview.styled';

export const SettingsOverview = ({ group }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [openModal] = useModal();
  const formReference = useRef(null);
  const [isEditMode, setIsEditMode] = useState(false);
  const groupNameValidator = useNameValidator('groupName');
  const phoneValidator = usePhoneValidator('phone');

  const baseLocationInfo = group.locations.find(location => !location.name);

  const { data: baseLocation, isLoading, error } = useGetLocationById(
    baseLocationInfo.uuid
  );

  if (isLoading) return <Spin />;
  if (error) return null;

  const handleServerError = () => {
    notification.error({
      message: intl.formatMessage({
        id: 'notification.updateGroup.error',
      }),
    });
  };

  const handleSuccessNotification = () => {
    notification.success({
      message: intl.formatMessage({
        id: 'notification.updateGroup.success',
      }),
    });
  };

  const onFinish = values => {
    const { phone, name } = values;
    const formattedPhoneNumber = getFormattedPhoneNumber(phone);
    const formattedGroupName = name.trim();

    if (
      formattedGroupName === group.name &&
      formattedPhoneNumber === baseLocation.phone
    ) {
      return;
    }

    updateGroup({
      groupId: group.groupId,
      data: { name: formattedGroupName, phone: formattedPhoneNumber },
    })
      .then(response => {
        if (response.status !== 204) {
          handleServerError();
        }
        queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
        formReference.current?.setFieldsValue({
          phone: formattedPhoneNumber,
          name: formattedGroupName,
        });
        handleSuccessNotification();
        setIsEditMode(false);
      })
      .catch(() => {
        handleServerError();
      });
  };

  const submitForm = () => {
    setIsEditMode(!isEditMode);
    if (isEditMode) {
      formReference.current.submit();
    }
  };

  const openEditAddressModal = () => {
    openModal({
      content: <EditAddressModal locationId={baseLocation.uuid} isGroup />,
      closable: true,
      emphasis: 'noHeader',
    });
  };

  return (
    <Overview>
      <Heading>{intl.formatMessage({ id: 'profile.overview' })}</Heading>
      {!isEditMode ? (
        <div>
          <OverviewHeading>
            {intl.formatMessage({
              id: 'settings.group.name',
            })}
          </OverviewHeading>
          <OverviewValue>{group.name}</OverviewValue>
          <OverviewHeading>
            {intl.formatMessage({
              id: 'settings.location.phone',
            })}
          </OverviewHeading>
          <OverviewValue>{baseLocation.phone}</OverviewValue>
        </div>
      ) : (
        <Form
          onFinish={onFinish}
          style={{ maxWidth: 350 }}
          ref={formReference}
          initialValues={{
            name: group.name,
            phone: baseLocation.phone,
          }}
        >
          <Form.Item
            name="name"
            colon={false}
            label={intl.formatMessage({
              id: 'settings.group.name',
            })}
            rules={groupNameValidator}
          >
            <Input />
          </Form.Item>
          <Form.Item
            colon={false}
            label={intl.formatMessage({
              id: 'settings.location.phone',
            })}
            name="phone"
            rules={phoneValidator}
          >
            <Input />
          </Form.Item>
        </Form>
      )}
      <ButtonWrapper>
        <PrimaryButton data-cy="editGroupName" onClick={submitForm}>
          {intl.formatMessage({
            id: isEditMode
              ? 'profile.overview.submit'
              : 'profile.overview.edit',
          })}
        </PrimaryButton>
      </ButtonWrapper>
      <StyledDivider />
      <Address
        isGroup
        isChangeBtnOutside
        location={baseLocation}
        streetName={baseLocation.streetName}
        streetNr={baseLocation.streetNr}
        city={baseLocation.city}
        zipCode={baseLocation.zipCode}
      />
      <ButtonWrapper>
        <PrimaryButton
          data-cy="editGroupAddress"
          onClick={openEditAddressModal}
        >
          {intl.formatMessage({
            id: 'profile.overview.edit',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </Overview>
  );
};
