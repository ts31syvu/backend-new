import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Dropdown } from 'antd';

import { MenuDefaultIcon, MenuActiveIcon } from 'assets/icons';
import { getIncompleteTransfers } from 'utils/shareData';

import { useGetTransfers } from 'components/hooks/queries';
import { MenuEntries } from './MenuEntries';

import {
  StyledIcon,
  StyledBadge,
  Wrapper,
  IconText,
} from './DetailsDropdown.styled';

export const DetailsDropdown = () => {
  const intl = useIntl();
  const [dropdownVisibility, setDropdownVisibility] = useState(false);

  const { isLoading, error, data: transfers } = useGetTransfers();

  if (error || isLoading) return null;

  return (
    <>
      <Dropdown
        overlay={
          <MenuEntries
            onAction={() => setDropdownVisibility(false)}
            transfers={transfers}
          />
        }
        placement="bottomRight"
        onVisibleChange={setDropdownVisibility}
      >
        <Wrapper>
          <StyledBadge count={getIncompleteTransfers(transfers).length} />
          <StyledIcon
            component={dropdownVisibility ? MenuActiveIcon : MenuDefaultIcon}
            data-cy="dropdownMenuTrigger"
          />
          <IconText>{intl.formatMessage({ id: 'menu.title' })}</IconText>
        </Wrapper>
      </Dropdown>
    </>
  );
};
