import React from 'react';
import { Menu, notification } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';
import { push } from 'connected-react-router';
import { useHistory } from 'react-router';
import { useDispatch } from 'react-redux';

import {
  LOGIN_ROUTE,
  PROFILE_ROUTE,
  ARCHIVE_ROUTE,
  BASE_DATA_TRANSFER_ROUTE,
} from 'constants/routes';

import { getIncompleteTransfers } from 'utils/shareData';
import { usePrivateKey } from 'utils/privateKey';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import { logout } from 'network/api';
import { clearJwt } from 'utils/jwtStorage';
import { StyledMenuItem, StyledBadge } from './MenuEntries.styled';

export const MenuEntries = ({ onAction, transfers, ...properties }) => {
  const intl = useIntl();
  const history = useHistory();
  const queryClient = useQueryClient();
  const dispatch = useDispatch();
  const [, clearPrivateKey] = usePrivateKey(null);

  const openProfile = () => {
    history.push(PROFILE_ROUTE);
    onAction();
  };

  const openArchive = () => {
    history.push(ARCHIVE_ROUTE);
    onAction();
  };

  const openDataRequests = () => {
    history.push(BASE_DATA_TRANSFER_ROUTE);
    onAction();
  };

  const handleLogout = () => {
    logout()
      .then(response => {
        if (response.status >= 400) {
          notification.error({
            message: intl.formatMessage({
              id: 'notification.logout.error',
            }),
          });
          return;
        }

        dispatch(push(LOGIN_ROUTE));
        queryClient.clear();
        clearPrivateKey(null);
        clearJwt();
        clearHasSeenPrivateKeyModal();
        notification.success({
          message: intl.formatMessage({
            id: 'notification.logout.success',
          }),
        });
      })
      .catch(() =>
        notification.error({
          message: intl.formatMessage({
            id: 'notification.logout.error',
          }),
        })
      );
  };

  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <Menu {...properties}>
      <StyledMenuItem key="0" data-cy="profile" onClick={openProfile}>
        {intl.formatMessage({
          id: 'header.profile',
        })}
      </StyledMenuItem>
      <StyledMenuItem key="1" onClick={openArchive}>
        {intl.formatMessage({
          id: 'header.archive',
        })}
      </StyledMenuItem>
      <StyledMenuItem key="2" data-cy="dataRequests" onClick={openDataRequests}>
        <StyledBadge count={getIncompleteTransfers(transfers).length} />
        {intl.formatMessage({ id: 'shareData.title' })}
      </StyledMenuItem>
      <Menu.Divider />
      <StyledMenuItem key="3" data-cy="logout" onClick={handleLogout}>
        {intl.formatMessage({
          id: 'header.logout',
        })}
      </StyledMenuItem>
    </Menu>
  );
};
