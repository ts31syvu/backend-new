import styled from 'styled-components';
import { Menu, Badge } from 'antd';

export const StyledMenuItem = styled(Menu.Item)`
  padding: 12px 32px;
  position: relative;
`;

export const StyledBadge = styled(Badge)`
  right: 4px;
  position: absolute;
`;
