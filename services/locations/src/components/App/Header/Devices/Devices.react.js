import React from 'react';
import { gt } from 'semver';
import { useIntl } from 'react-intl';
import { useHistory, useLocation } from 'react-router';
import { Badge } from 'antd';
import Icon from '@ant-design/icons';

import { RELEASE_OPERATOR_APP_NOTIFICATION } from 'constants/releaseVersions';
import { DEVICES_ROUTE } from 'constants/routes';
import { useGetMe } from 'components/hooks/queries';

import { DeviceActiveIcon, DeviceDefaultIcon } from 'assets/icons';

import {
  AnimatedDevicesComp,
  DevicesComp,
  StyledDeviceIcon,
  IconText,
} from './Devices.styled';

const DeviceIcon = (lastVersionSeen, isActive) => {
  if (gt(RELEASE_OPERATOR_APP_NOTIFICATION, lastVersionSeen) && !isActive) {
    return (
      <Badge dot>
        <StyledDeviceIcon
          component={isActive ? DeviceActiveIcon : DeviceDefaultIcon}
        />
      </Badge>
    );
  }
  return (
    <Icon
      component={isActive ? DeviceActiveIcon : DeviceDefaultIcon}
      style={{ fontSize: 32 }}
    />
  );
};

export function Devices() {
  const intl = useIntl();
  const history = useHistory();
  const currentRoute = useLocation();

  const isDeviceRoute = currentRoute.pathname === DEVICES_ROUTE;

  const { data: operator, isError, isLoading } = useGetMe();

  const navigate = () => {
    history.push(DEVICES_ROUTE);
  };
  if (isLoading || isError) return null;

  return (
    <>
      {gt(RELEASE_OPERATOR_APP_NOTIFICATION, operator.lastVersionSeen) &&
      !isDeviceRoute ? (
        <AnimatedDevicesComp onClick={navigate}>
          {DeviceIcon(operator.lastVersionSeen, isDeviceRoute)}
          <IconText>{intl.formatMessage({ id: 'device.title' })}</IconText>
        </AnimatedDevicesComp>
      ) : (
        <DevicesComp onClick={navigate}>
          {DeviceIcon(operator.lastVersionSeen, isDeviceRoute)}
          <IconText>{intl.formatMessage({ id: 'device.title' })}</IconText>
        </DevicesComp>
      )}
    </>
  );
}
