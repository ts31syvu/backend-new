import styled, { keyframes } from 'styled-components';
import Icon from '@ant-design/icons';

const pulse = keyframes`
0% {
    transform: scale(0.5, 0.5);
    opacity: 0.3;
  }
  50% {
    opacity: 1.0;
  }
  100% {
    transform: scale(1.1, 1.1);
    opacity: 0.3;
  }
  `;

export const AnimatedDevicesComp = styled.div`
  margin-top: 10px;
  cursor: pointer;
  display: list-item;
  animation: ${pulse} 2s ease-in-out;
  animation-iteration-count: infinite;
  flex-direction: column;
  justify-content: center;
`;

export const DevicesComp = styled.div`
  margin-top: 10px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const StyledDeviceIcon = styled(Icon)`
  font-size: 32px;
`;

export const IconText = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 12px;
  font-weight: 500;
  margin-top: 10px;
  margin-bottom: 10px;
`;
