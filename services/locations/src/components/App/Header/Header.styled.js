import styled from 'styled-components';
import { Header } from 'antd/es/layout/layout';
import { Link } from 'react-router-dom';

export const StyledLink = styled(Link)`
  line-height: 0;
  display: flex;
  justify-content: flex-start;
  align-items: baseline;
`;

export const HeaderWrapper = styled(Header)`
  background: rgb(0, 0, 0);
  display: flex;
  justify-content: space-between;
  align-content: center;
  align-items: center;
  padding: 24px 32px;
`;

export const Logo = styled.img`
  height: 33px;
  width: 74px;
`;

export const SubTitle = styled.div`
  color: rgb(255, 255, 255);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-left: 8px;
`;

export const MenuWrapper = styled.div`
  display: flex;
  line-height: 0;
`;
