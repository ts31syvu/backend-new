import React from 'react';

import { useIntl } from 'react-intl';
import { useHistory, useLocation } from 'react-router';
import Icon from '@ant-design/icons';

// CONSTANTS
import { HELP_CENTER_ROUTE } from 'constants/routes';

import { HelpCenterActiveIcon, HelpCenterDefaultIcon } from 'assets/icons';

import { HelpCenterComp, IconText } from './HelpCenter.styled';

const HelpCenterIcon = isActive => (
  <Icon
    component={isActive ? HelpCenterActiveIcon : HelpCenterDefaultIcon}
    style={{ fontSize: 32 }}
    data-cy="buttonHelpCenter"
  />
);

export const HelpCenter = () => {
  const intl = useIntl();
  const history = useHistory();
  const currentRoute = useLocation();

  const isHelpCenterRoute = currentRoute.pathname === HELP_CENTER_ROUTE;

  const handleClick = () => history.push(HELP_CENTER_ROUTE);

  return (
    <HelpCenterComp onClick={handleClick}>
      {HelpCenterIcon(isHelpCenterRoute)}
      <IconText>
        {intl.formatMessage({ id: 'helpCenter.header.title' })}
      </IconText>
    </HelpCenterComp>
  );
};
