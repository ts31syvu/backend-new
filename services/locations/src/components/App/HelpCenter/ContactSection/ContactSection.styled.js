import styled from 'styled-components';

export const Wrapper = styled.div`
  margin-top: 24px;
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 24px;
`;
