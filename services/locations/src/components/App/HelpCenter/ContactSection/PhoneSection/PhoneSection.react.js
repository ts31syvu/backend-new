import React from 'react';
import { useIntl } from 'react-intl';

import { StyledTooltip } from 'components/general';
import { HELPCENTER } from '../ContactSection.helper';
import {
  Wrapper,
  SupportCode,
  StyledPhoneOutlined,
} from './PhoneSection.styled';
import { Heading, Text } from '../MailSection/MailSection.styled';

export const PhoneSection = ({ operator }) => {
  const intl = useIntl();
  return (
    <Wrapper>
      <StyledPhoneOutlined rotate={90} />
      <Heading>
        {intl.formatMessage({ id: 'helpCenter.phone.heading' })}
      </Heading>
      <Text>
        {intl.formatMessage({ id: 'helpCenter.phone.text' }, { br: <br /> })}
      </Text>
      <StyledTooltip title={operator.supportCode} small>
        <SupportCode>
          {intl.formatMessage({ id: 'helpCenter.phone.showSupportCode' })}
        </SupportCode>
      </StyledTooltip>
      <Heading>{HELPCENTER.phone}</Heading>
    </Wrapper>
  );
};
