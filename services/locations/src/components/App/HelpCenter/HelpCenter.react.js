import React from 'react';
import { useIntl } from 'react-intl';
import { Layout } from 'antd';

import { LocationFooter } from 'components/App/LocationFooter';

import {
  contentStyles,
  NavigationButton,
  siderStyles,
} from 'components/general';

import { Links } from './Links';
import { ContactSection } from './ContactSection';

import { Wrapper, Header, CardWrapper } from './HelpCenter.styled';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';

const { Content, Sider } = Layout;

export const HelpCenter = ({ operator }) => {
  const intl = useIntl();
  return (
    <Layout>
      <Sider width={300} style={siderStyles}>
        <NavigationButton />
        <LanguageSwitcher />
      </Sider>
      <Layout>
        <Content style={contentStyles}>
          <Wrapper>
            <Header data-cy="helpCenterTitle">
              {intl.formatMessage({ id: 'helpCenter.title' })}
            </Header>
            <CardWrapper data-cy="helpCenterWrapper">
              <Links />
              <ContactSection operator={operator} />
            </CardWrapper>
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
