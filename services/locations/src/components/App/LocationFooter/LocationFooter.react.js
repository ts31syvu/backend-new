import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import { getVersion } from 'network/static';

import { FAQ_LINK, GITLAB_LINK } from 'constants/links';
import { HELP_CENTER_ROUTE } from 'constants/routes';
import { Link, Version, Wrapper } from './LocationFooter.styled';

export const LocationFooter = ({ color = '#000' }) => {
  const intl = useIntl();
  const { isSuccess, data: info } = useQuery('version', getVersion, {
    refetchOnWindowFocus: false,
  });

  return (
    <Wrapper>
      <Version color={color}>
        {isSuccess ? `luca Locations (${info.version})` : ''}
      </Version>
      <Link
        color={color}
        target="_blank"
        href={FAQ_LINK}
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'location.footer.faq' })}
      </Link>
      <Link color={color} href={HELP_CENTER_ROUTE} rel="noopener noreferrer">
        {intl.formatMessage({ id: 'helpCenter.title' })}
      </Link>
      <Link
        color={color}
        target="_blank"
        href={GITLAB_LINK}
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'location.footer.repository' })}
      </Link>
    </Wrapper>
  );
};
