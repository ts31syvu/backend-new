import styled from 'styled-components';

export const Heading = styled.div`
  margin-bottom: 16px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;

export const Wrapper = styled.div`
  padding: 24px 32px;
`;

export const Info = styled.div`
  margin-bottom: 24px;
`;
