import React from 'react';
import { useParams } from 'react-router-dom';

import { LocationFooter } from 'components/App/LocationFooter';

import { useGetLocationById } from 'components/hooks/queries';
import { SettingsOverview } from './SettingsOverview';
import { DeleteLocation } from './DeleteLocation';
import { Wrapper, SettingsContent } from './LocationSettings.styled';

export const LocationSettings = () => {
  const { locationId } = useParams();

  const { isError, isLoading, data: location } = useGetLocationById(locationId);

  if (isLoading || isError) return null;

  return (
    <Wrapper data-cy="areaProfile">
      <SettingsContent>
        <SettingsOverview location={location} isLast={location.name === null} />
        {location.name !== null && <DeleteLocation location={location} />}
      </SettingsContent>
      <LocationFooter />
    </Wrapper>
  );
};
