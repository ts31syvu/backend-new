import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 0 32px;
`;

export const Heading = styled.div`
  margin-bottom: 24px;
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;

export const Overview = styled.div`
  padding: 24px 32px;
  border-bottom: ${({ isLast }) => (isLast ? 0 : 1)}px solid rgb(151, 151, 151);
  margin: 0 -32px;
`;
