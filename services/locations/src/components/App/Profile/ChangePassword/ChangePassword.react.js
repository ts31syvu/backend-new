import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Input, Form, notification } from 'antd';
import { zxcvbn } from 'config.zxcvbn';

import { PrimaryButton } from 'components/general';
import { changePassword } from 'network/api';
import { StrengthMeter } from 'components/general/StrengthMeter';
import { MIN_STRENGTH_SCORE } from 'constants/general';
import { handleResponse } from './ChangePassword.helper';
import {
  ProfileContent,
  Heading,
  ButtonWrapper,
} from './ChangePassword.styled';

export const ChangePassword = ({ operator }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const { firstName, lastName, email } = operator;
  const [strengthScore, setStrengthScore] = useState(0);
  const [warningsList, setWarningsList] = useState([]);
  const [displayStrengthMeter, setDisplayStrengthMeter] = useState(false);

  const onChange = values => {
    const { newPassword } = values;

    if (newPassword) {
      setDisplayStrengthMeter(!!newPassword);

      const passwordStrength = zxcvbn(newPassword, [
        firstName,
        lastName,
        email,
      ]);

      setStrengthScore(passwordStrength.score);
      setWarningsList([
        passwordStrength?.feedback?.warning,
        ...passwordStrength?.feedback?.suggestions,
      ]);
    }
  };

  const onFinish = values => {
    const { currentPassword, newPassword } = values;

    changePassword({ currentPassword, newPassword })
      .then(response => {
        setDisplayStrengthMeter(false);
        setStrengthScore(0);

        handleResponse(response, intl, form);
      })
      .catch(() =>
        notification.error({
          message: intl.formatMessage({
            id: 'registration.server.error.msg',
          }),
          description: intl.formatMessage({
            id: 'registration.server.error.desc',
          }),
        })
      );
  };

  const submitForm = () => {
    form.submit();
  };

  return (
    <ProfileContent>
      <Heading>{intl.formatMessage({ id: 'profile.changePassword' })}</Heading>
      <Form
        form={form}
        onFinish={onFinish}
        onValuesChange={onChange}
        autoComplete="off"
        style={{ maxWidth: 350 }}
      >
        <Form.Item
          colon={false}
          name="currentPassword"
          label={intl.formatMessage({
            id: 'profile.changePassword.oldPassword',
          })}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'error.password',
              }),
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          colon={false}
          name="newPassword"
          hasFeedback
          label={intl.formatMessage({
            id: 'profile.changePassword.newPassword',
          })}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'error.password',
              }),
            },
            () => ({
              validator() {
                if (
                  displayStrengthMeter &&
                  strengthScore < MIN_STRENGTH_SCORE
                ) {
                  return strengthScore !== MIN_STRENGTH_SCORE - 1
                    ? Promise.reject(warningsList)
                    : Promise.reject(
                        intl.formatMessage({
                          id: 'error.password.minStrengthRequired',
                        })
                      );
                }

                return Promise.resolve();
              },
            }),
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (getFieldValue('currentPassword') === value)
                  return Promise.reject(
                    intl.formatMessage({
                      id: 'error.password.same',
                    })
                  );

                return Promise.resolve();
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
        {displayStrengthMeter && (
          <StrengthMeter strengthScore={strengthScore} lightVariation />
        )}
        <Form.Item
          colon={false}
          name="newPasswordConfirm"
          label={intl.formatMessage({
            id: 'profile.changePassword.newPasswordRepeat',
          })}
          hasFeedback
          dependencies={['newPassword']}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'error.passwordConfirm',
              }),
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('newPassword') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  intl.formatMessage({
                    id: 'error.passwordConfirm',
                  })
                );
              },
            }),
          ]}
        >
          <Input.Password />
        </Form.Item>
      </Form>
      <ButtonWrapper>
        <PrimaryButton data-cy="changePassword" onClick={submitForm}>
          {intl.formatMessage({ id: 'profile.overview.submit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
