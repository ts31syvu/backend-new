import styled from 'styled-components';

export const ProfileContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-bottom: 1px solid rgb(151, 151, 151);
`;

export const Heading = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 600;
  font-family: Montserrat-Bold, sans-serif;
  margin-bottom: 16px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;
