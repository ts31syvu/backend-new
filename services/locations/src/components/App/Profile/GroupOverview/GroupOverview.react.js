import React from 'react';
import { useIntl } from 'react-intl';

import { useGetGroups } from 'components/hooks/queries';

import {
  Content,
  Heading,
  Wrapper,
  Count,
  Details,
  Detail,
  GroupName,
} from './GroupOverview.styled';

export const GroupOverview = () => {
  const intl = useIntl();

  const { isLoading, error, data: groups } = useGetGroups('profile');

  if (isLoading || error) return null;
  return (
    <Content>
      <Heading>{intl.formatMessage({ id: 'profile.groups.overview' })}</Heading>
      <Wrapper data-cy="groupsOverview">
        <Count data-cy="groupsCount">{groups.length}</Count>
        <Details>
          {groups.map((group, index) => (
            <Detail key={group.groupId}>
              <div data-cy="groupsNumber" style={{ marginRight: 16 }}>
                {index + 1}
              </div>
              <GroupName data-cy="groupsName">{group.name}</GroupName>
            </Detail>
          ))}
        </Details>
      </Wrapper>
    </Content>
  );
};
