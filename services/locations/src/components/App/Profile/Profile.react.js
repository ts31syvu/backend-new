import React from 'react';
import { Layout, Spin } from 'antd';

import { LocationFooter } from 'components/App/LocationFooter';

import {
  contentStyles,
  siderStyles,
  NavigationButton,
} from 'components/general';

// Components
import { ProfileOverview } from './ProfileOverview';
import { ChangePassword } from './ChangePassword';
import { GroupOverview } from './GroupOverview';
import { Services } from './Services';

import { Wrapper, Header, SpinnerWrapper } from './Profile.styled';
import { AccountDeletion } from './AccountDeletion';
import { LanguageSwitcher } from '../Dashboard/LanguageSwitcher';
import { ResetKey } from './ResetKey';
import { useGetPaymentEnabled } from '../../hooks/queries';

const { Content, Sider } = Layout;

export const Profile = ({ operator, refetch }) => {
  const {
    isLoading: isPaymentLoading,
    error: paymentError,
    data: paymentEnabled,
  } = useGetPaymentEnabled(operator.operatorId);

  if (paymentError) {
    return null;
  }

  const accountIsActive = !operator.deletedAt;
  return (
    <Layout>
      <Sider width={300} style={siderStyles}>
        <NavigationButton />
        <LanguageSwitcher />
      </Sider>
      <Layout>
        <Content style={contentStyles}>
          <Wrapper>
            <Header data-cy="operatorName">{`${operator.firstName} ${operator.lastName}`}</Header>
            {accountIsActive && (
              <>
                <ProfileOverview operator={operator} refetch={refetch} />
                <ChangePassword operator={operator} />
              </>
            )}
            <GroupOverview />
            <ResetKey />
            <Services />
            {!isPaymentLoading ? (
              <AccountDeletion
                operator={operator}
                refetch={refetch}
                isPaymentEnabled={paymentEnabled.paymentEnabled}
              />
            ) : (
              <SpinnerWrapper>
                <Spin />
              </SpinnerWrapper>
            )}
            <LocationFooter />
          </Wrapper>
        </Content>
      </Layout>
    </Layout>
  );
};
