import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { BusinessAddress } from 'components/Authentication/Register/steps/BusinessInfoStep/BusinessAddress';

import { useNameValidator } from 'components/hooks/useValidators';

import { PrimaryButton } from 'components/general';
import { StyledInput } from 'components/Authentication/Authentication.styled';
import { Wrapper } from 'components/App/modals/BusinessAddressModal/BussinessAddressModal.styled';
import { BusinessInformationPreview } from '../ProfilePreview';
import {
  ProfileContent,
  Heading,
  ButtonWrapper,
} from '../ProfileOverview.styled';
import { shouldUpdateOperatorBusinessInfo } from '../ProfileOverview.helper';

export const BusinessInformation = ({ operator, updateOperatorInfo }) => {
  const intl = useIntl();
  const [form] = Form.useForm();
  const [isEditable, setIsEditable] = useState(false);

  const businessNameValidator = useNameValidator('businessEntityName');

  const getInitialValues = () => {
    const {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    } = operator;

    return {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    };
  };

  const onFinish = values => {
    const {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    } = values;

    if (shouldUpdateOperatorBusinessInfo(values, operator)) {
      const infoData = {
        businessEntityCity,
        businessEntityName,
        businessEntityStreetName,
        businessEntityStreetNumber,
        businessEntityZipCode,
      };

      return updateOperatorInfo(infoData, () => {
        setIsEditable(false);
      });
    }

    return setIsEditable(false);
  };

  return (
    <>
      {!isEditable ? (
        <BusinessInformationPreview
          data-cy="profileBusinessPreview"
          setIsEditable={setIsEditable}
          operator={operator}
        />
      ) : (
        <ProfileContent data-cy="profileBusinessEdit">
          <Form
            onFinish={onFinish}
            form={form}
            initialValues={getInitialValues()}
          >
            <Heading>
              {intl.formatMessage({ id: 'profile.businessInfo.title' })}
            </Heading>
            <Wrapper>
              <Form.Item
                data-cy="businessEntityName"
                colon={false}
                name="businessEntityName"
                label={intl.formatMessage({
                  id: 'register.businessEntityName',
                })}
                rules={businessNameValidator}
              >
                <StyledInput />
              </Form.Item>
              <BusinessAddress />
            </Wrapper>
          </Form>
          <ButtonWrapper>
            <PrimaryButton
              data-cy="changeOperatorBusinessInfo"
              onClick={() => form.submit()}
            >
              {intl.formatMessage({ id: 'profile.overview.submit' })}
            </PrimaryButton>
          </ButtonWrapper>
        </ProfileContent>
      )}
    </>
  );
};
