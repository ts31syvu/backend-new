import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Alert, Form, notification } from 'antd';

import { PasswordModal } from 'components/App/modals/PasswordModal';

import {
  useEmailValidator,
  usePersonNameValidator,
  usePhoneValidator,
} from 'components/hooks/useValidators';
import { updateEmail } from 'network/api';

import { PrimaryButton } from 'components/general';
import { StyledInput } from 'components/Authentication/Authentication.styled';
import {
  shouldUpdateOperatorPersonalInfo,
  useQueryMailChange,
} from '../ProfileOverview.helper';

import {
  ProfileContent,
  Heading,
  ButtonWrapper,
  StyledContentBasicInfo,
} from '../ProfileOverview.styled';
import { PersonalInformationPreview } from '../ProfilePreview';

export const PersonalInformation = ({ operator, updateOperatorInfo }) => {
  const intl = useIntl();
  const [form] = Form.useForm();

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isEditable, setIsEditable] = useState(false);
  const [newMail, setNewMail] = useState(null);

  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');
  const emailValidator = useEmailValidator();
  const phoneValidator = usePhoneValidator('phone');

  const {
    data: emailChangeIsActive,
    refetch: refetchEmailPending,
  } = useQueryMailChange();

  const getInitialValues = () => {
    const { firstName, lastName, email, phone } = operator;

    return {
      firstName,
      lastName,
      email,
      phone,
    };
  };

  const handleUpdateEmailError = errorString => {
    setNewMail(null);
    notification.error({
      message: intl.formatMessage({
        id: errorString,
      }),
    });
    form.resetFields();
  };

  const resetModal = () => {
    setNewMail(null);
    setIsModalOpen(false);
    form.resetFields();
  };

  const onChangeEmail = password => {
    updateEmail({ email: newMail, lang: intl.locale, password })
      .then(response => {
        if (response.status === 204) {
          refetchEmailPending();
          resetModal();
        } else if (response.status === 403) {
          resetModal();
          handleUpdateEmailError(
            'notification.profile.updateEmail.error.incorrectPassword'
          );
        } else {
          resetModal();
          handleUpdateEmailError('notification.profile.updateEmail.error');
        }
      })
      .catch(() => {
        resetModal();
        handleUpdateEmailError('notification.profile.updateEmail.error');
      });
  };

  const onFinish = values => {
    const { firstName, lastName, email, phone } = values;
    if (shouldUpdateOperatorPersonalInfo(values, operator)) {
      const infoData = {
        firstName,
        lastName,
        phone,
      };

      return updateOperatorInfo(infoData, () => {
        setIsEditable(false);
      });
    }

    if (email !== operator.email) {
      setIsModalOpen(true);
      setNewMail(email);
      return null;
    }

    return setIsEditable(false);
  };

  return (
    <>
      {!isEditable ? (
        <PersonalInformationPreview
          data-cy="profilePersonalPreview"
          setIsEditable={setIsEditable}
          operator={operator}
        />
      ) : (
        <ProfileContent data-cy="profilePersonalEdit">
          <Heading>
            {intl.formatMessage({ id: 'profile.personalData' })}
          </Heading>
          <Form
            onFinish={onFinish}
            form={form}
            initialValues={getInitialValues()}
          >
            <StyledContentBasicInfo>
              <Form.Item
                colon={false}
                label={intl.formatMessage({
                  id: 'generic.firstName',
                })}
                name="firstName"
                rules={firstNameValidator}
              >
                <StyledInput />
              </Form.Item>
              <Form.Item
                colon={false}
                label={intl.formatMessage({
                  id: 'generic.lastName',
                })}
                name="lastName"
                rules={lastNameValidator}
              >
                <StyledInput />
              </Form.Item>
              <Form.Item
                colon={false}
                label={intl.formatMessage({
                  id: 'settings.location.phone',
                })}
                name="phone"
                rules={phoneValidator}
              >
                <StyledInput />
              </Form.Item>
              <Form.Item
                colon={false}
                label={intl.formatMessage({
                  id: 'registration.form.email',
                })}
                name="email"
                rules={emailValidator}
              >
                <StyledInput />
              </Form.Item>
              {emailChangeIsActive && (
                <Alert
                  data-cy="activeEmailChange"
                  type="info"
                  message={intl.formatMessage({
                    id: 'profile.changeEmailProgress',
                  })}
                />
              )}
            </StyledContentBasicInfo>
          </Form>
          <ButtonWrapper>
            <PrimaryButton
              data-cy="changeOperatorName"
              onClick={() => form.submit()}
            >
              {intl.formatMessage({ id: 'profile.overview.submit' })}
            </PrimaryButton>
          </ButtonWrapper>
        </ProfileContent>
      )}
      {isModalOpen && (
        <PasswordModal callback={onChangeEmail} onClose={resetModal} />
      )}
    </>
  );
};
