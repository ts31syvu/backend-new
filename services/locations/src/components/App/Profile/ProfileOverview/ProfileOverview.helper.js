import { useQuery } from 'react-query';

import { isEmailUpdatePending } from 'network/api';

export const useQueryMailChange = () => {
  const { data, refetch } = useQuery(
    'isMailChangeInProgress',
    () =>
      isEmailUpdatePending()
        .then(() => true)
        .catch(() => false),
    { cacheTime: 0 }
  );

  return { data, refetch };
};

export const shouldUpdateOperatorPersonalInfo = (values, operator) => {
  return (
    values.firstName !== operator.firstName ||
    values.lastName !== operator.lastName ||
    values.phone !== operator.phone
  );
};

export const shouldUpdateOperatorBusinessInfo = (values, operator) => {
  return (
    values.businessEntityName !== operator.businessEntityName ||
    values.businessEntityCity !== operator.businessEntityCity ||
    values.businessEntityStreetName !== operator.businessEntityStreetName ||
    values.businessEntityStreetNumber !== operator.businessEntityStreetNumber ||
    values.businessEntityZipCode !== operator.businessEntityZipCode
  );
};
