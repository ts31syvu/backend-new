import React from 'react';
import { useIntl } from 'react-intl';
import { notification } from 'antd';

import { updateOperator } from 'network/api';
import { BusinessInformation } from './BusinessInformation/BusinessInformation.react';
import { PersonalInformation } from './PersonalInformation/PersonalInformation.react';

export const ProfileOverview = ({ operator, refetch: refetchOperator }) => {
  const intl = useIntl();

  const updateOperatorInfo = (infoData, successCallback) => {
    updateOperator(infoData)
      .then(response => {
        if (response.status !== 204) {
          notification.error({
            message: intl.formatMessage({
              id: 'notification.profile.updateUser.success',
            }),
          });
          return;
        }

        refetchOperator();
        successCallback();

        notification.success({
          message: intl.formatMessage({
            id: 'notification.profile.updateUser.success',
          }),
        });
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'notification.profile.updateUser.error',
          }),
        });
      });
  };

  return (
    <>
      <PersonalInformation
        operator={operator}
        updateOperatorInfo={updateOperatorInfo}
      />
      <BusinessInformation
        operator={operator}
        updateOperatorInfo={updateOperatorInfo}
      />
    </>
  );
};
