import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import {
  ProfileContent,
  Heading,
  ButtonWrapper,
  ProfileTitle,
  ProfileDescription,
} from './ProfilePreview.styled';

export const BusinessInformationPreview = ({
  setIsEditable,
  operator: {
    businessEntityName,
    businessEntityCity,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
  },
}) => {
  const intl = useIntl();

  return (
    <ProfileContent data-cy="businessProfilePreview">
      <Heading>
        {intl.formatMessage({ id: 'profile.businessInfo.title' })}
      </Heading>
      {businessEntityName && (
        <>
          <ProfileTitle>
            {intl.formatMessage({ id: 'profile.businessInfo.name' })}
          </ProfileTitle>
          <ProfileDescription marginBottom="16px">
            {businessEntityName}
          </ProfileDescription>
        </>
      )}
      {businessEntityStreetName &&
        businessEntityStreetNumber &&
        businessEntityZipCode &&
        businessEntityCity && (
          <>
            <ProfileTitle>
              {intl.formatMessage({ id: 'profile.businessInfo.address' })}
            </ProfileTitle>
            <ProfileDescription>{`${businessEntityStreetName} ${businessEntityStreetNumber}`}</ProfileDescription>
            <ProfileDescription marginBottom="16px">{`${businessEntityZipCode} ${businessEntityCity}`}</ProfileDescription>
          </>
        )}
      <ButtonWrapper>
        <PrimaryButton
          data-cy="editBusinessProfileInfo"
          onClick={() => {
            setIsEditable(true);
          }}
        >
          {intl.formatMessage({ id: 'location.edit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
