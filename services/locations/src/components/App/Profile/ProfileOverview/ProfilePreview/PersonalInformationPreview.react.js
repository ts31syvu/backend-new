import React from 'react';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import {
  ProfileContent,
  Heading,
  ButtonWrapper,
  ProfileTitle,
  ProfileDescription,
} from './ProfilePreview.styled';

export const PersonalInformationPreview = ({
  setIsEditable,
  operator: { firstName, lastName, email, phone },
}) => {
  const intl = useIntl();

  return (
    <ProfileContent data-cy="personalProfilePreview">
      <Heading>{intl.formatMessage({ id: 'profile.personalData' })}</Heading>
      {firstName && lastName && (
        <>
          <ProfileTitle>
            {intl.formatMessage({ id: 'contactForm.modal.name' })}
          </ProfileTitle>
          <ProfileDescription marginBottom="16px">{`${firstName} ${lastName}`}</ProfileDescription>
        </>
      )}
      <>
        <ProfileTitle>
          {intl.formatMessage({ id: 'settings.location.phone' })}
        </ProfileTitle>
        {phone ? (
          <ProfileDescription marginBottom="16px">{phone}</ProfileDescription>
        ) : (
          <ProfileDescription marginBottom="16px">
            {intl.formatMessage({ id: 'profile.personalData.noPhone' })}
          </ProfileDescription>
        )}
      </>

      {email && (
        <>
          <ProfileTitle>
            {intl.formatMessage({ id: 'profile.email' })}
          </ProfileTitle>
          <ProfileDescription marginBottom="16px">{email}</ProfileDescription>
        </>
      )}
      <ButtonWrapper>
        <PrimaryButton
          data-cy="editPersonalProfileInfo"
          onClick={() => {
            setIsEditable(true);
          }}
        >
          {intl.formatMessage({ id: 'location.edit' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ProfileContent>
  );
};
