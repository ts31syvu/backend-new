import styled from 'styled-components';

export const ProfileContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-bottom: 1px solid rgb(151, 151, 151);
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 600;
  font-family: Montserrat-Bold, sans-serif;
  margin-bottom: 16px;
`;

export const ProfileTitle = styled.div`
  color: rgba(0, 0, 0);
  font-size: 14px;
  font-weight: 500;
  font-family: Montserrat-Medium, sans-serif;
`;

export const ProfileDescription = styled.div`
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  font-family: Montserrat-Medium, sans-serif;
  margin-bottom: ${({ marginBottom }) => marginBottom || ''};
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-bottom: 32px;
`;

export const contentStyles = {
  backgroundColor: '#f3f5f7',
};

export const sliderStyles = {
  ...contentStyles,
  borderRight: '1px solid rgb(151, 151, 151)',
};

export const Overview = styled.div`
  border-bottom: 1px solid rgb(151, 151, 151);
`;
