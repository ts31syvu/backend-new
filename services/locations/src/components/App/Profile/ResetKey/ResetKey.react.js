import React from 'react';
import { PrimaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { Spin } from 'antd';
import { useModal } from 'components/hooks/useModal';
import { useGetMe, useGetPrivateKeySecret } from 'components/hooks/queries';
import { ResetKey as ResetKeyLoader } from 'components/PrivateKeyLoader/ResetKey';
import {
  ButtonWrapper,
  ResetKeyContent,
  Heading,
  PrivateKeyInfo,
} from './ResetKey.styled';

export const ResetKey = () => {
  const intl = useIntl();
  const {
    data: privateKeySecret,
    isLoading: isPrivateKeySecretLoading,
  } = useGetPrivateKeySecret();

  const { data: operator, isLoading: isOperatorLoading } = useGetMe();

  const [openModal] = useModal();

  const openResetKeyModal = () => {
    openModal({
      content: (
        <ResetKeyLoader
          operator={operator}
          publicKey={operator.publicKey}
          privateKeySecret={privateKeySecret}
        />
      ),
      closable: true,
    });
  };

  if (isPrivateKeySecretLoading || isOperatorLoading) return <Spin />;

  return (
    <ResetKeyContent>
      <Heading> {intl.formatMessage({ id: 'profile.resetKey' })}</Heading>
      <PrivateKeyInfo>
        {intl.formatMessage({ id: 'privateKey.modal.info' })}
      </PrivateKeyInfo>
      <ButtonWrapper>
        <PrimaryButton
          data-cy="resetPrivateKeyButton"
          onClick={openResetKeyModal}
        >
          {intl.formatMessage({ id: 'profile.resetKey' })}
        </PrimaryButton>
      </ButtonWrapper>
    </ResetKeyContent>
  );
};
