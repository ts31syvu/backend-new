import styled from 'styled-components';

export const ResetKeyContent = styled.div`
  padding: 24px 32px;
  background-color: white;
  border-bottom: 1px solid rgb(151, 151, 151);
`;

export const Heading = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 16px;
`;

export const PrivateKeyInfo = styled.div``;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 18px 0 32px 0;
`;
