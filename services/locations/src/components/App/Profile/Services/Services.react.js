import React from 'react';
import { useIntl } from 'react-intl';

import Icon from '@ant-design/icons';
import { getDownloadLinks, getExternalLinks } from './Services.helper';
import { Content, Heading, Wrapper, Link } from './Services.styled';

export const Services = () => {
  const intl = useIntl();

  const downloadLinks = getDownloadLinks(intl);
  const externalLinks = getExternalLinks(intl);

  return (
    <Content>
      <Heading>
        {intl.formatMessage({ id: 'profile.services.overview' })}
      </Heading>
      <Wrapper>
        {downloadLinks.map(({ intlId, dataCy, download, href, icon }) => (
          <Link key={intlId} data-cy={dataCy} download={download} href={href}>
            {intlId}
            <Icon component={icon.component} style={{ fontSize: icon.size }} />
          </Link>
        ))}
        {externalLinks.map(({ intlId, dataCy, href, icon }) => (
          <Link
            key={intlId}
            data-cy={dataCy}
            target="_blank"
            rel="noopener noreferrer"
            href={href}
          >
            {intlId}
            <Icon component={icon.component} style={{ fontSize: icon.size }} />
          </Link>
        ))}
      </Wrapper>
    </Content>
  );
};
