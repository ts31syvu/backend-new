import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form, notification } from 'antd';
import { useQueryClient } from 'react-query';

// Components
import { BusinessAddress } from 'components/Authentication/Register/steps/BusinessInfoStep/BusinessAddress';
import {
  ButtonWrapper,
  CardTitle,
  StyledInput,
} from 'components/Authentication/Authentication.styled';
import { PrimaryButton } from 'components/general';

// Actions
import { useNameValidator } from 'components/hooks/useValidators';
import { updateOperator } from 'network/api';

import { Description } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { Wrapper } from './BussinessAddressModal.styled';

const REQUIRED_KEYS = [
  'businessEntityName',
  'businessEntityCity',
  'businessEntityStreetName',
  'businessEntityStreetNumber',
  'businessEntityZipCode',
];

export const BusinessAddressModal = ({ closeModal }) => {
  const intl = useIntl();
  const businessNameValidator = useNameValidator('businessEntityName');
  const [businessInfo, setBusinessInfo] = useState({});
  const [disabled, setDisabled] = useState(false);
  const queryClient = useQueryClient();

  useEffect(() => {
    setDisabled(
      !REQUIRED_KEYS.every(item =>
        Object.prototype.hasOwnProperty.call(businessInfo, item)
      )
    );
  }, [businessInfo]);

  const onFinish = ({
    businessEntityCity,
    businessEntityName,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
  }) => {
    const updatedAddress = {
      businessEntityCity,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
    };

    updateOperator(updatedAddress)
      .then(response => {
        if (response.status !== 204) {
          notification.error({
            message: intl.formatMessage({
              id: 'notification.profile.updateUser.error',
            }),
          });
          return;
        }

        notification.success({
          message: intl.formatMessage({
            id: 'notification.profile.updateUser.success',
          }),
        });

        queryClient.invalidateQueries('me');

        closeModal();
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'notification.profile.updateUser.error',
          }),
        });
      });
  };

  const onValuesChange = values => {
    setBusinessInfo({
      ...businessInfo,
      ...values,
    });
  };

  return (
    <Wrapper>
      <Form onFinish={onFinish} onValuesChange={onValuesChange}>
        <CardTitle data-cy="modalBusinessAddressTitle">
          {intl.formatMessage({
            id: 'modal.businessAddressTitle',
          })}
        </CardTitle>
        <Description>
          {intl.formatMessage({
            id: 'modal.businessAddressDescription',
          })}
        </Description>
        <Form.Item
          data-cy="businessEntityName"
          colon={false}
          name="businessEntityName"
          label={intl.formatMessage({
            id: 'register.businessEntityName',
          })}
          rules={businessNameValidator}
        >
          <StyledInput autoFocus />
        </Form.Item>

        <BusinessAddress />

        <ButtonWrapper>
          <Form.Item>
            <PrimaryButton
              disabled={disabled}
              htmlType="submit"
              data-cy="confirmNameButton"
            >
              {intl.formatMessage({
                id: 'location.save',
              })}
            </PrimaryButton>
          </Form.Item>
        </ButtonWrapper>
      </Form>
    </Wrapper>
  );
};
