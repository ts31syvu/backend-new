import styled from 'styled-components';
import QRCode from 'qrcode.react';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Description = styled.div`
  margin-bottom: 40px;
`;

export const QrCodeWrapper = styled.div`
  margin-bottom: 40px;
  width: 100%;
  text-align: center;
`;

export const StyledQRCode = styled(QRCode)`
  border-radius: 4px;
  padding: 8px;
  box-shadow: 0 2px 4px 0 rgba(143, 143, 143, 0.5);
`;
