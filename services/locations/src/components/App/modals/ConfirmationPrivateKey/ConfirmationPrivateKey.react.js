import React from 'react';
import { useIntl } from 'react-intl';
import { CheckIcon } from 'assets/icons';
import { PrimaryButton } from 'components/general';
import { Confirmation, Wrapper } from './ConfirmationPrivateKey.styled';

export const ConfirmationPrivateKey = ({ onFinish }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <CheckIcon />
      <Confirmation>
        {intl.formatMessage({
          id: 'confirmationPrivateKey',
        })}
      </Confirmation>
      <PrimaryButton onClick={onFinish} data-cy="finish">
        {intl.formatMessage({
          id: 'modal.registerOperator.finish',
        })}
      </PrimaryButton>
    </Wrapper>
  );
};
