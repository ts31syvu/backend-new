import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const Confirmation = styled.div`
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  font-size: 16px;
  margin: 36px 0;
`;
