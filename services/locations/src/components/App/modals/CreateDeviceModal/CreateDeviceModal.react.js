import React, { useState, useEffect, useCallback } from 'react';

import { useIntl } from 'react-intl';
import { notification } from 'antd';
import { useQueryClient } from 'react-query';

import { createDevice } from 'network/api';
import { QUERY_KEYS, useGetPrivateKeySecret } from 'components/hooks/queries';
import { generatePrivateKeyFile } from 'utils/privateKey';
import { generatePIN, generateTransferQRCodeData } from 'utils/operatorDevice';
import { useChallenge } from 'components/hooks/useChallenge';
import { useModal } from 'components/hooks/useModal';
import { Steps } from 'components/general';

import {
  SELECT_DEVICE_ROLE_STEP,
  DEVICE_PRIVATE_KEY_QR_CODE_STEP,
  DEVICE_AUTHENTICATION_QR_CODE_STEP,
  DEVICE_PRIVATE_KEY_ENCRYPTION_PIN_STEP,
  DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP,
} from './CreateDeviceModal.helper';

import { PrivateKeyPIN } from '../deviceSetupSteps/PrivateKeyPIN';
import { PrivateKeyQRCode } from '../deviceSetupSteps/PrivateKeyQRCode';
import { SelectDeviceRole } from '../deviceSetupSteps/SelectDeviceRole';
import { DeviceAuthenticationPIN } from '../deviceSetupSteps/DeviceAuthenticationPIN';
import { DeviceAuthenticationQRCode } from '../deviceSetupSteps/DeviceAuthenticationQRCode';

export const CreateDeviceModal = ({ privateKey }) => {
  const intl = useIntl();
  const [, closeModal] = useModal();
  const queryClient = useQueryClient();
  const { challenge, challengeState } = useChallenge();

  const [currentStep, setCurrentStep] = useState(0);
  const [deviceRole, setDeviceRole] = useState(null);
  const [privateKeyPIN, setPrivateKeyPIN] = useState('');
  const [authenticationPIN, setAuthenticationPIN] = useState('');
  const [privateKeyQRCode, setPrivateKeyQRCode] = useState(null);
  const [authenticationQRCode, setAuthenticationQRCode] = useState(null);
  const { data: privateKeySecret, isLoading } = useGetPrivateKeySecret();

  const isPrivateKeyNeeded = deviceRole !== 'scanner';
  const numberOfSteps = isPrivateKeyNeeded ? 4 : 2;

  const onCancel = useCallback(() => {
    closeModal();
  }, [closeModal]);

  const onDone = useCallback(() => {
    closeModal();
    queryClient.invalidateQueries(QUERY_KEYS.OPERATOR_DEVICES);
  }, [closeModal, queryClient]);

  useEffect(() => {
    if (challengeState.authPinRequired) {
      setCurrentStep(2);
      return;
    }

    if (challengeState.privateKeyRequired) {
      if (isPrivateKeyNeeded) {
        setCurrentStep(3);
        return;
      }

      onCancel();
      return;
    }

    if (challengeState.privateKeyPinRequired) {
      if (isPrivateKeyNeeded) {
        setCurrentStep(4);
        return;
      }

      onCancel();
      return;
    }

    if (challengeState.done) {
      notification.success({
        message: intl.formatMessage({ id: 'notification.device.success' }),
      });

      onDone();
      return;
    }

    if (challengeState.canceled) {
      onCancel();
    }
  }, [challengeState, intl, isPrivateKeyNeeded, onDone, onCancel]);

  const onCreate = useCallback(
    async role => {
      if (!challenge) return;

      try {
        const privateKeyEncryptionPIN = generatePIN();
        const authenticationEncryptionPIN = generatePIN();
        const device = await createDevice(role);

        setPrivateKeyPIN(privateKeyEncryptionPIN);
        setAuthenticationPIN(authenticationEncryptionPIN);

        const authenticationQRCodeData = await generateTransferQRCodeData(
          'AUTHENTICATION_TOKEN',
          device.refreshToken,
          authenticationEncryptionPIN,
          challenge.uuid
        );

        const privateKeyQRCodeData = await generateTransferQRCodeData(
          'PRIVATE_KEY',
          generatePrivateKeyFile(privateKey, privateKeySecret),
          privateKeyEncryptionPIN,
          challenge.uuid
        );

        setDeviceRole(role);
        setPrivateKeyQRCode(privateKeyQRCodeData);
        setAuthenticationQRCode(authenticationQRCodeData);
        setCurrentStep(1);
      } catch {
        notification.error({
          message: intl.formatMessage({ id: 'notification.device.error' }),
        });
      }
    },
    [challenge, intl, privateKey, privateKeySecret]
  );

  const steps = [
    {
      id: SELECT_DEVICE_ROLE_STEP,
      content: <SelectDeviceRole onCreate={onCreate} onCancel={onCancel} />,
    },
    {
      id: DEVICE_AUTHENTICATION_QR_CODE_STEP,
      content: (
        <DeviceAuthenticationQRCode
          currentStep={1}
          deviceRole={deviceRole}
          numberOfSteps={numberOfSteps}
          authenticationQRCode={authenticationQRCode}
        />
      ),
    },
    {
      id: DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP,
      content: (
        <DeviceAuthenticationPIN
          currentStep={2}
          deviceRole={deviceRole}
          numberOfSteps={numberOfSteps}
          authenticationPIN={authenticationPIN}
        />
      ),
    },
    ...(isPrivateKeyNeeded
      ? [
          {
            id: DEVICE_PRIVATE_KEY_QR_CODE_STEP,
            content: (
              <PrivateKeyQRCode
                currentStep={3}
                deviceRole={deviceRole}
                numberOfSteps={numberOfSteps}
                privateKeyQRCode={privateKeyQRCode}
              />
            ),
          },
          {
            id: DEVICE_PRIVATE_KEY_ENCRYPTION_PIN_STEP,
            content: (
              <PrivateKeyPIN
                currentStep={numberOfSteps}
                numberOfSteps={numberOfSteps}
                privateKeyPIN={privateKeyPIN}
              />
            ),
          },
        ]
      : []),
  ];

  if (isLoading) {
    return null;
  }

  return <Steps steps={steps} currentStep={currentStep} />;
};
