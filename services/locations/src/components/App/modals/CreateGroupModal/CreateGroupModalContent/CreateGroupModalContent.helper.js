import { getMinutesFromTimeString } from 'utils/time';

export const SELECT_GROUP_STEP = 'SELECT_GROUP_STEP';
export const NAME_INPUT_STEP = 'NAME_INPUT_STEP';
export const GOOGLE_PLACES_OPT_IN_STEP = 'GOOGLE_PLACES_OPT_IN_STEP';
export const ADDRESS_INPUT_STEP = 'ADDRESS_INPUT_STEP';
export const PHONE_INPUT_STEP = 'PHONE_INPUT_STEP';
export const TABLE_INPUT_STEP = 'TABLE_INPUT_STEP';
export const AUTOMATIC_CHECKOUT_STEP = 'AUTOMATIC_CHECKOUT_STEP';
export const COMPLETE_STEP = 'COMPLETE_STEP';
export const AREA_SELECTION_STEP = 'AREA_SELECTION_STEP';
export const AREA_DETAILS_STEP = 'AREA_DETAILS_STEP';
export const TOTAL_STEPS = 9;

export const getGroupPayload = ({
  groupName,
  phone,
  address,
  radius,
  tableCount,
  groupType,
  isIndoor,
  masks,
  ventilation,
  entryPolicyInfo,
  roomHeight,
  roomDepth,
  roomWidth,
  areas,
  averageCheckinTime,
  coordinates,
}) => ({
  type: groupType,
  name: groupName,
  phone,
  streetName: address?.streetName,
  streetNr: address?.streetNr,
  zipCode: address?.zipCode,
  city: address?.city,
  state: address?.state,
  lat: coordinates?.lat,
  lng: coordinates?.lng,
  radius: radius ? parseInt(radius, 10) : 0,
  tableCount: tableCount ? parseInt(tableCount, 10) : null,
  isIndoor,
  masks,
  ventilation,
  entryPolicyInfo,
  roomHeight: roomHeight ?? null,
  roomDepth: roomDepth ?? null,
  roomWidth: roomWidth ?? null,
  areas,
  averageCheckinTime: getMinutesFromTimeString(averageCheckinTime),
});
