import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Steps } from 'antd';

import { TableInput } from 'components/App/modals/generalOnboarding/TableInput';
import { Checkout } from 'components/App/modals/generalOnboarding/Checkout';
import { PhoneInput } from 'components/App/modals/generalOnboarding/PhoneInput';
import { AreaDetails } from 'components/App/modals/generalOnboarding/AreaDetails';

import { BASE_GROUP_ROUTE, BASE_LOCATION_ROUTE } from 'constants/routes';
import {
  SELECT_GROUP_STEP,
  NAME_INPUT_STEP,
  PHONE_INPUT_STEP,
  TABLE_INPUT_STEP,
  AUTOMATIC_CHECKOUT_STEP,
  COMPLETE_STEP,
  AREA_SELECTION_STEP,
  getGroupPayload,
  AREA_DETAILS_STEP,
  GOOGLE_PLACES_OPT_IN_STEP,
  TOTAL_STEPS,
} from './CreateGroupModalContent.helper';

import { SelectGroupType } from './steps/SelectGroupType';
import { AreaDivision } from './steps/AreaDivision';
import { Complete } from './steps/Complete';
import { GooglePlacesInput } from './steps/GooglePlacesInput';
import { NameInputGroup } from './steps/NameInputGroup';

export const CreateGroupModalContent = ({ group, setGroup, onClose }) => {
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState(0);
  const [groupType, setGroupType] = useState(null);
  const [groupName, setGroupName] = useState(null);
  const [googlePlaces, setGooglePlaces] = useState(false);
  const [address, setAddress] = useState(null);
  const [phone, setPhone] = useState(null);
  const [averageCheckinTime, setAverageCheckinTime] = useState(null);
  const [areaDetails, setAreaDetails] = useState({
    isIndoor: null,
    masks: null,
    ventilation: null,
    entryPolicyInfo: null,
    roomHeight: null,
    roomWidth: null,
    roomDepth: null,
  });
  const [tableCount, setTableCount] = useState(null);
  const [radius, setRadius] = useState(null);
  const [coordinates, setCoordinates] = useState(null);
  const [automaticCheckout, setAutomaticCheckout] = useState(false);
  const [automaticCheckInTime, setAutomaticCheckInTime] = useState(false);

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const onDone = () => {
    history.push(
      `${BASE_GROUP_ROUTE}${group.groupId}${BASE_LOCATION_ROUTE}${group.location.locationId}`
    );
    onClose();
  };

  const groupPayload = getGroupPayload({
    groupName,
    phone,
    address,
    radius,
    tableCount,
    groupType,
    isIndoor: areaDetails.isIndoor,
    masks: areaDetails.masks,
    ventilation: areaDetails.ventilation,
    entryPolicyInfo: areaDetails.entryPolicyInfo,
    roomHeight: areaDetails.roomHeight,
    roomDepth: areaDetails.roomDepth,
    roomWidth: areaDetails.roomWidth,
    averageCheckinTime,
    coordinates,
  });

  const steps = [
    {
      id: SELECT_GROUP_STEP,
      content: <SelectGroupType setGroupType={setGroupType} next={nextStep} />,
    },
    {
      id: NAME_INPUT_STEP,
      content: (
        <NameInputGroup
          groupName={groupName}
          setGroupName={setGroupName}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: PHONE_INPUT_STEP,
      content: (
        <PhoneInput
          phone={phone}
          setPhone={setPhone}
          titleId="modal.createGroup.phoneInput.title"
          descriptionId="modal.createGroup.phoneInput.description"
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: GOOGLE_PLACES_OPT_IN_STEP,
      content: (
        <GooglePlacesInput
          firstDescriptionId="modal.createGroup.googlePlacesInput.firstDescription"
          secondDescriptionId="modal.createGroup.googlePlacesInput.secondDescription"
          headingManualAddressId="modal.createGroup.googlePlacesInput.headingManualAddress"
          firstDescriptionManualAddressId="modal.createGroup.googlePlacesInput.firstDescriptionManualAddress"
          toggleDescriptionManualAddressId="modal.createGroup.googlePlacesInput.toggleDescriptionManualAddress"
          totalSteps={TOTAL_STEPS}
          setEnabled={setGooglePlaces}
          enabled={googlePlaces}
          address={address}
          setAddress={setAddress}
          coordinates={coordinates}
          setCoordinates={setCoordinates}
          googleEnabled={googlePlaces}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: AUTOMATIC_CHECKOUT_STEP,
      content: (
        <Checkout
          radiusTitleId="modal.createGroup.automaticCheckout.radiusTitle"
          radiusInfoId="modal.createGroup.automaticCheckout.radiusInfo"
          totalSteps={TOTAL_STEPS}
          radius={radius}
          setRadius={setRadius}
          coordinates={coordinates}
          googleEnabled={googlePlaces}
          setCoordinates={setCoordinates}
          averageCheckinTime={averageCheckinTime}
          setAverageCheckinTime={setAverageCheckinTime}
          automaticCheckout={automaticCheckout}
          setAutomaticCheckout={setAutomaticCheckout}
          automaticCheckInTime={automaticCheckInTime}
          setAutomaticCheckInTime={setAutomaticCheckInTime}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: AREA_DETAILS_STEP,
      content: (
        <AreaDetails
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          modalArea="createGroup"
        />
      ),
    },
    {
      id: TABLE_INPUT_STEP,
      content: (
        <TableInput
          tableCount={tableCount}
          setTableCount={setTableCount}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: AREA_SELECTION_STEP,
      content: (
        <AreaDivision
          groupType={groupType}
          next={nextStep}
          back={previousStep}
          groupPayload={groupPayload}
          onClose={onClose}
          setGroup={setGroup}
        />
      ),
    },
    {
      id: COMPLETE_STEP,
      content: <Complete group={group} done={onDone} back={previousStep} />,
    },
  ];

  return (
    <>
      <Steps
        progressDot={() => null}
        current={currentStep}
        style={{ margin: 0 }}
      >
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </>
  );
};
