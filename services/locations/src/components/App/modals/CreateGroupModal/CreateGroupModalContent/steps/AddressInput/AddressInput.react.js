import React, { useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { ManualAddressText } from 'components/App/modals/generalOnboarding/ManualAddressText';
import { GooglePlacesWrapper } from 'components/App/modals/generalOnboarding/GooglePlacesWrapper';
import {
  ButtonWrapper,
  Description,
  Header,
  InlineLink,
  Wrapper,
  GoogleMapSelectorWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { GoogleMapSelectMarker } from 'components/App/modals/generalOnboarding/GoogleMapSelectMarker';
import { LocationSearch } from './LocationSearch';
import { FormFields } from './FormFields';

export const AddressInput = ({
  address: currentAddress,
  setAddress,
  coordinates,
  setCoordinates,
  groupType,
  back,
  next,
  googleEnabled,
}) => {
  const intl = useIntl();
  const formReference = useRef(null);
  const [showManualInput, setShowManualInput] = useState(!googleEnabled);
  const [temporaryAddress, setTemporaryAddress] = useState(currentAddress);
  const [isError, setIsError] = useState(false);
  const [filled, setFilled] = useState(!!temporaryAddress);
  const [disabled, setDisabled] = useState(googleEnabled);
  const [showMapInput, setShowMapInput] = useState(false);

  const onFinish = values => {
    setTemporaryAddress(values);
    setAddress(values);
    next();
  };

  const onProceed = () => {
    if (coordinates != null) {
      formReference.current.setFieldsValue({ ...coordinates });
    }
    formReference.current.submit();
  };

  return (
    <Wrapper>
      <Header>
        {intl.formatMessage({
          id: `modal.createGroup.addressInput.${groupType}.title`,
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.addressInput.usageDecscription',
        })}
      </Description>
      <Description>
        {intl.formatMessage({
          id: `modal.createGroup.addressInput.${groupType}.description`,
        })}
      </Description>
      <Description
        style={{ fontWeight: 'bold', fontFamily: 'Montserrat-bold,sans-serif' }}
      >
        {intl.formatMessage({
          id: 'modal.addressInput.help.description',
        })}
      </Description>
      <GooglePlacesWrapper enabled={googleEnabled}>
        <Form
          ref={formReference}
          onFinish={onFinish}
          initialValues={temporaryAddress}
        >
          {googleEnabled ? (
            <>
              <LocationSearch
                formReference={formReference}
                setCoordinates={setCoordinates}
                setFilled={setFilled}
                setDisabled={setDisabled}
                isError={isError}
                setIsError={setIsError}
              />
              <InlineLink
                onClick={() => {
                  setShowMapInput(!showMapInput);
                }}
              >
                {intl.formatMessage({
                  id: showMapInput
                    ? 'addressInput.disableMapInputTitle'
                    : 'addressInput.enableMapInputTitle',
                })}
              </InlineLink>

              {showMapInput && (
                <GoogleMapSelectorWrapper>
                  <GoogleMapSelectMarker
                    coordinates={coordinates}
                    setCoordinates={setCoordinates}
                  />
                </GoogleMapSelectorWrapper>
              )}

              <InlineLink
                data-cy="manuellSearch"
                onClick={() => {
                  setDisabled(false);
                  setShowManualInput(true);
                }}
              >
                {intl.formatMessage({ id: 'addressInput.manualInputTitle' })}
              </InlineLink>
            </>
          ) : (
            <ManualAddressText />
          )}

          <FormFields show={filled || showManualInput} disabled={disabled} />
          <ButtonWrapper multipleButtons>
            <SecondaryButton onClick={back}>
              {intl.formatMessage({
                id: 'authentication.form.button.back',
              })}
            </SecondaryButton>
            <PrimaryButton
              data-cy="proceed"
              disabled={isError || (!filled && !showManualInput)}
              onClick={onProceed}
            >
              {intl.formatMessage({
                id: 'authentication.form.button.next',
              })}
            </PrimaryButton>
          </ButtonWrapper>
        </Form>
      </GooglePlacesWrapper>
    </Wrapper>
  );
};
