import React, { useEffect, useState } from 'react';

import { v4 as uuidv4 } from 'uuid';
import { useIntl } from 'react-intl';
import { Form, notification } from 'antd';
import { Switch } from 'components/general';
import {
  Header,
  Wrapper,
  Description,
  SwitchWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { createGroup as createGroupRequest } from 'network/api';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { ButtonRow } from 'components/App/modals/generalOnboarding/common/ButtonRow';
import { useQueryClient } from 'react-query';
import { QUERY_KEYS } from 'components/hooks/queries';
import { AreaNameInput } from './AreaNameInput';

export const AreaDivision = ({
  setGroup,
  next,
  back,
  groupPayload,
  onClose,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [isAreaSelection, setIsAreaSelection] = useState(false);
  const [temporaryAreas, setTemporaryAreas] = useState([]);
  const [form] = Form.useForm();

  const onChange = () => {
    setIsAreaSelection(!isAreaSelection);
  };

  useEffect(() => {
    if (isAreaSelection) {
      setTemporaryAreas([{ id: uuidv4() }]);

      form.setFieldsValue({
        [`areaType${temporaryAreas.length}`]: null,
      });
    } else setTemporaryAreas([]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAreaSelection]);

  const handleServerError = () => {
    notification.error({
      message: intl.formatMessage({
        id: 'notification.createGroup.error',
      }),
    });
    onClose();
  };

  const handleResponse = response => {
    if (!response) {
      handleServerError();
      return;
    }
    notification.success({
      message: intl.formatMessage({
        id: 'notification.createGroup.success',
      }),
    });
    setGroup(response);
    queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
  };

  const onFinish = values => {
    const addedAreas = temporaryAreas
      .map((area, index) => ({
        name: values[`areaName${index}`],
        isIndoor: values[`areaType${index}`],
      }))
      .filter(area => !!area.name);

    const createGroupPayload = { ...groupPayload, areas: addedAreas };

    createGroupRequest(createGroupPayload)
      .then(response => {
        handleResponse(response);
        next();
      })
      .catch(() => {
        handleServerError();
      });
  };

  return (
    <Wrapper>
      <StepProgress currentStep={8} totalSteps={9} />
      <Header>
        {intl.formatMessage({
          id: `modal.createGroup.areaSelection.title`,
        })}
      </Header>
      <Description>
        {intl.formatMessage(
          {
            id: `modal.createGroup.areaSelection.description`,
          },
          { br: <br /> }
        )}
      </Description>
      <SwitchWrapper>
        <Switch
          onChange={onChange}
          checked={isAreaSelection}
          data-cy="toggleAreaDivision"
        />
      </SwitchWrapper>

      <Form onFinish={onFinish} form={form}>
        {isAreaSelection && (
          <AreaNameInput
            temporaryAreas={temporaryAreas}
            setTemporaryAreas={setTemporaryAreas}
            next={next}
            back={back}
            form={form}
          />
        )}
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'createGroup.button.done',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
