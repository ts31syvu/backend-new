import React, { useState } from 'react';
import { useIntl } from 'react-intl';

// Worker
import { useWorker } from 'components/hooks/useWorker';
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';

import {
  ButtonWrapper,
  QRCodeDownloadWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import {
  PrimaryButton,
  SuccessButton,
} from 'components/general/Buttons.styled';
import { QRDownloadCard } from 'components/App/modals/generalOnboarding/common/QRDownloadCard/QRDownloadCard.react';
import { useGetLocationById } from 'components/hooks/queries';

export const QRDownload = ({ group, done }) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);

  const { data: location, isLoading, error } = useGetLocationById(
    group.location.locationId
  );

  const pdfWorkerApiReference = useWorker(getPDFWorker());

  if (isLoading || error) return null;

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location,
    intl,
    isTableQRCodeEnabled: location.tableCount,
    isCWAEventEnabled: true,
  };

  const triggerDownload = () => downloadPDF(downloadOptions);

  return (
    <QRDownloadCard
      title={intl.formatMessage({
        id: 'modal.createGroup.QRDownload.title',
      })}
      description={intl.formatMessage({
        id: 'modal.createGroup.QRDownload.description',
      })}
    >
      <QRCodeDownloadWrapper>
        <SuccessButton
          loading={isDownloading}
          disabled={isDownloading}
          onClick={triggerDownload}
          data-cy="downloadQRCode"
        >
          {intl.formatMessage({
            id: location.tableCount
              ? 'qrCodesDownload.table'
              : 'qrCodesDownload.entire',
          })}
        </SuccessButton>
      </QRCodeDownloadWrapper>
      <ButtonWrapper>
        <PrimaryButton data-cy="done" onClick={done}>
          {intl.formatMessage({
            id: 'done',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </QRDownloadCard>
  );
};
