import styled from 'styled-components';
import { Form } from 'antd';

export const RowWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledFormItemLarge = styled(Form.Item)`
  width: 60%;
  display: ${({ $visible }) => ($visible ? 'unset' : 'none')};
`;

export const StyledFormItemSmall = styled(Form.Item)`
  width: 38%;
  display: ${({ $visible }) => ($visible ? 'unset' : 'none')};
`;
