import React, { useState } from 'react';

import { GoogleMapSelectMarker } from 'components/App/modals/generalOnboarding/GoogleMapSelectMarker';
import { LocationSearch } from '../LocationSearch';
import { FormFields } from '../FormFields';
import {
  AddressInfo,
  CompanyName,
  GoogleAddressWrapper,
  GoogleMapSelectorWrapper,
  PreviewAddress,
} from './GoogleAddress.styled';

export const GoogleAddress = ({
  coordinates,
  setCoordinates,
  formReference,
  setFilled,
  filled,
  setIsError,
  isError,
  hasPlace,
  setHasPlace,
}) => {
  const [addressFromPlace, setAddressFromPlace] = useState({});

  return (
    <>
      <LocationSearch
        formReference={formReference}
        setCoordinates={setCoordinates}
        setFilled={setFilled}
        isError={isError}
        setIsError={setIsError}
        setHasPlace={setHasPlace}
        setAddressFromPlace={setAddressFromPlace}
      />

      <GoogleAddressWrapper filled={filled}>
        {filled && (
          <PreviewAddress data-cy="googlemapAddressPreview">
            <CompanyName>{addressFromPlace?.locationName}</CompanyName>
            <AddressInfo>{`${addressFromPlace?.streetName} ${addressFromPlace?.streetNr}`}</AddressInfo>
            <AddressInfo>{`${addressFromPlace?.zipCode} ${addressFromPlace?.state}`}</AddressInfo>
          </PreviewAddress>
        )}
        <GoogleMapSelectorWrapper $filled={filled}>
          <GoogleMapSelectMarker
            coordinates={coordinates}
            setCoordinates={setCoordinates}
          />
        </GoogleMapSelectorWrapper>
      </GoogleAddressWrapper>
      <FormFields visible={hasPlace && !filled} data-cy="manualAddressInput" />
    </>
  );
};
