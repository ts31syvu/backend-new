import styled from 'styled-components';

export const GoogleAddressWrapper = styled.div`
  display: ${({ filled }) => (filled ? 'flex' : 'unset')};
  justify-content: space-between;
`;

export const PreviewAddress = styled.div`
  padding-left: 30px;
  padding-top: 35px;
`;

export const CompanyName = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const AddressInfo = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const GoogleMapSelectorWrapper = styled.div`
  height: ${({ $filled }) => ($filled ? '240px' : '350px')};
  width: ${({ $filled }) => ($filled ? '318px' : 'unset')};
`;
