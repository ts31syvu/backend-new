import React, { useEffect, useRef, useState } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { PrimaryButton, SecondaryButton, Switch } from 'components/general';
import {
  ButtonWrapper,
  Description,
  Header,
  Wrapper,
  StyledSwitchContainer,
  ToggleDescription,
  Expand,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { useGetMe } from 'components/hooks/queries';
import { GooglePlacesWrapper } from 'components/App/modals/generalOnboarding/GooglePlacesWrapper';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress/StepProgress.react';
import { FormFields } from './FormFields';
import { GoogleAddress } from './GoogleAddress';

// eslint-disable-next-line complexity
export const GooglePlacesInput = ({
  totalSteps,
  setEnabled,
  enabled,
  back,
  next,
  address,
  setAddress,
  coordinates,
  setCoordinates,
  firstDescriptionId,
  secondDescriptionId,
  headingManualAddressId,
  firstDescriptionManualAddressId,
  baseLocation,
  toggleDescriptionManualAddressId,
}) => {
  const intl = useIntl();
  const formReference = useRef(null);
  const [displayManualAddress, setDisplayManualAddress] = useState(false);
  const [businessAddressEnabled, setBusinessAddressEnabled] = useState(false);
  const [temporaryAddress, setTemporaryAddress] = useState(address);
  const [isError, setIsError] = useState(false);
  const [filled, setFilled] = useState(false);
  const [hasPlace, setHasPlace] = useState(false);

  const { data: operator } = useGetMe();

  useEffect(() => {
    if (businessAddressEnabled) {
      formReference?.current?.setFieldsValue({
        city: baseLocation?.city || operator?.businessEntityCity,
        streetName:
          baseLocation?.streetName || operator?.businessEntityStreetName,
        streetNr:
          baseLocation?.streetNr || operator?.businessEntityStreetNumber,
        zipCode: baseLocation?.zipCode || operator?.businessEntityZipCode,
      });
    } else {
      formReference?.current?.setFieldsValue({
        city: '',
        streetName: '',
        streetNr: '',
        zipCode: '',
      });
    }
  }, [
    baseLocation?.city,
    baseLocation?.streetName,
    baseLocation?.streetNr,
    baseLocation?.zipCode,
    businessAddressEnabled,
    operator?.businessEntityCity,
    operator?.businessEntityStreetName,
    operator?.businessEntityStreetNumber,
    operator?.businessEntityZipCode,
    temporaryAddress,
  ]);

  const onFinish = values => {
    setTemporaryAddress(values);
    setAddress(values);
    next();
  };

  const processContinue = () => {
    if (!enabled && !displayManualAddress) {
      return setDisplayManualAddress(true);
    }
    if (coordinates != null) {
      formReference.current.setFieldsValue({ ...coordinates });
    }

    return formReference.current.submit();
  };

  return (
    <Wrapper>
      <StepProgress currentStep={4} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: displayManualAddress
            ? headingManualAddressId
            : 'modal.createGroup.googlePlacesInput.heading',
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: displayManualAddress
            ? firstDescriptionManualAddressId
            : firstDescriptionId,
        })}
      </Description>
      {!displayManualAddress && (
        <Description>
          {intl.formatMessage(
            {
              id: secondDescriptionId,
            },
            // eslint-disable-next-line react/display-name
            { br: <br />, b: (...chunks) => <b>{chunks}</b> }
          )}
        </Description>
      )}

      <StyledSwitchContainer>
        <ToggleDescription>
          {intl.formatMessage({
            id: displayManualAddress
              ? toggleDescriptionManualAddressId
              : 'modal.createGroup.googlePlacesInput.toggleDescription',
          })}
        </ToggleDescription>
        <Switch
          data-cy={
            displayManualAddress
              ? 'toggleUseBusinessAddress'
              : 'toggleGoogleService'
          }
          checked={displayManualAddress ? businessAddressEnabled : enabled}
          onChange={() => {
            if (displayManualAddress) {
              return setBusinessAddressEnabled(!businessAddressEnabled);
            }

            setTemporaryAddress({
              city: '',
              streetName: '',
              streetNr: '',
              zipCode: '',
            });
            setFilled(false);
            setHasPlace(false);
            return setEnabled(!enabled);
          }}
        />
      </StyledSwitchContainer>
      <Expand open={displayManualAddress || enabled}>
        {displayManualAddress && (
          <Wrapper>
            <Form
              ref={formReference}
              onFinish={onFinish}
              initialValues={temporaryAddress}
            >
              <FormFields visible />
            </Form>
          </Wrapper>
        )}
        {enabled && (
          <Wrapper>
            <GooglePlacesWrapper enabled>
              <Form
                ref={formReference}
                onFinish={onFinish}
                initialValues={temporaryAddress}
              >
                <GoogleAddress
                  coordinates={coordinates}
                  setCoordinates={setCoordinates}
                  formReference={formReference}
                  setFilled={setFilled}
                  filled={filled}
                  setIsError={setIsError}
                  isError={isError}
                  hasPlace={hasPlace}
                  setHasPlace={setHasPlace}
                />
              </Form>
            </GooglePlacesWrapper>
          </Wrapper>
        )}
      </Expand>
      <ButtonWrapper multipleButtons>
        <SecondaryButton onClick={back} data-cy="previousStep">
          {intl.formatMessage({
            id: 'authentication.form.button.back',
          })}
        </SecondaryButton>
        <div>
          <PrimaryButton
            data-cy="nextStep"
            onClick={processContinue}
            disabled={isError || (enabled && !hasPlace)}
          >
            {intl.formatMessage({
              id: 'modal.createGroup.googlePlacesInput.continue',
            })}
          </PrimaryButton>
        </div>
      </ButtonWrapper>
    </Wrapper>
  );
};
