import styled from 'styled-components';
import { Input, Form } from 'antd';
import Icon from '@ant-design/icons';

export const StyledInput = styled(Input)`
  border-radius: 0;
  border: none;
  box-shadow: none;
  border-bottom: 1px solid rgb(0, 0, 0);
  padding-left: 30px;
  :focus {
    box-shadow: none;
    border-bottom: 1px solid rgb(0, 0, 0);
  }
`;

export const StyledIcon = styled(Icon)`
  position: absolute;
  top: 7px;
`;

export const StyledItem = styled(Form.Item)`
  width: 348px;
`;

export const SearchWrapper = styled.div`
  position: relative;
`;
