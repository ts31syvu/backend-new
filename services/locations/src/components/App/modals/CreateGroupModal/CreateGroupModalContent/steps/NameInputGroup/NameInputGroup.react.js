import React from 'react';

import { useNameValidator } from 'components/hooks/useValidators';

import { NameInput } from 'components/App/modals/generalOnboarding/NameInput';

export const NameInputGroup = ({
  groupName: currentGroupName,
  setGroupName,
  totalSteps,
  back,
  next,
}) => {
  const groupNameValidator = useNameValidator('groupName');

  const onFinish = values => {
    const { groupName } = values;
    setGroupName(groupName);
    next();
  };

  return (
    <NameInput
      initialValues={{ groupName: currentGroupName ?? '' }}
      modalArea="createGroup"
      back={back}
      onFinish={onFinish}
      totalSteps={totalSteps}
      rules={groupNameValidator}
      name="groupName"
    />
  );
};
