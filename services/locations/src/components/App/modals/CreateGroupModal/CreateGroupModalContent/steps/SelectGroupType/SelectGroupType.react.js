import React from 'react';
import { useIntl } from 'react-intl';

import {
  Wrapper,
  Header,
  Description,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';

import { groupOptions } from 'components/App/modals/generalOnboarding/SelectType';

import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import {
  Selection,
  TypeWrapper,
  StyledIcon,
  StyledTypeText,
} from './SelectGroupType.styled';

export const SelectGroupType = ({ next, setGroupType }) => {
  const intl = useIntl();

  const select = type => {
    setGroupType(type);
    next();
  };

  return (
    <Wrapper data-cy="selectGroupType">
      <StepProgress currentStep={1} totalSteps={9} />
      <Header>
        {intl.formatMessage({ id: 'modal.createGroup.selectType.title' })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.selectType.description',
        })}
      </Description>
      <Selection>
        {groupOptions.map(option => (
          <TypeWrapper
            data-cy={option.type}
            key={option.type}
            onClick={() => select(option.type)}
          >
            <StyledIcon component={option.icon} />
            <StyledTypeText>
              {intl.formatMessage({
                id: option.intlId,
              })}
            </StyledTypeText>
          </TypeWrapper>
        ))}
      </Selection>
    </Wrapper>
  );
};
