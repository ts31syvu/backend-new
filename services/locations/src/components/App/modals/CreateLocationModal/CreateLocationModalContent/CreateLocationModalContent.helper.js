import { getMinutesFromTimeString } from 'utils/time';

export const RESTAURANT_TYPE = 'restaurant';

export const SELECT_LOCATION_STEP = 'SELECT_LOCATION_STEP';
export const NAME_INPUT_STEP = 'NAME_INPUT_STEP';
export const ADDRESS_INPUT_STEP = 'ADDRESS_INPUT_STEP';
export const PHONE_INPUT_STEP = 'PHONE_INPUT_STEP';
export const TABLE_INPUT_STEP = 'TABLE_INPUT_STEP';
export const AUTOMATIC_CHECKOUT_STEP = 'AUTOMATIC_CHECKOUT_STEP';
export const COMPLETE_STEP = 'COMPLETE_STEP';
export const AREA_DETAILS_STEP = 'AREA_DETAILS_STEP';
export const TOTAL_STEPS = 8;

export const BASE_ADDRESS_INDICATOR = 'BASE_ADDRESS_INDICATOR';

// eslint-disable-next-line complexity
export const getLocationPayload = ({
  groupId,
  locationName,
  phone,
  address,
  baseLocation,
  radius,
  tableCount,
  locationType,
  isIndoor,
  masks,
  ventilation,
  entryPolicyInfo,
  roomHeight,
  roomDepth,
  roomWidth,
  averageCheckinTime,
  coordinates,
}) => {
  const isSameAddress = address === BASE_ADDRESS_INDICATOR;

  return {
    groupId,
    locationName,
    phone,
    streetName: isSameAddress ? baseLocation?.streetName : address?.streetName,
    streetNr: isSameAddress ? baseLocation?.streetNr : address?.streetNr,
    zipCode: isSameAddress ? baseLocation?.zipCode : address?.zipCode,
    city: isSameAddress ? baseLocation?.city : address?.city,
    state: isSameAddress ? baseLocation?.state : address?.state,
    lat: isSameAddress ? baseLocation?.lat : coordinates?.lat,
    lng: isSameAddress ? baseLocation?.lng : coordinates?.lng,
    radius: radius ? parseInt(radius, 10) : 0,
    tableCount: tableCount ? parseInt(tableCount, 10) : null,
    type: locationType,
    isIndoor,
    masks,
    ventilation,
    entryPolicyInfo,
    roomHeight: roomHeight ?? null,
    roomDepth: roomDepth ?? null,
    roomWidth: roomWidth ?? null,
    averageCheckinTime: getMinutesFromTimeString(averageCheckinTime),
  };
};
