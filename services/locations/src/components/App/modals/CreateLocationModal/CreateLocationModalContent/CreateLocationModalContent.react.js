import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Steps } from 'antd';

import { getBaseLocationFromGroup } from 'utils/group';

import { useGetGroup } from 'components/hooks/queries';
import { TableInput } from 'components/App/modals/generalOnboarding/TableInput';
import { Checkout } from 'components/App/modals/generalOnboarding/Checkout';
import { PhoneInput } from 'components/App/modals/generalOnboarding/PhoneInput';

import { BASE_GROUP_ROUTE, BASE_LOCATION_ROUTE } from 'constants/routes';

import { AreaDetails } from 'components/App/modals/generalOnboarding/AreaDetails';
import {
  AUTOMATIC_CHECKOUT_STEP,
  COMPLETE_STEP,
  NAME_INPUT_STEP,
  PHONE_INPUT_STEP,
  SELECT_LOCATION_STEP,
  TABLE_INPUT_STEP,
  getLocationPayload,
  AREA_DETAILS_STEP,
  TOTAL_STEPS,
} from './CreateLocationModalContent.helper';

import { SelectLocationType } from './steps/SelectLocationType';
import { NameInputLocations } from './steps/NameInputLocations';
import { Complete } from './steps/Complete';
import { GOOGLE_PLACES_OPT_IN_STEP } from '../../CreateGroupModal/CreateGroupModalContent/CreateGroupModalContent.helper';
import { GooglePlacesInput } from '../../CreateGroupModal/CreateGroupModalContent/steps/GooglePlacesInput';

export const CreateLocationModalContent = ({
  groupId,
  setLocationId,
  onClose,
}) => {
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState(0);
  const [locationType, setLocationType] = useState(null);
  const [locationName, setLocationName] = useState(null);
  const [address, setAddress] = useState(null);
  const [phone, setPhone] = useState(null);
  const [areaDetails, setAreaDetails] = useState({
    isIndoor: null,
    masks: null,
    ventilation: null,
    entryPolicyInfo: null,
    roomHeight: null,
    roomWidth: null,
    roomDepth: null,
  });
  const [averageCheckinTime, setAverageCheckinTime] = useState(null);
  const [tableCount, setTableCount] = useState(null);
  const [radius, setRadius] = useState(null);
  const [location, setLocation] = useState(null);
  const [baseLocation, setBaseLocation] = useState(null);
  const [coordinates, setCoordinates] = useState(null);
  const [googlePlaces, setGooglePlaces] = useState(false);
  const [automaticCheckout, setAutomaticCheckout] = useState(false);
  const [automaticCheckInTime, setAutomaticCheckInTime] = useState(false);

  const { isLoading, error, data: group } = useGetGroup(groupId);

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const onDone = () => {
    history.push(
      `${BASE_GROUP_ROUTE}${groupId}${BASE_LOCATION_ROUTE}${location.uuid}`
    );
    onClose();
  };

  const locationPayload = getLocationPayload({
    groupId,
    locationName,
    phone,
    address,
    baseLocation,
    radius,
    tableCount,
    locationType,
    isIndoor: areaDetails.isIndoor,
    masks: areaDetails.masks,
    ventilation: areaDetails.ventilation,
    entryPolicyInfo: areaDetails.entryPolicyInfo,
    roomHeight: areaDetails.roomHeight,
    roomDepth: areaDetails.roomDepth,
    roomWidth: areaDetails.roomWidth,
    averageCheckinTime,
    coordinates,
  });

  const steps = [
    {
      id: SELECT_LOCATION_STEP,
      content: (
        <SelectLocationType
          setLocationType={setLocationType}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: NAME_INPUT_STEP,
      content: (
        <NameInputLocations
          locationType={locationType}
          locationName={locationName}
          setLocationName={setLocationName}
          next={nextStep}
          back={previousStep}
          groupId={groupId}
          totalSteps={TOTAL_STEPS}
        />
      ),
    },
    {
      id: PHONE_INPUT_STEP,
      content: (
        <PhoneInput
          phone={phone}
          setPhone={setPhone}
          titleId="modal.createLocation.phoneInput.title"
          descriptionId="modal.createLocation.phoneInput.description"
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: GOOGLE_PLACES_OPT_IN_STEP,
      content: (
        <GooglePlacesInput
          firstDescriptionId="modal.createGroup.googlePlacesInput.firstDescriptionArea"
          secondDescriptionId="modal.createGroup.googlePlacesInput.secondDescriptionArea"
          headingManualAddressId="modal.createGroup.googlePlacesInput.headingManualAddressArea"
          firstDescriptionManualAddressId="modal.createGroup.googlePlacesInput.firstDescriptionManualAddressArea"
          toggleDescriptionManualAddressId="modal.createGroup.googlePlacesInput.toggleDescriptionManualAddressArea"
          baseLocation={baseLocation}
          totalSteps={TOTAL_STEPS}
          setEnabled={setGooglePlaces}
          enabled={googlePlaces}
          address={address}
          setAddress={setAddress}
          coordinates={coordinates}
          setCoordinates={setCoordinates}
          googleEnabled={googlePlaces}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: AUTOMATIC_CHECKOUT_STEP,
      content: (
        <Checkout
          radiusTitleId="modal.createGroup.automaticCheckout.radiusTitleArea"
          radiusInfoId="modal.createGroup.automaticCheckout.radiusInfoArea"
          totalSteps={TOTAL_STEPS}
          radius={radius}
          setRadius={setRadius}
          coordinates={coordinates}
          googleEnabled={googlePlaces}
          setCoordinates={setCoordinates}
          averageCheckinTime={averageCheckinTime}
          setAverageCheckinTime={setAverageCheckinTime}
          automaticCheckout={automaticCheckout}
          setAutomaticCheckout={setAutomaticCheckout}
          automaticCheckInTime={automaticCheckInTime}
          setAutomaticCheckInTime={setAutomaticCheckInTime}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: AREA_DETAILS_STEP,
      content: (
        <AreaDetails
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
          totalSteps={TOTAL_STEPS}
          next={nextStep}
          back={previousStep}
          modalArea="createLocation"
        />
      ),
    },
    {
      id: TABLE_INPUT_STEP,
      content: (
        <TableInput
          tableCount={tableCount}
          setTableCount={setTableCount}
          next={nextStep}
          back={previousStep}
          totalSteps={TOTAL_STEPS}
          setLocationId={setLocationId}
          setLocation={setLocation}
          onClose={onClose}
          locationPayload={locationPayload}
          isLocationModal
        />
      ),
    },
    {
      id: COMPLETE_STEP,
      content: (
        <Complete
          next={nextStep}
          back={previousStep}
          location={location}
          group={group}
          done={onDone}
        />
      ),
    },
  ];

  useEffect(() => {
    if (isLoading || error || baseLocation) return;
    setBaseLocation(getBaseLocationFromGroup(group));
  }, [isLoading, error, baseLocation, group]);

  if (isLoading || error) return null;

  return (
    <>
      <Steps
        progressDot={() => null}
        current={currentStep}
        style={{ margin: 0 }}
      >
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </>
  );
};
