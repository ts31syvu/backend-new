import React, { useState, useRef } from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import {
  PrimaryButton,
  SecondaryButton,
  WhiteButton,
} from 'components/general';

import { LoadScript } from '@react-google-maps/api';

import { GOOGLE_LIBRARIES, GOOGLE_MAPS_API_KEY } from 'constants/googleApi';

import { ManualAddressText } from 'components/App/modals/generalOnboarding/ManualAddressText';
import {
  Wrapper,
  Header,
  Description,
  ButtonWrapper,
  InlineLink,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { BASE_ADDRESS_INDICATOR } from 'components/App/modals/CreateLocationModal/CreateLocationModalContent/CreateLocationModalContent.helper';
import { GoogleMapSelectMarker } from 'components/App/modals/generalOnboarding/GoogleMapSelectMarker';
import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import { FormFields } from './FormFields';
import { LocationSearch } from './LocationSearch';
import {
  AddressWrapper,
  Address,
  GoogleMapSelectorWrapper,
} from './AddressInput.styled';

export const AddressInput = ({
  baseLocation,
  address: currentAddress,
  setAddress,
  coordinates,
  setCoordinates,
  locationType,
  totalSteps,
  back,
  next,
}) => {
  const intl = useIntl();
  const formReference = useRef(null);
  const [showManualInput, setShowManualInput] = useState(false);
  const [temporaryAddress, setTemporaryAddress] = useState(currentAddress);
  const [isError, setIsError] = useState(false);
  const [isNewAddress, setIsNewAddress] = useState(false);
  const [filled, setFilled] = useState(!!temporaryAddress);
  const [disabled, setDisabled] = useState(true);
  const [showMapInput, setShowMapInput] = useState(false);

  const onFinish = values => {
    setTemporaryAddress(values);
    setAddress(values);
    next();
  };

  const onNext = () => {
    setAddress(BASE_ADDRESS_INDICATOR);
    next();
  };

  return (
    <>
      {!isNewAddress ? (
        <Wrapper>
          <StepProgress currentStep={4} totalSteps={totalSteps} />
          <Header>
            {intl.formatMessage({
              id: `modal.createLocation.addressInput.${locationType}.title`,
            })}
          </Header>
          <Description>
            {intl.formatMessage({
              id: `modal.createLocation.addressInput.${locationType}.sameAddress.description`,
            })}
          </Description>
          <AddressWrapper>
            <Address>
              {`${baseLocation.streetName} ${baseLocation.streetNr}`}
            </Address>
            <Address>{`${baseLocation.zipCode} ${baseLocation.city}`}</Address>
          </AddressWrapper>
          <ButtonWrapper
            multipleButtons
            style={isNewAddress ? { justifyContent: 'flex-end' } : {}}
          >
            <SecondaryButton onClick={back}>
              {intl.formatMessage({
                id: 'authentication.form.button.back',
              })}
            </SecondaryButton>
            <div>
              <WhiteButton
                data-cy="no"
                style={{ marginRight: 24 }}
                onClick={() => setIsNewAddress(true)}
              >
                {intl.formatMessage({
                  id: 'no',
                })}
              </WhiteButton>
              <PrimaryButton data-cy="yes" onClick={onNext}>
                {intl.formatMessage({
                  id: 'yes',
                })}
              </PrimaryButton>
            </div>
          </ButtonWrapper>
        </Wrapper>
      ) : (
        <Wrapper>
          <StepProgress currentStep={4} totalSteps={totalSteps} />
          <Header>
            {intl.formatMessage({
              id: `modal.createLocation.addressInput.${locationType}.title`,
            })}
          </Header>
          <Description>
            {intl.formatMessage({
              id: 'modal.createGroup.addressInput.usageDecscription',
            })}
          </Description>
          <Description>
            {intl.formatMessage({
              id: `modal.createLocation.addressInput.${locationType}.description`,
            })}
          </Description>
          <Description
            style={{
              fontWeight: 'bold',
              fontFamily: 'Montserrat-bold,sans-serif',
            }}
          >
            {intl.formatMessage({
              id: 'modal.addressInput.help.description',
            })}
          </Description>
          <LoadScript
            googleMapsApiKey={GOOGLE_MAPS_API_KEY}
            libraries={GOOGLE_LIBRARIES}
          >
            <Form
              ref={formReference}
              onFinish={onFinish}
              initialValues={temporaryAddress}
            >
              <LocationSearch
                formReference={formReference}
                setCoordinates={setCoordinates}
                setFilled={setFilled}
                setDisabled={setDisabled}
                isError={isError}
                setIsError={setIsError}
              />
              <InlineLink
                onClick={() => {
                  setShowMapInput(!showMapInput);
                }}
              >
                {intl.formatMessage({
                  id: showMapInput
                    ? 'addressInput.disableMapInputTitle'
                    : 'addressInput.enableMapInputTitle',
                })}
              </InlineLink>

              {showMapInput && (
                <GoogleMapSelectorWrapper>
                  <GoogleMapSelectMarker
                    coordinates={coordinates}
                    setCoordinates={setCoordinates}
                  />
                </GoogleMapSelectorWrapper>
              )}
              <InlineLink
                onClick={() => {
                  setShowManualInput(true);
                  setDisabled(false);
                }}
              >
                {intl.formatMessage({ id: 'addressInput.manualInputTitle' })}
              </InlineLink>
              {showManualInput && <ManualAddressText />}
              <FormFields
                disabled={disabled}
                show={filled || showManualInput}
              />
              <ButtonWrapper multipleButtons>
                <SecondaryButton onClick={back} data-cy="previousStep">
                  {intl.formatMessage({
                    id: 'authentication.form.button.back',
                  })}
                </SecondaryButton>
                <PrimaryButton
                  data-cy="nextStep"
                  disabled={isError || (!filled && !showManualInput)}
                  onClick={() => {
                    if (coordinates != null) {
                      formReference.current.setFieldsValue({ ...coordinates });
                    }
                    formReference.current.submit();
                  }}
                >
                  {intl.formatMessage({
                    id: 'authentication.form.button.next',
                  })}
                </PrimaryButton>
              </ButtonWrapper>
            </Form>
          </LoadScript>
        </Wrapper>
      )}
    </>
  );
};
