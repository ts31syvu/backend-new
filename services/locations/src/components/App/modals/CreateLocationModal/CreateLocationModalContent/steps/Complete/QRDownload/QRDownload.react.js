import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton, SuccessButton } from 'components/general';
import {
  ButtonWrapper,
  QRCodeDownloadWrapper,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';

// Worker
import { useWorker } from 'components/hooks/useWorker';
import { downloadPDF } from 'utils/downloadPDF';
import { getPDFWorker } from 'utils/workers';

import { QRDownloadCard } from 'components/App/modals/generalOnboarding/common/QRDownloadCard/QRDownloadCard.react';

export const QRDownload = ({ location, group, done }) => {
  const intl = useIntl();
  const [isDownloading, setIsDownloading] = useState(false);

  const pdfWorkerApiReference = useWorker(getPDFWorker());

  const downloadOptions = {
    setIsDownloading,
    pdfWorkerApiReference,
    location: {
      ...location,
      groupName: group?.name,
    },
    intl,
    isTableQRCodeEnabled: location.tableCount,
    isCWAEventEnabled: true,
  };
  const triggerDownload = () => downloadPDF(downloadOptions);

  return (
    <QRDownloadCard
      title={intl.formatMessage({
        id: 'modal.createLocation.QRDownload.title',
      })}
      description={intl.formatMessage({
        id: 'modal.createLocation.QRDownload.description',
      })}
    >
      <QRCodeDownloadWrapper>
        <SuccessButton
          loading={isDownloading}
          disabled={isDownloading}
          onClick={triggerDownload}
          data-cy="downloadQRCode"
        >
          {intl.formatMessage({
            id: location.tableCount
              ? 'qrCodesDownload.table'
              : 'qrCodesDownload.entire',
          })}
        </SuccessButton>
      </QRCodeDownloadWrapper>
      <ButtonWrapper>
        <PrimaryButton data-cy="done" onClick={done}>
          {intl.formatMessage({
            id: 'done',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </QRDownloadCard>
  );
};
