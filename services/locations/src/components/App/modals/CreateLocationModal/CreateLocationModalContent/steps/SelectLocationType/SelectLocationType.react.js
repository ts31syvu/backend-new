import React from 'react';
import { useIntl } from 'react-intl';

import {
  Wrapper,
  Header,
  Description,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';

import { locationOptions } from 'components/App/modals/generalOnboarding/SelectType';

import { StepProgress } from 'components/App/modals/generalOnboarding/StepProgress';
import {
  Selection,
  StyledIcon,
  StyledTypeText,
  TypeWrapper,
} from './SelectLocationType.styled';

export const SelectLocationType = ({ next, setLocationType, totalSteps }) => {
  const intl = useIntl();

  const select = type => {
    setLocationType(type);
    next();
  };

  return (
    <Wrapper>
      <StepProgress currentStep={1} totalSteps={totalSteps} />
      <Header data-cy="createAreaModalHeader">
        {intl.formatMessage({ id: 'modal.createLoction.selectType.title' })}
      </Header>
      <Description data-cy="createAreaModalDescription">
        {intl.formatMessage({
          id: 'modal.createLocation.selectType.description',
        })}
      </Description>
      <Selection>
        {locationOptions.map(option => (
          <TypeWrapper
            data-cy={option.type}
            key={option.type}
            onClick={() => select(option.type)}
          >
            <StyledIcon component={option.icon} />
            <StyledTypeText>
              {intl.formatMessage({
                id: option.intlId,
              })}
            </StyledTypeText>
          </TypeWrapper>
        ))}
      </Selection>
    </Wrapper>
  );
};
