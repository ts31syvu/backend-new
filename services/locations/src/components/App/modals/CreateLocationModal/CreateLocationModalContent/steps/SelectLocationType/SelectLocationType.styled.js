import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Selection = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export const StyledIcon = styled(Icon)`
  font-size: 40px;
  margin-bottom: 8px;
`;

export const StyledTypeText = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
`;

export const TypeWrapper = styled.div`
  width: 134px;
  display: flex;
  text-align: center;
  cursor: pointer;
  flex-direction: column;
  background: rgb(195, 206, 217);
  border-radius: 4px;
  padding: 24px 16px 10px 16px;
  margin-right: 16px;
  :last-child {
    margin-right: 0;
  }
`;
