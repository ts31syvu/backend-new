import styled from 'styled-components';

export const Header = styled.div`
  color: rgba(0, 0, 0, 0.87);
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
`;
