import React from 'react';

import { useIntl } from 'react-intl';
import { Popconfirm } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { PrimaryButton } from 'components/general';

export const CheckoutButton = ({ trace, onCheckoutSingleGuest }) => {
  const intl = useIntl();

  if (trace.checkout) return null;

  return (
    <Popconfirm
      placement="topLeft"
      onConfirm={() => onCheckoutSingleGuest(trace.traceId)}
      title={intl.formatMessage({
        id: 'location.checkout.confirmSingleCheckoutText',
      })}
      okText={intl.formatMessage({
        id: 'location.checkout.confirmButton',
      })}
      cancelText={intl.formatMessage({
        id: 'location.checkout.declineButton',
      })}
      icon={<QuestionCircleOutlined style={{ color: 'red' }} />}
    >
      <PrimaryButton headline="true" data-cy="checkoutGuestSingle">
        {intl.formatMessage({
          id: 'group.view.overview.checkoutSingleGuest',
        })}
      </PrimaryButton>
    </Popconfirm>
  );
};
