import styled from 'styled-components';

export const StyledContainer = styled.div`
  margin-right: 16px;

  .ant-select {
    .ant-select-selector {
      border-radius: unset;
      border: none;

      .ant-select-selection-item {
        font-size: 16px;
        color: rgb(0, 0, 0);
        font-family: Montserrat-Bold, sans-serif;
        font-weight: bold;
      }
    }
    .ant-select-arrow {
      color: rgb(80, 102, 124);
    }
  }
`;

export const StyledOptionTitle = styled.div``;
export const StyledOptionContainer = styled.div`
  display: flex;
  align-items: center;
  flex-direction: row;

  .ant-checkbox-wrapper {
    padding-right: 8px;

    .ant-checkbox-inner {
      border-radius: 0 !important;
      border-color: #000 !important;

      &:after {
        border-color: #000;
      }
    }
  }
`;
