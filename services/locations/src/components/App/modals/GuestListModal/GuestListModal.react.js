import React, { useState } from 'react';

import moment from 'moment';
import { useIntl } from 'react-intl';
import { Spin, notification, Table } from 'antd';
import { useQuery, useQueryClient } from 'react-query';

// Utils
import { sortTraces } from 'utils/sort';
import { base64ToHex } from '@lucaapp/crypto';

// Api
import { getTraces, forceCheckoutByOperator } from 'network/api';

// Components
import { RefreshIcon } from 'assets/icons';
import { DurationFilter, TODAY_OPTION, ALL_OPTION } from './DurationFilter';
import { DeviceIcon } from './DeviceIcon';
import { CheckoutButton } from './CheckoutButton';
import {
  Wrapper,
  DescriptionCount,
  DescriptionPeriod,
  Header,
  Loading,
  GuestListHeading,
  RefreshTime,
  IconStyled,
  CountValue,
  CountWrapper,
  TableWrapper,
  DurationWrapper,
} from './GuestListModal.styled';

export const GuestListModal = ({ location }) => {
  const intl = useIntl();
  const [duration, setActiveDuration] = useState(TODAY_OPTION);
  const [lastRefresh, setLastRefresh] = useState(moment());
  const queryClient = useQueryClient();
  const {
    isLoading,
    error,
    data: traces,
    refetch,
  } = useQuery(`traces/${location.uuid}/${duration}`, () =>
    getTraces(location.uuid, duration !== ALL_OPTION ? duration : null)
  );

  const renderCheckoutError = () =>
    notification.error({
      message: intl.formatMessage({
        id: 'notification.checkOut.error',
      }),
    });

  const onCheckoutSingleGuest = traceId => {
    forceCheckoutByOperator(traceId)
      .then(response => {
        if (response.status === 204) {
          queryClient.invalidateQueries(`current/${location.scannerId}`);
          queryClient.invalidateQueries(`traces/${location.uuid}/${duration}`);
          notification.success({
            message: intl.formatMessage({
              id: 'notification.checkOut.success',
            }),
            className: 'successCheckout',
          });
        } else {
          renderCheckoutError();
        }
      })
      .catch(() => renderCheckoutError());
  };

  if (isLoading)
    return (
      <Loading>
        <Spin size="large" />
      </Loading>
    );
  if (error) return null;

  const columns = [
    {
      title: intl.formatMessage({ id: 'modal.guestList.type' }),
      key: 'deviceType',
      render: function renderDeviceType(trace) {
        return <DeviceIcon deviceType={trace.deviceType} />;
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.checkinDate' }),
      key: 'checkin',
      render: function renderCheckDate(trace) {
        return (
          <div>
            <span data-cy="checkinDate">
              {moment.unix(trace.checkin).format('DD.MM.YYYY')}
            </span>
            <span>{' - '}</span>
            <span data-cy="checkinTime">
              {moment.unix(trace.checkin).format('HH:mm')}
            </span>
          </div>
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.checkoutDate' }),
      key: 'checkout',
      render: function renderCheckDate(trace) {
        return (
          <div>
            <span data-cy="checkoutDate">
              {trace.checkout
                ? moment.unix(trace.checkout).format('DD.MM.YYYY')
                : ''}
            </span>
            <span>{' - '}</span>
            <span data-cy="checkoutTime">
              {trace.checkout
                ? moment.unix(trace.checkout).format('HH:mm')
                : ''}
            </span>
            <span>
              {trace.checkout &&
                intl.formatMessage({ id: 'modal.guestList.time.clock' })}
            </span>
          </div>
        );
      },
    },
    {
      title: intl.formatMessage({ id: 'modal.guestList.guest' }),
      key: 'guest',
      render: function renderCheckin(trace) {
        return <>{base64ToHex(trace.traceId).slice(0, 7)}</>;
      },
    },
    {
      title: intl.formatMessage({
        id: 'group.view.overview.checkout',
      }),
      key: 'checkout',
      render: function renderCheckout(trace) {
        return (
          <CheckoutButton
            trace={trace}
            onCheckoutSingleGuest={onCheckoutSingleGuest}
          />
        );
      },
    },
  ];

  const refresh = () => {
    setLastRefresh(moment());
    refetch();
  };

  return (
    <Wrapper>
      <Header>
        <GuestListHeading>
          {intl.formatMessage({
            id: 'modal.guestList.title',
          })}
        </GuestListHeading>
        <RefreshTime>
          {`${intl.formatMessage({
            id: 'modal.tableAllocation.lastRefresh',
          })}: ${moment(lastRefresh).format('DD.MM.YYYY - HH:mm:ss')}`}
          {intl.formatMessage({ id: 'modal.guestList.time.clock' })}
        </RefreshTime>
        <IconStyled onClick={refresh} component={RefreshIcon} />
      </Header>

      <CountWrapper>
        <DescriptionCount>
          {intl.formatMessage({ id: 'modal.guestList.totalCount' })}
        </DescriptionCount>
        <CountValue data-cy="totalCheckinCount">{traces.length}</CountValue>
      </CountWrapper>

      <DurationWrapper>
        <DescriptionPeriod>
          {intl.formatMessage({ id: 'modal.guestList.periodSelect' })}
        </DescriptionPeriod>
        <DurationFilter active={duration} onChange={setActiveDuration} />
      </DurationWrapper>

      <TableWrapper>
        <Table
          columns={columns}
          dataSource={sortTraces(traces)}
          pagination={false}
          rowKey={record => record.traceId}
          locale={{
            emptyText: intl.formatMessage({ id: 'guestlist.table.empty' }),
          }}
        />
      </TableWrapper>
    </Wrapper>
  );
};
