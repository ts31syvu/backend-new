import styled from 'styled-components';
import Icon from '@ant-design/icons';

export const Wrapper = styled.div`
  display: flex;
  min-width: 375px;
  flex-direction: column;
  max-height: 592px;
  padding-left: 10px;

  .ant-table-wrapper {
    max-width: calc(100% - 10px);
  }
`;

export const Header = styled.div`
  display: flex;
  align-items: baseline;
  margin-bottom: 24px;
`;

export const DescriptionCount = styled.span`
  font-family: Montserrat-Medium, sans-serif;
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  width: 183.5px;
  margin-right: 16px;
`;

export const DescriptionPeriod = styled.span`
  font-family: Montserrat-Medium, sans-serif;
  color: rgba(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  width: 183.5px;
  margin-right: 6px;
`;

export const CountValue = styled.span`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;

export const Loading = styled.div`
  font-size: 24px;
  text-align: center;
  margin-top: 24px;
`;

export const GuestListHeading = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
  margin-right: 16px;
`;

export const RefreshTime = styled.div`
  font-size: 12px;
  font-weight: 500;
`;

export const IconStyled = styled(Icon)`
  margin-left: 16px;
  cursor: pointer;

  polyline {
    stroke: rgb(80, 102, 124);
  }
  path {
    fill: rgb(80, 102, 124);
  }
`;

export const CountWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 16px;
`;

export const DurationWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 40px;
`;

export const TableWrapper = styled.div`
  overflow: scroll;

  & .ant-table-tbody {
    & tr {
      & td {
        border-color: #fff;
        padding-left: 0;
        color: rgb(0, 0, 0);
        font-family: Montserrat-Medium, sans-serif;
        font-size: 14px;
        font-weight: 500;

        & div {
          justify-content: flex-start;
        }
      }

      & td:last-child {
        padding-right: 0;
        text-align: right;
      }
    }

    & .ant-table-placeholder {
      & td:last-child {
        text-align: center;
      }
    }
  }

  & .ant-table-thead {
    & tr {
      & th {
        background: unset;
        color: rgb(0, 0, 0);
        font-family: Montserrat-Bold, sans-serif;
        font-size: 14px;
        font-weight: bold;
      }
    }

    & tr:first-child {
      & th {
        padding-left: 0;
      }
      & th:last-child {
        padding-right: 0;
        text-align: right;
      }
    }
  }
`;
