import React from 'react';
import { Modal } from 'antd';
import { createGlobalStyle } from 'styled-components';

// Constants
import { zIndex } from 'constants/layout';

import { PasswordModalContent } from './PasswordModalContent';

export const PasswordModal = ({ callback, onClose }) => {
  const GlobalModalStyle = createGlobalStyle`
  .noHeader .ant-modal-title {
    display: none;
  }

  .noHeader .ant-steps {
    display: none;
  }
`;

  return (
    <>
      <GlobalModalStyle />
      <Modal
        className="noHeader"
        visible
        zIndex={zIndex.modalArea}
        onCancel={onClose}
        centered
        width="760px"
        footer={null}
      >
        <PasswordModalContent callback={callback} />
      </Modal>
    </>
  );
};
