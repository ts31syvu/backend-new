import styled from 'styled-components';
import { Input } from 'antd';

export const StyledPasswordInput = styled(Input.Password)`
  border: 1px solid #696969;
  background-color: transparent;
`;

export const Wrapper = styled.div`
  padding: 24px 32px;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 24px;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-family: Montserrat-Medium, sans-serif;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
`;
