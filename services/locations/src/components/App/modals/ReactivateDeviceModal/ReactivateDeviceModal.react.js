import React, { useEffect, useCallback, useState } from 'react';

import { notification } from 'antd';
import { useQueryClient } from 'react-query';
import { useIntl } from 'react-intl';

import { reactivateDevice } from 'network/api';

import { generatePIN, generateTransferQRCodeData } from 'utils/operatorDevice';
import { useModal } from 'components/hooks/useModal';
import { useChallenge } from 'components/hooks/useChallenge';
import { Steps } from 'components/general';
import {
  DEVICE_AUTHENTICATION_QR_CODE_STEP,
  DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP,
} from './ReactivateDeviceModal.helper';
import { DeviceAuthenticationPIN } from '../deviceSetupSteps/DeviceAuthenticationPIN';
import { DeviceAuthenticationQRCode } from '../deviceSetupSteps/DeviceAuthenticationQRCode';

export const ReactivateDeviceModal = ({ deviceId }) => {
  const intl = useIntl();
  const [, closeModal] = useModal();
  const { challenge, challengeState } = useChallenge();

  const queryClient = useQueryClient();

  const [currentStep, setCurrentStep] = useState(0);
  const [authenticationPIN] = useState(generatePIN());
  const [authenticationQRCode, setAuthenticationQRCode] = useState(null);

  const onReactivate = useCallback(async () => {
    if (!challenge) return;

    try {
      const device = await reactivateDevice(deviceId);

      const authenticationQRCodeData = await generateTransferQRCodeData(
        'AUTHENTICATION_TOKEN',
        device.refreshToken,
        authenticationPIN,
        challenge.uuid
      );

      setAuthenticationQRCode(authenticationQRCodeData);
    } catch {
      notification.error({
        message: intl.formatMessage({ id: 'notification.device.error' }),
      });

      closeModal();
    }
  }, [authenticationPIN, challenge, closeModal, deviceId, intl]);

  const steps = [
    {
      id: DEVICE_AUTHENTICATION_QR_CODE_STEP,
      content: (
        <DeviceAuthenticationQRCode
          currentStep={1}
          numberOfSteps={2}
          authenticationQRCode={authenticationQRCode}
        />
      ),
    },
    {
      id: DEVICE_AUTHENTICATION_ENCRYPTION_PIN_STEP,
      content: (
        <DeviceAuthenticationPIN
          currentStep={2}
          numberOfSteps={2}
          authenticationPIN={authenticationPIN}
        />
      ),
    },
  ];

  const onDone = useCallback(() => {
    closeModal();
    queryClient.invalidateQueries('devices');
  }, [closeModal, queryClient]);

  useEffect(() => {
    onReactivate();
  }, [onReactivate]);

  useEffect(() => {
    if (challengeState.authPinRequired) {
      setCurrentStep(1);
      return;
    }

    if (challengeState.done) {
      onDone();
    }
  }, [challengeState, onDone]);

  if (!authenticationQRCode) {
    return null;
  }

  return <Steps steps={steps} currentStep={currentStep} />;
};
