import React, { useState } from 'react';
import { Steps } from 'antd';

import { useIntl } from 'react-intl';
import { useQueryClient } from 'react-query';
import { storePublicKey, updateLastSeenPrivateKey } from 'network/api';
import { useModal } from 'components/hooks/useModal';
import { usePrivateKey } from 'utils/privateKey';

import { ConfirmationPrivateKey } from 'components/App/modals/ConfirmationPrivateKey';
import { DownloadPrivateKey } from './steps/DownloadPrivateKey';
import { VerifyPrivateKey } from './steps/VerifyPrivateKey';

/**
 * Modal for registering a new location. Generates a new keypair and
 * uploads the public key to the backend.
 *
 * @see https://www.luca-app.de/securityoverview/processes/venue_registration.html#process
 */
export const RegisterOperatorModal = ({ operator, privateKeySecret }) => {
  const intl = useIntl();
  const [publicKey, setPublicKey] = useState(null);
  const [currentStep, setCurrentStep] = useState(0);
  const [, closeModal] = useModal();
  const [, clearPrivateKey] = usePrivateKey();

  const queryClient = useQueryClient();

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const onConfirmKey = () => {
    storePublicKey({ publicKey })
      .then(async () => {
        await queryClient.invalidateQueries('me');
        await updateLastSeenPrivateKey();
        nextStep();
      })
      .catch(error => {
        clearPrivateKey();
        console.error(error);
      });
  };

  const steps = [
    {
      id: '0',
      title: 'modal.registerOperator.title',
      content: (
        <DownloadPrivateKey
          privateKeySecret={privateKeySecret}
          operator={operator}
          next={nextStep}
          setPublicKey={setPublicKey}
        />
      ),
    },
    {
      id: '1',
      title: 'modal.registerOperator.keyTest',
      content: (
        <VerifyPrivateKey
          privateKeySecret={privateKeySecret}
          publicKey={publicKey}
          back={previousStep}
          confirmKey={onConfirmKey}
        />
      ),
    },
    {
      id: '2',
      content: <ConfirmationPrivateKey onFinish={closeModal} />,
    },
  ];

  return (
    <div>
      {steps[currentStep].title && (
        <h1>{intl.formatMessage({ id: steps[currentStep].title })}</h1>
      )}
      <Steps progressDot={() => null} current={currentStep}>
        {steps.map(step => (
          <Steps.Step key={step.id} />
        ))}
      </Steps>
      {steps[currentStep].content}
    </div>
  );
};
