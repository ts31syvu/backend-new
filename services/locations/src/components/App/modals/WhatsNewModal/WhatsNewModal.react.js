import React from 'react';
import { useIntl } from 'react-intl';

import { NewsIconSVG } from 'assets/icons';
import { WhiteButton } from 'components/general';
import {
  Wrapper,
  StyledImage,
  Headline,
  Content,
} from './WhatsNewModal.styled';

export const WhatsNewModal = ({ headline, content, onAccept }) => {
  const intl = useIntl();

  return (
    <Wrapper>
      <StyledImage src={NewsIconSVG} />
      <Headline>{headline}</Headline>
      <Content>{content}</Content>
      <WhiteButton style={{ float: 'right' }} onClick={onAccept}>
        {intl.formatMessage({ id: 'whatsNew.accept' })}
      </WhiteButton>
    </Wrapper>
  );
};
