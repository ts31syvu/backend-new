import React from 'react';
import { useIntl } from 'react-intl';

import { StepHeadline, StepDescription } from '../CreateDeviceModal.styled';

import {
  PINValue,
  DeviceAuthenticationPINContent,
  DeviceAuthenticationPINWrapper,
} from './DeviceAuthenticationPIN.styled';

export function DeviceAuthenticationPIN({
  currentStep,
  numberOfSteps,
  authenticationPIN,
}) {
  const intl = useIntl();

  return (
    <DeviceAuthenticationPINWrapper>
      <StepHeadline>
        {`(${currentStep}/${numberOfSteps})
        ${intl.formatMessage({ id: 'modal.createDevice.authenticationPIN' })}`}
      </StepHeadline>
      <StepDescription>
        {intl.formatMessage({
          id: 'modal.createDevice.authenticationPIN.description',
        })}
      </StepDescription>
      <DeviceAuthenticationPINContent>
        <PINValue>{authenticationPIN}</PINValue>
      </DeviceAuthenticationPINContent>
    </DeviceAuthenticationPINWrapper>
  );
}
