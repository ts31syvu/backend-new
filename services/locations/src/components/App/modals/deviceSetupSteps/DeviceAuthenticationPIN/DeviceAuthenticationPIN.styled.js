import styled from 'styled-components';

export const DeviceAuthenticationPINWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
`;

export const DeviceAuthenticationPINContent = styled.div`
  flex: 1;
  width: 100%;
  display: flex;
  padding: 32px 0;
  align-items: center;
  justify-content: center;
`;
export const PINValue = styled.div`
  font-size: 32px;
  font-weight: 900;
  color: rgba(0, 0, 0, 0.87);
  font-family: Arial-Black, sans-serif;
`;
