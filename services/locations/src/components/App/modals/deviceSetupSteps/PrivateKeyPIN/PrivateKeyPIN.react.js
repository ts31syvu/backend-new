import React from 'react';
import { useIntl } from 'react-intl';

import { StepHeadline, StepDescription } from '../CreateDeviceModal.styled';

import {
  PINValue,
  DeviceAuthenticationPINContent,
  DeviceAuthenticationPINWrapper,
} from './PrivateKeyPIN.styled';

export function PrivateKeyPIN({ privateKeyPIN }) {
  const intl = useIntl();

  return (
    <DeviceAuthenticationPINWrapper>
      <StepHeadline>
        (4/4) {intl.formatMessage({ id: 'modal.createDevice.privateKeyPIN' })}
      </StepHeadline>
      <StepDescription>
        {intl.formatMessage({
          id: 'modal.createDevice.privateKeyPIN.description',
        })}
      </StepDescription>
      <DeviceAuthenticationPINContent>
        <PINValue>{privateKeyPIN}</PINValue>
      </DeviceAuthenticationPINContent>
    </DeviceAuthenticationPINWrapper>
  );
}
