export const getRoles = intl => [
  {
    value: 'scanner',
    name: intl.formatMessage({
      id: 'device.role.scanner',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.scanner',
    }),
  },
  {
    value: 'employee',
    name: intl.formatMessage({
      id: 'device.role.employee',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.employee',
    }),
    tooltip: intl.formatMessage({
      id: 'modal.createDevice.selectRole.tooltip',
    }),
  },
  {
    value: 'manager',
    name: intl.formatMessage({
      id: 'device.role.manager',
    }),
    description: intl.formatMessage({
      id: 'modal.createDevice.selectRole.manager',
    }),
    tooltip: intl.formatMessage({
      id: 'modal.createDevice.selectRole.tooltip',
    }),
  },
];
