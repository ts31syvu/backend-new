import React from 'react';

import { Radio } from 'antd';
import Icon from '@ant-design/icons';
import { useIntl } from 'react-intl';

import { LockIcon } from 'assets/icons';
import { StyledTooltip } from 'components/general';

import { getRoles } from './Roles.helper';

import {
  DeviceAuthenticationContent,
  RadioWrapper,
  SectionWrapper,
  RoleWrapper,
  TextWrapper,
  FormItem,
  RoleName,
  RoleDescription,
  StyledRadio,
} from './Roles.styled';

const LockIconComp = () => (
  <Icon
    component={LockIcon}
    style={{ fontSize: 16, marginLeft: 16, cursor: 'pointer' }}
  />
);

export const Roles = () => {
  const intl = useIntl();

  const roles = getRoles(intl);

  return (
    <DeviceAuthenticationContent>
      <FormItem name="role">
        <Radio.Group>
          <RadioWrapper>
            {roles.map(({ value, name, description, tooltip }) => (
              <SectionWrapper key={value}>
                <StyledRadio value={value} />
                <TextWrapper>
                  {tooltip ? (
                    <RoleWrapper>
                      <StyledTooltip title={tooltip}>
                        <RoleName>{name}</RoleName>
                        <LockIconComp />
                      </StyledTooltip>
                    </RoleWrapper>
                  ) : (
                    <RoleName>{name}</RoleName>
                  )}
                  <RoleDescription>{description}</RoleDescription>
                </TextWrapper>
              </SectionWrapper>
            ))}
          </RadioWrapper>
        </Radio.Group>
      </FormItem>
    </DeviceAuthenticationContent>
  );
};
