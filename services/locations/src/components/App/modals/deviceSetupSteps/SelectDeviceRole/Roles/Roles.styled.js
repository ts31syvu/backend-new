import { Form, Radio } from 'antd';
import styled from 'styled-components';

export const DeviceAuthenticationContent = styled.div`
  flex: 1;
  width: 100%;
  display: flex;

  .ant-form-item {
    display: flex;
    align-items: center;
    margin-bottom: 0 !important;
  }
`;

export const RadioWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const SectionWrapper = styled.div`
  display: flex;
  margin-bottom: 24px;
`;

export const RoleWrapper = styled.div`
  display: flex;
`;

export const TextWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FormItem = styled(Form.Item)`
  flex: 1;
`;

export const RoleName = styled.span`
  font-size: 14px;
  font-weight: bold;
  cursor: pointer;
  font-family: Montserrat-Bold, sans-serif;
`;
export const RoleDescription = styled.div`
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
`;

export const StyledRadio = styled(Radio)`
  display: flex;
  margin-bottom: 24px;
  border: 1px solid black;
  & .ant-radio-inner {
    border-color: white !important;
  }

  & .ant-radio-inner::after {
    background-color: black;
    margin-top: 1px;
  }
`;
