import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';
import {
  StyledDivider,
  Wrapper,
  Header,
  Description,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SelectAreaType } from './SelectAreaType';
import { SelectMedicalProtection } from './SelectMedicalProtection';
import { SelectEntryPolicyInfo } from './SelectEntryPolicyInfo';
import { SelectVentilation } from './SelectVentilation';
import { RoomSizeInput } from './RoomSizeInput';
import { ButtonRow } from '../common/ButtonRow';
import { StepProgress } from '../StepProgress';

export const AreaDetails = ({
  areaDetails,
  setAreaDetails,
  totalSteps,
  modalArea,
  back,
  next,
}) => {
  const intl = useIntl();

  const onValuesChange = (_, values) => {
    if (values.roomWidth)
      setAreaDetails({ ...areaDetails, roomWidth: values.roomWidth });
    if (values.roomDepth)
      setAreaDetails({ ...areaDetails, roomDepth: values.roomDepth });
    if (values.roomHeight)
      setAreaDetails({ ...areaDetails, roomHeight: values.roomHeight });
  };

  return (
    <Wrapper>
      <StepProgress currentStep={6} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({ id: 'modal.createGroup.areaDetails.headline' })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.areaDetails.description',
        })}
      </Description>
      <StyledDivider />
      <Form
        onValuesChange={onValuesChange}
        onFinish={next}
        initialValues={{
          areaType: areaDetails.isIndoor,
          masks: areaDetails.masks,
          entryPolicyInfo: areaDetails.entryPolicyInfo,
          ventilation: areaDetails.ventilation,
        }}
      >
        <SelectAreaType
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
        />
        <StyledDivider />
        <SelectMedicalProtection
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
        />
        <StyledDivider />
        <SelectEntryPolicyInfo
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
        />
        <StyledDivider />
        <SelectVentilation
          areaDetails={areaDetails}
          setAreaDetails={setAreaDetails}
          modalArea={modalArea}
        />
        <StyledDivider />
        <RoomSizeInput areaDetails={areaDetails} modalArea={modalArea} />
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
