import React from 'react';
import { useIntl } from 'react-intl';
import { SectionCard } from '../common/SectionCard';
import { RoomSizeInputField } from '../common/RoomSizeInputField';
import { useOptions } from '../common/useOptions';

export const RoomSizeInput = ({ areaDetails, modalArea }) => {
  const intl = useIntl();
  const { roomDimensions } = useOptions();

  return (
    <SectionCard
      title={intl.formatMessage({
        id: `modal.${modalArea}.areaDetails.roomSize.title`,
      })}
      description={intl.formatMessage({
        id: `modal.${modalArea}.areaDetails.roomSize.description`,
      })}
      isOptional
      dataCy="roomSize"
    >
      {roomDimensions.map(dimension => (
        <div key={dimension.key}>
          <RoomSizeInputField
            name={dimension.name}
            value={dimension.value}
            label={dimension.label}
            disabled={!areaDetails.isIndoor}
          />
        </div>
      ))}
    </SectionCard>
  );
};
