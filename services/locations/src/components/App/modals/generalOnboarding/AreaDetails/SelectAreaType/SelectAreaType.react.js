import React from 'react';
import { useIntl } from 'react-intl';

import { StyledSelect } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SectionCard } from '../common/SectionCard';
import { SelectInput } from '../common/SelectInput';
import { useOptions } from '../common/useOptions';

export const SelectAreaType = ({ areaDetails, setAreaDetails }) => {
  const intl = useIntl();
  const { areaTypeOptions } = useOptions();

  const handleAreaTypeChange = value => {
    setAreaDetails({
      ...areaDetails,
      isIndoor: value,
    });
  };

  return (
    <SectionCard
      title={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.areaType.title',
      })}
      description={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.areaType.description',
      })}
      dataCy="areaType"
    >
      <StyledSelect>
        <SelectInput
          dataCy="selectAreaType"
          name="areaType"
          handleSelect={handleAreaTypeChange}
          options={areaTypeOptions}
          required
        />
      </StyledSelect>
    </SectionCard>
  );
};
