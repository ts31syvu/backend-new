import React from 'react';
import { useIntl } from 'react-intl';

import { StyledSelect } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { InformationIcon, StyledTooltip } from 'components/general';
import { SectionCard } from '../common/SectionCard';
import { SelectInput } from '../common/SelectInput';
import { useOptions } from '../common/useOptions';

export const SelectEntryPolicyInfo = ({ areaDetails, setAreaDetails }) => {
  const intl = useIntl();
  const { entryPolicyInfoOptions } = useOptions();

  const handleEntryPolicyInfoChange = value => {
    setAreaDetails({ ...areaDetails, entryPolicyInfo: value });
  };

  const getTooltip = () => (
    <StyledTooltip
      title={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.tooltip',
      })}
    >
      <InformationIcon data-cy="entryPolicyInfoTooltip" />
    </StyledTooltip>
  );

  return (
    <SectionCard
      title={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.title',
      })}
      description={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.entryPolicyInfo.description',
      })}
      isOptional
      tooltip={getTooltip()}
      dataCy="entryPolicyInfo"
    >
      <StyledSelect>
        <SelectInput
          dataCy="selectEntryPolicyInfo"
          name="entryPolicyInfo"
          defaultOption={areaDetails.entryPolicyInfo}
          handleSelect={handleEntryPolicyInfoChange}
          options={entryPolicyInfoOptions}
        />
      </StyledSelect>
    </SectionCard>
  );
};
