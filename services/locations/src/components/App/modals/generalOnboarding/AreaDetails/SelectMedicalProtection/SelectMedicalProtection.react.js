import React from 'react';
import { useIntl } from 'react-intl';

import { StyledSelect } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SectionCard } from '../common/SectionCard';
import { SelectInput } from '../common/SelectInput';
import { useOptions } from '../common/useOptions';

export const SelectMedicalProtection = ({ areaDetails, setAreaDetails }) => {
  const intl = useIntl();
  const { medicalProtectionOptions } = useOptions();

  const handleMasksChange = value => {
    setAreaDetails({ ...areaDetails, masks: value });
  };

  return (
    <SectionCard
      title={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.masks.title',
      })}
      description={intl.formatMessage({
        id: 'modal.createGroup.areaDetails.masks.description',
      })}
      isOptional
      dataCy="masks"
    >
      <StyledSelect>
        <SelectInput
          dataCy="selectMasks"
          name="masks"
          defaultOption={areaDetails.masks}
          handleSelect={handleMasksChange}
          options={medicalProtectionOptions}
        />
      </StyledSelect>
    </SectionCard>
  );
};
