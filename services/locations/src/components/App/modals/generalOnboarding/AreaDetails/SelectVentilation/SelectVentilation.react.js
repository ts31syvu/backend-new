import React from 'react';
import { useIntl } from 'react-intl';

import { StyledSelect } from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { SectionCard } from '../common/SectionCard';
import { SelectInput } from '../common/SelectInput';
import { useOptions } from '../common/useOptions';

export const SelectVentilation = ({
  areaDetails,
  setAreaDetails,
  modalArea,
}) => {
  const intl = useIntl();
  const { ventilationOptions } = useOptions();

  const handleVentilationChange = value => {
    setAreaDetails({
      ...areaDetails,
      ventilation: value,
    });
  };

  return (
    <SectionCard
      title={intl.formatMessage({
        id: `modal.${modalArea}.areaDetails.selectVentilation.title`,
      })}
      description={intl.formatMessage({
        id: `modal.${modalArea}.areaDetails.selectVentilation.description`,
      })}
      isOptional
      dataCy="ventilation"
    >
      <StyledSelect>
        <SelectInput
          dataCy="selectVentilation"
          name="ventilation"
          defaultOption={areaDetails.ventilation}
          handleSelect={handleVentilationChange}
          options={ventilationOptions}
          disabled={!areaDetails.isIndoor}
        />
      </StyledSelect>
    </SectionCard>
  );
};
