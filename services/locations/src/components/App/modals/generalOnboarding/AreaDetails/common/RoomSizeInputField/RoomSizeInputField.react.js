import React from 'react';
import { StyledFormItem, StyledInputNumber } from './RoomSizeInputField.styled';

export const RoomSizeInputField = ({ name, label, disabled }) => {
  return (
    <StyledFormItem label={label} name={name}>
      <StyledInputNumber
        step={0.01}
        min={0}
        placeholder="0.0"
        disabled={disabled}
        data-cy={name}
      />
    </StyledFormItem>
  );
};
