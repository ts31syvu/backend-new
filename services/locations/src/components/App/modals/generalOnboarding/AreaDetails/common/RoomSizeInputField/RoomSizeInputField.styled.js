import styled from 'styled-components';
import { Form, InputNumber } from 'antd';

export const StyledFormItem = styled(Form.Item)`
  margin-right: 16px;
`;

export const StyledInputNumber = styled(InputNumber)`
  width: 222px;
  height: 40px;
  border-radius: 0;
  border: 0.5px solid rgb(0, 0, 0);

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
  }

  .ant-input-number-input {
    padding-top: 10px;
  }
`;
