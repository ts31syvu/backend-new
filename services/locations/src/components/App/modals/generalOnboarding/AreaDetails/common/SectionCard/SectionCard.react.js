import React from 'react';
import { OptionalSticker } from 'components/general/OptionalSticker';
import {
  Title,
  Description,
  Selection,
  HeaderWrapper,
} from './SectionCard.styled';

export const SectionCard = ({
  title,
  description,
  isOptional,
  tooltip,
  children,
  dataCy,
}) => {
  return (
    <>
      <HeaderWrapper>
        <Title>{title}</Title>
        {tooltip && <div>{tooltip}</div>}
        {isOptional && <OptionalSticker dataCy={dataCy} />}
      </HeaderWrapper>
      <Description>{description}</Description>
      <Selection>{children}</Selection>
    </>
  );
};
