import styled from 'styled-components';

export const HeaderWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
`;

export const Description = styled.div`
  color: rgb(0, 0, 0);
  font-size: 14px;
  font-family: Montserrat-Medium, sans-serif;
  font-weight: 500;
  margin: 15px 0;
`;

export const Selection = styled.div`
  display: flex;
`;
