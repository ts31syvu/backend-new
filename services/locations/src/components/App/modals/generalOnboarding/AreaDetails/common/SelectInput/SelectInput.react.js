import React from 'react';
import { Select } from 'antd';
import { useIntl } from 'react-intl';
import { getSelectRequiredRule } from 'utils/validatorRules';
import { ArrowDownBlueIcon } from 'assets/icons';
import { StyledFormItem } from './SelectInput.styled';

export const SelectInput = ({
  dataCy,
  name,
  handleSelect,
  options,
  disabled = false,
  required,
  label,
}) => {
  const intl = useIntl();

  return (
    <StyledFormItem
      name={name}
      label={label}
      rules={required ? [getSelectRequiredRule(intl, 'isIndoor')] : []}
    >
      <Select
        getPopupContainer={trigger => trigger.parentNode}
        data-cy={dataCy}
        disabled={disabled}
        onSelect={handleSelect}
        suffixIcon={<ArrowDownBlueIcon />}
      >
        {options.map(option => {
          return (
            <Select.Option
              value={option.value}
              data-cy={option.key}
              className={option.key}
              key={option.key}
            >
              {option.label}
            </Select.Option>
          );
        })}
      </Select>
    </StyledFormItem>
  );
};
