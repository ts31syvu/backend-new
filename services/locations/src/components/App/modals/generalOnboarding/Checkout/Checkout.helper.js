import { notification } from 'antd';
import { getMinutesFromTimeString } from 'utils/time';

export const timeDiffValidator = (averageCheckinTime, intl) => {
  if (
    averageCheckinTime !== null &&
    getMinutesFromTimeString(averageCheckinTime) < 15
  ) {
    return notification.error({
      message: intl.formatMessage({
        id: 'notification.updateAverageCheckinTime.TimeError',
      }),
    });
  }

  return null;
};
