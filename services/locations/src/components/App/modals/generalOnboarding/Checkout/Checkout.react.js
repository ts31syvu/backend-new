import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  PrimaryButton,
  SecondaryButton,
} from 'components/general/Buttons.styled';

import { timeDiffValidator } from './Checkout.helper';
import { RadiusAutomaticCheckout } from './RadiusAutomaticCheckout';
import { AverageTimeAutomaticCheckout } from './AverageTimeAutomaticCheckout';
import { Wrapper, Header, ButtonWrapper } from '../Onboarding.styled';
import { StyledDivider, StyledDividerTime } from './Checkout.styled';
import { StepProgress } from '../StepProgress';

export const Checkout = ({
  radiusTitleId,
  radiusInfoId,
  totalSteps,
  radius: currentRadius,
  setRadius,
  coordinates,
  googleEnabled,
  setCoordinates,
  averageCheckinTime: currentAverageCheckinTime,
  setAverageCheckinTime,
  automaticCheckout,
  setAutomaticCheckout,
  automaticCheckInTime,
  setAutomaticCheckInTime,
  back,
  next,
}) => {
  const intl = useIntl();
  const [radiusForm] = Form.useForm();
  const [checkinTimeForm] = Form.useForm();

  const onFinishRadius = values => {
    const { radius } = values;
    setRadius(automaticCheckout ? radius : 0);
  };

  const onFinishCheckinTime = values => {
    const { averageCheckinTime } = values;

    setAverageCheckinTime(automaticCheckInTime ? averageCheckinTime : null);
    timeDiffValidator(averageCheckinTime, intl);
  };

  const submitForms = () => {
    radiusForm?.submit();
    checkinTimeForm?.submit();

    next();
  };

  return (
    <Wrapper>
      <StepProgress currentStep={5} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: 'modal.createGroup.automaticCheckout.header',
        })}
      </Header>
      <StyledDivider />
      <RadiusAutomaticCheckout
        radiusTitleId={radiusTitleId}
        radiusInfoId={radiusInfoId}
        radius={currentRadius}
        setRadius={setRadius}
        coordinates={coordinates}
        setCoordinates={setCoordinates}
        automaticCheckout={automaticCheckout}
        setAutomaticCheckout={setAutomaticCheckout}
        googleEnabled={googleEnabled}
        onFinishRadius={onFinishRadius}
        radiusForm={radiusForm}
      />

      <StyledDividerTime />

      <AverageTimeAutomaticCheckout
        averageCheckinTime={currentAverageCheckinTime}
        automaticCheckInTime={automaticCheckInTime}
        setAutomaticCheckInTime={setAutomaticCheckInTime}
        checkinTimeForm={checkinTimeForm}
        onFinishCheckinTime={onFinishCheckinTime}
      />

      <ButtonWrapper multipleButtons>
        <SecondaryButton onClick={back} data-cy="previousStep">
          {intl.formatMessage({
            id: 'authentication.form.button.back',
          })}
        </SecondaryButton>
        <PrimaryButton data-cy="nextStep" onClick={submitForms}>
          {intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
        </PrimaryButton>
      </ButtonWrapper>
    </Wrapper>
  );
};
