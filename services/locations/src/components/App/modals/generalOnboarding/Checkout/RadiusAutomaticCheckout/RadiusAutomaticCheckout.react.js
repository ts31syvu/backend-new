import React from 'react';
import { useIntl } from 'react-intl';

import { useCheckoutRadiusValidator } from 'components/hooks/useValidators';

import { Switch } from 'components/general';
import {
  DEFAULT_CHECKOUT_RADIUS,
  MAX_CHECKOUT_RADIUS,
} from 'constants/checkout';

import { GooglePlacesWrapper } from '../../GooglePlacesWrapper';
import { GoogleMapSelectMarker } from '../../GoogleMapSelectMarker';
import {
  CheckoutSectionTile,
  StyledForm,
  StyledInputNumber,
  CheckoutSectionDescription,
  StyledSwitchContainer,
  StyledItem,
  GoogleMapSelectorWrapper,
} from '../Checkout.styled';

export const RadiusAutomaticCheckout = ({
  radiusTitleId,
  radiusInfoId,
  radius: currentRadius,
  setRadius,
  googleEnabled,
  coordinates,
  setCoordinates,
  automaticCheckout,
  setAutomaticCheckout,
  onFinishRadius,
  radiusForm,
}) => {
  const intl = useIntl();
  const checkoutRadiusValidator = useCheckoutRadiusValidator();

  return (
    <>
      <CheckoutSectionTile>
        {intl.formatMessage({
          id: radiusTitleId,
        })}
      </CheckoutSectionTile>
      <CheckoutSectionDescription $isError={!googleEnabled}>
        {intl.formatMessage({
          id: googleEnabled
            ? radiusInfoId
            : 'modal.createGroup.automaticCheckout.radiusInfoError',
        })}
      </CheckoutSectionDescription>
      <StyledSwitchContainer>
        <Switch
          data-cy="toggleAutomaticCheckout"
          disabled={!googleEnabled}
          checked={automaticCheckout}
          onChange={() => {
            setAutomaticCheckout(!automaticCheckout);
          }}
        />
      </StyledSwitchContainer>

      <GooglePlacesWrapper enabled>
        <StyledForm
          onFinish={onFinishRadius}
          form={radiusForm}
          initialValues={{
            radius: currentRadius || DEFAULT_CHECKOUT_RADIUS,
          }}
          $display={automaticCheckout}
        >
          <StyledItem
            colon={false}
            label={intl.formatMessage({
              id: 'createGroup.radiusLabel',
            })}
            name="radius"
            rules={checkoutRadiusValidator}
          >
            <StyledInputNumber
              min={DEFAULT_CHECKOUT_RADIUS}
              max={MAX_CHECKOUT_RADIUS}
              onChange={setRadius}
              autoFocus
            />
          </StyledItem>
          <GoogleMapSelectorWrapper>
            <GoogleMapSelectMarker
              coordinates={coordinates}
              setCoordinates={setCoordinates}
              circleRadius={currentRadius || DEFAULT_CHECKOUT_RADIUS}
            />
          </GoogleMapSelectorWrapper>
        </StyledForm>
      </GooglePlacesWrapper>
    </>
  );
};
