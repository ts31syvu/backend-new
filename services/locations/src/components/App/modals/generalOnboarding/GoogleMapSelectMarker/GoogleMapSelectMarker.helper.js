export const defaultMapOptions = {
  fullscreenControl: false,
  streetViewControl: false,
};

export const fallbackLocation = {
  // fallback is Nexenio location in Berlin
  lat: 52.5123098,
  lng: 13.3909663,
};

export const circleOptions = {
  strokeColor: `#000000`,
  fillColor: `#B2C596`,
  fillOpacity: 0.6,
  strokeWeight: 2,
  clickable: false,
  editable: false,
  zIndex: 1,
};
