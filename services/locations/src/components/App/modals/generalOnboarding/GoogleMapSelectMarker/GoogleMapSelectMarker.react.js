// To be used inside GooglePlacesWrapper
import { GoogleMap, Marker, Circle } from '@react-google-maps/api';
import React, { useEffect, useState } from 'react';
import {
  circleOptions,
  defaultMapOptions,
  fallbackLocation,
} from './GoogleMapSelectMarker.helper';

export const GoogleMapSelectMarker = ({
  coordinates,
  setCoordinates,
  circleRadius,
}) => {
  const [mapChanged, setMapChanged] = useState(false);
  const [mapMarker, setMapMarker] = useState(null);
  const [zoom, setZoom] = useState(18);

  useEffect(() => {
    if (circleRadius > 100 && circleRadius <= 250) setZoom(17);
    if (circleRadius > 250 && circleRadius <= 1000) setZoom(15);
    if (circleRadius > 1000 && circleRadius <= 2000) setZoom(14);
    if (circleRadius > 2000 && circleRadius <= 3000) setZoom(13);
    if (circleRadius > 3000 && circleRadius <= 5000) setZoom(12);
  }, [circleRadius]);

  useEffect(() => {
    if (coordinates && coordinates.lat && coordinates.lng) {
      setMapMarker(
        new window.google.maps.LatLng(
          parseFloat(coordinates.lat),
          parseFloat(coordinates.lng)
        )
      );
    }
  }, [coordinates]);

  const mapContainerStyle = {
    height: '100%',
  };

  const mapCenter = mapChanged ? null : mapMarker || fallbackLocation;

  const mapOptions = {
    ...defaultMapOptions,
    center: mapCenter,
  };

  return (
    <GoogleMap
      mapContainerStyle={mapContainerStyle}
      options={mapOptions}
      zoom={zoom}
      onClick={event => {
        setMapChanged(true);
        setCoordinates({
          lat: event.latLng.lat(),
          lng: event.latLng.lng(),
        });
      }}
    >
      {mapMarker && <Marker position={mapMarker} />}
      {circleRadius && (
        <Circle
          center={mapMarker}
          radius={circleRadius}
          options={circleOptions}
        />
      )}
    </GoogleMap>
  );
};
