import styled from 'styled-components';
import { WhiteButton } from 'components/general';

export const GooglePlacesPrimaryButton = styled(WhiteButton)`
  margin-right: 24px;
`;
