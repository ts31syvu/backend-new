import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import { useQuery } from 'react-query';

import { getMe } from 'network/api';

// hooks
import { usePhoneValidator } from 'components/hooks/useValidators';
import { getFormattedPhoneNumber } from 'utils/parsePhoneNumber';

import {
  Wrapper,
  Header,
  Description,
  StyledInput,
} from 'components/App/modals/generalOnboarding/Onboarding.styled';
import { StepProgress } from '../StepProgress';
import { ButtonRow } from '../common/ButtonRow';

export const PhoneInput = ({
  phone: currentPhone,
  setPhone,
  back,
  next,
  titleId,
  descriptionId,
  totalSteps,
}) => {
  const intl = useIntl();
  const phoneValidator = usePhoneValidator('phone');

  const { isLoading, error, data: operator } = useQuery('me', () => getMe(), {
    retry: false,
  });

  const onFinish = ({ phone }) => {
    setPhone(getFormattedPhoneNumber(phone));
    next();
  };

  if (error || isLoading) return null;

  return (
    <Wrapper>
      <StepProgress currentStep={3} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: titleId,
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: descriptionId,
        })}
      </Description>
      <Form
        onFinish={onFinish}
        initialValues={
          currentPhone ? { phone: currentPhone } : { phone: operator.phone }
        }
      >
        <Form.Item
          data-cy="phone"
          colon={false}
          label={intl.formatMessage({
            id: 'createGroup.phone',
          })}
          name="phone"
          rules={phoneValidator}
        >
          <StyledInput autoFocus />
        </Form.Item>
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
