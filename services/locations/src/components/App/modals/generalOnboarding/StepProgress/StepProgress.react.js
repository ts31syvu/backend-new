import React from 'react';
import { StepProgressWrapper } from 'components/App/modals/generalOnboarding/Onboarding.styled';

export const StepProgress = ({ currentStep, totalSteps }) => {
  return (
    <StepProgressWrapper data-cy="stepCount">{`${currentStep} / ${totalSteps}`}</StepProgressWrapper>
  );
};
