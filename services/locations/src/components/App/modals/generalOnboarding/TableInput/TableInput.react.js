import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Form, notification } from 'antd';

import { useTableNumberValidator } from 'components/hooks/useValidators';

import { Switch } from 'components/general';

import { MIN_TABLE_NUMBER, MAX_TABLE_NUMBER } from 'constants/tableNumber';
import { useQueryClient } from 'react-query';
import { createLocation as createLocationRequest } from 'network/api';
import { QUERY_KEYS } from 'components/hooks/queries';
import {
  Wrapper,
  Header,
  Description,
  SwitchWrapper,
} from '../Onboarding.styled';
import { StyledInputNumber } from './TableInput.styled';
import { StepProgress } from '../StepProgress';
import { ButtonRow } from '../common/ButtonRow';

export const TableInput = ({
  tableCount: currentTableCount,
  setTableCount,
  totalSteps,
  back,
  next,
  setLocation,
  setLocationId,
  locationPayload,
  onClose,
  isLocationModal,
}) => {
  const intl = useIntl();
  const queryClient = useQueryClient();

  const [hasTables, setHasTables] = useState(false);
  const tableNumberValidator = useTableNumberValidator();

  const onFinishNext = ({ tableCount }) => {
    setTableCount(tableCount);
    next();
  };

  const handleServerError = () => {
    notification.error({
      message: intl.formatMessage({
        id: 'notification.createLocation.error',
      }),
    });
    onClose();
  };

  const handleResponse = response => {
    if (!response) {
      handleServerError();
      return;
    }

    notification.success({
      message: intl.formatMessage({
        id: 'notification.createLocation.success',
      }),
    });

    setLocationId(response.uuid);
    setLocation(response);

    queryClient.invalidateQueries(QUERY_KEYS.GROUPS);
    next();
  };

  const onFinishCreateLocation = ({ tableCount }) => {
    const createLocationPayload = { ...locationPayload, tableCount };
    createLocationRequest(createLocationPayload)
      .then(response => {
        handleResponse(response);
      })
      .catch(() => {
        handleServerError();
      });
  };

  return (
    <Wrapper>
      <StepProgress currentStep={7} totalSteps={totalSteps} />
      <Header>
        {intl.formatMessage({
          id: 'modal.createGroup.tableInput.title',
        })}
      </Header>
      <Description>
        {intl.formatMessage({
          id: 'modal.createGroup.tableInput.description',
        })}
      </Description>
      <SwitchWrapper>
        <Switch
          checked={hasTables}
          onChange={() => setHasTables(!hasTables)}
          data-cy="toggleTableInput"
        />
      </SwitchWrapper>
      <Form
        onFinish={isLocationModal ? onFinishCreateLocation : onFinishNext}
        initialValues={
          currentTableCount ? { tableCount: currentTableCount } : {}
        }
        style={{ marginTop: 40 }}
      >
        {hasTables && (
          <Form.Item
            colon={false}
            label={intl.formatMessage({
              id: 'createGroup.tableAmount',
            })}
            name="tableCount"
            rules={tableNumberValidator}
          >
            <StyledInputNumber
              style={{ width: '100%' }}
              min={MIN_TABLE_NUMBER}
              max={MAX_TABLE_NUMBER}
              placeholder={intl.formatMessage({
                id: 'createGroup.tableCount.placeholder',
              })}
            />
          </Form.Item>
        )}
        <ButtonRow
          onBack={back}
          confirmButtonLabel={intl.formatMessage({
            id: isLocationModal
              ? 'createLocation.button.done'
              : 'authentication.form.button.next',
          })}
          htmlType="submit"
        />
      </Form>
    </Wrapper>
  );
};
