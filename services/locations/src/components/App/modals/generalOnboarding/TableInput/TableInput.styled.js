import styled from 'styled-components';
import { InputNumber } from 'antd';

export const StyledInputNumber = styled(InputNumber)`
  border-radius: 0;
  border: 0.5px solid rgb(0, 0, 0);
  height: 40px;

  :focus,
  :hover,
  :active {
    border-radius: 0;
    border: 0.5px solid rgb(0, 0, 0);
    height: 40px;
  }

  .ant-input-number-input {
    padding-top: 10px;
  }
`;
