import React from 'react';
import { PrimaryButton, SecondaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { ButtonWrapper } from '../../Onboarding.styled';

export const ButtonRow = ({
  onBack,
  onConfirm,
  htmlType,
  confirmButtonLabel,
}) => {
  const intl = useIntl();

  return (
    <ButtonWrapper multipleButtons>
      <SecondaryButton onClick={onBack} data-cy="previousStep">
        {intl.formatMessage({
          id: 'authentication.form.button.back',
        })}
      </SecondaryButton>
      <PrimaryButton data-cy="nextStep" onClick={onConfirm} htmlType={htmlType}>
        {confirmButtonLabel}
      </PrimaryButton>
    </ButtonWrapper>
  );
};
