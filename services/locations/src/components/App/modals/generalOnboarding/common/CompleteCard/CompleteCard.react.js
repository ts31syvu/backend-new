import React from 'react';
import { Description, Header } from '../../Onboarding.styled';

export const CompleteCard = ({ title, description, children }) => {
  return (
    <>
      <Header>{title}</Header>
      <Description>{description}</Description>
      <div>{children}</div>
    </>
  );
};
