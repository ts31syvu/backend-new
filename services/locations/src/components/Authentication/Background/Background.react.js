import React from 'react';
import { useIntl } from 'react-intl';

// Assets
import { LucaLogoWhiteIconSVG } from 'assets/icons';
import { Login0Image, Login1Image, Login2Image } from 'assets/images';

import {
  Left,
  HeaderWrapper,
  Logo,
  SubTitle,
  Right0,
  Right1,
  Right2,
} from './Background.styled';

export const Background = ({ isRegistration }) => {
  const intl = useIntl();
  return (
    <>
      <Left style={isRegistration ? { right: 0 } : { right: '50%' }}>
        <HeaderWrapper>
          <Logo src={LucaLogoWhiteIconSVG} />
          <SubTitle>
            {intl.formatMessage({
              id: 'header.subtitle',
            })}
          </SubTitle>
        </HeaderWrapper>
      </Left>
      {!isRegistration && (
        <>
          <Right0 src={Login0Image} />
          <Right1 src={Login1Image} />
          <Right2 src={Login2Image} />
        </>
      )}
    </>
  );
};
