import React from 'react';
import { useIntl } from 'react-intl';

// Constants
import { FORGOT_PASSWORD_ROUTE } from 'constants/routes';

import {
  ForgotPasswordLinkWrapper,
  StyledLink,
} from './ForgotPasswordLink.styled';

export const ForgotPasswordLink = () => {
  const intl = useIntl();

  return (
    <ForgotPasswordLinkWrapper>
      <StyledLink
        to={{ pathname: FORGOT_PASSWORD_ROUTE }}
        data-cy="forgotPasswordLink"
      >
        {intl.formatMessage({ id: 'login.error.forgotPassword' })}
      </StyledLink>
    </ForgotPasswordLinkWrapper>
  );
};
