import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useLocation } from 'react-router';
import { Form, notification } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

import { useEmailValidator } from 'components/hooks/useValidators';

// Api
import { login } from 'network/api';

// Constants
import { BASE_GROUP_ROUTE } from 'constants/routes';

// Utils
import { usePrivateKey } from 'utils/privateKey';
import { clearHasSeenPrivateKeyModal } from 'utils/storage';
import queryString from 'query-string';

import { clearJwt } from 'utils/jwtStorage';
import { ForgotPasswordLink } from './ForgotPasswordLink';
import { LoginError } from './LoginError';
import { LoginActions } from './LoginActions';

import {
  StyledFormItem,
  StyledInput,
  StyledInputPassword,
} from './LoginCard.styled';
import { AuthenticationCard, CardTitle } from '../../Authentication.styled';

export const LoginCard = () => {
  const intl = useIntl();
  const [, clearPrivateKey] = usePrivateKey(null);
  const [error, setError] = useState(null);
  const location = useLocation();

  const emailValidator = useEmailValidator();

  const handleLoginErrors = response => {
    if (response.status === 429) {
      setError({
        message: 'registration.server.error.tooManyRequests.title',
      });
      // Too many requests
      notification.error({
        message: intl.formatMessage({
          id: 'registration.server.error.tooManyRequests.title',
        }),
        description: intl.formatMessage({
          id: 'registration.server.error.tooManyRequests.desc',
        }),
      });
      return;
    }
    if (response.status === 423) {
      setError({
        message: 'registration.server.error.notActivated.title',
      });
      // Not activated
      notification.error({
        message: intl.formatMessage({
          id: 'registration.server.error.notActivated.title',
        }),
        description: intl.formatMessage({
          id: 'registration.server.error.notActivated.desc',
        }),
      });
      return;
    }
    setError({
      message: 'login.error',
    });
    notification.error({
      message: intl.formatMessage({
        id: 'notification.login.error',
      }),
    });
  };

  const onFinish = values => {
    const { email, password } = values;
    login({ username: email, password })
      .then(response => {
        if (response.status !== 204) {
          handleLoginErrors(response);
          return;
        }
        setError(null);
        clearPrivateKey(null);
        clearJwt();
        clearHasSeenPrivateKeyModal();

        const { redirectTo } = queryString.parse(location.search);

        const newLocation = redirectTo ?? BASE_GROUP_ROUTE;

        window.location = newLocation;
      })
      .catch(() => {
        notification.error({
          message: intl.formatMessage({
            id: 'registration.server.error.msg',
          }),
          description: intl.formatMessage({
            id: 'registration.server.error.desc',
          }),
        });
      });
  };

  return (
    <AuthenticationCard>
      <CardTitle data-cy="loginPage">
        {intl.formatMessage({
          id: 'loginCard.title',
        })}
      </CardTitle>
      <Form onFinish={onFinish}>
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.email',
          })}
          name="email"
          rules={emailValidator}
        >
          <StyledInput autoFocus autoComplete="username" />
        </StyledFormItem>
        <StyledFormItem
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.password',
          })}
          name="password"
        >
          <StyledInputPassword
            autoComplete="current-password"
            iconRender={visible =>
              visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
            }
          />
        </StyledFormItem>
        <LoginError error={error} />
        <ForgotPasswordLink />
        <LoginActions />
      </Form>
    </AuthenticationCard>
  );
};
