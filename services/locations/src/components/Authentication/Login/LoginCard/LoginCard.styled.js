import styled from 'styled-components';
import { Form, Input } from 'antd';

const inputStyles = {
  border: '1px solid #696969 !important',
  backgroundColor: 'transparent !important',
};

export const StyledFormItem = styled(Form.Item)`
  & label {
    font-weight: 500;
  }
`;

export const StyledInput = styled(Input)`
  ${inputStyles}
`;

export const StyledInputPassword = styled(Input.Password)`
  ${inputStyles}
`;
