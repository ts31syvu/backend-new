import styled from 'styled-components';

export const ErrorMessage = styled.div`
  color: #ff4d4f;
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 24px;
`;
