import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';
import { useHistory } from 'react-router';
import { Steps, notification } from 'antd';

import { registerOperator } from 'network/api';

import { IS_MOBILE } from 'constants/environment';
import { LOGIN_ROUTE } from 'constants/routes';

import { Background } from '../Background';
import { Footer } from '../Footer';
import { EmailStep } from './steps/EmailStep';
import { ContactInputStep } from './steps/ContactInputStep';
import { SetPasswordStep } from './steps/SetPasswordStep';
import { LegalTermsStep } from './steps/LegalTermsStep';
import { FinishRegisterStep } from './steps/FinishRegisterStep';

import {
  Wrapper,
  AuthenticationWrapper,
  AuthenticationCard,
} from '../Authentication.styled';
import { BusinessInfoStep } from './steps/BusinessInfoStep';

const STEP_LENGTH = 6;

export const Register = () => {
  const intl = useIntl();
  const history = useHistory();
  const [currentStep, setCurrentStep] = useState(0);
  const [email, setEmail] = useState(null);
  const [contacts, setContacts] = useState(null);
  const [password, setPassword] = useState(null);
  const [businessEntityName, setBusinessEntityName] = useState('');
  const [businessEntityAddress, setBusinessEntityAddress] = useState({
    businessEntityStreetName: '',
    businessEntityStreetNumber: '',
    businessEntityZipCode: '',
    businessEntityCity: '',
  });

  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const resetAuthentication = () => {
    setEmail(null);
    setContacts(null);
    setPassword(null);
    setCurrentStep(0);
    history.push(LOGIN_ROUTE);
  };

  const getNavigation = () => {
    return `${currentStep + 1}/${STEP_LENGTH}`;
  };

  const register = async () => {
    const { firstName, lastName, phone } = contacts;
    const {
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
      businessEntityCity,
    } = businessEntityAddress;

    const registrationData = {
      email,
      firstName,
      lastName,
      phone,
      businessEntityName,
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
      businessEntityCity,
      password,
      agreement: true,
      avvAccepted: true,
      lastVersionSeen: process.env.REACT_APP_VERSION,
    };

    const response = await registerOperator(registrationData);

    if (response.ok) {
      return nextStep();
    }

    if (response.status === 403) {
      return notification.error({
        message: intl.formatMessage({
          id: 'register.error.ipAddress',
        }),
      });
    }

    if (response.status === 400) {
      return notification.error({
        message: intl.formatMessage({
          id: 'register.error.invalidSchema',
        }),
      });
    }

    return notification.error({
      message: intl.formatMessage({
        id: 'registration.server.error.msg',
      }),
    });
  };

  const steps = [
    {
      id: '0',
      content: (
        <EmailStep
          email={email}
          setEmail={setEmail}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '1',
      content: (
        <ContactInputStep
          contacts={contacts}
          setContacts={setContacts}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '2',
      content: (
        <BusinessInfoStep
          businessEntityName={businessEntityName}
          setBusinessEntityName={setBusinessEntityName}
          businessEntityAddress={businessEntityAddress}
          setBusinessEntityAddress={setBusinessEntityAddress}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '3',
      content: (
        <SetPasswordStep
          setPassword={setPassword}
          previousSetPassword={password}
          name={{
            firstName: contacts?.firstName,
            lastName: contacts?.lastName,
          }}
          email={email}
          back={previousStep}
          next={nextStep}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '4',
      content: (
        <LegalTermsStep
          back={previousStep}
          next={register}
          navigation={getNavigation()}
        />
      ),
    },
    {
      id: '5',
      content: (
        <FinishRegisterStep
          next={resetAuthentication}
          navigation={getNavigation()}
        />
      ),
    },
  ];

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'locations.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'locations.site.meta' })}
        />
      </Helmet>
      <Wrapper id={IS_MOBILE ? 'isMobile' : ''}>
        <Background isRegistration />
        <AuthenticationWrapper id="noSteps">
          <AuthenticationCard>
            <Steps
              progressDot={() => null}
              current={currentStep}
              style={{
                marginTop: 16,
                marginBottom: 48,
              }}
            >
              {steps.map(step => (
                <Steps.Step key={step.id} />
              ))}
            </Steps>
            {steps[currentStep].content}
          </AuthenticationCard>
          <Footer />
        </AuthenticationWrapper>
      </Wrapper>
    </>
  );
};
