import React from 'react';
import { Form } from 'antd';
import { WhiteButton, SecondaryButton } from 'components/general';
import { useIntl } from 'react-intl';
import { useNameValidator } from 'components/hooks/useValidators';
import {
  ButtonWrapper,
  CardSubTitle,
  CardTitle,
  Step,
  StyledInput,
} from 'components/Authentication/Authentication.styled';
import { BusinessAddress } from './BusinessAddress';

export const BusinessInfoStep = ({
  setBusinessEntityName,
  setBusinessEntityAddress,
  back,
  next,
  navigation,
}) => {
  const intl = useIntl();
  const businessNameValidator = useNameValidator('businessEntityName');

  const onFinish = ({
    businessEntityName,
    businessEntityStreetName,
    businessEntityStreetNumber,
    businessEntityZipCode,
    businessEntityCity,
  }) => {
    setBusinessEntityName(businessEntityName);
    setBusinessEntityAddress({
      businessEntityStreetName,
      businessEntityStreetNumber,
      businessEntityZipCode,
      businessEntityCity,
    });
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle>
        {intl.formatMessage({
          id: 'authentication.businessInfo.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.businessInfo.subTitle',
        })}
      </CardSubTitle>
      <Form onFinish={onFinish}>
        <Form.Item
          data-cy="businessEntityName"
          colon={false}
          name="businessEntityName"
          label={intl.formatMessage({
            id: 'register.businessEntityName',
          })}
          rules={businessNameValidator}
        >
          <StyledInput autoFocus />
        </Form.Item>

        <BusinessAddress />

        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </SecondaryButton>
          <WhiteButton htmlType="submit" data-cy="confirmBusinessInfoButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </WhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
