import React from 'react';
import { useIntl } from 'react-intl';
import { Form } from 'antd';

import {
  usePersonNameValidator,
  usePhoneValidator,
} from 'components/hooks/useValidators';

import { WhiteButton, SecondaryButton } from 'components/general';

import {
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  Step,
  StyledInput,
} from 'components/Authentication/Authentication.styled';

export const ContactInputStep = ({
  contacts,
  setContacts,
  next,
  back,
  navigation,
}) => {
  const intl = useIntl();
  const firstNameValidator = usePersonNameValidator('firstName');
  const lastNameValidator = usePersonNameValidator('lastName');
  const phoneValidator = usePhoneValidator('phone');

  const onFinish = ({ firstName, lastName, phone }) => {
    setContacts({ firstName, lastName, phone });
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle>
        {intl.formatMessage({
          id: 'authentication.contactInput.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.contactInput.subTitle',
        })}
      </CardSubTitle>
      <Form
        onFinish={onFinish}
        initialValues={
          contacts
            ? {
                firstName: contacts.firstName,
                lastName: contacts.lastName,
                phone: contacts.phone,
              }
            : {}
        }
      >
        <Form.Item
          data-cy="registerFirstName"
          colon={false}
          name="firstName"
          label={intl.formatMessage({
            id: 'generic.firstName',
          })}
          rules={firstNameValidator}
        >
          <StyledInput autoFocus />
        </Form.Item>
        <Form.Item
          data-cy="registerLastName"
          colon={false}
          name="lastName"
          label={intl.formatMessage({
            id: 'generic.lastName',
          })}
          rules={lastNameValidator}
        >
          <StyledInput />
        </Form.Item>
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'settings.location.phone',
          })}
          name="phone"
          rules={phoneValidator}
        >
          <StyledInput />
        </Form.Item>
        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </SecondaryButton>
          <WhiteButton htmlType="submit" data-cy="confirmContactsButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </WhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
