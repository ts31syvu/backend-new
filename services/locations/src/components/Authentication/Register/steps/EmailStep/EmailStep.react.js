import React from 'react';
import { useIntl } from 'react-intl';
import { Form, Input } from 'antd';
import { WhiteButton } from 'components/general';

import { useEmailValidator } from 'components/hooks/useValidators';

import {
  CardTitle,
  ButtonWrapper,
  Step,
} from 'components/Authentication/Authentication.styled';

export const EmailStep = ({
  email: currentEmail,
  setEmail,
  next,
  navigation,
}) => {
  const intl = useIntl();
  const emailValidator = useEmailValidator();

  const onFinish = values => {
    const { email } = values;
    setEmail(email);
    next();
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle data-cy="loginPage">
        {intl.formatMessage({
          id: 'authentication.register.title',
        })}
      </CardTitle>
      <Form
        onFinish={onFinish}
        initialValues={currentEmail ? { email: currentEmail } : {}}
      >
        <Form.Item
          colon={false}
          label={intl.formatMessage({
            id: 'registration.form.email',
          })}
          name="email"
          rules={emailValidator}
        >
          <Input
            autoFocus
            autoComplete="username"
            style={{
              border: '1px solid #696969',
              backgroundColor: 'transparent',
            }}
          />
        </Form.Item>
        <ButtonWrapper>
          <WhiteButton htmlType="submit" data-cy="createNewAccountButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </WhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
