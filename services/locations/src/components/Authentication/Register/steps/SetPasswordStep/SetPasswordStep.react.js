import React, { useState, useRef, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { Form, Input } from 'antd';
import { zxcvbn } from 'config.zxcvbn';

import { WhiteButton, SecondaryButton } from 'components/general';
import { StrengthMeter } from 'components/general/StrengthMeter';

import {
  CardTitle,
  CardSubTitle,
  ButtonWrapper,
  Step,
} from 'components/Authentication/Authentication.styled';
import { MIN_STRENGTH_SCORE } from 'constants/general';
import { inputStyle, FormItem } from './SetPasswordStep.styled';

export const SetPasswordStep = ({
  setPassword,
  previousSetPassword,
  name,
  email,
  next,
  back,
  navigation,
}) => {
  const { firstName, lastName } = name;
  const intl = useIntl();
  const formReference = useRef(null);
  const [strengthScore, setStrengthScore] = useState(0);
  const [warningsList, setWarningsList] = useState([]);
  const [displayStrengthMeter, setDisplayStrengthMeter] = useState(
    !!previousSetPassword || false
  );

  const onChange = () => {
    const { password } = formReference.current.getFieldsValue();

    setDisplayStrengthMeter(!!password);

    const passwordStrength = zxcvbn(password, [firstName, lastName, email]);

    setStrengthScore(passwordStrength.score);
    setWarningsList([
      passwordStrength?.feedback?.warning,
      ...passwordStrength?.feedback?.suggestions,
    ]);
  };

  useEffect(() => {
    if (previousSetPassword) {
      onChange();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [previousSetPassword]);

  const onFinish = values => {
    setPassword(values.password);

    if (strengthScore >= MIN_STRENGTH_SCORE) {
      next();
    }
  };

  return (
    <>
      <Step>{navigation}</Step>
      <CardTitle data-cy="setPassword">
        {intl.formatMessage({
          id: 'authentication.setPassword.title',
        })}
      </CardTitle>
      <CardSubTitle>
        {intl.formatMessage({
          id: 'authentication.setPassword.subTitle',
        })}
      </CardSubTitle>
      <Form
        onFinish={onFinish}
        initialValues={{ password: previousSetPassword || '' }}
        onValuesChange={onChange}
        ref={formReference}
      >
        <FormItem
          colon={false}
          name="password"
          label={intl.formatMessage({
            id: 'registration.form.password',
          })}
          hasFeedback
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'error.password',
              }),
            },
            () => ({
              validator() {
                if (
                  displayStrengthMeter &&
                  strengthScore < MIN_STRENGTH_SCORE
                ) {
                  return strengthScore !== MIN_STRENGTH_SCORE - 1
                    ? Promise.reject(warningsList)
                    : Promise.reject(
                        intl.formatMessage({
                          id: 'error.password.minStrengthRequired',
                        })
                      );
                }

                return Promise.resolve();
              },
            }),
          ]}
        >
          <Input.Password
            style={inputStyle}
            autoFocus
            data-cy="setPasswordField"
          />
        </FormItem>
        {displayStrengthMeter && (
          <StrengthMeter strengthScore={strengthScore} />
        )}
        <Form.Item
          colon={false}
          name="passwordConfirm"
          label={intl.formatMessage({
            id: 'registration.form.passwordConfirm',
          })}
          hasFeedback
          dependencies={['password']}
          rules={[
            {
              required: true,
              message: intl.formatMessage({
                id: 'error.passwordConfirm',
              }),
            },
            ({ getFieldValue }) => ({
              validator(rule, value) {
                if (!value || getFieldValue('password') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  intl.formatMessage({
                    id: 'error.passwordConfirm',
                  })
                );
              },
            }),
          ]}
        >
          <Input.Password
            style={inputStyle}
            data-cy="setPasswordConfirmField"
          />
        </Form.Item>

        <ButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({
              id: 'authentication.form.button.back',
            })}
          </SecondaryButton>
          <WhiteButton htmlType="submit" data-cy="setPasswordSubmitButton">
            {intl.formatMessage({
              id: 'authentication.form.button.next',
            })}
          </WhiteButton>
        </ButtonWrapper>
      </Form>
    </>
  );
};
