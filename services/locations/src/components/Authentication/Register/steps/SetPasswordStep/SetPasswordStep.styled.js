import styled from 'styled-components';
import { Form } from 'antd';

export const inputStyle = {
  border: '1px solid #696969',
  backgroundColor: 'transparent',
};

export const FormItem = styled(Form.Item)`
  margin-bottom: 10px;
`;
