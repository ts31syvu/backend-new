import React, { useState, useEffect } from 'react';
import { useIntl } from 'react-intl';
import { Spin } from 'antd';
import { useQuery } from 'react-query';

import { getPrivateKeySecret } from 'network/api';
import { usePrivateKey } from 'utils/privateKey';
import { QUERY_KEYS } from 'components/hooks/queries';

import { uploadMessages, statusProgress } from '../PrivateKeyLoader.helper';

import { ButtonRow } from './ButtonRow';
import { FileUpload } from './FileUpload';

import {
  InfoBlock,
  RequestContent,
  Title,
  ErrorContent,
} from './KeyInput.styled';

export const KeyInput = ({
  infoTextId,
  footerItem,
  onSuccess,
  onError,
  publicKey,
  remainingDaysToUploadKey,
  setIsResetKey,
}) => {
  const intl = useIntl();
  const [progressPercent, setProgressPercent] = useState(0);
  const [uploadStatus, setUploadStatus] = useState(statusProgress.initial);
  const [uploadMessageId, setUploadMessageId] = useState(
    uploadMessages.initial
  );

  const { data: privateKeySecret, isLoading } = useQuery(
    QUERY_KEYS.PRIVATE_KEY_SECRET,
    getPrivateKeySecret,
    {
      retry: false,
      onError: error => {
        onError(error);
      },
    }
  );

  const [existingPrivateKey, setPrivateKey] = usePrivateKey(privateKeySecret);

  useEffect(() => {
    if (existingPrivateKey) {
      onSuccess(existingPrivateKey);
    }
  }, [existingPrivateKey, onSuccess]);

  if (isLoading) {
    return <Spin />;
  }

  return (
    <>
      <RequestContent>
        <Title>
          {intl.formatMessage({
            id: 'privateKey.modal.title',
          })}
        </Title>
        <InfoBlock>
          {intl.formatMessage({ id: infoTextId }, { br: <br /> })}
        </InfoBlock>
        {!!remainingDaysToUploadKey && (
          <InfoBlock>
            {intl.formatMessage(
              { id: 'keyUpload.remainingDays' },
              {
                remainingDays: <b>{remainingDaysToUploadKey}</b>,
                // eslint-disable-next-line react/display-name
                b: (...chunks) => <b>{chunks}</b>,
              }
            )}
          </InfoBlock>
        )}
        <FileUpload
          onSuccess={onSuccess}
          publicKey={publicKey}
          setProgressPercent={setProgressPercent}
          setUploadStatus={setUploadStatus}
          setUploadMessageId={setUploadMessageId}
          setPrivateKey={setPrivateKey}
          privateKeySecret={privateKeySecret}
          progressPercent={progressPercent}
          uploadStatus={uploadStatus}
          uploadMessageId={uploadMessageId}
        />
        {uploadStatus === statusProgress.exception && (
          <ErrorContent data-cy="keyUploadError">
            {intl.formatMessage({
              id: 'keyUpload.modal.generalError',
            })}
          </ErrorContent>
        )}
      </RequestContent>
      <ButtonRow
        footerItem={footerItem}
        setProgressPercent={setProgressPercent}
        setUploadStatus={setUploadStatus}
        uploadStatus={uploadStatus}
        setUploadMessageId={setUploadMessageId}
        setIsResetKey={setIsResetKey}
      />
    </>
  );
};
