import React from 'react';
import { Checkbox, Form } from 'antd';
import { useIntl } from 'react-intl';
import { getCheckboxRule } from 'utils/validatorRules';

export const ConfirmCheckbox = () => {
  const intl = useIntl();
  const checkboxErrorMessage = intl.formatMessage({
    id: 'error.readConfirmation',
  });

  return (
    <Form.Item
      name="readConfirmation"
      valuePropName="checked"
      rules={[getCheckboxRule(checkboxErrorMessage)]}
    >
      <Checkbox data-cy="readConfirmationCheckbox">
        {intl.formatMessage({ id: 'resetKey.readConfirmation' })}
      </Checkbox>
    </Form.Item>
  );
};
