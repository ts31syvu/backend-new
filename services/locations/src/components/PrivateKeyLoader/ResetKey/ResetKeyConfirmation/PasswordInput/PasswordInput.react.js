import React from 'react';
import { Form } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import { getRequiredRule } from 'utils/validatorRules';
import { StyledPasswordInput } from './PasswordInput.styled';

export const PasswordInput = () => {
  const intl = useIntl();

  return (
    <Form.Item
      colon={false}
      label={intl.formatMessage({
        id: 'registration.form.password',
      })}
      name="password"
      rules={[getRequiredRule(intl, 'password')]}
    >
      <StyledPasswordInput
        autoComplete="current-password"
        iconRender={visible =>
          visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />
        }
      />
    </Form.Item>
  );
};
