import styled from 'styled-components';
import { Input } from 'antd';

export const StyledPasswordInput = styled(Input.Password)`
  border: 1px solid #696969;
  background-color: transparent;
`;
