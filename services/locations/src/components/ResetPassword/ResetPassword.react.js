import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';
import { useQuery } from 'react-query';
import { zxcvbn } from 'config.zxcvbn';
import { Form, Input, notification, Alert } from 'antd';
import { useDispatch } from 'react-redux';
import { replace } from 'connected-react-router';
import { useParams } from 'react-router-dom';

import { WhiteButton } from 'components/general';
import { LucaLogoWhiteIconSVG } from 'assets/icons';
import { resetPassword, getPasswordResetRequest } from 'network/api';
import { LOGIN_ROUTE } from 'constants/routes';

import { MIN_STRENGTH_SCORE } from 'constants/general';
import {
  ForgotPasswordWrapper,
  ForgotPasswordCard,
  ButtonWrapper,
  Wrapper,
  Logo,
  SubTitle,
  HeaderWrapper,
  Title,
} from '../ForgotPassword/ForgotPassword.styled';
import { StrengthMeter } from '../general/StrengthMeter';

const inputStyle = {
  border: '1px solid #696969',
  backgroundColor: 'transparent',
};

export const ResetPassword = () => {
  const intl = useIntl();
  const dispatch = useDispatch();
  const { requestId } = useParams();
  const [strengthScore, setStrengthScore] = useState(0);
  const [warningsList, setWarningsList] = useState([]);
  const [displayStrengthMeter, setDisplayStrengthMeter] = useState(false);
  const {
    isLoading,
    error: requestError,
  } = useQuery(
    `passwordRequest/${requestId}`,
    () => getPasswordResetRequest(requestId),
    { cacheTime: 0, retry: 0 }
  );

  const onChange = values => {
    const { newPassword } = values;

    if (newPassword) {
      setDisplayStrengthMeter(!!newPassword);

      const passwordStrength = zxcvbn(newPassword);

      setStrengthScore(passwordStrength.score);
      setWarningsList([
        passwordStrength?.feedback?.warning,
        ...passwordStrength?.feedback?.suggestions,
      ]);
    }
  };

  const onFinish = values => {
    const { newPassword } = values;

    resetPassword({ newPassword, resetId: requestId })
      .then(response => {
        switch (response.status) {
          case 204:
            notification.success({
              message: intl.formatMessage({
                id: 'notification.resetPassword.success',
              }),
            });

            setDisplayStrengthMeter(false);
            setStrengthScore(0);

            dispatch(replace(LOGIN_ROUTE));
            break;
          case 409:
            notification.error({
              message: intl.formatMessage({
                id: 'notification.resetPassword.error.matchesOldPassword',
              }),
            });
            break;
          default:
            notification.error({
              message: intl.formatMessage({
                id: 'notification.resetPassword.error',
              }),
            });
            break;
        }
      })
      .catch(() =>
        notification.error({
          message: intl.formatMessage({
            id: 'notification.network.error',
          }),
        })
      );
  };

  if (isLoading) return null;

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'resetPassword.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'resetPassword.site.meta' })}
        />
      </Helmet>

      <Wrapper>
        <HeaderWrapper>
          <Logo src={LucaLogoWhiteIconSVG} />
          <SubTitle>
            {intl.formatMessage({
              id: 'header.subtitle',
            })}
          </SubTitle>
        </HeaderWrapper>
        <ForgotPasswordWrapper>
          <ForgotPasswordCard>
            {requestError ? (
              <Alert
                message={intl.formatMessage({
                  id: 'resetPassword.error.expired',
                })}
                type="error"
              />
            ) : (
              <Form onFinish={onFinish} onValuesChange={onChange}>
                <Title>
                  {intl.formatMessage({
                    id: 'resetPassword.title',
                  })}
                </Title>
                <Form.Item
                  colon={false}
                  name="newPassword"
                  label={intl.formatMessage({
                    id: 'profile.changePassword.newPassword',
                  })}
                  hasFeedback
                  rules={[
                    {
                      required: true,
                      message: intl.formatMessage({
                        id: 'error.password',
                      }),
                    },
                    () => ({
                      validator() {
                        if (
                          displayStrengthMeter &&
                          strengthScore < MIN_STRENGTH_SCORE
                        ) {
                          return strengthScore !== MIN_STRENGTH_SCORE - 1
                            ? Promise.reject(warningsList)
                            : Promise.reject(
                                intl.formatMessage({
                                  id: 'error.password.minStrengthRequired',
                                })
                              );
                        }

                        return Promise.resolve();
                      },
                    }),
                  ]}
                >
                  <Input.Password style={inputStyle} />
                </Form.Item>
                {displayStrengthMeter && (
                  <StrengthMeter strengthScore={strengthScore} lightVariation />
                )}
                <Form.Item
                  colon={false}
                  name="newPasswordConfirm"
                  label={intl.formatMessage({
                    id: 'profile.changePassword.newPasswordRepeat',
                  })}
                  hasFeedback
                  dependencies={['newPassword']}
                  rules={[
                    {
                      required: true,
                      message: intl.formatMessage({
                        id: 'error.passwordConfirm',
                      }),
                    },
                    ({ getFieldValue }) => ({
                      validator(rule, value) {
                        if (!value || getFieldValue('newPassword') === value) {
                          return Promise.resolve();
                        }
                        return Promise.reject(
                          intl.formatMessage({
                            id: 'error.passwordConfirm',
                          })
                        );
                      },
                    }),
                  ]}
                >
                  <Input.Password style={inputStyle} />
                </Form.Item>
                <ButtonWrapper>
                  <WhiteButton htmlType="submit">
                    {intl.formatMessage({
                      id: 'resetPassword.form.button',
                    })}
                  </WhiteButton>
                </ButtonWrapper>
              </Form>
            )}
          </ForgotPasswordCard>
        </ForgotPasswordWrapper>
      </Wrapper>
    </>
  );
};
