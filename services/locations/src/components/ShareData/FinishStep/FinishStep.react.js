import React from 'react';
import { useIntl } from 'react-intl';
import { Tick } from 'react-crude-animated-tick';

import { HD_SUPPORT_EMAIL } from 'constants/links';
import { HTTP_PAYLOAD_TOO_LARGE } from 'constants/httpApi';
import {
  BoldSubTitle,
  ContentWrapper,
  ErrorIconWrapper,
  StyledErrorIcon,
  SubTitle,
  SuccessWrapper,
} from './FinishStep.styled';

export const FinishStep = ({ shareDataErrors }) => {
  const intl = useIntl();

  const emailLink = `mailto:${HD_SUPPORT_EMAIL}`;

  const shareDataErrorMessage = () => {
    const requestTooBig = shareDataErrors.some(
      transfer => transfer.status === HTTP_PAYLOAD_TOO_LARGE
    );

    if (requestTooBig) {
      return intl.formatMessage(
        {
          id: 'notification.shareData.errorRequestTooBig',
        },
        {
          // eslint-disable-next-line react/display-name
          a: (...chunks) => (
            <a href={emailLink} rel="noopener noreferrer">
              {chunks}
            </a>
          ),
          email: HD_SUPPORT_EMAIL,
        }
      );
    }

    return intl.formatMessage(
      {
        id: 'notification.shareData.error',
      },
      {
        // eslint-disable-next-line react/display-name
        a: (...chunks) => (
          <a href={emailLink} rel="noopener noreferrer">
            {chunks}
          </a>
        ),
        email: HD_SUPPORT_EMAIL,
      }
    );
  };

  const getShortendTransferId = transferId => transferId.slice(0, 8);

  return (
    <ContentWrapper>
      {shareDataErrors.length > 0 ? (
        <>
          <ErrorIconWrapper>
            <StyledErrorIcon />
          </ErrorIconWrapper>
          <BoldSubTitle>{shareDataErrorMessage()}</BoldSubTitle>
          {shareDataErrors.map(error => (
            <>
              <SubTitle key={error.transferId}>
                {intl.formatMessage(
                  {
                    id: 'shareData.error.failedTransferInfo',
                  },
                  {
                    transferId: getShortendTransferId(error.transferId),
                    status: error.status,
                  }
                )}
              </SubTitle>
              <SubTitle>
                {intl.formatMessage({ id: 'shareData.finish.closeTab' })}
              </SubTitle>
            </>
          ))}
        </>
      ) : (
        <>
          <SuccessWrapper>
            <Tick size={100} />
          </SuccessWrapper>
          <SubTitle>
            {intl.formatMessage({ id: 'shareData.finish.text' })}
          </SubTitle>
          <SubTitle>
            {intl.formatMessage({ id: 'shareData.finish.closeTab' })}
          </SubTitle>
        </>
      )}
    </ContentWrapper>
  );
};
