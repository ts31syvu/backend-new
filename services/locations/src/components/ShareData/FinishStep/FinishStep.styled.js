import styled from 'styled-components';
import { IssuesCloseOutlined } from '@ant-design/icons';

export const SuccessWrapper = styled.div`
  margin: 24px;
`;

export const ContentWrapper = styled.div`
  margin-top: 24px;
`;

export const SubTitle = styled.div`
  margin-bottom: 32px;
`;

export const BoldSubTitle = styled.div`
  margin-bottom: 32px;
  font-weight: bold;
`;

export const StyledErrorIcon = styled(IssuesCloseOutlined)`
  font-size: 60px;
  color: #ffb84d;
  text-align: center;
`;

export const ErrorIconWrapper = styled.div`
  margin: 24px;
  display: flex;
  justify-content: center;
`;
