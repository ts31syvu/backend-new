import React from 'react';

import { useHistory, useLocation } from 'react-router-dom';

import { BASE_ROUTE } from 'constants/routes';
import { PrivateKeyLoader } from 'components/PrivateKeyLoader';
import { RequestContent, StepLabel } from '../ShareData.styled';

export const PrivateKeyStep = ({
  next,
  setPrivateKey,
  publicKey,
  privateKeySecret,
  operator,
}) => {
  const history = useHistory();
  const location = useLocation();

  const handleError = () => {
    history.push(`${BASE_ROUTE}?redirect=${location.pathname}`);
  };

  return (
    <RequestContent>
      <StepLabel>1/2</StepLabel>
      <PrivateKeyLoader
        publicKey={publicKey}
        onSuccess={privateKey => {
          setPrivateKey(privateKey);
          setTimeout(next, 300);
        }}
        onError={handleError}
        infoTextId="shareData.privateKeyStep.info"
        privateKeySecret={privateKeySecret}
        operator={operator}
      />
    </RequestContent>
  );
};
