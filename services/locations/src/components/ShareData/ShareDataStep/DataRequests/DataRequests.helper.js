import moment from 'moment';

const REQUEST_TIME_WARINING = 72;
const timestampFormat = 'DD.MM.YYYY HH:mm';

export function formatTimeStamp(timestamp, intl) {
  return `${moment
    .unix(timestamp)
    .format(timestampFormat)} ${intl.formatMessage({
    id: 'dataTransfers.transfer.timeLabel',
  })}`;
}

export function isRequestTimeSuspicious(startTime, endTime) {
  const duration = moment.duration(
    moment.unix(endTime).diff(moment.unix(startTime))
  );
  const requestTime = duration.asHours();
  return requestTime > REQUEST_TIME_WARINING;
}
