import React from 'react';
import { useIntl } from 'react-intl';

import { RequestContent } from '../../ShareData.styled';
import {
  StyledLabel,
  StyledValue,
  StyledTransfer,
} from '../ShareDataStep.styled';
import {
  isRequestTimeSuspicious,
  formatTimeStamp,
} from './DataRequests.helper';
import { RequestWarning } from './RequestWarning';

export const DataRequests = ({ transfers }) => {
  const intl = useIntl();
  return (
    <RequestContent>
      <StyledLabel>
        {intl.formatMessage({ id: 'shareData.dataRequest' })}
      </StyledLabel>
      {transfers.map(transfer => (
        <StyledTransfer key={transfer.uuid}>
          <StyledTransfer key={transfer.transferId}>
            <StyledValue>
              {`${transfer.groupName} - ${
                transfer.locationName ||
                intl.formatMessage({ id: 'location.defaultName' })
              }`}
            </StyledValue>
          </StyledTransfer>
          <StyledValue>
            {isRequestTimeSuspicious(transfer.time[0], transfer.time[1]) && (
              <RequestWarning />
            )}
          </StyledValue>
          <StyledValue>{`${formatTimeStamp(
            transfer.time[0],
            intl
          )} -`}</StyledValue>
          <StyledValue>{formatTimeStamp(transfer.time[1], intl)}</StyledValue>
        </StyledTransfer>
      ))}
    </RequestContent>
  );
};
