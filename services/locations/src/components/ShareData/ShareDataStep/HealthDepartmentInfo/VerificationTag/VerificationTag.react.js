import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';
import Icon from '@ant-design/icons';
import { getIssuer } from 'network/api';

// Assets
import { VerificationIcon } from 'assets/icons';

import { StyledTooltip } from 'components/general';

import { IconWrapper } from './VerificationTag.styled';

const VerificationIconComp = () => (
  <Icon component={VerificationIcon} style={{ fontSize: 16 }} />
);

export const VerificationTag = ({ healthDepartment }) => {
  const intl = useIntl();
  const { data: issuer } = useQuery(
    ['issuer', healthDepartment.departmentId],
    () => getIssuer(healthDepartment.departmentId)
  );

  if (!issuer?.signedPublicHDEKP) return null;

  return (
    <StyledTooltip title={intl.formatMessage({ id: 'verificationTag.info' })}>
      <IconWrapper>
        <VerificationIconComp />
      </IconWrapper>
    </StyledTooltip>
  );
};
