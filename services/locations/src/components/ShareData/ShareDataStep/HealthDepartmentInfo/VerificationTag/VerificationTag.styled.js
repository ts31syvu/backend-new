import styled from 'styled-components';

export const IconWrapper = styled.div`
  display: flex;
  margin-left: 8px;
`;
