import React from 'react';
import { useIntl } from 'react-intl';

import { SubHeader } from '../../ShareData.styled';
import { InfoText } from '../ShareDataStep.styled';

export const RequestContentContainer = () => {
  const intl = useIntl();

  return (
    <>
      <SubHeader>{intl.formatMessage({ id: 'shareData.shareData' })}</SubHeader>
      <InfoText>
        {intl.formatMessage(
          { id: 'shareData.transfersLabel' },
          {
            note: <b>{intl.formatMessage({ id: 'shareData.note' })}</b>,
          }
        )}
      </InfoText>
      <InfoText>
        {intl.formatMessage({
          id: 'shareData.emailInfo',
        })}
      </InfoText>
    </>
  );
};
