import {
  decryptTrace,
  reencryptAdditionalData,
  reencryptWithHDEKP,
} from 'utils/crypto';
import { ZodError } from 'zod';
import { shareData } from 'network/api';
import { TraceSchema } from 'utils/traceSchema';

/**
 * This function decrypts the outer encryption layer of the traces requested by
 * the health department for the corresponding locationTransfer process. Any
 * additional data will be re-encrypted with the public key of the responsible
 * health department. The remaining data is still encrypted with the daily key.
 *
 * @see https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html#process
 *
 * @param transfer
 * @param trace
 * @param privateKey
 */
export const decryptTraceByOperator = (transfer, trace, privateKey) => {
  const decryptedTrace = decryptTrace(trace, privateKey);
  const hdEncryptedTraceData = reencryptWithHDEKP(
    decryptedTrace.data,
    transfer.department.publicHDEKP
  );
  const reencryptedAdditionalData = reencryptAdditionalData(
    trace.additionalData,
    privateKey,
    transfer.department.publicHDEKP
  );

  return {
    traceId: trace.traceId,
    version: decryptedTrace.version,
    keyId: decryptedTrace.keyId,
    publicKey: decryptedTrace.publicKey,
    verification: decryptedTrace.verification,
    data: hdEncryptedTraceData,
    additionalData: reencryptedAdditionalData,
  };
};

export const getDecryptedTraces = (transfer, privateKey) => {
  const decryptionErrors = [];
  const malformedErrors = [];
  const validTraces = [];

  for (const trace of transfer.traces) {
    try {
      const newValue = decryptTraceByOperator(transfer, trace, privateKey);
      const validatedTrace = TraceSchema.parse(newValue);
      validTraces.push(validatedTrace);
    } catch (error) {
      if (error instanceof ZodError) {
        malformedErrors.push({
          error,
          malformed: true,
          malformedTrace: trace,
        });
      }
      decryptionErrors.push({
        error,
        isInvalid: true,
        invalidTrace: trace,
      });
    }
  }
  if (decryptionErrors.length > 0) {
    console.error(
      'Failed to decrypt traces of',
      `locationTransferId: ${transfer.uuid}`,
      decryptionErrors
    );
  }
  if (malformedErrors.length > 0) {
    console.error(
      'Malformed traces of',
      `locationTransferId: ${transfer.uuid}`,
      malformedErrors
    );
  }

  return validTraces;
};

export const handleShareData = (transfers, privateKey, shareDataErrors) =>
  transfers.map(transfer =>
    shareData({
      traces: { traces: getDecryptedTraces(transfer, privateKey) },
      locationTransferId: transfer.uuid,
    }).then(({ status }) => {
      if (status !== 204) {
        shareDataErrors.push({
          transferId: transfer.uuid,
          status,
        });
      }
    })
  );
