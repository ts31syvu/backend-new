import React, { useState } from 'react';
import moment from 'moment';
import { Alert } from 'antd';
import { useIntl } from 'react-intl';

import { PrimaryButton } from 'components/general';
import { SignatureDownload } from 'components/general/SignatureDownload';

import { useEnrichedTransfers } from 'components/hooks/useEnrichedTransfers';
import {
  FinishButtonWrapper,
  StepLabel,
  RequestContent,
} from '../ShareData.styled';

import { Checkins } from './Checkins';
import { HealthDepartmentInfo } from './HealthDepartmentInfo';
import { DataRequests } from './DataRequests';
import { RequestContentContainer } from './RequestContentContainer';
import { handleShareData } from './ShareDataStep.helper';

/**
 * This step decrypts the outer encryption layer of the traces requested by
 * the health department for the corresponding locationTransfer process. Any
 * additional data will be re-encrypted with the public key of the responsible
 * health department. The remaining data is still encrypted with the daily key.
 *
 * @see https://www.luca-app.de/securityoverview/processes/tracing_find_contacts.html#process
 */

export const ShareDataStep = ({
  lastKeyUpdate,
  transfers,
  next,
  privateKey,
  showStepLabel,
  setShareDataErrors,
}) => {
  const intl = useIntl();
  const [isShareDataButtonDisabled, setIsShareDataButtonDisabled] = useState(
    false
  );
  const enrichedTransfers = useEnrichedTransfers(transfers);

  if (!enrichedTransfers) return null;

  const onFinish = async () => {
    // Disabled the button during decryption process
    setIsShareDataButtonDisabled(true);
    const errors = [];
    // share data
    await Promise.allSettled(
      handleShareData(enrichedTransfers, privateKey, errors)
    );

    setShareDataErrors(errors);

    // Enable the button again after decryption process and data sharing
    setIsShareDataButtonDisabled(false);

    next();
  };

  // get number of too old checkins
  const totalCheckins = enrichedTransfers.reduce(
    (sum, transfer) => sum + transfer.traces.length,
    0
  );
  const tooOldCheckinsForKey = enrichedTransfers.reduce((sum, transfer) => {
    return (
      sum +
      transfer.traces.filter(trace =>
        moment(lastKeyUpdate).isAfter(moment.unix(trace.time[0]))
      ).length
    );
  }, 0);

  const tooOldKeyAll =
    totalCheckins === tooOldCheckinsForKey && tooOldCheckinsForKey > 0;
  const tooOldKeyPartial =
    totalCheckins > tooOldCheckinsForKey && tooOldCheckinsForKey > 0;

  return (
    <>
      {showStepLabel && <StepLabel>2/2</StepLabel>}
      <RequestContent>
        <RequestContentContainer />
      </RequestContent>
      <DataRequests transfers={enrichedTransfers} />
      <Checkins transfers={enrichedTransfers} />
      <HealthDepartmentInfo transfers={enrichedTransfers} />
      {tooOldKeyAll && (
        <Alert
          message={intl.formatMessage({
            id: 'shareData.errorText.tooOldKeyAll',
          })}
          type="error"
        />
      )}
      {tooOldKeyPartial && (
        <Alert
          message={intl.formatMessage({
            id: 'shareData.errorText.tooOldKeyPartial',
          })}
          type="error"
        />
      )}
      <SignatureDownload transfers={enrichedTransfers} />
      <FinishButtonWrapper align="flex-end">
        <PrimaryButton
          data-cy="next"
          onClick={onFinish}
          disabled={isShareDataButtonDisabled || tooOldKeyAll}
        >
          {intl.formatMessage({ id: 'shareData.finish' })}
        </PrimaryButton>
      </FinishButtonWrapper>
    </>
  );
};
