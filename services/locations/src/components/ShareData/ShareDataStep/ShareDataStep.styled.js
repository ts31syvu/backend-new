import styled from 'styled-components';

export const InfoText = styled.div`
  font-size: 16px;
  margin-bottom: 24px;
`;

export const StyledLabel = styled.div`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 8px;
`;

export const StyledValue = styled.div`
  font-size: 16px;
  font-weight: bold;
  display: flex;
  flex-flow: column;
`;

export const StyledTransfer = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 24px;
`;

export const StyledHealthDepartment = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledLink = styled.a`
  color: rgb(80, 102, 124);
  font-size: 12px;
  font-weight: bold;
  text-decoration: none;
`;

export const StyledTextSmall = styled.p`
  color: rgba(0, 0, 0, 0.87);
  font-size: 12px;
  font-weight: 500;
  margin-bottom: 0;
`;
