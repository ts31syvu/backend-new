import React from 'react';

import { IntlProvider } from 'react-intl';
import { messages } from 'messages';
import { useSwitchLanguageContext } from 'components/hooks/useSwitchLanguageContext';

export const InternationalizeProvider = ({ children }) => {
  const { currentLanguage } = useSwitchLanguageContext();

  return (
    <IntlProvider
      locale={currentLanguage}
      messages={messages[currentLanguage]}
      wrapRichTextChunksInFragment
    >
      {children}
    </IntlProvider>
  );
};
