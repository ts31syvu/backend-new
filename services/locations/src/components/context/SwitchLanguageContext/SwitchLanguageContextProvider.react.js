import React, { useEffect, useMemo, useState } from 'react';

import { updateOperator, getMe } from 'network/api';
import { getLanguage, saveLanguage } from 'services';
import { isSupportedLanguage } from 'utils/language';
import { SwitchLanguageContext } from './SwitchLanguageContext.react';

export const SwitchLanguageContextProvider = ({ children }) => {
  const defaultLanguage = getLanguage();
  const [currentLanguage, setCurrentLanguage] = useState(() => defaultLanguage);

  const checkAndStoreSelectedLanguage = (language, callback) => {
    setCurrentLanguage(previousLanguage => {
      if (previousLanguage === language || !isSupportedLanguage(language))
        return previousLanguage;

      callback(language);

      return language;
    });
  };

  const getAndStoreOperatorLanguage = async () => {
    try {
      const { languageOverwrite } = await getMe();

      const operatorLanguage = languageOverwrite ?? currentLanguage;

      checkAndStoreSelectedLanguage(operatorLanguage, language =>
        saveLanguage(language)
      );
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
    }
  };

  const getOperatorLanguage = useMemo(
    () => getAndStoreOperatorLanguage,
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  useEffect(() => {
    getOperatorLanguage();
  }, [getOperatorLanguage]);

  const handleLanguageChange = selectedLanguage => {
    checkAndStoreSelectedLanguage(selectedLanguage, language => {
      try {
        updateOperator({ languageOverwrite: language });
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      }
    });
  };

  const values = {
    currentLanguage,
    handleLanguageChange,
  };

  return (
    <SwitchLanguageContext.Provider value={values}>
      {children}
    </SwitchLanguageContext.Provider>
  );
};
