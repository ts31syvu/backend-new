import React from 'react';

import { CardSectionTitle } from 'components/App/Dashboard/Location/LocationCard';
import { InformationIcon } from 'components/general/InformationIcon';
import { StyledTooltip } from 'components/general/StyledTooltip';

export const CardSection = ({ title, tooltip, children }) => {
  return (
    <CardSectionTitle>
      {title}
      {tooltip && (
        <StyledTooltip title={tooltip}>
          <InformationIcon />
        </StyledTooltip>
      )}
      {children}
    </CardSectionTitle>
  );
};
