import React from 'react';
import { Formik } from 'formik';
import { Form } from 'antd';

import { WithTranslateFormErrors } from '../hoc';

export const AppForm = ({
  children,
  form,
  initialValues,
  onSubmit,
  validationSchema,
  enableReinitialize = false,
}) => {
  return (
    <Formik
      initialValues={initialValues}
      onSubmit={onSubmit}
      validationSchema={validationSchema}
      enableReinitialize={enableReinitialize}
    >
      <WithTranslateFormErrors>
        <Form form={form} style={{ width: '100%' }}>
          {() => <>{children}</>}
        </Form>
      </WithTranslateFormErrors>
    </Formik>
  );
};
