import React from 'react';

import { useFormikContext } from 'formik';
import { Input } from 'antd';

import { ErrorMessage } from '../ErrorMessage/ErrorMessage.react';

export const AppFormField = ({ name, placeholder, disabled, suffix }) => {
  const {
    setFieldTouched,
    handleChange,
    errors,
    touched,
    values,
  } = useFormikContext();

  return (
    <>
      <Input
        autoComplete="off"
        disabled={disabled}
        id={`${name}-input`}
        onBlur={() => setFieldTouched(name)}
        onChange={handleChange(name)}
        placeholder={placeholder}
        suffix={suffix}
        value={values[name]}
      />
      <ErrorMessage error={errors[name]} visible={touched[name]} />
    </>
  );
};
