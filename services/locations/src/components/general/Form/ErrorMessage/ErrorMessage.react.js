import React from 'react';

import { StyledAlert } from './ErrorMessage.styled';

export const ErrorMessage = ({ error, visible }) => {
  if (!visible || !error) return null;

  return <StyledAlert message={error} type="error" showIcon />;
};
