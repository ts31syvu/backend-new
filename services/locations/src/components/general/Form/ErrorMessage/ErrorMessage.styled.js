import { Alert } from 'antd';
import styled from 'styled-components';

export const StyledAlert = styled(Alert)`
  padding: 0 0 0 5px;
  background: none;
  border: 0;
  margin-top: 2px;

  .ant-alert-message {
    color: red;
  }
`;
