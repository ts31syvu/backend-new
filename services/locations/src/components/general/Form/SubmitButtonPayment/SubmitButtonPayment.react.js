import React from 'react';
import { useIntl } from 'react-intl';
import { useFormikContext } from 'formik';
import {
  ButtonWrapper,
  StyledPrimaryButton,
} from './SubmitButtonPayment.styled';

export const SubmitButtonPayment = ({ titleId, dataCy, loading, disabled }) => {
  const intl = useIntl();
  const { handleSubmit } = useFormikContext();

  return (
    <ButtonWrapper>
      <StyledPrimaryButton
        data-cy={dataCy}
        onClick={handleSubmit}
        loading={loading}
        disabled={disabled}
      >
        {intl.formatMessage({
          id: titleId,
        })}
      </StyledPrimaryButton>
    </ButtonWrapper>
  );
};
