import styled from 'styled-components';
import { Media } from 'utils/media';
import { PrimaryButton } from '../../Buttons.styled';

export const StyledPrimaryButton = styled(PrimaryButton)`
  background: rgb(255, 220, 95);
  border: 2px solid rgb(255, 220, 95);

  &:hover,
  &:active,
  &:focus,
  &:focus-visible {
    background: rgb(255, 220, 95) !important;
    border: 2px solid rgb(255, 220, 95) !important;
  }
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: flex-end;
  margin-top: 44px;

  ${Media.mobile`
    margin-top: 40px;
    flex-direction: column;
  `}
`;
