import * as Yup from 'yup';
import { useIntl } from 'react-intl';
import { isValidPhoneNumber } from 'utils/parsePhoneNumber';

Yup.addMethod(Yup.string, 'phoneNumber', function handleError(errorMessage) {
  return this.test(
    'test-phone-number',
    errorMessage,
    value => value && isValidPhoneNumber(value)
  );
});

export const usePhoneValidationSchema = () => {
  const intl = useIntl();

  return Yup.object().shape({
    phone: Yup.string()
      .required(intl.formatMessage({ id: 'error.phone' }))
      .phoneNumber(intl.formatMessage({ id: 'error.phone.invalid' })),
  });
};

// Known as Fully Qualified Telephone Number (FQTN)
export const getE164PhoneValidationSchema = intl => {
  const phoneE164RegExp = /^\+([\s1-9]{1,3})([\d\s]{1,12})$/;

  return Yup.string()
    .required(intl.formatMessage({ id: 'error.phone' }))
    .matches(
      phoneE164RegExp,
      intl.formatMessage({ id: 'error.phone.invalid' })
    );
};
