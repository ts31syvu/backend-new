import { useEffect } from 'react';
import { useFormikContext } from 'formik';

import { useSwitchLanguageContext } from 'components/hooks/useSwitchLanguageContext';

export const useTranslateFormErrors = () => {
  const { setFieldTouched, errors, touched } = useFormikContext();

  const { currentLanguage } = useSwitchLanguageContext();

  useEffect(() => {
    Object.keys(errors).forEach(fieldName => {
      if (Object.keys(touched).includes(fieldName)) {
        setFieldTouched(fieldName);
      }
    });
  }, [currentLanguage, errors, touched, setFieldTouched]);
};
