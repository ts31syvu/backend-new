import React from 'react';

import { setHasSeenDailyKeyBannerToStorage } from 'utils/storage';
import {
  BannerTextWrapper,
  DailyKeyBanner,
  StyledCloseOutlined,
  StyledInfoCircleOutlined,
} from './InfoBanner.styled';

export const InfoBanner = ({ children, setShowBanner }) => {
  const handleCloseBanner = event => {
    event.preventDefault();
    setShowBanner(false);
    setHasSeenDailyKeyBannerToStorage(true);
  };

  return (
    <DailyKeyBanner data-cy="banner">
      <BannerTextWrapper>
        <StyledInfoCircleOutlined />
        {children}
      </BannerTextWrapper>
      <StyledCloseOutlined onClick={handleCloseBanner} data-cy="closeBanner" />
    </DailyKeyBanner>
  );
};
