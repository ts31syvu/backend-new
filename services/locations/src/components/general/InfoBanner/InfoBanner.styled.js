import styled from 'styled-components';
import { CloseOutlined, InfoCircleOutlined } from '@ant-design/icons';

export const DailyKeyBanner = styled.div`
  margin-left: -300px;
  padding: 12px 0;
  background: rgb(80, 102, 124);
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: relative;
  top: 0;
  width: 100vw;
`;

export const BannerTextWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-left: 380px;
`;

export const StyledInfoCircleOutlined = styled(InfoCircleOutlined)`
  font-size: 22px;
  color: #fff;
  margin-right: 11px;
`;

export const StyledCloseOutlined = styled(CloseOutlined)`
  font-size: 12px;
  color: #fff;
  margin-right: 34px;
  cursor: pointer;
`;
