import React from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router';

import { BASE_GROUP_ROUTE } from 'constants/routes';

import { useHistoryContext } from 'components/hooks/useHistoryContext';
import { Navigation, StyledLeftOutlined } from './NavigationButton.styled';

export const NavigationButton = () => {
  const intl = useIntl();
  const history = useHistory();
  const { lastVisitedLocationGroup } = useHistoryContext();

  const navigate = () =>
    history.push(lastVisitedLocationGroup ?? BASE_GROUP_ROUTE);

  return (
    <Navigation onClick={navigate} data-cy="backButton">
      <StyledLeftOutlined />
      {intl.formatMessage({
        id: 'header.profile.back',
      })}
    </Navigation>
  );
};
