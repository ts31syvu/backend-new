import styled from 'styled-components';

export const StyledSticker = styled.div`
  width: 77px;
  height: 16px;
  background: #d3dec3;
  border-radius: 8px;
  color: rgb(0, 0, 0);
  font-size: 10px;
  font-family: Montserrat-Bold, sans-serif;
  font-weight: bold;
  text-align: center;
  margin-left: 8px;
`;
