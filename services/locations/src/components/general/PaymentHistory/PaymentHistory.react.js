import React from 'react';
import { useIntl } from 'react-intl';

import { LocationCard } from 'components/App/Dashboard/Location/LocationCard';
import { CollapsableWrapper } from 'components/App/Dashboard/Location/GenerateQRCodes/GenerateQRCodes.styled';
import { StyledTooltip } from 'components/general';
import { PaymentsTable } from 'components/general/PaymentHistory/PaymentsTable';
import { StyledInformationIcon } from './PaymentHistory.styled';

export const PaymentHistory = ({ locationGroupId, locationId }) => {
  const intl = useIntl();

  const tooltipId = locationId
    ? 'payment.paymentHistory.location.tooltip'
    : 'payment.paymentHistory.locationGroup.tooltip';

  const PaymentHistoryCardTitle = () => (
    <>
      {intl.formatMessage({ id: 'payment.areaPaymentHistoryHeadline' })}
      <StyledTooltip
        placement="bottom"
        title={intl.formatMessage({
          id: tooltipId,
        })}
      >
        <StyledInformationIcon />
      </StyledTooltip>
    </>
  );

  return (
    <LocationCard isCollapse open title={<PaymentHistoryCardTitle />}>
      <CollapsableWrapper>
        <PaymentsTable
          locationId={locationId}
          locationGroupId={locationGroupId}
        />
      </CollapsableWrapper>
    </LocationCard>
  );
};
