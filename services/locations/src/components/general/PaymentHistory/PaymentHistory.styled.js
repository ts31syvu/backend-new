import styled from 'styled-components';
import { InformationIcon } from 'components/general';

export const StyledInformationIcon = styled(InformationIcon)`
  align-self: center;
`;
