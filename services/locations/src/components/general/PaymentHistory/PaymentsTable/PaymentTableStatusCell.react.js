import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import { Badge, Popconfirm } from 'antd';
import React from 'react';
import { useIntl } from 'react-intl';
import { StyledTooltip } from 'components/general';
import Icon from '@ant-design/icons';
import { ReturnIcon } from 'assets/icons';
import { FlexCell, LinkContent } from './PaymentsTable.styled';

export const PaymentTableStatusCell = ({ record, onRefundPayment }) => {
  const intl = useIntl();

  if (record.refunded) {
    return (
      <StyledTooltip
        placement="top"
        title={`${parseFloat(record.refundedAmount).toFixed(
          2
        )} € ${intl.formatMessage({
          id: 'payment.location.paymentHistory.refunded',
        })}`}
      >
        <Badge status="error" text="" />
      </StyledTooltip>
    );
  }

  const RefundConfirm = ({ paymentId }) => (
    <Popconfirm
      placement="topLeft"
      onConfirm={() => onRefundPayment(paymentId)}
      title={intl.formatMessage({
        id: 'payment.location.paymentHistory.refund.confirm.title',
      })}
      okText={intl.formatMessage({
        id: 'payment.location.paymentHistory.refund.confirm.okText',
      })}
      cancelText={intl.formatMessage({
        id: 'payment.location.paymentHistory.refund.confirm.cancelText',
      })}
    >
      <LinkContent>
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.location.paymentHistory.refund',
          })}
        >
          <Icon component={ReturnIcon} />
        </StyledTooltip>
      </LinkContent>
    </Popconfirm>
  );

  switch (record.status) {
    case PAYMENT_STATUS.ACTIVE:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.active',
          })}
        >
          <Badge status="processing" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.CANCELED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.canceled',
          })}
        >
          <Badge status="error" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.CLOSED:
      return (
        <FlexCell>
          <RefundConfirm paymentId={record.id} />
          <StyledTooltip
            placement="top"
            title={intl.formatMessage({
              id: 'payment.historyTable.paymentStatus.tooltip.title.closed',
            })}
          >
            <Badge status="success" text="" />
          </StyledTooltip>
        </FlexCell>
      );
    case PAYMENT_STATUS.ERROR:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.error',
          })}
        >
          <Badge status="error" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.EXPIRED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.expired',
          })}
        >
          <Badge status="warning" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.NEW:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.new',
          })}
        >
          <Badge status="processing" text="" />
        </StyledTooltip>
      );
    case PAYMENT_STATUS.REVERSED:
      return (
        <StyledTooltip
          placement="top"
          title={intl.formatMessage({
            id: 'payment.historyTable.paymentStatus.tooltip.title.reversed',
          })}
        >
          <Badge status="warning" text="" />
        </StyledTooltip>
      );

    default:
      return <Badge status="default" />;
  }
};
