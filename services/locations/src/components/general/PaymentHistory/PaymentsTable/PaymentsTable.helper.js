import React from 'react';
import { PAYMENT_STATUS } from 'constants/paymentOnboarding';
import {
  getFormattedShortDateFromDateTime,
  getFormattedTimeFromDateTime,
} from 'utils/time';
import {
  AmountCell,
  CrossedTableCell,
  TableCell,
} from './PaymentsTable.styled';
import { PaymentTableStatusCell } from './PaymentTableStatusCell.react';

export const getLocationNameMap = groups => {
  return new Map(groups.flatMap(g => g.locations.map(l => [l.uuid, l.name])));
};

const isLineCrossed = record =>
  record.status !== PAYMENT_STATUS.CLOSED || record.refunded;

export const columnConfig = (
  intl,
  locationId,
  locationNameMap,
  onRefundPayment
) => [
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.date',
    }),
    dataIndex: 'date',
    align: 'left',
    render(string, record) {
      return (
        <TableCell>
          {getFormattedShortDateFromDateTime(
            record.status === PAYMENT_STATUS.CLOSED
              ? record.dateTime
              : record.createdDateTime
          )}
        </TableCell>
      );
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.time',
    }),
    dataIndex: 'time',
    align: 'left',
    render(string, record) {
      return (
        <TableCell>
          {getFormattedTimeFromDateTime(
            record.status === PAYMENT_STATUS.CLOSED
              ? record.dateTime
              : record.createdDateTime
          )}
        </TableCell>
      );
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.table',
    }),
    dataIndex: 'table',
    align: 'left',
    render(string) {
      return <TableCell>{string}</TableCell>;
    },
  },
  !locationId
    ? {
        title: intl.formatMessage({
          id: 'payment.paymentHistoryTable.column.area',
        }),
        dataIndex: 'locationId',
        align: 'left',
        render(string, record) {
          const name =
            locationNameMap.get(record.locationId) ||
            intl.formatMessage({ id: 'location.defaultName' });

          return <TableCell>{name}</TableCell>;
        },
      }
    : {
        className: 'hidden',
      },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.code',
    }),
    dataIndex: 'paymentVerifier',
    align: 'center',
    render(string) {
      return <TableCell>{string}</TableCell>;
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.invoiceAmount',
    }),
    dataIndex: 'invoiceAmount',
    align: 'right',
    render(string, record) {
      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {parseFloat(record.invoiceAmount).toFixed(2)} €
          </CrossedTableCell>
        );
      }

      return (
        <TableCell>{parseFloat(record.invoiceAmount).toFixed(2)} €</TableCell>
      );
    },
  },
  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.tipAmount',
    }),
    dataIndex: 'tipAmount',
    align: 'right',
    render(string, record) {
      if (!string) {
        return <TableCell>-</TableCell>;
      }

      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {parseFloat(record.tipAmount).toFixed(2)} €
          </CrossedTableCell>
        );
      }

      return <TableCell>{parseFloat(record.tipAmount).toFixed(2)} €</TableCell>;
    },
  },

  {
    title: intl.formatMessage({
      id: 'payment.paymentHistoryTable.column.total',
    }),
    dataIndex: 'amount',
    align: 'right',
    render(string, record) {
      if (isLineCrossed(record)) {
        return (
          <CrossedTableCell>
            {parseFloat(string || record.originalAmount).toFixed(2)} €
          </CrossedTableCell>
        );
      }

      if (record.status === PAYMENT_STATUS.CLOSED) {
        return <AmountCell>{parseFloat(string).toFixed(2)} €</AmountCell>;
      }
      return <TableCell>-</TableCell>;
    },
  },
  {
    title: '',
    align: 'right',
    render(string, record) {
      return (
        <PaymentTableStatusCell
          record={record}
          onRefundPayment={() => onRefundPayment(record.id)}
        />
      );
    },
  },
];
