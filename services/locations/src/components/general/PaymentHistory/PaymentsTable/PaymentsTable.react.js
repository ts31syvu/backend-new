import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { useQuery, useQueryClient } from 'react-query';
import { notification, Table } from 'antd';

import {
  getLocationGroupPayments,
  getLocationPayments,
  triggerRefund,
} from 'network/payment';

import { QUERY_KEYS, useGetGroups } from 'components/hooks/queries';
import { queryDoNotRetryOnCode } from 'network/utlis';
import { rapydErrorNotification } from 'utils/rapydErrorNotification';
import {
  PaginationWrapper,
  StyledArrowRightIcon,
  StyledButton,
  TableWrapper,
} from './RapydTable.styled';

import { columnConfig, getLocationNameMap } from './PaymentsTable.helper';

const REFETCH_INTERVAL = 30 * 1000; // 30 seconds
const PAGE_SIZE = 10;

export const PaymentsTable = ({ locationGroupId, locationId }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [cursorPayment, setCursorPayment] = useState('');
  const [pageCount, setPageCount] = useState(0);
  const { data: groups } = useGetGroups(locationGroupId);
  const { error, isFetching, data: payments } = useQuery(
    [QUERY_KEYS.PAYMENTS, cursorPayment, locationGroupId, locationId],
    locationId
      ? () => getLocationPayments(locationId, cursorPayment)
      : () => getLocationGroupPayments(locationGroupId, cursorPayment),
    {
      refetchInterval: REFETCH_INTERVAL,
      retry: queryDoNotRetryOnCode([404, 403]),
    }
  );

  if (!locationGroupId || !groups || error) return null;

  const onNext = () => {
    setCursorPayment(payments.cursor ? payments.cursor : '');
    setPageCount(pageCount + 1);
  };
  const onReset = () => {
    setCursorPayment('');
    setPageCount(0);
  };

  const onRefundPayment = paymentId => {
    const onSuccess = async () => {
      await Promise.all([
        queryClient.invalidateQueries(QUERY_KEYS.PAYMENTS),
        queryClient.invalidateQueries(QUERY_KEYS.PAYOUTS),
        queryClient.invalidateQueries(QUERY_KEYS.BALANCE),
      ]).then(() => {
        notification.success({
          message: intl.formatMessage({
            id: 'payment.location.paymentHistory.refund.success',
          }),
        });
      });
    };
    const onError = errorObject => {
      rapydErrorNotification(
        intl,
        errorObject,
        'payment.location.paymentHistory.refund.error'
      );
    };

    triggerRefund(locationGroupId, paymentId)
      .then(result => (result ? onSuccess() : onError()))
      .catch(refundError => onError(refundError));
  };

  const locationNameMap = getLocationNameMap(groups);

  return (
    <>
      <TableWrapper>
        <Table
          columns={columnConfig(
            intl,
            locationId,
            locationNameMap,
            onRefundPayment
          )}
          dataSource={
            payments && payments.payments
              ? payments.payments.map((entry, index) => ({
                  ...entry,
                  key: index + (1 + PAGE_SIZE * pageCount),
                }))
              : []
          }
          loading={isFetching}
          locale={{
            emptyText: intl.formatMessage({
              id: 'payment.paymentHistoryTable.emptyState',
            }),
          }}
        />
      </TableWrapper>
      <PaginationWrapper>
        <StyledButton onClick={() => onReset()}>
          {intl.formatMessage({ id: 'payment.historyTable.previous' })}
        </StyledButton>
        <StyledButton disabled={!payments?.cursor} onClick={() => onNext()}>
          {intl.formatMessage({ id: 'payment.historyTable.next' })}
          <StyledArrowRightIcon />
        </StyledButton>
      </PaginationWrapper>
    </>
  );
};
