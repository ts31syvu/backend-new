import styled from 'styled-components';
import { Button } from 'antd';
import { ArrowRightBlueIcon } from 'assets/icons';

export const TableWrapper = styled.div`
  .ant-table-thead {
    tr {
      th {
        background: unset;
        color: rgb(0, 0, 0);
        font-size: 14px;
        font-weight: bold;
        text-align: center;
        padding-left: 8px;
        border-bottom: 2px solid #979797;
      }
    }
  }
  .ant-table-tbody {
    tr {
      td {
        border: none;
        padding: 8px;
        color: rgb(0, 0, 0);
        font-size: 14px;
        font-weight: 500;
        text-align: center;

        div {
          justify-content: flex-start;
        }
      }
      &:nth-child(2n) {
        background: rgb(243, 245, 247);
      }

      &:last-child {
        td {
          border-bottom: 2px solid rgba(135, 135, 135, 0.25);
        }
      }
    }
  }
  .ant-table-pagination.ant-pagination {
    display: none;
  }
  .ant-table-cell.hidden {
    display: none;
  }
  .ant-table-thead
    > tr
    > th:not(:last-child):not(.ant-table-selection-column):not(.ant-table-row-expand-icon-cell):not([colspan])::before {
    width: 0 !important;
  }
`;

export const PaginationWrapper = styled.div`
  padding: 24px 0;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

export const StyledButton = styled(Button)`
  border: none;
  color: rgb(80, 102, 124);
  font-size: 14px;
  font-weight: bold;
  background-color: transparent;
  box-shadow: none;
  display: flex;
  align-items: center;
  padding: 0;

  &[ant-click-animating-without-extra-node='true']::after {
    display: none;
  }

  &:disabled {
    border: none;
    color: #d3d3d3;
    font-size: 14px;
    font-family: Montserrat-Bold, sans-serif;
    font-weight: bold;
    background-color: transparent;
    box-shadow: none;

    &:hover {
      border: none;
      color: #d3d3d3;
      background-color: transparent;
      box-shadow: none;
    }
  }
`;

export const StyledArrowRightIcon = styled(ArrowRightBlueIcon)`
  margin-left: 8px;
`;
