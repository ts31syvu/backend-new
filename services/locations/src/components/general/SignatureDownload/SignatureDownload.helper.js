import moment from 'moment';

const downloadSignature = (filename, signatureString) => {
  const blob = new Blob([signatureString], {
    type: 'text/plain;charset=utf-8',
  });
  const element = window.document.createElement('a');
  element.href = window.URL.createObjectURL(blob);
  element.download = `${filename}.txt`;
  document.body.append(element);
  element.click();
  element.remove();
};

const formatTimeStamp = (timestamp, intl) =>
  `${moment.unix(timestamp).format('DD.MM.YYYY HH:mm')} ${intl.formatMessage({
    id: 'dataTransfers.transfer.timeLabel',
  })}`;

export const downloadHandler = ({
  transfers,
  issuers,
  basicCA,
  rootCA,
  intl,
}) => {
  let downloadCounter = 1;

  for (const transfer of transfers) {
    const issuer = issuers.find(
      issuerObject => issuerObject.uuid === transfer.departmentId
    );

    const date = moment(transfer.createdAt).format('YYYYMMDD');
    const locationName = `${transfer.groupName} - ${
      transfer.locationName ||
      intl.formatMessage({ id: 'location.defaultName' })
    }`;
    const noSpaceLocationName = locationName.replace(/\s/g, '');
    const fileName = `${intl.formatMessage(
      { id: 'shareData.signatureDownloader.filename' },
      { locationName: noSpaceLocationName, date }
    )}${transfers.length > 1 ? `_${downloadCounter}` : ''}`;

    const formattedLocationName = `Location Name:\n${locationName}`;
    const numberOfCheckins = transfer.traces
      ? `Number Of Checkins:\n${transfer.traces.length}`
      : '';
    const requestTimeframe = `Requested Timeframe:\n${formatTimeStamp(
      transfer.time[0],
      intl
    )} - ${formatTimeStamp(transfer.time[1], intl)}`;

    const rootCertificate = `Root Certificate:\n${rootCA}`;
    const intermediateCertificate = `Intermediate Certificate:\n${basicCA}`;
    const publicCertificate = `Public Certificate:\n${issuer.publicCertificate}`;
    const signedPublicHDSKP = `Signed Public HDSKP:\n${issuer.signedPublicHDSKP}`;
    const signedLocationTransfer = `Signed Location Transfer:\n${transfer.signedLocationTransfer}`;

    const transferDownload = `${formattedLocationName}\n\n${numberOfCheckins}\n\n${requestTimeframe}\n\n\n${rootCertificate}\n\n${intermediateCertificate}\n\n${publicCertificate}\n\n${signedPublicHDSKP}\n\n${signedLocationTransfer}`;

    downloadCounter += 1;
    downloadSignature(fileName, transferDownload);
  }
};
