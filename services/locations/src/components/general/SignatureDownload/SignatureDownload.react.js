import React from 'react';
import { useIntl } from 'react-intl';
import { useQueries, useQuery } from 'react-query';

import { getIssuer } from 'network/api';
import { getBasicCA, getRootCA } from 'network/static';

import { downloadHandler } from './SignatureDownload.helper';
import { StyledLink, StyledTextSmall } from './SignatureDownload.styled';

export const SignatureDownload = ({ transfers }) => {
  const intl = useIntl();

  const { data: basicCA } = useQuery('basicCA', getBasicCA);
  const { data: rootCA } = useQuery('rootCA', getRootCA);

  const issuersQueries = useQueries(
    transfers.map(({ departmentId }) => ({
      queryKey: ['issuer', departmentId],
      queryFn: () => getIssuer(departmentId),
    }))
  );

  const issuerQueriesComplete = issuersQueries.every(
    issuerQuery => issuerQuery.isSuccess
  );

  if (!basicCA || !rootCA || !issuerQueriesComplete) return null;

  const issuers = issuersQueries.map(issuerQuery => issuerQuery.data);

  return (
    <>
      <StyledTextSmall>
        {intl.formatMessage({ id: 'shareData.signatureDownloadInfo' })}
      </StyledTextSmall>
      <StyledLink
        onClick={() =>
          downloadHandler({ transfers, issuers, basicCA, rootCA, intl })
        }
      >
        {intl.formatMessage({ id: 'shareData.signatureDownload' })}
      </StyledLink>
    </>
  );
};
