import styled from 'styled-components';

const TRANSITION = 'width 0.5s ease-in-out, background 0.25s';
const TRANSITION1 = 'width 0.5s ease-in-out, background 0.35s';
const TRANSITION2 = 'width 0.5s ease-in-out, background 0.45s';
const TRANSITION3 = 'width 0.5s ease-in-out, background 0.55s';
const TRANSITION4 = 'width 0.5s ease-in-out, background 0.65s';

export const veryWeakPassword = {
  backgroundColor: '#f16704',
  transition: TRANSITION,
};

export const weakPassword = {
  backgroundColor: '#fdac72',
  transition: TRANSITION1,
};

export const mediumPassword = {
  backgroundColor: '#8399af',
  transition: TRANSITION2,
};

export const strongPassword = {
  backgroundColor: '#b2c596',
  transition: TRANSITION3,
};

export const veryStrongPassword = {
  backgroundColor: '#6c8448',
  transition: TRANSITION4,
};

export const Container = styled.div`
  width: 460px;
  display: flex;
  align-items: center;
  margin-bottom: 20px;
`;

export const Wrapper = styled.div`
  flex: 50%;
  display: flex;
  justify-content: space-between;
  height: 8px;
  background: ${lightVariation =>
    lightVariation ? 'transparent' : 'rgb(195, 206, 217)'};
`;

export const StrengthDescription = styled.div`
  flex: 50%;
  font-size: 14px;
  font-weight: 500;
  margin-left: 10px;
`;

export const StrengthCell = styled.div`
  width: 40px;
  border: 0.5px solid rgb(41, 41, 41);
  border-top-left-radius: ${isStartCell => (isStartCell ? '100px' : 0)};
  border-bottom-left-radius: ${isStartCell => (isStartCell ? '100px' : 0)};
  border-top-right-radius: ${isEndCell => (isEndCell ? '100px' : 0)};
  border-bottom-right-radius: ${isEndCell => (isEndCell ? '100px' : 0)};
`;
