/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Switch as UISwitch } from 'antd';
import { useIntl } from 'react-intl';
import {
  StyledContainer,
  StyledInfo,
  StyledCheckOutlined,
  StyledCloseOutlined,
} from './Switch.styled';

export function Switch({
  checked,
  onChange,
  customCheckedLabel,
  isLabelVisible,
  ...properties
}) {
  const intl = useIntl();
  const label =
    customCheckedLabel || intl.formatMessage({ id: 'switch.active' });

  return (
    <StyledContainer>
      {(checked || isLabelVisible) && <StyledInfo>{label}</StyledInfo>}
      <UISwitch
        {...properties}
        checked={checked}
        onChange={(value, event) => {
          // eslint-disable-next-line no-unused-expressions
          onChange && onChange(value, event);
        }}
        checkedChildren={<StyledCheckOutlined />}
        unCheckedChildren={<StyledCloseOutlined />}
      />
    </StyledContainer>
  );
}
