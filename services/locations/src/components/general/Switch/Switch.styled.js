import styled from 'styled-components';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

export const StyledInfo = styled.div`
  font-size: 12px;
  font-weight: 500;
  text-align: right;
  padding-right: 8px;
  color: rgba(0, 0, 0, 0.87);
`;
export const StyledContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledCloseOutlined = styled(CloseOutlined)`
  color: #898989;
`;

export const StyledCheckOutlined = styled(CheckOutlined)`
  color: #6c8448;
`;
