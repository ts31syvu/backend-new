import { useMediaQuery } from 'react-responsive';
import { TABLET_BREAKPOINT } from 'constants/environment';

export const useTabletSize = () => {
  return useMediaQuery({ maxWidth: TABLET_BREAKPOINT });
};
