import { useQuery, useQueries } from 'react-query';
import {
  getMe,
  getPrivateKeySecret,
  getDeletedGroups,
  getLocationLinks,
  getGroups,
  getAdditionalData,
  getOperatorDevices,
  getAllTransfers,
  getHealthDepartment,
  getGroup,
  getLocation,
} from 'network/api';
import { getSignatureValidityForAllTransfers } from 'utils/signatures';
import { getValidatedExtractedDailyKey } from 'utils/dailyKey';
import {
  getBalance,
  getPaymentActive,
  getOperatorPaymentEnabled,
  getOnboardingStatus,
} from 'network/payment';

export const QUERY_KEYS = {
  ME: 'ME',
  LOCATION: 'LOCATION',
  SETTINGS: 'SETTINGS',
  GROUPS: 'GROUPS',
  PRIVATE_KEY_SECRET: 'PRIVATE_KEY_SECRET',
  DELETED_GROUPS: 'DELETED_GROUPS',
  LOCATION_LINKS: 'LOCATION_LINKS',
  ADDITIONAL_DATA: 'ADDITIONAL_DATA',
  OPERATOR_DEVICES: 'OPERATOR_DEVICES',
  TRANSFERS: 'TRANSFERS',
  TRANSFERS_WITH_VALIDITY: 'TRANSFERS_WITH_VALIDITY',
  HEALTH_DEPARTMENT: 'HEALTH_DEPARTMENT',
  GROUP: 'GROUP',
  PAYMENT_ENABLED: 'PAYMENT_ENABLED',
  PAYMENTS: 'GET_PAYMENTS',
  PAYOUTS: 'GET_PAYOUTS',
  BALANCE: 'GET_BALANCE',
  CURRENT_COUNT: 'CURRENT',
  VERSION: 'VERSION',
  TRACES: 'TRACES',
  CLIENT_CONFIG: 'CLIENT_CONFIG',
  CHALLENGE: 'CHALLENGE',
  IS_MAIL_CHALLENGE_IN_PROGRESS: 'IS_MAIL_CHALLENGE_IN_PROGRESS',
  DAILY_KEY: 'DAILY_KEY',
  PAYMENT_ONBOARDING_STATUS: 'PAYMENT_ONBOARDING_STATUS',
  LOCATION_PAYMENT_ACTIVE: 'LOCATION_PAYMENT_ACTIVE',
};

export const useGetMe = () =>
  useQuery(QUERY_KEYS.ME, getMe, {
    cacheTime: 0,
    retry: false,
  });

export const useDailyKey = () =>
  useQuery(QUERY_KEYS.DAILY_KEY, getValidatedExtractedDailyKey, {
    cacheTime: 0,
    retry: 0,
  });

export const useGetLocationById = locationId =>
  useQuery([QUERY_KEYS.LOCATION, locationId], () => getLocation(locationId), {
    cacheTime: 0,
  });

export const useGetLocationsOfGroup = (queryKey, locationIds) =>
  useQueries(
    locationIds.map(locationId => {
      return {
        queryKey: [queryKey, locationId],
        queryFn: () => getLocation(locationId),
        cacheTime: 0,
      };
    })
  );

export const useGetGroups = otherKeys =>
  useQuery([QUERY_KEYS.GROUPS, otherKeys ?? ''], getGroups);

export const useGetPrivateKeySecret = () =>
  useQuery(QUERY_KEYS.PRIVATE_KEY_SECRET, getPrivateKeySecret, {
    retry: false,
  });

export const useDeletedGroups = () =>
  useQuery(QUERY_KEYS.DELETED_GROUPS, getDeletedGroups);

export const useGetLocationLinks = locationId =>
  useQuery([QUERY_KEYS.LOCATION_LINKS, locationId], () =>
    getLocationLinks(locationId)
  );

export const useGetAdditionalData = locationId =>
  useQuery([QUERY_KEYS.ADDITIONAL_DATA, locationId], () =>
    getAdditionalData(locationId)
  );

export const useGetTransfers = () =>
  useQuery(QUERY_KEYS.TRANSFERS, getAllTransfers);

export const useGetAllTransfersWithValidity = () =>
  useQuery(QUERY_KEYS.TRANSFERS_WITH_VALIDITY, async () => {
    return getSignatureValidityForAllTransfers(await getAllTransfers());
  });

export const useGetOperatorDevices = () =>
  useQuery(QUERY_KEYS.OPERATOR_DEVICES, getOperatorDevices);

export const useGetHealthDepartment = departmentId =>
  useQuery([QUERY_KEYS.HEALTH_DEPARTMENT, departmentId], () =>
    getHealthDepartment(departmentId)
  );

export const useGetMultipleHealthDepartments = departmentIds =>
  useQueries(
    departmentIds.map(departmentId => ({
      queryKey: [QUERY_KEYS.HEALTH_DEPARTMENT, departmentId],
      queryFn: () => getHealthDepartment(departmentId),
    }))
  );

export const useGetGroup = groupId =>
  useQuery([QUERY_KEYS.GROUP, groupId], () => getGroup(groupId));

export const useGetBalance = (locationGroupId, options) =>
  useQuery(QUERY_KEYS.BALANCE, () => getBalance(locationGroupId), options);

export const useGetPaymentActive = locationId =>
  useQuery([QUERY_KEYS.LOCATION_PAYMENT_ACTIVE, locationId], () =>
    getPaymentActive(locationId)
  );

export const useGetPaymentEnabled = (operatorId, options) =>
  useQuery(
    QUERY_KEYS.PAYMENT_ENABLED,
    () => getOperatorPaymentEnabled(operatorId),
    { ...options }
  );

export const useGetPaymentOnboardingStatus = locationGroupId =>
  useQuery(
    [QUERY_KEYS.PAYMENT_ONBOARDING_STATUS, locationGroupId],
    () => getOnboardingStatus(locationGroupId),
    { enabled: !!locationGroupId }
  );
