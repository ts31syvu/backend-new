import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';

import { createChallenge, getChallengeState } from 'network/api';

const challengeStates = state => ({
  authPinRequired: state === 'AUTHENTICATION_PIN_REQUIRED',
  canceled: state === 'CANCELED',
  done: state === 'DONE',
  privateKeyPinRequired: state === 'PRIVATE_KEY_PIN_REQUIRED',
  privateKeyRequired: state === 'PRIVATE_KEY_REQUIRED',
});

export const useChallenge = (intervalMs = 750) => {
  const [challengeUuid, setChallengeUuid] = useState(null);

  const queryKey = ['challenge', challengeUuid];
  const queryFunction = () => getChallengeState(challengeUuid);
  const queryOptions = {
    refetchInterval: intervalMs,
    enabled: !!challengeUuid,
  };

  const { data: challenge } = useQuery(queryKey, queryFunction, queryOptions);

  useEffect(() => {
    if (challengeUuid) return;

    const generateChallendeUid = async () => {
      const { uuid } = await createChallenge();

      setChallengeUuid(uuid);
    };

    generateChallendeUid();
  }, [challengeUuid]);

  const challengeState = challengeStates(challenge?.state);

  return { challenge, challengeState };
};
