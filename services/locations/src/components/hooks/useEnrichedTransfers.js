import { useQueries } from 'react-query';
import { getLocationTransferTraces } from 'network/api';
import { useGetMultipleHealthDepartments } from 'components/hooks/queries';

export const useEnrichedTransfers = transfers => {
  const transferTraceQueries = useQueries(
    transfers.map(({ uuid }) => ({
      queryKey: ['traces', uuid],
      queryFn: async () => {
        const traces = await getLocationTransferTraces(uuid);
        return { transferId: uuid, traces };
      },
    }))
  );

  const healthDepartmentQueries = useGetMultipleHealthDepartments([
    ...new Set(transfers?.map(transfer => transfer.departmentId) || []),
  ]);

  const transferTraceQueriesComplete = transferTraceQueries.every(
    transferTraceQuery => transferTraceQuery.isSuccess === true
  );

  const healthDepartmentsQueryComplete = healthDepartmentQueries.every(
    healthDepartmentQuery => healthDepartmentQuery.isSuccess === true
  );

  if (!healthDepartmentsQueryComplete || !transferTraceQueriesComplete)
    return null;

  const transferTraces = transferTraceQueries.map(
    transferTraceQuery => transferTraceQuery.data
  );

  const healthDepartments = healthDepartmentQueries.map(
    healthDepartmentQuery => healthDepartmentQuery.data
  );

  return transfers.map(transfer => ({
    ...transfer,
    traces:
      transferTraces.find(
        transferTrace => transferTrace.transferId === transfer.uuid
      )?.traces || [],
    department: healthDepartments.find(
      healthDepartment =>
        healthDepartment.departmentId === transfer.departmentId
    ),
  }));
};
