import { useQuery } from 'react-query';
import { getClientConfig } from 'network/api';
import { defaultFeatureFlags } from 'constants/featureFlags';

export const useFeatureFlags = () => {
  const { data: clientConfig = {} } = useQuery(
    'clientConfig',
    getClientConfig,
    {
      staleTime: Number.POSITIVE_INFINITY,
      refetchOnWindowFocus: false,
    }
  );

  return { ...defaultFeatureFlags, ...clientConfig };
};
