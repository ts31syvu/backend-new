import React from 'react';

import { HistoryContext } from 'components/context/HistoryContext';

export const useHistoryContext = () => React.useContext(HistoryContext);
