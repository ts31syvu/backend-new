import React from 'react';

import { SwitchLanguageContext } from '../context/SwitchLanguageContext';

export const useSwitchLanguageContext = () =>
  React.useContext(SwitchLanguageContext);
