import { ZxcvbnOptions, zxcvbn } from '@zxcvbn-ts/core';
import zxcvbnCommonPackage from '@zxcvbn-ts/language-common';
import zxcvbnDePackage from '@zxcvbn-ts/language-de';
import zxcvbnEnPackage from '@zxcvbn-ts/language-en';

const options = {
  graphs: zxcvbnCommonPackage.adjacencyGraphs,
  translations: {
    ...zxcvbnDePackage.translations,
    ...zxcvbnEnPackage.translations,
  },
  dictionary: {
    ...zxcvbnCommonPackage.dictionary,
    ...zxcvbnDePackage.dictionary,
    ...zxcvbnEnPackage.dictionary,
  },
};

ZxcvbnOptions.setOptions(options);

export { zxcvbn };
