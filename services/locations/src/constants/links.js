export const TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions/';

export const FAQ_LINK = 'https://www.luca-app.de/faq/';
export const VIDEOS_LINK = 'https://youtu.be/aYj9hyldumM';
export const TOOLKIT_LINK = 'https://www.luca-app.de/download/';

export const QR_PRINT_LINK = 'https://www.qr-print.de/luca';
export const GITLAB_LINK =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/locations';

export const WEB_APP_BASE_PATH = `https://${window.location.host}/webapp/`;

export const HD_SUPPORT_EMAIL = 'gesundheitsamt@luca-app.de';

export const OPERATOR_APP_ANDROID_LINK =
  'https://play.google.com/store/apps/details?id=de.culture4life.lucalocations';

export const OPERATOR_APP_IOS_LINK =
  'https://apps.apple.com/de/app/luca-locations/id1581199297';

export const NO_DAILY_KEY_BLOG_POST_LINK =
  'https://www.luca-app.de/luca-digitale-plattform-fuer-gastronomie-und-kultur/ ';

export const LOCATION_RAPYD_LINK = 'https://luca-app.de/luca-Pay ';

export const LOCATION_LUCA_PAY_TERMS_CONDITIONS_LINK =
  'https://www.luca-app.de/operator-terms-and-conditions-payment';

export const LOCATION_LUCA_PAY_DPA_LINK =
  'https://www.luca-app.de/operator-dpa-payment';
