export const ONBOARDING_STATUS = {
  GROUP_NOT_FOUND: 'GROUP_NOT_FOUND', // Frontend status for 404 response
  NOT_STARTED: 'NOT_STARTED',
  KYB_PENDING: 'KYB_PENDING',
  KYB_SUBMITTED: 'KYB_SUBMITTED',
  KYB_COMPLETED: 'KYB_COMPLETED',
  PAYOUT_PENDING: 'PAYOUT_PENDING',
  KYB_REJECTED: 'KYB_REJECTED',
  DONE: 'DONE',
};

export const PAYMENT_STATUS = {
  ACTIVE: 'ACTIVE', // Active and awaiting completion of 3DS or capture. Can be updated.
  CANCELED: 'CANCELED', // Canceled by the client or the customer's bank.
  CLOSED: 'CLOSED', // Closed and paid.
  ERROR: 'ERROR', // Error. An attempt was made to create or complete a payment, but it failed.
  EXPIRED: 'EXPIRED', // The payment has expired.
  NEW: 'NEW', // Not closed.
  REVERSED: 'REVERSED', // Reversed by Rapyd.
};
