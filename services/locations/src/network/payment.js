import { ONBOARDING_STATUS } from 'constants/paymentOnboarding';
import { requestWithStatusHandlers } from './utlis';
import { getJwt } from '../utils/jwtStorage';
import * as ApiUtils from './utlis';

const PAYMENT_API_PATH = '/pay/api';

const headers = jwt => ({
  'Content-Type': 'application/json',
  'X-Auth': jwt,
});

export const getOperatorPaymentEnabled = async operatorId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/operators/${operatorId}/paymentEnabled`,
    headers(await getJwt()),
    null,
    () => ({ paymentEnabled: false })
  ).catch(() => ({ paymentEnabled: false }));

export const getOnboardingStatus = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding`,
    headers(await getJwt()),
    {
      200: response => response.json(),
      404: () => ({
        status: ONBOARDING_STATUS.GROUP_NOT_FOUND,
      }),
    }
  );

export const getLocationGroup = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}`,
    headers(await getJwt())
  );

export const getPayoutDetails = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payoutDetails`,
    headers(await getJwt())
  );

export const createLocationGroup = async (
  locationGroupId,
  phoneNumber,
  companyName,
  businessFirstName,
  businessLastName,
  street,
  streetNumber,
  zipCode,
  city,
  invoiceEmail,
  vatNumber
) =>
  fetch(`${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}`, {
    method: 'POST',
    headers: headers(await getJwt()),
    body: JSON.stringify({
      locationGroupId,
      phoneNumber,
      companyName,
      businessFirstName,
      businessLastName,
      street,
      streetNumber,
      zipCode,
      city,
      invoiceEmail,
      vatNumber,
    }),
  }).then(ApiUtils.checkResponse);

export const startRapydKybProcess = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/kyb`,
    {
      method: 'POST',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const initiatePayoutOnboarding = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/payout`,
    {
      method: 'POST',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const changePayoutOnboarding = async locationGroupId =>
  fetch(
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/onboarding/payout`,
    {
      method: 'PUT',
      headers: headers(await getJwt()),
    }
  ).then(ApiUtils.checkResponse);

export const getPaymentActive = async locationId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locations/${locationId}/paymentActive`,
    headers(await getJwt())
  );

export const patchLocation = async (locationId, paymentActive) =>
  fetch(`${PAYMENT_API_PATH}/v1/locations/${locationId}`, {
    method: 'PATCH',
    headers: headers(await getJwt()),
    body: JSON.stringify({ paymentActive }),
  }).then(ApiUtils.checkResponse);

export const getLocationGroupPayments = async (locationGroupId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payments?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payments: [] })
  );

export const getLocationPayments = async (locationId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locations/${locationId}/payments?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payments: [] })
  );

export const getPayouts = async (locationGroupId, cursor) =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payouts?cursor=${cursor}`,
    headers(await getJwt()),
    null,
    () => ({ cursor: null, payouts: [] })
  );

export const getBalance = async locationGroupId =>
  requestWithStatusHandlers(
    'GET',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/balance`,
    headers(await getJwt()),
    null,
    () => {}
  );

export const triggerPayout = async locationGroupId =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payouts`,
    headers(await getJwt()),
    {
      200: () => true,
      201: () => true,
    },
    () => false
  );

export const triggerRefund = async (locationGroupId, paymentId) =>
  requestWithStatusHandlers(
    'POST',
    `${PAYMENT_API_PATH}/v1/locationGroups/${locationGroupId}/payments/${paymentId}/refund`,
    headers(await getJwt()),
    {
      200: () => true,
      201: () => true,
    },
    () => false
  );
