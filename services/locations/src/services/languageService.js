import {
  getLanguage as getLanguageFromBrowser,
  isSupportedLanguage,
} from 'utils/language';

const languageKey = 'lang';

export function saveLanguage(language) {
  localStorage.setItem(languageKey, language);

  return language;
}

export function getLanguage() {
  const clientBrowserLanguage = getLanguageFromBrowser();
  const language = localStorage.getItem(languageKey) ?? clientBrowserLanguage;

  if (!isSupportedLanguage(language)) {
    return saveLanguage(clientBrowserLanguage);
  }

  return language;
}
