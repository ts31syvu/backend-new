import { proxy } from 'comlink';
import { message } from 'antd';
import FileSaver from 'file-saver';
import { LOADING_MESSAGE, openLoadingMessage } from 'components/notifications';
import { WEB_APP_BASE_PATH } from 'constants/links';
import { getQRCodeFilename } from './qrCodeData';

const TABLE_KEY = 'table';

export const downloadPDF = async ({
  setIsDownloading,
  pdfWorkerApiReference,
  location,
  intl,
  isTableQRCodeEnabled,
  isCWAEventEnabled,
}) => {
  const messageText = intl.formatMessage({ id: 'message.generatingPDF' });
  const showLoadingProgress = percentage =>
    openLoadingMessage(percentage, messageText);
  setIsDownloading(true);
  showLoadingProgress(0);
  const { getPDF } = pdfWorkerApiReference.current;
  const pdf = await getPDF(
    location,
    isTableQRCodeEnabled,
    isCWAEventEnabled,
    WEB_APP_BASE_PATH,
    intl.formatMessage({
      id: 'modal.qrCodeDocument.table',
    }),
    TABLE_KEY,
    proxy(showLoadingProgress)
  );
  showLoadingProgress(100);

  const filename = getQRCodeFilename(
    location,
    isTableQRCodeEnabled,
    intl,
    'pdf'
  );

  FileSaver.saveAs(pdf, filename);
  setIsDownloading(false);
  message.destroy(LOADING_MESSAGE);
};
