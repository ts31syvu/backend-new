export const getEntryPolicyOptions = enable2GPlus =>
  enable2GPlus
    ? [
        {
          id: 'TWO_G',
          locale: 'settings.location.entryPolicy.2G',
          value: '2G',
        },
        {
          id: 'TWO_G_PLUS',
          locale: 'settings.location.entryPolicy.2GPlus',
          value: '2GPlus',
        },
        {
          id: 'THREE_G',
          locale: 'settings.location.entryPolicy.3G',
          value: '3G',
        },
      ]
    : [
        {
          id: 'TWO_G',
          locale: 'settings.location.entryPolicy.2G',
          value: '2G',
        },
        {
          id: 'THREE_G',
          locale: 'settings.location.entryPolicy.3G',
          value: '3G',
        },
      ];

export const LocationEntryPolicyTypes = {
  TWO_G: '2G',
  THREE_G: '3G',
  TWO_G_PLUS: '2GPlus',
};
