import { useIntl } from 'react-intl';
import { messages } from 'messages';

const languages = Object.keys(messages);

export const isSupportedLanguage = currentLanguage =>
  languages.includes(currentLanguage);

export const excludeCurrentLanguageFromLocales = currentLanguage =>
  languages.filter(language => currentLanguage !== language);

export const getLanguage = () => {
  const language = navigator.language.split(/[_-]/)[0];
  if (isSupportedLanguage(language)) {
    return language;
  }

  return 'en';
};

export const useFormatMessage = () => {
  const intl = useIntl();
  return id => intl.formatMessage({ id });
};
