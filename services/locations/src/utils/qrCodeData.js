import { generateQRPayload } from '@lucaapp/cwa-event';
import { RESTAURANT_TYPE } from 'components/App/modals/CreateLocationModal/CreateLocationModalContent/CreateLocationModalContent.helper';
import { CWA } from 'constants/cwaQrCodeValue';
import sanitize from 'sanitize-filename';

const getLocationAddress = location =>
  `${location.streetName} ${location.streetNr}, ${location.zipCode} ${location.city}`;

export const getLocationName = location =>
  location.name === null || location.name === undefined
    ? `${location.groupName}`
    : `${location.groupName} ${location.name}`;

const generateLocationCWAContentPart = location => {
  const address = getLocationAddress(location).slice(0, CWA.MAX_ADDRESS_LENGTH);
  const description = getLocationName(location).slice(
    0,
    CWA.MAX_DESCRIPTION_LENGTH
  );
  const qrCodeContent = {
    description,
    address,
    defaultcheckinlengthMinutes: location.averageCheckinTime
      ? location.averageCheckinTime
      : CWA.DEFAULT_CHECKIN_MINUTES,
    locationType:
      location.type === RESTAURANT_TYPE
        ? CWA.LOCATION_TYPE_PERMANENT_FOOD_SERVICE
        : CWA.LOCATION_TYPE_PERMANENT_OTHER,
  };
  return generateQRPayload(qrCodeContent);
};

export const getCWAFragment = (location, isCWAEventEnabled) =>
  isCWAEventEnabled ? `/CWA1/${generateLocationCWAContentPart(location)}` : '';

export const getQRCodeFilename = (
  location,
  isTableQRCodeEnabled,
  intl,
  format
) => {
  const locationName = `_${location.name}`;
  const fileName = `${location.groupName.replace(' ', '_')}${
    location.name ? locationName.replace(new RegExp(' ', 'g'), '_') : ''
  }`;

  const fileNameLocale = isTableQRCodeEnabled
    ? 'downloadFile.locations.tableQrCodes'
    : 'downloadFile.locations.generalQrCode';

  return sanitize(
    intl.formatMessage({ id: fileNameLocale }, { fileName, format })
  );
};
