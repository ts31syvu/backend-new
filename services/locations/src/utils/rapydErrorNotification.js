import { notification } from 'antd';

const getPoEditorId = (rapydErrorCode, fallbackPoEditorId) => {
  switch (rapydErrorCode) {
    case 'ERROR_GET_EWALLET_NOT_ENOUGH_FUNDS':
      return 'payment.rapydError.errorGetEwalletNotEnoughFunds';
    default:
      return fallbackPoEditorId;
  }
};

export const rapydErrorNotification = (
  intl,
  errorObject,
  fallbackPoEditorId
) => {
  if (errorObject && errorObject?.rapydErrorCode) {
    const id = getPoEditorId(errorObject.rapydErrorCode, fallbackPoEditorId);
    notification.error({
      message: intl.formatMessage({ id }),
    });
  }

  return null;
};
