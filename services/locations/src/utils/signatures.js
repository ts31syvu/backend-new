import { verifySignedLocationTransfer } from '@lucaapp/crypto';
import { getRootCA, getBasicCA } from 'network/static';
import { getIssuer } from 'network/api';

export const getSignatureValidityForTransfer = (transfer, rootCA, basicCA) =>
  getIssuer(transfer.departmentId)
    .then(issuer => {
      const decodedLocationTransfer = verifySignedLocationTransfer({
        certificateChain: [rootCA, basicCA],
        signedLocationTransfer: transfer.signedLocationTransfer,
        issuer,
      });
      return { ...transfer, time: decodedLocationTransfer.time };
    })
    .catch(error => console.error(error));

export const getSignatureValidityForAllTransfers = transfers =>
  Promise.all([getRootCA(), getBasicCA()]).then(([rootCA, basicCA]) =>
    Promise.all(
      transfers.map(transfer =>
        getSignatureValidityForTransfer(transfer, rootCA, basicCA)
      )
    ).then(checkedTransfers =>
      checkedTransfers.filter(checkedTransfer => !!checkedTransfer)
    )
  );
