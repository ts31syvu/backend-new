import {
  DAILY_KEY_BANNER_SEEN_STORAGE_KEY,
  PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY,
} from 'constants/storage';

export const setHasSeenPrivateKeyModal = value =>
  sessionStorage.setItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY, value.toString());

export const hasSeenPrivateKeyModal = () =>
  sessionStorage.getItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY);

export const clearHasSeenPrivateKeyModal = () =>
  sessionStorage.removeItem(PRIVATE_KEY_MODAL_SEEN_STORAGE_KEY);

// Daily key banner
export const setHasSeenDailyKeyBannerToStorage = value =>
  localStorage.setItem(DAILY_KEY_BANNER_SEEN_STORAGE_KEY, value.toString());

export const getHasSeenDailyKeyBannerFromStorage = () =>
  localStorage.getItem(DAILY_KEY_BANNER_SEEN_STORAGE_KEY);
