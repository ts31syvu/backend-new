export const whitespaceTrim = stringValue => stringValue.replace(/\s/g, '');
