import { z } from 'utils/tracesValidation';

export const TraceSchema = z.object({
  traceId: z.traceId(),
  data: z.object({
    data: z.base64({ max: 44 }),
    publicKey: z.ecPublicKey(),
    mac: z.mac(),
    iv: z.iv(),
  }),
  publicKey: z.ecCompressedPublicKey(),
  keyId: z.dailyKeyId(),
  version: z.number().int().optional(),
  verification: z.base64({ rawLength: 8 }),
  additionalData: z
    .object({
      data: z.base64({ max: 1024 }),
      publicKey: z.ecPublicKey(),
      mac: z.mac(),
      iv: z.iv(),
    })
    .optional()
    .nullable(),
});
