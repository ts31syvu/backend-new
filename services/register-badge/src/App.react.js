import React from 'react';

import moment from 'moment';
import 'moment/locale/de';

import { Route, Switch, Redirect } from 'react-router';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import { getLanguage } from 'utils/language';
import { ErrorWrapper } from 'components/ErrorWrapper';

// i18n
import { IntlProvider } from 'react-intl';
import { messages } from 'messages';

// Misc
import { AppContent } from 'components/AppContent';
import { QueryClientProvider, QueryClient } from 'react-query';
import { REGISTER_BADGE_ROUTE, LICENSES_ROUTE } from 'constants/routes';
import { Licenses } from './components/Licenses';
import { AppWrapper } from './App.styled';
import { configureStore } from './configureStore';

moment.locale(getLanguage());
document.documentElement.lang = getLanguage();

const history = createBrowserHistory();
const store = configureStore(undefined, history);
const queryClient = new QueryClient();

export const Main = () => {
  return (
    <Provider store={store}>
      <IntlProvider
        locale={getLanguage()}
        messages={messages[getLanguage()]}
        wrapRichTextChunksInFragment
      >
        <ConnectedRouter history={history}>
          <QueryClientProvider client={queryClient}>
            <ErrorWrapper>
              <AppWrapper>
                <Switch>
                  <Route path={LICENSES_ROUTE} component={Licenses} />
                  <Route path={REGISTER_BADGE_ROUTE} component={AppContent} />
                  <Redirect to={REGISTER_BADGE_ROUTE} />
                </Switch>
              </AppWrapper>
            </ErrorWrapper>
          </QueryClientProvider>
        </ConnectedRouter>
      </IntlProvider>
    </Provider>
  );
};
