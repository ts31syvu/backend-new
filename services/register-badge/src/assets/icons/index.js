// SVGs
import ErrorIconSVG from './ErrorIcon.svg';
import LucaLogoWhiteIconSVG from './LucaLogoWhiteIcon.svg';

// Icons
export { ReactComponent as ErrorIcon } from './ErrorIcon.svg';
export { ReactComponent as LucaLogoWhiteIcon } from './LucaLogoWhiteIcon.svg';
export { ReactComponent as MenuActiveIcon } from './MenuActiveIcon.svg';
export { ReactComponent as MenuInactiveIcon } from './MenuInactiveIcon.svg';

export { ErrorIconSVG, LucaLogoWhiteIconSVG };
