import { getMe } from 'network/api';
import { useQuery } from 'react-query';
import { QUERY_KEYS } from 'constants/queryKeys';

export function useGetMe() {
  const { isLoading, error, data: operator } = useQuery(
    QUERY_KEYS.me,
    () => getMe(),
    {
      retry: false,
    }
  );
  const isLoggedIn = !!operator?.operatorId;
  const requiresVerification = !!error || !operator?.isTrusted;
  return { isLoading, requiresVerification, isLoggedIn, operator };
}
