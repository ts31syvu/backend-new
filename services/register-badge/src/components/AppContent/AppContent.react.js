import React from 'react';
import { Spin } from 'antd';
import { Header } from './Header';
import { Footer } from './Footer';
import { useGetMe } from './AppContent.helper';
import { RegisterCard } from './RegisterCard';

export const AppContent = () => {
  const { isLoading, requiresVerification, isLoggedIn, operator } = useGetMe();

  if (isLoading) return <Spin />;

  return (
    <>
      <Header operator={operator} isLoggedIn={isLoggedIn} />
      <RegisterCard
        requiresVerification={requiresVerification}
        stepLength={requiresVerification ? 5 : 4}
      />
      <Footer />
    </>
  );
};
