import React from 'react';
import { useIntl } from 'react-intl';
import { useQuery } from 'react-query';

import { getVersion } from 'network/static';
import { TERMS_CONDITIONS_LINK, FAQ_LINK, GITLAB_LINK } from 'constants/links';

import { QUERY_KEYS } from 'constants/queryKeys';
import { Wrapper, Link, Version } from './Footer.styled';

export const Footer = () => {
  const intl = useIntl();
  const { error, isLoading, data: info } = useQuery(
    QUERY_KEYS.version,
    getVersion,
    {
      refetchOnWindowFocus: false,
    }
  );

  if (error || isLoading) return null;

  return (
    <Wrapper data-cy="footerLinks">
      <Link
        href={FAQ_LINK}
        data-cy="linkFaq"
        target="_blank"
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'footer.registerBadge.faq' })}
      </Link>
      <Link
        href={GITLAB_LINK}
        data-cy="linkGit"
        target="_blank"
        rel="noopener noreferrer"
      >
        {intl.formatMessage({ id: 'footer.registerBadge.repository' })}
      </Link>
      <Link
        href={TERMS_CONDITIONS_LINK}
        data-cy="linkTermsCondition"
        target="_blank"
      >
        {intl.formatMessage({ id: 'footer.registerBadge.agb' })}
      </Link>
      <Version>
        {intl.formatMessage({ id: 'footer.registerBadge.title' })}
      </Version>
      <Version data-cy="lucaVersionNumber" title={`(${info.version})`}>
        {`(${info.version})`}
      </Version>
    </Wrapper>
  );
};
