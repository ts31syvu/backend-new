import React from 'react';
import { screen } from '@testing-library/react';

import { render } from 'utils/testing';

import { Header } from './Header.react';

describe('rendering', () => {
  test('renders the luca logo, headline and link-menu', () => {
    render(<Header />);
    expect(screen.queryByTestId('logo')).toBeInTheDocument();
    expect(
      screen.getByText('Schlüsselanhänger Registrieren')
    ).toBeInTheDocument();
  });
});
