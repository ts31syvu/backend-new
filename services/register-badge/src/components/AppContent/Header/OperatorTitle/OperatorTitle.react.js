import React, { useEffect } from 'react';
import { notification } from 'antd';
import { useIntl } from 'react-intl';
import { OperatorTitleWrapper } from '../Header.styled';

export const OperatorTitle = ({ operator, isLoggedIn }) => {
  const intl = useIntl();

  useEffect(() => {
    if (!isLoggedIn) {
      notification.info({
        message: intl.formatMessage({
          id: 'registerBadge.header.operator.notTrusted',
        }),
        duration: 0,
      });
    } else {
      notification.destroy();
    }
  }, [isLoggedIn, intl]);

  return (
    <>
      {operator?.isTrusted && (
        <OperatorTitleWrapper>
          {intl.formatMessage(
            { id: 'registerBadge.header.operatorGreeting' },
            { name: `${operator.firstName} ${operator.lastName}` }
          )}
        </OperatorTitleWrapper>
      )}
    </>
  );
};
