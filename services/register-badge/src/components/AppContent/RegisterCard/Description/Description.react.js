import React from 'react';
import { useIntl } from 'react-intl';
import { SubTitle, Content } from './Description.styled';

export const Description = ({
  stepNumber,
  stepTitle,
  children,
  hasStep = true,
  stepLength,
}) => {
  const intl = useIntl();
  return (
    <>
      {hasStep && (
        <SubTitle>
          {intl.formatMessage({ id: 'registerBadge.serialCode.step' })}{' '}
          {stepNumber + 1}/{stepLength}: {stepTitle}
        </SubTitle>
      )}
      <Content>{children}</Content>
    </>
  );
};
