import styled from 'styled-components';

export const SubTitle = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: bold;
  margin-top: 32px;
`;

export const Content = styled.div`
  color: rgb(0, 0, 0);
  font-size: 16px;
  font-weight: 500;
  margin-top: 8px;
`;
