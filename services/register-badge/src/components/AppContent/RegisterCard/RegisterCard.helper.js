import {
  bytesToHex,
  encodeUtf8,
  ENCRYPT_AES_CTR,
  GET_RANDOM_BYTES,
  hexToBase64,
  HMAC_SHA256,
  SIGN_EC_SHA256_DER,
} from '@lucaapp/crypto';
import {
  MAX_CITY_LENGTH,
  MAX_EMAIL_LENGTH,
  MAX_HOUSE_NUMBER_LENGTH,
  MAX_NAME_LENGTH,
  MAX_PHONE_LENGTH,
  MAX_POSTAL_CODE_LENGTH,
  MAX_STREET_LENGTH,
} from 'constants/valueLength';
import { requestTan, updateBadgeUser, verifyTan } from 'network/api';
import { notify } from 'utils/notify';
import { registerBadge } from 'constants/registerBadge.model';

export const SERIAL_CODE = 'REGISTER_BADGE_SERIAL_CODE';
export const CONTACT_INFO_NAME = 'REGISTER_BADGE_CONTACT_INFO_NAMES';
export const CONTACT_INFO_ADDRESS = 'REGISTER_BADGE_CONTACT_INFO_ADDRESS';
export const CONTACT_INFO_VERIFICATION =
  'REGISTER_BADGE_CONTACT_INFO_VERIFICATION';
export const CONTACT_TAN_VERIFICATION = 'REGISTER_BADGE_TAN_VERIFICATION';
export const REGISTER_BADGE_SUCCESS = 'REGISTER_BADGE_SUCCESS';

export const validateResponse = response => {
  if (
    response.errors &&
    response.errors.some(error => (error.path ?? [''])[0] === 'phone')
  ) {
    throw new Error(response.error);
  }
  if (!response.challengeId) throw new Error('missing challengeId');
};

export const verifyTanForUser = (tan, next, intl, form) => {
  const { tanVerification } = tan;
  const { challengeId } = registerBadge.badgeInfo;
  verifyTan({ challengeId, tan: tanVerification })
    .then(response => {
      if (response.status !== 204) {
        return;
      }
      form.submit();
    })
    .catch(() => notify('notification.verifyTan.error', intl));
};

export const requestTanForUser = async (values, next, intl) => {
  try {
    const response = await requestTan({ phone: values.phone });
    validateResponse(response);
    const { challengeId } = response;
    const finalValues = { ...values, challengeId };
    registerBadge.badgeInfo = finalValues;
    next();
  } catch (error) {
    console.error(error);
    notify('notification.verifyTan.error', intl);
  }
};

export const saveInput = (response, next) => {
  registerBadge.badgeInfo = response;
  next();
};

export const handleFormInputValues = (
  form,
  next,
  callbackFunction,
  intl,
  message = 'registerBadge.error.general'
) => {
  form
    .validateFields()
    .then(response => {
      callbackFunction(response, next, intl, form);
    })
    .catch(() => {
      notify(message, intl);
    });
};

const generateServerPayload = (userSecrets, userData) => {
  const buffer = bytesToHex(encodeUtf8(JSON.stringify(userData)));
  const iv = GET_RANDOM_BYTES(16);
  const encryptedUserData = ENCRYPT_AES_CTR(
    buffer,
    userSecrets.userDataKey,
    iv
  );

  const mac = HMAC_SHA256(
    encryptedUserData,
    userSecrets.userVerificationSecret
  );
  const signature = SIGN_EC_SHA256_DER(
    userSecrets.privateKey,
    encryptedUserData + mac + iv
  );

  return {
    data: hexToBase64(encryptedUserData),
    iv: hexToBase64(iv),
    mac: hexToBase64(mac),
    signature: hexToBase64(signature),
  };
};

export const onResetHandler = form => {
  form.resetFields();
  Object.keys(registerBadge.badgeInfo).forEach(key => {
    delete registerBadge.badgeInfo[key];
  });
};

export const onSubmitHandler = ({
  values,
  setIsRegisterBadgeDone,
  userSecrets,
  intl,
}) => {
  registerBadge.badgeInfo = { ...registerBadge.badgeInfo, ...values };
  const userData = {
    v: 2,
    vs: hexToBase64(userSecrets?.userVerificationSecret),
    fn: String(registerBadge.badgeInfo?.firstName).slice(0, MAX_NAME_LENGTH),
    ln: String(registerBadge.badgeInfo?.lastName).slice(0, MAX_NAME_LENGTH),
    pn: String(registerBadge.badgeInfo?.phone).slice(0, MAX_PHONE_LENGTH),
    e: String(registerBadge.badgeInfo?.email).slice(0, MAX_EMAIL_LENGTH),
    st: String(registerBadge.badgeInfo?.street).slice(0, MAX_STREET_LENGTH),
    hn: String(registerBadge.badgeInfo?.houseNr)
      .slice(0, MAX_HOUSE_NUMBER_LENGTH)
      .trim(),
    pc: String(registerBadge.badgeInfo?.zipCode).slice(
      0,
      MAX_POSTAL_CODE_LENGTH
    ),
    c: String(registerBadge.badgeInfo?.city).slice(0, MAX_CITY_LENGTH).trim(),
  };

  updateBadgeUser(
    userSecrets.userId,
    generateServerPayload(userSecrets, userData)
  )
    .then(() => setIsRegisterBadgeDone(true))
    .catch(() => notify('error.registerBadge.serverError', intl));
};
