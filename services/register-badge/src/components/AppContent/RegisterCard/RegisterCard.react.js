import React, { useState } from 'react';
import { Steps } from 'antd';
import { useIntl } from 'react-intl';

import { Wrapper, RegisterWrapper, StyledForm } from './RegisterCard.styled';

import {
  SerialCode,
  ContactInformation,
  ContactAddress,
  RegisterSuccess,
  ContactVerification,
  TanVerification,
} from './Steps';

import {
  SERIAL_CODE,
  CONTACT_INFO_NAME,
  CONTACT_INFO_ADDRESS,
  CONTACT_INFO_VERIFICATION,
  CONTACT_TAN_VERIFICATION,
  onSubmitHandler,
  onResetHandler,
} from './RegisterCard.helper';

export const RegisterCard = ({ requiresVerification, stepLength }) => {
  const [currentStep, setCurrentStep] = useState(0);
  const [form] = StyledForm.useForm();
  const intl = useIntl();
  const [userSecrets, setUserSecrets] = useState({});

  const [isRegisterBadgeDone, setIsRegisterBadgeDone] = useState(false);
  const nextStep = () => setCurrentStep(currentStep + 1);
  const previousStep = () => setCurrentStep(currentStep - 1);

  const reset = () => {
    setIsRegisterBadgeDone(false);
    setCurrentStep(0);
    onResetHandler(form);
  };

  const onSubmit = values =>
    onSubmitHandler({ values, userSecrets, intl, setIsRegisterBadgeDone });

  const basicSteps = [
    {
      id: SERIAL_CODE,
      content: (
        <SerialCode
          stepNumber={currentStep}
          stepLength={stepLength}
          setUserSecrets={setUserSecrets}
          next={nextStep}
        />
      ),
    },
    {
      id: CONTACT_INFO_NAME,
      content: (
        <ContactInformation
          form={form}
          stepNumber={currentStep}
          stepLength={stepLength}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: CONTACT_INFO_ADDRESS,
      content: (
        <ContactAddress
          form={form}
          stepNumber={currentStep}
          stepLength={stepLength}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
    {
      id: CONTACT_INFO_VERIFICATION,
      content: (
        <ContactVerification
          form={form}
          stepNumber={currentStep}
          stepLength={stepLength}
          requirePhoneVerification={requiresVerification}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
  ];

  const withVerificationSteps = [
    ...basicSteps,
    {
      id: CONTACT_TAN_VERIFICATION,
      content: (
        <TanVerification
          form={form}
          stepNumber={currentStep}
          stepLength={stepLength}
          next={nextStep}
          back={previousStep}
        />
      ),
    },
  ];

  const steps = requiresVerification ? withVerificationSteps : basicSteps;

  return (
    <Wrapper>
      <RegisterWrapper>
        {!isRegisterBadgeDone ? (
          <StyledForm form={form} validateMessages={null} onFinish={onSubmit}>
            <Steps
              progressDot={() => null}
              current={currentStep}
              style={{ margin: 0 }}
            >
              {steps.map(step => (
                <Steps.Step key={step.id} />
              ))}
            </Steps>
            {steps[currentStep].content}
          </StyledForm>
        ) : (
          <RegisterSuccess finish={reset} />
        )}
      </RegisterWrapper>
    </Wrapper>
  );
};
