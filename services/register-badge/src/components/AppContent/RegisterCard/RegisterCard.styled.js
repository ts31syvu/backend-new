import styled from 'styled-components';
import { Media } from 'utils/media';
import { Form, Input } from 'antd';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  padding: 24px 0;

  ${Media.mobile`
    height: auto;
  `}
`;

export const RegisterWrapper = styled.div`
  width: 800px;
  height: auto;
  padding: 32px;
  background: rgb(195, 206, 217);
  color: #000;
  border-radius: 4px;

  display: flex;
  flex-direction: column;
  margin: auto;

  ${Media.mobile`
    width: 100%;
    margin-top: 24px;
    padding: 40px 16px;
  `}
`;

export const Title = styled.div`
  color: rgb(0, 0, 0);
  font-size: 34px;
  line-height: 42px;
  font-weight: 500;
  letter-spacing: 0;

  ${Media.mobile`
    font-size: 24px;
  `}
`;

export const ButtonWrapper = styled.div`
  display: flex;
  justify-content: ${({ multipleButtons }) =>
    multipleButtons ? 'space-between' : 'flex-end'};
  margin-top: 45px;

  ${Media.smallMobile`
    flex-direction: column-reverse;

    & button:last-child {
      margin-bottom: 8px;
    }
  `}
`;

export const FormWrapper = styled.div`
  margin-top: 32px;
  display: flex;
  flex-direction: column;
  width: 100%;
`;

export const RegisterInput = styled(Input)`
  border: 1px solid black;
  border-radius: 0;
  background: transparent !important;
  color: black;
`;

export const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
`;

export const StyledFormItem = styled(Form.Item)`
  display: block;
`;
