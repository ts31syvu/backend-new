import React from 'react';
import { useIntl } from 'react-intl';

import { Success } from 'components/general';
import { Title } from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { Description } from 'components/AppContent/RegisterCard/Description';

import { IS_MOBILE } from 'constants/environment';
import {
  SuccessWrapper,
  ButtonWrapper,
  SecondaryButtonSuccess,
  StyledWhiteButton,
} from './RegisterSuccess.styled';

export const RegisterSuccess = ({ finish }) => {
  const intl = useIntl();

  return (
    <SuccessWrapper>
      <Success />
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.success.title' })}
      </Title>
      <Description hasStep={false}>
        {intl.formatMessage({
          id: 'registerBadge.serialCode.success.description',
        })}
      </Description>
      <ButtonWrapper>
        {!IS_MOBILE && (
          <SecondaryButtonSuccess type="link" href="/">
            {intl.formatMessage({
              id: 'registerBadge.serialCode.success.back',
            })}
          </SecondaryButtonSuccess>
        )}
        <StyledWhiteButton onClick={finish}>
          {intl.formatMessage({
            id: 'registerBadge.serialCode.success.registerAnother',
          })}
        </StyledWhiteButton>
      </ButtonWrapper>
    </SuccessWrapper>
  );
};
