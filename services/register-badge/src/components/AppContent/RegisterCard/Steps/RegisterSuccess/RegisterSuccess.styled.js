import styled from 'styled-components';
import { Media } from 'utils/media';
import { SecondaryButton, WhiteButton } from 'components/general';

export const SuccessWrapper = styled.div`
  padding: 40px 60px;
  text-align: center;

  ${Media.mobile`
    padding: 0;
  `}
`;

export const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 40px;
`;

export const SecondaryButtonSuccess = styled(SecondaryButton)`
  align-self: center;
  width: fit-content;
  margin-bottom: 16px;

  ${Media.smallMobile`
    width: 100%;
  `}
`;

export const StyledWhiteButton = styled(WhiteButton)`
  align-self: center;
  width: fit-content;

  ${Media.smallMobile`
    width: 100%;
  `}
`;
