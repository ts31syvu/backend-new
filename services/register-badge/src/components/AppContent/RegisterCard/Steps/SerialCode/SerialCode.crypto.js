import argon2 from 'argon2-browser';

import {
  base32CrockfordToHex,
  hexToBytes,
  hexToUuid4,
  bytesToHex,
  bytesToUint8Array,
  HKDF_SHA256,
} from '@lucaapp/crypto';

// ref: https://luca-app.de/securityoverview/badge/check_in.html
const SERIAL_NUMBER_END = {
  int_2: 2,
  char_h: 'h',
  char_g: 'g',
  int_0: 0,
};

const ARGON_SALT = 'da3ae5ecd280924e';

const bytesToUuid = (hexUserId, rawUserId) => {
  return `${hexUserId.slice(0, 8)}-${hexUserId.slice(8, 12)}-${hexUserId.slice(
    12,
    16
  )}-${hexUserId.slice(16, 20)}-${rawUserId.slice(20, 32)}`;
};

// Used to encrypt data
// Level 1 is the serial number found in the badges
// Level 2 is the QR-Code
const LEVEL1_INFO = bytesToHex('badge_crypto_assets');
const LEVEL2_INFO = bytesToHex('badge_tracing_assets');

const isV4Badge = serialNumber =>
  serialNumber?.toLowerCase().endsWith(SERIAL_NUMBER_END.int_2) ||
  serialNumber?.toLowerCase().endsWith(SERIAL_NUMBER_END.char_h);

const calculateV4Secrets = async serialNumber => {
  const realSerialNumber = serialNumber
    .toLowerCase()
    .replace(/h$/, SERIAL_NUMBER_END.char_g)
    .replace(/2$/, SERIAL_NUMBER_END.int_0);

  const entropy = base32CrockfordToHex(realSerialNumber);

  const argon2hash = await argon2.hash({
    pass: bytesToUint8Array(hexToBytes(entropy)),
    salt: ARGON_SALT,
    time: 11,
    mem: 32 * 1024,
    hashLen: 16,
    parallelism: 1,
    type: argon2.ArgonType.Argon2id,
  });
  const seed = argon2hash.hashHex;

  // Level1: can only be calculated using the badges serial number
  const level1 = await HKDF_SHA256(seed, 64, LEVEL1_INFO, '');
  const userDataKey = level1.slice(0, 32);
  const tracingSeed = level1.slice(32, 64);
  const privateKey = level1.slice(64, 128);

  // Level2: can only be calculated using the badges QR code
  const level2 = await HKDF_SHA256(tracingSeed, 48, LEVEL2_INFO, '');
  const rawUserId = level2.slice(0, 32);
  const userVerificationSecret = level2.slice(32, 64);
  const hexUserId = hexToUuid4(rawUserId);
  const userId = bytesToUuid(hexUserId, rawUserId);

  return {
    userId,
    userDataKey,
    userVerificationSecret,
    privateKey,
  };
};

export const decodeSerial = serialNumber => {
  if (isV4Badge(serialNumber)) {
    return calculateV4Secrets(serialNumber);
  }
  return null;
};
