import React from 'react';

import { useIntl } from 'react-intl';

import { useTanValidator } from 'components/hooks/useValidators';
import {
  FormWrapper,
  Title,
} from 'components/AppContent/RegisterCard/RegisterCard.styled';
import { SecondaryButton, WhiteButton } from 'components/general';
import { Description } from 'components/AppContent/RegisterCard/Description';
import {
  TanCodeInput,
  FormItem,
  StyledButtonWrapper,
} from './TanVerification.styled';
import {
  handleFormInputValues,
  verifyTanForUser,
} from '../../RegisterCard.helper';

export const TanVerification = ({
  form,
  next,
  back,
  stepNumber,
  stepLength,
}) => {
  const intl = useIntl();
  const tanValidator = useTanValidator();

  return (
    <>
      <Title>
        {intl.formatMessage({ id: 'registerBadge.serialCode.title' })}
      </Title>
      <Description
        stepNumber={stepNumber}
        stepLength={stepLength}
        stepTitle={intl.formatMessage({
          id: 'registerBadge.tanVerification.stepTitle',
        })}
      >
        {intl.formatMessage({
          id: 'registerBadge.tanVerification.desciption',
        })}
      </Description>
      <FormWrapper>
        <FormItem name="tanVerification" rules={tanValidator}>
          <TanCodeInput />
        </FormItem>
        <StyledButtonWrapper multipleButtons>
          <SecondaryButton onClick={back}>
            {intl.formatMessage({ id: 'registerBadge.sericalCode.back' })}
          </SecondaryButton>
          <WhiteButton
            onClick={() =>
              handleFormInputValues(
                form,
                next,
                verifyTanForUser,
                intl,
                'error.registerBadge.invalidTanVerification'
              )
            }
          >
            {intl.formatMessage({ id: 'registerBadge.sericalCode.next' })}
          </WhiteButton>
        </StyledButtonWrapper>
      </FormWrapper>
    </>
  );
};
