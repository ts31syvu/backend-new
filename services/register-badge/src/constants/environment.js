export const IS_DEV = process.env.NODE_ENV === 'development';

// Styles
export const MOBILE_BREAKPOINT = 600;
export const SMALL_MOBILE_BREAKPOINT = 360;
export const HEIGHT_BREAKPOINT = 768;
export const IS_MOBILE = window.innerWidth <= MOBILE_BREAKPOINT;
