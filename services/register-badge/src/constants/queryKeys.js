export const QUERY_KEYS = {
  me: 'me',
  licenses: 'licenses',
  version: 'version',
};
