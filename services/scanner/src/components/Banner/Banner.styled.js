import styled from 'styled-components';

export const BannerBox = styled.div`
  background-color: #e8e7e5;
  color: black;
  border-radius: 4px;
  margin: 64px 100px 0 100px;
  padding: 10px 20px;
`;
