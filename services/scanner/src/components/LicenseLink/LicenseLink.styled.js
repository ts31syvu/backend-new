import styled from 'styled-components';

export const Link = styled.a`
  margin-right: 16px;
  color: rgb(255, 255, 255);
  font-size: 14px;
  text-decoration: underline;
  font-weight: bold;
  letter-spacing: 0;
  line-height: 16px;
`;
