import React, { useState } from 'react';
import { useIntl } from 'react-intl';
import { Helmet } from 'react-helmet-async';

import { Header } from 'components/Header';
import { useDailyKey } from 'components/hooks/queries';
import { ScanForm } from './ScanForm';
import { ScannerWrapper } from './QrScanner.styled';
import { Banner } from '../Banner';

export const QrScanner = ({ scanner }) => {
  const intl = useIntl();
  const [outerFocus, setOuterFocus] = useState(false);

  const { isLoading, error, data: dailyKey } = useDailyKey();

  if (isLoading) return null;

  return (
    <>
      <Helmet>
        <title>{intl.formatMessage({ id: 'scanner.site.title' })}</title>
        <meta
          name="description"
          content={intl.formatMessage({ id: 'scanner.site.meta' })}
        />
      </Helmet>
      <ScannerWrapper
        onClick={() => {
          setOuterFocus(true);
        }}
      >
        <Header />
        {error || !dailyKey ? (
          <Banner
            title={intl.formatMessage({ id: 'scanner.banner.noDailyKey' })}
          />
        ) : (
          <ScanForm
            scanner={scanner}
            outerFocus={outerFocus}
            setOuterFocus={setOuterFocus}
          />
        )}
      </ScannerWrapper>
    </>
  );
};
