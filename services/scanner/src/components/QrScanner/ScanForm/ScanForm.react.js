import React, { useRef, useState, useEffect, useCallback } from 'react';
import { useSelector } from 'react-redux';
import { useIntl } from 'react-intl';
import moment from 'moment';
import { useQueryClient } from 'react-query';
import { Tick } from 'react-crude-animated-tick';

import { handleScanData } from 'helpers';

import { useModal } from 'components/hooks/useModal';
import { AdditionalDataModal } from 'components/modals/AdditionalDataModal';
import { EntryPolicyModal } from 'components/modals/EntryPolicyModal';
import { Update } from 'components/Update';

import { reloadFilter } from 'utils/bloomFilter';

import {
  QUERY_KEYS,
  useGetAdditionalData,
  useGetCurrentCheckinsCount,
  useGetTotalCheckinsCount,
} from 'components/hooks/queries';

import {
  ScanFormWrapper,
  FormWrapper,
  Wrapper,
  Content,
  Count,
  HiddenInput,
  SuccessOverlay,
} from './ScanForm.styled';

export const ScanForm = ({ scanner, outerFocus, setOuterFocus }) => {
  const intl = useIntl();
  const queryClient = useQueryClient();
  const [openModal, closeModal] = useModal();
  const [isSuccess, setIsSuccess] = useState(false);
  const [inputValue, setInputValue] = useState('');
  const [latestUpdate, setLatestUpdate] = useState(moment().unix());
  const inputReference = useRef(null);
  const debounceTimeout = useRef(null);
  const isOpen = useSelector(({ modal }) => !!modal);

  const triggerFocus = useCallback(() => {
    if (!inputReference.current) return;
    inputReference.current.focus();
  }, [inputReference]);

  const { data: currentCount } = useGetCurrentCheckinsCount(
    scanner.scannerAccessId
  );
  const { data: totalCount } = useGetTotalCheckinsCount(
    scanner.scannerAccessId
  );
  const { data: additionalData } = useGetAdditionalData(scanner.locationId);

  useEffect(() => {
    setLatestUpdate(moment().unix());
  }, [currentCount]);

  const refetch = () => {
    queryClient.invalidateQueries([
      QUERY_KEYS.TOTAL_CHECKINS_COUNT,
      scanner.scannerAccessId,
    ]);
    queryClient.invalidateQueries([
      QUERY_KEYS.CURRENT_CHECKINS_COUNT,
      scanner.scannerAccessId,
    ]);
    triggerFocus();
  };

  const checkForAdditionalData = traceId => {
    if (
      !scanner.tableCount &&
      !additionalData?.additionalData?.some(field => field.isRequired)
    ) {
      return;
    }

    openModal({
      title: intl.formatMessage({
        id: 'modal.additionalData.title',
      }),
      content: (
        <AdditionalDataModal
          scanner={scanner}
          additionalData={additionalData.additionalData}
          traceId={traceId}
          close={() => {
            closeModal();
            triggerFocus();
          }}
        />
      ),
      closable: false,
    });
  };

  const checkAndHandleEntryPolicyManually = (parameterPassDown, version) =>
    openModal({
      title: intl.formatMessage({
        id: 'modal.entryPolicy.title',
      }),
      content: (
        <EntryPolicyModal
          parameters={parameterPassDown}
          close={closeModal}
          version={version}
        />
      ),
      closable: false,
    });

  const onSubmit = event => {
    if (event) event.preventDefault();
    if (debounceTimeout.current) clearTimeout(debounceTimeout.current);

    handleScanData({
      scanData: inputValue,
      intl,
      scanner,
      setIsSuccess,
      checkForAdditionalData,
      checkAndHandleEntryPolicyManually,
      refetch,
    });

    setInputValue('');
  };

  const handleChange = event => {
    if (debounceTimeout.current) clearTimeout(debounceTimeout.current);

    setInputValue(event.target.value);

    // if value ends with `` it's possible that the submit was suppressed
    if (!event.target.value.endsWith('``')) return;
    debounceTimeout.current = setTimeout(() => {
      onSubmit();
    }, 200);
  };

  useEffect(() => {
    const focusInput = () => {
      inputReference.current.focus();
    };

    window.addEventListener('focus', focusInput);
    return () => {
      window.removeEventListener('focus', focusInput);
    };
  }, []);

  useEffect(() => {
    if (outerFocus) setOuterFocus(false);
    triggerFocus();
  }, [outerFocus, triggerFocus, setOuterFocus]);

  useEffect(() => {
    reloadFilter();
  }, []);

  return (
    <ScanFormWrapper>
      <Update latestUpdate={latestUpdate} callback={refetch} cam={false} />
      <Wrapper onClick={triggerFocus}>
        <FormWrapper>
          <Content>
            {intl.formatMessage({
              id: 'form.checkins',
            })}
            <b>{scanner.name}</b>
            {intl.formatMessage({
              id: 'form.checkinsSuffix',
            })}
            <Count>
              {currentCount}/{totalCount}
            </Count>
          </Content>
          {isSuccess ? (
            <SuccessOverlay data-cy="badgeCheckInSuccess" />
          ) : (
            <form onSubmit={onSubmit}>
              <HiddenInput
                type="text"
                ref={inputReference}
                autoFocus={!isOpen}
                autoComplete="off"
                value={inputValue}
                onChange={handleChange}
              />
            </form>
          )}

          {isSuccess && <Tick size={200} />}
        </FormWrapper>
      </Wrapper>
    </ScanFormWrapper>
  );
};
