import React from 'react';
import { useIntl } from 'react-intl';
import { PrimaryButton, SecondaryButton } from 'general';
import { handleV3Data, handleV4Data } from 'helpers/handleScanData';

import { Wrapper, ButtonRow, Info } from './EntryPolicyModal.styled';

export const EntryPolicyModal = ({ parameters, version, close }) => {
  const intl = useIntl();

  const checkin = () => {
    if (version === 3) {
      handleV3Data(parameters);
      return close();
    }
    if (version === 4) {
      handleV4Data(parameters);
      return close();
    }
    return close();
  };

  return (
    <Wrapper>
      <Info>
        {intl.formatMessage({
          id: 'modal.entryPolicy.description',
        })}
      </Info>

      <ButtonRow>
        <SecondaryButton onClick={close}>
          {intl.formatMessage({ id: 'modal.entryPolicy.denyCheckin' })}
        </SecondaryButton>
        <PrimaryButton onClick={checkin}>
          {intl.formatMessage({ id: 'modal.entryPolicy.checkin' })}
        </PrimaryButton>
      </ButtonRow>
    </Wrapper>
  );
};
