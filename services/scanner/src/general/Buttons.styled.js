import styled from 'styled-components';
import { Button } from 'antd';

const ButtonGeneral = {
  fontFamily: 'Montserrat-Bold, sans-serif',
  fontSize: '14px',
  fontWeight: 'bold',
  letterSpacing: 0,
  lineHeight: '16px',
  textTransform: 'uppercase',
  height: '32px',
  boxShadow: 'none',
};

export const PrimaryButton = styled(Button)`
  ${{ ...ButtonGeneral }}
  min-width: 139px;
  width: auto;
  padding-left: 40px;
  padding-right: 40px;
  background: rgb(195, 206, 217);
  border: 2px solid rgb(195, 206, 217);
  border-radius: 24px;
  color: rgba(0, 0, 0, 0.87);
  align-self: flex-end;
  cursor: pointer;
  transition: 0.1s ease-in-out all;

  &:hover,
  &:focus {
    color: rgba(0, 0, 0, 0.87) !important;
    background: rgb(155, 173, 191) !important;
    border-color: rgb(155, 173, 191) !important;
  }

  &:disabled {
    background: rgb(218, 224, 231) !important;
    border-color: rgb(218, 224, 231) !important;
    color: rgb(0 0 0 / 50%) !important;
    cursor: no-drop;
  }
`;

export const SecondaryButton = styled(Button)`
  ${{ ...ButtonGeneral }}
  color: rgba(0, 0, 0, 0.87);
  background: transparent;
  border-radius: 24px;
  border: 2px solid rgb(80, 102, 124);
  cursor: pointer;
  transition: 0.1s ease-in-out all;

  &:hover,
  &:focus {
    color: rgba(0, 0, 0, 0.87);
    background: rgb(218, 224, 231);
    border: 2px solid rgb(80, 102, 124);
  }

  &:disabled {
    color: rgba(0, 0, 0, 0.25);
    background: #f5f5f5;
    border-color: #d9d9d9;
    text-shadow: none;
    box-shadow: none;
    cursor: no-drop;
  }
`;
