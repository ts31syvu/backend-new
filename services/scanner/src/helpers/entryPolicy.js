/* eslint-disable no-bitwise */
import {
  QUICK_TESTED,
  PCR_TESTED,
  RECOVERED,
  VACCINATED,
} from 'constants/entryPolicy';

// eslint-disable-next-line complexity
export const checkEntryPolicy = (locationEntryPolicy, qrCodeEntryPolicy) => {
  switch (locationEntryPolicy) {
    case '2G':
      if (qrCodeEntryPolicy & (RECOVERED | VACCINATED)) return true;
      break;

    case '3G':
      if (
        qrCodeEntryPolicy &
        (RECOVERED | VACCINATED | QUICK_TESTED | PCR_TESTED)
      )
        return true;
      break;

    case '2GPlus':
      if (
        (RECOVERED & qrCodeEntryPolicy && QUICK_TESTED & qrCodeEntryPolicy) ||
        (VACCINATED & qrCodeEntryPolicy && QUICK_TESTED & qrCodeEntryPolicy) ||
        (RECOVERED & qrCodeEntryPolicy && PCR_TESTED & qrCodeEntryPolicy) ||
        (VACCINATED & qrCodeEntryPolicy && PCR_TESTED & qrCodeEntryPolicy)
      )
        return true;
      break;

    default:
      return true;
  }
  return false;
};
