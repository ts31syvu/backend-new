import jwt from 'jsonwebtoken';
import { verifySignedPublicDailyKey } from '@lucaapp/crypto';
import { getBasicCA, getRootCA } from 'network/static';
import { getDailyKey, getIssuer } from 'network/api';

export const getValidatedExtractedDailyKey = async () => {
  const rootCA = await getRootCA();
  const basicCA = await getBasicCA();
  const { signedPublicDailyKey } = await getDailyKey();

  const decodedJWT = jwt.decode(signedPublicDailyKey);
  const issuerId = decodedJWT.iss;

  const issuer = await getIssuer(issuerId);

  return verifySignedPublicDailyKey({
    certificateChain: [rootCA, basicCA],
    issuer,
    signedPublicDailyKey,
  });
};
