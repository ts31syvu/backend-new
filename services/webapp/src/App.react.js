import React from 'react';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router';
import { createBrowserHistory } from 'history';
import { ConnectedRouter } from 'connected-react-router';
import { QueryClient, QueryClientProvider } from 'react-query';

import 'moment/locale/de';
import moment from 'moment';

import { IntlProvider } from 'react-intl';

import {
  HOME_PATH,
  HISTORY_PATH,
  SETTINGS_PATH,
  LICENSES_ROUTE,
  EDIT_CONTACT_INFORMATION_SETTING,
} from 'constants/routes';

import { getLanguage } from 'utils/language';

import { ErrorWrapper } from 'components/ErrorWrapper';

import { History } from 'components/History';
import { Licenses } from 'components/Licenses';
import { Settings } from 'components/Settings';
import { LandingPage } from 'components/LandingPage';
import { ContactInformation } from 'components/ContactInformation';
import { AuthenticationWrapper } from 'components/AuthenticationWrapper.react';

import { configureStore } from 'configureStore';

import { AppWrapper } from './App.styled';

import { messages } from './messages';

const history = createBrowserHistory();
const store = configureStore(undefined, history);

moment.locale(getLanguage());
document.documentElement.lang = getLanguage();

const queryClient = new QueryClient();

export const Main = () => {
  return (
    <AppWrapper>
      <HelmetProvider>
        <Helmet>
          <meta name="apple-itunes-app" content="app-id=1531742708" />
        </Helmet>
        <Provider store={store}>
          <IntlProvider
            locale={getLanguage()}
            messages={messages[getLanguage()]}
            wrapRichTextChunksInFragment
          >
            <QueryClientProvider client={queryClient}>
              <ConnectedRouter history={history}>
                <ErrorWrapper>
                  <AuthenticationWrapper>
                    <Switch>
                      <Route path={LICENSES_ROUTE} component={Licenses} />
                      <Route
                        path={EDIT_CONTACT_INFORMATION_SETTING}
                        component={ContactInformation}
                      />
                      <Route path={HISTORY_PATH} component={History} />
                      <Route path={SETTINGS_PATH} component={Settings} />
                      <Route
                        path={`${HOME_PATH}/:scannerId/`}
                        component={LandingPage}
                      />
                      <Route path={HOME_PATH} component={LandingPage} />
                    </Switch>
                  </AuthenticationWrapper>
                </ErrorWrapper>
              </ConnectedRouter>
            </QueryClientProvider>
          </IntlProvider>
        </Provider>
      </HelmetProvider>
    </AppWrapper>
  );
};
