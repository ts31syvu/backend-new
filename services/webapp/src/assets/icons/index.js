// SVGs
import ErrorIconSVG from './ErrorIcon.svg';
import LucaLogoBlackIconSVG from './LucaLogoBlackIcon.svg';
import LucaLogoWhiteIconSVG from './LucaLogoWhiteIcon.svg';
import MissingFeatureIconSVG from './MissingFeatureIcon.svg';
import MenuIconSVG from './MenuIcon.svg';
import DownloadIconSVG from './DownloadIcon.svg';

// Icons
export { ReactComponent as ErrorIcon } from './ErrorIcon.svg';
export { ReactComponent as LucaLogoWhiteIcon } from './LucaLogoWhiteIcon.svg';
export { ReactComponent as LucaLogoBlackIcon } from './LucaLogoBlackIcon.svg';
export { ReactComponent as DownloadIcon } from './DownloadIcon.svg';
export { ReactComponent as MenuIcon } from './MenuIcon.svg';
export { ReactComponent as MissingFeatureIcon } from './MissingFeatureIcon.svg';
export { ReactComponent as FoodIcon } from './Food.svg';
export { ReactComponent as MapIcon } from './Map.svg';
export { ReactComponent as TimetableIcon } from './Timetable.svg';
export { ReactComponent as WebsiteIcon } from './Website.svg';
export { ReactComponent as MoreInfosIcon } from './MoreInfos.svg';

export {
  ErrorIconSVG,
  LucaLogoBlackIconSVG,
  LucaLogoWhiteIconSVG,
  MissingFeatureIconSVG,
  MenuIconSVG,
  DownloadIconSVG,
};
