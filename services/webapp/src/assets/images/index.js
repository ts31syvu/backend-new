// Images
import HelloImage from './HelloImage.png';
import AppStoreImage from './AppStoreImage.png';
import GooglePlayImage from './GooglePlayImage.png';
import BackgroundImage from './BackgroundImage.png';

export { HelloImage, AppStoreImage, GooglePlayImage, BackgroundImage };
