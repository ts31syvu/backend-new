import { Steps } from 'antd';
import styled from 'styled-components';

import { AppContent } from 'components/AppLayout';
import { SecondaryButton } from 'components/Buttons';
import { SmallHeadline, Text } from 'components/Text';

export const StyledHeaderMenuIconContainer = styled.button`
  flex: 1;
  margin: 0;
  border: none;
  display: flex;
  outline: none;
  padding: 20px 0;
  align-items: center;
  justify-content: flex-end;
  background-color: transparent;
`;

export const StyledMenuIcon = styled.img`
  height: 4px;
  width: 20px;
`;

export const StyledHistoryStepContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;
export const StyledHistoryInfoContainer = styled.div`
  width: 100%;
  display: flex;
`;

export const StyledSteps = styled(Steps)`
  margin: 16px 0 0;

  .ant-steps-item-title {
    padding: 0;
    width: 100%;
  }
`;
export const StyledSecondaryButton = styled(SecondaryButton)`
  width: 100%;
  height: 36px;
  margin: 12px 0;
  max-width: 296px;
`;
export const StyledAddButton = styled(SecondaryButton)`
  width: 100%;
  height: 36px;
  max-width: 296px;
  margin-top: 12px;
  color: rgb(184, 192, 202);
  border: 2px solid rgb(184, 192, 202);
  background-color: rgba(227, 225, 221, 0.1);

  &:focus,
  &:active {
    color: rgb(184, 192, 202);
    border: 2px solid rgb(184, 192, 202);
    background-color: rgba(227, 225, 221, 0.1);
  }
`;
export const StyledNumberOfDataAccessContainer = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
`;
export const StyledNumberOfDataAccessInfoText = styled(Text)`
  flex: 1;
  display: block;
  font-weight: bold;
  color: rgb(255, 255, 255);
`;
export const StyledFooter = styled(AppContent)`
  flex: unset;
  padding: 0 8px;
`;
export const StyledNumberOfDataAccess = styled(SmallHeadline)`
  font-weight: bold;
`;
export const StyledHistoryTitle = styled(Text)`
  color: rgb(255, 255, 255);
`;
export const StyledHistoryInfoTitle = styled(Text)`
  flex: 1;
  color: rgb(255, 255, 255);
`;
export const StyledHistoryContent = styled(Text)`
  font-weight: bold;
  color: rgb(255, 255, 255);
`;
