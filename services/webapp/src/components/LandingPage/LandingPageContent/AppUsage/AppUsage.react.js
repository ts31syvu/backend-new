import React, { useEffect, useState } from 'react';
import { useIntl } from 'react-intl';
import { useHistory } from 'react-router-dom';
import { indexDB } from 'db';
import { AppStoreImage, GooglePlayImage } from 'assets/images';

import { HISTORY_PATH } from 'constants/routes';

import {
  CheckInWrapper,
  UseContactFormLink,
  StyledInfoText,
  StyledHeadline,
  StyledStoreLogo,
  StyledStoreLink,
  StyledContainer,
  StyledStoreWrapper,
} from './AppUsage.styled';

export const AppUsage = ({ formId }) => {
  const intl = useIntl();
  const history = useHistory();
  const [userExists, setUserExists] = useState(false);

  useEffect(() => {
    (async () => {
      const userCount = await indexDB.users.count().catch(() => 0);
      setUserExists(userCount !== 0);
    })();
  }, []);

  const visitContactForm = () =>
    window.location.replace(`${window.location.origin}/contact-form/${formId}`);

  const showHistory = () => {
    history.push(HISTORY_PATH);
  };

  return (
    <StyledContainer>
      <StyledHeadline>
        {intl.formatMessage({
          id: 'OnBoarding.WebAppWarningStep.Headline',
        })}
      </StyledHeadline>
      <StyledInfoText>
        {intl.formatMessage({
          id: 'OnBoarding.WebAppWarningStep.Description',
        })}
      </StyledInfoText>
      <StyledStoreWrapper>
        <StyledStoreLink
          rel="noopener noreferrer"
          href="https://play.google.com/store/apps/details?id=de.culture4life.luca"
        >
          <StyledStoreLogo src={GooglePlayImage} alt="Google Play" />
        </StyledStoreLink>
        <StyledStoreLink
          rel="noopener noreferrer"
          href="https://apps.apple.com/de/app/luca-app/id1531742708"
        >
          <StyledStoreLogo src={AppStoreImage} alt="Apple App Store" />
        </StyledStoreLink>
      </StyledStoreWrapper>
      {formId && (
        <CheckInWrapper>
          <UseContactFormLink id="next" tabIndex="5" onClick={visitContactForm}>
            {intl.formatMessage(
              { id: 'OnBoarding.WebAppWarningStep.Submit' },
              { br: <br /> }
            )}
          </UseContactFormLink>
        </CheckInWrapper>
      )}
      <StyledInfoText>
        {intl.formatMessage({
          id: 'OnBoarding.historyUsage.Description',
        })}
      </StyledInfoText>
      {userExists && (
        <CheckInWrapper>
          <UseContactFormLink onClick={showHistory}>
            {intl.formatMessage({
              id: 'OnBoarding.historyUsage.Button',
            })}
          </UseContactFormLink>
        </CheckInWrapper>
      )}
    </StyledContainer>
  );
};
