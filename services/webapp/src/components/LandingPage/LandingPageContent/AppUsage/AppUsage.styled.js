import styled from 'styled-components';
import { Headline, Text } from 'components/Text';
import { SecondaryButton } from 'components/Buttons';

export const StyledContainer = styled.div`
  flex: 1;
  display: flex;
  box-sizing: border-box;
  flex-direction: column;
  padding: 40px 24px 0 24px;
  align-items: center;
`;
export const StyledContent = styled.main`
  flex: 1;
  display: flex;
  overflow-y: scroll;
  overflow-x: hidden;
  padding-bottom: 32px;
  flex-direction: column;
`;
export const CheckInWrapper = styled.div`
  margin-bottom: 40px;
`;
export const StyledHeadline = styled(Headline)`
  margin-bottom: 16px;
  color: rgb(255, 255, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 16px;
  font-weight: bold;
`;
export const StyledInfoText = styled(Text)`
  display: block;
  color: #fff;
  line-height: 20px;
  margin-bottom: 24px;
`;
export const StyledStoreLogo = styled.img`
  height: 60px;
  object-fit: contain;
`;
export const StyledStoreLink = styled.a`
  border: none;
  outline: none;
  background: transparent;
`;
export const StyledStoreWrapper = styled.div`
  display: flex;
  padding: 8px 0;
  flex-wrap: wrap;
  justify-content: center;
`;
export const UseContactFormLink = styled(SecondaryButton)`
  width: 100%;
  color: rgb(160, 200, 255);
  font-family: Montserrat-Bold, sans-serif;
  font-size: 14px;
  font-weight: bold;
  text-align: center;
  text-transform: uppercase;
  background: transparent;
  border: none;
  &:hover {
    color: #fff;
    background: transparent;
  }
`;

export const ShowHistoryButton = styled(SecondaryButton)``;
