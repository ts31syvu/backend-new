import React from 'react';
import { LucaLogoBlackIconSVG } from 'assets/icons';

import { StyledLucaLogo, StyledHeader } from './Header.styled';

export const Header = () => {
  return (
    <StyledHeader>
      <StyledLucaLogo alt="luca" src={LucaLogoBlackIconSVG} />
    </StyledHeader>
  );
};
