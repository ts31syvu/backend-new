import styled from 'styled-components';

export const StyledHeader = styled.div`
  padding: 24px;
`;

export const StyledLucaLogo = styled.img`
  height: 48px;
  width: 87px;
`;
