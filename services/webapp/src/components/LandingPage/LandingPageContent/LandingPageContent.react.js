import React from 'react';
import { useQuery } from 'react-query';

import { getScanner } from 'network/api';
import { Header } from './Header';
import { AppUsage } from './AppUsage';
import { LocationInfo } from './LocationInfo';

import {
  StyledContainer,
  BackgroundWrapper,
} from './LandingPageContent.styled';

export const LandingPageContent = ({ scannerId }) => {
  const { data } = useQuery(
    ['scanner', scannerId],
    () => getScanner(scannerId),
    {
      enabled: !!scannerId,
    }
  );

  return (
    <StyledContainer>
      <BackgroundWrapper>
        <Header />
        <LocationInfo locationId={data?.locationId} />
      </BackgroundWrapper>
      <AppUsage formId={data?.formId} />
    </StyledContainer>
  );
};
