import styled from 'styled-components';
import { BackgroundImage } from 'assets/images';

export const StyledContainer = styled.div`
  flex: 1;
  display: flex;
  overflow-x: hidden;
  overflow-y: scroll;
  box-sizing: border-box;
  flex-direction: column;
`;

export const BackgroundWrapper = styled.div`
  background-image: url(${BackgroundImage});
`;
