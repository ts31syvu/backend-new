import React from 'react';
import { useIntl } from 'react-intl';

import {
  FoodIcon,
  MapIcon,
  TimetableIcon,
  WebsiteIcon,
  MoreInfosIcon,
} from 'assets/icons';

import {
  Wrapper,
  LinkItem,
  LinkItemText,
  ClickableLinkWrapper,
} from './LinkDisplay.styled';

const iconMap = {
  general: MoreInfosIcon,
  map: MapIcon,
  menu: FoodIcon,
  schedule: TimetableIcon,
  website: WebsiteIcon,
};

const linkItem = (key, value, intl) => {
  const Icon = iconMap[key];

  return (
    <ClickableLinkWrapper href={value} target="_blank" key={key}>
      <LinkItem>
        <Icon />
        <LinkItemText>
          {intl.formatMessage({
            id: `OnBoarding.WelcomeStep.Link.${key}`,
          })}
        </LinkItemText>
      </LinkItem>
    </ClickableLinkWrapper>
  );
};

export const LinkDisplay = ({ links }) => {
  const intl = useIntl();
  const linkItems = Object.keys(links)
    .filter(key => !!links[key])
    .map(key => linkItem(key, links[key], intl));

  return <Wrapper>{linkItems}</Wrapper>;
};
