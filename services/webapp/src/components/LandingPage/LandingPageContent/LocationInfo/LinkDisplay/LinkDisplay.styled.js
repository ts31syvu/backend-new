import styled from 'styled-components';
import { Media } from 'utils/media';

export const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  flex-direction: row;

  ${Media.tablet`
   overflow-x: scroll;
    max-width: 375px;
    padding: 0 10px;
`};
`;

export const ClickableLinkWrapper = styled.a`
  &:active,
  &:focus {
    outline: 0;
    border: none;
    -moz-outline-style: none;
  }
`;

export const LinkItem = styled.div`
  border-radius: 32px;
  border: 1px solid black;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 12px;
  margin: 8px;
`;

export const LinkItemText = styled.div`
  padding-left: 6px;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  color: black;
  white-space: nowrap;
`;
