import { SUPPORT_EMAIL } from 'constants/links';

export const getMailTo = (locationName, links, intl) => {
  const linkString = Object.values(links)
    .filter(value => !!value)
    .map(value => encodeURIComponent(value))
    .join(' %0D%0A%0D%0A ');

  const subject = intl.formatMessage({
    id: 'OnBoarding.WelcomeStep.Link.reportEmailSubject',
  });
  const body = intl.formatMessage(
    { id: 'OnBoarding.WelcomeStep.Link.reportEmailBody' },
    { linkString, locationName }
  );

  return `mailto:${SUPPORT_EMAIL}?subject=${subject}&body=${body}`;
};
