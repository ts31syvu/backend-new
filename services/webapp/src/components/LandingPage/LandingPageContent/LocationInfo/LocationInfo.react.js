import React from 'react';
import { useQuery } from 'react-query';
import { useIntl } from 'react-intl';
import { getLocationURLs, getLocation, getPaymentActive } from 'network/api';
import { PAYMENT_PATH } from 'constants/environment';
import { getMailTo } from './LocationInfo.helper';
import {
  Wrapper,
  HeaderText,
  NameText,
  ReportLinkMailTo,
  LocationNameText,
  PaymentWrapper,
  PoweredByRapyd,
  StyledPaymentButton,
} from './LocationInfo.styled';
import { LinkDisplay } from './LinkDisplay';

export const LocationInfo = ({ locationId }) => {
  const intl = useIntl();
  const { data: location } = useQuery(
    ['location', locationId],
    () => getLocation(locationId),
    { enabled: !!locationId }
  );

  const { data: links } = useQuery(
    ['locationURLs', locationId],
    () => getLocationURLs(locationId),
    { enabled: !!locationId }
  );

  const { data: payment } = useQuery(
    ['paymentActive', locationId],
    () => getPaymentActive(locationId),
    { enabled: !!locationId }
  );

  const isPaymentAvailable = payment?.paymentActive;

  if (!links || !location) return null;

  const hasNoLinks = Object.values(links).every(link => !link);

  const openPayWebapp = () => window.open(`${PAYMENT_PATH}/${locationId}`);

  return (
    <Wrapper>
      <HeaderText>
        {intl.formatMessage({ id: 'OnBoarding.WelcomeStep.WelcomeTo' })}
      </HeaderText>

      {location.locationName && (
        <LocationNameText>{location.groupName}</LocationNameText>
      )}
      <NameText>
        {location.locationName ? location.locationName : location.groupName}
      </NameText>

      <LinkDisplay links={links} />
      {!hasNoLinks && (
        <ReportLinkMailTo
          href={getMailTo(location.name, links, intl)}
          rel="noopener noreferrer"
        >
          {intl.formatMessage({ id: 'OnBoarding.WelcomeStep.ReportAbuse' })}
        </ReportLinkMailTo>
      )}
      {isPaymentAvailable && (
        <PaymentWrapper>
          <PoweredByRapyd>
            {intl.formatMessage({
              id: 'OnBoarding.WelcomeStep.PoweredByRapyd',
            })}
          </PoweredByRapyd>
          <StyledPaymentButton onClick={openPayWebapp}>
            {intl.formatMessage({
              id: 'OnBoarding.WelcomeStep.StartPayment',
            })}
          </StyledPaymentButton>
        </PaymentWrapper>
      )}
    </Wrapper>
  );
};
