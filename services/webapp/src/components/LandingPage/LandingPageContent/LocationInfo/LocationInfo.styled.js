import styled from 'styled-components';
import { Button } from 'antd';

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const HeaderText = styled.div`
  font-size: 14px;
  font-weight: bold;
  color: black;
  padding-bottom: 8px;
`;

export const NameText = styled.div`
  font-size: 20px;
  font-weight: bold;
  color: black;
  padding-bottom: 24px;
`;

export const LocationNameText = styled.div`
  color: black;
  font-size: 14px;
`;
export const ReportLinkMailTo = styled.a`
  font-size: 12px;
  text-transform: uppercase;
  color: black;
  padding-top: 16px;
  padding-bottom: 40px;
`;

export const PaymentWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 8px;
  padding: 40px 16px;
  width: 100%;
`;

export const PoweredByRapyd = styled.p`
  color: rgb(92, 92, 96);
  font-size: 14px;
  font-weight: 500;
  height: 20px;
  letter-spacing: 0;
  line-height: 20px;
  margin: 0;
  text-align: center;
`;

export const StyledPaymentButton = styled(Button)`
  background: rgb(0, 0, 0);
  color: white;
  border-radius: 24px;
  height: 48px;
`;
