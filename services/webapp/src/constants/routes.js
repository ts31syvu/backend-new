export const BASE_PATH = '/webapp';
export const HOME_PATH = BASE_PATH;
export const HISTORY_PATH = `${BASE_PATH}/history`;
export const LICENSES_ROUTE = `${BASE_PATH}/licenses`;
export const SETTINGS_PATH = `${BASE_PATH}/settings`;
export const EDIT_CONTACT_INFORMATION_SETTING = `${BASE_PATH}/settings/contact`;
export const REPOSITORY_URL =
  'https://gitlab.com/lucaapp/web/-/tree/master/services/webapp';
